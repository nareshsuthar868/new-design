$(document).ready(function(){
    
     $("#cartSubmit").submit(function(e){
        var radioValue = $("input[name='payment-radio']:checked").val();
        if(radioValue == '' || radioValue == null){
            e.preventDefault(e);
            alert('Please select payment method');
        }
    });
    
    $('input[type=radio][name=payment_mode]').change(function() {
            if (this.value == 'debit_cart') {
               $('#si_debit_banks').show();
            }else{
                  $('#si_debit_banks').hide();
            }
    });
    
    $(".password-show").click(function(){
    	if($(this).prev('input').attr('type') == 'password'){
            $(this).prev('input').prop('type', 'text');
        }else{
            $(this).prev('input').prop('type', 'password');
        }
        
    	$(this).children().toggleClass('icn-eye');
    });		

    
    // open popup in my orders 
    $('.manage_order').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name'		
	});
	$('.change_payment_mode').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name'		
	});
	
	$('.service_request_type').click(function(){
		$(".service-type-listing").slideToggle().toggleClass('open');	
	});
		
	$('body').click(function(e) {
        if($('.service-type-listing').hasClass('open')) {
            if (e.target != $('.service_request_type')[0] || e.target  == $('.service-type-listing')[0]){
             	$(".service-type-listing").slideToggle().removeClass('open');
            }
        }
    }); 
    
	$('.proceed-btn').click(function(){
		var tab_id = $(this).attr('data-tab');
	    $('.fixed-rental-plans-section').addClass('d-none');
		$('#'+tab_id).removeClass('d-none');
		window.scrollTo({top:0,behavior:'smooth'});
	})
	
	$('.back-btn').click(function(){
		var tab_id = $(this).attr('data-tab');
	    $('.fixed-rental-plans-section').addClass('d-none');
		$('#'+tab_id).removeClass('d-none');
		window.scrollTo({top:0,behavior:'smooth'}); 
	})
	
	$('.add-coin').click(function(){
	    $('.credit-form').removeClass('d-none');
	})
	
	$(document).on('click','.apply-coupon',function(){
	    var mythis = this;
	    $('#CouponErr').html('');
        $('#CouponErr').show();
        var cartValue = $('#cart_amount').val();
    
        if (cartValue > 0) {
            var code =  $(this).attr('data-coupon');
            var amount = $('#cart_total_amount').val();
            var shipamount = $('#cart_ship_amount').val();
            var taxamount = $('#cart_tax_amount').val();
            if (code != '') {
                $.ajax({
                    type: 'POST',
                    url: baseURL + 'site/cart/checkCode',
                    data: {
                        'code': code,
                        'amount': amount,
                        'shipamount': shipamount
                    },
                    success: function(response) {
                        // checkRemove(code);
                        var resarr = response.split('|');
                    
                        // console.log("here is code message",resarr);
                        if (response == 1) {
                            $('#CouponErr').html('Entered code is invalid');
                            $('#CouponErr').css('color','red');
                            return false;
                        } else if (response == 2) {
                            $('#CouponErr').html('Remove existing coupon before applied.');
                             $('#CouponErr').css('color','red');
                            return false;
                        } else if (response == 3) {
                            $('#CouponErr').html('Please add more items in the cart and enter the coupon code');
                            $('#CouponErr').css('color','red'); 
                            return false;
                        } else if (response == 4) {
                            $('#CouponErr').html('Entered Coupon code is not valid for this product');
                             $('#CouponErr').css('color','red');
                            return false;
                        } else if (response == 5) {
                            $('#CouponErr').html('Entered Coupon code is expired');
                             $('#CouponErr').css('color','red');
                            return false;
                        } else if (response == 6) {
                            $('#CouponErr').html('Entered code is Not Valid');
                             $('#CouponErr').css('color','red');
                            return false;
                        } else if (response == 7) {
                            $('#CouponErr').html('Please add more items quantity in the particular category or product, for using this coupon code');
                             $('#CouponErr').css('color','red');
                            return false;
                        } else if (response == 8) {
                            $('#CouponErr').html('Entered Gift code is expired');
                             $('#CouponErr').css('color','red');
                            return false;
                        }else if ($.trim(resarr[0]) == 'Success') {
                              $('#is_coupon').val(code);
                            $.ajax({
                                type: 'POST',
                                url: baseURL + 'site/cart/checkCodeSuccess',
                                data: {
                                    'code': code,
                                    'amount': amount,
                                    'shipamount': shipamount
                                },
                                success: function(response) {
                                    var arr = response.split('|');
                                    $('#CouponErr').html('Coupon Code Applied');
                                    $('#CouponErr').css('color','green');
                                    $('#CouponMessage').html(resarr[3]);
                                    $('#CouponMessage').css('color','green');
                                    $('#cart_amount').val(arr[0]);
                                    $('#cart_ship_amount').val(arr[1]);
                                    $('#cart_tax_amount').val(arr[2]);
                                    $('#cart_total_amount').val(arr[3]);
                                    $('#discount_Amt').val(arr[4]);
                                    //this is for advance rental
                                    // $('#CartAmt').html(arr[0]);
                                    $('#rental_amt').html('<i id="p-price" class="fa fa-inr" aria-hidden="true"></i>'+ arr[0]);
                                    // $('#CartAmt1').html(arr[0]);
                                    $('#CartTAmt').html(arr[2]);
                                    $('#CartGAmt').html('<i id="p-price" class="fa fa-inr" aria-hidden="true"></i>'+ arr[3]);
                                    $('#disAmtVal').html(arr[4]);
                                    $('#3rdStepDiscount').html(arr[4]);
                                    $('#disAmtVal_header').html('<i id="p-price" class="fa fa-inr" aria-hidden="true"></i>'+arr[4]);
                                    $('#disAmtValDiv').show();
                                    $('#CouponCode').val(code);
                                    $('#Coupon_id').val(resarr[1]);
                                    $('#couponType').val(resarr[2]);
                                    $('#MCartGAmt').html(arr[3]);
                                    $('#CartCartGAmt').html(arr[3]);
                                    $('#CartCartGAmt1').html(arr[3]);
                                    //Step 4
                                    $('#CartCartGAmt2').html(arr[3]);
                                    var j = 6;
                                    for (var i = 0; i < arr[5]; i++) {
                                        $('#IndTotalVal' + i).html(arr[j]);
                                        j++;
                                    }
                                    $("#CheckCodeButton").val('Remove');
                                    $("#is_coupon").attr('readonly', 'readonly');
                                    $(mythis).removeClass('apply-coupon');
                                    // $('section[data-coupon = '+code+']').removeClass('apply-coupon');
                                    // $('section[data-coupon = '+code+']').addClass('coupon-code');
                                    $(mythis).addClass('coupon-code');
                                    document.getElementById("CheckCodeButton").setAttribute("onclick", "javascript:checkRemove();");
                                }
                            });
                        }
                    }
                });
            } else {
                $('#CouponErr').html("<font color='red'>Enter Valid Code</font>");
            }
        } else {
            $('#CouponErr').html('Please add items in cart and enter the coupon code');
        }
        setTimeout("hideErrDiv('CouponErr')", 3000);
	})
	
	$(document).on('click','.address-box',function(e){
	   $('.address-box').removeClass('selected');
	   $(this).addClass('selected');
	   $('.icn').removeClass('icn-correct-red');
	   $(this).find('.icn').addClass('icn-correct-red');
	   $("#address_proceed_button").attr("data-address", $(this).attr('data-current-add-id'));
	})
    
	/*--- Trending product tabbing ---*/
	$('ul.trending li').click(function(){
	   	var tab_id = $(this).attr('data-tab');
		$('ul.trending li').removeClass('current');
		$(this).addClass('current');
		$(".trending-content").removeClass('current');
		$("#"+tab_id).addClass('current');
		$('.trending-product-slider').slick('refresh');
	});

	/*--- Trending product slider ---*/
	$('.trending-product-slider').slick({
		dots: true,
		arrows: true,
		slidesToShow: 4,
		infinite: false,
		prevArrow: '<span class="prev-arrow"><i class="icn-left-arrow-red"></i></span>',
		nextArrow: '<span class="next-arrow"><i class="icn-right-arrow-red"></i></span>',
		responsive: [
		    {
		      breakpoint: 1025,
		      settings: {		       		        
		        slidesToShow: 3
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {		        
		        slidesToShow: 2,
		        dots: true
		      }
		    }
		]
	});

	/*--- thumbnail slider ---*/
	$('#banner-slider').slick({
		dots: false,
		arrows: false,
		slidesToShow: 1,
		infinite: true,		
		prevArrow: '<span class="prev-arrow"><i class="icn-left-arrow-big-red"></i></span>',
		nextArrow: '<span class="next-arrow"><i class="icn-right-arrow-big-red"></i></span>',
		asNavFor: '#banner-tags-listing',
		responsive: [		    
		    {
		      breakpoint: 1025,
		      settings: {		        
		        dots: true,
		      }
		    }
		]
	});
	$('#banner-tags-listing').slick({
		slidesToShow: 4,
		slidesToScroll: 4,
		arrows: false,
		asNavFor: '#banner-slider',	
		focusOnSelect: true,
		variableWidth: true,
		swipe: false,
	});

	$("#header-search").on('click',function(){
    	$('header .search-bar').addClass('show');
    });

    $("#search-close").on('click',function(){
    	$('header .search-bar').removeClass('show');
    });
	/*--- sticky header ---*/
    var num = 35;

	$(window).bind('scroll', function () {
	    if ($(window).scrollTop() > num) {
	        $('header').addClass('sticky');
	    } else {
	        $('header').removeClass('sticky');
	    }
	});

    $(".menu-mobile").on('click',function(){
    	$('.mobile-navigation').addClass('active');
    	$(this).addClass('active');
    	$("body").addClass('scroll-hide');    	
    });
    $(".mobile-navigation .close").on('click',function(){
    	$('.mobile-navigation').removeClass('active');
    	$(this).removeClass('active');
    	$("body").removeClass('scroll-hide'); 
    });    
	
	/*--- header sub menu open on click ---*/
	$(".sub-menu").siblings("a").after("<span class='caret icn-arrow-bottom'></span>");
	$('.caret').click(function(ev) {
		if (!($(this).parent().hasClass("open"))) {
			$('.sub-menu').not($(this).parents('.sub-menu')).slideUp();
			$('.mobile-navigation-part > li').not($(this).parents('li')).removeClass('open');
			$(this).next('.sub-menu').slideToggle();
			$(this).parent().addClass("open");
			ev.stopPropagation();
		} else {
			$(this).parent().removeClass("open");
			$(this).siblings('.sub-menu').slideUp();
		}
	});

	/*--- Accordion ---*/
	$('.accordion-tab.current .accordion-content').slideDown();
	$('.accordion-tab.current').addClass('active');
	$(".accordion-title").click(function() {
		$(".accordion-content").not($(this).siblings()).slideUp(); 
		$(".accordion-tab").not($(this).parent()).removeClass('active');

		$(this).siblings().slideToggle();
		$(this).parent('.accordion-tab').toggleClass('active'); 
	});

	/*--- Header location Expand ---*/
	$(".header-location .location").click(function(){
		$(".location-part").slideDown();
		$("body").addClass('overlay');
	});
	$(".location-part .close").click(function(){
		$(".location-part").slideUp();
		$("body").removeClass('overlay');
	});

	/*--- Innerpage banner content Expand ---*/
	$("#clickToggle").click(function(){
		$(".content-expand").slideToggle();
	});

	/*--- more items Expand ---*/
	$(".items-count").click(function(){
		$(".more-items-expand").slideToggle().toggleClass('expanded');
		$(this).toggleClass('expanded');
	});


	/*--- mobile category slider ---*/
	$('.mobile-category-slider').slick({
		dots: false,
		arrows: true,
		slidesToShow: 3,
		infinite: false,
		prevArrow: '<span class="prev-arrow"><i class="icn-arrow-left"></i></span>',
		nextArrow: '<span class="next-arrow"><i class="icn-arrow-right"></i></span>'
	});

	/*--- filter/sort tabbing ---*/
	$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	});

	$(".mobile-filterbar-wrapper > span").click(function(){
		$(".mobile-filterbox").addClass('open');
		$("body").addClass('scroll-hide');
	});
	$(".mobile-filterbox .close").click(function(){
		$(".mobile-filterbox").removeClass('open');
		$("body").removeClass('scroll-hide');
	});

	/*product details page*/

	/*--- thumbnail slider ---*/
	$('.product-slider .slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		adaptiveHeight: true,
		fade: true,
		asNavFor: '.slider-nav',
		responsive: [		    
		    {
		      breakpoint: 768,
		      settings: {		        
		        dots: true,
		      }
		    }
		]
	});
	$('.product-slider .slider-nav').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: false,
		asNavFor: '.product-slider .slider-for',	
		focusOnSelect: true,
		variableWidth: true,
		vertical: true,
		verticalSwiping: true
	});

	/*--- product duration Expand ---*/
// 	$('body').click(function(e) {
//         if($('.duration-dropdown').hasClass('open')) {
//             if (e.target != $('.duration-box')[0] || e.target  == $('.duration-dropdown')[0]){
//              	$(".duration-dropdown").slideToggle().removeClass('open');
//             }
//         }
//     });
	$(".duration-box").click(function(){
	    if($(".duration-dropdown").hasClass('open')){
	        $(".duration-dropdown").slideToggle().removeClass('open');
	    }else{
	        $(".duration-dropdown").slideToggle().toggleClass('open');   
	    }
	});
	
	$("ul.duration-dropdown li label span").click(function(){
	    $(".duration-dropdown").slideToggle().removeClass('open');
	});


	/*--- related product slider ---*/
	$('.related-product-slider').slick({
		dots: false,
		arrows: true,
		slidesToShow: 4,
		infinite: false,
		prevArrow: '<span class="prev-arrow"><i class="icn-left-arrow-big-red"></i></span>',
		nextArrow: '<span class="next-arrow"><i class="icn-right-arrow-big-red"></i></span>',
		responsive: [
		    {
		      breakpoint: 1025,
		      settings: {		       		        
		        slidesToShow: 3
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {		        
		        slidesToShow: 2,
		        dots: true
		      }
		    }
		]
	});

	/*--- testimonial slider ---*/
	$('.testimonial-slider').slick({
		dots: false,
		arrows: true,
		slidesToShow: 1,
		infinite: true,
		centerMode: true,
		centerPadding: '330px',
		prevArrow: '<span class="prev-arrow"><i class="icn-left-arrow-big-red"></i></span>',
		nextArrow: '<span class="next-arrow"><i class="icn-right-arrow-big-red"></i></span>',
		responsive: [
		    {
		      breakpoint: 1231,
		      settings: {		       
		        centerMode: true,
		        centerPadding: '100px',
		        slidesToShow: 1
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {		        
		        centerMode: false,
		        centerPadding: '0',
		        slidesToShow: 1,
		        arrows: false
		      }
		    },
		   
		]
	});
	
	/*--- custom scrollbar ---*/
	$(window).on("load",function(){
        $(".content").mCustomScrollbar({theme:"minimal-dark"});
    });

    /*--- home testimonial slider ---*/
	$('.home-testimonial-slider').slick({
		dots: false,
		arrows: true,
		slidesToShow: 3,
		infinite: true,
		centerMode: true,
		centerPadding: '0px',
		prevArrow: '<span class="prev-arrow"><i class="icn-left-arrow-red"></i></span>',
		nextArrow: '<span class="next-arrow"><i class="icn-right-arrow-red"></i></span>',
		responsive: [
		    {
		      breakpoint: 1231,
		      settings: {		       
		        centerMode: true,		        
		        slidesToShow: 3
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {		        
		        // centerMode: false,
		        centerPadding: '40px',
		        slidesToShow: 1,
		        arrows: false
		      }
		    },
		   
		]
	});

	// $('#add-product-link').magnificPopup({
	// 	type: 'inline',
	// 	preloader: false,
	// 	focus: '#name',
	// 	callbacks: {
	// 		beforeOpen: function() {
	// 			$('.product-popup-slider').slick('refresh');
	// 			if($(window).width() < 700) {
	// 				this.st.focus = false;
	// 			} else {
	// 				this.st.focus = '#name';
	// 			}
	// 		}
	// 	},
	// });


	$(".header-icons-listing > li > a .icn-cart").click(function(){
		$(".minicart-section").addClass('open');
		$("body").addClass('scroll-hide');
	});
	$(".minicart-section-wrapper .close").click(function(){
		$(".minicart-section").removeClass('open');
		$("body").removeClass('scroll-hide');
	});



	$('.FlowupLabels').FlowupLabels({
		/*
		 * These are all the default values
		 * You may exclude any/all of these options
		 * if you won't be changing them
		 */
		
		// Handles the possibility of having input boxes prefilled on page load
		feature_onInitLoad: true, 
		
		// Class when focusing an input
		class_focused: 		'focused',
		// Class when an input has text entered
		class_populated: 	'populated'	
	});

	/*--- items included slider (for mobile) ---*/
    $('.items-included-slider').slick({
		dots: true,
		arrows: false,
		slidesToShow: 1,
		infinite: true,		
	});

	/*--- month range slider (for mobile) ---*/
	$(".js-range-slider").ionRangeSlider({
        type: "single",
        min: 6,
        max: 36,        
        grid: true,
        skin: "round"
    });    


	$('.product-popup-slider').slick({
		dots: true,
		arrows: true,
		slidesToShow: 1,
		infinite: true,		
		prevArrow: '<span class="prev-arrow"><i class="icn-left-arrow-red"></i></span>',
		nextArrow: '<span class="next-arrow"><i class="icn-right-arrow-red"></i></span>',
		responsive: [
		    {
		      breakpoint: 1231,
		      settings: {		       
		        
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {		        
		        
		      }
		    },
		   
		]
	});

	$(".delivery-info-listing li span .icn").click(function(){
		$(".help-box").toggleClass('show');

	});
	
	$(".account-sidebar").stick_in_parent({
		offset_top: 95
	});

	$('.product-options-wrapper .product-img').slick({
		dots: true,
		arrows: true,
		slidesToShow: 1,
		infinite: true,		
		prevArrow: '<span class="prev-arrow"><i class="icn-left-arrow-red"></i></span>',
		nextArrow: '<span class="next-arrow"><i class="icn-right-arrow-red"></i></span>',
		responsive: [
		    {
		      breakpoint: 1231,
		      settings: {		       
		        
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {		        
		        
		      }
		    },
		   
		]
	});

	$('body').on('click', 'footer .footer-widget h4', function(e) {
	    $('.footer-widget').find('.footer-links-wrapper').stop().slideUp();	    
	    $('.footer-widget').find('h4').stop().removeClass('open');
	    $(this).closest('.footer-widget').find('.footer-links-wrapper').stop().slideToggle().toggleClass('open');
	    $(this).closest('.footer-widget').find('h4').stop().toggleClass('open');
	});
	$('.footer-expand').click(function(){
		$(".footer-main").slideToggle();		
		$(".footer-copyright").slideToggle();		
	});	
});

$(document).ready(function(e) {
	$('#add-product-link').click(function(){
		$(".product-options-wrapper").addClass('open');
		$("body").addClass('scroll-hide');
		event.stopPropagation('.product-options-wrapper');
	});
	$(".product-options-wrapper .close").click(function(){
		$(".product-options-wrapper").removeClass('open');
		$("body").removeClass('scroll-hide');
	});
	$(document).on('click', function (event) {
	  if (!$(event.target).closest('.product-options-wrapper').length) {
	    $(".product-options-wrapper").removeClass("open");
	    $("body").removeClass('scroll-hide');
	  }
	});


});

