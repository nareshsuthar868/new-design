// text/x-generic custom.js ( ASCII C++ program text )

$(document).ready(function(){
    
	/*--- Trending product tabbing ---*/
	$('ul.trending li').click(function(){
	   	var tab_id = $(this).attr('data-tab');
		$('ul.trending li').removeClass('current');
		$(this).addClass('current');
		$(".trending-content").removeClass('current');
		$("#"+tab_id).addClass('current');
		$('.trending-product-slider').slick('refresh');
	});

	/*--- Trending product slider ---*/
	$('.trending-product-slider').slick({
		dots: true,
		arrows: true,
		slidesToShow: 4,
		infinite: false,
		prevArrow: '<span class="prev-arrow"><i class="icn-left-arrow-red"></i></span>',
		nextArrow: '<span class="next-arrow"><i class="icn-right-arrow-red"></i></span>',
		responsive: [
		    {
		      breakpoint: 1025,
		      settings: {		       		        
		        slidesToShow: 3
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {		        
		        slidesToShow: 2,
		        dots: true
		      }
		    }
		]
	});

	/*--- thumbnail slider ---*/
	$('#banner-slider').slick({
		dots: false,
		arrows: false,
		slidesToShow: 1,
		infinite: true,		
		prevArrow: '<span class="prev-arrow"><i class="icn-left-arrow-big-red"></i></span>',
		nextArrow: '<span class="next-arrow"><i class="icn-right-arrow-big-red"></i></span>',
		asNavFor: '#banner-tags-listing',
		responsive: [		    
		    {
		      breakpoint: 1025,
		      settings: {		        
		        dots: true,
		      }
		    }
		]
	});
	$('#banner-tags-listing').slick({
		slidesToShow: 4,
		slidesToScroll: 4,
		arrows: false,
		asNavFor: '#banner-slider',	
		focusOnSelect: true,
		variableWidth: true,
		swipe: false,
	});

	$("#header-search").on('click',function(){
    	$('header .search-bar').addClass('show');
    });

    $("#search-close").on('click',function(){
    	$('header .search-bar').removeClass('show');
    });
	/*--- sticky header ---*/
    var num = 35;

	$(window).bind('scroll', function () {
	    if ($(window).scrollTop() > num) {
	        $('header').addClass('sticky');
	    } else {
	        $('header').removeClass('sticky');
	    }
	});

    $(".menu-mobile").on('click',function(){
    	$('.mobile-navigation').addClass('active');
    	$(this).addClass('active');
    	$("body").addClass('scroll-hide');    	
    });
    $(".mobile-navigation .close").on('click',function(){
    	$('.mobile-navigation').removeClass('active');
    	$(this).removeClass('active');
    	$("body").removeClass('scroll-hide'); 
    });    
	
	/*--- header sub menu open on click ---*/
	$(".sub-menu").siblings("a").after("<span class='caret icn-arrow-bottom'></span>");
	$('.caret').click(function(ev) {
		if (!($(this).parent().hasClass("open"))) {
			$('.sub-menu').not($(this).parents('.sub-menu')).slideUp();
			$('.mobile-navigation-part > li').not($(this).parents('li')).removeClass('open');
			$(this).next('.sub-menu').slideToggle();
			$(this).parent().addClass("open");
			ev.stopPropagation();
		} else {
			$(this).parent().removeClass("open");
			$(this).siblings('.sub-menu').slideUp();
		}
	});

	/*--- Accordion ---*/
	$('.accordion-tab.current .accordion-content').slideDown();
	$('.accordion-tab.current').addClass('active');
	$(".accordion-title").click(function() {
		$(".accordion-content").not($(this).siblings()).slideUp(); 
		$(".accordion-tab").not($(this).parent()).removeClass('active');
		$(document.body).trigger("sticky_kit:recalc");

		$(this).siblings().slideToggle();
		$(this).parent('.accordion-tab').toggleClass('active'); 
	});

	/*--- Header location Expand ---*/
	$(".header-location .location").click(function(){
		$(".location-part").slideDown();
		$("body").addClass('overlay');
	});
	$(".location-part .close").click(function(){
		$(".location-part").slideUp();
		$("body").removeClass('overlay');
	});

	/*--- Innerpage banner content Expand ---*/
	$("#clickToggle").click(function(){
		$(this).toggleClass('open');
		$(".content-expand").slideToggle();
	});

	/*--- more items Expand ---*/
	$(".items-count").click(function(){
		$(".more-items-expand").slideToggle().toggleClass('expanded');
		$(this).toggleClass('expanded');
	});


	/*--- mobile category slider ---*/
	$('.mobile-category-slider').slick({
		dots: false,
		arrows: true,
		slidesToShow: 3,
		infinite: false,
		prevArrow: '<span class="prev-arrow"><i class="icn-arrow-left"></i></span>',
		nextArrow: '<span class="next-arrow"><i class="icn-arrow-right"></i></span>'
	});

	/*--- filter/sort tabbing ---*/
	$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	});

	$(".mobile-filterbar-wrapper > span").click(function(){
		$(".mobile-filterbox").addClass('open');
		$("body").addClass('scroll-hide');
	});
	$(".mobile-filterbox .close").click(function(){
		$(".mobile-filterbox").removeClass('open');
		$("body").removeClass('scroll-hide');
	});

	/*product details page*/

	/*--- thumbnail slider ---*/
	$('.product-slider .slider-for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		adaptiveHeight: true,
		fade: true,
		asNavFor: '.slider-nav',
		responsive: [		    
		    {
		      breakpoint: 768,
		      settings: {		        
		        dots: true,
		      }
		    }
		]
	});
	$('.product-slider .slider-nav').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: false,
		asNavFor: '.product-slider .slider-for',	
		focusOnSelect: true,
		variableWidth: true,
		vertical: true,
		verticalSwiping: true
	});

	/*--- product duration Expand ---*/
	$('body').click(function(e) {
        if($('.duration-dropdown').hasClass('open')) {
            if (e.target != $('.duration-box')[0] || e.target  == $('.duration-dropdown')[0]){
             	$(".duration-dropdown").slideToggle().removeClass('open');
            }
        }
    });
	$(".duration-box").click(function(){
		$(".duration-dropdown").slideToggle().toggleClass('open');
	});


	/*--- related product slider ---*/
	$('.related-product-slider').slick({
		dots: false,
		arrows: true,
		slidesToShow: 4,
		infinite: false,
		prevArrow: '<span class="prev-arrow"><i class="icn-left-arrow-big-red"></i></span>',
		nextArrow: '<span class="next-arrow"><i class="icn-right-arrow-big-red"></i></span>',
		responsive: [
		    {
		      breakpoint: 1025,
		      settings: {		       		        
		        slidesToShow: 3
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {		        
		        slidesToShow: 2,
		        dots: true
		      }
		    }
		]
	});

	/*--- testimonial slider ---*/
	$('.testimonial-slider').slick({
		dots: false,
		arrows: true,
		slidesToShow: 1,
		infinite: true,
		centerMode: true,
		centerPadding: '330px',
		prevArrow: '<span class="prev-arrow"><i class="icn-left-arrow-big-red"></i></span>',
		nextArrow: '<span class="next-arrow"><i class="icn-right-arrow-big-red"></i></span>',
		responsive: [
		    {
		      breakpoint: 1231,
		      settings: {		       
		        centerMode: true,
		        centerPadding: '100px',
		        slidesToShow: 1
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {		        
		        centerMode: false,
		        centerPadding: '0',
		        slidesToShow: 1,
		        arrows: false
		      }
		    },
		   
		]
	});
	
	/*--- custom scrollbar ---*/
	$(window).on("load",function(){
        $(".content").mCustomScrollbar({theme:"minimal-dark"});
    });

    /*--- home testimonial slider ---*/
	$('.home-testimonial-slider').slick({
		dots: false,
		arrows: true,
		slidesToShow: 3,
		infinite: true,
		centerMode: true,
		centerPadding: '0px',
		prevArrow: '<span class="prev-arrow"><i class="icn-left-arrow-red"></i></span>',
		nextArrow: '<span class="next-arrow"><i class="icn-right-arrow-red"></i></span>',
		responsive: [
		    {
		      breakpoint: 1231,
		      settings: {		       
		        centerMode: true,		        
		        slidesToShow: 3
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {		        
		        // centerMode: false,
		        centerPadding: '40px',
		        slidesToShow: 1,
		        arrows: false
		      }
		    },
		   
		]
	});

	// $('#add-product-link').magnificPopup({
	// 	type: 'inline',
	// 	preloader: false,
	// 	focus: '#name',
	// 	callbacks: {
	// 		beforeOpen: function() {
	// 			$('.product-popup-slider').slick('refresh');
	// 			if($(window).width() < 700) {
	// 				this.st.focus = false;
	// 			} else {
	// 				this.st.focus = '#name';
	// 			}
	// 		}
	// 	},
	// });


	$(".header-icons-listing > li > a .icn-cart, .header-icons-listing > li > a .icn-cart-white").click(function(){
		$(".minicart-section").addClass('open');
		$("body").addClass('scroll-hide');
	});
	$(".minicart-section-wrapper .close").click(function(){
		$(".minicart-section").removeClass('open');
		$("body").removeClass('scroll-hide');
	});



	$('.FlowupLabels').FlowupLabels({
		/*
		 * These are all the default values
		 * You may exclude any/all of these options
		 * if you won't be changing them
		 */
		
		// Handles the possibility of having input boxes prefilled on page load
		feature_onInitLoad: true, 
		
		// Class when focusing an input
		class_focused: 		'focused',
		// Class when an input has text entered
		class_populated: 	'populated'	
	});

	/*--- items included slider (for mobile) ---*/
    $('.items-included-slider').slick({
		dots: true,
		arrows: false,
		slidesToShow: 1,
		infinite: true,		
	});

	/*--- month range slider (for mobile) ---*/
	var custom_values = ['4 Mo','8 Mo', '12 Mo','16 Mo', '20 Mo', '24 Mo'];
	$(".js-range-slider").ionRangeSlider({
		type: "double",
        grid: true,
        skin: "round",
        step: 4,
        values: custom_values
    });    



	$('.product-popup-slider').slick({
		dots: true,
		arrows: true,
		slidesToShow: 1,
		infinite: true,		
		prevArrow: '<span class="prev-arrow"><i class="icn-left-arrow-red"></i></span>',
		nextArrow: '<span class="next-arrow"><i class="icn-right-arrow-red"></i></span>',
		responsive: [
		    {
		      breakpoint: 1231,
		      settings: {		       
		        
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {		        
		        
		      }
		    },
		   
		]
	});

	$(".delivery-info-listing li span .icn").click(function(){
		$(".help-box").toggleClass('show');

	});
	
	$(".account-sidebar").stick_in_parent({
		offset_top: 95,
		recalc_every: 1
	});

	$(document.body).trigger("sticky_kit:recalc");

	$('.product-options-wrapper .product-img').slick({
		dots: true,
		arrows: true,
		slidesToShow: 1,
		infinite: true,		
		prevArrow: '<span class="prev-arrow"><i class="icn-left-arrow-chevron"></i></span>',
		nextArrow: '<span class="next-arrow"><i class="icn-right-arrow-chevron"></i></span>',
		responsive: [
		    {
		      breakpoint: 1231,
		      settings: {		       
		        
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {		        
		        
		      }
		    },
		   
		]
	});

	$('body').on('click', 'footer .footer-widget h4', function(e) {
	    $('.footer-widget').find('.footer-links-wrapper').stop().slideUp();	    
	    $('.footer-widget').find('h4').stop().removeClass('open');
	    $(this).closest('.footer-widget').find('.footer-links-wrapper').stop().slideToggle().toggleClass('open');
	    $(this).closest('.footer-widget').find('h4').stop().toggleClass('open');
	});
	$('.footer-expand').click(function(){
		$(".footer-main").slideToggle();		
		$(".footer-copyright").slideToggle();		
	});	

	// $('#add-product-link').magnificPopup({
	// 	type: 'inline',
	// 	preloader: false,
	// 	focus: '#name',
	// 	callbacks: {
	// 		beforeOpen: function() {
	// 			$('.product-popup-slider').slick('refresh');
	// 			if($(window).width() < 700) {
	// 				this.st.focus = false;
	// 			} else {
	// 				this.st.focus = '#name';
	// 			}
	// 		}
	// 	},
	// });

	$('#manage_order').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name'		
	});
	$('#change_payment_mode').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#name'		
	});


	$('.sort-by > span').click(function(){
		$(".sort-dropdown").slideToggle().toggleClass('open');	
	});
	
	$('body').click(function(e) {
        if($('.sort-dropdown').hasClass('open')) {
            if (e.target != $('.sort-by > span')[0] || e.target  == $('.sort-dropdown')[0]){
             	$(".sort-dropdown").slideToggle().removeClass('open');
            }
        }
    });

    $('.service_request_type').click(function(){
		$(".service-type-listing").slideToggle().toggleClass('open');	
	});
	
	$('body').click(function(e) {
        if($('.service-type-listing').hasClass('open')) {
            if (e.target != $('.service_request_type')[0] || e.target  == $('.service-type-listing')[0]){
             	$(".service-type-listing").slideToggle().removeClass('open');
            }
        }
    });   
});

$(document).ready(function(e) {
	$('#add-product-link').click(function(){
		$(".product-options-wrapper").addClass('open');
		$("body").addClass('scroll-hide');
		event.stopPropagation('.product-options-wrapper');
	});
	$(".product-options-wrapper .close").click(function(){
		$(".product-options-wrapper").removeClass('open');
		$("body").removeClass('scroll-hide');
	});
	$(document).on('click', function (event) {
	  if (!$(event.target).closest('.product-options-wrapper').length) {
	    $(".product-options-wrapper").removeClass("open");
	    $("body").removeClass('scroll-hide');
	  }
	});
});