<?php
$this->load->view('template/header.php');
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<section class="hedertopbanner">
    <figure>
        <img src="<?php echo base_url()?>assets/images/Cityfurnish_Bannner.jpg" id="bannner_img" alt="Product-banner">
        <figcaption class="captionheadertitle">
            <h1><span style="font-weight: bold;">Rent Furniture &amp; Appliances</span></h1>
            <!--<a href="https://cityfurnish.com/shop" class="btn btn-default">Explore Offerings</a> -->
        </figcaption>
    </figure>
    <div class="header_top_part visible-xs">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <p><span><img src="https://d1eohs8f9n2nha.cloudfront.net/images/top-percent-icn.svg" alt="percenticn"></span> 
                    <span>Get Your First Month Free. Hurry! Limited Period Offer. <a style="color:white;" href="https://cityfurnish.com/pages/offers"> <u> KNOW MORE..</u></a></span> </p>
                </div>
            </div>
        </div>
    </div>
</section>
            <section class="productcategoryrow" id="productdown">
                <section class="container">
<section class="row">
                        <!-- <h2>Popular Categories</h2> -->
                                                    <aside class="productcat">
                                <a href="https://cityfurnish.com/shopby/rental-packages">
                                <div class="productcat-wrapper">
                                <figure><img src="https://d1eohs8f9n2nha.cloudfront.net/images/category/packages6.svg" alt="Packages"></figure>
                                <article><h3>Packages</h3><!-- <p>Indulge into the world of breathtaking products by exploring our carefully curated furniture and app...</p> --><!-- <a href="https://cityfurnish.com/shopby/rental-packages" class="btn btn-primary hidden-xs">RENT NOW</a> -->
                                </article></div>
                                </a>
                                </aside>                                
                                                        <aside class="productcat">
                                <a href="https://cityfurnish.com/shopby/furniture-rental">
                                <div class="productcat-wrapper">
                                <figure><img src="https://d1eohs8f9n2nha.cloudfront.net/images/category/home-furniture2.svg" alt="Home Furniture"></figure>
                                <article><h3>Home Furniture</h3><!-- <p>Why buy when you can rent furniture? Revamp your home by choosing from our vast variety of furnit...</p> --><!-- <a href="https://cityfurnish.com/shopby/furniture-rental" class="btn btn-primary hidden-xs">RENT NOW</a> -->
                                </article></div>
                                </a>
                                </aside>                                
                                                        <aside class="productcat">
                                <a href="https://cityfurnish.com/shopby/office-furniture-rental">
                                <div class="productcat-wrapper">
                                <figure><img src="https://d1eohs8f9n2nha.cloudfront.net/images/category/office-furniture2.svg" alt="Office Furniture"></figure>
                                <article><h3>Office Furniture</h3><!-- <p>Are you planning to rent office furniture? Check our office furniture rental packages here! Our pack...</p> --><!-- <a href="https://cityfurnish.com/shopby/office-furniture-rental" class="btn btn-primary hidden-xs">RENT NOW</a> -->
                                </article></div>
                                </a>
                                </aside>                                
                                                        <aside class="productcat">
                                <a href="https://cityfurnish.com/shopby/rent-electronics">
                                <div class="productcat-wrapper">
                                <figure><img src="https://d1eohs8f9n2nha.cloudfront.net/images/category/electronics-new-icn1.svg" alt="Electronics"></figure>
                                <article><h3>Electronics</h3><!-- <p>Branded home appliances for all needs - Our collection of home appliances is made considering req...</p> --><!-- <a href="https://cityfurnish.com/shopby/rent-electronics" class="btn btn-primary hidden-xs">RENT NOW</a> -->
                                </article></div>
                                </a>
                                </aside>                                
                                                        <aside class="productcat">
                                <a href="https://cityfurnish.com/shopby/fitness-equipments-on-rent">
                                <div class="productcat-wrapper">
                                <figure><img src="https://d1eohs8f9n2nha.cloudfront.net/images/category/fitness2.svg" alt="Fitness"></figure>
                                <article><h3>Fitness</h3><!-- <p>Set up your home gym by choosing from vast variety of our fitness products. Enjoy an intense cardiov...</p> --><!-- <a href="https://cityfurnish.com/shopby/fitness-equipments-on-rent" class="btn btn-primary hidden-xs">RENT NOW</a> -->
                                </article></div>
                                </a>
                                </aside>                                
                                                    
                    </section>
                </section>
            </section>
  <!--<section class="projectcomprow">-->
  <!--<div class="container">-->
  <!--  	<div class="row">-->
  <!--      	<h3>Previous completed projects</h3>-->
  <!--          <div class="col-xs-6 col-sm-6 col-md-6 margin-bottom-20">-->
  <!--          	<img src="images/bulk-prev-project-1.jpg" alt="Project" />-->
  <!--          </div>-->
  <!--          <div class="col-xs-6 col-sm-6 col-md-6 padding-left-5 padding-right-5">-->
            	
  <!--              	<div class="bulkhalfwcol"><img src="images/bulk-prev-project-2.jpg" alt="Project" /></div>-->
  <!--                  <div class="bulkhalfwcol"><img src="images/bulk-prev-project-3.jpg" alt="Project" /></div>-->
  <!--                  <div class="bulkhalfwcol"><img src="images/bulk-prev-project-4.jpg" alt="Project" /></div>-->
  <!--                  <div class="bulkhalfwcol"><img src="images/bulk-prev-project-5.jpg" alt="Project" /></div>-->
              
  <!--          </div>-->
  <!--      </div>-->
  <!--  </div>-->
  <!--</section>-->
  <section class="bulkorderformrow">
  	<div class="container">
    	<div class="row">
        	<div class="col-sm-9 col-md-7">
            	<h3>Get in Touch</h3>
                <p>Mention your requirements in brief and we will get back to you promptly</p>
                <div class="bulkfrmwhite">
                	<form id="bulk_order_form" name="bulk_order_form">
                    	<div class="bulkinput">
                           <div class="textboxcol"> 
                            <label>Name</label>
                        	<input type="text" id="User_Name" name="User_Name" class="form-control" />
                           </div>
                           <div class="textboxcol">
                            <label>Email Address</label>
                        	<input type="email" id="Email"  name="Email" class="form-control" />
                         </div>
                         <div class="textboxcol">
                            <label>Phone</label>
                        	<input type="text" id="Phone"  name="Phone" class="form-control" />
                        </div>
                        <div class="textboxcol">
                            <label>City</label>
                        	<input type="text" id="City"  name="City" class="form-control" />
                        </div>
                        </div>
                        <div class="bulkinput">
                         
                            <label>Message</label>
							<textarea  id="Message"  name="Message" class="form-control"></textarea>
                            <input type="submit" onclick="BulkOrder()" id="bulk_order_button" class="btn-check" value="Submit" />
                            <!-- <button type="button" onclick="BulkOrder()" class="btn-check">Submit</button> -->
                        </div>
                    </form>
                </div>
               <h3 id="response_msg" style="display: none;">hello</h3>
            </div>
        </div>
    </div>
  </section>
  <!--<section class="quotionsrow">-->
  <!--	<div class="container">-->
  <!--  	<div class="row">-->
  <!--      	<div class="col-xs-6 col-sm-6 col-md-6">-->
  <!--          	<div class="borderboxcol">-->
  <!--                <div class="thumbpic">	-->
  <!--                  <img src="images/help-quotion-pic.jpg" alt="Quotation" />-->
  <!--                </div>-->
  <!--                  <h6>Help With Quotation</h6>-->
  <!--                  <p>You will get a elaborated quotation covering all aspects of the offerings</p>-->
  <!--              </div>-->
  <!--          </div>-->
  <!--          <div class="col-xs-6 col-sm-6 col-md-6">-->
  <!--            <div class="borderboxcol">-->
  <!--             <div class="thumbpic">-->
  <!--              <img src="images/fast-delivery-pic.jpg"  alt="Fast Delivery"/>-->
  <!--              <strong>Call : +918010845000</strong>-->
  <!--             </div>  -->
  <!--          	<h6>Custom Made Products</h6>-->
  <!--              <p>We provide custom made furniture as per the demand of your project</p>-->
  <!--            </div>  -->
  <!--          </div>-->
  <!--      </div>-->
  <!--  </div>-->
  <!--</section>-->
</div>
<?php 
$this->load->view('template/footer.php');
?>