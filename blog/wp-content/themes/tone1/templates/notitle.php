<?php
/*
Template Name: No Title
*/

get_header(); ?>

<section id="single" role="main">

    <div class="boxed white">

        <?php while (have_posts()) : the_post(); ?>

            <?php if ( class_exists( 'RW_Meta_Box' ) ) {
                $sidebar = rwmb_meta( 'tonetheme_layout' );
                $drop_cap = rwmb_meta( 'tonetheme_dropcap' );
            } else {
                $sidebar = '';
                $drop_cap = '';
            }
            ?>

            <article <?php post_class() ?> id="post-<?php the_ID(); ?>">

                <div class="entry-wrapper">

                    <?php get_template_part( 'inc/featured' ); ?>

                    <div class="boxed narrow">

                        <?php dynamic_sidebar('before-post-horizontal'); ?>


                        <div class="entry-content<?php if ($drop_cap) { ?> drop-cap<?php } ?>">

                            <?php the_content(); ?>

                            <?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:&nbsp;&nbsp;', 'tone'), 'link_before' =>'&nbsp;&nbsp;', 'after' => '</p></nav>' )); ?>

                        </div>


                        <?php dynamic_sidebar('after-post-horizontal'); ?>

                    </div>

                </div><!--enrty-wrapper end-->

            </article>

        <?php endwhile; // End the loop ?>

    </div>

</section>


<?php get_footer(); ?>