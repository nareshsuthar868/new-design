<article id="post-0" class="post none">

    <div class="row entry-wrapper">

        <div class="large-7 large-centered columns">

            <header class="entry-header">

                <h1 class="entry-title"><?php esc_html_e('Nothing Found', 'tone' ); ?></h1>

            </header>

            <div class="entry-content no-caps">
                <p><?php esc_html_e('Apologies, but no results were found. Perhaps searching will help find a related post.', 'tone' ); ?></p>
                <div class="divider-medium"></div>
                <?php get_search_form(); ?>

            </div><!--end entry content-->

        </div><!--end large col-->

    </div><!--end row-->

</article>