<article <?php post_class('post grid-item'); ?>>

    <div class="media-holder overlay">

        <?php
        if ( has_post_thumbnail() ) { ?>

            <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">

                <?php the_post_thumbnail('tonetheme_medium_crop'); ?>

            </a>

        <?php } else { ?>

            <img src="<?php echo esc_url(get_template_directory_uri()) ?>/img/blank.png" alt="<?php the_title(); ?>" />

        <?php } ?>

        <?php get_template_part('inc/badge'); ?>

    </div>


    <div class="text-holder">

        <div class="inner">

            <?php  if ( has_post_format( 'link' ) ) { ?>

                <h2 class="entry-title">
                    <?php tonetheme_link_title(); ?>
                </h2>

            <?php } elseif ( has_post_format( 'aside' ) ) { } else { ?>

            <h2 class="entry-title">

                    <a href="<?php the_permalink(); ?>">
                        <?php the_title(); ?>
                    </a>

            </h2>

            <?php } ?>

            <div class="excerpt-text">

                <?php if ( has_post_format( 'aside' ) ) { ?>

                    <a href="<?php the_permalink(); ?>">

                        <?php the_excerpt(); ?>

                    </a>

                <?php } else { ?>

                    <?php the_excerpt(); ?>

                    <?php get_template_part( 'inc/meta' ); ?>

                <?php } ?>

            </div>

        </div>

    </div>


</article>