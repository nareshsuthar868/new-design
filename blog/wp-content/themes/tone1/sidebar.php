<aside id="sidebar-side" role="complementary" class="dark">

    <?php if ( is_page() ) {


        if ( function_exists('is_active_sidebar') && is_active_sidebar('page-sidebar') ) {
            dynamic_sidebar( 'page-sidebar' );

        } elseif ( function_exists('is_active_sidebar') && is_active_sidebar('primary-sidebar') ) {

            dynamic_sidebar( 'primary-sidebar' );

        } elseif ( current_user_can('administrator') ) { ?>

            <div class="notice"><p><?php esc_html_e('Place any widget here via', 'tone'); ?> <a href="<?php esc_url(home_url('/wp-admin/widgets.php'))?>"><?php esc_html_e('Appearance -> Widget', 'tone'); ?></a> <?php esc_html_e('section', 'tone'); ?>.</p></div>

        <?php }

    } elseif ( is_single() ) {


        if ( function_exists('is_active_sidebar') && is_active_sidebar('post-sidebar') ) {
            dynamic_sidebar( 'post-sidebar' );

        } elseif ( function_exists('is_active_sidebar') && is_active_sidebar('primary-sidebar') ) {

            dynamic_sidebar( 'primary-sidebar' );

        } elseif ( current_user_can('administrator') ) { ?>

            <div class="notice"><p><?php esc_html_e('Place any widget here via', 'tone'); ?> <a href="<?php esc_url(home_url('/wp-admin/widgets.php'))?>"><?php esc_html_e('Appearance -> Widget', 'tone'); ?></a> <?php esc_html_e('section', 'tone'); ?>.</p></div>

        <?php }

    } else {


        if ( function_exists('is_active_sidebar') && is_active_sidebar('primary-sidebar') ) {
        dynamic_sidebar( 'primary-sidebar' );

        } elseif ( current_user_can('administrator') ) { ?>

            <div class="notice"><p><?php esc_html_e('Place any widget here via', 'tone'); ?> <a href="<?php esc_url(home_url('/wp-admin/widgets.php'))?>"><?php esc_html_e('Appearance -> Widget', 'tone'); ?></a> <?php esc_html_e('section', 'tone'); ?>.</p></div>

        <?php }

     } ?>

</aside>