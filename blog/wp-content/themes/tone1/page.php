<?php get_header(); ?>

<section id="single" role="main">

    <div class="boxed white">

        <?php get_template_part('content'); ?>

    </div>

</section>


<?php get_footer(); ?>