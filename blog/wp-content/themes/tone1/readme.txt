=== Description ===

Tone is a responsive blogging WordPress theme.

Please read this guidance, this will help you to install and configure the theme easily.

Also you may find the detailed documentation with screenshots in our [Knowledge Base](http://themeous.net/knowledge-base/ "Themeous Knowledge Base")

===Getting Started===

==Important Note==

The first thing you'll need to do after activating the theme is this: configure Custom Menus and Widgets to make your theme work properly.

=If you already have lots of content=

In order to generate new thumbnails install Regenerate Thumbnails or Force Regenerate Thumbnails and go to Tools > Rebuild Thumbnails.

Recommended minimum image size is 800 px in width and 800 px in height. If your images are smaller they may not crop correctly. In this case you should install Thumbnail Upscale plugin and Force Regenerate Thumbnails.

==1. Uploading the Theme & Installing Demo Content==

Method 1: To upload the theme you need to extract the downloaded zip file. Once extracted you'll see a file named tone.zip. Go to Appearance > Themes > Install Theme and click Upload. Select the "tone.zip" file on your hard disk.

Please note! Use installable tone.zip file only.

Method 2: If the method above fails you will need to upload via FTP. I recommend you to install FileZilla and upload just the "tone" folder into your-website.com/wp-content/themes/ folder on your server.

Please, make sure you've installed and activated Meta Box plugin.

==2. Theme Customization==

Tone Theme supports WordPress Theme Customizer for color variations, fonts, logo, etc.

To access the Customizer, just click on the Customize link. For details go to Appearance & Theme Options

==3. Adding Menus==

To create a custom menu, go to Appearance > Menus in your blog's dashboard. There are two menu locations: Top Navigation and Footer Navigation. To learn more about creating menus please refer to the WordPress Codex.

You may drag a menu item to the right to create expandable menu.

Also you can add any menu in Off-Canvas panel using Custom Menu widget.

If you wish to display menu for mobile devices only, create a custom menu with the name Mobile and select this menu in Custom Menu widget.

You can specify custom links to your profiles in social networks and RSS-feed. The corresponding icons will appear in the site top or footer menu.

==4. Sidebars & Widgets==

To place some widgets in widget areas, go to Appearance > Widgets.

Primary Sidebar Widget Area is used for Off-Canvas Sidebar.

Post Widget Area is used to show any content for single posts.

Horizontal Header, Homepage Horizontal Header, Before Post, After Post Widget Areas can be used for ads, etc.

===Appearance & Theme Options===

To access the theme options go to Appearance > Customize. Here you will see a tabbed panel with various options.

==5. Adding Your Logo==

Go to Appearance > Customize. You'll see the General tab. Click upload and navigate to the file on your hard disk.

You can limit the logo width, if you wish to download a larger logo for Retina screens.

You can also download the favicon for the site in the format .png.

For newer WordPress versions it is recommended to use Site Identity section in customizer for favicon upload.

==6. Colors==

Using a color picker, you can control main colors of your site. Change the look of a website just in a few clicks.

==7. Typography==

You can choose the font you want from Google Webfonts and easily add it to the theme.

Just enter the name of any Google Font, wait for a few seconds - and you'll see the result in a live preview.

===Posts & Pages===

==8. Social Sharing==

For social sharing you may install any plugin you like, for example Social Likes.

Make sure you've set Meta Descriptions for your posts for proper excerpts when share on Facebook (if you use Yoast Seo Plugin, put %%excerpt%% in Titles & Metas > Post Types section).

==9. Creating posts==

Tone has a some additional options for creating posts. You'll see them when you'll try to add or edit a post.

=9.1. Standard & Image=

It's a basic post without featured image. Post Format is set to "Standard" by default.

If you wish to display featured image at the top of the post, you can select "Image" post format when you edit a post, also you can enable "Image" post format as default in Settings > Writing section.

You can enable beautiful fullwidth images for any post using post settings. Also you can add Drop Cap or disable meta info.

Also it is possible to add Custom Excerpt, it will be displayed at the top of the post with larger font as intro text.

If you do not see this field, you need to enable it on the Editor Screen.

=9.2. Galleries=

You can add beautiful tiled galleries for any post. The theme utilizes native WordPress galleries for your convenience, but their display is significantly improved.

Just insert a gallery in your post text. You can change the tiles size by selecting the number of columns (9 sizes are available).

=9.3. Video & Audio=

Tone Theme supports all embeds that WordPress supports, for example Vimeo or YouTube. The responsive video can be displayed before the post content or inside your text.

You just need to copy the video or audio url and insert it in your post.

If you copy a link to the Embed field and select Video or Audio post format your video will be displayed at the top of the post in full width.

=9.4. Link Post=

Using the "Link" post format, you can add a link to another site that will be opened, when you click on the post title.

Don't forget to select "Link" post format.

===Miscellaneous===

==10. Attribution==

IMAGES

lightgallery images / Apache 2.0

ANIMATION

Animate.css
http://daneden.me/animate / MIT

JAVASCRIPT

CollagePlus for jQuery
https://github.com/ed-lea/jquery-collagePlus /  MIT or GPL 2

lightgallery
http://sachinchoolur.github.io/lightGallery / Apache 2.0

FRAMEWORKS

ZURB's Foundation
http://foundation.zurb.com / MIT

PHP

TGM-Plugin-Activation
https://github.com/thomasgriffin/TGM-Plugin-Activation / GPL 2

Meta Box
https://metabox.io / GPL 2

FONTS

Entypo, Font Awesome, Typicons / SIL
via Fontello
http://fontello.com

Lora
https://www.google.com/fonts/specimen/Lora / SIL