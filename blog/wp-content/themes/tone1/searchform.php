<form class="custom-search" method="get" id="searchform" action="<?php echo esc_url(home_url('/')); ?>">
    <div class="row collapse">
        <div class="small-8 large-8 medium-7 columns">

            <input type="search" class="search-field" placeholder="<?php esc_html_e('Enter keyword', 'tone'); ?>" value="<?php echo get_search_query() ?>" name="s" title="<?php esc_html_e('Search', 'tone'); ?>" />

        </div>
        <div class="small-4 large-4 medium-5 columns">
            <button type="submit" id="search-button" class="postfix button search-submit"><?php esc_html_e('Search', 'tone'); ?></button>
        </div>
    </div>
</form>