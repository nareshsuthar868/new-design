<?php
add_filter( 'rwmb_meta_boxes', 'tonetheme_register_meta_boxes' );

function tonetheme_register_meta_boxes( $meta_boxes )
{
    $prefix = 'tonetheme_';


    $meta_boxes[] = array(
        'title'      => esc_html__('Post Settings', 'tone'),
        'post_types' => 'post',

        'fields'     => array(
            array(
                'name' => esc_html__( 'Add Drop Cap', 'tone' ),
                'desc' => esc_html__( 'Add Drop Cap to the first paragraph', 'tone' ),
                'id'   => "{$prefix}dropcap",
                'type' => 'checkbox',
                'std'  => 0,
            ),
            array(
                'name' => esc_html__( 'Disable Post Info', 'tone' ),
                'desc' => esc_html__( 'You may disable post date, category and other meta info by checking this option', 'tone' ),
                'id'   => "{$prefix}postinfo",
                'type' => 'checkbox',
                'std'  => 0,
            ),
            array(
                'name' => esc_html__( 'Wide Images', 'tone' ),
                'desc' => esc_html__( 'Enable beautiful fullwidth images for this post', 'tone' ),
                'id'   => "{$prefix}wideimg",
                'type' => 'checkbox',
                'std'  => 0,
            ),
            array(
                'name' => esc_html__( 'Embed Url', 'tone' ),
                'desc' => esc_html__( 'Will be embed at the top of the post for Video and Audio post format.', 'tone' ),
                'id'    => "{$prefix}embedurl",
                'type'  => 'url',
            ),

            array(
                'name' => esc_html__( 'Title Url', 'tone' ),
                'desc' => esc_html__( 'This link will be used in post title for Link post format.', 'tone' ),
                'id'    => "{$prefix}titleurl",
                'type'  => 'url',
            ),

        )
    );

    return $meta_boxes;
}