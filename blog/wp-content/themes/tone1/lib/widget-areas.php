<?php // Create widget areas

add_action( 'widgets_init', 'tonetheme_widgets_init' );

function tonetheme_widgets_init() {

    register_sidebar(array('name'=> esc_html__('Primary Sidebar', 'tone'),
        'id' => 'primary-sidebar',
        'description' => esc_html__('Sidebar for recent posts homepage and categories.', 'tone'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
    ));

    register_sidebar(array('name'=> esc_html__('Post Sidebar', 'tone'),
    'id' => 'post-sidebar',
    'description' => esc_html__('Sidebar for single posts.', 'tone'),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget' => '</section>',
    'before_title' => '<header class="widget-title"><h6>',
    'after_title' => '</h6></header>'
    ));

    register_sidebar(array('name'=> esc_html__('Page Sidebar', 'tone'),
    'id' => 'page-sidebar',
    'description' => esc_html__('Sidebar for static pages. If is empty, the Post Sidebar widget area will be used.', 'tone'),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget' => '</section>',
    'before_title' => '<header class="widget-title"><h6>',
    'after_title' => '</h6></header>'
    ));

    register_sidebar(array('name'=> esc_html__('Horizontal Header', 'tone'),
    'id' => 'header',
    'description' => esc_html__('Appears on the top of all pages. You can place ads here, textwidget or any other widget with horizontal layout.', 'tone'),
    'before_widget' => '<div id="%1$s" class="widget %2$s horizontal">',
        'after_widget' => '</div>',
    'before_title' => '<header class="widget-title"><h6>',
            'after_title' => '</h6></header>'
    ));

    register_sidebar(array('name'=> esc_html__('Homepage Horizontal Header', 'tone'),
        'id' => 'homepage-header',
        'description' => esc_html__('Appears on the top of homepage. You can place ads here, textwidget or any other widget with horizontal layout.', 'tone'),
        'before_widget' => '<div id="%1$s" class="widget %2$s horizontal">',
        'after_widget' => '</div>',
        'before_title' => '<header class="widget-title"><h6>',
        'after_title' => '</h6></header>'
    ));

    register_sidebar(array('name'=> esc_html__('Before Post Content Horizontal', 'tone'),
    'id' => 'before-post-horizontal',
    'description' => esc_html__('Appears before single post content. You can place ads here, textwidget or any other widget with horizontal layout.', 'tone'),
    'before_widget' => '<section id="%1$s" class="widget %2$s horizontal before-post">',
        'after_widget' => '</section>',
    'before_title' => '<header class="widget-title"><h6>',
            'after_title' => '</h6></header>'
    ));

    register_sidebar(array('name'=> esc_html__('After Post Content Horizontal', 'tone'),
    'id' => 'after-post-horizontal',
    'description' => esc_html__('Appears after single post content. You can place ads here, textwidget or any other widget with horizontal layout.', 'tone'),
    'before_widget' => '<section id="%1$s" class="widget %2$s horizontal after-post">',
        'after_widget' => '</section>',
    'before_title' => '<header class="widget-title"><h6>',
            'after_title' => '</h6></header>'
    ));
}