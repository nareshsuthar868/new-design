<?php get_header(); ?>
<div class="boxed white page_section_offset">
        <section class="innerbanner">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h1>Blog</h1>
          <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Blog</li>
          </ul>
        </div>
      </div>
    </div>
    <div class="boxed white">

        <section id="content" class="half-grid" role="main">

            <div class="row collapse">

                <div class="large-12 columns">

                    <?php if ( have_posts() ) : ?>

                        <div id="grid-content" class="grid">

                            <?php while ( have_posts() ) : the_post(); ?>

                                <?php get_template_part( 'loop', 'halfwidth'); ?>

                            <?php endwhile; ?>

                        </div>
                        

                    <?php else : ?>

                        <?php get_template_part( 'content', 'none' ); ?>

                    <?php endif; ?>

                </div>

            </div>

        </section>

    </div>
</div>
<?php get_footer(); ?>