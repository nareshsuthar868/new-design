<?php function tonetheme_comments($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class(); ?>>

		<article id="comment-<?php comment_ID(); ?>">

            <div class="comment-full">

                <div class="comment-avatar">
                    <div class="comment-avatar-inner">
                        <?php echo get_avatar($comment,$size='56'); ?>
                    </div>
                </div>

                <section class="comment-text">

                    <header class="comment-author">

                    <div class="author-meta">
                        <cite class="fn"><?php echo get_comment_author_link(); ?></cite>
                        <time datetime="<?php echo esc_attr(comment_date('c')) ?>" class="comment-date"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ) ?>"><?php echo esc_html(get_comment_date());?> <span class="comment-time"><?php echo esc_html(get_comment_time()); ?></span></a></time>
                        <?php edit_comment_link(esc_html__('Edit', 'tone'), '', '') ?>
                        <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
                    </div>

                    </header>

                    <?php if ($comment->comment_approved == '0') : ?>
                        <div class="notice">
                            <p class="bottom"><?php esc_html_e('Your comment is awaiting moderation.', 'tone') ?></p>
                        </div>
                    <?php endif; ?>

                    <?php esc_attr(comment_text()) ?>

                </section>

            </div>

		</article>
<?php } ?>

<?php
// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die (esc_html__('Please do not load this page directly. Thanks!', 'tone'));

	if ( post_password_required() ) { ?>
		<div class="notice">
			<p class="bottom"><?php esc_html_e('This post is password protected. Enter the password to view comments.', 'tone'); ?></p>
		</div>
	<?php
		return;
	}
?>
<?php // You can start editing here. Customize the respond form below ?>
<?php if ( have_comments() ) : ?>

    <div class="comments-tree">

            <h3><?php comments_number(esc_html__('No Responses to', 'tone'), esc_html__('One Response to', 'tone'), esc_html__('% Responses to', 'tone') ); ?> &#8220;<?php the_title(); ?>&#8221;</h3>

            <ul class="commentlist">
            <?php wp_list_comments('type=comment&callback=tonetheme_comments'); ?>

            </ul>

            <footer>
                <nav id="comments-nav">
                    <div class="comments-previous"><?php previous_comments_link( esc_html__( '&larr; Older comments', 'tone' ) ); ?></div>
                    <div class="comments-next"><?php next_comments_link( esc_html__( 'Newer comments &rarr;', 'tone' ) ); ?></div>
                </nav>
            </footer>

        </div>
<?php else : // this is displayed if there are no comments so far ?>
	<?php if ( comments_open() ) : ?>
	<?php else : // comments are closed ?>



	<?php endif; ?>
<?php endif; ?>
<?php if ( comments_open() ) : ?>

    <div id="comments">

        <?php comment_form(array('comment_notes_after' => '','title_reply' => esc_html__( 'Leave a Reply', 'tone' ))) ?>

    </div>

<?php endif; // if you delete this the sky will fall on your head ?>