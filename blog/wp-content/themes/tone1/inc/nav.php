<?php if ( get_next_posts_link() ) { ?>

        <nav id="nav-bottom" class="post-nav wide">
            <div class="post-previous colorful"><?php next_posts_link(''.esc_html__('Load More...', 'tone').''); ?></div>
        </nav>

<?php } ?>