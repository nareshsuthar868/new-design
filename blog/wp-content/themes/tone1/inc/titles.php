<?php if ( is_category() ) : ?>

    <div class="archive-header colorful">

        <h1 class="archive-title"><?php single_cat_title(); ?></h1>

    </div>

<?php elseif ( is_author() ) : ?>

    <?php $author = get_userdata( get_query_var('author') );?>
    <?php $author_id = $author->ID; ?>

    <div class="archive-header colorful">

        <h1 class="archive-title"><?php echo esc_attr($author->display_name);?></h1>

    </div>

<?php elseif ( is_tag() ) : ?>

    <div class="archive-header colorful">

        <h1 class="archive-title"><?php esc_html_e('Tag:', 'tone'); ?> <?php single_tag_title(); ?></h1>

    </div>

<?php elseif ( is_search() ) : ?>

    <div class="archive-header colorful">

        <h1 class="archive-title"><?php esc_html_e('keyword:', 'tone'); ?> <?php echo get_search_query(); ?></h1>

    </div>

<?php elseif ( is_day() ) : ?>

    <div class="archive-header colorful">

        <h1 class="archive-title"><?php echo esc_attr( get_the_date( 'D, j F Y' ) ); ?></h1>

    </div>

<?php elseif ( is_month() ) : ?>

    <div class="archive-header colorful">

        <h1 class="archive-title"><?php echo esc_attr( get_the_date( 'F Y' ) ); ?></h1>

    </div>

<?php elseif ( is_year() ) : ?>

    <div class="archive-header colorful">

        <h1 class="archive-title"><?php echo esc_attr( get_the_date( 'Y' ) ); ?></h1>

    </div>

<?php elseif ( is_tax() ) : ?>

    <div class="archive-header colorful">

        <h1 class="archive-title"><?php single_term_title(); ?></h1>

    </div>

<?php else : endif; ?>