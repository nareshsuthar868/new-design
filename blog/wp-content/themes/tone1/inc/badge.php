<?php if ( class_exists( 'RW_Meta_Box' ) ) {
        $badge_text = rwmb_meta( 'tonetheme_badge_text' );
        $badge_color = rwmb_meta( 'tonetheme_badge_color' );
    } else {
        $badge_text = '';
        $badge_color = '';
    } ?>

    <?php if ( $badge_text ) {?>

        <div class="badge-wrapper dark show-for-medium" <?php if ( $badge_color ) {?>style="background: <?php echo esc_attr($badge_color); ?>"<?php } ?>>
            <div class="badge"><?php echo esc_html($badge_text); ?></div>
        </div>

    <?php } ?>