<div class="show-for-large">

    <div id="primary-menu">

        <ul class="main-menu dropdown menu" data-dropdown-menu>

            <?php
            wp_nav_menu( array(
                'theme_location' => 'top',
                'container' => false,
                'depth' => 3,
                'items_wrap' => '%3$s',
                'fallback_cb' => ''
            ) );

            ?>


        </ul>

    </div>


</div>