<?php

$img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large_full' );

if ( class_exists( 'RW_Meta_Box' ) ) {
    $embed_link = rwmb_meta( 'tonetheme_embedurl' );
} else {
    $embed_link = '';
}
?>

<?php
if ( has_post_thumbnail() && has_post_format('image') ) { ?>

    <div class="media-holder">
        <a href="<?php echo esc_url($img[0]); ?>" class="light" title="<?php the_title_attribute(); ?>">
            <?php
                the_post_thumbnail('tonetheme_large_full');
           ?>

        </a>
    </div>

<?php } elseif ( has_post_format( 'audio' ) ) { ?>

    <?php if ($embed_link) { ?>

        <div class="media-holder">

            <div class="soundcloud">

                <?php echo wp_oembed_get($embed_link); ?>

            </div>

        </div>

    <?php } ?>

<?php } elseif ( has_post_format( 'video' )  ) { ?>

    <?php if ($embed_link) { ?>

        <div class="media-holder">

            <div class="flex-video widescreen vimeo">

                <?php
                echo wp_oembed_get($embed_link); ?>

            </div>

        </div>

    <?php } ?>

<?php } ?>