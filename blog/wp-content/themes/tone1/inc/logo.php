<?php
$logo_width = get_theme_mod( 'tonetheme_logo_width' );
$disable_logo_icon = get_theme_mod( 'tonetheme_disable_logo_icon' );
?>


    <div id="logo">

        <?php  if ( function_exists( 'the_custom_logo' ) && has_custom_logo() ) { ?>

            <div class="image-logo"<?php if ($logo_width) { ?> style="width:<?php echo esc_attr($logo_width); ?>px;"<?php }?>>

                <?php the_custom_logo(); ?>

            </div>

        <?php } else { ?>

            <div class="text-logo<?php if ( !$disable_logo_icon ) { echo ' logo-icon'; } ?>">

                <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a>

            </div>

        <?php } ?>

        <?php if ( get_bloginfo('description') ) {
                echo '<div class="row collapse"><div class="large-4 medium-6 large-centered medium-centered columns "><h4 class="tagline">';
                echo get_bloginfo ('description');
                echo '</h4></div></div>';
            }  ?>

    </div>
