<?php
$normal_font = get_theme_mod( 'tonetheme_disable_google_italic' );
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo esc_url( home_url('/feed/') ); ?>">
<?php wp_head(); ?>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_directory'); ?>/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_directory'); ?>/css/custom-styles.css">
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/easing.jquery.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/readmore.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/custom-function.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/grid-isotope.pkgd.js"></script>
</head>
<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">
<div class="off-canvas-wrapper">
<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
<div class="off-canvas-content" data-off-canvas-content>

<!-- Start the main container -->
<section id="wrapper" class="animated fadeIn<?php if ( $normal_font ) { echo ' normal-font'; } ?>" role="document">

<!-- <button type="button" class="menu-icon" data-toggle="offCanvasLeft"></button>
-->

<?php if ( is_home() || is_front_page () ) {

                    dynamic_sidebar('homepage-header');

                } else {

                    dynamic_sidebar('header');

                } ?>
<header id="header" role="banner" class="header">
  <div class="headertop">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <?php get_template_part( 'inc/logo'); ?>
          <!-- <ul class="headeropt">
            <li><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal3"><i class="material-icons">person</i></a></li>
            <li> <a class="carticn" href="javascript:void(0)"> <i class="material-icons nw_theme_color">shopping_cart</i> <span>02</span> </a> </li>
          </ul> -->
          <div class="mobileoverlay"></div>
          <?php get_template_part( 'inc/menu' ); ?>
          <?php if ( has_header_image() ) { ?>
          <div class="banner"> <img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="<?php esc_html_e('Banner', 'tone'); ?>" /> </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
  <!-- cart sidebar mene -->
  <!-- <aside class="cartsidebar"> <a href="javascript:void(0)" class="cartcross"><i class="material-icons">close</i></a>
    <div class="cartsys">
      <div class="titlecart">Recently Added Item</div>
      <ul>
        <li>
          <div class="cart-thumb"><img src="<?php bloginfo('template_directory'); ?>/img/Belle_Double_Bed_Without_Storage3.jpg" alt="cart" /></div>
          <div class="cart-desc"> <strong>Belle Double Bed </strong> <small>Duration / 12 months</small> <span>1 x &#8377; 900.00</span> <a href="javascript:void(0)"><i class="material-icons">mode_edit</i></a> <a href="javascript:void(0)"><i class="material-icons">delete</i></a> </div>
        </li>
        <li>
          <div class="cart-thumb"><img src="<?php bloginfo('template_directory'); ?>/img/Belle_Double_Bed_Without_Storage3.jpg" alt="cart" /></div>
          <div class="cart-desc"> <strong>Belle Double Bed </strong> <small>Duration / 12 months</small> <span>1 x &#8377; 900.00</span> <a href="javascript:void(0)"><i class="material-icons">mode_edit</i></a> <a href="javascript:void(0)"><i class="material-icons">delete</i></a> </div>
        </li>
        <li>
          <div class="pull-left">SUBTOTAL</div>
          <div class="pull-right"><strong>&#8377;  1800.00</strong></div>
        </li>
      </ul>
      <div class="btncartdiv"> <a href="javascript:void(0)" class="btn-check pull-left"><i class="material-icons">shopping_cart</i><span>View Cart</span></a> <a href="javascript:void(0)" class="btn-check pull-right"><span>Check Out</span> <i class="material-icons checkrot">reply</i></a> </div>
    </div>
  </aside> -->
  <!-- cart sidebar mene --> 
</header>
