<?php get_header(); ?>

    <div class="boxed white page_section_offset">
        <section class="innerbanner">
    		<div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h1>Blog</h1>
          <ul class="breadcrumb">
            <?php if(get_site_url() == $site_url){  ?>
            <li><a href="<?php echo  $new_url; ?>">Home</a></li>
            <li class="active">Blog</li>
            <?php }?>
          </ul>
        </div>
      </div>
    </div>
        </section>
        <section id="content">
		   <div class="container">	
            <div class="row">
                <div class="bloglistleft">
                    <?php if ( have_posts() ) : ?>
                        <div id="grid-content" class="grid">
                            <?php while ( have_posts() ) : the_post(); ?>
                                <?php get_template_part( 'loop', 'halfwidth'); ?>
                            <?php endwhile; ?>
                        </div>
                    <?php else : ?>
                        <?php get_template_part( 'content', 'none' ); ?>
                    <?php endif; ?>
                </div>
                <div class="blogsidebar">
                 	<?php get_sidebar(); ?>
				</div>
            </div>
		   </div>	
        </section>
    </div>

<?php get_footer(); ?>