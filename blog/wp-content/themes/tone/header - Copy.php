<?php
$normal_font = get_theme_mod( 'tonetheme_disable_google_italic' );
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo('charset'); ?>">

    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />

	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo esc_url( home_url('/feed/') ); ?>">

<?php wp_head(); ?>

</head>


<body <?php body_class(); ?> itemscope itemtype="http://schema.org/WebPage">

<div class="off-canvas-wrapper">

    <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>

        <div class="off-canvas-content" data-off-canvas-content>
            
            <!-- Start the main container -->
            <section id="wrapper" class="animated fadeIn<?php if ( $normal_font ) { echo ' normal-font'; } ?>" role="document">

                <button type="button" class="menu-icon" data-toggle="offCanvasLeft"></button>

                
                <?php if ( is_home() || is_front_page () ) {

                    dynamic_sidebar('homepage-header');

                } else {

                    dynamic_sidebar('header');

                } ?>


            <header id="header" role="banner" class="header">


                <div class="boxed">

                    <?php get_template_part( 'inc/logo'); ?>

                    <?php get_template_part( 'inc/menu' ); ?>

                    <?php if ( has_header_image() ) { ?>

                        <div class="banner">

                            <img src="<?php header_image(); ?>" height="<?php echo get_custom_header()->height; ?>" width="<?php echo get_custom_header()->width; ?>" alt="<?php esc_html_e('Banner', 'tone'); ?>" />

                        </div>

                    <?php } ?>

                </div>

            </header>
