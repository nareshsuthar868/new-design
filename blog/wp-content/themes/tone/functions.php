<?php

// Main

include_once( get_template_directory() . '/lib/enqueue.php');
include_once( get_template_directory() . '/lib/utility.php');
include_once( get_template_directory() . '/lib/widget-areas.php');
include_once( get_template_directory() . '/lib/metaboxes.php');
include_once( get_template_directory() . '/vendor/tgm/install-plugins.php');

// Add the Customize page to the WordPress admin area
function tonetheme_customizer_menu() {
    add_theme_page( esc_html__( 'Customize', 'tone' ), esc_html__( 'Customize', 'tone' ), 'edit_theme_options', 'customize.php' );
}
include_once( get_template_directory() . '/lib/customizer.php');

// Add theme supports
function tonetheme_theme_support() {

	// language supports
	load_theme_textdomain('tone', get_stylesheet_directory() . '/languages');
    // rss
    add_theme_support('automatic-feed-links');
    // thumbnails
    add_theme_support( 'post-thumbnails' );
    // seo titles
    add_theme_support( 'title-tag' );
    // custom header
    $args_header = array(
        'flex-height'            => true,
        'flex-width'             => true,
        'height'                 => 340,
        'width'                  => 1280,
        'header-text'            => false,
    );
    add_theme_support( 'custom-header', $args_header );
    // background
    $args_bg = array(
        'header-text'            => false
    );
    add_theme_support( 'custom-background', $args_bg );

    add_theme_support( 'custom-logo', array(
        'flex-width' => true,
    ) );

        // sizes
        add_image_size( 'tonetheme_medium_full', 800, 0 );
        add_image_size( 'tonetheme_medium_crop', 800, 800, true );
        add_image_size( 'tonetheme_small_full', 480, 0 );
        add_image_size( 'tonetheme_large_full', 1280, 0 );

    // post formats support
	add_theme_support('post-formats', array('image', 'video', 'aside', 'link', 'audio'));

    // valid HTML5

    add_theme_support( 'html5', array(
        'search-form',
        'comment-form',
        'comment-list',
        'gallery',
        'caption',
    ) );

	// menus support

	register_nav_menus(array(
		'top' => esc_html__('Top Navigation', 'tone'),
		'footer' => esc_html__('Footer Navigation', 'tone')
	));

}
add_action('after_setup_theme', 'tonetheme_theme_support'); /* end theme support */

if ( ! isset( $content_width ) ) $content_width = 1280;

function tonetheme_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'admin_init', 'tonetheme_add_editor_styles' );

