<div class="navigation">

    <div id="primary-menu">
        <div class="menuheader  visible-xs">
        <div class="mobilemenu"> 
            <a href="javascript:void(0)" class="togglemenu"> <i class="material-icons">menu</i> </a> 
        </div>
        <div class="logo"> <a href="http://180.211.99.165:8080/rajan/cityfurnish" class="d_inline_b show_on_mobile"> <img src="<?php bloginfo('template_directory'); ?>/img/mobile-logo-white.png" alt="Logo Here"> </a> </div>
        <div class="header-right-top" >
          <ul>
            <li><a  href="mailto:hello@cityfurnish.com"> <i class="material-icons">mail_outline</i></a> </li>
            <li><a  href="tel:8010845000"><i class="material-icons">phone</i></a> 
          </ul>
        </div> 
       </div>
        <ul class="main-menu dropdown menu" data-dropdown-menu>
            <?php
            wp_nav_menu( array(
                'theme_location' => 'top',
                'container' => false,
                'depth' => 3,
                'items_wrap' => '%3$s',
                'fallback_cb' => ''
            ) );

            ?>


        </ul>

    </div>


</div>