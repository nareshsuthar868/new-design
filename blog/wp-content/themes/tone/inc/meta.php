<?php $hide_meta = get_theme_mod( 'tonetheme_hide_meta' ); ?>

<?php if ( $hide_meta !== 'hide_all' ) { ?>
    <?php $hide_meta = get_theme_mod( 'tonetheme_hide_meta' ); ?>

    <div class="entry-meta hide-for-small">

        <?php if ($hide_meta == 'show_date' || $hide_meta == 'show_date_comments') { } else { ?>
            <span class="author"><?php esc_html_e('by', 'tone'); ?>&nbsp;<?php echo get_the_author_link() ?></span>&nbsp;&nbsp;&nbsp;

        <?php } ?>

        <?php if ( $hide_meta == 'show_author' || $hide_meta == 'show_author_comments') { } else { ?>
            <span class="updated">
                <time datetime="<?php echo esc_attr(get_the_time('c')); ?>">
                    <strong><?php echo esc_html(get_the_date('d')); ?></strong>
                    <small><?php echo esc_html(get_the_date('M')); ?></small>
                </time>
            </span>
        <?php } ?>

        <?php if ($hide_meta == 'show_date' || $hide_meta == 'show_author_date' || $hide_meta == 'show_author') { } else { ?>
            <i class="i-comment-1"></i>&nbsp;<a href="<?php echo get_comments_link();?>">(<?php echo get_comments_number( '0', '1', '%' )?>)&nbsp;&nbsp;Comments</a>
        <?php } ?>

    </div>

<?php } else { } ?>