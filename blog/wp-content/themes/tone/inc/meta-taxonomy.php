<?php $hide_meta = get_theme_mod( 'tonetheme_hide_meta' ); ?>

<?php if ( class_exists( 'RW_Meta_Box' ) ) {
    $post_info = rwmb_meta( 'tonetheme_postinfo' );
} else {
    $post_info = '';
}?>

<?php if ( $hide_meta !== 'hide_all' && $post_info == false ) { ?>

<footer class="single-bottom">

    <div class="entry-meta show-for-large">

        <?php if ($hide_meta == 'show_date' || $hide_meta == 'show_date_comments') { } else { ?>
            <span class="author"><?php esc_html_e('by', 'tone'); ?>&nbsp;<?php echo get_the_author_link() ?></span>&nbsp;&nbsp;&nbsp;

        <?php } ?>


        <?php if ( $hide_meta == 'show_author' || $hide_meta == 'show_author_comments') { } else { ?>
            <span class="updated">
                <time datetime="<?php echo esc_attr(get_the_time('c')); ?>">
                    <?php echo esc_html(get_the_date()); ?>&nbsp;&nbsp;&nbsp;
                </time>
            </span>

        <?php } ?>

        <?php if ($hide_meta == 'show_date' || $hide_meta == 'show_author_date' || $hide_meta == 'show_author') { } else { ?>
            <i class="i-comment-1"></i>&nbsp;<a href="<?php echo get_comments_link();?>">(<?php echo get_comments_number( '0', '1', '%' )?>)&nbsp;&nbsp;Comments</a>
        <?php } ?>

        <div class="right">
        <i class="i-doc-text"></i>&nbsp;<?php the_category(', '); ?>

        <?php the_tags('&nbsp;&nbsp;&nbsp;<i class="i-tag">&nbsp;</i>'); ?>
        </div>
    </div>

</footer>

<?php } else  { echo '<div class="divider"></div>'; } ?>