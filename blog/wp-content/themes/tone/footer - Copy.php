<?php $credit_link = get_theme_mod( 'tonetheme_disable_credit_link' ); ?>

<div class="boxed">

    <div id="footer">

        <?php get_template_part( 'inc/nav'); ?>

        <div class="row collapse">

            <div class="large-6 columns">

                <ul id="footer-menu" class="secondary-menu horizontal menu">

                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'footer',
                        'container' => false,
                        'depth' => 1,
                        'items_wrap' => '%3$s',
                        'fallback_cb' => ''
                    ) );

                    ?>


                </ul>

           </div>

           <div class="large-6 columns">

               <span class="text-line">

                   <?php $copyright_textbox = get_theme_mod( 'tonetheme_copyright_textbox' );
                   if ($copyright_textbox != '') { ?>

                       <?php echo esc_html($copyright_textbox); ?>

                   <?php } else { ?>

                       <?php esc_html_e('Copyright', 'tone'); ?> &copy; <?php echo esc_attr( date('Y') ); ?> <?php bloginfo('name'); ?><?php if ( !$credit_link && (is_home() || is_front_page () )) {?><?php esc_html_e('. Design by', 'tone'); ?> <a href="http://themeous.net">Themeous</a><?php } ?>

                   <?php } ?>

                </span>

           </div>

        </div>

    </div>

</div>

</section><!-- Wrapper End -->

</div>
<div class="off-canvas position-left" id="offCanvasLeft" data-off-canvas>
    <?php get_sidebar(); ?>
</div>
</div>
</div>

<?php wp_footer(); ?>
	
</body>
</html>