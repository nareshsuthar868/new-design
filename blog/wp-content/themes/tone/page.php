<?php get_header(); ?>

<section class="innerbanner box-top-padd">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h1><?php echo get_the_title(); ?></h1>
        <ul class="breadcrumb">
          <li><a href="<?php echo $new_url; ?>">Home</a></li>
          <li class="active"><?php echo get_the_title(); ?></li>
        </ul>
      </div>
    </div>
  </div>
</section>
<section id="single" role="main">
  <div class="boxed white box-single-content">
    <div class="container">
     <div class="row">
          <div class="blog_single_content">
            <?php get_template_part('content','page'); ?>
          </div>
          <div class="blogsidebar">
            <?php get_sidebar(); ?>
          </div>
     </div> 
    </div>
  </div>
</section>
<?php get_footer(); ?>
