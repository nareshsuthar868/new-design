(function($, result, nextlink, collage, img) {
    "use strict";

    // Lightbox


    $('.gallery').lightGallery({
        mode: 'lg-slide',
        cssEasing : 'ease',
        easing: 'linear',
        selector: '.gallery-item a',
        download: false
    });
    $('.entry-content').lightGallery({
        mode: 'lg-slide',
        cssEasing : 'ease',
        easing: 'linear',
        selector: 'a.light',
        download: false
    });


    // Collage

    $(document).ready(function(){
        collage();
    });

    function collage() {
        $('.gallery.gallery-columns-1').removeWhitespace().collagePlus(
            {
                'fadeSpeed'     : 2000,
                'targetHeight'  : 800
            }
        );
        $('.gallery.gallery-columns-2').removeWhitespace().collagePlus(
            {
                'fadeSpeed'     : 2000,
                'targetHeight'  : 700
            }
        );
        $('.gallery.gallery-columns-3').removeWhitespace().collagePlus(
            {
                'fadeSpeed'     : 2000,
                'targetHeight'  : 520
            }
        );
        $('.gallery.gallery-columns-4').removeWhitespace().collagePlus(
            {
                'fadeSpeed'     : 2000,
                'targetHeight'  : 480
            }
        );
        $('.gallery.gallery-columns-5').removeWhitespace().collagePlus(
            {
                'fadeSpeed'     : 2000,
                'targetHeight'  : 360
            }
        );
        $('.gallery.gallery-columns-6').removeWhitespace().collagePlus(
            {
                'fadeSpeed'     : 2000,
                'targetHeight'  : 280
            }
        );
        $('.gallery.gallery-columns-7').removeWhitespace().collagePlus(
            {
                'fadeSpeed'     : 2000,
                'targetHeight'  : 200
            }
        );
        $('.gallery.gallery-columns-8').removeWhitespace().collagePlus(
            {
                'fadeSpeed'     : 2000,
                'targetHeight'  : 100
            }
        );
        $('.gallery.gallery-columns-9').removeWhitespace().collagePlus(
            {
                'fadeSpeed'     : 2000,
                'targetHeight'  : 80
            }
        );
    }

    // This is just for the case that the browser window is resized
    var resizeTimer = null;
    $(window).bind('resize', function() {
        // hide all the images until we resize them
        $('.gallery .gallery-item').css("opacity", 0);
        // set a timer to re-apply the plugin
        if (resizeTimer) clearTimeout(resizeTimer);
        resizeTimer = setTimeout(collage, 200);
    });

   
    // Ajax Load More

   /* $('.post-previous a').live('click', function(e) {
        e.preventDefault();
        $(this).addClass('loading').text(tone_load_more.Loading);
        $.ajax({
            type: "GET",
            url: $(this).attr('href') + '#grid-content',
            dataType: "html",
            success: function(out) {
                result = $(out).find('.grid .grid-item');
                nextlink = $(out).find('.post-previous a').attr('href');
                $('#grid-content').append(result);
                $('.post-previous a').removeClass('loading').text(tone_load_more.LoadMore);
                if (nextlink != undefined) {
                    $('.post-previous a').attr('href', nextlink);
                } else {
                    $('.post-previous a').remove();
                    $('.post-previous').addClass('load-more last').text(tone_load_more.TheEnd);
                }
            }
        });
    });*/

    /* Foundation */

   // $(document).foundation();

})(jQuery);