$(document).ready(function(){
	// sticky menu
	$(window).scroll(function() {
		if ($(this).scrollTop() > 10){  
			$('header').addClass("stickyinner");
		}
		else{
			$('header').removeClass("stickyinner");
		}	
	});


	
	
	//Check to see if the window is top if not then display button
	$(window).scroll(function(){
		if ($(this).scrollTop() > 100) {
			
			$('.back_to_top').fadeIn();
		} else {
			$('.back_to_top').fadeOut();
		}
	});
	
	//Click event to scroll to top
	$('.back_to_top').click(function(){
		//alert("hello");
		$('html, body').animate({scrollTop : 0},500);
		return false;
	});
	
	 // read more expand function 
		   $('article.qulitysec').readmore({
			   speed: 500,
			   collapsedHeight: 50,
			   moreLink: '<a href="javascript:void(0)" class="readmorebtn">Read More<i class="material-icons">keyboard_arrow_down</i></a>',
			   lessLink: '<a href="javascript:void(0)" class="readmorebtn">Read Less<i class="material-icons icnup">keyboard_arrow_up</i></a>',
			});
	
	
	


	
	// bootstrap menu responsive
	$("a.togglemenu").click(function(event){
		event.stopPropagation();
		$("div.navigation").toggleClass("intro");
		$(".mobileoverlay").toggleClass("overvisible");
	});
	
	$(".mobileoverlay").on('click', function(){
		$("div.navigation").removeClass("intro");
		$(this).removeClass("overvisible");
	});
	
	// header select city dropdown
	$(".dropdown-menu li a").click(function(){
		var selText = $(this).text();
		$(this).parents('.btn-group').find('.dropdown-toggle').html('<span>'+ selText +'</span>'+'<i class="downarrow"></i>');
	});
	
	// header select city dropdown
	
	$(".menu-item-has-children a").click(function(){
		 $(this).toggleClass('open');
		 $(this).parent().siblings().children("ul.sub-menu").hide();
		 $(this).parent().siblings().children("a").removeClass("open");
		 $(this).next("ul.sub-menu").toggle();
	});
	
	
	/*// bootstrap menu responsive
	$("a.togglemenublog").click(function(event){
		event.stopPropagation();
		$("div.navigation").toggleClass("intro");
		$(".mobileoverlay").toggleClass("overvisible");
	});
	
	$(".mobileoverlay").on('click', function(){
		$("div.navigation").removeClass("intro");
		$(this).removeClass("overvisible");
	});*/
	
	//cart sidebar 
	$(".carticn").click(function(){
 	    //event.stopPropagation();
		$(".cartsidebar").addClass("sideright");
	});
	$(".cartcross").click(function(){
		$(".cartsidebar").removeClass("sideright");	
	}); 
	
	
	$('.tabsignup').click(function(e) {
		e.preventDefault();
		$('.popup-left span').show();
	});
	
	$('.tabsignin').click(function(e) {
		e.preventDefault();
		$('.popup-left span').hide();
	}); 
	
	$('.forgot-pass').click(function(e) {
		e.preventDefault();
		$('.login-block').hide();
		$('.popup-left span').hide();
		$('.forgot-pass-block').show();
	});
	
	$('.backtologin').click(function(e) {
		e.preventDefault();
		$('.login-block').show();
		$('.forgot-pass-block').hide();
    }); 
			  

/* end main ready funciton over */
});

$(window).load(function() {
    $('.grid').isotope({
		  itemSelector: '.grid-item',
		  percentPosition: true,
		  masonry: {
			// use outer width of grid-sizer for columnWidth
			//columnWidth: '.grid-item'
		  }
		})
	
});
