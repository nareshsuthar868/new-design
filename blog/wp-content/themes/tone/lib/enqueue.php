<?php

// start all the functions
add_action('after_setup_theme','tonetheme_startup');

function tonetheme_startup() {

    add_action('wp_enqueue_scripts', 'tonetheme_load_fonts');
    add_action('wp_enqueue_scripts', 'tonetheme_scripts', 999);
    add_action( 'wp_enqueue_scripts', 'tonetheme_css_style' );
    add_action( 'wp_enqueue_scripts', 'tonetheme_custom_css_style' );
    add_action( 'admin_print_styles', 'tonetheme_admin_styles' );
    // ie conditional wrapper
    add_filter( 'style_loader_tag', 'tonetheme_ie_conditional', 10, 2 );
    // clean up gallery output in wp
    add_filter('gallery_style', 'tonetheme_gallery_style');

}

/**********************
Enqueue CSS and Scripts
**********************/

function tonetheme_fonts_url() {

    $fonts_url = '';
    $font_name = esc_attr( get_theme_mod( 'tonetheme_google_font') );
    $subset = get_theme_mod( 'tonetheme_subset' );

    $font = _x( 'on', 'Google font: on or off', 'tone' );

    if ( 'off' !== $font ) {
        $font_families = array();

        if ( 'off' !== $font ) {
            if ($font_name) {

                $font_families[] = $font_name.':400,400italic,600,800';

            } else {

                $font_families[] = 'Lora:400,400italic,700,700italic';

            }

        }

        if ($subset == 'cyrillic') {
            $query_args = array(
                'family' => urlencode( implode( '|', $font_families ) ),
                'subset' => urlencode( 'latin,cyrillic' ),
            );

        } elseif  ($subset == 'latin-ext') {
            $query_args = array(
                'family' => urlencode( implode( '|', $font_families ) ),
                'subset' => urlencode( 'latin,latin-ext' ),
            );
        } else {
            $query_args = array(
                'family' => urlencode( implode( '|', $font_families ) ),
                'subset' => urlencode( 'latin' ),
            );
        }

        $fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
    }

    return esc_url_raw( $fonts_url );
}

function tonetheme_load_fonts() {

    $disable_font = get_theme_mod( 'tonetheme_disable_google_font' );
    if ($disable_font) { } else {
        wp_enqueue_style( 'tonetheme-fonts', tonetheme_fonts_url(), array(), '1.1.0' );

    }
}

// loading modernizr and jquery, and reply script
function tonetheme_scripts() {
  if (!is_admin()) {

    // comment reply script for threaded comments
    if( get_option( 'thread_comments' ) )  { wp_enqueue_script( 'comment-reply' ); }

      // adding scripts files in footer

      wp_register_script( 'tonetheme-plugins-js', get_template_directory_uri() . '/js/plugins.min.js', array( 'jquery'), '1.0.0', true );

    wp_register_script( 'tonetheme-settings-js', get_template_directory_uri() . '/js/settings.js', array( 'jquery' ), '', true );

      // Localize the script with new data
      $translation_array = array(
          'Loading' => esc_html__( 'Loading...', 'tone' ),
          'LoadMore' => esc_html__( 'Load More...', 'tone' ),
          'TheEnd' => esc_html__( 'The end...', 'tone' )
      );
      wp_localize_script( 'tonetheme-settings-js', 'tone_load_more', $translation_array );


    // enqueue scripts
    wp_enqueue_script( 'tonetheme-plugins-js' );
    wp_enqueue_script( 'tonetheme-settings-js' );

  }
}
function tonetheme_css_style()
{

    if (!is_admin()) {

    // foundation stylesheet
        wp_enqueue_style( 'tonetheme-basic', get_template_directory_uri() . '/css/basic.min.css', array(), '',  'all' );

    // main style

        wp_enqueue_style( 'tonetheme-main', get_template_directory_uri() . '/css/styles.min.css', array(), '', 'all' );

        wp_enqueue_style( 'tonetheme-style', get_stylesheet_uri(), array(), '', 'all' );

    // responsive styles

        wp_enqueue_style( 'tonetheme-responsive', get_template_directory_uri() . '/css/responsive.min.css', array(), '', 'screen');

    }

}

function tonetheme_admin_styles(){
    global $typenow;

    wp_register_style( 'tonetheme-admin-stylesheet', get_template_directory_uri() . '/css/theme-admin.css', array(), '', 'screen' );

    if( $typenow == 'post' || $typenow == 'portfolio' || $typenow == 'page' ) {
        wp_enqueue_style( 'tonetheme-admin-stylesheet' );
    }
}

function tonetheme_custom_css_style() {

    wp_enqueue_style(
        'tonetheme-custom',
        get_template_directory_uri() . '/css/custom.css'
    );

    $color_1 = esc_attr(get_theme_mod( 'tonetheme_custom_color_1' ));
    $color_2 = esc_attr(get_theme_mod( 'tonetheme_custom_color_2' ));
    $color_3 = esc_attr(get_theme_mod( 'tonetheme_custom_color_3' ));
    $color_4 = esc_attr(get_theme_mod( 'tonetheme_custom_color_4' ));
    $font = esc_attr(get_theme_mod( 'tonetheme_google_font' ));
    $body_bg_color = esc_attr(get_background_color());

    $custom_font = "body { font-family: '$font', Georgia, serif !important; }";

    $custom_color_1 = "
    
            #logo .text-logo:before {
                color: $color_1;
            }
    
            table#wp-calendar thead, .tabs dd.active a, input#submit, input#wp-submit, .wpcf7-submit, .postfix, .colorful, .menu-icon {
            background-color: $color_1;
        }
        input[type='text']:focus,input[type='password']:focus,input[type='email']:focus,input[type='search']:focus,input[type='tel']:focus,input[type='url']:focus,textarea:focus, .widget .textwidget a {
            border-color: $color_1;
        }
      
    ";

    $custom_color_2 = "
    
        .text-logo a, .text-logo a:hover, #logo a, ul.main-menu li a:hover, ul.main-menu li.current_page_item>a, ul.main-menu li a, ul.secondary-menu li a, .sub-nav li a, #logo .tagline, #primary-menu li a:before, #footer-menu li a:before, .text-line, .text-line a {
            color: $color_2;
        }

    ";

    $custom_color_3 = "
      
        .entry-content a {
            background-color: $color_3;
        }
    ";

    $custom_color_4 = "
      
        .entry-content a {
            color: $color_4;
        }
    ";

    $body_bg = "
        body {
            background: #$body_bg_color;
        }
        #primary-menu:after {
            border-color: #$body_bg_color transparent transparent transparent;
        }
        #footer:before {
            border-color: transparent transparent #$body_bg_color transparent;
        }

    ";

    if ($font !== 'Lora' && $font ) {
        wp_add_inline_style( 'tonetheme-custom', $custom_font );
    }
    if ($color_1 !== '#fff388' && $color_1 ) {
        wp_add_inline_style( 'tonetheme-custom', $custom_color_1 );
    }
    if ($color_2 !== '#403f3f' && $color_2 ) {
        wp_add_inline_style( 'tonetheme-custom', $custom_color_2 );
    }
    if ($color_3 !== '#fff388' && $color_3 ) {
        wp_add_inline_style( 'tonetheme-custom', $custom_color_3 );
    }
    if ($color_4 !== '#403f3f' && $color_4 ) {
        wp_add_inline_style( 'tonetheme-custom', $custom_color_4 );
    }
    if ($body_bg_color !== '#f8f8f8' && $body_bg_color ) {
        wp_add_inline_style( 'tonetheme-custom', $body_bg );
    }
}
    
// adding the conditional wrapper around ie stylesheet
// source: http://code.garyjones.co.uk/ie-conditional-style-sheets-wordpress/
function tonetheme_ie_conditional( $tag, $handle ) {
	if ( 'theme-ie-only' == $handle )
		$tag = '<!--[if lt IE 9]>' . "\n" . $tag . '<![endif]-->' . "\n";
	return $tag;
}

// remove injected CSS from gallery
function tonetheme_gallery_style($css) {
    return preg_replace("!<style type='text/css'>(.*?)</style>!s", '', $css);
}

/* Add Odd/Even Class
 * Courtesy http://wpsnipp.com/index.php/css/alternate-odd-even-post-class/
 */

function tonetheme_oddeven_post_class ( $classes ) {
    global $current_class;
    $classes[] = $current_class;
    $current_class = ($current_class == 'odd') ? 'even' : 'odd';
    return $classes;
}
add_filter ( 'post_class' , 'tonetheme_oddeven_post_class' );
global $current_class;
$current_class = 'odd';