<?php

function tonetheme_customizer( $wp_customize ) {
    $wp_customize->add_section(
        'tonetheme_section_one',
        array(
            'title' => __('General','tone'),
            'description' => __('General settings','tone'),
            'priority' => 20,
        )
    );

    $wp_customize->add_section(
        'tonetheme_section_two',
        array(
            'title' => __('Fonts','tone'),
            'description' => __('Google Fonts','tone'),
            'priority' => 21,
        )
    );

    $wp_customize->add_section(
        'tonetheme_section_four',
        array(
            'title' => __('Social Media','tone'),
            'description' => __('Social Media buttons','tone'),
            'priority' => 41,
        )
    );


    $wp_customize->add_section(
        'tonetheme_section_five',
        array(
            'title' => __('Blog','tone'),
            'description' => __('Blog settings','tone'),
            'priority' => 43,
        )
    );

    /*Custom Color*/

    $wp_customize->add_setting(
        'tonetheme_custom_color_1',
        array(
            'default' => '#fff388',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'tonetheme_custom_color_1',
            array(
                'label' => __('Accent color','tone'),
                'section' => 'colors',
                'settings' => 'tonetheme_custom_color_1',
                'priority' => 1
            )
        )
    );

    $wp_customize->add_setting(
        'tonetheme_custom_color_2',
        array(
            'default' => '#403f3f',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'tonetheme_custom_color_2',
            array(
                'label' => __('Header/Footer Links & Text','tone'),
                'section' => 'colors',
                'settings' => 'tonetheme_custom_color_2',
                'priority' => 2
            )
        )
    );

    $wp_customize->add_setting(
        'tonetheme_custom_color_3',
        array(
            'default' => '#fff388',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'tonetheme_custom_color_3',
            array(
                'label' => __('Post Links Background','tone'),
                'section' => 'colors',
                'settings' => 'tonetheme_custom_color_3',
                'priority' => 3
            )
        )
    );

    $wp_customize->add_setting(
        'tonetheme_custom_color_4',
        array(
            'default' => '#403f3f',
            'sanitize_callback' => 'sanitize_hex_color',
        )
    );

    $wp_customize->add_control(
        new WP_Customize_Color_Control(
            $wp_customize,
            'tonetheme_custom_color_4',
            array(
                'label' => __('Post Links Color','tone'),
                'section' => 'colors',
                'settings' => 'tonetheme_custom_color_4',
                'priority' => 4
            )
        )
    );

    /* Logo Width */
    $wp_customize->add_setting(
        'tonetheme_logo_width',
        array(
            'default' => '',
            'sanitize_callback' => 'tonetheme_sanitize_number',
        )
    );

    $wp_customize->add_control(
        'tonetheme_logo_width',
        array(
            'label' => __('Limit logo width (in pixels)','tone'),
            'section' => 'title_tagline',
            'type' => 'text',
            'priority' => 8
        )
    );

    $wp_customize->add_setting(
        'tonetheme_disable_logo_icon',
        array(
            'default' => '',
            'sanitize_callback' => 'tonetheme_sanitize_checkbox',
        )
    );

    $wp_customize->add_control(
        'tonetheme_disable_logo_icon',
        array(
            'type' => 'checkbox',
            'label' => esc_html__('Disable Text Logo Icon?','tone'),
            'section' => 'title_tagline',
            'priority' => 19
        )
    );

    /*Copyright Text*/
    $wp_customize->add_setting(
        'tonetheme_copyright_textbox',
        array(
            'default' => '',
            'sanitize_callback' => 'tonetheme_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'tonetheme_copyright_textbox',
        array(
            'label' => __('Copyright text','tone'),
            'section' => 'tonetheme_section_one',
            'type' => 'text',
            'priority' => 12
        )
    );

    $wp_customize->add_setting(
        'tonetheme_disable_credit_link',
        array(
            'default' => '',
            'sanitize_callback' => 'tonetheme_sanitize_checkbox',
        )
    );

    $wp_customize->add_control(
        'tonetheme_disable_credit_link',
        array(
            'type' => 'checkbox',
            'label' => esc_html__('Disable Credit Link on homepage?','tone'),
            'section' => 'tonetheme_section_one',
            'priority' => 14
        )
    );

    /* Google Fonts */

    /* Subset */
    $wp_customize->add_setting(
        'tonetheme_subset',
        array(
            'default' => '',
            'sanitize_callback' => 'tonetheme_sanitize_subset'
        )
    );

    $wp_customize->add_control(
        'tonetheme_subset',
        array(
            'type' => 'select',
            'label' => __('Subset Type','tone'),
            'section' => 'tonetheme_section_two',
            'choices' => array(
                'latin' => __('Latin (Default)','tone'),
                'latin-ext' => __('Latin Extended','tone'),
                'cyrillic' => __('Cyrillic','tone')
            ),
            'priority' => 12
        )
    );


    $wp_customize->add_setting(
        'tonetheme_google_font',
        array(
            'default' => '',
            'sanitize_callback' => 'tonetheme_sanitize_text',
        )
    );

    $wp_customize->add_control(
        'tonetheme_google_font',
        array(
            'label' => __('Google Font Name, ex. Roboto','tone'),
            'section' => 'tonetheme_section_two',
            'type' => 'text',
            'priority' => 5
        )
    );

    $wp_customize->add_setting(
        'tonetheme_disable_google_italic',
        array(
            'default' => '',
            'sanitize_callback' => 'tonetheme_sanitize_checkbox',
        )
    );

    $wp_customize->add_control(
        'tonetheme_disable_google_italic',
        array(
            'type' => 'checkbox',
            'label' => esc_html__('Disable Italic Titles?','tone'),
            'section' => 'tonetheme_section_two',
            'priority' => 6
        )
    );

    $wp_customize->add_setting(
        'tonetheme_disable_google_font',
        array(
            'default' => '',
            'sanitize_callback' => 'tonetheme_sanitize_checkbox',
        )
    );

    $wp_customize->add_control(
        'tonetheme_disable_google_font',
        array(
            'type' => 'checkbox',
            'label' => esc_html__('Disable Google Fonts?','tone'),
            'section' => 'tonetheme_section_two',
            'priority' => 8
        )
    );

    /*Hide Meta */

    $wp_customize->add_setting(
        'tonetheme_hide_meta',
        array(
            'default' => '',
            'sanitize_callback' => 'tonetheme_sanitize_hide_meta'
        )
    );

    $wp_customize->add_control(
        'tonetheme_hide_meta',
        array(
            'type' => 'select',
            'label' => __('Post info to show','tone'),
            'section' => 'tonetheme_section_five',
            'choices' => array(
                'show_all' => __('Show all Post Info','tone'),
                'hide_all' => __('Hide all Post Info','tone'),
                'show_author_date' => __('Author + Date','tone'),
                'show_date_comments' => __('Date + Comments','tone'),
                'show_author_comments' => __('Author + Comments','tone'),
                'show_author' => __('Just Author','tone'),
                'show_date' => __('Just Date','tone')
            ),
            'priority' => 6
        )
    );



}
add_action( 'customize_register', 'tonetheme_customizer' );


function tonetheme_sanitize_text( $input ) {
    return wp_kses_post( force_balance_tags( $input ) );
}

function tonetheme_sanitize_checkbox( $input ) {
    if ( $input == 1 ) {
        return 1;
    } else {
        return '';
    }
}

function tonetheme_sanitize_hide_meta( $input ) {
    $valid = array(
        'show_all' => __('Show all Post Info','tone'),
        'hide_all' => __('Hide all Post Info','tone'),
        'show_author_date' => __('Author + Date','tone'),
        'show_date_comments' => __('Date + Comments','tone'),
        'show_author_comments' => __('Author + Comments','tone'),
        'show_author' => __('Just Author','tone'),
        'show_date' => __('Just Date','tone')
    );

    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}
function tonetheme_sanitize_nav( $input ) {
    $valid = array(
        'infinite' => __('Infinite Scroll','tone'),
        'page_numbers' => __('Page Numbers','tone')
    );

    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}
function tonetheme_sanitize_subset( $input ) {
    $valid = array(
        'latin' => __('Latin (Default)','tone'),
        'latin-ext' => __('Latin Extended','tone'),
        'cyrillic' => __('Cyrillic','tone')
    );

    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

function tonetheme_sanitize_layout( $input ) {
    $valid = array(
        'fullwidth' => __('Classic (Default)','tone'),
        'sidebar' => __('Classic with Sidebar','tone'),
        'halfwidth' => __('Halfwidth','tone')
    );

    if ( array_key_exists( $input, $valid ) ) {
        return $input;
    } else {
        return '';
    }
}

function tonetheme_sanitize_number( $value ) {
    $value = (int) $value; // Force the value into integer type.
    return ( 0  <= $value ) ? $value : null;
}