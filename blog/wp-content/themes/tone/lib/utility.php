<?php

/* Add Foundation 'active' class for the current menu item
 * From Reverie Framework http://themefortress.com/reverie/
 */

function tonetheme_active_nav_class( $classes, $item ) {
    if ( $item->current == 1 || $item->current_item_ancestor == true ) {
        $classes[] = 'active';
    }
    return $classes;
}
add_filter( 'nav_menu_css_class', 'tonetheme_active_nav_class', 10, 2 );

/*
 * Use the active class of ZURB Foundation on wp_list_pages output.
 * From required+ Foundation http://themes.required.ch
 */
function tonetheme_active_list_pages_class( $input ) {

    $pattern = '/current_page_item/';
    $replace = 'current_page_item active';

    $output = preg_replace( $pattern, $replace, $input );

    return $output;
}
add_filter( 'wp_list_pages', 'tonetheme_active_list_pages_class', 10, 2 );

// External Links from the Post Title
function tonetheme_link_title() {
    global $post;
    $thePostID = $post->ID;
    $post_id = get_post($thePostID);
    $title = $post_id->post_title;
    if ( class_exists( 'RW_Meta_Box' ) ) {
        $link = rwmb_meta( 'tonetheme_titleurl' );
    } else {
        $link = '';
    }
    if ( $link !== '') {
        echo '<a href="'.esc_url($link).'" rel="nofollow" target="_blank" title="'.esc_html($title).'">'.esc_html($title).'<i class="i-link-1">&nbsp;</i></a>';
    }
}

function tonetheme_link_subtitle() {
    global $post;
    $thePostID = $post->ID;
    $post_id = get_post($thePostID);
    $title = $post_id->post_title;
    if ( class_exists( 'RW_Meta_Box' ) ) {
        $link = rwmb_meta( 'tonetheme_titleurl' );
    } else {
        $link = '';
    }
    if ( $link !== '') {

        echo '<a href="'.esc_attr($link).'" rel="nofollow" target="_blank" class="link" title="'.esc_html($title).'">'.esc_attr($link).'</a>';

    }
}

/* Responsive Video */

add_filter( 'embed_oembed_html', 'tonetheme_oembed_filter' );
function tonetheme_oembed_filter( $code ){
    if( (stripos( $code, 'youtube.com' ) !== FALSE) || (stripos( $code, 'vimeo.com' ) !== FALSE ) && stripos( $code, 'iframe' ) !== FALSE )
        $code = '<div class="flex-video widescreen">'.balanceTags($code).'</div>';

    return $code;
}


/* Tag Cloud */

function tonetheme_filter_tag_cloud($args) {
    $args['smallest'] = 1;
    $args['largest'] = 1.618;
    //$args['format'] = 'list';
    $args['unit'] = 'em';
    return $args;
}
add_filter ( 'widget_tag_cloud_args', 'tonetheme_filter_tag_cloud');

// HTML tags in excerpts

function tonetheme_improved_excerpt($text) {
    global $post;
    if ( '' == $text ) {
        $text = get_the_content('');
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]>', ']]&gt;', $text);
        $text = preg_replace('@<script[^>]*?>.*?</script>@si', '', $text);
        $text = strip_tags($text, '');
        $excerpt_length = 20;
        $words = explode(' ', $text, $excerpt_length + 1);
        if (count($words)> $excerpt_length) {
            array_pop($words);
            array_push($words, '...');
            $text = implode(' ', $words);
        }
    }
    return balanceTags($text);
}

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'tonetheme_improved_excerpt');


/* Load More Class */

add_filter('next_posts_link_attributes', 'tonetheme_posts_link_attributes');
add_filter('previous_posts_link_attributes', 'tonetheme_posts_link_attributes');

function tonetheme_posts_link_attributes() {
    return 'class="load-more"';
}

add_filter( 'the_content_more_link', 'tonetheme_modify_read_more_link' );
function tonetheme_modify_read_more_link() {

    return '<span class="more"><a href="' . get_permalink() . '" class="more-link">'.esc_html__('Continue reading', 'tone').'<i class="i-arrow-right"></i></a></span>';

}

/* Excerpts for Pages */

add_action('init', 'tonetheme_page_excerpt_init');
function tonetheme_page_excerpt_init() {
    add_post_type_support( 'page', 'excerpt' );
}

/* Lightbox for images in posts */

add_filter('the_content', 'tonetheme_lighbox');
function tonetheme_lighbox ($content) {

    global $post;

    if ( class_exists( 'RW_Meta_Box' ) ) {
        $wideimg = rwmb_meta( 'tonetheme_wideimg' );
    } else {
        $wideimg = '';
    }
    if ($wideimg) {
        $imgclass = 'wide-img light';
    } else {
        $imgclass = 'light';
    }

    $pattern = "/<a(.*?)href=('|\")(.*?).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>/i";
    $replacement = '<a$1href=$2$3.$4$5 class="'.$imgclass.'">';
    $content = preg_replace($pattern, $replacement, $content);
    return $content;

}