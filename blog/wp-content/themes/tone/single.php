<?php get_header(); ?>

<section id="single" role="main">

    <div class="boxed white page_section_offset">
        <section class="innerbanner">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h1>Blog</h1>
                        <ul class="breadcrumb">
                        <li><a href="<?php echo  get_site_url(); ?>">Home</a></li>
                        <li class="active">Blog</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
         <div class="container">	
           <div class="row">
			<div class="blogsingle">
            <div class="bloglistleft">
			 
            	<?php get_template_part('content'); ?>
              
            </div>
            <div class="blogsidebar">
                <?php get_sidebar(); ?>
            </div>
            </div>
         </div>
        </div>
    </div>

</section>

<?php get_footer(); ?>