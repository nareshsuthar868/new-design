<?php $credit_link = get_theme_mod( 'tonetheme_disable_credit_link' ); 
           $site_url = get_site_url();
           $url =  explode('/',$site_url); 
            array_pop($url); 
           $new_url = implode('/', $url);
           ?>
<div class="boxed">
	  <?php get_template_part( 'inc/nav'); ?>
    <div id="footer" class="footer">

      
		
        <!--<div class="row">
		
            <div class="large-6 columns">

                <ul id="footer-menu" class="secondary-menu horizontal menu">

                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'footer',
                        'container' => false,
                        'depth' => 1,
                        'items_wrap' => '%3$s',
                        'fallback_cb' => ''
                    ) );

                    ?>


                </ul>

           </div>
            <div class="large-6 columns">

               <span class="text-line">

                   <?php $copyright_textbox = get_theme_mod( 'tonetheme_copyright_textbox' );
                   if ($copyright_textbox != '') { ?>

                       <?php echo esc_html($copyright_textbox); ?>

                   <?php } else { ?>

                       <?php esc_html_e('Copyright', 'tone'); ?> &copy; <?php echo esc_attr( date('Y') ); ?> <?php bloginfo('name'); ?><?php if ( !$credit_link && (is_home() || is_front_page () )) {?><?php esc_html_e('. Design by', 'tone'); ?> <a href="http://themeous.net">Themeous</a><?php } ?>

                   <?php } ?>

                </span>

           </div>

        </div>-->
        
        <div class="container hidden-xs">
    <div class="row">
      <div class="footerlogosec">
        <figure class="m_bottom_15"><img src="<?php bloginfo('template_directory'); ?>/img/logo-stick-footer.png" alt="footer logo" /></figure>
        <div class="m_bottom_15">
          <div class="color_light"> Cityfurnish is revolutionizing the furniture industry by providing quality furniture and home appliances on easy monthly rental. With the immense focus on product quality and customer service, we strive to become most preferred name in furniture industry by customer's choice.</div>
        </div>
      </div>
      <div class="footerlink">
        <h5>Help</h5>
        <ul>
            <li><a href="<?php echo $new_url; ?>/pages/contact-us">Contact us</a></li>
            <li><a href="<?php echo $new_url; ?>/pages/how-it-works">How It Works?</a>  </li>
            <li><a href="<?php echo $new_url; ?>/pages/faq">FAQs</a></li>
            <li><a href="<?php echo $new_url; ?>/reviews-testimonials/all">Customer Reviews</a></li>
            <li><a href="<?php echo $new_url; ?>/customerpayment" target="_blank">Customer Payment</a></li>
        </ul>
      </div>
      <div class="footerlink">
        <h5>Information</h5>
        <ul>
                    <li><a href="<?php echo $new_url; ?>/blog/" target="_blank">Blog</a></li>
                    <li><a target="_blank" href="http://vior.in">Clearance Sale</a></li>
                    <li><a href="<?php echo $new_url; ?>/pages/offers">Offers</a></li>
                    <li><a href="<?php echo $new_url; ?>/pages/careers">We are hiring</a></li>
                    <li><a href="<?php echo $new_url; ?>/pages/friends-and-partners">Friends & Partners</a></li>
                </ul>
      </div>
      <div class="footerlink last">
        <h5>POLICIES</h5>
        <ul class="second_font vr_list_type_1 with_links">
                    <li><a href="<?php echo $new_url; ?>/pages/terms-of-use">Terms of use</a></li>
                    <li><a href="<?php echo $new_url; ?>/pages/privacy-policy">Privacy policy</a></li>
                    <li><a href="<?php echo $new_url; ?>/pages/refer-a-friend">Referral Terms of use</a></li>
                    <li><a href="<?php echo $new_url; ?>/pages/rentalagreement">Sample Rental Agreement</a></li>
          </ul>
      </div>
      <div class="quickcontactcol">
      <div class="footerquickcontact">
        <h5 class="m_bottom_15 nw_scheme_color">QUICK CONTACT</h5>
        <ul class="contactquick">
          <li><a href="mailto:hello@cityfurnish.com"><i class="material-icons">mail_outline</i><span>hello@cityfurnish.com</span></a></li>
          <li><a href="tel:8010845000"><i class="material-icons">phone</i><span>8010845000</span></a> </li>
        </ul>
        <ul class="socialicon">
                        <li>
                            <a href="https://www.facebook.com/cityFurnishRental" target="_blank">
                             
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/CityFurnish" target="_blank" class="twitter">
                             
                            </a>
                        </li>
                        <li>
                            <a href="https://plus.google.com/+cityfurnish" target="_blank" class="google">
                               
                            </a>
                        </li>
                        <li>
                            <a href="https://in.pinterest.com/cityfurnish/" target="_blank" class="pintrest">
                               
                            </a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/cityfurnish?trk=biz-companies-cym" target="_blank" class="linkedin">
                            </a>
                        </li>
                    </ul>
      </div>
      </div>
    </div>
  </div>
       
 		
    </div>
    <section class="copyright">
            <div class="container">
              <div class="row">
                <div class="col-sm-6 col-md-6 col-lg-6 hidden-xs">
                  <p>© Copyright 2017 <strong class="nw_theme_color">Cityfurnish</strong>. All Rights Reserved.</p>
                </div>
                 <div class="col-lg-4 col-md-4 col-sm-4 headertopcontact visible-xs"> <a href="mailto:hello@cityfurnish.com"><i class="material-icons">mail_outline</i><span>hello@cityfurnish.com</span></a> <a href="tel:8010845000"><i class="material-icons">phone</i><span>8010845000</span></a> </div>
                 <div class="col-sm-6 col-md-6 col-lg-6 cityright hidden-xs">
                  <p>Soon coming to: Hyderabad and Chennai</p>
                </div>
      	</div>
       
       
    </div>
    <!--back to top-->
  </section>

</div>

</section><!-- Wrapper End -->
 <button class="back_to_top scrollToTop"><i class="material-icons d_inline_m">keyboard_arrow_up</i></button>

</div>
</div>
</div>
<?php wp_footer(); ?>

</body>
</html>