<?php get_header(); ?>

<section id="attachment">

    <div class="boxed white">

        <div class="row">

            <div class="large-12 columns">

            <?php while ( have_posts() ) : the_post(); ?>

                <article id="post-<?php the_ID(); ?>" <?php post_class( 'image-attachment' ); ?>>

                    <div class="entry-wrapper">

                        <header class="entry-header">

                            <h1 class="entry-title"><?php the_title(); ?></h1>


                            <?php if ( ! empty( $post->post_excerpt ) ) { ?>

                                <div class="keynote">
                                    <h2><?php the_excerpt(); ?></h2>
                                </div>

                            <?php } ?>

                            <div class="divider-line"></div>

                        </header><!-- .entry-header -->

                        <div class="entry-attachment">

                            <?php

                            $attachments = array_values( get_children( array( 'post_parent' => $post->post_parent, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => 'ASC', 'orderby' => 'menu_order ID' ) ) );
                            foreach ( $attachments as $k => $attachment ) :
                                if ( $attachment->ID == $post->ID )
                                    break;
                            endforeach;

                            $k++;
                            // If there is more than 1 attachment in a gallery
                            if ( count( $attachments ) > 1 ) :
                                if ( isset( $attachments[ $k ] ) ) :
                                    // get the URL of the next image attachment
                                    $next_attachment_url = get_attachment_link( $attachments[ $k ]->ID );
                                else :
                                    // or get the URL of the first image attachment
                                    $next_attachment_url = get_attachment_link( $attachments[ 0 ]->ID );
                                endif;
                            else :
                                // or, if there's only 1 image, get the URL of the image
                                $next_attachment_url = wp_get_attachment_url();
                            endif;
                            ?>


                            <div class="media-holder">

                                <?php

                                $image_size = apply_filters( 'tonetheme_attachment_size', 'large' );

                                echo wp_get_attachment_image( get_the_ID(), $image_size );
                                ?>

                            </div>

                        </div><!-- .entry-attachment -->

                        <div class="entry-content">

                            <?php the_content(); ?>

                            <footer class="single-bottom">

                                <div class="entry-meta">
                                    <?php
                                    $metadata = wp_get_attachment_metadata(); ?>
                                    <span class="meta-prep meta-prep-date">
                                        <?php esc_html_e('Published:', 'tone'); ?>
                                    </span>

                                    <span class="date">
                                        <time  datetime="<?php echo esc_attr( get_the_date( 'c' ) );?>">
                                            <?php echo esc_html( get_the_date() );?>

                                        </time>
                                    </span>.

                                    <?php esc_html_e('Original size:', 'tone'); ?>
                                    <a href="<?php echo esc_url( wp_get_attachment_url() );?>" title="<?php esc_html_e('Link to full-size image', 'tone'); ?>"><?php echo esc_attr($metadata['width']);?> &times; <?php echo esc_attr($metadata['height']);?></a>. <?php esc_html_e('In:', 'tone'); ?>
                                    <a href="<?php echo esc_url( get_permalink( $post->post_parent ) ); ?>" title="<?php esc_html_e('Return to', 'tone'); ?> <?php echo esc_attr( strip_tags( get_the_title( $post->post_parent ) ) ); ?>" rel="gallery"><?php echo get_the_title( $post->post_parent );?></a>.


                                    <?php edit_post_link( esc_html__( 'Edit', 'tone' ), '<span class="edit-link">', '</span>' ); ?>
                                </div><!-- .entry-meta -->

                            <?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'tone' ), 'after' => '</div>' ) ); ?>

                            </footer>

                        </div>

                        <?php comments_template( '', true ); ?>

                    </div><!--end entry wrapper-->

                </article><!-- #post -->


            <?php endwhile; // end of the loop. ?>

            </div>

        </div>

    </div>

</section>


<?php get_footer(); ?>