<?php while (have_posts()) : the_post(); ?>

    <?php if ( class_exists( 'RW_Meta_Box' ) ) {
            $sidebar = rwmb_meta( 'tonetheme_layout' );
            $post_info = rwmb_meta( 'tonetheme_postinfo' );
            $drop_cap = rwmb_meta( 'tonetheme_dropcap' );
        } else {
            $sidebar = '';
            $post_info = '';
            $drop_cap = '';
        }
     ?>

    <article <?php post_class() ?> id="post-<?php the_ID(); ?>">

        <div class="entry-wrapper<?php if ( has_excerpt() ) { echo ' has-excerpt'; } else { echo ' no-excerpt'; } ?>">

            <div class="boxed">
            	<div class="thumbfeturedetail">
                <div class="detaildate">
					<strong><?php echo get_the_date('d');?></strong><span><?php echo get_the_date('M');?></span>
                </div>
				<?php get_template_part( 'inc/featured' ); ?>
                </div>
                <header class="entry-header">

                    <?php  if ( has_post_format( 'link' ) ) { ?>

                        <h1 class="entry-title">

                            <?php tonetheme_link_title(); ?>

                        </h1>

                        <h2 class="subheader"><?php tonetheme_link_subtitle(); ?></h2>

                    <?php } elseif ( has_post_format( 'aside' ) ) { }

                    else { ?>

                        <h1 class="entry-title">

                            <?php the_title(); ?>

                        </h1>

                    <?php } ?>

                    <?php if( $post->post_excerpt ) {
                        echo '<div class="keynote"><h2>';
                        echo get_the_excerpt();
                        echo '</h2></div>';
                    } else { } ?>

                    <div class="divider-line"></div>

                </header>

            </div>

            

            <div class="boxed narrow">

                <?php //dynamic_sidebar('before-post-horizontal'); ?>


                <div class="entry-content<?php if ($drop_cap) { ?> drop-cap<?php } ?>">

                    <?php the_content(); ?>

                    <?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:&nbsp;&nbsp;', 'tone'), 'link_before' =>'&nbsp;&nbsp;', 'after' => '</p></nav>' )); ?>

                </div>

                <?php if ( is_page() || has_post_format('quote') || has_post_format('status') || has_post_format( 'aside' ) ) { } else {
                    get_template_part( 'inc/meta', 'taxonomy' );
                } ?>

                <?php // dynamic_sidebar('after-post-horizontal'); ?>

                <?php if ( is_page() ) { } else { ?>

                    <?php comments_template( '', true );
					/*	$comments_args = array(
        // change the title of send button 
        'label_submit'=>'Send',
        // change the title of the reply section
        'title_reply'=>'Leave a Reply',
        // remove "Text or HTML to be displayed after the set of comment fields"
        'comment_notes_after' => '',
        // redefine your own textarea (the comment body)
        'comment_field' => '<p class="comment-txt-in"><label for="author">Name <span class="required">*</span></label> <input id="author" name="author" type="text" value="" size="30" maxlength="245" aria-required="true" required="required"></p> <p class="comment-txt-in"><label for="email">Email <span class="required">*</span></label> <input id="email" name="email" type="email" value="" size="30" maxlength="100" aria-describedby="email-notes" aria-required="true" required="required"></p> <p class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label><br /><textarea id="comment" name="comment" aria-required="true"></textarea></p>',
);

comment_form($comments_args);*/
					 ?>
                    

                <?php } ?>

            </div>

        </div><!--enrty-wrapper end-->

    </article>

<?php endwhile; // End the loop ?>

