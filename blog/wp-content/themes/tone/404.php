<?php get_header(); ?>

<!-- Row for main content area -->
<section id="content" role="main">

    <article id="post-0" class="post">

        <div class="boxed">

            <div class="row collapse">

                <div class="large-7 large-centered columns">

                    <div class="entry-wrapper">

                        <header class="entry-header">

                            <h1 class="entry-title"><?php esc_html_e('Page Not Found', 'tone'); ?></h1>

                        </header>

                        <div class="entry-content">
                            <p><?php esc_html_e('The page you are looking for might have been removed, had its name changed, or is temporarily unavailable.', 'tone'); ?></p>

                            <div class="divider-large"></div>

                            <?php get_search_form(); ?>

                        </div><!--end entry content-->

                    </div>

                </div><!--end large col-->

            </div><!--end row-->

        </div>

    </article>

</section>

<?php get_footer();  ?>