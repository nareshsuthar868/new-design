<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|

echo $_SERVER['HTTP_HOST'];

switch ( $_SERVER['HTTP_HOST'] ) {
    case 'cityfurnish.nachmundiye.com':
        $route['default_controller'] = "site/landing";
    break;
    case 'nachmundiye.com':
        $route['default_controller'] = "site/landing";
    default:
        $route['default_controller'] = 'site/landing';
    break;
}

 if (file_exists($uri.'/site/landing.php')){ // load default controller
 echo "yes";
    
}else{
	echo "no";
}
*/


$route['default_controller'] = 'welcome';
//$route['default_controller'] = 'landing';
//$route['(.+)'] = "site/landing/$1";
$route['404_override'] = 'welcome/show_404';

$route['home'] = "site/landing";
$route['admin'] = "admin/adminlogin";
$route['bdadmin'] = "bdadmin/adminlogin";
$route['signup'] = "site/user/signup_form";
$route['login'] = "site/user/login_form";
$route['logout'] = "site/user/logout_user";
$route['forgot-password'] = "site/user/forgot_password_form";
$route['send-confirm-mail'] = "site/user/send_quick_register_mail";
$route['onboarding'] = "site/product/onboarding";
$route['gift-cards'] = "site/giftcard";
$route['cart'] = "site/cart";
$route['checkout/(.+)'] = "site/checkout";
$route['order/(.+)'] = "site/order";

$route['epc_calculator'] = "site/Epc_calculator";
$route['epc_calculator/get_coupan_details/(:any)'] = "site/Epc_calculator/get_coupan_details/$1";
$route['epc_calculator/calculate_ecp'] = "site/Epc_calculator/calculate_ecp";
$route['epc_calculator/get_sub_product_values'] = "site/Epc_calculator/get_sub_product_values";

$route['settings'] = "site/user_settings";
$route['settings/password'] = "site/user_settings/password_settings";
$route['settings/preferences'] = "site/user_settings/preferences_settings";
$route['settings/notifications'] = "site/user_settings/notifications_settings";
$route['purchases'] = "site/user_settings/user_purchases";
//$route['gifts/list'] = "site/user_settings/user_group_gifts";
$route['usersettings'] = "site/user_settings";
$route['orders'] = "site/user_settings/user_orders";
$route['fancyybox/manage'] = "site/user_settings/manage_fancyybox";
$route['settings/shipping'] = "site/user_settings/shipping_settings";
$route['credits'] = "site/user_settings/user_credits";
$route['referrals/(.+)'] = "site/user_settings/user_referrals/$1";
$route['referrals'] = "site/user_settings/user_referrals";
$route['settings/giftcards'] = "site/user_settings/user_giftcards";
$route['things/(.+)/edit'] = "site/product/edit_product_detail/$1";
$route['things/(.+)/edit/(.+)'] = "site/product/edit_product_detail/$1";
$route['things/(.+)/delete'] = "site/product/delete_product/$1";
$route['things/shuffle'] = "site/product/display_product_shuffle";
$route['things/(.+)'] = "site/product/display_product_detail/$1";
$route['user/(.+)/added'] = "site/user/display_user_added";
$route['user/(.+)/lists'] = "site/user/display_user_lists";
$route['user/(.+)/lists/(:num)'] = "site/user/display_user_lists_home";
$route['user/(.+)/lists/(:num)/followers'] = "site/user/display_user_lists_followers";
$route['user/(.+)/lists/(:num)/settings'] = "site/user/edit_user_lists";
$route['user/(.+)/wants'] = "site/user/display_user_wants";
$route['user/(.+)/owns'] = "site/user/display_user_owns";
$route['user/(.+)/follows'] = "site/user/display_user_follow";
$route['user/(.+)/following'] = "site/user/display_user_following/$1";
$route['user/(.+)/followers'] = "site/user/display_user_followers/$1";
$route['user/(.+)/things/(.+)'] = "site/product/display_user_thing/$1";
$route['user/(.+)'] = "site/user/display_user_profile/$1";
$route['user_sign_up'] = "site/user/signup_form";
$route['shopby/(.+)'] = "site/searchShop/search_shopby/$1";
$route['search/(:any)'] = "site/product/search/$1";
//$route['shop'] = "site/shop";
$route['reviews-testimonials/(.+)'] = "site/ideas/search_shopby_things/$1";
$route['add-thing'] = "site/add";
//$route['professionals/(.+)'] = "site/searchProfessionals/search_professionalsby/$1";
//$route['stores'] = "site/seller";

$route['giftguide/list/(.+)/followers'] = "site/searchShop/search_priceby_followers/$1";
$route['colorsby/list/(.+)/followers'] = "site/searchShop/search_colorby_followers/$1";
$route['giftguide/list/(.+)'] = "site/searchShop/search_priceby/$1";
$route['colorsby/list/(.+)'] = "site/searchShop/search_colorby/$1";

$route['fancybox'] = "site/fancybox";
$route['fancybox/(.+)/(.+)'] = "site/fancybox/display_fancybox/$1";

$route['sales/create'] = "site/product/sales_create";
$route['seller-signup'] = "site/user/seller_signup_form";

$route['pages/careers'] = "site/cms/careers";
$route['pages/offers'] = "site/cms/offers";
$route['pages/ex-offers'] = "site/cms/ex_offers";
$route['pages/whyus'] = "site/cms/whyus";
$route['pages/faq'] = "site/cms/faq";
$route['pages/about'] = "site/cms/about";
$route['pages/privacy-policy'] = "site/cms/privacy_policy";
$route['pages/how-it-works'] = "site/cms/how_it_works";
$route['pages/contact-us'] = "site/cms/contactus";
$route['pages/refer-a-friend'] = "site/cms/refer_a_friend";
$route['pages/rentalagreement'] = "site/cms/rental_agreement";
$route['pages/(:num)/(.+)'] = "site/cms/page_by_id";
$route['shop'] = "site/cms/categorylist";
/*$route['mumbai/furniture-rental'] = "site/cms/mumbaifurniture";
$route['delhi/furniture-rental'] = "site/cms/delhifurniture";*/
$route['pages/bulkorder'] = "site/cms/bulkorder";
$route['pages/(.+)'] = "site/cms";

$route['create-brand'] = "site/user/create_brand_form";
$route['customization-request'] = "site/user/custom_request_form";
$route['view-purchase/(.+)'] = "site/user/view_purchase";
$route['view-order/(.+)'] = "site/user/view_order";
$route['order-review/(:num)/(:num)/(.+)'] = "site/user/order_review";
$route['order-review/(:num)'] = "admin/order/order_review";

$route['image-crop/(.+)'] = "site/user/image_crop";
$route['lang/(.+)'] = "site/fancybox/language_change/$1";

$route['notifications'] = "site/notify/display_notifications";

$route['payment-success'] = "admin/commission/display_payment_success";
$route['payment-failed'] = "admin/commission/display_payment_failed";

$route['bookmarklet'] = "site/bookmarklet";
$route['bookmarklets'] = "site/bookmarklet/display_bookmarklet";

$route['invite-friends'] = "site/user/invite_friends";
$route['feedback/(:num)'] = "site/user/display_user_product_feedback/$1";
$route['common/(.+)'] = "site/ajaxhandler/setcity/$1";


$route['t/(.+)'] = "site/product/load_short_url";

$route['documentation'] = "site/user_settings/user_documentation";

/* Operations Dashboard */
$route['operation'] = 'operation/operation/index';
$route['dashboard'] = 'operation/operation/dashboard';
$route['addtickets'] = 'operation/operation/addtickets';
$route['viewtopics'] = 'operation/operation/viewtopics';
$route['edittopics/(.+)'] = 'operation/operation/edittopics/$1';
$route['deletetopic'] = 'operation/operation/deletetopic';
$route['viewtickets'] = 'operation/operation/viewtickets';
$route['changeticketstatus'] = 'operation/operation/changeticketstatus';
$route['addroles'] = 'operation/operation/addroles';
$route['viewadminusers'] = 'operation/operation/viewadminusers';
$route['operationlogout'] = 'operation/operation/operationlogout';
$route['ticketinfo/(.+)'] = 'operation/operation/ticketinfo/$1';
$route['doctorscont'] = 'Doctorscont';
$route['reset-password/(.+)'] = "site/user/reset_password/";

//new added 
$route['payments'] = "site/user_settings/my_payment";
$route['invoices'] = "site/user_settings/my_invoices";
$route['wallet'] = "site/user_settings/user_wallet";
$route['wishlist'] = "site/user_settings/show_wishlist";
$route['invoicepayment/(.+)'] = "site/InvoicePayment";

//offline order
$route['view-purchase-offline/(.+)'] = "site/user/view_purchase_offline";
$route['offlinePayment/(.+)'] = "site/offlinepayment";
$route['handlepayment/(.+)'] = "site/handleofflinepayment";

//View Referral Code
$route['referral'] = 'site/giftcard/show_referral_code';



//View Referral Code
$route['zoho_invoice'] = 'site/zoho/CreateSalesOrderInvoice';
$route['zoho_invoicetest'] = 'site/Zoho_test/CreateAllZohoInvoices';

$route['getallfailedOrders'] = 'crone/getFailedOrders';
$route['generateWeeklyReport'] = 'crone/create_csv_string';


$route['send_test_email'] = 'site/zoho_test/check_test_email';
$route['accept_zoho_so_update'] = 'site/zoho_test/get_so_params';

 



	if(strpos($_SERVER['REQUEST_URI'], 'admin/') === false && strpos($_SERVER['REQUEST_URI'], 'customerpayment/') === false && strpos($_SERVER['REQUEST_URI'], 'CustomerPayment/') === false && strpos($_SERVER['REQUEST_URI'], 'crone/') === false)  {
		$route['([^/]+)/(:any)'] = "site/searchShop/search_shopby/$1";	
	}

/*Admin Routes
Added by Saurabh Singh
*/

$route['admin/banner/edit_banner/(:any)'] = "admin/banner/add_banner/$1";
$route['admin/customer_feedback/edit_customer_feedback/(:any)'] = "admin/customer_feedback/add_customer_feedback/$1";
$route['admin/completed_project/edit_completed_project/(:any)'] = "admin/completed_project/add_completed_project/$1";

/* End of file routes.php */
/* Location: ./application/config/routes.php */