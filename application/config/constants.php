<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');

/*
|--------------------------------------------------------------------------
| Table Constants
|--------------------------------------------------------------------------
|
*/
define('TBL_PREF',						'fc_');

define('ADMIN',							TBL_PREF.'admin');
define('ADMIN_SETTINGS',				TBL_PREF.'admin_settings');
define('SUBADMIN',						TBL_PREF.'subadmin');
define('USERS',							TBL_PREF.'users');
define('CATEGORY',						TBL_PREF.'category');
define('COUPONCARDS',					TBL_PREF.'couponcards');
define('GIFTCARDS',						TBL_PREF.'giftcards');
define('GIFTCARDS_SETTINGS',			TBL_PREF.'giftcards_settings');
define('GIFTCARDS_TEMP',				TBL_PREF.'giftcards_temp');
define('SUBSCRIBERS_LIST',				TBL_PREF.'subscribers_list');
define('BANNER',				        TBL_PREF.'banner');
define('CUSTOMER_REVIEW',				TBL_PREF.'customer_review');
define('COMPLETED_PROJECT',				TBL_PREF.'completed_project');
define('NEWSLETTER',					TBL_PREF.'newsletter');
define('CMS',							TBL_PREF.'cms');
define('PRODUCT',						TBL_PREF.'product');
define('PRODUCT_CATEGORY',				TBL_PREF.'product_category');
define('LOCATIONS',						TBL_PREF.'location');
define('PAYMENT_GATEWAY',				TBL_PREF.'payment_gateway');
define('STATE_TAX',						TBL_PREF.'state_tax');
define('ATTRIBUTE',						TBL_PREF.'attribute');
define('PRODUCT_LIKES',					TBL_PREF.'product_likes');
define('LANGUAGES',						TBL_PREF.'languages');
define('SHOPPING_CART',					TBL_PREF.'shopping_carts');
define('PAYMENT',						TBL_PREF.'payment');
define('SHIPPING_ADDRESS',				TBL_PREF.'shipping_address');
define('COUNTRY_LIST',		 			TBL_PREF.'country');
define('USER_ACTIVITY',		 			TBL_PREF.'user_activity');
define('LISTS_DETAILS',		 			TBL_PREF.'lists');
define('WANTS_DETAILS',		 			TBL_PREF.'wants');
define('LIST_VALUES',		 			TBL_PREF.'list_values');
define('FANCYYBOX',		 				TBL_PREF.'fancybox');
define('FANCYYBOX_TEMP', 				TBL_PREF.'fancybox_temp');
define('FANCYYBOX_USES', 				TBL_PREF.'fancybox_uses');
define('USER_PRODUCTS', 				TBL_PREF.'user_product');
define('PRODUCT_COMMENTS', 				TBL_PREF.'product_comments');
define('NOTIFICATIONS', 				TBL_PREF.'notifications');
define('VENDOR_PAYMENT', 				TBL_PREF.'vendor_payment_table');
define('REVIEW_COMMENTS', 				TBL_PREF.'review_comments');
define('BANNER_CATEGORY', 				TBL_PREF.'banner_category');
define('PRICING', 						TBL_PREF.'pricing');
define('LAYOUT', 						TBL_PREF.'layout');
define('CONTROLMGMT', 					TBL_PREF.'control');
define('FOOTER', 						TBL_PREF.'footer');
define('UPLOAD_MAILS', 					TBL_PREF.'upload_mails');
define('THEME_LAYOUT', 					TBL_PREF.'theme_layout');
define('PRODUCT_ATTRIBUTE', 			TBL_PREF.'product_attribute');
define('SUBPRODUCT', 					TBL_PREF.'subproducts');
define('PRODUCT_FEEDBACK', 					TBL_PREF.'product_feedback');
define('SHORTURL',	 					TBL_PREF.'short_url');
define('UPLOAD_REQ',	 				TBL_PREF.'upload_requests');
define('CONTACTSELLER', 				TBL_PREF.'contact_seller');
define('CUSTOM', 						TBL_PREF.'custom_request');
define('SELLER_LOCATION', 						TBL_PREF.'seller_location');
define('PINCODE', 						TBL_PREF.'pincodes');
define('DEFAULT_LAT', 						'28.613939');
define('DEFAULT_LONG', 						'77.209021');
define('CUSTOMER_PAYMENT',						TBL_PREF.'customer_payment');
define('CP_CUSTOMER_PAYMENT',						TBL_PREF.'cp_customer_payment');
define('VOUCHER',                        TBL_PREF.'voucher_settings');
define('CITY_WEARHOUSE_CODE',           TBL_PREF.'city_wearhouse_code');
define('REFERRAL_SETTING',              TBL_PREF.'referral_setting');
define('REFERRAL_CODE',                 TBL_PREF.'referral_code');
define('LEAD_REQUEST',                 'us_fc_lead_request');
define('SI_DISCOUNT_SETTING',                 TBL_PREF.'si_discount_setting');
define('ZOHO_LOGS', 'fc_zoho_logs');
define('PAYOUT',                 'fc_payout');
define('BULK_ORDER', 'fc_bulk_order');
define('OFFICE_FURNITURE_ORDERS', 		TBL_PREF.'office_furniture_orders');

define('CITY_QUANTITY', 			    TBL_PREF.'city_product_quantity');
define('ORDER_NOTIFICATION',  			TBL_PREF.'order_status_notification');
define('FILTERS', 						TBL_PREF.'filters');
define('APPLIED_FILTERS', 				TBL_PREF.'product_applied_filters');
define('SITE_OFFERS', 				        TBL_PREF.'site_offers');
define('CASE_SETTINGS', 				TBL_PREF.'zohocase_oauthsettings');
define('OFFLINE_ORDERS', 				TBL_PREF.'offline_orders_master');
define('INVOICE_PAYMENT',            TBL_PREF.'invoice_payment');



//Wallet Tables 
define('WALLET_TRAN', 					TBL_PREF.'wallet_transaction');
define('MY_WALLET', 					TBL_PREF.'my_wallet');

//Crif Table
define('CRIF_SCORE', 					TBL_PREF.'crif_score');
define('CRIF_REQUEST',					TBL_PREF.'cibil_request');
define('CIBIL_DOCUMENTS', 				TBL_PREF.'cibil_slots');
define('CIBIL_REQUIRE_DOCS', 			TBL_PREF.'cibil_require_docs');
define('USER_UPLOADED_DOCS', 			TBL_PREF.'user_uploaded_docs');






///BD ADMIN 
define('PARTNER_USERS', 	            TBL_PREF.'partner_users');
define('BD_ADMIN', 	            		TBL_PREF.'bdadmin');
define('BD_SUBADMIN',					TBL_PREF.'bdsubadmin');
define('BD_LEADS',						TBL_PREF.'bd_leads');
define('BD_LEAD_COMMISSION', 			TBL_PREF.'bd_lead_commissions');
define('BD_CMS', 						TBL_PREF.'bd_cms');
define('BD_INVITE_CODE',				TBL_PREF.'bd_invite_code_relation');
define('BD_OFFERS', 					TBL_PREF.'bd_offers');
define('BD_NOTIFICATION', 				TBL_PREF.'bd_notification');
define('APP_OTP',  						TBL_PREF.'app_otp');
define('COMISSIONS',					TBL_PREF.'comission_list');




// Operations Table
define('TICKETS_TOPIC', TBL_PREF.'tickets_topic');
define('TICKETS', TBL_PREF.'tickets');
/*
|--------------------------------------------------------------------------
| Path Constants
|--------------------------------------------------------------------------
|
*/

define('CATEGORY_PATH',					'images/category/');
define('GIFTPATH', 						'images/giftcards/');
define('PRODUCTPATH', 					'images/product/');
define('FANCYBOXPATH', 					'images/fancyybox/');
define('FOOTERPATH', 					'images/footer/');

/* URL Constants */
define('BASE_URL', 'http://localhost/cityfurnish_newdesign/');
// define('CDN_URL', 'https://d1eohs8f9n2nha.cloudfront.net/'); //CDN URL: https://d1eohs8f9n2nha.cloudfront.net/
// define('CDN_URL', 'http://45.113.122.221/');
define('CDN_URL', 'https://d3juy0zp6vqec8.cloudfront.net/');

/* Case Creation in Zoho CRM constants */
define('CASE_OWNER','hello@cityfurnish.com'); //vinit.jain@cityfurnish.com
define('CRM_AUTH_TOKEN','8503c9b571a8e7abd1d29ca1541c7cb8');
define('CRM_SCOPE','crmapi');
define('CASE_PDF_FILE_PATH','/home/cityfism/public_html/uploaded/');
/* CSV File PAth */
define('CSV_FILE_PATH',APPPATH.'../uploaded/');

/* Techprocess constants */
define('TECHPROCESS_KEY','9226713999HXSTML');//UAT - 9411667838COBPFY, Test on Live - 9226713999HXSTML
define('TECHPROCESS_IV','2075159052LEHIYW');//UAT - 5195814397UERNCG, Test on Live - 2075159052LEHIYW
define('TECHPROCESS_REQUEST_TYPE','T');
define('TECHPROCESS_MERCHANT_CODE','L88097');//UAT - T142485, Test on Live - T88097
//define('TECHPROCESS_REQUEST_DETAIL','Test_1.0_0.0');
define('TECHPROCESS_S2S_RETURN_URL','https://tpslvksrv6046/LoginModule/Test.jsp');
define('TECHPROCESS_LOCATOR_URL','https://www.tpsl-india.in/PaymentGateway/TransactionDetailsNew.wsdl');//UAT - https://www.tekprocess.co.in/PaymentGateway/TransactionDetailsNew.wsdl
define('TECHPROCESS_RETURN_URL','https://cityfurnish.com/customerpayment/techProcessPayment');//http://nachmundiye.com/customerpayment/techProcessPayment
define('TECHPROCESS_CURRENCY_TYPE','INR');
define('TECHPROCESS_SCHEME_CODE','MAGN'); //UAT - Test, Test on Live - MAGN
define('FINANCE_TEAM_EMAIL','accounts@cityfurnish.com');//'accounts@cityfurnish.com'

/* Citruspay constants for One Time Payment */
define('CITRUS_RETURN_URL','https://cityfurnish.com/customerpayment/cpresponse');//Staging - https://cityfurnish.nachmundiye.com/customerpayment/cpresponse
define('CITRUS_CURRENCY_TYPE','INR');
define('CITRUS_VANITY_URL','cityfurnish'); //staging: 2ongw29r6s
define('CITRUS_SECRET_KEY','1a0c8df563972ec7f16e3c836b6b4a2026fef9c0'); //staging: 35e33b7647135f135394257e2bad3d62cb98e57e

/* Zoho Books Credentials */ 
define('BOOKS_AUTH_TOKEN', '99fbdb10bda4528751bd87ae29437772');
define('BOOKS_ORGANIZATION_ID', '82487023');

/* GST NUMBER */
define('GST_NUM', '06AAJCM9442J1Z2');

// define('CASHGRMA_URL', 'https://payout-gamma.cashfree.com');
define('CASHGRMA_URL', 'https://payout-api.cashfree.com');


define('SMS_KEY','uBM6NMuIJFRj3qEQ8Jqx%2b2Aj%2b0mLrg5fe2zZ83keNuw%3d'); //staging: 35e33b7647135f135394257e2bad3d62cb98e57e
define('SMS_API_KAY', 'AIzaSyCgykMjkZ3M4Jt_oEPvhCBsIdtWCRYml3o');
/* End of file constants.php */
/* Location: ./application/config/constants.php */