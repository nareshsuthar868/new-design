<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
  function send_sms($fields)
  {
    $curl = curl_init();
    $endpoint = 'http://www.myvaluefirst.com/smpp/sendsms';
    $url = $endpoint . '?' . http_build_query($fields);
   
    curl_setopt($curl, CURLOPT_URL,  $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

      $response = curl_exec($curl);
        
        $err = curl_error($curl);
        curl_close($curl);
        
         if ($err) {
           echo json_encode('error');
         } else {
           echo json_encode('success');
         }
  }
