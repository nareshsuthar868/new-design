<?php

function get_customer_review($status = null){
    $ci =& get_instance();
    $ci->load->model('customer_feedback_model');
    if(!empty($status)){
        $get_customer_review = $ci->customer_feedback_model->get_all_details(CUSTOMER_REVIEW, ['status' => $status])->result();
    }else{
        $get_customer_review = $ci->customer_feedback_model->get_all_details(CUSTOMER_REVIEW, ['status' => 'Publish'])->result();
    }
    return $get_customer_review;
}