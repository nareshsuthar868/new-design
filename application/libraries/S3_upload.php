<?php

/**
 * Amazon S3 Upload PHP class
 *
 * @version 0.1
 */
class S3_upload {

	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->library('s3');

		$this->CI->config->load('s3', TRUE);
		$s3_config = $this->CI->config->item('s3');
		$this->bucket_name = $s3_config['bucket_name'];
		$this->folder_name = $s3_config['folder_name'];
		$this->s3_url = $s3_config['s3_url'];
	}

    
	function upload_file($fileName = null, $uploadPath = 'images/', $allowedTypes = '*', $newFileName = "", $isMultiple = false)
	{
		// Set default response        
        $response = array();
        $response['success'] = false;
        $response['errors'] = "";
        $response['upload_data'] = array();

        //Check for filename
        if (!isset($fileName) || empty($_FILES[$fileName]['name'])) {
            $response['errors'] = "Please upload file.";
            return $response;
        }
        $file_ext = pathinfo($_FILES[$fileName]["name"], PATHINFO_EXTENSION);
        $randomName = $newFileName != "" ? $newFileName : time() . rand(111, 999).'.'.$file_ext;           

        $uploadError = false;
        if ($isMultiple) {
            $isUpload = $this->do_multi_upload($fileName, $uploadPath, $randomName);
            $response['upload_data'] = $isUpload;
        } else {
            try {     
                $saved = $this->CI->s3->putObjectFile(
                    $_FILES[$fileName]['tmp_name'],
                    $this->bucket_name,
                    $uploadPath.'/'.$randomName,
                    S3::ACL_PUBLIC_READ,
                    array()
                );
            } catch (S3Exception $e) {
                $uploadError = true;
            } catch (Exception $e) {
                $uploadError = true;
            }
            if ($uploadError) {
                $response['errors'] = 'Image upload error';
            } else {
                $response['success'] = true;
                $response['upload_data']['file_name'] = $randomName;
            }
        }        
        return $response;
	}

	public function do_upload_multiple_files($fieldName, $uploadPath, $randomName, $newFileName = "")
    {
        $response = array();
        $files = $_FILES;
        $cpt = count($_FILES[$fieldName]['name']);

        $uploadError = false;
        for ($i = 0; $i < $cpt; $i++) {
            try {
                $uplodfilename = $files[$fieldName]['tmp_name'][$i];    
                $file_ext = pathinfo($_FILES[$fieldName]["name"][$i], PATHINFO_EXTENSION);
                $randomName = $newFileName != "" ? $newFileName : time() . rand(111, 999).'.'.$file_ext;  
                $saved = $this->CI->s3->putObjectFile(
                    $uplodfilename,
                    $this->bucket_name,
                    $uploadPath.'/'.$fieldName.'/'.$randomName,
                    S3::ACL_PUBLIC_READ,
                    array()
                );
            } catch (S3Exception $e) {
                $uploadError = true;
            } catch (Exception $e) {
                $uploadError = true;
            }            
            
            if ($uploadError) {
                $response['errors'] = 'Image upload error';
            } else {
                $response['success'] = true;
                $response['upload_data']['file_name'][$i] = $randomName;
            }
        }
        // print_r($response);exit;
        return $response;
    }

}