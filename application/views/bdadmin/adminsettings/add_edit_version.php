<?php
$this->load->view('bdadmin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6><?php echo $heading; ?></h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'adduser_form', 'enctype' => 'multipart/form-data');
						echo form_open_multipart('bdadmin/setting/add_update_appversions',$attributes) 
					?>
	 						<ul>
	 							<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">App Versions</label>
										<div class="form_input">
											<input name="app_v" style=" width:295px" id="app_v"  type="text" tabindex="1" class="required tipTop" title="Please enter" placeholder="App Verison"
											 value="<?php if(!empty($app_version)) { echo $app_version->app_v; } ?> " />
										</div>
									</div>
								</li>
								<?php if(!empty($app_version)) { ?>
								<input type="hidden" value="<?php echo $app_version->id; ?>" name="id">
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">Force Update</label>
										<div class="form_input">
											<select name="force_update">
												<option value="Yes" <?php if($app_version->force_update == 'Yes') { echo "selected"; } ?> >Yes</option>
												<option value="No" <?php if($app_version->force_update == 'No') { echo "selected"; } ?>>No</option>
											</select>
										</div>
									</div>
								</li>
							<?php } else{ ?>
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">Force Update</label>
										<div class="form_input">
											<select name="force_update">
												<option value="Yes">Yes</option>
												<option value="No">No</option>
											</select>
										</div>
									</div>
								</li>
							<?php }?>

								<li>
								<div class="form_grid_12">
									<div class="form_input">
										<button type="submit" class="btn_small btn_blue" tabindex="4"><span><?php echo $button_text; ?></span></button>
									</div>
								</div>
								</li>
							</ul>
						</form>
						<img src="">
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('bdadmin/templates/footer.php');
?>