<?php
$this->load->view('bdadmin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Comission Rates</h6>
							<div style="float: right;line-height:40px;padding:0px 10px;height:39px;">
								<div class="btn_30_light" style="height: 29px;">
									<a href="javascript:void(0)" onclick="return addNewComission();" class="tipTop" title="Select any checkbox and click here to active records"><span class="icon accept_co"></span><span class="btn_link">Add Comission</span></a>
								</div>
							</div>
					</div>
					<div class="widget_content">
						<table  class="display data_editable">
							<thead>
								<th>Tenure</th>
								<th>Rates</th>
								<th>Action</th>
							</thead>
							<tbody id="comission_tbody">
								<?php
								foreach ($comission_list->result() as $key => $value) {?>
									<tr id="static_tenure_<?php echo $value->id ?>">
										<td><?php echo $value->tenure; ?></td>
										<td><?php echo $value->comission; ?>%</td>
										<td>
											<?php $tenure = explode(' ',  $value->tenure); ?>
											<button onclick="showComssionEditable(<?php echo $value->id; ?>,<?php echo  $tenure[0]; ?>,<?php echo $value->comission; ?>)" >Edit</button>
											<button onclick="deleteComissions(<?php echo $value->id; ?>)">Delete</button>
										</td>
									</tr>

								 <?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('bdadmin/templates/footer.php');
?>