<?php
$this->load->view('bdadmin/templates/header.php');

extract($bdprivileges);
?>
  
    <div class="switch_bar">
		<ul>
			<!--<li>
			<a href="#"><span class="stats_icon current_work_sl"></span><span class="label">Analytics</span></a>
			</li>-->
			<li>
				<a href="bdadmin/users/display_user_list" ><span class="stats_icon user_sl"><span class="alert_notify orange"><?php echo $totalCampusUserCounts;?></span></span><span class="label">Campus</span></a>
			</li>
			<li>
				<a href="bdadmin/users/display_user_list" ><span class="stats_icon user_sl"><span class="alert_notify orange"><?php echo $totalParnterUserCounts;?></span></span><span class="label">Channel</span></a>
			</li>
			<li>
				<a href="bdadmin/bdusers/display_bd_users" ><span class="stats_icon user_sl"><span class="alert_notify orange"><?php echo $totalBDUserCounts;?></span></span><span class="label">BD</span></a>
			</li>


			<li><a href="bdadmin/commission/show_commissions"><span class="stats_icon bank_sl"></span><span class="label">Commissions</span></a></li>

						
			<li><a href="bdadmin/setting/show_comission_rates"><span class="stats_icon config_sl"></span><span class="label">Settings</span></a></li>           
		</ul>
	</div>
	<div id="content">
		<div class="grid_container">
			
			<span class="clear"></span>
			<div class="social_activities">
				<div class="activities_s">
					<div class="block_label">
						Total Leads<span><?php echo $getTotalLeadCount;?></span>
					</div>
				</div>
				<div class="activities_s">
					<div class="block_label">
						Total Pending Leads<span><?php echo $getTotalPendingLeadCount;?></span>
					</div>
				</div>
				<div class="activities_s">
					<div class="block_label">
						Total Converted Leads<span><?php echo $getTotalCovertedLeadCount;?></span>
					</div>
				</div>
				<div class="activities_s">
					<div class="block_label">
						Total Paid Commissions<span>  &#x20a8; <?php echo $getTotalPaidCommissions ;?></span>
					</div>
					<!--<span class="badge_icon comment_sl"></span>-->
				</div>
				<div class="activities_s">
					<div class="block_label">
						Total Pending Commissions<span>  &#x20a8; <?php echo $getTotalPendingCommissions;?></span>
					</div> 
					<!--<span class="badge_icon bank_sl"></span>-->
				</div>
			</div>
			<!--<div class="grid_12">
				<div class="widget_wrap collapsible_widget">
					<div class="widget_top active">
						<span class="h_icon"></span>
						<h6>Active Collapsible Widget</h6>
					</div>
					
				</div>
			</div>-->
			<div class="grid_6">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon graph"></span>
						<h6>Users</h6>
					</div>
					<div class="widget_content">
						<div class="stat_block">
							<h4>Users Count <?php echo $totalUserCounts;?></h4>
							<table>
							<tbody>
							<tr>
								<td>
									Today
								</td>
								<td>
									<?php echo $todayUserCounts;?>
								</td>
								<!-- <td class="min_chart">
									<span class="bar">20,30,50,200,250,280,350</span>
								</td>-->
							</tr>
							<tr>
								<td>
									This Month
								</td>
								<td>
									<?php echo $getThisMonthCount;?>
								</td>
								<!-- <td class="min_chart">
									<span class="line">20,30,50,200,250,280,350</span>
								</td>-->
							</tr>
							<tr>
								<td>
									This Year
								</td>
								<td>
									<?php echo $getLastYearCount;?>
								</td>
								<!-- <td class="min_chart">
									<span class="line">20,30,50,200,250,280,350</span>
								</td>-->
							</tr>
							</tbody>
							</table>
							<!--<div class="stat_chart">
								<div class="pie_chart">
									<span class="inner_circle">1/1.5</span>
									<span class="pie">1/1.5</span>
								</div>
								<div class="chart_label">
									<ul>
										<li><span class="new_visits"></span>New Visitors: 7000</li>
										<li><span class="unique_visits"></span>Unique Visitors: 3000</li>
									</ul>
								</div>
							</div>-->
						</div>
					</div>
				</div>
			</div>
			<div class="grid_6">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon users"></span>
						<h6>Recent Users</h6>
					</div>
					<div class="widget_content">
						<div class="user_list">
							
                            <?php foreach($getRecentUsersList as $userList) { ?>
							
							<div class="user_block">
								<div class="info_block">
									<div class="widget_thumb">
                                    <?php if($userList['profile_pic'] != '') {?>
                                   		 <img src="images/partner_app/profile_pic/<?php echo $userList['profile_pic'];?> " width="40" height="40" alt="user">
                                      <?php } else { ?>
										<img src="images/user-thumb1.png" width="40" height="40" alt="user">
                                      <?php } ?>
									</div>
									<ul class="list_info">
										<?php if($userList['user_type'] == 'BD Executive'){ ?>
										<li><span>Name: <i><a href="bdadmin/bdusers/view_user/<?php echo $userList['id'];?>">
										<?php } else{ ?>
										<li><span>Name: <i><a href="bdadmin/users/view_user/<?php echo $userList['id'];?>">
										<?php } echo stripslashes($userList['full_name']); ?></a></i></span></li>
										<li> Type: <?php echo $userList['user_type']; ?></span></li>
										<li> Date: <?php echo $userList['created_date']; ?></span></li>
										<!-- <li><span>User Type: Paid, <i>Package Name:</i><b>Gold</b></span></li> -->
									</ul>
								</div>
								<ul class="action_list">
									<?php if($userList['user_type'] == 'BD Executive'){ ?>
										<li><a class="p_edit" href="bdadmin/bdusers/edit_user_form/<?php echo $userList['id'];?>";>Edit</a></li>
										<li><a class="p_del" href="javascript:confirm_delete('bdadmin/bdusers/delete_user/<?php echo $userList['id'];?>')">Delete</a></li>
									<?php } else{ ?>
										<li><a class="p_edit" href="bdadmin/users/edit_user_form/<?php echo $userList['id'];?>";>Edit</a></li>
										<li><a class="p_del" href="javascript:confirm_delete('bdadmin/users/delete_user/<?php echo $userList['id'];?>')">Delete</a></li>
									<?php }?>
								</ul>
							</div>
                            <?php } ?>
                            
                            
						</div>
					</div>
				</div>
			</div>

      
		
			<span class="clear"></span>
			<div class="grid_6" style="margin-top: -219px;">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon shopping_cart_3"></span>
						<h6>Recent Leads</h6>
					</div>
					<div class="widget_content">
						<table class="wtbl_list">
						<thead>
						<tr>
							<th>
								Lead Owner
							</th>
							<th>
								Full Name
							</th>
							<th>
								 Email
							</th>
							<th>
								 Phone
							</th>
						</tr>
						</thead>
						<tbody>
                        
                         <?php 
						 $orderIndex = 0;
						 foreach($getOrderDetails as $orderdetails) { ?>
						<tr class="<?php if($orderIndex == 0 || $orderIndex==2) echo 'tr_even';else echo 'tr_odd'; ?>">
							<td class="noborder_b round_l">
								 <?php echo $orderdetails['full_name'].'<br>('.$orderdetails['owner_email'].')'; ?>
							</td>
							<td class="noborder_b">
								<span><?php echo $orderdetails['first_name'].' '.$orderdetails['last_name']; ?></span>
							</td>
							<td class="noborder_b">
								<span><?php echo $orderdetails['email']; ?></span>
							</td>
							<td class="noborder_b round_r">
								 <?php echo $orderdetails['phone_no']; ?>
							</td>
						</tr>
                        
                        <?php $orderIndex++;
						} ?>
                        
						</tbody>
						</table>
					</div>
				</div>
			</div>

			<span class="clear"></span>
		</div>
		<span class="clear"></span>
	</div>
	
</div>
<?php 
$this->load->view('bdadmin/templates/footer.php');
?>