<?php
$this->load->view('bdadmin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6><?php echo $heading; ?></h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'adduser_form', 'enctype' => 'multipart/form-data');
						echo form_open_multipart('bdadmin/setting/update_client_response_status',$attributes) 
					?>
	 						<ul>
	 							<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">Status</label>
										<div class="form_input">
											<input name="status" style=" width:295px" id="discount_label"  type="text" tabindex="1" class="tipTop" title="Please enter"  value="<?php echo $status_details->row()->status; ?>" />
										</div>
									</div>
								</li>
	 							<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">Status Color </label>
										<div class="form_input">
											<input name="color_code" style=" width:295px" id="price"  type="color" tabindex="1" class="tipTop" title="Please enter price" value="<?php echo $status_details->row()->color_code; ?>"/>
										</div>
									</div>
								</li>
								<li>
								<input type="hidden" name="id" value="<?php echo $status_details->row()->id; ?>">
									<div class="form_grid_12">
										<div class="form_input">
											<button type="submit" class="btn_small btn_blue" tabindex="4"><span>Update</span></button>
										</div>
									</div>
								</li>
							</ul>
						</form>
						<img src="">
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('bdadmin/templates/footer.php');
?>