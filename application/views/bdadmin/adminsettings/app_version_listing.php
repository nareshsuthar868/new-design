<?php
$this->load->view('bdadmin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>App Versions</h6>
							<!-- <div style="float: right;line-height:40px;padding:0px 10px;height:39px;">
								<div class="btn_30_light" style="height: 29px;">
									<a href="bdadmin/setting/add_version" class="tipTop" ><span class="icon accept_co"></span><span class="btn_link">
										Add New
									</span></a>
								</div>
							</div> -->
					</div>
					<div class="widget_content">
						<table  class="display data_editable">
							<thead>
								<th>APP Version</th>
								<th>Force Update</th>
								<th>Added On</th>
								<th>Action</th>
							</thead>
							<tbody id="comission_tbody">
								<?php
								foreach ($version_list->result() as $key => $value) {?>
									<tr id="static_tenure_<?php echo $value->id ?>">
										<td><?php echo $value->app_v; ?></td>
										<td><?php echo $value->force_update; ?></td>
										<td><?php echo $value->created_date; ?></td>
										<td>
											<span><a class="action-icons c-edit" href="bdadmin/setting/edit_app_version/<?php echo $value->id;?>" title="Edit">Edit</a></span>
										</td>
									</tr>

								 <?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('bdadmin/templates/footer.php');
?>