<?php
$this->load->view('bdadmin/templates/header.php');
extract($bdprivileges);
?>

<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('bdadmin/users/change_user_status_global',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>

					</div>
					<div class="widget_content">
					    <div class="form_grid_12">
    					    <div class="form_input" style="margin-top: 10px;margin-bottom: 10px;">
								<label>Filter Type</label>
								<select class="large tipTop" onchange="getfilters(this)" id="filter_type">
									<option disabled selected value="null">Select Status</option>
									<option value="rm_name">RM Name</option>
									<option value="rm_email">RM Email</option>
									<option value="no_rm">Without RM</option>
								</select>
							</div>
							<div id="filter_div_box" style="margin-top: 10px;margin-bottom: 10px;">
    								
							</div>
						</div>
						<table class="display sort-table" id="userListing">
                            <thead>
                                <tr>
	                            	<!-- <th class="center">
										<input name="checkbox_id[]" type="checkbox" value="on" class="selectall">
									</th> -->
	                                <th>Full Name</th>
	                                <th>Email</th>
									<th>RM Name</th>
									<th>RM Email</th>
	                                <th>Thumbnail</th>
	                                <th>User Type</th>
	                                <th>City</th>
									<th>Submission date</th>
									<!--<th>Approval date</th>-->
	                                <th>Last Login Date</th>
									<th>Status</th>
	                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                        	</tbody>
                          	<tfoot>
                            	<tr>
	                            	<!-- <th class="center">
										<input name="checkbox_id[]"  type="checkbox" value="on" class="selectall12">
									</th> -->
	                                <th>Full Name</th>
	                                <th>Email</th>
									<th>RM Name</th>
									<th>RM Email</th>
	                                <th>Thumbnail</th>
	                                <th>User Type</th>
	                                <th>City</th>
									<th>Submission date</th>
									<!--<th>Approval date</th>-->
	                                <th>Last Login Date</th>
									<th>Status</th>
	                                <th>Action</th>
	                            </tr>
	                        </tfoot>
                        </table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<script type="text/javascript">
$(function(){
	// add multiple select / deselect functionality
	$(".selectall").click(function () {
		  $('.case').attr('checked', this.checked);
		  $('.selectall12').attr('checked', this.checked);
	});
	$(".selectall12").click(function () {
		  $('.case').attr('checked', this.checked);
		   $('.selectall').attr('checked', this.checked);
	});
});

function change_all(){
	if($(".case").length == $(".case:checked").length) {
		$(".selectall").attr("checked", "checked");
		$(".selectall12").attr("checked", "checked");
	} else {
		$(".selectall").removeAttr("checked");
		$(".selectall12").removeAttr("checked", "checked");
	}
}
</script>
<script type="text/javascript">
$(function (){
		get_userlisting('no');

 		$(document).on('change', ".pincode", function () {
 			$('#userListing').DataTable().destroy();
        	get_userlisting('yes');
    	});

 
    

});

   function get_userlisting(is_filter_applied,no_rm = null){
	    $('#userListing').DataTable({
	        processing:  true,
	        serverSide: true,
	       	"ajax": {
			            "url": "<?php echo base_url(); ?>bdadmin/users/Get_user_data",
			        	"data":{"is_filter_applied":is_filter_applied, "filter_type":$('#filter_type').val(), "filter_value":escape($('#filter_value').val()),"no_rm":no_rm},
			            "type": "POST"
			        },
	        "deferRender": true,
	        "pagingType": "full_numbers",
	        responsive: true,
	        // order: [6,'asc'],
	        columns: [
	            { "data": "full_name" },
				{ "data": "email" },
				{ "data": "executer_name" },
				{ "data": "executer_email" },
	            { "data": "profile_pic" },
	            { "data": "user_type" },
				{ "data": "city"},
				null,
				// { "data": "created"},
				{ "data": "last_login_date"},
	            { "data": "user_status"},
				{ "data": "Action"},
	        ],
	        columnDefs: [
	          	{
					orderable: false, targets: [4],
	                "render": function ( data, type, full, meta ) {
	                    var base_url =  '<?= BASE_URL?>';
	                    var image_url = base_url + "images/partner_app/profile_pic/";
	                    var alt_image = image_url + "default-user.png";
	                    if(data == '' || data == null){
	                        var url = alt_image;
	                    }
	                    else{
	                        var url = image_url + data;
	                    }
	                    return '<div class="widget_thumb" style="margin-left: 15px;"><img width="40px" height="40px" src=\'' + url + '\'></div>'
	                }
				},
				{
					orderable: true, targets: [7],
	                "render": function ( data, type, full, meta ) {
						if(full.user_status == "Pending")
						{return '--';}
						else
						{return full.created_date;}
	                }
	            },
	            {
					orderable: true, targets: [9],
	                "render": function ( data, type, full, meta ) {
	                	var mode = (data == 'Pending')?'0':'1';
	                    if(data == 'Pending'){
	                        var button = '<a title="Click to approve" class="tip_top" href="javascript:void(0);" onclick="Approvepartneruser('+  full.id +',1)"><span class="badge_style b_done">'+ data +'</span></a>';
	                    }
	                    else{
	                        var button = '<lable style="color:green;"><b>Approved</b></label>'
	                    }
	                    return button;
	                }
	            },
	            {
					orderable: false, targets: [10],
	                "render": function ( data, type, full, meta ) {
	                   var button = '<span><a class="action-icons c-suspend" href="bdadmin/users/view_user/'+ full.id +'" title="View">View</a></span>';
						   button += '<span><a class="action-icons c-edit" href="bdadmin/users/edit_user_form/'+ full.id + '" title="Edit">Edit</a></span>';
						   if(full.user_status == "Approved")
						   {
						   button += '<span><a class="action-icons c-approve" href="javascript:void(0);" title="pending"  onclick="Pendingpartneruser('+  full.id +',0)">Pending</a></span>';
						   }
	                    return button;
	                }
	            },
	   
	        {
	            orderable: false, targets: [0,3,4,5],
	        }],
	        language: {
	            searchPlaceholder: "Search By Email"
	        },
	        fnDrawCallback: function (oSettings) {
	        }
	    });
	}

    function getfilters(value){
        if(value.value == 'no_rm'){
            	$('#userListing').DataTable().destroy();
            	get_userlisting('yes','yes');
        }else{
        	$.ajax({
                url: baseURL + "bdadmin/users/get_filters_on_type",
                data: {
                    'filter_type':value.value
                },                         
                type: 'post',
                success: function(result){
                    if(result.data.length > 0){
                    	var DropDown = '<label>Filter </label><select class="pincode" id="filter_value"><option selected disabled>Select Value</option>';
                    	for (var i = 0; i < result.data.length; i++) {
                			var filterData = result.data[i];
            				DropDown += '<option value='+ escape(filterData.value) +'>'+ filterData.value +'</option>';
                    	}
                    	DropDown += '</select>';
                    	$('#filter_div_box').html(DropDown);
                    }
                } 
            });
        }
    }



</script>
<style type="text/css">
.dataTables_wrapper .dataTables_paginate .paginate_button {
	margin: 0px 0px 0px -2px !important;
	padding: 0px !important;
}
</style>
<?php 
$this->load->view('bdadmin/templates/footer.php');
?>