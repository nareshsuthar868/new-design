<?php
$this->load->view('bdadmin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Edit Cms</h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'adduser_form', 'enctype' => 'multipart/form-data');
						echo form_open_multipart('bdadmin/setting/updateCms',$attributes) 
					?>
	 						<ul>
	 							<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">Page Name </label>
										<div class="form_input">
											<input name="page_name" style=" width:295px" id="page_name" value="<?php echo $cms_details->row()->page_name;?>" type="text" tabindex="1" class="required tipTop" title="Please enter the user fullname"/>
										</div>
									</div>
								</li>
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">Page Name </label>
										<input type="hidden" name="cms_id" value="<?php echo $cms_details->row()->id; ?>">
										<div class="form_input">
											<textarea name="page_content" id="page_content" tabindex="2" style="width:370px;" class="required large tipTop mceEditor" title="Please enter the product description">
												<?php echo $cms_details->row()->page_content; ?>
											</textarea>
										</div>
									</div>
								</li>
							
								<li>
								<div class="form_grid_12">
									<div class="form_input">
										<button type="submit" class="btn_small btn_blue" tabindex="4"><span>Update</span></button>
									</div>
								</div>
								</li>
							</ul>
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('bdadmin/templates/footer.php');
?>