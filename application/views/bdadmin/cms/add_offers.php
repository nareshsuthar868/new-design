<?php
$this->load->view('bdadmin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Add Offer</h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'adduser_form', 'enctype' => 'multipart/form-data');
						echo form_open_multipart('bdadmin/setting/addInsertOffer',$attributes) 
					?>
	 						<ul>
	 							<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">Discount label</label>
										<div class="form_input">
											<input name="discount_label" style=" width:295px" id="discount_label"  type="text" tabindex="1" class="tipTop" title="Please enter "/>
										</div>
									</div>
								</li>
	 							<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">Discount Price </label>
										<div class="form_input">
											<input name="price" style=" width:295px" id="price" type="text" tabindex="1" class="tipTop" title="Please enter price"/>
										</div>
									</div>
								</li>
									<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">Price Label</label>
										<div class="form_input">
											<input name="price_label" style=" width:295px" id="price_label" type="text" tabindex="1" class="tipTop" title="Please enter price label"/>
										</div>
									</div>
								</li>
									<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">Coupon Code </label>
										<div class="form_input">
											<input name="coupon_code" style=" width:295px" id="v" type="text" tabindex="1" class="tipTop" title="Please enter coupon code"/>
										</div>
									</div>
								</li>
									<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">Coupon Description </label>
										<div class="form_input">
											<input name="coupon_desc" style=" width:295px" id="coupon_desc" type="text" tabindex="1" class="tipTop" title="Please enter coupon description"/>
										</div>
									</div>
								</li>
									<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">Coupon Condition </label>
										<div class="form_input">
											<input name="coupon_label" style=" width:295px" id="coupon_label" type="text" tabindex="1" class="tipTop" title="Please enter coupon condition"/>
										</div>
									</div>
								</li>
									<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">Coupon Tenure </label>
										<div class="form_input">
											<input name="coupon_tenure" style=" width:295px" id="coupon_tenure" type="text" tabindex="1" class="tipTop" title="Please enter coupon tenure"/>
										</div>
									</div>
								</li>
								<li>
									<a href="<?php echo base_url() ?>/images/partner_app/offer_view_template1.png" target="_blank"><p style="text-decoration: underline;">Want guidline click me</p></a>
								</li>
								<li>
								<div class="form_grid_12">
									<div class="form_input">
										<button type="submit" class="btn_small btn_blue" tabindex="4"><span>Add</span></button>
									</div>
								</div>
								</li>
							</ul>
						</form>
						<img src="">
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('bdadmin/templates/footer.php');
?>