<?php
$this->load->view('site/templates/header_inner');
?>	
<script type="text/javascript"  src="<?php echo base_url()?>/js/validation.js"></script>	
		<!--main content-->
<div class="page_section_offset lightgryabg pageheight">
    <section class="innerbanner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
              		<h1>Shipping address</h1>
                  	<ul class="breadcrumb">
                        <li><a href="<?php echo base_url();?>">Home</a></li>
                        <!-- <li><a href="#">Account</a></li> -->
                        <li class="active">Shipping address</li>
                  	</ul>
                </div>
			</div>
        </div>
    </section>
	<div class="container">
		<div class="row">
			<div class="profilecolumn">
			<?php $this->load->view('site/user/settings_sidebar');?>
				<main class="col-lg-9 col-md-9 col-sm-9">
					<div id="content">
						<div class="chart-wrap section shipping shipingcol">
							<div class="table-responsive">
							<?php  if($shippingList->row() > 0){ ?>
                            	<table class="table table-bordered" id="address_table">
									<thead >
										<tr>
											<th><b>Default</b></th>
											<!-- <th><b>Address Type</b></th> -->
											<th><b>Name</b></th>
											<th><b>Address</b></th>
											<th><b>Phone</b></th>
											<th><b>Options</b></th>
										</tr>
									</thead>
									<tbody>
										<?php 
										foreach ($shippingList->result() as $row){ ?>
										<tr id="<?php echo $row->id;?>" aid="<?php echo $row->id;?>" isdefault="<?php if($row->primary == 'Yes'){echo TRUE; }else {echo FALSE;}?>">
											<td data-cell-title="Product Name and Category">
												<div class="lh_small m_bottom_7">
												<?php if($row->primary == 'Yes'){?>Yes<?php }?>
												</div>
											</td>
										<!-- 	<td data-cell-title="Product Name and Category">
												<div class="lh_small m_bottom_7">
												<?php echo $row->nick_name;?>
												</div>
											</td> -->
											<td data-cell-title="Product Name and Category">
												<div class="lh_small m_bottom_7">
												<?php echo $row->full_name;?>
												</div>
											</td>
											<td data-cell-title="Product Name and Category" class="address">
												<div class="lh_small m_bottom_7">
							<?php echo $row->address1.',<br> '.$row->address2.','.$row->city.'-'.$row->state.',<br>'.$row->postal_code;?>
												</div>
											</td>
											<td data-cell-title="Product Name and Category">
												<div class="lh_small m_bottom_7">
												<?php echo $row->phone;
												if($row->phone_alternate != ''){
												echo ',<br>'.$row->phone_alternate; }?>
												</div>
											</td>
											<td>
													<a data-popup="#quick_view" data-popup-transition-in="bounceInUp" data-popup-transition-out="bounceOutUp" href="javascript:void(0);"  onclick="get_shiping(<?php echo $row->id;?>);"><i class="material-icons">mode_edit</i>Modify</a> 
																		
													<a href="javascript:void(0);"  onclick="delete_shipping_address_cart(<?php echo $row->id;?>, <?php if($row->primary == 'Yes'){echo TRUE; }else {echo 0;}?>);"><i class="material-icons deleticn">delete</i>
													<?php if($this->lang->line('shipping_delete') != '') { echo stripslashes($this->lang->line('shipping_delete')); } else echo "Delete"; ?>
													</a>	
											</td>

										</tr>
										  <tr class="divider_td">
                                		<td colspan="6">&nbsp;</td>
							    	</tr>
										<?php } ?>
										
									</tbody>
								</table>
								<?php } else { echo "<center>No Address Found</center>"; }?>
								<center id="empty_msg"></center>
								  <div class="newaddressbtn"><a href="javascript:void(0)" class="btn-check" data-toggle="modal" data-target="#myModaladdress">Add New Shipping Address</a></div><br>
								
						   		</div>
							</div>
						</div>
					</div>
				</main>
			</div>
		</div>
	</div>


				<?php
					$this->load->view('site/templates/footer');
				?>
</div>

		<!--Shipping Address popups-->
<?php 
if(isset($countryList) && $this->uri->segment(2) == 'shipping' || isset($countryList) && $this->uri->segment(1) == 'cart'){
if($this->uri->segment(1) == 'cart'){
	$acURL = 'site/cart/insert_shipping_address';
}else{
	$acURL = 'site/user_settings/insertEdit_shipping_address';
}
?>

<script type="text/javascript">
 $(document).ready(function()
     {
    $('#modal_reset').on('click', function () {
  $('#address').trigger("reset");
   $('#address').validate().resetForm();
    $('body').removeClass('wrapper modal-open');
 })
  });
</script>
 <input type="hidden" value="<?php echo base_url()?>site/user_settings/insertEdit_shipping_address" id="shiping_address">

 <input type="hidden" id="ship_id" />
 <input type="hidden" value="<?php echo $loginCheck ?>" id="user_id">

 <!--Add Shipping Address Popup new-->
		<div id="myModaladdress" class="modal common-popup shipaddnewpop">
            <div class="common-content">
                <div class="address-frm-box">
                     <a href="javascript:void(0)" class="btncross" id="modal_reset" data-toggle="modal" data-target="#myModaladdress">
                            <i aria-hidden="true" class="material-icons">close</i>
                 </a>
                     </a>	
                     <div class="newaddressfrm">
                      <div class="text-left"><h3>Add New Shipping Address</h3></div>
                      <form id="address">
                             <div class="col-lg-6 col-sm-6 col-md-6 addressinput field">
                                <input name="name" id="name" class="form-control FloatingLabel" type="text" placeholder="Name" value="<?php echo $full_name->row()->full_name; ?>" data-error="#inquiry_user_name_error">
                             	<label class="FloatingLabelClass"  for="name">Name</label>
                  				<div  class="error" id="inquiry_user_name_error"></div>
                               
                             </div>
                             <!-- <div class="col-lg-6 col-sm-6 col-md-6 addressinput">
                                <input name="type" id="type" class="form-control" type="text" placeholder="Address Type">
                             </div> -->
                             <div class="col-lg-6 col-sm-6 col-md-6 addressinput field">
                                <input name="Address1" id="Address1" class="form-control FloatingLabel" type="text" placeholder="Address 1" data-error="#inquiry_address1_error">
                                <label class="FloatingLabelClass"  for="Address1">Address 1</label>
                  				<div  class="error" id="inquiry_address1_error"></div>
                             </div>
                             <div class="col-lg-6 col-sm-6 col-md-6 addressinput field">
                                <input name="Address2" id="Address2" class="form-control FloatingLabel" type="text" placeholder="Address 2"  data-error="#inquiry_address2_error">
                                <label class="FloatingLabelClass"  for="Address2">Address 2</label>
                  				<div  class="error" id="inquiry_address2_error"></div>
                             </div>
                               <div class="col-lg-6 col-sm-6 col-md-6 addressinput">
                                <select name="city" id="city" class="form-control" data-error="#inquiry_city_error">
                                	<option value="default" selected>Select City</option>
                                       	<option value="Delhi">Delhi</option>
                                        <option value="Noida">Noida</option>
                                        <option value="Gurgaon">Gurgaon</option>
                                        <option value="Pune">Pune</option>
                                        <option value="Mumbai">Mumbai</option>
                                        <option value="Bangalore">Bangalore</option>
                                        <option value="Ghaziabad">Ghaziabad</option>   
                                </select>
                                <div  class="error" id="inquiry_city_error"></div>
                             </div>
                             <div class="col-lg-6 col-sm-6 col-md-6 addressinput field">
                                <input name="state" id="state" class="form-control FloatingLabel" type="text" placeholder="State" data-error="#inquiry_state_error">
                                <label class="FloatingLabelClass"  for="state">State</label>
                  				<div  class="error" id="inquiry_state_error"></div>
                             </div>
                             <div class="col-lg-6 col-sm-6 col-md-6  addressinput field">
                                <input name="postal_code" id="postal_code" class="form-control FloatingLabel" type="text" placeholder="Postal Code" data-error="#inquiry_postalcode_error">
                                <label class="FloatingLabelClass"  for="postal_code">Postal Code</label>
                  				<div  class="error" id="inquiry_postalcode_error"></div>
                             </div>
                              <div class="col-lg-6 col-sm-6 col-md-6  addressinput field">
                                <input name="number" id="number" class="form-control FloatingLabel" type="text" placeholder="Phone Number" data-error="#inquiry_mobile_number_error">
                                <label class="FloatingLabelClass"  for="number">Phone Number</label>
                  				<div  class="error" id="inquiry_mobile_number_error"></div>
                             </div>
                              <div class="col-lg-6 col-sm-6 col-md-6  addressinput field" >
                                <input name="alter_number" id="alter_number" class="form-control FloatingLabel" type="text" placeholder="Alter Phone Number" data-error="#inquiry_alter_mobile_number_error">
                                <label class="FloatingLabelClass"  for="alter_number">Alter Phone Number</label>
                  				<div  class="error" id="inquiry_alter_mobile_number_error"></div>
                             </div>
                            <!--  <div class="col-lg-6 col-sm-6 col-md-6  addressinput">
                                <select name="country" id="country" class="form-control">
                                    <?php 
										if ($countryList->num_rows()>0){
											foreach ($countryList->result() as $country){
												if($country->country_code=='IN'){?>
													<option value="<?php echo $country->country_code;?>"><?php echo $country->name;?></option>
									    	<?php
									    		}		
									    	}
										}?>                                   
                                </select>
                             </div> -->
                             <div class="col-lg-6 col-sm-6 col-md-6  addressinput">
                               <div class="checkdiv">
                                      <input type="checkbox"  name="default_address" id="default_address"/>
                                           <label  for="default_address">Make this my primary shipping address</label>
                                         </div>
                             </div>
                             <div class="btncenter">
              					<button id="add_button"  class="btn-check" onclick="Add_New_Address()">Submit</button>
              					<div class="alert alert-success" id="div" style="display: none">
  									<strong id="success_msg"></strong>
								</div>
             				 </div>
             				 

                       </form>
		            </div>
                </div>
            </div>
		</div>   

<!--Add Shipping Address Popup-->
<div id="add_address"  class="modal fade" role="dialog" >
	<div class="modal-dialog modal-lg" role="document"  >
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="second_font m_bottom_20 product_title"><a href="#" class="sc_hover">Add Shipping Address</a></h3>
				<hr class="divider_light m_bottom_15">
				<!-- <aside class="col-lg-2 col-md-2 col-sm-2 p_top_4">
				</aside> -->
			</div>
			<form  id="shippingAddForm" method="post" action="<?php echo $acURL;?>">
				<div  class="clearfix"    style="padding:56px 56px 56px 56px;">
					<div class="error-box">
						<p><?php if($this->lang->line('seller_some_requi') != '') { echo stripslashes($this->lang->line('seller_some_requi')); } else echo "Some required information is missing or incomplete. Please correct your entries and try again"; ?>.
						</p>
						<ul></ul>
					</div>
					<div class="row">
							<section class="col-lg-6 col-md-6 col-sm-6">
								<ul class="m_bottom_14">
									<li class="m_bottom_2">
										<label for="username" >Full Name</label>
										<input name="full_name" class="w_full tr_all full required form-control" type="text">
									</li>
									<li class="m_bottom_2">
										<label for="username" class="second_font m_bottom_4 d_inline_b fs_medium ">Address Type</label>
										<input name="nick_name" class="w_full tr_all full required form-control" placeholder="<?php if($this->lang->line('header_home_work') != '') { echo stripslashes($this->lang->line('header_home_work')); } else echo "E.g. Home Address, Work Address"; ?>" type="text">
									</li>
									<li class="m_bottom_2">
										<label class="second_font m_bottom_4 d_inline_b fs_medium"><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?></label>
										<select name="country" class="full required w_full tr_all country select-round select-shipping-addr select_title fs_medium fw_light color_light relative tr_all form-control">
										<?php 
										if ($countryList->num_rows()>0){
											foreach ($countryList->result() as $country){
												if($country->country_code=='IN'){?>
													<option value="<?php echo $country->country_code;?>"><?php echo $country->name;?></option>
									    	<?php
									    		}		
									    	}
										}?>
										</select>										
													</li>
													<li class="m_bottom_2">
														<label for="username" class="second_font m_bottom_4 d_inline_b fs_medium">State</label>
														<input class="w_full tr_all state required form-control" name="state" type="text">
													</li>
													<li class="m_bottom_2">
														<label for="password" class="second_font m_bottom_4 d_inline_b fs_medium">Postal Code</label>
														<input name="postal_code" class="w_full tr_all zip required form-control" type="text">
													</li>

								</ul>
							</section>
							<section class="col-lg-6 col-md-6 col-sm-6">
								<ul class="m_bottom_14">
									<li class="m_bottom_2">
										<label for="password" class="second_font m_bottom_4 d_inline_b fs_medium">Address Line 1</label>
										<input name="address1" class="w_full tr_all full required form-control" type="text">
									</li>
									<li class="m_bottom_2">
										<label for="password" class="second_font m_bottom_4 d_inline_b fs_medium">Address Line 2</label>
										<input name="address2" class="w_full tr_all full form-control" type="text">
									</li>
									<li class="m_bottom_2">
										<label for="password" class="second_font m_bottom_4 d_inline_b fs_medium">City</label>
										<input name="city" class="w_full tr_all full required form-control" type="text">
									</li>
									<li class="m_bottom_30">
										<label for="password" class="second_font m_bottom_4 d_inline_b fs_medium">Phone</label>													
										<input name="phone" class="w_full tr_all full required digits form-control" placeholder="<?php if($this->lang->line('header_ten_only') != '') { echo stripslashes($this->lang->line('header_ten_only')); } else echo "10 digits only, no dashes"; ?>" type="text">
									</li>
									<li class="m_bottom_2">
										<input name="set_default" id="make_this_primary_addr" value="true" type="checkbox" class="form-control" style="border-color: red;">
										<label class="check second_font m_bottom_4 d_inline_b fs_medium" for="make_this_primary_addr"><?php if($this->lang->line('header_make_primary') != '') { echo stripslashes($this->lang->line('header_make_primary')); } else echo "Make this my primary shipping address"; ?></label>
									</li>
									<input type="hidden" name="user_id" value="<?php echo $loginCheck;?>"/>													
								</ul>
							</section>
					</div>
					<section class="col-lg-12 col-md-12 col-sm-12 m_bottom_27">
							<ul>
								<li>
									<input type="submit" style="cursor:pointer;" class="btn btn-success form-control" value="<?php echo "Save"; ?>"/>
								</li>
							</ul>
					</section>
				</div>
			</form>	
		</div>
		<button class="close_popup fw_light fs_large tr_all">x</button>
	</div>
</div>
		
		<!--Edit Shipping Address Popup-->
		<div class="init_popup" id="quick_view">
			<div class="popup init editadds-frm">
				<div class="clearfix">
						<h3 class="second_font m_bottom_20 product_title"><a href="#" class="sc_hover">Modify Shipping Address</a></h3>
						<hr class="divider_light m_bottom_15">
						<aside class="col-lg-2 col-md-2 col-sm-2 p_top_4">
						</aside>
						<form class="ltxt col-lg-8 col-md-8 col-sm-8" id="shippingEditForm" method="post" action="site/user_settings/insertEdit_shipping_address">
						  <div class="section profile">
							<div class="error-box" style="display:none;">
								<p><?php if($this->lang->line('seller_some_requi') != '') { echo stripslashes($this->lang->line('seller_some_requi')); } else echo "Some required information is missing or incomplete. Please correct your entries and try again"; ?>.</p>
								<ul></ul>
							</div>
							<div class="row">
							<section class="col-lg-6 col-md-6 col-sm-6">
								<ul class="m_bottom_14">
													<li class="m_bottom_2">
														<label for="username" class="second_font m_bottom_4 d_inline_b fs_medium">Full Name</label>
														<input name="full_name" class="w_full tr_all full required full_name" type="text">
													</li>
													<li class="m_bottom_2">
														<label for="username" class="second_font m_bottom_4 d_inline_b fs_medium">Address Type</label>
														<input name="nick_name" class="w_full tr_all full required nick_name" placeholder="<?php if($this->lang->line('header_home_work') != '') { echo stripslashes($this->lang->line('header_home_work')); } else echo "E.g. Home Address, Work Address"; ?>" type="text">
													</li>
													<li class="m_bottom_2">
														<label class="second_font m_bottom_4 d_inline_b fs_medium"><?php if($this->lang->line('header_country') != '') { echo stripslashes($this->lang->line('header_country')); } else echo "Country"; ?></label>
																<select name="country" class="full required w_full tr_all country select-round select-shipping-addr select_title fs_medium fw_light color_light relative tr_all">
																	<?php 
																	if ($countryList->num_rows()>0){
																		foreach ($countryList->result() as $country){if($country->country_code=='IN'){
																	?>
																	<option value="<?php echo $country->country_code;?>"><?php echo $country->name;?></option>
																	<?php 
																		}}
																	}
																	?>
																</select>										
													</li>
													<li class="m_bottom_2">
														<label for="username" class="second_font m_bottom_4 d_inline_b fs_medium">State</label>
														<input class="w_full tr_all state required state" name="state" type="text">
													</li>
													<li class="m_bottom_2">
														<label for="password" class="second_font m_bottom_4 d_inline_b fs_medium">Postal Code</label>
														<input name="postal_code" class="w_full tr_all zip required postal_code" type="text">
													</li>

								</ul>
							</section>
							<section class="col-lg-6 col-md-6 col-sm-6">
								<ul class="m_bottom_14">
													<li class="m_bottom_2">
														<label for="password" class="second_font m_bottom_4 d_inline_b fs_medium">Address Line 1</label>
														<input name="address1" class="w_full tr_all full required address1" type="text">
													</li>
													<li class="m_bottom_2">
														<label for="password" class="second_font m_bottom_4 d_inline_b fs_medium">Address Line 2</label>
														<input name="address2" class="w_full tr_all full address2" type="text">
													</li>
													<li class="m_bottom_2">
														<label for="password" class="second_font m_bottom_4 d_inline_b fs_medium">City</label>
														<input name="city" class="w_full tr_all full required city" type="text"></p>
													</li>
													<li class="m_bottom_30">
														<label for="password" class="second_font m_bottom_4 d_inline_b fs_medium">Phone</label>													
														<input name="phone" class="w_full tr_all full required digits phone" placeholder="<?php if($this->lang->line('header_ten_only') != '') { echo stripslashes($this->lang->line('header_ten_only')); } else echo "10 digits only, no dashes"; ?>" type="text">
													</li>
													<li class="m_bottom_2">
														<input name="set_default" id="make_primary_modify" value="true" type="checkbox"> 
														 <label class="check second_font m_bottom_4 d_inline_b fs_medium" for="make_primary_modify"><?php if($this->lang->line('header_make_primary') != '') { echo stripslashes($this->lang->line('header_make_primary')); } else echo "Make this my primary shipping address"; ?></label>
														 														 
													</li>
													<input type="hidden" name="user_id" value="<?php echo $loginCheck;?>"/>													

								</ul>
							</section>
							</div>
							<section class="col-lg-12 col-md-12 col-sm-12 m_bottom_27">
												<ul>
									
													<li>
														<input type="submit" style="cursor:pointer;" class="btn-save t_align_c tt_uppercase w_full second_font d_block fs_medium button_type_2 lbrown tr_all" value="<?php echo "Save"; ?>"/>
													</li>
												</ul>
							</section>
						  </div>
						</form>	
						<aside class="col-lg-2 col-md-2 col-sm-2 p_top_4">
						</aside>
				</div>
				<button class="close_popup fw_light fs_large tr_all">x</button>
			</div>
		</div>


<?php }?> 
		<!--libs include-->
		<script src="plugins/jquery-ui.min.js"></script>
		<script src="plugins/isotope.pkgd.min.js"></script>
		<script src="plugins/jquery.appear.js"></script>
		<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
		<script src="plugins/twitter/jquery.tweet.min.js"></script><script src="plugins/flickr.js"></script>
		<script src="plugins/afterresize.min.js"></script>
		<script src="plugins/jackbox/js/jackbox-packed.min.js"></script>
		<script src="plugins/jquery.elevateZoom-3.0.8.min.js"></script>
		<script src="plugins/fancybox/jquery.fancybox.pack.js"></script>
		<script src="js/retina.min.js"></script>
		<script src="plugins/colorpicker/colorpicker.js"></script>
		 
		<!--Page Js-->
		<script type="text/javascript" src="js/site/jquery.validate.js"></script>
		<!--<script type="text/javascript" src="js/site/socktail-address_helper.js"></script>-->
		<script>

			$("#shippingEditForm").validate();
			$("#shippingAddForm").validate();
		</script>
		
		<!--theme initializer-->
		<script src="js/themeCore.js"></script>
		<script src="js/theme.js"></script>
	</body>
</html>