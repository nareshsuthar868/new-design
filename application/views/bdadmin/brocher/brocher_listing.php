<?php
$this->load->view('bdadmin/templates/header.php');
extract($bdprivileges);
?>

<div id="content">
		<div class="grid_container">
            <?php 
                $attributes = array('id' => 'cms_lisitingID');
            ?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>

					</div>
					<div class="widget_content">
						<table class="display sort-table" id="newsletter_tbl">
                            <thead>
                                <tr>
	                                <th>Brocher Name</th>
                                    <th>Status</th>
                                    <th>Created On</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            	<?php if($brochers_listing->num_rows() > 0){
                            		foreach ($brochers_listing->result() as $key => $value) {?>
                            			<tr>
                            				<td><a href="<?php echo base_url().'uploaded/brochers/'. $value->brocher_file; ?>" target="_blank"><?php echo $value->brocher_name; ?></a></td>
                            				<td><?php if($value->status  == '1'){ echo "Active"; } else {echo "InActive"; } ?></td>
                            				<td><?php echo $value->created_date; ?></td>
                            				<td>
                            					<span><a class="action-icons c-edit" href="bdadmin/setting/edit_brochure/<?php echo $value->id;?>" title="Edit">Edit</a></span>
                                                <span><a class="action-icons c-trash" href="javascript:confirm_delete('bdadmin/setting/delete_brochure/<?php echo $value->id; ?>')" title="Edit">Delete</a></span>
                            				</td>
                            			</tr>
                            	<?php  } }else{ ?>
                            		<tr>
                        				<td colspan="5" style="text-align: center;">No Data!</td>
                        			</tr>
                    			<?php }?>
                        	</tbody>
                          	<tfoot>
                            	<tr>
	                                <th>Brocher Name</th>
                                    <th>Status</th>
                                    <th>Created On</th>
                                    <th>Action</th>
	                            </tr>
	                        </tfoot>
                        </table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>

<?php 
$this->load->view('bdadmin/templates/footer.php');
?>