<?php
$this->load->view('bdadmin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Add Offer</h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'adduser_form', 'enctype' => 'multipart/form-data');
						echo form_open_multipart('bdadmin/setting/addInsertBorcher',$attributes) 
					?>
	 						<ul>
	 							<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">Brocher Name</label>
										<div class="form_input">
											<input name="brocher_name" style=" width:295px" id="brocher_name"  type="text" tabindex="1" class="required tipTop" title="Please enter" placeholder="Brocher Name" />
										</div>
									</div>
								</li>
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">Brocher</label>
										<div class="form_input">
											<input type="file" name="brocher" class="required tipTop" title="Select brocher"> 
											<br><span style="color: red">*Select only PDF file*</span>
										</div>
									</div>
								</li>
	 							<li>
									<div class="form_grid_12">
										<label class="field_title" for="user_name">Brocher Status</label>
										<div class="form_input">
											<div class="active_inactive">
												<input type="checkbox" value="1" name="status" id="active_inactive_active" class="active_inactive"/>
											</div>
											<span style="color: red">*Acrive brocher will be show in app*</span>
										</div>
									</div>
								</li>
								<li>
								<div class="form_grid_12">
									<div class="form_input">
										<button type="submit" class="btn_small btn_blue" tabindex="4"><span>Add</span></button>
									</div>
								</div>
								</li>
							</ul>
						</form>
						<img src="">
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('bdadmin/templates/footer.php');
?>