<?php
$this->load->view('bdadmin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Add Bd Executer</h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'adduser_form', 'enctype' => 'multipart/form-data');
						echo form_open_multipart('bdadmin/bdusers/insertBdExecuter',$attributes) 
					?>
	 						<ul>
	 							<li>
									<div class="form_grid_12">
										<label class="field_title" for="first_name">First Name </label>
										<div class="form_input">
											<input name="first_name" style=" width:295px" id="first_name" type="text" tabindex="1" class="required tipTop" title="Please enter the user firstname"/>
										</div>
									</div>
								</li>
                                <li>
									<div class="form_grid_12">
										<label class="field_title" for="last_name">Last Name </label>
										<div class="form_input">
											<input name="last_name" style=" width:295px" id="last_name" type="text" tabindex="1" class="required tipTop" title="Please enter the user lastname"/>
										</div>
									</div>
								</li>
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="email">Email Address </label>
										<div class="form_input">
										<input name="email" style=" width:295px" id="email" type="text" tabindex="1" class="required tipTop" title="Please enter the user Email"/>
										</div>
									</div>
								</li>
                                <li>
									<div class="form_grid_12">
										<label class="field_title" for="group">Mobile Number <span class="req">*</span></label>
										<div class="form_input">
											<div class="user_seller">
												<input name="phone_no" style=" width:295px" id="phone_no" type="text" tabindex="1" class="required tipTop" title="Please enter the user phone number"/>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="thumbnail">User Image<span class="req">*</span></label>
										<div class="form_input">
											<input name="thumbnail" id="thumbnail" type="file" tabindex="7" class="large tipTop" title="Please select user image"/>
										</div>
									</div>
								</li>
                                <li>
									<div class="form_grid_12">
										<label class="field_title" for="Employee_Id">Employee Id<span class="req">*</span></label>
										<div class="form_input">
											<div class="user_seller">
												<input name="Employee_Id" style=" width:295px" id="Employee_Id" type="text" tabindex="1" title="Please enter the user Employee Id"/>
											</div>
										</div>
									</div>
								</li>
								<!-- <li>
									<div class="form_grid_12" style="display:none;">
									<label class="field_title" for="admin_name">User Type<span class="req">*</span></label>
										<div class="form_input">
											<div class="active_inactive">
												<select name="user_type" id="user_type" class="required tipTop">
													<option value="BD Executive">BD Executive</option>
												</select>
											</div>
										</div>
									</div>
								</li> -->
								<li>
                                <input type="hidden" name="group" value="user">
								<div class="form_grid_12">
									<div class="form_input">
										<button type="submit" class="btn_small btn_blue" tabindex="4"><span>Add</span></button>
									</div>
								</div>
								</li>
							</ul>
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('bdadmin/templates/footer.php');
?>