<?php
$this->load->view('bdadmin/templates/header.php');
extract($bdprivileges);
?>
<div id="content">
		<div class="grid_container">
            <form id="LeadRequestForm">
    			<div class="grid_12">
    				<div class="widget_wrap">
    					<div class="widget_top">
    						<span class="h_icon blocks_images"></span>
    						<h6><?php echo $heading?></h6>
        					<div class="widget_content" style="float: right;">
                                <button class="btn_small btn_blue" type="button" onclick="showexportForm()">Export CSV</button>
                            </div>	
    					</div>
    					<div class="widget_content">
    					   <div class="form_grid_12">
                                <div class="form_input" style="margin-top: 10px;margin-bottom: 10px;">
                                        <label>Filter Type</label>
                                        <select class="large tipTop"  id="filter_type">
                                            <option disabled selected value="null">Type</option>
                                            <option value="rm_name">RM Name</option>
                                            <option value="lead_city">City</option>
                                            <option value="lead_status">Lead Status</option>
                                        </select>
                                </div>
                                <div id="filter_div_box" style="margin-top: 10px;margin-bottom: 10px;">
                                    
                                </div>
                            </div>
    						<table class="display sort-table" id="userListTbl123">
                                <thead>
                                    <tr>
    	                                <th>Order ID</th>
    	                                <th>RM Name</th>
    	                                <th>Lead Owner Name</th>
    	                                <th>Lead Owner Email</th>
    	                                <th>Full Name</th>
    	                                <th>Email</th>
    	                                <th>City</th>
    	                                <th>Phone</th>
                                        <th>Status</th>
                                        <th>Payment Status</th>
    	                                <th>Created Date</th>
    	                                <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                            	</tbody>
                              	<tfoot>
                                	<tr>
    	                                <th>Order ID</th>
    	                                <th>RM Name</th>
    	                                <th>Lead Owner Name</th>
    	                                <th>Lead Owner Email</th>
    	                                <th>Full Name</th>
    	                                <th>Email</th>
    	                                <th>City</th>
    	                                 <th>Phone</th>
                                        <th>Status</th>
                                        <th>Payment Status</th>
    	                                <th>Created Date</th>
    	                                <th>Action</th>
    	                            </tr>
    	                        </tfoot>
                            </table>

    					</div>
    				</div>
    			</div>
            </form>
		</div>
		<span class="clear"></span>
	</div>
</div>

<!--<button type="button" class="trigger" style="display: none;">View</button>-->
<!--    <div class="modal refralmodal" id="model_css">-->
       
<!--    </div>-->

<button type="button" class="trigger" style="display: none;">View</button>
 	<div class="modal refralmodal" id="model_css">
        <div class="modal-content" id="other_allData">
            <span class="close-button">&times;</span>
            <div id="details_show" style="display: none">
                <h2 style="text-align:center">User Lead Details</h2>
    			<div class="card" >
    			  <img id="profile_pic" src="" alt="John" style="width:300px;height: 250px">
    			  <!-- <h1>John Doe</h1> -->
    			  <p class="title">City : <label id="city_lead"></label></p>
    			  <p class="title">Requiremnet : <label id="lead_reqirenment"></label></p>
    			</div>
            </div>

            <div id="add_form_show" style="display: none">
                <h2 style="text-align:center">Link lead with order</h2>
                <form  class="form_container left_label" id="lead_link_form" name="lead_link_form">
                    <ul class="payout_form_ul">
                        <li>
                            <div class="form_grid_12">
                                <label class="field_title" for="order_id">Order ID<span class="req">*</span></label>
                                <div class="form_input">
                                    <input name="bdorder_id" id="bdorder_id" type="text" tabindex="1" class="required large tipTop" required>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="form_grid_12">
                                <label class="field_title" for="commission_percentage">Commission Percentage<span class="req">*</span></label>
                                <div class="form_input">
                                    <input name="commission_percentage" id="bdcommission_percentage" type="text" tabindex="1" class="required large tipTop allownumericwithdecimal">
                                </div>
                            </div>
                        </li>
                    <li>
                        <div class="form_grid_12">
                            <label class="field_title" for="commission_rate">Commission Rate<span class="req">*</span></label>
                            <div class="form_input">
                                <input name="commission_rate" id="bdcommission_rate" type="text" tabindex="1" class="required large tipTop allownumericwithdecimal" >
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="form_grid_12">
                            <label class="field_title" for="status">Status<span class="req">*</span></label>
                            <div class="form_input">
                                <select id="bdstatus">
                                    <option value="paid">Paid</option>
                                    <option value="pending">Pending</option>
                                </select>
                            </div>
                        </div>
                    </li>
                        <li>
                        <div class="form_grid_12">
                            <label class="field_title" for="order_date">Order Date<span class="req">*</span></label>
                            <div class="form_input">
                                <input name="order_date" id="order_date" type="date" tabindex="1" class="required large tipTop" >
                            </div>
                        </div>
                    </li>
                    <input type="hidden" id="lead_id">
                    <li id="new_payout">
                        <div class="form_grid_12 cashgram_button"  style="text-align: right;">
                            <input type="button" onclick="LinkOrderWithLead()" id="cashgram_button" class="btn-check" value="Convert" />
                            <input type="hidden" id="resend" name="resend">
                        </div>
                    </li>
                </ul>

            </form>
            </div>
            <!-- <h1>Lead Details</h1><br> -->     
        </div>
        
         <div class="modal-content" id="export_model" style="display:none">
            <span class="close-button">&times;</span>
            <form method="post" target="_blank" action="<?php echo base_url()?>/bdadmin/lead_request/export_as_csv">
                <span style="color: red">*Without selecting date all leads will be export*</span><br>
                <div class="row">
                    <label>Date From </label>
                    <input type="date" name="date_from" id="from_date" class="from_date">  
                <!-- </div> -->
                    <label>Date To </label>
                    <input type="date" name="date_to" id="to_date" class="from_date">
                <!-- </div> -->
                <!-- <div class="row"> -->
                    <input type="submit" class="btn_small btn_blue" value="Export As CSV" name="" id="export_lead_as_csv" >
                </div>
                
                    
                
            </form>
                       
            <!-- <h1>Lead Details</h1><br> -->     
        </div>
    </div>



<style>
.card {box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);max-width: 300px;margin:auto;text-align:center;font-family:arial;}
.title {color: grey; font-size: 18px;}
.modal {position: fixed;left: 0top: 0;width: 100%;height: 100%;background-color: rgba(0, 0, 0, 0.5);opacity: 0;visibility: hidden;transform: scale(1.1);transition: visibility 0s linear 0.25s, opacity 0.25s 0s, transform 0.25s;}
.modal-content {position: absolute;top: 30%;left: 50%;transform: translate(-50%, -50%);background-color: white;padding: 1rem 1.5rem;width: 42rem;border-radius: 0.5rem;}
.close-button {float: right;width: 1.5rem;line-height: 1.5rem;text-align: center;cursor: pointer;border-radius: 0.25rem;background-color: lightgray;}
.close-button:hover {background-color: darkgray;}
.show-modal {opacity: 1;visibility: visible;transform: scale(1.0);transition: visibility 0s linear 0s, opacity 0.25s 0s, transform 0.25s;}
</style>

     <script type="text/javascript">
        $(document).ready(function(){
            
            get_leadlisting('no');

            $("#filter_type").on('change', function (event) {
                $.ajax({
                    url: baseURL + "bdadmin/lead_request/get_filtervalues",
                    data: {
                        'filter_type':$("#filter_type").val()
                    },                         
                    type: 'post',
                    success: function(result){
                        if(result.data.length > 0){
                            var DropDown = '<label>Filter </label><select class="pincode" id="filter_value"><option selected disabled>Select Value</option>';
                            for (var i = 0; i < result.data.length; i++) {
                                var filterData = result.data[i];
                                DropDown += '<option value='+ escape(filterData.value) +'>'+ filterData.value +'</option>';
                            }
                            DropDown += '</select>';
                            $('#filter_div_box').html(DropDown);
                        }
                    } 
                });
            });
        })

        $(document).on('change', ".pincode", function () {
            $('#userListTbl123').DataTable().destroy();
            get_leadlisting('yes');
        });

        function get_leadlisting(is_filter_applied){
            $('#userListTbl123').DataTable({
                processing:  true,
                serverSide: true,       
                // ajax: baseURL + 'bdadmin/lead_request/get_leadlisting',
                "ajax": {
                    "url": "<?php echo base_url(); ?>bdadmin/lead_request/get_leadlisting",
                    "data":{"is_filter_applied":is_filter_applied, "filter_type":escape($('#filter_type').val()), "filter_value":escape($('#filter_value').val())},
                    "type": "POST"
                },
                "deferRender": true,
                "pagingType": "full_numbers",
                responsive: true,
                order: [7,'desc'],
                columns: [
                    {"data": "order_id"},
                    { "data": "rm_full_name" },
                    { "data": "owner_name" },
                    { "data": "owner_email" },
                    { "data": "full_name" },
                    { "data": "email" },
                    { "data": "city" },
                    { "data": "phone_no" },
                    { "data": "lead_status" },
                    { "data": "cm_status" },
                    { "data": "created" },
                    { "data": "Action"},
                ],
                columnDefs: [

                    {
                        orderable: true, targets: [2],
                        "render": function ( data, type, full, meta ) {
                            var href = baseURL+'bdadmin/users/view_user/'+full.partner_id;
                            if(full.user_type == 'BD Executive'){
                                href = baseURL+'bdadmin/bdusers/view_user/'+full.partner_id;
                            }
                            var value  = '<a href='+href+' target="_blank"><b>'+ data + '</b></a>';
                            return value;
                        }
                    },
                    {
                        orderable: false, targets: [3],
                        "render": function ( data, type, full, meta ) {
                            var value  = '<b>'+ data + '</b>';
                            return value;
                        }
                    },
                    {
                        orderable: false, targets: [4],
                        "render": function ( data, type, full, meta ) {
                            var value  = full.first_name + ' ' + full.last_name;
                            return value;
                        }
                    },
                    {
                        orderable: true, targets: [8],
                        "render": function ( data, type, full, meta ) {
                            var value  = '<font style=color:green><b>'+data.toUpperCase()+'</b></font>';
                            if(data == 'Pending' || data == 'pending' ){
                                value = '<font style=color:red><b>'+data.toUpperCase()+'</b></font>';
                            }
                            return value;
                        }
                    },    
                    {
                        orderable: true, targets: [9],
                        "render": function ( data, type, full, meta ) {
                            if(data != '' && data != null){
                                var value  = '<font style=color:green><b>'+data.toUpperCase()+'</b></font>';
                                if(data == 'pending'){
                                    value = '<font style=color:red><b>'+data.toUpperCase()+'</b></font>';
                                }
                                return value;
                            }else{
                                return '--';
                            }
                        }
                    },    
                    {
                        orderable: false, targets: [11],
                        "render": function ( data, type, full, meta ) {
                            var image_url = baseURL + "images/partner_app/profile_pic/"+full.profile_pic;
                            var button = '';
                            if(full.lead_status == 'pending' || full.lead_status == 'Pending'){
                                button += '<span class="action-icons c-add"  original-title="Link Order" \
                                onclick="showLinkOrderForm('+full.id+')"></span>';      
                            }
                            if(full.cm_status == 'pending' || full.cm_status == 'Pending'){
                               button += '<span class="action-icons c-approve"  original-title="Mark As Paid" \
                                onclick="MarkAsPaid('+full.id+')"></span>';         
                            }


                            button += '<span class="action-icons c-edit"  original-title="View Requirements" \
                            onclick="ShowLeadRequirements(\''+ image_url +'\',\''+ full.city +'\',\''+ full.requirements +'\')"></span>';

                            return button;
                        }
                    },
           
                {
                    orderable: false, targets: [0],
                }],
                language: {
                    searchPlaceholder: "Search By Email"
                },
                fnDrawCallback: function (oSettings) {
                }
            });
            
        }
        $(".allownumericwithdecimal").on("keypress keyup blur",function (event) {
            $(this).val($(this).val().replace(/[^0-9\.]/g,''));
            if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        })

	 	var modal = document.querySelector(".modal");
    	var trigger = document.querySelector(".trigger");
    	var closeButton = document.querySelector(".close-button");

	    function toggleModal() {
	        modal.classList.toggle("show-modal"); 
	        document.getElementsByTagName('body')[0].style = 'overflow: auto';
	    }
	    function windowOnClick(event) {
	        if (event.target === modal) {
	            toggleModal();
	        }
	    }
	    trigger.addEventListener("click", toggleModal);
	    closeButton.addEventListener("click", toggleModal);
	    window.addEventListener("click", windowOnClick);
    </script>



<?php 
$this->load->view('bdadmin/templates/footer.php');
?>