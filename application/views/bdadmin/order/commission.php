<?php
$this->load->view('bdadmin/templates/header.php');
extract($bdprivileges);
?>
 <style type="text/css">

.modal {
    position: fixed;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(0, 0, 0, 0.5);
    opacity: 0;
    visibility: hidden;
    transform: scale(1.1);
    transition: visibility 0s linear 0.25s, opacity 0.25s 0s, transform 0.25s;
}
.modal-content {
    position: absolute;
    top: 50%;
    left: 50%;
    height: 88%;
    overflow: scroll;
    transform: translate(-50%, -50%);
    background-color: white;
    padding: 1rem 1.5rem;
    width: 80%;
    border-radius: 0.5rem;
}
.close-button {
    float: right;
    width: 1.5rem;
    line-height: 1.5rem;
    text-align: center;
    cursor: pointer;
    border-radius: 0.25rem;
    background-color: lightgray;
}
.close-button:hover {
    background-color: darkgray;
}
.show-modal {
    opacity: 1;
    visibility: visible;
    transform: scale(1.0);
    transition: visibility 0s linear 0s, opacity 0.25s 0s, transform 0.25s;
}
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: center;
    padding: 8px;
}
tr:nth-child(even) {
    background-color: #dddddd;
}
@media only screen and (max-width: 1366px) {
    .refralmodal{
        overflow: auto;
    }
    .refralmodal .modal-content{
        transform: translate(-50%, 0);
        top: 20px;
    } 
    .modal-open{
        overflow: hidden;
    }
}

.dataTables_wrapper .dataTables_paginate .paginate_button {
    margin: 0px 0px 0px -2px !important;
    padding: 0px !important;
}
</style>

<div id="content">
	<div class="grid_container">
		<?php 
			$attributes = array('id' => 'displayCommissions');
			echo form_open('bdadmin/users/change_user_status_global',$attributes) 
		?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
					</div>
					<div class="widget_content">
						<table class="display sort-table" id="commissions_table">
                            <thead>
                                <tr>
	                            	<th class="center">
										<input name="checkbox_id[]" type="checkbox" value="on" class="selectall">
									</th>
	                                <th>Full Name</th>
	                                <th>Email</th>
	                                <th>Total Commission</th>
	                                <th>User Type</th>
                                    <th>Commission Status</th>
	                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                        	</tbody>
                          	<tfoot>
                            	<tr>
	                            	<th class="center">
										<input name="checkbox_id[]"  type="checkbox" value="on" class="selectall12">
									</th>
	                                <th>Full Name</th>
	                                <th>Email</th>
	                                <th>Total Commission</th>
	                                <th>User Type</th>
                                    <th>Commission Status</th>
	                                <th>Action</th>
	                            </tr>
	                        </tfoot>
                        </table>
					</div>
				</div>
			</div>
   	   </form>	
   </div>
</div>
<button type="button" class="trigger" style="display: none;">View</button>
    <div class="modal refralmodal" id="model_css123">
        <div class="modal-content">
            <span class="close-button">&times;</span>  
                <div class="show_all_payouts">
                    Total Leads: <label id="total_leads">0</label>
                   <table class="display sort-table" >
                       <thead>
                           <tr>
                               <th>Order ID</th>
                               <th>Name</th>
                               <th>Email</th>
                               <th>Phone</th>
                               <th>Commission Percentage</th>
                               <th>Commission Rate</th>
                               <th>Lead Status</th>
                               <th>Commission Status</th>
                               <th>Commission Date</th>
                           </tr>
                       </thead>
                       <tbody id="payout_table_body">
                           
                       </tbody>
                   </table>
                </div> 
        </div>
    </div>

<script type="text/javascript">
  
    var modal = document.querySelector(".modal");
    var trigger = document.querySelector(".trigger");
    var closeButton = document.querySelector(".close-button");

    function toggleModal() {
        modal.classList.toggle("show-modal"); 
        document.getElementsByTagName('body')[0].style = 'overflow: auto';
    }

    function windowOnClick(event) {
        if (event.target === modal) {
            toggleModal();
        }
    }
    trigger.addEventListener("click", toggleModal);
    closeButton.addEventListener("click", toggleModal);
    window.addEventListener("click", windowOnClick);
    
</script>

<?php 
$this->load->view('bdadmin/templates/footer.php');
?>