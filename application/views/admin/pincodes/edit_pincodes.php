<?php
$this->load->view('admin/templates/header.php');
$rr = $pincode_details->result();
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6><?php echo $heading;?></h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'edituser_form');
						echo form_open('admin/pincodes/insertEditpincode',$attributes) 
					?>
	 				<ul>
                            <input type="hidden" name="pincode_id" value="<?php echo $rr[0]->ID; ?>" />
                            <li>
								<div class="form_grid_12">
									<label class="field_title" for="user_name">Pin code <span class="req">*</span></label>
									<div class="form_input">
										<input name="pincode" id="pincode" type="text" tabindex="2" class="required small tipTop" value="<?php echo $rr[0]->pincode; ?>" title="Please Enter the Pin Code" />
									</div>
								</div>
								</li>                                                               
                                
                                <li>
								<div class="form_grid_12">
                                    <label class="field_title">Select a City</label>
									<div class="form_input">
									<select data-placeholder="Select a Coupon Type" name="city" style=" width:300px" class="chzn-select-deselect" tabindex="13">
										<option value="">None</option>
									<?php foreach($listv as $row){ ?>                                      
										<option value="<?php echo $row->id; ?>" <?php if($rr[0]->cityid==$row->id){ echo "selected"; } ?>><?php echo $row->list_value; ?></option>
									<?php } ?>
									</select>
									</div>
								</div>
								</li>
                            						
								<li>
								<div class="form_grid_12">
									<div class="form_input">
										<button type="submit" class="btn_small btn_blue" tabindex="4"><span>Update</span></button>
									</div>
								</div>
								</li>
							</ul>
							<input type="hidden" name="coupon_id" value="<?php echo $couponcard_details->row()->id?>"/>
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>