<?php
// print_r($allPrev);exit;
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('admin/seller/change_seller_status_global',$attributes) 
			?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						<div style="float: right;line-height:40px;padding:0px 10px;height:39px;">
						<?php 
						if ($allPrev == '1' || in_array('3', $seller)){
						?>
							<div class="btn_30_light" style="height: 29px;">
								<a href="javascript:void(0)" onclick="return checkBoxValidationAdmin('Delete','<?php echo $subAdminMail; ?>');" class="tipTop" title="Select any checkbox and click here to delete records"><span class="icon cross_co"></span><span class="btn_link">Delete</span></a>
							</div>
						<?php }?>
						</div>
					</div>
					<div class="widget_content">
							<div class="widget_content">
						<table class="display sort-table" id="SellerListing">
                            <thead>
                                <tr>
	                            	<th class="center">
										<input name="checkbox_id[]" type="checkbox" value="on" class="selectall">
									</th>
									<th> ID</th>
	                                <th>Full Name</th>
	                                <th>Email</th>
	                                <th>Phone</th>
	                                <th>Thumbnail</th>
	                                <th>Commision</th>
	                                <th>Brand Name</th>
	                                <th>Last Login Date</th>
	                                <th>Status</th>
	                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                        	</tbody>
                          	<tfoot>
                            	<tr>
	                            	<th class="center">
										<input name="checkbox_id[]"  type="checkbox" value="on" class="selectall12">
									</th>
	                             	<th> ID</th>
	                                <th>Full Name</th>
	                                <th>Email</th>
	                                <th>Phone</th>
	                                <th>Thumbnail</th>
	                                <th>Commision</th>
	                                <th>Brand Name</th>
	                                <th>Last Login Date</th>
	                                <th>Status</th>
	                                <th>Action</th>
	                            </tr>
	                        </tfoot>
                        </table>
					</div>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
</div>

<style>
#seller_list_tbl tr td,#seller_list_tbl tr th{
	border-right: 1px solid #ccc;
}
</style>
<script type="text/javascript">
$(function (){

    $('#SellerListing').DataTable({
        processing:  true,
        serverSide: true,
        ajax: '<?php echo base_url(); ?>admin/seller/get_seller_listing',
        "deferRender": true,
        "pagingType": "full_numbers",
        responsive: true,
        order: [],
        columns: [
        	null,
        	{ "data": "id" },
            { "data": "full_name" },
            { "data": "email" },
            { "data": "s_phone_no"},
            { "data": "thumbnail" },
            { "data": "commision"},
            { "data": "brand_name"},
            { "data": "last_login_date" },
            { "data": "status"},
             { "data": "Action"},
        ],
        columnDefs: [
        	{
                 orderable: false, targets: [0],
                "render": function ( data, type, full, meta ) {
                    var link  = '<input name="checkbox_id[]" type="checkbox" value="'+ full.id +'" class="case" onclick="change_all()">';
                    return link;
                }
            },
            {"targets": [0], "className": "center tr_select"},
          	{
                 orderable: false, targets: [5],
                "render": function ( data, type, full, meta ) {
                    var base_url =  '<?= BASE_URL?>';
                    var image_url = base_url + "images/";
                    var alt_image = image_url + "user-thumb1.png";
                    if(data == ''){
                        var url = alt_image;
                    }
                    else{
                        var url = image_url + data;
                    }
                    return '<div class="widget_thumb" style="margin-left: 65px;"><img width="40px" height="40px" src=\'' + url + '\'></div>'
                }
            },

            {
                orderable: false, targets: [6],
                "render": function ( data, type, full, meta ) {
                    return data+' %';

                }
            },
            {
                 orderable: false, targets: [9],
                "render": function ( data, type, full, meta ) {
                	var mode = 0;
                	if(full.status == 'Active'){
                		mode = 0;
                	}else{
                		mode = 1;
                	}
                	if(data == 'Active'){
                		var status_button = '<a title="Click to inactive" class="tip_top" href="javascript:confirm_status(\'admin/seller/change_user_status/'+mode+'/'+full.id+'\');"><span class="badge_style b_done">'+full.status+'</span></a>';
                	}else{
            			var status_button = '<a title="Click to inactive" class="tip_top" href="javascript:confirm_status(\'admin/seller/change_user_status/'+mode+'/'+full.id+'\');"><span class="badge_style">'+full.status+'</span></a>';
                	}
                    return status_button;
                }
            },

            {
                orderable: false, targets: [9],
                "render": function ( data, type, full, meta ) {
                	var mode = 0;
                	if(full.status == 'Active'){
                		mode = 0;
                	}else{
                		mode = 1;
                	}
                	if(data == 'Active'){
                		var status_button = '<a title="Click to inactive" class="tip_top" href="javascript:confirm_status(\'admin/seller/change_user_status/'+mode+'/'+full.id+'\');"><span class="badge_style b_done">'+full.status+'</span></a>';
                	}else{
            			var status_button = '<a title="Click to inactive" class="tip_top" href="javascript:confirm_status(\'admin/seller/change_user_status/'+mode+'/'+full.id+'\');"><span class="badge_style">'+full.status+'</span></a>';
                	}
                    return status_button;
                }
            },

            {
                orderable: false, targets: [10],
                "render": function ( data, type, full, meta ) {
                	<?php if ($allPrev == '1' || in_array('2', $seller)){?>
                		var button = '<span><a class="action-icons c-edit" href="admin/seller/edit_seller_form/'+full.id+'" title="Edit">Edit</a></span>';
                	<?php }?>
                	button+= '<span><a class="action-icons c-suspend" href="admin/seller/view_seller/'+full.id+'" title="View">View</a></span>';
                	<?php if ($allPrev == '1' || in_array('3', $seller)){?>	
					button+= '<span><a class="action-icons c-delete" href="javascript:confirm_delete(\'admin/seller/delete_seller/'+full.id+'\')" title="Delete">Delete</a></span>'
					<?php }?>
                
                    return button;
                }
            },
   
        {
            orderable: false, targets: [0,10],
        }
        ],
        language: {
            searchPlaceholder: "Search By Email"
        },
        fnDrawCallback: function (oSettings) {
        }
    });
});


</script>
<?php 
$this->load->view('admin/templates/footer.php');
?>