<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>SI or CC Discount Setting</h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label','id' => 'adduser_form');
						echo form_open('admin/couponcards/update_si_discount_setting',$attributes) 
					?>
	 					<ul>
                             <li>
								<div class="form_grid_12">
									<label class="field_title" for="heading">Discount Amount<span class="req">*</span></label>
									<div>
										<input name="discount_amount" type="text" tabindex="2" class="required small tipTop" title="Please Enter the Amount" 
										value="<?php echo $si_discount_setting->row()->discount_amount; ?>" />
									</div>
								</div>
							</li>
                            <li>
								<div class="form_grid_12">
									<label class="field_title" for="user_name">Active<span class="req">*</span></label>
									<div>
										<input type="radio" name="active" value="Yes" <?php if($si_discount_setting->row()->active == 'Yes'){ echo 'checked'; } ?>>Yes
										<input type="radio" name="active" value="No" <?php if($si_discount_setting->row()->active == 'No'){ echo 'checked'; } ?>>No
										
									</div>

								</div>
							</li>                                                               

							<li>
								<div class="form_grid_12">
                                    <label class="field_title">Updated On<span class="req"></span></label>
									<div>
										<label><?php 
										$input = $si_discount_setting->row()->updated_date;
										$date = strtotime($input); 
										echo date('d/M/Y h:i:s', $date); 
										?></label>
									</div>
								</div>

							</li>


							
					
						
 
                        <li>
							<div class="form_grid_12">
								<div class="form_input">
										<button type="submit"  id="submit" class="btn_small btn_blue" tabindex="8"><span>Update</span></button>
								</div>
							</div>
						</li>
					</ul>
				</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>