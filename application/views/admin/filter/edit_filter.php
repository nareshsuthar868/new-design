<?php
$this->load->view('admin/templates/header.php');
?>

<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="widget_wrap tabby">
          <div class="widget_top"> <span class="h_icon list"></span>
            <h6>Add New Main Page</h6>
          </div>
          <div class="widget_content">
            <?php 
				      $attributes = array('class' => 'form_container left_label', 'id' => 'adduser_form');
				      echo form_open('admin/filter/insertUpdateFilters',$attributes) 
			       ?>
            <div>
              <ul>
                <li>
                  <div class="form_grid_12">
                    <label class="field_title" for="page_name">Select Category <span class="req">*</span></label>
                    <div class="form_input">
                      <select id="mainCategory" name="parentCategory">
                      <?php
                        foreach ($parentCategory->result_array() as $key => $value) { ?>
                          <option class="required large tipTop" value="<?php echo $value['id'];?>" <?php if($value['id'] == $this->data['filterDetails']->row()->category_id) { echo "selected"; } ?>>
                           <?php echo $value['cat_name']; ?>
                          </option>
                       <?php } ?>
                      </select>
                    </div>
                  </div>
                </li>
                <li  id="SubCategoryli">
                  <div class="form_grid_12">
                    <label class="field_title" for="page_title">SubCategory</label>
                    <div class="form_input">
                        <select id="DynamicSubCategory" name="ChildCategory">
                          <?php
                          // print_r($childCategory->num_rows());exit;
                          if($childCategory->num_rows() > 0){
                            foreach ($childCategory->result_array() as $key => $value) { ?>
                              <option class="required large tipTop" value="<?php echo $value['id'];?>" <?php if($value['id'] == $this->data['filterDetails']->row()->sub_category_id) { echo "selected"; } ?>>
                                <?php echo $value['cat_name']; ?>
                              </option>
                          <?php }}else{ echo "<option disabled selected>NO DATA</option>"; } ?>
                        </select>
                        <p id="subCateogry"></p>
                    </div>
                  </div>
                </li>
                <li>
                  <div class="form_grid_12">
                    <label class="field_title" for="description">Filter Name</label>
                    <div class="form_input">
                      <input type="text" name="filter_name" class="large tipTop required" title="Please enter filter value"
                      value="<?php echo $this->data['filterDetails']->row()->filter_name; ?>"> 
                    </div>
                  </div>
                </li>
              </ul>
              <ul>
                <li>
                  <div class="form_grid_12">
			             <div class="form_input">
                    <input type="hidden" name="id" value="<?php echo $this->data['filterDetails']->row()->id; ?>">
			  	            <button type="submit" class="btn_small btn_blue" tabindex="5"><span>Submit</span></button>
				            </div>
		              </div>
                </li>
                </ul>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<span class="clear"></span> </div>
</div>

<script type="text/javascript">
    $(function () {
        // get_childCategory();
        $('#mainCategory').on("change",function(){
            get_childCategory();
        });
    });
    
    function get_childCategory(){
      var mainCategory =  $('#mainCategory').val();
      $.ajax({
          url: baseURL + 'admin/filter/get_subcategories',
          method: 'GET',
          data: {"catID":mainCategory },
          success: function(response) {
            var DynamicSubCategory = '';
            if(response.data.length > 0){
              for (var i = 0; i < response.data.length; i++) {
                DynamicSubCategory += '<option value='+response.data[i].id+'>'+response.data[i].cat_name+'</option>';
              }
              $('#subCateogry').hide();
              $('#DynamicSubCategory').show();
              $('#DynamicSubCategory').html(DynamicSubCategory);
            }
            else{
              $('#subCateogry').show();
              $('#DynamicSubCategory').hide();
              $('#subCateogry').html("No SubCategory Found For Selected Category");
            }
          }
      });
    }
</script>
<?php 
$this->load->view('admin/templates/footer.php');
?>
