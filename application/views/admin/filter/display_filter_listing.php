<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
	<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading; ?></h6>
					</div>
					<div class="widget_content">
						<table class="display"  id="Paid_orders_list">
							<thead>
								<tr>
									<th class="tip_top">Filter Name</th>
									<th class="center">SubCategory Name</th>
									<th>Category Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								
							</tbody>
							<tfoot>
								<tr>
									<th class="tip_top">Filter Name</th>
									<th class="center">SubCategory Name</th>
									<th>Category Name</th>
									<th>Action</th>
								</tr>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
	</div>
	<span class="clear"></span>
</div>
</div>

<script type="text/javascript">
$(function (){
    $('#Paid_orders_list').DataTable({
        processing:  true,
        serverSide: true,
        ajax: '<?php echo base_url(); ?>admin/filter/get_filter_listing',
        "deferRender": true,
        "pagingType": "full_numbers",
        responsive: true,
        // order: [[ 1, "desc" ]],
        columns: [
            { "data": "filter_name" },
            { "data": "cat_name"},
         	{ "data": "subcatname"},
          	null
        ],
        columnDefs: [
            {
                 orderable: false, targets: [3],
                "render": function ( data, type, full, meta ) {
                	var filter_id = full.id;
            	   	var base_url = "<?php echo  base_url(); ?>";
                    var link  = '<a href="'+base_url+'admin/filter/edit_filter/'+ filter_id + '" ><span class="action-icons c-suspend tipTop" title="Update Filter" style="cursor:pointer;"></span></a>';
                    return link;
                }
            },
          	
   
        {
            orderable: false, targets: [0],
        }],
        language: {
            searchPlaceholder: "Search By Email"
        },
        fnDrawCallback: function (oSettings) {
        }
    });
});
</script>

<?php
$this->load->view('admin/templates/footer.php');
?>