<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6><?php echo $heading; ?></h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'commentForm');
						echo form_open('admin/documents/insert_update_documents',$attributes) 
					?> 		
            			<div>
	 						<ul>
                                <li>
									<div class="form_grid_12">
										<label class="field_title" for="location_name">Document Name<span class="req">*</span></label>
										<div class="form_input">
                                    		<input name="document_name" style=" width:295px" id="doc_name" value="<?php if(!empty($cibil_documents)){ echo $cibil_documents->doc_name; } ?>" type="text" tabindex="1" class="required tipTop" title="Please document name"/>
										</div>
									</div>
								</li>
								<li>
									<div class="form_grid_12">
										<label class="field_title" for="location_name">Document Upload Limit<span class="req">*</span></label>
										<div class="form_input">
                                    		<input name="max_files" style=" width:295px" id="max_files" value="<?php if(!empty($cibil_documents)){ echo $cibil_documents->max_files; } ?>" type="text" tabindex="1" class="required tipTop" title="Please document doc limit"/>
										</div>
									</div>
								</li>
								<input type="hidden" name="doc_id" value="<?php if(!empty($cibil_documents)){ echo $cibil_documents->id; } ?>"> 
								<li>
									<div class="form_grid_12">
										<div class="form_input">
											<button type="submit" class="btn_small btn_blue" tabindex="4"><span>Submit</span></button>
										</div>
									</div>
								</li>
							</ul>
                        </div>
					</form>
				</div>
			</div>
		</div>
	</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>