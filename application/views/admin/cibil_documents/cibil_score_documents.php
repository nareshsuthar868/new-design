<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">
				<div class="widget_top">
					<span class="h_icon blocks_images"></span>
					<h6><?php echo $heading?></h6>
				</div>
				<div class="widget_content">
					<table class="display" id="newsletter_tbl">
						<thead>
							<tr>
								<th class="tip_top" title="Click to sort">
									Documents Name
								</th>
								<th class="center" title="Click to sort">
                             		Created On
								</th>
                            	<th class="center" title="Click to sort">
                           			Action
								</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if($cibil_documents->num_rows() > 0){
						foreach ($cibil_documents->result_array() as $key => $value) { ?>
							<tr>
								<td><?php echo $value['doc_name']; ?></td>
								<td><?php echo $value['created_on']; ?></td>
								<td><span><a class="action-icons c-edit" href="admin/documents/edit_doc/<?php echo $value['id'];?>" title="Edit">Edit</a></span></td>
							</tr>
							<?php }
						} ?>
						</tbody>
						<tfoot>
							<tr>
								<th class="tip_top" title="Click to sort">
									Documents Name
								</th>
								<th class="center" title="Click to sort">
                             		Created On
								</th>
                            	<th class="center" title="Click to sort">
                           			Action
								</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>