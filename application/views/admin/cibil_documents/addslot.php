<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6><?php echo $heading; ?></h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'commentForm');
						echo form_open('admin/documents/insertEditslots',$attributes) 
					?> 		
            			<div>
	 						<ul>
                                <li>
									<div class="form_grid_12">
										<label class="field_title" for="location_name">Slot From<span class="req">*</span></label>
										<div class="form_input">
                                    		<input name="start_from" style=" width:295px" id="start_from" value="<?php if(!empty($cibil_documents)){ echo $cibil_documents->score_from; } ?>" type="number" tabindex="1" class="required tipTop" title="Please enter slot start from"/>
										</div>
									</div>
								</li>
								<input type="hidden" name="slot_id" value="<?php if(!empty($cibil_documents)){ echo $cibil_documents->id; } ?>"> 
                                <li>
									<div class="form_grid_12">
										<label class="field_title" for="iso_code2">Slot To<span class="req">*</span></label>
										<div class="form_input">
                                    		<input name="to_from" style=" width:295px" id="to_from" value="<?php if(!empty($cibil_documents)){ echo $cibil_documents->score_to; } ?>" type="text" tabindex="1" class="required tipTop" title="Please enter slot end to"/>
										</div>
									</div>
								</li>
                                <li>
									<div class="form_grid_12">
										<label class="field_title" for="iso_code3">Select Documents<span class="req">*</span></label>
										<div class="form_input">
                                    		<select name="documents[]" multiple>
                                    			<?php
                                    			$docs = array(); 
                                    			if(!empty($cibil_documents)){  
                                    				$docs = explode(',', $cibil_documents->documents);
                                    			}
                                    			$dropDown = '';
                                    			foreach ($cibil_require_documents->result() as $key => $value) {
                                    				$dropDown .=  '<option value="'.$value->id.'"';

													if(in_array($value->id, $docs)) { 
														$dropDown .= 'Selected';
													};
													$dropDown .= '>'.$value->doc_name.'</option>';  
                                    			}
                                    			echo $dropDown;
                                    			?>

                                    		</select>
										</div>
									</div>
								</li>
								<li>
									<div class="form_grid_12">
										<div class="form_input">
											<button type="submit" class="btn_small btn_blue" tabindex="4"><span>Submit</span></button>
										</div>
									</div>
								</li>
							</ul>
                        </div>
					</form>
				</div>
			</div>
		</div>
	</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>