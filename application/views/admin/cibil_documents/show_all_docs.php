<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">
				<div class="widget_top">
					<span class="h_icon blocks_images"></span>
					<h6><?php echo $heading?></h6>
				</div>
				<div class="widget_content">
					<table class="display" id="newsletter_tbl">
						<thead>
							<tr>
								<th class="tip_top" title="Click to sort">Document Type</th>
								<th class="center" title="Click to sort">Uploaded On</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if($doc_more_details->num_rows() > 0){
							foreach ($doc_more_details->result_array() as $key => $value) { ?>
								<tr>
									<td><?php echo ucfirst(str_replace('_', ' ', $value['doc_type'])); ?></td>
									<td><?php echo $value['uploaded_date']; ?></td>
									<td><span><a class="action-icons c-suspend tipTop" href="admin/documents/explain_details/<?php echo $value['user_id'];?>" title="view details">view documents</a></span></td>
								</tr>
							<?php }
						} ?>
						</tbody>
						<tfoot>
							<tr>
								<th class="tip_top" title="Click to sort">Document Type</th>
								<th class="center" title="Click to sort">Uploaded On</th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>