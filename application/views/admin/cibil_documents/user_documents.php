<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">
				<div class="widget_top">
					<span class="h_icon blocks_images"></span>
					<h6><?php echo $heading?></h6>
				</div>
				<div class="widget_content">
					<table class="display" id="newsletter_tbl">
						<thead>
							<tr>
								<th class="tip_top" title="Click to sort">User Name</th>
								<th class="center" title="Click to sort">Email</th>
								<th class="center" title="Click to sort">Order ID</th>
        						<th class="center" title="Click to sort">Last Added On</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if($user_documents->num_rows() > 0){
							foreach ($user_documents->result_array() as $key => $value) { ?>
								<tr>
									<td><?php echo $value['full_name']; ?></td>
									<td><?php echo $value['email']; ?></td>
									<td>#<?php echo $value['order_id']; ?></td>
									<td><?php echo $value['uploaded_date']; ?></td>
									<td><span><a class="action-icons c-suspend tipTop" href="javascript:void(0)" onclick="showDetails('<?php echo $value['order_id'] ?>','<?php echo $value['full_name']; ?>')" title="view details">view details</a></span></td>
								</tr>
							<?php }
						} ?>
						</tbody>
						<tfoot>
							<tr>
								<th class="tip_top" title="Click to sort">User Name</th>
								<th class="center" title="Click to sort">Email</th>
								<th class="center" title="Click to sort">Order ID</th>
        						<th class="center" title="Click to sort">Last Added On</th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<button type="button" class="trigger" style="display: none;">View</button>
    <div class="modal  refralmodal" id="dynamic_modal">
        <div class="modal-content modal-lg">
            <span class="close-button">&times;</span>
            <h1 id="modal_title">Modal</h1><br>
            <div id="modal_data">  
            </div>
        </div>
    </div>
<script type="text/javascript">
 	var modal = document.querySelector(".modal");
	var trigger = document.querySelector(".trigger");
	var closeButton = document.querySelector(".close-button");
	function toggleModal() {
        modal.classList.toggle("show-modal"); 
        document.getElementsByTagName('body')[0].style = 'overflow: auto';
    }
    function windowOnClick(event) {
        if (event.target === modal) {
            toggleModal();
        }
    }
    trigger.addEventListener("click", toggleModal);
    closeButton.addEventListener("click", toggleModal);
    window.addEventListener("click", windowOnClick);
</script>


<script type="text/javascript">
	function showDetails(order_id,user_name){
		var img_url = '<?php echo CDN_URL; ?>images/cibil_docments/';
	 	$("#dynamic_modal").show(), 
		$("#dynamic_modal").addClass("show-modal")
		$('#modal_title').html('( '+ user_name +' ) Documents');
     	$.ajax({
        	type: "POST",
        	url: baseURL + "admin/documents/show_user_documents",
        	data: {
            	order_id: order_id,
        	}, success: function(e){
        		if(e.status == 200){
        			if(e.data.length > 0){
        				var html = '<h6>Linkedin Url: '+ e.data[0].linkedin_url +' </h6><br><h6>Special Remarks : '+e.data[0].special_remarks+'</h6><br>';
        				for (var i = 0; i < e.data.length; i++) {
        					var doc = e.data[i];
        					var doc_type = doc.doc_type.replace(/_/g,' ');
        					html += '<h3>'+  doc_type.toUpperCase() +'</h3>\
        						<img src="'+ img_url + doc.doc_type	+'/'+ doc.doc_name +'" style="height: 200px;width: 200px;">\
        						 ';
        				}
        				$('#modal_data').html(html);
        			}
        		}else{
        			location.href = baseURL+'admin';
        		}
        	}
    	})
	}
</script>


<?php 
$this->load->view('admin/templates/footer.php');
?>