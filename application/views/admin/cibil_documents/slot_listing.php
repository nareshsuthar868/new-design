<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">
				<div class="widget_top">
					<span class="h_icon blocks_images"></span>
					<h6><?php echo $heading?></h6>
				</div>
				<div class="widget_content">
					<table class="display" id="newsletter_tbl">
						<thead>
							<tr>
								<th class="tip_top" title="Click to sort">Score From</th>
								<th class="center" title="Click to sort">Score To</th>
        						<th class="center" title="Click to sort">Documents</th>
								<th>Modified On </th>
								<th>Created On </th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
						<?php
						if($cibil_documents->num_rows() > 0){
							foreach ($cibil_documents->result_array() as $key => $value) { ?>
								<tr>
									<td><?php echo $value['score_from']; ?></td>
									<td><?php echo $value['score_to']; ?></td>
									<td>
										<?php 
										$doc = explode(',', $value['documents']); 
										$li_list = '<ul>';
										foreach ($cibil_require_documents as $doc_key => $val) {
											if(in_array($val['id'], $doc)){
												$li_list .= '<li>'.$val['doc_name'] .'</li><br>';
											}
										}
										$li_list .= '</ul>';
										echo $li_list;
										?>	
									</td>
									<td><?php echo $value['modified_at']; ?></td>
									<td><?php echo $value['created_date']; ?></td>
									<td><span><a class="action-icons c-edit" href="admin/documents/edit_slot/<?php echo $value['id'];?>" title="Edit">Edit</a></span></td>
								</tr>
							<?php }
						} ?>
						</tbody>
						<tfoot>
							<tr>
								<th>Score From</th>
								<th class="center">Score To</th>
                        		<th>Documents</th>
								<th>Modified On </th>
								<th>Created On </th>
								<th>Action</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>