<?php
$this->load->view('admin/templates/header.php');
?> 	
<script type="text/javascript">
    $(document).ready(function () {
        
        var tenure_array = [];
        $("input:checkbox[class=subproduct___1]:checked").each(function(){
             tenure_array.push($(this).val());
        });
        if(tenure_array.length > 0){
        	 $('.package_discount_box').show();
        }
        $('#tab2 input[type="checkbox"]').click(function () {
            var cat = $(this).parent().attr('class');
            var curCat = cat;
            var catPos = '';
            var added = '';
            var curPos = curCat.substring(3);
            var newspan = $(this).parent().prev();
            if ($(this).is(':checked')) {
                while (cat != 'cat1') {
                    cat = newspan.attr('class');
                    catPos = cat.substring(3);
                    if (cat != curCat && catPos < curPos) {
                        if (jQuery.inArray(catPos, added.replace(/,\s+/g, ',').split(',')) >= 0) {
                            //Found it!
                        } else {
                            newspan.find('input[type="checkbox"]').attr('checked', 'checked');
                            added += catPos + ',';
                        }
                    }
                    newspan = newspan.prev();
                }
            } else {
                var newspan = $(this).parent().next();
                if (newspan.get(0)) {
                    var cat = newspan.attr('class');
                    var catPos = cat.substring(3);
                }
                while (newspan.get(0) && cat != curCat && catPos > curPos) {
                    newspan.find('input[type="checkbox"]').attr('checked', this.checked);
                    newspan = newspan.next();
                    cat = newspan.attr('class');
                    catPos = cat.substring(3);
                }
            }
        });
        
        $('#addAttr').click(function () {
            var last_count = $('#last_count').val();
		    ++last_count;
            $('<div style="float: left; margin: 12px 10px 10px; width:85%;" class="field">' +
                    '<div class="image_text" style="float: left;margin: 5px;margin-right:50px;">' +
                    '<span>Attribute Name:</span>&nbsp;' +
                    '<select name="product_attribute_type[]" style="width:200px;color:gray;width:206px;" class="chzn-select">' +
                    '<option value="">--Select--</option>' +
<?php foreach ($PrdattrVal->result() as $prdattrRow) { ?>
                '<option value="<?php echo $prdattrRow->id; ?>">\n\<?php echo $prdattrRow->attr_name; ?></option>' +
<?php } ?>
            '</select>' +
            '</div>' +
                    '<div class="attribute_box attrInput" style="float: left;margin: 5px;" >' +
                    '<span>Attribute Value:</span>&nbsp;' +
                        '<select name="product_attribute_name[]" style="width:110px;color:gray;" class="chzn-select">'+
        					<?php 
        					for($i=3; $i < 13; $i++) {  ?>
        						 '<option value="<?php echo $i.' Months'  ?>"><?php echo $i.' Months'; ?></option>'+
        						
        					<?php }	?>
        					'<option value="18 Months">18 Months</option>'+
        					'<option value="24 Months">24 Months</option>'+
        					'<option value="36 Months">36 Months</option>'+
					    '</select>'+
                    '</div>' +
                    '<div class="attribute_box attrInput" style="float: left;margin: 5px;" >' +
                    '<span>Attribute Price :</span>&nbsp;' +
                    '<input type="text" name="product_attribute_val[]" id="tenure_value_'+  last_count  +'" style="width:100px;color:gray;" class="chzn-select" />' +
                    '</div>' +
                    '<div class="btn_30_blue">' +
                    '<a href="javascript:void(0)" onclick="removeAttr(this)" id="removeAttr" class="tipTop" title="Remove this attribute">' +
                    '<span class="icon cross_co"></span>' +
                    '<span class="btn_link">Remove</span>' +
                    '</a>' +
                    '</div>' +
                    '</div>'
            ).fadeIn('slow').appendTo('.inputss');
           $('#last_count').val(last_count);
        });


        $("#product_image").change(function (e) {
            var _URL = window.URL || window.webkitURL;
            var file, img;

            if ((file = this.files[0])) {
                img = new Image();
                // console.log(img);
                img.onload = function () {
                    //alert(this.width + " " + this.height);
                    if (this.width < 550 && this.height < 1680) {
                        alert("Please Select Big images");
                        $('button').attr('disabled', 'disabled');
                    }
                };
                img.src = _URL.createObjectURL(file);


            }

        });
        
        


    });

    function removeAttr(evt) {
        var last_count = $('#last_count').val();
	    $('#last_count').val(last_count - 1);
        $(evt).parent().parent().remove();
    }
    function removeAttrDb(pid, evt) {
        var last_count = $('#last_count').val();
	    $('#last_count').val(last_count - 1);
        if (pid != '') {
            $.post(baseURL + 'admin/product/remove_attr', {pid: pid});
        }
        $(evt).parent().parent().remove();
    }



    function validate(file) {
        $('#form_submit').removeAttr('disabled', 'disabled');
        var files = document.getElementById('product_image').files;

        var x = document.getElementById("product_image").width;
        console.log(x);
        var fp = $("#product_image");
        var lg = files.length; // get length
        var items = files;
        var fileSize = 0;
        var img_height = $(this).height();
        var img_width = $(this).width();

        var arrayExtensions = ["jpg", "jpeg", "png", "bmp", "gif"];
        var ext = file.split(".");
        ext = ext[ext.length - 1].toLowerCase();

        if (lg > 0) {
            for (var i = 0; i < lg; i++) {
                fileSize = fileSize + items[i].size; // get file size
                // file_height = fileSize+items[i].height;
                //  file_width = fileSize+items[i].width;
            }

            if (arrayExtensions.lastIndexOf(ext) == -1) {
                alert("Please Select Images.");
                $("#product_image").val("");
                //location.reload();
            }

            // if(img_height < 600 && img_width < 1400) {
            //     alert('Please Select Big File');
            //     $('#product_image').val('');
            //     location.reload();    
            // }


        }

    }
    
    var CatIds = [];
    var arrayFromPHP = <?php echo json_encode($category_id); ?>;
    $.each(arrayFromPHP, function (i, elem) {
         CatIds.push(elem);
    });
    function getfilteroncategory(cat_id,value){
        if($('#'+cat_id).prop("checked") == true){
           CatIds.push(cat_id);
        } else if($('#'+cat_id).prop("checked") == false){
            var index = CatIds.indexOf(cat_id);
            if (index !== -1) CatIds.splice(index, 1);
        }
        var arrayFromPHP = <?php echo json_encode($category_id); ?>;
        $.each(arrayFromPHP, function (i, elem) {
             CatIds.push(elem);
        });
        
        
        if(CatIds.length > 0){
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: baseURL + 'admin/product/getmyfilters',
                data:{
                    'cate_ids': CatIds
                },
                success:function(response){
                    var options = '';
                    if(response.data.length > 0){
                        for (var i = 0; i < response.data.length; i++) {
                            var filter  = response.data[i];
                            options += '<option value='+ filter.filter_tag +'>'+ filter.filter_name +'</option>';
                        }
                        $('#dynamicFilters').html(options);
                    }else{
                        $('#dynamicFilters').html('<option>No Filters</option>'); 
                    }
                }
            });
        }else{
             $('#dynamicFilters').html('<option>No Filters</option>');
        }
    }
</script>
<?php
$is_this_addon = $product_details->row()->is_addon;
$is_sub_product = $product_details->row()->is_sub_product;
?>

<div id="content">
    <div class="grid_container">
        <div class="grid_12">
            <div class="widget_wrap">
                <div class="widget_top">
                    <span class="h_icon list"></span>
                    <h6>Edit Product</h6>
                    <div id="widget_tab">
                        <ul>
                            <li><a href="#tab1" class="active_tab link_tab1">Content</a></li>
                            <li><a href="#tab2" class="link_tab2">Category</a></li>
                            <li><a href="#tab3" class="link_tab3">Images</a></li>
                            <li><a href="#tab4" class="link_tab4">Lists</a></li>
                            <li><a href="#tab5">Attribute</a></li>
                            <li><a href="#tab6">SEO</a></li>
                            <?php if ($is_this_addon == 0) { ?>
                                <li><a href="#tab7" class="link_tab7">Addon Products</a></li>
                            <?php } ?>								 
                            <li><a href="#tab8" class="link_tab8">Sub Products</a></li>
                            <li><a href="#tab9" class="link_tab9">Recommended Products</a></li>
                            <li><a href="#tab10" class="link_tab10">You Might Also Like</a></li>
                        </ul>
                    </div>
                </div>
                <div class="widget_content">
                    <?php
                    $attributes = array('class' => 'form_container left_label', 'id' => 'addproduct_form', 'enctype' => 'multipart/form-data');
                    echo form_open_multipart('admin/product/insertEditProduct', $attributes);
                    /* 						$optionsArr = unserialize($product_details->row()->option);
                      if (is_array($optionsArr) && count($optionsArr)>0){
                      $options = 'available';
                      $attNameArr = $optionsArr['attribute_name'];
                      $attValArr = $optionsArr['attribute_val'];
                      $attWeightArr = $optionsArr['attribute_weight'];
                      $attPriceArr = $optionsArr['attribute_price'];
                      }else {
                     */ $options = '';
//						}
                    $list_names = $product_details->row()->list_name;
                    $list_names_arr = explode(',', $list_names);
                    $list_values = $product_details->row()->list_value;
                    $list_values_arr = explode(',', $list_values);
//						$listsArr = array_combine($list_names_arr,$list_values_arr);
//						echo "<pre>";print_r($list_names_arr);print_r($list_values_arr);print_r($listsArr);die;
                    $imgArr = explode(',', $product_details->row()->image);
                    ?>

                    <div id="tab1" class="tab_con" data-tab="link_tab1">
                        <ul>

                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="product_name">Product Name <span class="req">*</span></label>
                                    <div class="form_input">
                                        <input name="product_name" id="product_name" value="<?php echo $product_details->row()->product_name; ?>" type="text" tabindex="1" class="required large tipTop" title="Please enter the product name"/>
                                    </div>
                                </div>
                            </li> 
                            <!--added by sandeep start-->
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="product_name">Product Order </label>
                                    <div class="form_input">
                                        <input name="product_order" id="product_order" value="<?php echo $product_details->row()->product_order; ?>" type="text" tabindex="2" class="large tipTop" title="Please enter the product Order"/>
                                    </div>
                                </div>
                            </li>
                            <!--added by sandeep end-->

                            <li> 


                                <div class="form_grid_12">
                                    <label class="field_title" for="description">Description<span class="req">*</span></label>
                                    <div class="form_input">
                                        <textarea name="description" id="description" tabindex="3" style="width:370px;" class="required large tipTop mceEditor" title="Please enter the product description"><?php echo $product_details->row()->description; ?></textarea>
                                    </div>
                                </div>
                            </li>

                            <li>
								<div class="form_grid_12">
								<label class="field_title" for="description">Excerpt</label>
								<div class="form_input">
								<textarea name="excerpt" id="excerpt" tabindex="4" style="width:370px;" class="large tipTop mceEditor" title="Please enter the product Excerpt"><?php echo $product_details->row()->excerpt;?></textarea>
								</div>
								</div>
							</li>
                            
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="description">Dispaly Excerpt?</label>
                                    <div class="form_input">
                                        <input type="checkbox" name="display_excerpt" value="yes" id="is_addon" <?php if ($product_details->row()->display_excerpt == 'yes') { echo 'checked="checked"'; } ?> />                                        
                                    </div>
                                </div>
                            </li>   
                            
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="shipping_policies">Shipping &amp; Policies<span class="req">*</span></label>
                                    <div class="form_input">
                                        <textarea name="shipping_policies" id="shipping_policies" tabindex="5" style="width:370px;" class="large tipTop mceEditor" title="Please enter the product shipping &amp; policies"><?php echo $product_details->row()->shipping_policies; ?></textarea>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="quantity">Quantity<span class="req">*</span></label>
                                    <div class="form_input">
                                        <input type="text" name="quantity" id="quantity" value="<?php echo $product_details->row()->quantity; ?>" tabindex="6" class="required number large tipTop" title="Please enter the product quantity" />
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <div class="form_fields">
                                        <label class="field_title" for="description"><?php if ($this->lang->line('shipping_in') != '') {
                        echo stripslashes($this->lang->line('shipping_in'));
                    } else echo "Shipping in"; ?></label>
                                        <div class="form_input">
<?php $shipping = $product_details->row()->shipping; ?>
                                            <select name="shipping">
                                                <option value="<?php $value = '7';
echo $value; ?>"<?php if ($value == $shipping) {
    echo 'selected="selected"';
} ?>><?php if ($this->lang->line('shipping_in_option_hours') != '') {
    echo stripslashes($this->lang->line('shipping_in_option_hours'));
} else echo "72 hours"; ?></option>
                                                <option value="<?php $value = '1';
echo $value; ?>"<?php if ($value == $shipping) {
    echo 'selected="selected"';
} ?>><?php if ($this->lang->line('shipping_in_option_first') != '') {
    echo stripslashes($this->lang->line('shipping_in_option_first'));
} else echo "1-3 days"; ?></option>
                                                <option value="<?php $value = '2';
echo $value; ?>"<?php if ($value == $shipping) {
    echo 'selected="selected"';
} ?>><?php if ($this->lang->line('shipping_in_option_second') != '') {
    echo stripslashes($this->lang->line('shipping_in_option_second'));
} else echo "4-7 days"; ?></option>
                                                <option value="<?php $value = '3';
echo $value; ?>"<?php if ($value == $shipping) {
    echo 'selected="selected"';
} ?>><?php if ($this->lang->line('shipping_in_option_third') != '') {
    echo stripslashes($this->lang->line('shipping_in_option_third'));
} else echo "2-3 weeks"; ?></option>
                                                <option value="<?php $value = '4';
echo $value; ?>"<?php if ($value == $shipping) {
    echo 'selected="selected"';
} ?>><?php if ($this->lang->line('shipping_in_option_fourth') != '') {
    echo stripslashes($this->lang->line('shipping_in_option_fourth'));
} else echo "3-4 weeks"; ?></option>
                                                <option value="<?php $value = '5';
echo $value; ?>"<?php if ($value == $shipping) {
    echo 'selected="selected"';
} ?>><?php if ($this->lang->line('shipping_in_option_fifth') != '') {
    echo stripslashes($this->lang->line('shipping_in_option_fifth'));
} else echo "4-6 weeks"; ?></option>
                                                <option value="<?php $value = '6';
echo $value; ?>"<?php if ($value == $shipping) {
    echo 'selected="selected"';
} ?>><?php if ($this->lang->line('shipping_in_option_sixth') != '') {
    echo stripslashes($this->lang->line('shipping_in_option_sixth'));
} else echo "6-8 weeks"; ?></option>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="shipping_cost">Security Deposit</label>
                                    <div class="form_input">
                                        <input type="text" name="shipping_cost" id="shipping_cost" tabindex="4" class="large tipTop" title="Please enter the product shipping cost" value="<?php echo $product_details->row()->shipping_cost; ?>" />
                                    </div>
                                </div>
                            </li>
                            </li>

                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="sku">Min Tenure</label>
                                    <div class="form_input">
                                        <input type="text" name="min_tenure" id="tenure" tabindex="7" 
                                               value="<?php echo $product_details->row()->min_tenure; ?>" class="large tipTop" title="Please enter the min tenure" />
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="sku">Rental Freq</label>
                                    <div class="form_input">
                                        <input type="text" name="rental_freq" id="freq" tabindex="7" 
                                               value="<?php echo $product_details->row()->rental_freq; ?>" class="large tipTop" title="Please enter the rental freq" />
                                    </div>
                                </div>
                            </li>

                            <!--	<li>
                                            <div class="form_grid_12">
                                            <label class="field_title" for="shipping_cost">Immediate Shipping</label>
                                            <div class="form_input">
                                            <input type="radio" name="ship_immediate" value="true" <?php if ($product_details->row()->ship_immediate == 'true') { ?> checked="checked"<?php } ?> />Yes&nbsp;&nbsp;&nbsp;
                                            <input type="radio" name="ship_immediate" value="false" <?php if ($product_details->row()->ship_immediate == 'false') { ?> checked="checked"<?php } ?> />No
                                            </div>
                                            </div>
                                    </li> -->
                            <!--                             
                                                                                     <li>
                                                                                            <div class="form_grid_12">
                                                                                            <label class="field_title" for="shipping_cost">Shipping cost</label>
                                                                                            <div class="form_input">
                                                            <input type="text" name="shipping_cost" id="shipping_cost" value="<?php echo $product_details->row()->shipping_cost; ?>" tabindex="4" class="large tipTop" title="Please enter the product shipping cost" />
                                                                                            </div>
                                                                                            </div>
                                                                                    </li>
                                                        
                                                         <li>
                                                                                            <div class="form_grid_12">
                                                                                            <label class="field_title" for="tax_cost">Tax</label>
                                                                                            <div class="form_input">
                                                            <input type="text" name="tax_cost" id="tax_cost"  value="<?php echo $product_details->row()->tax_cost; ?>" tabindex="4" class="large tipTop" title="Please enter the product tax" />
                                                                                            </div>
                                                                                            </div>
                                                                                    </li>
                            -->								
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="sku">SKU</label>
                                    <div class="form_input">
                                        <input type="text" name="sku" id="sku" tabindex="8" value="<?php echo $product_details->row()->sku; ?>" class="required large tipTop" title="Please enter the product sku" />
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="weight">Size</label>
                                    <div class="form_input">
                                        <input type="text" name="weight" id="weight" tabindex="9" value="<?php echo $product_details->row()->weight; ?>" class="large tipTop" title="Please enter the product Size" />
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="brand">Brand</label>
                                    <div class="form_input">
                                        <input type="text" name="brand" id="brand" tabindex="9" value="<?php echo $product_details->row()->brand; ?>" class="large tipTop" title="Please enter the product Brand" />
                                    </div>
                                </div>
                            </li>
                            
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="material">Material</label>
                                    <div class="form_input">
                                        <input type="text" name="material" id="material" tabindex="8" value="<?php echo $product_details->row()->material; ?>" class="large tipTop" title="Please enter the product Material" />
                                    </div>
                                </div>
                            </li>
                            
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="colour">Colour</label>
                                    <div class="form_input">
                                        <input type="text" name="colour" id="colour" tabindex="8" value="<?php echo $product_details->row()->colour; ?>" class="large tipTop" title="Please enter the product Colour" />
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="description">Price<span class="req">*</span></label>
                                    <div class="form_input">
                                        <input type="text" name="price" id="price" tabindex="10" value="<?php echo $product_details->row()->price; ?>" class="required number large tipTop" title="Please enter the product price" />
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="description">Sale Price<span class="req">*</span></label>
                                    <div class="form_input">
                                        <input type="text" name="sale_price" id="sale_price" tabindex="11" value="<?php echo $product_details->row()->sale_price; ?>" class="required number smallerThan large tipTop" data-min="price" title="Please enter the product sale price" />
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="admin_name">Status <span class="req">*</span></label>
                                    <div class="form_input">
                                        <div class="publish_unpublish">
                                            <input type="checkbox" tabindex="12" name="status" <?php if ($product_details->row()->status == 'Publish') {
    echo 'checked="checked"';
} ?> id="publish_unpublish_publish" class="publish_unpublish"/>
                                        </div>
                                    </div>
                                </div>
                            </li>

                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="isaddon">Is this Addon? </label>
                                    <div class="form_input">

                                        <input type="checkbox" tabindex="13" name="is_addon"  id="is_addon" <?php if ($product_details->row()->is_addon == '1') {
    echo 'checked="checked"';
} ?>/>

                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title">Publish Slider View ? </label>
                                    <div class="form_input">

                                        <input type="checkbox" tabindex="13" name="slider_view" id="slider_view" <?php if ($product_details->row()->slider_view == 'yes') {
    echo 'checked="checked"';
} ?> />

                                    </div>
                                </div>
                            </li>								

                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="isaddon">
                                        Is this Sub Product? </label>
                                    <div class="form_input">
                                        <input type="checkbox" tabindex="13" name="is_sub_product"  id="is_sub_product"
<?php if ($product_details->row()->is_sub_product == '1') {
    echo 'checked="checked"';
} ?>
                                               />
                                    </div>
                                </div>
                            </li>
                            
                            <li>
								<div class="form_grid_12">
									<label class="field_title" for="isaddon">
											Is this Package? </label>
									<div class="form_input">
										<input type="checkbox" tabindex="13" name="is_package" value="1"  id="is_package" onchange="show_discount_box()" <?php if($product_details->row()->subproducts != ''){ echo 'checked="checked"';}  ?> />
								        <input type="hidden" tabindex="13"   id="total_subproduct_price"/>
									</div>
								</div>
							</li>
							
							 <li>
								<div class="form_grid_12">
									<label class="field_title" for="product_label">
										Product Label </label>
									<div class="form_input">
									   <input type="radio" value="new" name="product_label" <?php if($product_details->row()->product_label == 'new') { echo "checked";  } ?> >New 
									   <input type="radio" value="best_seller" name="product_label" <?php if($product_details->row()->product_label == 'best_seller') { echo "checked";  } ?> > Best Seller
									   <input type="radio" value="" name="product_label">NULL
								    </div>
								</div>
							</li>
							
							
							<li class="package_discount_box" style="display: none;">
								<div class="form_grid_12">
									<label class="field_title" for="description">Package Discount</label>
									<div class="form_input">
									    <input type="number" name="package_discount" placeholder="For Package Only" id="package_discount" tabindex="9" class="number large tipTop" title="Please enter the package discount" value="<?php echo $product_details->row()->package_discount; ?>" />
								    </div>
								</div>
							</li>

                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input">
                                        <button type="submit" class="btn_small btn_blue" tabindex="9"><span>Update</span></button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div id="tab2" class="tab_con" data-tab="link_tab2">
                        <div class="cateogryView">
                                            <?php echo $categoryView; ?>
                            <div class="form_grid_12"  style="padding-top: 22px; padding-bottom: 16px;">
                                <div class="form_input">
                                    <select name="filters[]" class="large tipTop" id="dynamicFilters" multiple style="width: 400px; height: 166px;">
                                        <?php if($filters->num_rows() > 0){
                                            $myFilters = explode(',',$product_details->row()->filter_tag);
                                            foreach($filters->result() as $key => $val){ ?>
                                               <option value="<?php echo $val->filter_tag; ?>" <?php if($val->filter_tag == $myFilters[$key]) { echo "selected"; } ?>><?php echo $val->filter_name; ?></option>
                                        <?php  } } ?>
                                    </select>
                                </div>
                            </div>
                                         
                        </div>
                        <button type="submit" style="margin: 20px 5px;" class="btn_small btn_blue" tabindex="9"><span>Update</span></button>
                    </div>
                    <div id="tab3" class="tab_con" data-tab="link_tab3">
                        <ul>
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="product_image">Product Image</label>
                                    <div class="form_input">
                                        <input name="product_image[]" onchange="validate(this.value)"  id="product_image" type="file" tabindex="7" class="large multi tipTop" title="Please select product image"/><span class="input_instruction green">You Can Upload Multiple Images</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="widget_content">
                                    <table class="display display_tbl" id="image_tbl">
                                        <thead>
                                            <tr>
                                                <th class="center">
                                                    Sno
                                                </th>
                                                <th>
                                                    Image
                                                </th>
                                                <th>
                                                    Position
                                                </th>
                                                <th>
                                                    Action
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
<?php
if (count($imgArr) > 0) {
    $i = 0;
    $j = 1;
    $this->session->set_userdata(array('product_image_' . $product_details->row()->id => $product_details->row()->image));
    foreach ($imgArr as $img) {
        if ($img != '') {
            ?>
                                                        <tr id="img_<?php echo $i ?>">
                                                            <td class="center tr_select ">
                                                                <input type="hidden" name="imaged[]" value="<?php echo $img; ?>"/>
            <?php echo $j; ?>
                                                            </td>
                                                            <td class="center">
                                                                <img src="<?php echo CDN_URL; ?>images/product/<?php echo $img; ?>"  height="80px" width="80px" />
                                                            </td>
                                                            <td class="center">
                                                                <span>
                                                                    <input type="text" style="width: 15%;" name="changeorder[]" value="<?php echo $i; ?>" size="3" />
                                                                </span>
                                                            </td>
                                                            <td class="center">
                                                                <ul class="action_list" style="background:none;border-top:none;"><li style="width:100%;"><a class="p_del tipTop" href="javascript:void(0)" onclick="editPictureProducts(<?php echo $i; ?>,<?php echo $product_details->row()->id; ?>);" title="Delete this image">Remove</a></li></ul>
                                                            </td>
                                                        </tr>
            <?php
            $j++;
        }
        $i++;
    }
}
?>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th class="center">
                                                    Sno
                                                </th>
                                                <th>
                                                    Image
                                                </th>
                                                <th>
                                                    Position
                                                </th>
                                                <th>
                                                    Action
                                                </th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input">
                                        <button type="submit" onclick="check_image()" id="form_submit"  class="btn_small btn_blue" tabindex="9"><span>Update</span></button>
                                    </div>
                                </div>
                            </li>     
                        </ul>          
                    </div>
                    <div id="tab4" class="tab_con" data-tab="link_tab4">

                        <ul id="AttributeView">

                            <li>
                                <div class="inputs" style="float: left;width:100%; border:1px dashed #1DB3F0;">
                                    <div style="margin:12px;">
                                        <div class="btn_30_blue">
                                            <a href="javascript:void(0)" id="add" class="tipTop" title="Add new attribute">
                                                <span class="icon add_co"></span>
                                                <span class="btn_link">Add</span>
                                            </a>
                                        </div>
                                        <div class="btn_30_blue">
                                            <a href="javascript:void(0)" id="remove" class="tipTop" title="Remove last attribute">
                                                <span class="icon cross_co"></span>
                                                <span class="btn_link">Remove</span>
                                            </a>
                                        </div>
                                    </div>
<?php
//							            if ($options != '' && is_array($attNameArr) && count($attNameArr)>0){
//							            	for ($i=0;$i<count($attNameArr);$i++){
if (count($list_names_arr) > 0) {
    foreach ($list_names_arr as $list_names_key => $list_names_val) {
        ?>
                                            <div style="float: left; margin: 12px 10px 10px; width:85%;" class="field">
                                                <div class="image_text" style="float: left;margin: 5px;margin-right:50px;">
                                                    <span>List Name:</span>
                                                    <select name="attribute_name[]" onchange="javascript:changeListValues(this, '<?php echo $list_values_arr[$list_names_key]; ?>')" style="width:200px;color:gray;width:206px;" class="">
                                            <?php
                                            foreach ($atrributeValue->result() as $attrRow) {
                                                if (strtolower($attrRow->attribute_name) != 'price') {
                                                    ?>
                                                                <option <?php if ($list_names_val == $attrRow->id) {
                                        echo 'selected="selected"';
                                    } ?> value="<?php echo $attrRow->id; ?>"><?php echo $attrRow->attribute_name; ?></option>
            <?php }
        } ?>
                                                    </select>
                                                </div>
                                                <div class="attribute_box attrInput" style="float: left;margin: 5px;" >
                                                    <span>List Value :</span>&nbsp;
                                                    <select name="attribute_val[]" style="width:200px;color:gray;width:206px;">
                                                    </select>
                                                </div>

                                            </div>
        <?php
    }
}
?>
                                </div>

                                <button type="submit" style="margin-top: 20px;" class="btn_small btn_blue" tabindex="9"><span>Update</span></button>
                            </li>


                        </ul>

                    </div>
                    <div id="tab5">

                        <ul id="AttributeView">

                            <li>
                                <div class="inputss" style="float: left;width:100%; border:1px dashed #1DB3F0;">
                                    <div style="margin:12px;">
                                        <div class="btn_30_blue">
                                            <a href="javascript:void(0)" id="addAttr" class="tipTop" title="Add new attribute">
                                                <span class="icon add_co"></span>
                                                <span class="btn_link">Add</span>
                                            </a>
                                        </div>

                                    </div>
<?php
//onchange="javascript:changeListValues123(this,'<?php echo $SubPrdValS['attr_price'];');"
if (count($SubPrdVal) > 0) {
    $j = 1;

    foreach ($SubPrdVal->result_array() as $SubPrdValS) {
        ?>
                                            <div style="float: left; margin: 12px 10px 10px; width:85%;" class="field1">
                                                <div class="image_text" style="float: left;margin: 5px;margin-right:50px;">
                                                    <span>Attribute Name:</span>
                                                    <select name="attr_type1[]" style="width:200px;color:gray;" onchange="javascript:ajaxEditproductAttribute(this.value, '<?php echo $SubPrdValS['attr_name']; ?>', '<?php echo $SubPrdValS['attr_price']; ?>', '<?php echo $SubPrdValS['pid']; ?>');" >
        <?php foreach ($PrdattrVal->result() as $prdattrRow) { ?>
                                                            <option value="<?php echo $prdattrRow->id; ?>" <?php if ($prdattrRow->id == $SubPrdValS['attr_id']) {
                echo 'selected="selected"';
            } ?> ><?php echo $prdattrRow->attr_name; ?></option>
        <?php } ?>
                                                    </select>
                                                </div>
                                                <div class="attribute_box attrInput" style="float: left;margin: 5px;" >
                                                    <span>Attribute Value:</span>&nbsp;
                                                  <select  name="attr_name1[]"   style="width:110px;color:gray;">
                                					<?php for ($i=3; $i < 13; $i++) {   ?>
                                					<option value="<?php echo $i.' Months' ?>" <?php if(strtolower($SubPrdValS['attr_name'])  ==  $i.' months'){ echo 'selected'; } ?>><?php echo $i; ?> Months</option>
                                					<?php } ?>
                                					<option value="14 Months" <?php if(strtolower($SubPrdValS['attr_name']) == '14 months'){ echo 'selected'; } ?> >14 Months</option>
                                					<option value="18 Months" <?php if(strtolower($SubPrdValS['attr_name']) == '18 months'){ echo 'selected'; } ?> >18 Months</option>
                                					<option value="24 Months" <?php if(strtolower($SubPrdValS['attr_name'])== '24 months'){ echo 'selected'; } ?> >24 Months</option>
                                					<option value="36 Months" <?php if(strtolower($SubPrdValS['attr_name']) == '36 months'){ echo 'selected'; } ?> >36 Months</option>
                                				</select>
                                                </div>
                                                <div class="attribute_box attrInput" style="float: left;margin: 5px;" >
                                                    <span>Attribute Price :</span>&nbsp;
                                                    <input type="text"  class="attribute_price" data-attribute_id="<?php echo $SubPrdValS['attr_id']; ?>" data-name="<?php echo $SubPrdValS['attr_name']; ?>" data-pid="<?php echo $SubPrdValS['pid']; ?>" name="attr_val1[]" id="tenure_value_<?php echo $j; ?>" style="width:100px;color:gray;" value="<?php echo $SubPrdValS['attr_price']; ?>" onchange="javascript:ajaxEditproductAttribute('<?php echo $SubPrdValS['attr_id']; ?>', '<?php echo $SubPrdValS['attr_name']; ?>', this.value, '<?php echo $SubPrdValS['pid']; ?>');" />
                                                </div>
                                                <div id="loadingImg_<?php echo $SubPrdValS['pid']; ?>"></div>
                                                <div class="btn_30_blue">
                                                    <a href="javascript:void(0)" onclick="removeAttrDb('<?php echo $SubPrdValS['pid']; ?>', this)" id="removeAttr" class="tipTop" title="Remove this attribute">
                                                        <span class="icon cross_co"></span>
                                                        <span class="btn_link">Remove</span>
                                                    </a>
                                                </div>
                                            </div>
    <?php  $j++; }
} ?>
<input type="hidden" value="<?php echo count($SubPrdVal->result_array()); ?>" id="last_count">
                                </div>
                                <button type="submit" style="margin-top: 20px;" class="btn_small btn_blue" tabindex="9"><span>Update</span></button>
                            </li>
                            <li class="package_discount_box" style="display: none;">
			                    <button type="button" class="btn_small btn_blue" style="float: right;" onclick="return Calculate_amount_edit()">Calculate Price</button>
		                    </li>


                        </ul>

                    </div>
                    <div id="tab6" class="tab_con" data-tab="link_tab5">
                        <ul>
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="meta_title">Meta Title</label>
                                    <div class="form_input">
                                        <input name="meta_title" id="meta_title" value="<?php echo $product_details->row()->meta_title; ?>" type="text" tabindex="1" class="large tipTop" title="Please enter the page meta title"/>
                                    </div>
                                </div>
                            </li>
                           <li>
                              <div class="form_grid_12">
                                <label class="field_title" for="meta_tag">Meta Keyword</label>
                                <div class="form_input">
                                  <textarea name="meta_keyword" id="meta_keyword"  tabindex="2" class="large tipTop mceEditor" title="Please enter the page meta keyword"><?php echo $product_details->row()->meta_keyword;?></textarea>
                                </div>
                              </div>
                            </li>
                            <li>
                                <div class="form_grid_12">
                                    <label class="field_title" for="meta_description">Meta Description</label>
                                    <div class="form_input">
                                        <textarea name="meta_description" id="meta_description" tabindex="3" class="large tipTop" title="Please enter the meta description"><?php echo $product_details->row()->meta_description; ?></textarea>
                                    </div>
                                </div>
                            </li>
                            <li>
            					<div class="form_grid_12">
            						<label class="field_title" for="header_code_snippet">Header Code snippet</label>
            						<div class="form_input">
            							<textarea name="header_code_snippet" id="header_code_snippet" tabindex="1" class="large tipTop" title="Please enter the header code snippet"><?php echo $product_details->row()->header_code_snippet;?></textarea>
            						</div>
            					</div>
            				</li>
                        </ul>
                        <ul><li><div class="form_grid_12">
                                    <div class="form_input">
                                        <button type="submit" class="btn_small btn_blue" tabindex="4"><span>Update</span></button>
                                    </div>
                                </div></li></ul>
                    </div>
                                    <?php if ($is_this_addon == 0) { ?> 
                        <div id="tab7" class="tab_con" data-tab="link_tab7">
                            <div class="widget_content">
                                <table class="display display_tbl" id="image_tbl">
                                    <thead>
                                        <tr>

                                            <th class="tip_top">
                                                <div style="text-align:left;"> Product Name </div>
                                            </th>

                                            <th class="tip_top">
                                                <div style="text-align:left;">SKU</div>
                                            </th>
                                            <th class="tip_top">
                                                <div style="text-align:left;">Image</div>
                                            </th>
                                            <th class="center">
                                                    <!--<input name="checkbox_id[]" type="checkbox" value="on" class="checkall">-->
                                            </th>									
                                        </tr>
                                    </thead>
                                    <tbody>	
    <?php
    //var_dump($addon_prod_list);
    foreach ($addon_prod_list as $addon) {
        $addon_explode = explode(',', $addon['image']);
        $addon_prod_img = $addon_explode[0];
        $addon_prod_ids = array();
        $addon_prod_ids = explode(",", $product_details->row()->addons);
        ?>


                                            <tr>									
                                                <td class="tr_select">
        <?php echo $addon['product_name']; ?>
                                                </td>
                                                <td class="tr_select">
                                            <?php echo $addon['sku']; ?>
                                                </td>
                                                <td class="tr_select">
                                                    <div class="widget_thumb" style="text-align:left;">
                                                        <img width="40px" height="40px" src="<?php echo base_url(); ?>images/product/<?php echo $addon_prod_img; ?>" />
                                                    </div>
                                                </td>
                                                <td class="center">

                                                    <input name="addon_checkbox_id[]" type="checkbox" <?php if (in_array($addon['id'], $addon_prod_ids, true)) {
                                        echo "checked='checked'";
                                    } ?> value="<?php echo $addon['id']; ?>">
                                                </td>									
                                            </tr>
                                                <?php } ?>								
                                    </tbody>						
                                </table>
                            </div>
                            <ul>
                                <li>
                                    <div class="form_grid_12">
                                        <div class="form_input">
                                            <button type="submit" class="btn_small btn_blue" 
                                                    tabindex="4"><span>Update</span>
                                            </button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                                    <?php } ?>


                    <div id="tab8" class="tab_con" data-tab="link_tab8">
                        <div class="widget_content">
                            <table class="display display_tbl" id="image_tbl">
                                <thead>
                                    <tr>									
                                        <th class="tip_top">
                                            <div style="text-align:left;"> Product Name </div>
                                        </th>									
                                        <th class="tip_top">
                                            <div style="text-align:left;">SKU</div>
                                        </th>
                                        <th class="tip_top">
                                            <div style="text-align:left;">Image</div>
                                        </th>
                                        <th class="tip_top">
										 <div style="text-align:left;">Quantity</div>
									    </th>
                                        <th class="center">
                                                <!--<input name="checkbox_id[]" type="checkbox" value="on" class="checkall">-->
                                        </th>									
                                    </tr>
                                </thead>
                                <tbody>	
<?php
//print_r($sub_products_list);exit;
$product_quantity = unserialize($product_details->row()->subproduct_quantity);
foreach ($sub_products_list as $subproduct) {
    $total_quantity = unserialize($subproduct['subproduct_quantity']);
    $subproduct_explode = explode(',', $subproduct['image']);
    $subproduct_prod_img = $subproduct_explode[0];
    $subproduct_prod_ids = array();
    $subproduct_prod_ids = explode(",", $product_details->row()->subproducts);
    ?>


                                        <tr>									
                                            <td class="tr_select">
    <?php echo $subproduct['product_name']; ?>
                                            </td>
                                            <td class="tr_select">
    <?php echo $subproduct['sku']; ?>
                                            </td>
                                            <td class="tr_select">
                                                <div class="widget_thumb" style="text-align:left;">
                                                    <img width="40px" height="40px" src="<?php echo base_url(); ?>images/product/<?php echo $subproduct_prod_img; ?>" />
                                                </div>
                                            </td>
                                            <td class="tip_top">
										            <input type="text"  value="<?php echo $product_quantity[$subproduct['id']]; ?>" id="package_quantity_<?php echo $subproduct['id']; ?>" placeholder="Enter Quantity" style="width: 100px;">
										    </td>
                                            <td class="center">
                                               <input name="subproduct_checkbox_id[]" id="package_id_<?php echo $subproduct['id']; ?>"  class="subproduct___1" type="checkbox" <?php if (in_array($subproduct['id'], $subproduct_prod_ids,true)) { echo "checked='checked'";}?> value="<?php echo $subproduct['id'];?>">
                                            </td>									
                                        </tr>
<?php } ?>								
                                </tbody>						
                            </table>
                        </div>
                        <ul>
                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input">
                                        <button type="submit" class="btn_small btn_blue" 
                                                tabindex="4"><span>Update</span>
                                        </button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div id="tab9" class="tab_con" data-tab="link_tab9">
                        <div class="widget_content">
                            <table class="display display_tbl" id="image_tbl">
                                <thead>
                                    <tr>									
                                        <th class="tip_top">
                                            <div style="text-align:left;"> Product Name </div>
                                        </th>									
                                        <th class="tip_top">
                                            <div style="text-align:left;">SKU</div>
                                        </th>
                                        <th class="tip_top">
                                            <div style="text-align:left;">Image</div>
                                        </th>
									    </th>
                                        <th class="center">
                                                <!--<input name="checkbox_id[]" type="checkbox" value="on" class="checkall">-->
                                        </th>									
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if(!empty($allProducts)){
                                            $productData = $product_details->result();
                                            $recommended_product_data = $productData[0]->recommended_products;
                                            $recommended_product_arr = explode(',', $recommended_product_data);
                                            foreach($allProducts as $recommended_pro){
                                                $recommended_pro_img = explode(',', $recommended_pro->image);
                                    ?>
                                                <tr>
                                                    <td class="tr_select">
                                                        <?php echo $recommended_pro->product_name; ?>
                                                    </td>
                                                    <td class="tr_select">
                                                        <?php echo $recommended_pro->sku; ?>
                                                    </td>
                                                    <td class="tr_select">
                                                        <div class="widget_thumb" style="text-align:left;">
                                                            <img width="40px" height="40px" src="<?php echo base_url(); ?>images/product/<?php echo $recommended_pro_img[0]; ?>" />
                                                        </div>
                                                    </td>
                                                    <td class="center">
                                                       <input name="recommended_products[]" id="recommended_product_<?php echo $recommended_pro->id; ?>"  class="recommended_product" type="checkbox"  value="<?php echo $recommended_pro->id;?>" <?php echo in_array($recommended_pro->id, $recommended_product_arr) ? 'checked' : ''; ?>>
                                                    </td>
                                                </tr>
                                    <?php
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <ul>
                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input">
                                        <button type="submit" class="btn_small btn_blue" 
                                                tabindex="4"><span>Update</span>
                                        </button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div id="tab10" class="tab_con" data-tab="link_tab10">
                        <div class="widget_content">
                            <table class="display display_tbl" id="image_tbl">
                                <thead>
                                    <tr>									
                                        <th class="tip_top">
                                            <div style="text-align:left;"> Product Name </div>
                                        </th>									
                                        <th class="tip_top">
                                            <div style="text-align:left;">SKU</div>
                                        </th>
                                        <th class="tip_top">
                                            <div style="text-align:left;">Image</div>
                                        </th>
									    </th>
                                        <th class="center">
                                                <!--<input name="checkbox_id[]" type="checkbox" value="on" class="checkall">-->
                                        </th>									
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        if(!empty($allProducts)){
                                            $productData = $product_details->result();
                                            $you_might_also_like_data = $productData[0]->you_might_also_like;
                                            $you_might_also_like_data_arr = explode(',', $you_might_also_like_data);
                                            foreach($allProducts as $val){
                                                $you_might_also_like_img = explode(',', $val->image);
                                    ?>
                                                <tr>
                                                    <td class="tr_select">
                                                        <?php echo $val->product_name; ?>
                                                    </td>
                                                    <td class="tr_select">
                                                        <?php echo $val->sku; ?>
                                                    </td>
                                                    <td class="tr_select">
                                                        <div class="widget_thumb" style="text-align:left;">
                                                            <img width="40px" height="40px" src="<?php echo base_url(); ?>images/product/<?php echo $you_might_also_like_img[0]; ?>" />
                                                        </div>
                                                    </td>
                                                    <td class="center">
                                                       <input name="you_might_also_like[]" id="you_might_also_like_<?php echo $val->id; ?>"  class="you_might_also_like" type="checkbox"  value="<?php echo $val->id;?>" <?php echo in_array($val->id, $you_might_also_like_data_arr) ? 'checked' : ''; ?>>
                                                    </td>
                                                </tr>
                                    <?php
                                            }
                                        }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <ul>
                            <li>
                                <div class="form_grid_12">
                                    <div class="form_input">
                                        <button type="submit" class="btn_small btn_blue" 
                                                tabindex="4"><span>Update</span>
                                        </button>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <input type="hidden" name="productID"  id="productID" 
                           value="<?php echo $product_details->row()->id; ?>"/>  

                    </form>
                </div>
            </div>
        </div>
    </div>
    <span class="clear"></span>
</div>
</div>
<script>
    $(document).ready(function () {


        var i = 1;


        $('#add').click(function () {
            $('<div style="float: left; margin: 12px 10px 10px; width:85%;" class="field">' +
                    '<div class="image_text" style="float: left;margin: 5px;margin-right:50px;">' +
                    '<span>List Name:</span>&nbsp;' +
                    '<select name="attribute_name[]" onchange="javascript:loadListValues(this)" style="width:200px;color:gray;width:206px;" class="chzn-select">' +
                    '<option value="">--Select--</option>' +
<?php
foreach ($atrributeValue->result() as $attrRow) {
    if (strtolower($attrRow->attribute_name) != 'price') {
        ?>
                    '<option value="<?php echo $attrRow->id; ?>"><?php echo $attrRow->attribute_name; ?></option>' +
    <?php }
} ?>
            '</select>' +
            '</div>' +
                    '<div class="attribute_box attrInput" style="float: left;margin: 5px;" >' +
                    '<span>List Value :</span>&nbsp;' +
                    '<select name="attribute_val[]" style="width:200px;color:gray;width:206px;" class="chzn-select">' +
                    '<option value="">--Select--</option>' +
                    '</select>' +
                    '</div>' +
                    '</div>'
            ).fadeIn('slow').appendTo('.inputs');
            i++;
        });

        $('#remove').click(function () {
            $('.field:last').remove();
        });

        $('#reset').click(function () {
            $('.field').remove();
            $('#add').show();
            i = 0;


        });


    });
    $.validator.addMethod("smallerThan", function (value, element, param) {
        var $element = $(element)
                , $min;

        if (typeof (param) === "string") {
            $min = $(param);
        } else {
            $min = $("#" + $element.data("min"));
        }

        if (this.settings.onfocusout) {
            $min.off(".validate-smallerThan").on("blur.validate-smallerThan", function () {
                $element.valid();
            });
        }
        return parseFloat(value) <= parseFloat($min.val());
    }, "Sale price must be smaller than price");
    $.validator.addClassRules({
        smallerThan: {
            smallerThan: true
        }
    });
    $('#addproduct_form').validate({
        ignore: '',
        errorPlacement: function (error, element) {
            error.appendTo(element.parent());
            $('.tab_con').hide();
            $('#widget_tab ul li a').removeClass('active_tab');
            var $link_tab = $('label.error:first').parents('.tab_con').show().data('tab');
            $('.' + $link_tab).addClass('active_tab');
        }
    });
</script>
<?php
$this->load->view('admin/templates/footer.php');
?>