<?php
$this->load->view('admin/templates/header.php');
?>
<script type="text/javascript">

$(document).ready(function() {
  $("#update_caption").validate({
    rules: {
    	heading: {
                required:true,
                minlength: 2,
                maxlength: 100
            },
    	  caption: {
                required:true,
                minlength: 2,
                maxlength: 40
            },
            discount: {
                required: true,
                range:[1,100]
            },
            url: {
                required: true,
                minlength: 2,
                maxlength: 255
            },
    },
    messages: {
    		heading: {
                required : "<font color='red'>Heading cannot be empty.</font>",
                minlength : "<font color='red'>Heading has atleast 2 character long.</font>",
                maxlength : "<font color='red'>Heading length should be 100 characters maximum.</font>"
            },

    		caption: {
                required : "<font color='red'>Caption cannot be empty.</font>",
                minlength : "<font color='red'>Caption has atleast 2 character long.</font>",
                maxlength : "<font color='red'>Caption length should be 40 characters maximum.</font>"
            },
             discount: {
                required: "<font color='red'>Discount Required.</font>",
                range: 	"<font color='red'>Please Select Discount Between 1 To 100.</font>"
            },
              url: {
                required: "<font color='red'>Url Required.</font>",
                minlength : "<font color='red'>Url has atleast 2 character long.</font>",
                maxlength : "<font color='red'>Url length should be 40 characters maximum.</font>"
            },
    },
    errorElement: "span",
        errorPlacement: function(error, element) {
                error.appendTo(element.parent());
        }

  });
});
</script>

<?php
if($_SESSION['insert_success_msg']){?>
<center><span style="color: blue;">
<?php echo $_SESSION['insert_success_msg'];}
unset($_SESSION['insert_success_msg']);?>
</span></center>
<?php 
if($_SESSION['insert_error_msg']){?>
<center><span style="color: blue;">
<?php echo $_SESSION['insert_error_msg'];}
unset($_SESSION['insert_error_msg']);?>
</span></center>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Update Your Caption</h6>
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'update_caption');
						echo form_open('admin/product/insertcaption',$attributes) 
					?>
	 					<ul>
                            <input type="hidden" name="id" value="<?php echo $caption->id; ?>" />
                             <li>
								<div class="form_grid_12">
									<label class="field_title" for="heading">Heading<span class="req">*</span></label>
									<div>
										<input name="heading" id="heading" type="text" tabindex="2" class="required small tipTop" title="Please Enter the Heading" 
										value="<?php if($caption->heading !=""){ echo $caption->heading; } ?>" />
									</div>
								</div>
							</li>
                            <li>
								<div class="form_grid_12">
									<label class="field_title" for="user_name">Caption <span class="req">*</span></label>
									<div>
										<input name="caption" id="caption" type="text" tabindex="2" class="required small tipTop" title="Please Enter the Caption" 
										value="<?php if($caption->caption !=""){ echo $caption->caption; } ?>" />
									</div>
								</div>
							</li>                                                               
                            <li>
								<div class="form_grid_12">
                                    <label class="field_title">Insert Total Discount<span class="req">*</span></label>
									<div>
									<input name="discount" id="discount" type="number" tabindex="2" class="required small tipTop" title="Please Enter the Discount"
									value="<?php if($caption->discount !=""){ echo $caption->discount; } ?>" />
									</div>
								</div>
							</li>
							<li>
								<div class="form_grid_12">
                                    <label class="field_title">URL<span class="req">*</span></label>
									<div>
									<input name="url" id="url" type="text" tabindex="2" class="required small tipTop" title="Enter The Url"
									value="<?php if($caption->url !=""){ echo $caption->url; } ?>" />
									</div>
								</div>
							</li>
							<li>
								<div class="form_grid_12">
                                    <label class="field_title">Visible<span class="req">*</span></label>
									<div>
								<input name="visible" id="visible"  value="yes" type="checkbox" tabindex="2" <?php if($caption->visible =="yes"){ echo 
									'checked'; } else { echo "title=yes"; } ?> >
									</div>
								</div>
							</li>
 
                        <li>
							<div class="form_grid_12">
								<div class="form_input">
										<button type="submit"  id="submit" class="btn_small btn_blue" tabindex="8"><span>Update</span></button>
								</div>
							</div>
						</li>
					</ul>
				</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>