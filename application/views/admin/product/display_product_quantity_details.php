<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<?php 
    $image_url = base_url()."images/product/";
    $alt_image = $image_url.'dummyProductImage.jpg';
    $cities = $cityList->result();
    $filter_type = $_GET['type'];
?>
<div id="content">
	<div class="grid_container">
		<?php if(isset($_SESSION['error'])) { echo '<h2><font color="red">'.$_SESSION['error'].'</h2></font>'; } ?>
		<div class="grid_12">
			<div class="widget_wrap">
				<div class="widget_top">
					<span class="h_icon blocks_images"></span>
					<h6><?php echo $heading?></h6>
					<div style="float: right;line-height:40px;padding:0px 10px;height:39px;">
						<div class="btn_30_light" style="height: 29px;">
							<a href="admin/product/exportProduct?type=<?php echo $filter_type; ?>" class="tipTop" title="Click here to export product quantity"><span class="icon add_co"></span><span class="btn_link">Export</span></a>
						</div>
					</div>
				</div>
				<div class="widget_content">
					    <div class="form_grid_12">
    					    <div class="form_input" style="margin-top: 10px;margin-bottom: 10px;">
								<label>Status</label>
								<select class="large tipTop" onchange="getfilters(this)" id="filter_type">
									<option disabled value="">Type</option>
									<option value="all" <?php if($filter_type == 'all'){echo "selected";} ?>>All</option>
									<option value="Publish" <?php if($filter_type == 'Publish'){echo "selected";} ?> >Publish</option>
									<option value="UnPublish" <?php if($filter_type == 'UnPublish'){echo "selected";} ?> >Unpublish</option>
								</select>

							</div>
							<div id="filter_div_box" style="margin-top: 10px;margin-bottom: 10px;">
    								
							</div>
						</div>
					<table id="example">
						<thead>
							<tr>
								<th class="center">Sr No</th>
								<th>Product Name</th>
								<th>Image</th>
								<th>Bangalore Remain</th>
								<th>Delhi Remain</th>
								<th>Ghaziabad/Noida Remain</th>
								<th>Gurgaon Remain</th>
								<th>Hyderabad Remain</th>								
								<th>Mumbai Remain</th>
								<th>Pune Remain</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<?php if(count($datas) > 0) { 
								foreach ($datas as $key => $value) { ?>
								<tr>
									<td><?php echo $key+1; ?></td>
									<td><?php echo $value->product_name; ?></td>
									<td>
										<?php 
										$img = explode(',', $value->image);
											if($img[0] != '') {
												echo '<div class="widget_thumb" style="margin-left: 25%;"><img width="40px" height="40px"   src='.$image_url.$img[0].'></div>';
											}else{
												echo '<div class="widget_thumb" style="margin-left: 25%;"><img width="40px" height="40px"   src='.$alt_image.'></div>';
											}
										?>
											
									</td>
									<td id="city_drop_down_<?php echo $cities[0]->id.'_'.$value->id; ?>">
									<?php  
									$city = $cities[0]->id;
									if($value->city_quantity[$city] != '0' && $value->city_quantity[$city]  != '') {
								        echo '<label title="Click to update" id="label_'.$city.'_'.$value->id.'" onclick="showInputBox('.$city.','.$value->id.','.$value->city_quantity[$city].')">'.$value->city_quantity[$city].'</label>';
									}else{
									   	echo '<label title="Click to update"  id="zero_label_'.$city.'_'.$value->id.'" onclick="showInputBox('.$city.','.$value->id.',0,0)">0</label>';
									}
									?>
									</td>
									<td id="city_drop_down_<?php echo $cities[1]->id.'_'.$value->id; ?>">
									<?php  
									$city = $cities[1]->id;
									if($value->city_quantity[$city] != '0' && $value->city_quantity[$city]  != '') {
								        echo '<label title="Click to update" id="label_'.$city.'_'.$value->id.'" onclick="showInputBox('.$city.','.$value->id.','.$value->city_quantity[$city].')">'.$value->city_quantity[$city].'</label>';
									}else{
									   	echo '<label title="Click to update"  id="zero_label_'.$city.'_'.$value->id.'" onclick="showInputBox('.$city.','.$value->id.',0,0)">0</label>';
									}
									?>
									</td>
									<td id="city_drop_down_<?php echo $cities[2]->id.'_'.$value->id; ?>">
									<?php  
									$city = $cities[2]->id;
								    if($value->city_quantity[$city] != '0' && $value->city_quantity[$city]  != '') {
								        echo '<label title="Click to update" id="label_'.$city.'_'.$value->id.'" onclick="showInputBox('.$city.','.$value->id.','.$value->city_quantity[$city].')">'.$value->city_quantity[$city].'</label>';
									}else{
									   	echo '<label title="Click to update"  id="zero_label_'.$city.'_'.$value->id.'" onclick="showInputBox('.$city.','.$value->id.',0,0)">0</label>';
									}
									?>
									</td>
									<td id="city_drop_down_<?php echo $cities[3]->id.'_'.$value->id; ?>">
										<?php  
									$city = $cities[3]->id;
								    if($value->city_quantity[$city] != '0' && $value->city_quantity[$city]  != '') {
								        echo '<label title="Click to update" id="label_'.$city.'_'.$value->id.'" onclick="showInputBox('.$city.','.$value->id.','.$value->city_quantity[$city].')">'.$value->city_quantity[$city].'</label>';
									}else{
									   	echo '<label title="Click to update"  id="zero_label_'.$city.'_'.$value->id.'" onclick="showInputBox('.$city.','.$value->id.',0,0)">0</label>';
									}
									?>
									</td>
									<td id="city_drop_down_<?php echo $cities[4]->id.'_'.$value->id; ?>">
										<?php  
									$city = $cities[4]->id;
									if($value->city_quantity[$city] != '0' && $value->city_quantity[$city]  != '') {
								        echo '<label title="Click to update" id="label_'.$city.'_'.$value->id.'" onclick="showInputBox('.$city.','.$value->id.','.$value->city_quantity[$city].')">'.$value->city_quantity[$city].'</label>';
									}else{
									   	echo '<label title="Click to update"  id="zero_label_'.$city.'_'.$value->id.'" onclick="showInputBox('.$city.','.$value->id.',0,0)">0</label>';
									}
									?>	
										
									</td>
									<td id="city_drop_down_<?php echo $cities[5]->id.'_'.$value->id; ?>">
											<?php  
									$city = $cities[5]->id;
									if($value->city_quantity[$city] != '0' && $value->city_quantity[$city]  != '') {
								        echo '<label title="Click to update" id="label_'.$city.'_'.$value->id.'" onclick="showInputBox('.$city.','.$value->id.','.$value->city_quantity[$city].')">'.$value->city_quantity[$city].'</label>';
									}else{
									   	echo '<label title="Click to update"  id="zero_label_'.$city.'_'.$value->id.'" onclick="showInputBox('.$city.','.$value->id.',0,0)">0</label>';
									}
									?>
									</td>
									<td id="city_drop_down_<?php echo $cities[6]->id.'_'.$value->id; ?>">
									<?php  
									$city = $cities[6]->id;
									if($value->city_quantity[$city] != '0' && $value->city_quantity[$city]  != '') {
								        echo '<label title="Click to update" id="label_'.$city.'_'.$value->id.'" onclick="showInputBox('.$city.','.$value->id.','.$value->city_quantity[$city].')">'.$value->city_quantity[$city].'</label>';
									}else{
									   	echo '<label title="Click to update"  id="zero_label_'.$city.'_'.$value->id.'" onclick="showInputBox('.$city.','.$value->id.',0,0)">0</label>';
									}
									?>
									</td>
									<td>
										<?php echo $value->status; ?>
									</td>
								</tr>



							<?php } } ?>
						</tbody>
						<tfoot>
							<tr>
								<th class="center">Sr No</th>
								<th>Product Name</th>
								<th>Image</th>
								<th>Bangalore Remain</th>
								<th>Delhi Remain</th>
								<th>Ghaziabad/Noida Remain</th>
								<th>Gurgaon Remain</th>
								<th>Hyderabad Remain</th>
								<th>Mumbai Remain</th>
								<th>Pune Remain</th>
								<th>Status</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<input type="hidden" name="statusMode" id="statusMode"/>
		<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>

	</div>
	<span class="clear"></span>
</div>

<style>
#selling_product_tbl tr td{
	border-right:#ccc 1px solid;
}
</style>

<script>
var last_selected = '';
	function showInputBox(city_id,product_id,quantity = 0,val = null ){
		if(val == null){
		    last_selected = 1;
			$('#city_'+city_id+'_'+product_id).show();
			$('#label_'+city_id+'_'+product_id).hide();

			$('#zero_city_'+city_id+'_'+product_id).hide();
			$('#zero_label_'+city_id+'_'+product_id).hide();
			var showInputBox = '<div id="city_'+city_id+'_'+product_id+'"><input style="width: 50px;" type="number" name="qty" id="qty" min="0" value='+quantity+'><a class="udpate_qty" href="javascript:void(0);" style="margin:5px;display:block;"  id="'+city_id+'" data-proid="'+product_id+'">Update</a></div>';
		    $('#city_drop_down_'+city_id+'_'+product_id).html(showInputBox);
		    
		}else{
		    var showInputBox = '<div id="zero_city_'+city_id+'_'+product_id+'"><input style="width: 50px;" type="number" name="qty" id="qty" min="0" value=0><a class="udpate_qty" href="javascript:void(0);" style="margin:5px;display:block;"  id="'+city_id+'" data-proid="'+product_id+'">Update</a></div>';
		    $('#city_drop_down_'+city_id+'_'+product_id).html(showInputBox);
		    last_selected = 0;
			$('#zero_city_'+city_id+'_'+product_id).show();
			$('#zero_label_'+city_id+'_'+product_id).hide();

			$('#city_'+city_id+'_'+product_id).hide();
			$('#label_'+city_id+'_'+product_id).hide();
		}
	}
	$(document).ready(function() {
    $('#example').DataTable();
} );
// var sellerListing = $('#city_quantity_table123').dataTable({   
// 	"aoColumnDefs": [
//      { "bSortable": false, "aTargets": [ 0] }
//      ],
//      "aaSorting": [[0, 'asc']],
//      "sPaginationType": "full_numbers",
//      "iDisplayLength": 50,
//      "oLanguage": {
//     	 "sLengthMenu": "<span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'>Entries per page:</span>",	
//      },
//      "sDom": '<"table_top"fl<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>'   	 
// });

$(document).on('click', '.udpate_qty', function(){
		var proId = $(this).data('proid');
		var cityid = $(this).attr('id');
		var qty = $(this).prev('#qty').val();
		
		$.post("<?php echo base_url(); ?>admin/product/update_city_qty", {"qty": qty, "cityid": cityid, "proId": proId}, function(result){
            alert('Quantity Is Updated');
            console.log("last_selected",last_selected);
            // if(qty > 0){
                if(last_selected == 0 || qty == 0){
        //          	$('#zero_city_'+cityid+'_'+proId).hide();
				    // $('#zero_label_'+cityid+'_'+proId).html(qty).show();
				    var label = '<label title="Click to update" id="zero_label_'+cityid+'_'+proId+'" onclick="showInputBox('+cityid+','+proId+','+qty+')">'+qty+'</label>';
                }else if(last_selected == 0 || qty != 0){
                //     $('#city_'+cityid+'_'+proId).hide();
                //     $('#zero_city_'+cityid+'_'+proId).hide();
             	  //  $('#label_'+cityid+'_'+proId).html(qty).show();
				    var label = '<label title="Click to update" id="label_'+cityid+'_'+proId+'" onclick="showInputBox('+cityid+','+proId+','+qty+')">'+qty+'</label>';
                }else if(last_selected != 0 || qty != 0){
                    // $('#city_'+cityid+'_'+proId).hide();
                    // $('#zero_city_'+cityid+'_'+proId).hide();
				    // $('#label_'+cityid+'_'+proId).html(qty).show(); 
				    var label = '<label title="Click to update" id="label_'+cityid+'_'+proId+'" onclick="showInputBox('+cityid+','+proId+','+qty+')">'+qty+'</label>';
                }
                $('#city_drop_down_'+cityid+'_'+proId).html(label);
		})
	})



function getfilters(value){
	var filter_type = '<?php echo $filter_type; ?>';
	if(value.value != 'null' &&   value.value != ''){
		if(filter_type != value.value){
			location.href = baseURL + "admin/product/display_product_quantity_details?type="+value.value;
		}
	}
	// $.ajax({
 //        url: baseURL + "admin/product/get_filters_on_type",
 //        data: {
 //            'filter_type':value.value
 //        },                         
 //        type: 'post',
 //        dataType:'json',
 //        success: function(result){
 //            // sellerListing.draw();
 //            if(result.status == 200)
 //            {
 //                sellerListing.clear().draw();
 //                sellerListing.rows.add(result.data);
 //                sellerListing.columns.adjust().draw();
 //             console.log(result.data);
 //            }
 //            // console.log('resut=='+result.data);
 //            // alert('Success');
 //        },
 //        error:function(error){
 //            console.log(error);
 //        }
 //    });
}
</script>


<?php
$this->load->view('admin/templates/footer.php');
?>