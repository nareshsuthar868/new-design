<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">
				<div class="widget_top">
					<span class="h_icon blocks_images"></span>
					<h6><?php echo $heading?></h6>
				</div>
				<div class="widget_content">
					<table class="display" id="text_layout_tbl">
						<thead>
							<tr>
								<th class="tip_top" title="Click to sort">#</th>
                            	<th class="tip_top" title="Click to sort">Order ID</th>
								<th class="tip_top" title="Click to sort">User Email</th>
								<th class="tip_top" title="Click to sort">Mandate ID</th>
                            	<th class="tip_top" title="Click to sort">Card Token</th>
								<th class="tip_top" title="Click to sort">User Credentials</th>
								<th>Is Customer Payment</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							$sno = 1;
							foreach ($recurring_payment as $row){
							?>
							<tr>
								<td class="center tr_select ">
									<?php echo $sno;$sno++;?>
								</td>
								<td class="center">
									<?php echo $row->order_id;?>
								</td>
	                            <td class="center">
									<?php echo $row->email;?>
								</td>
								<td class="center">
									<?php echo $row->mandate_id;?>
								</td> 
								<td class="center">
									<?php echo $row->card_token;?>
								</td> 
								<td class="center">
									<?php echo $row->user_credentials;?>
								</td> 
								<td class="center">
									<?php if(isset($row->ccp_id)){
										echo 'Yes';
									}else echo "No"; ?>
								</td>
							</tr>
							<?php } ?>
						</tbody>
						<tfoot>
							<tr>
								<th class="center">#</th>
                            	<th>Order ID</th>
								<th>User Email</th>
								<th>Mandate ID</th>
								<th>Card Token</th>
								<th>Is Customer Payment</th>	
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
<style>
#text_layout_tbl tr td{
	border-right:#ccc 1px solid;
}
</style>	
<script>
$('#text_layout_tbl').dataTable({   
	"aoColumnDefs": [
     { "bSortable": false, "aTargets": [ 3 ] }
     ],
     "aaSorting": [[0, 'asc']],
     "sPaginationType": "full_numbers",
     "iDisplayLength": 100,
     "oLanguage": {
    	 "sLengthMenu": "<span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'>Entries per page:</span>",	
     },
     "sDom": '<"table_top"fl<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>'
	                	 
});
</script>	
<?php 
$this->load->view('admin/templates/footer.php');
?>