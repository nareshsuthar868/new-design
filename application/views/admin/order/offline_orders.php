<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<script type="text/javascript" src="<?php echo base_url()?>assets/js/check_login.js"></script>
<div id="admin_loader" style="display: none;"><div class="loader-inner"><img src="<?php echo base_url()?>images/admin_loader.gif"></div></div>

<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						
					</div>
					<div class="widget_content">
						<table class="display sort-table" id="OfficeOrderListing">
                            <thead>
                                <tr>
	                                <th>Order Id</th>
                                    <!-- <th>Full NAme</th> -->
                                    <th>User Email</th>
                                    <th>Phone no</th>
                                    <th>Order Date</th>
                                    <th>Transaction ID</th>
                                    <th>Advance Payment</th>
                                    <th>Total</th>
                                    <th>City</th>
                                    <th>Coupon Code</th>
                                    <th>Order Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                        	</tbody>
                          	<tfoot>
                            	<tr>
	                            	<th>Order Id</th>
                                    <!-- <th>Full NAme</th> -->
                                    <th>User Email</th>
                                    <th>Phone no</th>
                                    <th>Order Date</th>
                                    <th>Transaction ID</th>
                                    <th>Advance Payment</th>
                                    <th>Total</th>
                                    <th>City</th>
                                    <th>Coupon Code</th>
                                    <th>Order Status</th>
                                    <th>Action</th>
	                            </tr>
	                        </tfoot>
                        </table>
					</div>
				</div>
			</div>
			
		</div>
		<span class="clear"></span>
	</div>
</div>


<button type="button" class="trigger" style="display: none;">View</button>
    <div class="modal refralmodal" id="model_css11">
        <div class="modal-content"  >
            <span class="close-button">&times;</span>
            <h1>Update And Approve Order</h1><br>
            <form  class="form_container left_label">
                <ul class="payout_form_ul">

                    <li>
                        <div class="form_grid_12">
                            <label class="field_title" >Total Amount</label>
                            <div class="form_input">
                                <label id="total_amount"></label>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="form_grid_12">
                            <label class="field_title">Advance Payment</label>
                            <div class="form_input">
                                <label id="advance_amount"></label>
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="form_grid_12">
                            <label class="field_title">Total Pending Amount</label>
                            <div class="form_input">
                                <label id="total_due_amount"></label>
                            </div>
                        </div>
                    </li>
                    <li>
                        <div class="form_grid_12">
                            <label class="field_title" for="transaction_id">Transaction ID<span class="req">*</span></label>
                            <div class="form_input">
                                <input name="transaction_id" id="transaction_id" type="text" tabindex="1" class="required large tipTop">
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="form_grid_12">
                            <label class="field_title" for="payment_mode">Payment Mode<span class="req">*</span></label>
                            <div class="form_input">
                                <select name="payment_mode" id="payment_mode" class="required dropdown">
                                    <option value="Online">Online</option>
                                    <option value="NEFT">NEFT</option>
                                    <option value="Paytm">Paytm</option>
                                    <option value="Cheque">Cheque</option>
                                </select>
                               <!--  <input name="payment_mode" id="payment_mode" type="text" tabindex="1" class="required large tipTop"> -->
                            </div>
                        </div>
                    </li>

                    <li>
                        <div class="form_grid_12">
                            <label class="field_title" for="order_date">Transaction Date<span class="req">*</span></label>
                            <div class="form_input">
                            <?php
                                $date = new DateTime(); // Date object using current date and time
                                $dt= $date->format('Y-m-d'); 
                                echo "<input type='date' id='order_date' name='order_date' value='$dt'  >";
                            ?>
                            </div>
                        </div>
                    </li>
                    <input type="hidden" id="dealCodeNumber">
                    <input type="hidden" id="user_id">
                          
                    <li id="new_payout">
                        <div class="form_grid_12 cashgram_button"  style="text-align: right;">
                            <input type="button" onclick="ApprovePayment()" id="cashgram_button" class="btn_small btn_blue" value="Update" />
                            <input type="hidden" id="resend" name="resend">
                        </div>
                    </li>
                </ul>
            </form>
        </div>
    </div>

<style type="text/css">
    .modal {position: fixed;left: 0;top: 0;width: 100%;height: 100%;background-color: rgba(0, 0, 0, 0.5);opacity: 0;visibility: hidden;transform: scale(1.1);transition: visibility 0s linear 0.25s, opacity 0.25s 0s, transform 0.25s;}
    .modal-content {position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);background-color: white;padding: 1rem 1.5rem;width: 42rem;border-radius: 0.5rem;}
    .close-button {float: right;width: 1.5rem;line-height: 1.5rem;text-align: center;cursor: pointer;border-radius: 0.25rem;background-color: lightgray;}
    .close-button:hover {background-color: darkgray;}
    .show-modal {opacity: 1;visibility: visible;transform: scale(1.0);transition: visibility 0s linear 0s, opacity 0.25s 0s, transform 0.25s;}
    table {font-family: arial, sans-serif;border-collapse: collapse;width: 100%;}
    td, th {border: 1px solid #dddddd;text-align: center;padding: 8px;}
    tr:nth-child(even) {background-color: #dddddd;}
    @media only screen and (max-width: 1366px) {.refralmodal{overflow: auto;}.refralmodal .modal-content{transform: translate(-50%, 0);top: 20px;} .modal-open{overflow: hidden;}}
</style>

<script type="text/javascript">
$(function (){

   var bulkOrder =  $('#OfficeOrderListing').DataTable({
        processing:  true,
        serverSide: true,
        ajax: '<?php echo base_url(); ?>admin/order/get_offline_order_details',
        "deferRender": true,
        "pagingType": "full_numbers",
        responsive: true,
        order: [3,'desc'],
        columns: [
            { "data": "id" },
            // { "data": "full_name"},
            { "data": "email"},
            { "data": "phone_no"},
            { "data": "created" },
            { "data": "dealCodeNumber" },
            { "data": "advance_rental" },
            { "data": "total" },
            { "data": "shippingcity"},
            { "data": "couponCode"},
            null,
            null
        ],
           columnDefs: [

            {"targets": [0], "className": "center tr_select"},
            {
                orderable: true, targets: [9],
                "render": function ( data, type, full, meta ) {
                    var link = '<label style="color:green">'+full.status+'</label>';
                    if(full.status == 'Pending'){
                        link  = '<label style="color:red">'+full.status+'</label>';
                    }
                    return link;
                }
            },
            {
                 orderable: false, targets: [10],
                "render": function ( data, type, full, meta ) {
                    var user_id = full.user_id;
                    var base_url = "<?php echo  base_url(); ?>";
                    var link = ''; 
                    var dealCodeNumber = full.dealCodeNumber;
                    if(full.status == 'Pending'){
                        link += '<a class="tipTop" title="Create Zoho Invoice" onclick="show_popup_model('+ full.user_id +','+ full.dealCodeNumber +','+ full.total +','+ full.advance_rental +')"><span class="action-icons c-add" style="cursor:pointer;"></span></a>';
                    }
                  
                    link  += '<a class="tipTop" title="Send Voucher Email" onclick="Send_Voucher('+ full.user_id +','+ full.dealCodeNumber +')"><span class="action-icons c-approve" style="cursor:pointer;"></span></a>\
                    <a href="'+ base_url +'admin/order/view_order/'+ user_id +'/'+dealCodeNumber +'" target="popup" onclick="window.open(\''+base_url+'admin/order/view_order/'+ user_id +'/'+dealCodeNumber +'\',\'popup\',\'width=1100,height=700\')"><span class="action-icons c-suspend tipTop" title="View Invoice" style="cursor:pointer;"></span></a>';


                    return link;
                }
            },
            
   
        {
            orderable: false, targets: [0,5,6,7,9],
        }],
        language: {
            searchPlaceholder: "Search By All"
        },
        fnDrawCallback: function (oSettings) {
        }
    });

      bulkOrder.on( 'order.dt search.dt', function () {
        bulkOrder.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

});

function show_popup_model(user_id,dealCodeNumber,total_amount,advance_rental){
        $('#model_css11').show();
        $('#model_css11').addClass('show-modal');
        $('#dealCodeNumber').val(dealCodeNumber);
        $('#user_id').val(user_id);
        $('#total_amount').html(total_amount.toFixed(2));
        $('#advance_amount').html(advance_rental.toFixed(2));
        var dueAmount = total_amount - advance_rental;
        $('#total_due_amount').html(dueAmount.toFixed(2));
}


</script>
 <script type="text/javascript">
         var modal = document.querySelector(".modal");
        var trigger = document.querySelector(".trigger");
        var closeButton = document.querySelector(".close-button");

        function toggleModal() {
            modal.classList.toggle("show-modal"); 
            document.getElementsByTagName('body')[0].style = 'overflow: auto';
        }

        function windowOnClick(event) {
            if (event.target === modal) {
                toggleModal();
            }
        }

        trigger.addEventListener("click", toggleModal);
        closeButton.addEventListener("click", toggleModal);
        window.addEventListener("click", windowOnClick);
    </script>

<?php 
$this->load->view('admin/templates/footer.php');
?>