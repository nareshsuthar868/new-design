<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						
					</div>
					<div class="widget_content">
						<table class="display sort-table" id="BulkOrderListing">
                            <thead>
                                <tr>
	                            	<th class="center">#</th>
									<th>User Name</th>
	                                <th>User Email</th>
	                                <th>User Phone</th>
	                                <th>City</th>
	                                <th>Message</th>
	                                <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                        	</tbody>
                          	<tfoot>
                            	<tr>
	                            	<th class="center">#</th>
									<th>User Name</th>
	                                <th>User Email</th>
	                                <th>User Phone</th>
	                                <th>City</th>
	                                <th>Message</th>
	                                <th>Action</th>
	                            </tr>
	                        </tfoot>
                        </table>
					</div>
				</div>
			</div>
			
		</div>
		<span class="clear"></span>
	</div>
</div>

<script type="text/javascript">
$(function (){

   var bulkOrder =  $('#BulkOrderListing').DataTable({
        processing:  true,
        serverSide: true,
        ajax: '<?php echo base_url(); ?>admin/order/get_bulk_order_listing',
        "deferRender": true,
        "pagingType": "full_numbers",
        responsive: true,
        order: [],
        columns: [
        	{ "data": "id" },
        	{ "data": "name" },
            { "data": "email" },
            { "data": "phone" },
            { "data": "city"},
            { "data": "message" },
            { "data": "Action"},
        ],
        columnDefs: [
            {"targets": [0], "className": "center tr_select"},
            {
                orderable: false, targets: [6],
                "render": function ( data, type, full, meta ) {
                	var buttons = '<span class="action-icons c-delete"  original-title="Delete" onclick="Delete_BulkOrder('+full.id+')"></span>';
                
                    return buttons;
                }
            },
	        {
	            orderable: false, targets: [0,6],
	        }
        ],
        language: {
            searchPlaceholder: "Search By Email"
        },
        fnDrawCallback: function (oSettings) {
        }
    });

      bulkOrder.on( 'order.dt search.dt', function () {
        bulkOrder.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
            cell.innerHTML = i+1;
        } );
    } ).draw();

});


</script>
<?php 
$this->load->view('admin/templates/footer.php');
?>