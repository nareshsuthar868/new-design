<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						
					</div>
					<div class="widget_content">
						<table class="display display_tbl" id="bulkOrder_tbl">
						<thead>
						<tr>
							
                            <th class="tip_top" title="Click to sort">
								 #
							</th>
							<th class="tip_top" title="Click to sort">
								Error Reason
							</th>
							<th class="tip_top" title="Click to sort">
								Transactrion ID
							</th>
                            <th class="tip_top" title="Click to sort">
								Invoice Type
							</th>
							<th class="tip_top" title="Click to sort">
								Solved Status
							</th>
							 <th class="tip_top" title="Click to sort">
								Created Datetime	
							</th>
							<th>
								 Action
							</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						if ($logs->num_rows() > 0){
							$i = 0;
							foreach ($logs->result() as $row){
								$i++;
						?>
						<tr>
							
                            <td class="center">
								<?php echo $i;?>
							</td>
							<td class="center">
								<?php echo $row->log_message;?>
							</td>
							<td class="center">
								<?php echo $row->order_id;?>
							</td>
   							<td class="center">
								<?php echo $row->invoice_name;?>
							</td>
							<td class="center">
								<?php 
								if($row->solved == 0){
								 echo '<button onclick=SolvedLogs('.$row->id.',"1")>Pending</button>';
								}else{
									 echo "Solved";
								}
								?>
								
							</td>

							<td class="center">
								<?php echo  date("d-m-Y h:i:s a" , strtotime($row->created_date));?>
							</td>
							<!-- onclick="View_Zoho_Logs('<?php echo $row->id;?>')" -->
							<td class="center">
								<a href="<?php echo base_url(); ?>admin/order/get_all_logs?id=<?php echo $row->id;  ?>">
									<span class="action-icons c-suspend"  original-title="View" ></span>
								</a>
								<span class="action-icons c-delete"  original-title="Delete" onclick="Delete_logs('<?php echo $row->id;?>')"></span>
							</td>
							<!-- <td class="center"></td> -->
						</tr>
                        
						<?php 
							}
						}
						?>
						</tbody>
						<tfoot>
						<tr>
							
                            <th>
                               #
                            </th>
							<th>
								Error Reason
                            </th>
							<th class="tip_top" title="Click to sort">
								Transactrion ID
							</th>
							<th>
								Invoice Type
							</th>
							<th>
								Solved Status
							</th>
							<th>
								Created Datetime	
							</th>
							<th>
								Action
							</th>
						</tr>
						</tfoot>
						</table>
					</div>
				</div>
			</div>
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<script type="text/javascript">
	


</script>
<?php 
$this->load->view('admin/templates/footer.php');
?>