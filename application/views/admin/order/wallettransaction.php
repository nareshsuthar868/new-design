<?php

$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						
					</div>
					<div class="widget_content">
						<table class="display display_tbl" id="bulkOrder_tbl">
						<thead>
						<tr>
							
                            <th class="tip_top" title="Click to sort">
								 #
							</th>
							<th class="tip_top" title="Click to sort">
								Order Id
							</th>
							<th class="tip_top" title="Click to sort">
								Txn Id
							</th>
							<th class="tip_top" title="Click to sort">
								 Payment Mode
							</th>
                        	<th class="tip_top" title="Click to sort">
								 MODE
							</th>
							 <th class="tip_top" title="Click to sort">
								 Amount	
							</th>
							<!-- <th class="tip_top" title="Click to sort">
								 Expiry Amount	
							</th> -->
							<th class="tip_top" title="Click to sort">
								 Wallet Recharge Date	
							</th>
							<!-- <th>
								 Action
							</th> -->
						</tr>
						</thead>
						<tbody>
						<?php 
						if ($leadDetails > 0){
							$i = 0;
							foreach ($leadDetails as $row){
								$i++;
						?>
						<tr>
							
                            <td class="center">
								<?php echo $i;?>
							</td>
							<td class="center">
								<?php echo $row->order_id;?>
							</td>
							<td class="center">
								<?php echo $row->txnid;?>
							</td>
							<td class="center">
								<?php if($row->payment_mode == '0' ){ echo "Debited"; } else { echo "CC"; }?>
							</td>
   							<td class="center">
								<?php if($row->mode == 'credit' ){ echo "<span style='color:green;'>".strtoupper($row->mode)."</span>"; } else { echo "<span style='color:red;'>".strtoupper($row->mode)."</span>"; }?>
							</td>
							<td class="center">
								<?php echo $row->amount;?>
							</td>
							<!-- <td>
								<?php echo 0;?>
							</td> -->
							<td class="center">
								<?php echo $row->created_at;?>
							</td>
							<!-- <td class="center">
							<span><a class="action-icons c-suspend" href="admin/uslead/transaction/<?php echo $row->user_id;   ?>" title="View">View</a></span>
							</td> -->
							<!-- <td class="center"></td> -->
						</tr>
                        
						<?php 
							}
						}
						?>
						</tbody>
						</table>
					</div>
				</div>
			</div>
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');


?>
