<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						
					</div>
					<div class="widget_content">
						<table class="display display_tbl" id="bulkOrder_tbl">
						<thead>
						<tr>
							
                            <th class="tip_top" title="Click to sort">
								 #
							</th>
							<th class="tip_top" title="Click to sort">
								User Name
							</th>
							<th class="tip_top" title="Click to sort">
								 User Email
							</th>
                            <th class="tip_top" title="Click to sort">
								 User Phone	
							</th>
							 <th class="tip_top" title="Click to sort">
								 City	
							</th>
							<th class="tip_top" title="Click to sort">
								 Message
							</th>
							<th>
								 Action
							</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						if ($leadDetails->num_rows() > 0){
							$i = 0;
							foreach ($leadDetails->result() as $row){
								$i++;
						?>
						<tr>
							
                            <td class="center">
								<?php echo $i;?>
							</td>
							<td class="center">
								<?php echo $row->name;?>
							</td>
							<td class="center">
								<?php echo $row->email;?>
							</td>
   							<td class="center">
								<?php echo $row->phone;?>
							</td>

							<td class="center">
								<?php echo $row->city;?>
							</td>
							<td class="center" style=" word-break: break-all;">
								 <?php echo $row->message;?>
							</td>
							<td class="center">
							<span class="action-icons c-delete"  original-title="Delete" onclick="Delete_Leadrequest('<?php echo $row->id;?>')"></span>
							</td>
							<!-- <td class="center"></td> -->
						</tr>
                        
						<?php 
							}
						}
						?>
						</tbody>
						<tfoot>
						<tr>
							
                            <th>
                               #
                            </th>
							<th>
								 User Name
                            </th>
							<th class="tip_top" title="Click to sort">
								 User Email
							</th>
							<th>
								 User Phone
							</th>
							<th>
								City
							</th>
                            <th>
                            	Message
                            </th>
							<th>
								Action
							</th>
						</tr>
						</tfoot>
						</table>
					</div>
				</div>
			</div>
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>