<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6>Add CF Coins</h6>
                        
					</div>
					<div class="widget_content">
					<?php 
						$attributes = array('class' => 'form_container left_label', 'id' => 'addlistvalue_form', 'enctype' => 'multipart/form-data');
						echo form_open_multipart('admin/Userwallet/addeditwallettransaction/'.$id,$attributes) 
					?>
							<ul>
		 						<li>
									<div class="form_grid_12" style="margin-bottom:15px;">
									<label class="field_title" for="coin_balance">Coins<span class="req">*</span></label>
										<div class="form_input">
											<input name="coin_balance" id="coin_balance" type="text" tabindex="1" class="required large tipTop" title="Please enter the Coin Balance"/>
										</div>
									</div>
									<div class="form_grid_12" style="margin-bottom:15px;">
									<label class="field_title" for="coin_balance">Payment Mode<span class="req">*</span></label>
										<div class="form_input">
											<input name="payment_mode" id="payment_mode" type="text" tabindex="1" class="required large tipTop" title="Please enter payment mode"/>
										</div>
									</div>

		                            <div class="form_grid_12">
										<label class="field_title" for="remarks">Remarks<span class="req">*</span></label>
										<div class="form_input">
											<!-- <input name="remarks" id="remarks" type="text" tabindex="1" class="required large tipTop" title="Please enter the Remarks value"/> -->
											<textarea  name="remarks" id="remarks" tabindex="1" class="required large tipTop" title="Please enter the Remarks value" style="width:50%;"></textarea>
										</div>
									</div>
								</li>                        	
								<li>
									<div class="form_grid_12">
										<div class="form_input">
											<button type="submit" class="btn_small btn_blue" tabindex="2"><span>Submit</span></button>
										</div>
									</div>
								</li>
							</ul>
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<script>
$('#addlistvalue_form').validate();

</script>
<?php 
$this->load->view('admin/templates/footer.php');
?>