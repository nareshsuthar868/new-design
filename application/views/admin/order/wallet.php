<?php

$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						
					</div>
					<div class="widget_content">
						<table class="display display_tbl" id="bulkOrder_tbl">
						<thead>
						<tr>	
                            <th class="tip_top" title="Click to sort">
								 #
							</th>
							<th class="tip_top" title="Click to sort">
								User Name
							</th>
							<th class="tip_top" title="Click to sort">
								 User Email
							</th>
							<th class="tip_top" title="Click to sort">
								 Coin Balance	
							</th>
							<th class="tip_top" title="Click to sort">
								 Last Wallet Updated Date	
							</th>
							<th class="tip_top" title="Click to sort">
								 Wallet Created Date
							</th>
							<th>
								 Action
							</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						if ($leadDetails > 0){
							$i = 0;
							foreach ($leadDetails as $row){
								$i++;
						?>
						<tr>
                            <td class="center">
								<?php echo $i;?>
							</td>
							<td class="center">
								<?php echo $row->full_name;?>
							</td>
							<td class="center">
								<?php echo $row->email;?>
							</td>
   							<td class="center">
								<?php echo $row->topup_amount;?>
							</td>
							<td class="center">
								<?php echo $row->updated_at;?>
							</td>
								<td class="center">
								<?php echo $row->created_at;?>
							</td>

							<td class="center">
							<span><a class="action-icons c-suspend" href="admin/userwallet/transaction/<?php echo $row->user_id;   ?>" title="View">View</a></span>
							<span><a class="action-icons c-add" href="admin/userwallet/addwallettransaction/<?php echo $row->user_id;   ?>" title="View">View</a></span>
							</td>
						</tr>
                        
						<?php 
							}
						}
						?>
						</tbody>
						</table>
					</div>
				</div>
			</div>
			
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');

?>
