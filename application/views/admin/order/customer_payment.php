 <?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>

<script type="text/javascript">
    function filterGlobal () {
        $('#get_customer_data').DataTable().search(
            $('#global_filter').val(),
        ).draw();
    }
$(function (){
    $('input.global_filter').on( 'keyup click', function () {
        filterGlobal();
    });
    $( ".selector" ).datepicker({ dateFormat: 'yy-mm-dd' });
 	var ref=  $('#get_customer_data').DataTable({
        processing:  true,
        serverSide: true,
        ajax: '<?php echo base_url(); ?>admin/order/show_customer_payment',
        "deferRender": true,
        "pagingType": "full_numbers",
        responsive: true,
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('offersDataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('offersDataTables'));
        },
        order: [],
        columns: [
        	{ "data": "ccp_id"},
            { "data": "ccp_email" },
            { "data": "ccp_invoice_number" },
            { "data": "mandate_id"},
        ],
        columnDefs: [ 
        ],
        language: {
            searchPlaceholder: "Search"
        },
        fnDrawCallback: function (oSettings) {
        }
    });

   ////  create index for table at columns zero
  //   ref.on('order.dt search.dt', function () {
  //    	ref.column(0, { search: 'applied', order: 'applied' }).nodes().each(function (cell, i) {
  //            cell.innerHTML = i + 1;
  //    	});
 	// }).draw();

});
</script>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
					</div>
					<div class="widget_content">
                        <table class="display sort-table" id="get_customer_data">
                            <thead>
                                <tr>
                                   	<th>ID</th>
	                                <th>User Email</th>
                                    <th>Invoice Number</th>
	                                <th>Mandate ID</th>
                            </thead>
                            <tbody>
                        	</tbody>
                          	<tfoot>
                            	<tr>
	                              	<th>ID</th>
	                                <th>User Email</th>
                                    <th>Invoice Number</th>
	                                <th>Mandate ID</th>
	                            </tr>
	                        </tfoot>
                        </table>


					</div>
				</div>
			</div>		
		</div>
		<span class="clear"></span>
	</div>
</div>

    <?php 
$this->load->view('admin/templates/footer.php');
?>
