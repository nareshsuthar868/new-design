<?php
$this->load->view('admin/templates/header.php');
// extract($privileges);
?>
<div id="content">
		<div class="grid_container">
			<?php 
				$attributes = array('id' => 'display_form');
				echo form_open('admin/completed_project/change_banner_status',$attributes) 
			?>
<?php if(isset($_SESSION['error'])) { echo '<h2><font color="red">'.$_SESSION['error'].'</h2></font>'; } ?>
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon blocks_images"></span>
						<h6><?php echo $heading?></h6>
						<div style="float: right;line-height:40px;padding:0px 10px;height:39px;">
						<!-- Need to add the code -->
						</div>
					</div>
					<div class="widget_content">
						<table class="display display_tbl" id="selling_product_tbl">
						<thead>
						<tr>
						    <th class="tip_top" title="Click to sort">
								 Image
							</th>
                            <th class="tip_top" title="Click to sort">
								 Company
							</th>
							<th class="tip_top" title="Click to sort">
								 Location
							</th>
							<th class="tip_top" title="Click to sort">
								 Position
							</th>
							<th class="tip_top" title="Click to sort">
								Status
							</th>
							<th class="tip_top" title="Click to sort">
								Created On
							</th>
							<th>
								 Action
							</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						if ($projectList->num_rows() > 0){
							foreach ($projectList->result() as $row){
						?>
						<tr>
							<td class="center">
								<div class="widget_thumb" style="margin-left: 25%;">
								 <img width="40px" height="40px" src="<?php echo base_url();?>images/completed_project/<?php echo $row->image;?>" />
								</div>
							</td>
                            <td class="center">
								<?php echo $row->company_name;?>
							</td>
							<td class="center">
								<?php echo $row->location;?>
							</td>
							<td class="center">
							    <input type="number" name="position_<?php echo $row->id ?>" value="<?php echo $row->position ?>" style="width:40px;">
							    <a href="javascript:void(0)" onclick="updateProjectPostition(<?php echo $row->id ?>, <?php echo $row->position ?>)">Update</a>
							</td>
							<td class="center">
							    <?php
							        if($row->status == 'Publish'){
							    ?>
							            <a title="Click to unpublish" class="tip_top" href="javascript:confirm_status('admin/completed_project/change_completed_project_status/<?php echo $row->status; ?>/<?php echo $row->id;?>');"><span class="badge_style b_done"><?php echo $row->status;?></span></a>
							    <?php
							        }else{
							    ?>
							            <a title="Click to publish" class="tip_top" href="javascript:confirm_status('admin/completed_project/change_completed_project_status/<?php echo $row->status; ?>/<?php echo $row->id;?>')"><span class="badge_style"><?php echo $row->status;?></span></a>
							    <?php
							        }
							    ?>
							</td>
							<td class="center">
								<?php echo $row->dateAdded;?>
							</td>
							<td class="center">
							    <span><a class="action-icons c-edit" href="admin/completed_project/edit_completed_project/<?php echo $row->id;?>" title="Edit">Edit</a></span>
							</td>
						</tr>
						<?php 
							}
						}
						?>
						</tbody>
						<tfoot>
						<tr>
							<th class="tip_top" title="Click to sort">
								 Image
							</th>
                            <th class="tip_top" title="Click to sort">
								 Company
							</th>
							<th class="tip_top" title="Click to sort">
								 Location
							</th>
							<th class="tip_top" title="Click to sort">
								 Position
							</th>
							<th class="tip_top" title="Click to sort">
								Status
							</th>
							<th class="tip_top" title="Click to sort">
								Created On
							</th>
							<th>
								 Action
							</th>
						</tr>
						</tfoot>
						</table>
					</div>
				</div>
			</div>
			<input type="hidden" name="statusMode" id="statusMode"/>
			<input type="hidden" name="SubAdminEmail" id="SubAdminEmail"/>
		</form>	
			
		</div>
		<span class="clear"></span>
	</div>
<style>
#selling_product_tbl tr td{
	border-right:#ccc 1px solid;
}
</style>	
<script>
$('#selling_product_tbl').dataTable({   
	"aoColumnDefs": [
	                 { "bSortable": false, "aTargets": [ 0,3,10 ] }
	                 ],
	                 "aaSorting": [[11, 'asc']],
	                 "sPaginationType": "full_numbers",
	                 "iDisplayLength": 100,
	                 "oLanguage": {
	                	 "sLengthMenu": "<span class='lenghtMenu'> _MENU_</span><span class='lengthLabel'>Entries per page:</span>",	
	                 },
	                 "sDom": '<"table_top"fl<"clear">>,<"table_content"t>,<"table_bottom"p<"clear">>'
	                	 
});

function updateProjectPostition(id, old_position){
    let new_position = $("input[name='position_"+ id +"']").val();
    // console.log({
    //     'id':id,
    //     'old_position':old_position,
    //     'new_position':new_position
    // });
    $.ajax({
        url:"<?php echo base_url('admin/completed_project/update_project_position') ?>",
        method:"post",
        data:{
            "id":id,
            "old_position":old_position,
            "new_position":new_position
        },
        dataType:'json',
        success:function(data){
            location.reload();
            // console.log(data);
        },
        error:function(error){
            console.log(error);
        }
    });
}
</script>	
<?php 
$this->load->view('admin/templates/footer.php');
?>