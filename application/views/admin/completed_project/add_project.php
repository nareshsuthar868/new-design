<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6><?php echo $heading ?></h6>
                        
					</div>
					<div class="widget_content">
					<?php 
						$productattributes = array('class' => 'form_container left_label', 'id' => 'completed_project_form', 'enctype' => 'multipart/form-data');
						if(!empty($projectDetails->id)){
						    echo form_open_multipart('admin/completed_project/edit_completed_project/'.$projectDetails->id,$productattributes);
						}else{
						    echo form_open_multipart('admin/completed_project/add_completed_project',$productattributes);
						}
						
					?>
                    
						<ul>
						    <li>
    							<div class="form_grid_12">
        							<label class="field_title" for="image">Project Image <?php echo empty($projectDetails->id) ? '<span class="req">*</span>' : '' ?></label>
        							<div class="form_input">
        								<input name="image" id="image" type="file" tabindex="7" class="large required multi tipTop" title="Please select project image"/>
        							</div>
    							</div>
							</li>
	 							
							<li>
    							<div class="form_grid_12">
        							<label class="field_title" for="text">Company Name <span class="req">*</span></label>
        							<div class="form_input">
        								<input name="company_name" id="company_name" type="text" tabindex="1" class="required large tipTop" title="Please enter the company name" value="<?php echo set_value('company_name', @$projectDetails->company_name) ?>"/>
        							</div>
    							</div>
							</li>
							
							<li>
    							<div class="form_grid_12">
        							<label class="field_title" for="text">Location <span class="req">*</span></label>
        							<div class="form_input">
        								<input name="location" id="location" type="text" tabindex="1" class="required large tipTop" title="Please enter the location" value="<?php echo set_value('location', @$projectDetails->location) ?>"/>
        							</div>
    							</div>
							</li>
						
								<li>
								<div class="form_grid_12">
									<div class="form_input">
										<button type="submit" class="btn_small btn_blue" tabindex="9"><span>Submit</span></button>
									</div>
								</div>
								</li>
							</ul>
                    
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>