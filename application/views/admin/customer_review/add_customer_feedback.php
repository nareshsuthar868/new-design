<?php
$this->load->view('admin/templates/header.php');
?>
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6><?php echo $heading ?></h6>
                        
					</div>
					<div class="widget_content">
					<?php 
						$productattributes = array('class' => 'form_container left_label', 'id' => 'customer_feedback_form', 'enctype' => 'multipart/form-data');
						if(!empty($customerFeedbackDetails->id)){
						    echo form_open_multipart('admin/customer_feedback/edit_customer_feedback/'.$customerFeedbackDetails->id,$productattributes);
						}else{
						    echo form_open_multipart('admin/customer_feedback/add_customer_feedback',$productattributes);
						}
						
					?>
                    
						<ul>
						    <li>
    							<div class="form_grid_12">
        							<label class="field_title" for="customer_name">Customer Name <?php echo empty($customerFeedbackDetails->id) ? '<span class="req">*</span>' : '' ?></label>
        							<div class="form_input">
        								<input name="customer_name" id="customer_name" type="text" tabindex="7" class="large required multi tipTop" title="Customer Name" value="<?php echo set_value('customer_name', @$customerFeedbackDetails->customer_name) ?>"/>
        							</div>
    							</div>
							</li>
						    <li>
    							<div class="form_grid_12">
        							<label class="field_title" for="product_image">Produc Image <?php echo empty($customerFeedbackDetails->id) ? '<span class="req">*</span>' : '' ?></label>
        							<div class="form_input">
        								<input name="product_image" id="product_image" type="file" tabindex="7" class="large required multi tipTop" title="Please select product image"/>
        							</div>
    							</div>
							</li>
	 							
							<li>
    							<div class="form_grid_12">
        							<label class="field_title" for="text">Customer Comment <span class="req">*</span></label>
        							<div class="form_input">
        								<input name="customer_comment" id="customer_comment" type="text" tabindex="1" class="required large tipTop" title="Please enter the customer comment" value="<?php echo set_value('customer_comment', @$customerFeedbackDetails->customer_comment) ?>"/>
        							</div>
    							</div>
							</li>
							
							<li>
    							<div class="form_grid_12">
        							<label class="field_title" for="text">Customer Location <span class="req">*</span></label>
        							<div class="form_input">
        								<input name="customer_place" id="customer_place" type="text" tabindex="1" class="required large tipTop" title="Please enter the customer location" value="<?php echo set_value('customer_place', @$customerFeedbackDetails->customer_place) ?>"/>
        							</div>
    							</div>
							</li>
						
							<li>
								<div class="form_grid_12">
									<div class="form_input">
										<button type="submit" class="btn_small btn_blue" tabindex="9"><span>Submit</span></button>
									</div>
								</div>
							</li>
							</ul>
                    
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>
<?php 
$this->load->view('admin/templates/footer.php');
?>