<?php
$this->load->view('admin/templates/header.php');
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.min.css" />
<div id="content">
		<div class="grid_container">
			<div class="grid_12">
				<div class="widget_wrap">
					<div class="widget_top">
						<span class="h_icon list"></span>
						<h6><?php echo $heading ?></h6>
                        
					</div>
					<div class="widget_content">
					<?php 
						$productattributes = array('class' => 'form_container left_label', 'id' => 'banner_form', 'enctype' => 'multipart/form-data');
						if(!empty($bannerDetails->id)){
						    echo form_open_multipart('admin/banner/edit_banner/'.$bannerDetails->id,$productattributes);
						}else{
						    echo form_open_multipart('admin/banner/add_banner',$productattributes);
						}
						
					?>
                    
						<ul>
						    <li>
    							<div class="form_grid_12">
        							<label class="field_title" for="image">Banner Image <?php echo empty($bannerDetails->id) ? '<span class="req">*</span>' : '' ?></label>
        							<div class="form_input">
        								<input name="image"  type="file" id="upload_image"  tabindex="7" class="large required tipTop" title="Please select banner image"/>
        						        <input type="hidden" id="banner_id" value="<?php echo $bannerDetails->id; ?>">
        							</div>
    							</div>
							</li>
							
							<?php
							    if(!empty($bannerDetails->image)){
							?>
							        <li>
            							<div class="form_grid_12">
                							<label class="field_title" for="image">Current Image</label>
                							<div class="form_input">
                								<img src="<?php echo base_url(); ?>images/banner/<?php echo $bannerDetails->image ?>">
                							</div>
            							</div>
        							</li>
							<?php
							    }
							?>
	 							
							<li>
    							<div class="form_grid_12">
        							<label class="field_title" for="text">Banner Text <span class="req">*</span></label>
        							<div class="form_input">
        								<input name="text" id="banner_text" type="text" tabindex="1" class="required large tipTop" title="Please enter the banner text" value="<?php echo set_value('text', @$bannerDetails->text) ?>"/>
        							</div>
    							</div>
							</li>
						
								<li>
								<div class="form_grid_12">
									<div class="form_input">
										<button type="button" class="btn_small btn_blue" onclick="AddUpdateBanner()" tabindex="9" ><span>Submit</span></button>
									</div>
								</div>
								</li>
							</ul>
                    
						</form>
					</div>
				</div>
			</div>
		</div>
		<span class="clear"></span>
	</div>
</div>


<div id="uploadimageModal" class="modal" role="dialog" style="display:none;">
 <div class="modal-dialog">
  <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload & Crop Image</h4>
        </div>
        <div class="modal-body">
          <div class="row">
       <div class="col-md-8 text-center">
        <div id="image_demo" style="width:100%; margin-top:30px"></div>
       </div>
       <div class="col-md-4" style="padding-top:30px;">
        <br />
        <br />
        <br/>
        <!--<button class="btn btn-success crop_image">Crop & Upload Image</button>-->
         <button class="modalCloseImg simplemodal-close crop_image">Crope</button>
     </div>
    </div>
        </div>
        <!--<div class="modal-footer">-->
        <!--  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
        <!--</div>-->
     </div>
    </div>
</div>

<script>  
var bannerImage = '';
$(document).ready(function(){
 $image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:1592,
      height:480,
      type:'square' //circle
    },
    boundary:{
      width:1592,
      height:480
    }
  });

  $('#upload_image').on('change', function(){
    var reader = new FileReader();
    reader.onload = function (event) {
      $image_crop.croppie('bind', {
        url: event.target.result
      }).then(function(){
        console.log('jQuery bind complete');
      });
    }
    reader.readAsDataURL(this.files[0]);
    $('#uploadimageModal').modal('show');
  });

  $('.crop_image').click(function(event){
// function submitBannerForm(){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
        bannerImage = response;
     
    })
// }
  });
  
  

}); 


  function AddUpdateBanner(){
      $.ajax({
        url:  baseURL + 'admin/banner/addupdate_banner',
        type: "POST",
        data:{"image": bannerImage,'banner_text':$('#banner_text').val(),'banner_id':$('#banner_id').val()},
        success:function(data)
        {
            alert(data.msg);
            window.location.href= baseURL + "admin/banner/banner_listing";
        //   $('#uploadimageModal').modal('hide');
        //   $('#uploaded_image').html(data);
        }
      });
  }
</script>
<style>
    .simplemodal-container{height: 800px !important;width: 1700px !important;left: 241px !important;}
</style>
<?php 
$this->load->view('admin/templates/footer.php');
?>