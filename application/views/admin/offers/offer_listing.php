<?php
$this->load->view('admin/templates/header.php');
extract($privileges);
?>
<div id="content">
	<div class="grid_container">
		<div class="grid_12">
			<div class="widget_wrap">
			    <div class="widget_top">
					<span class="h_icon blocks_images"></span>
					<h6><?php echo $heading?></h6>
				</div>
				<div class="widget_content">
					<table class="display display_tbl" id="newsletter_tbl">
					    <thead>
				    	    <tr>
							    <th class="tip_top" title="Click to sort">
							        Coupon Code
							    </th>
							    <th class="tip_top" title="Click to sort">
								    Discount Text
							    </th>
							    <th class="tip_top" title="Click to sort">
								    Position
							    </th>
							     <th class="tip_top" title="Click to sort">
								    Status
							    </th>
							    <th class="tip_top" title="Click to sort">
								    Created Date
							    </th>
    							<th>
    								 Action
    							</th>
						    </tr>
					    </thead>
					<tbody>
					    <?php 
        				if ($offers->num_rows() > 0){
        				    foreach ($offers->result() as $key =>  $row){
        				?>
        				<tr>
							<td class="center">
								<?php echo $row->coupon_code;?>
							</td>
							<td class="center">
							    <?php echo $row->price_text; ?>
							</td>
							<th class="tip_top" title="Click to sort">
								  <input type="text" name="position" id="offer_position_<?php echo $row->id; ?>" value="<?php echo $row->position; ?>" style="width: 40px;text-align: center;"> <a href="javascript:void(0)" onclick="updateOfferPostition('<?php echo $row->id; ?>','<?php echo $row->position; ?>')">Update</a>
							</th>
                            <td class="center">
					    		<?php 
        							if ($allPrev == '1' || in_array('2', $product)){
        								$mode = ($row->status == 'Publish')?'0':'1';
        								if ($mode == '0'){
        							?>
        								<a title="Click to unpublish" class="tip_top" href="javascript:confirm_status('admin/offers/change_offer_status/<?php echo $mode;?>/<?php echo $row->id;?>');"><span class="badge_style b_done"><?php echo $row->status;?></span></a>
        							<?php
        								}else {	
        							?>
        								<a title="Click to publish" class="tip_top" href="javascript:confirm_status('admin/offers/change_offer_status/<?php echo $mode;?>/<?php echo $row->id;?>')"><span class="badge_style"><?php echo $row->status;?></span></a>
        							<?php 
        								}
        							}else {
        							?>
        							<span class="badge_style b_done"><?php echo $row->status;?></span>
    							<?php }?>
							</td>
							<td class="center">
							    <?php echo $row->created_at;?>
							</td>
							<td class="center">
							<?php if ($allPrev == '1' || in_array('2', $cms)){?>
								<span><a class="action-icons c-edit" href="admin/offers/edit_offer_form/<?php echo $row->id;?>" title="Edit">Edit</a></span>
							<?php }?>
							<?php if ($allPrev == '1' || in_array('3', $cms)){?>	
								<span><a class="action-icons c-delete" href="javascript:confirm_delete('admin/offers/delete_offer/<?php echo $row->id;?>')" title="Delete">Delete</a></span>
							<?php }?>
							</td>
						</tr>
						<?php 
							}
						}
						?>
						</tbody>
						<tfoot>
						<tr>
						    <th class="tip_top" title="Click to sort">
						        Coupon Code
						    </th>
						    <th class="tip_top" title="Click to sort">
							    Discount Text
						    </th>
						   <th class="tip_top" title="Click to sort">
								Position
						    </th>
						    <th class="tip_top" title="Click to sort">
								Status
							</th>
						    <th class="tip_top" title="Click to sort">
							    Created Date
						    </th>
							<th>
								 Action
    						</th>
						</tr>
						</tfoot>
						</table>
					</div>
				</div>
			</div>
	
		</div>
		<span class="clear"></span>
	</div>
</div>
<script>
    function updateOfferPostition(offer_id,old_pos){
        var offer_positionValue = $('#offer_position_'+offer_id).val();
            $.ajax({
                type: 'POST',
                url: baseURL + 'admin/offers/update_offer_position',
                dataType: 'json',
                data: {
                     'position': offer_positionValue,
                     'offer_id': offer_id
                },
                success:function(response){
                    if(response.status  == 400){
                        $('#offer_position_'+offer_id).val(old_pos);
                    }
                    alert(response.msg);
                }
            });
    }
</script>
<?php 
$this->load->view('admin/templates/footer.php');
?>