<?php
$this->load->view('admin/templates/header.php');
?>

<div id="content">
  <div class="grid_container">
    <div class="grid_12">
      <div class="widget_wrap">
        <div class="widget_wrap tabby">
          <div class="widget_top"> <span class="h_icon list"></span>
            <h6>Add New Offer</h6>
          </div>
          <div class="widget_content">
            <?php 
				      $attributes = array('class' => 'form_container left_label', 'id' => 'adduser_form');
				      echo form_open('admin/offers/insertUpdateOffers',$attributes) 
			       ?>
            <div id="tab1">
              <ul>
                <li  id="SubCategoryli">
                  <div class="form_grid_12">
                    <label class="field_title" for="page_title">Offer Price Text</label>
                    <div class="form_input">
                        <input type="text" name="price_text" class="large tipTop required" title="Please enter offer price text" placeholder="15% Discount">
                    </div>
                  </div>
                </li>
                <li>
                  <div class="form_grid_12">
                    <label class="field_title" for="description">Offer Price Below Text</label>
                    <div class="form_input">
                      <input type="text" name="price_below_text" class="large tipTop required" title="Please enter offer price text" placeholder="on your first order"> 
                    </div>
                  </div>
                </li>
                
                <li>
                  <div class="form_grid_12">
                    <label class="field_title" for="description">Offer Coupon Code</label>
                    <div class="form_input">
                      <input type="text" name="coupon_code" class="large tipTop required" title="Please enter coupon code text" placeholder="SHORT3M"> 
                    </div>
                  </div>
                </li>
                
                 <li>
                  <div class="form_grid_12">
                    <label class="field_title" for="description">Max Discount Amount</label>
                    <div class="form_input">
                      <input type="text" name="max_discount" class="large tipTop required" title="Please enter max discount amount" placeholder="1200" > 
                    </div>
                  </div>
                </li>
                
                <li>
                  <div class="form_grid_12">
                    <label class="field_title" for="description">Offer Description</label>
                    <div class="form_input" id="dynamicDescription">
                      <input type="text" name="description[]"  class="large tipTop required" title="Please enter description" placeholder="First month's rent OFF on furniture and home appliances"> 
                      <button type="button" onclick="addmorerow()" >Add More</button>
                    </div>
                  </div>
                </li>
                
                <li style="display:none">
                  <div class="form_grid_12">
                    <label class="field_title" for="description">Tenure Text</label>
                    <div class="form_input">
                      <input type="text" name="offer_text" class="large tipTop required" title="Please enter description"> 
                    </div>
                  </div>
                </li>
                
                <li>
                  <div class="form_grid_12">
                    <label class="field_title" for="description">Publish On Wishlist Page</label>
                    <div class="form_input">
                      <input type="checkbox" name="is_publish_wishlist" value="1" > 
                    </div>
                  </div>
                </li>
                
              </ul>
              <ul>
                <li>
                  <div class="form_grid_12">
			             <div class="form_input">
			  	            <button type="submit" class="btn_small btn_blue" tabindex="5"><span>Submit</span></button>
				            </div>
		              </div>
                </li>
                </ul>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
<span class="clear"></span> </div>
</div>

<script type="text/javascript">

    function addmorerow(){
        $('#dynamicDescription').append('<input type="text" name="description[]" class="large tipTop required"  placeholder="First month\'s rent OFF on furniture and home appliances" title="Please enter description" style="margin-top: 12px;">');
    }
</script>
<?php 
$this->load->view('admin/templates/footer.php');
?>
