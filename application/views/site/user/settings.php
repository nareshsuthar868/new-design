<?php $this->load->view('site/templates/header_inner'); ?>
	<div class="page-wrapper flex-full">
		<div class="my-account-section flex-full">
			<div class="container">
				<div class="my-account-wrapper flex-full">
				    <?php $this->load->view('site/user/settings_sidebar');?>
					<div class="account-content-area flex-full align-items-start align-content-start">
						<h2 class="border-left">Profile</h2>
						<ul class="profile-listing flex-full">
							<li>
								<div class="profile-wrapper flex-full">
									<form class="profile-setting-form credit-form flex-full"  id="profile_settings_form" method="post" action="site/user_settings/changePhoto">
										<div class="FlowupLabels">
											<div class="fl_wrap">
												<label class="fl_label" for="name">Full Name *</label>
												<input class="fl_input" type="text" name="name" id="name" value="<?php echo $userDetails->row()->full_name;?>"  />
											</div>
											<div class="fl_wrap">
												<label class='fl_label' for='email'>Email  
												<?php if ($userDetails->row()->is_verified == 'No'){ ?>
												    <span class="error-color">( Not Verified )</span>
												<?php }else{?>
												<span class="success-color">( Verified )</span>
												<?php } ?>
												</label>
												<input class="fl_input" type="email"  name="setting-email" id="email" data-email="<?php echo $userDetails->row()->email;?>"  value="<?php echo $userDetails->row()->email;?>" />
												<span id="clockdiv" class="d-none" style="color: crimson;"></span>
												<?php if ($userDetails->row()->is_verified == 'No'){ ?>
												    <a href="javascript:void(0)"  id="do_verify" class="text-link" onclick="resendConfirmation('<?php echo  $userDetails->row()->email; ?>')" >Verify</a>
                                                <?php }?>
											</div>
											<div class="fl_wrap">
												<label class="fl_label" for="phone_no">Mobile Number 
												<?php if ($userDetails->row()->is_mobile_verified == 'No'){ ?>
												    <span id="is_validate" class="error-color">( Not Verified )</span>
												<?php }else{?>
													<span id="is_validate" class="success-color">( Verified )</span>
												<?php }?>
												</label>
												<input class="fl_input" type="text" id="phone_no" name="phone_no" value="<?php echo $userDetails->row()->phone_no;?>" />
											    <span id="clockdiv" class="d-none" style="color: crimson;"></span>
												<?php if ($userDetails->row()->is_mobile_verified == 'No'){ ?>
												    <a href="javascript:void(0)"  id="do_verify" class="text-link" onclick="Verified_field()" >Verify</a>
                                                <?php }?>
											</div>
											
											<div class="fl_wrap otp_section d-none">
												<label class="fl_label" for="phone_no">OTP</label>
												<input id="new_otp" class="fl_input" name="new_otp"  type="text">
												<a href="javascript:void(0)"  onclick="verify_otp()"  class="text-link" onclick="Verified_field()" >Confirm</a>
											</div>
											
										</div>										
										<div class="submit-btn flex-full justify-content-center">
											<input type="submit" name="" onclick="return profileUpdate();" class="explore-btn proceed" value="Save">
										</div>
									</form>
								</div>
							</li>
							<li>
								<div class="profile-wrapper flex-full">
									<form class="profile-setting-form credit-form flex-full"  action="<?php echo base_url().'site/user_settings/change_user_password'?>" method="post" id="change_pass">
										<div class="FlowupLabels change-passwrod">
											<div class="fl_wrap password-field form-grp flex-full populated">
												<label class="fl_label" for="pass">New Password</label>
												<input class="fl_input" type="password" id="pass" name="pass" value="" data-error="#errpassword" />
												  <a class="password-show" href="javascript:void(0)"><i class="icn"></i></a>
											    <span class="error" id="errcpassword"></span>
											</div>
											<div class="fl_wrap password-field form-grp flex-full populated" id="passwordBox">
												<label class="fl_label" for="confirmpass">Confirm Password</label>
												<input class="fl_input" name="confirmpass" id="confirmpass"  type="password" data-error="#errcpassword"/>
											    <a class="password-show" href="javascript:void(0)"><i class="icn"></i></a>
												    <span class="error" id="errpassword"></span>
											</div>											
										</div>										
										<div class="submit-btn flex-full justify-content-center">
											<input type="submit"  class="explore-btn proceed" onclick="change_user_password()" value="Change Password">
										</div>
									</form>
								</div>
								<?php if (validation_errors() != ''){?>
                                                    <div id="validationErr">
                                                        <script>setTimeout("hideErrDiv('validationErr')", 2000);</script>
                                                        <span class="d_inline_m second_font fs_medium color_red d_md_block"><?php echo validation_errors();?></span>
                                                    </div>
                                                    <?php }?>
                                                    <?php if($flash_data != '') { ?>
                                                    <div class="errorContainer" id="<?php echo $flash_data_type;?>">
                                                        <script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 2000);</script>
                                                        <span class="d_inline_m second_font fs_medium color_red d_md_block"><?php echo $flash_data;?></span>
                                                    </div>
                                <?php } ?>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('site/templates/footer'); ?>
</body>
</html>