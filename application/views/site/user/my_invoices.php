<?php 
$this->load->view('site/templates/header_inner.php');
?>
		<div class="page-wrapper flex-full">
			<div class="my-account-section flex-full">
				<div class="container">
					<div class="my-account-wrapper flex-full">
                        <?php 
                            $this->load->view('site/user/settings_sidebar');
                        ?>
						<div class="account-content-area flex-full align-items-start align-content-start">
							<h2 class="border-left">Invoices</h2>
							<div class="my-payments-wrapper flex-full">
								<div class="pay-block flex-full justify-content-end">
							        <!--<form method="post" action="site/user_settings/makeOutStandingpayment">-->
    							        <?php
    							            $total = array_sum(array_map(function($item) {
                                                  if($item['current_sub_status'] != 'paid') {
                                                    return $item['total'];
                                                  }
                                              }, $my_invoice));
                                            if($_SESSION['Invoice_Applied_Walled']){
                                                $total = $total - $_SESSION['Invoice_Applied_Walled'];
                                            }
                                        ?>
    									<span>Total Outstanding : <strong>&#x20B9; <?php echo $total; ?></strong></span>
    									<!--<a href="javascript:void(0)" class="explore-btn">Pay Now</a>-->
    									<?php if($total < 1){ 
    									echo '<a type="submit" class="explore-btn invoice-payment-button" style="pointer-events: none;
  cursor: default; opacity: 0.7;">Pay Now</a>';
    									} else{ 
    										echo '<a type="submit" class="explore-btn invoice-payment-button" >Pay Now</a>';
										} ?> 

								    <!--</form>-->
								</div>
								<div class="table-wrapper d-none-xs">
									<table>
										<tr>
											<th>Invoice Number</th>
											<th>Order Number</th>
											<th>Amount</th>
											<th>Invoice Date</th>
											<th>Status</th>
											<th>Action</th>
										</tr>
										<?php foreach($my_invoice as $key => $row){ ?>
										<tr>
											<td><span>	<?php echo $row['invoice_number'];?></span></td>
											<td><span> 	<?php echo $row['deal_code'];?></span></td>
											<td><span>&#x20B9; <?php echo $row['total'];?></span></td>
											<td><span><?php echo date("Y-m-d", strtotime($row['created_time'])); ?></span></td>
											<td>
											    <?php
                                            	    if($row['current_sub_status'] == 'paid'){
                                                		echo '<span style=color:green>Payment Done</span>';
                                                	}else{
                                                        echo '<span style=color:red>Payment Due</span>';
                                                	}
                                                ?>
											   
										    </td>
											<td><span><a target="_blank" href="<?php echo $row['invoice_url'];  ?>" class="action-btn">Download
														Invoice</a></span></td>
										</tr>
										<?php } ?>

									</table>
								</div>
								<div class="table-mobile account-accordion flex-full d-none d-flex-xs">
									<div class="accordion">
								    <?php foreach($my_invoice as $key => $row){ ?>
										<div class="accordion-tab <?php if($key == 0){ echo "current";} ?>" >
											<h3 class="accordion-title set-tab">
												<span class="h4">Invoice Number</span>
												<span class="span">	<?php echo $row['invoice_number'];?></span>
												 <!--<i class="fa fa-plus"></i>-->
											</h3>
											<div class="accordion-content">
												<div class="accordion-body">
													<ul class="product-desc-listing flex-full">
														<li>
															<h4>Invoice Number</h4>
															<span><?php echo $row['invoice_number'];?></span>
														</li>
														<li>
															<h4>Order Number</h4>
															<span><?php echo $row['deal_code'];?></span>
														</li>
														<li>
															<h4>Amount</h4>
															<span>&#x20B9; <?php echo $row['total'];?></span>
														</li>
														<li>
															<h4>Invoice Date</h4>
															<span><?php echo date("Y-m-d", strtotime($row['created_time'])); ?></span>
														</li>
														<li>
															<h4>Status</h4>
															 <?php
                                                        	    if($row['current_sub_status'] == 'paid'){
                                                            		echo '<span style=color:green>Payment Done</span>';
                                                            	}else{
                                                                    echo '<span style=color:red>Payment Due</span>';
                                                            	}
                                                            ?>
															<!--<span></span>-->
														</li>
														<li>
															<span class="flex-full justify-content-end download-btn">
																<a target="_blank" href="<?php echo $row['invoice_url'];  ?>" class="action-btn">Download
																	Invoice</a>
															</span>
														</li>
													</ul>
												</div>
											</div>
										</div>
								    <?php }?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
$this->load->view('site/templates/footer');
?>
    <!-- Mini Cart section starts -->
	<div class="cart-main-section minicart-section flex-full align-items-start align-content-start " id="invoice-payment-slider">
		<div class="minicart-section-wrapper flex-full h-100 align-items-start align-content-start">
			<a><span class="close close-minicart"><i class="icn icn-close-black"></i></span></a>
		    <div class="minicart-main flex-full h-100 align-items-start align-content-start position-relative">
			    <h2 class="border-left">Outstanding Payment</h2>
				<div class="order-summary-scroll mCustomScrollbar content">
			        <ul class="order-summary-listing flex-full" id="min-cart-order-summary">
		            	<div class="coin-balance-wrapper flex-full">
		            	    <h3>Outstanding Amount : <strong>&#x20B9; <?php echo $total; ?></strong></h3>
							<div class="outstanding-payment-column flex-full">
								<div class="cf-balance-block flex-full">
									<h3>Your CF Coins Balance : <span class="coins-amt"><i class="icn icn-coins-gold"></i>
										<?php if($my_wallet->num_rows() > 0){ echo $my_wallet->row()->topup_amount - $_SESSION['Invoice_Applied_Walled'];; } else { echo 0; } ?>
									    </span> 
								    </h3>
                                    <div class="coins-redeemed flex-full" id="redeemcoinDiv">
    								    <label>Coins to be Redeemed</label>
    							    	<?php
    						            if(isset($_SESSION['Invoice_Applied_Walled'])){ ?>
    						                <input type="text" id="wallet_amount" name="" value="<?php echo $_SESSION['Invoice_Applied_Walled']; ?>">
            						        <span class="redeem-btn flex-full justify-content-center"><a href="javascript:void(0)" class="explore-btn" onclick="RemoveAppliedWalletAmount('<?php echo $_SESSION['Invoice_Applied_Walled'] ?>','<?php echo $my_wallet->row()->topup_amount; ?>','invoice_payment')">Remove</a></span>
    						            <?php }else{ ?>
    				                        <input type="text" id="wallet_amount" name="" value="">
    								        <span class="redeem-btn flex-full justify-content-center"><a href="javascript:void(0)" onclick="VerifyWalletAndApply('invoice_payment')" class="explore-btn">Redeem</a></span>
    						           <?php } ?>
    								</div>
							        <div id="wallet_message"></div>
							    </div>
							</div>
						</div>
                    </ul>
				</div>
				<div class="checkout-wrapper flex-full" id="mini-cart-checkout-wrapper">
					<ul class="product-desc-listing flex-full">
						<li>
							<h4>Total Amount to Pay : </h4>
							<span id="min-cart-duration" class="cartInvoiceBal"><?php echo $total; ?></span>
						</li>                							
					</ul>
					<form method="post" action="site/user_settings/makeOutStandingpayment" style="flex: auto;">
					<span class="submit-btn flex-full justify-content-center">
						<button type="submit"  id="invoicepayment_button" class="explore-btn">Pay Now</button>
					</span>
					</form>
				</div>
            </div>
        </div>
    </div>
</div>
</body>
</html>