<?php $this->load->view('site/templates/header_inner'); ?>
	<div class="page-wrapper flex-full">
		<div class="my-account-section flex-full">
			<div class="container">
				<div class="my-account-wrapper flex-full">
				    <?php $this->load->view('site/user/settings_sidebar');?>
					<div class="account-content-area flex-full align-items-start align-content-start">
						<h2 class="border-left">My Orders</h2>
						<ul class="my-orders-listing flex-full">
						    <?php
						    foreach ($purchasesList->result() as $row){ 
						    ?>
							<li>
								<div class="my-orders-box flex-full">
									<div class="order-details-wrapper flex-full">
										<ul class="product-desc-listing flex-full">
											<li>
												<h4>Order Number</h4>
												<span><?php echo $row->dealCodeNumber;?></span>
											</li>
											<li>
												<h4>Order Date</h4>
												<span><?php echo $row->created;?></span>
											</li>
											<li>
												<h4>Payment Mode</h4>
												<span><?php echo $row->so_payment_mode ? $row->so_payment_mode : 'NA' ; ?></span>
											</li>											
										</ul>
										<div class="my-order-actions flex-full">
											<a target="_blank" href="view-purchase/<?php echo $row->user_id;?>/<?php echo $row->dealCodeNumber;?>" >View Order</a>
								    		<a href="#manage-order" class="manage_order" onclick="zoho_submit('<?php  echo $row->user_id; ?>','<?php  echo $row->dealCodeNumber; ?>')">Manage Order</a>
								    	    <a href="#change-payment-mode" class="change_payment_mode" onclick="zoho_update('<?php  echo $row->dealCodeNumber; ?>','<?php echo  $row->so_payment_mode; ?>','<?php  echo $row->user_id; ?>')">Change Payment Mode</a>
										</div>
									</div>
								    <input type="hidden" id="user_id" name="user_id" value="">
                                    <input type="hidden" id="deal_id" name="deal_id" value="">
									<div class="order-status-wrapper flex-full">
										<h3>Order Status</h3>
										<ul class="fixed-rental-plans-steps-listing flex-full justify-content-between">
										    <?php if($row->zoho_sub_status == 'KYC In Progress'){ ?>
    											<li class="active"><span>KYC in Progress</span></li>
    										   	<li><span>KYC Completed</span></li>
    											<li><span>Delivery Scheduled</span></li>							
    											<li><span>Delivered</span></li>	
											<?php }else if($row->zoho_sub_status == 'KYC Completed'){ ?>
    											<li class="complete"><span>KYC in Progress</span></li>
    											<li class="active"><span>KYC Completed</span></li>
    											<li><span>Delivery Scheduled</span></li>							
    											<li><span>Delivered</span></li>		
											<?php }else if($row->zoho_sub_status == 'Delivery Scheduled'){ ?>
												<li class="complete"><span>KYC in Progress</span></li>
    											<li class="complete"><span>KYC Completed</span></li>
    											<li class="active"><span>Delivery Scheduled</span></li>							
    											<li><span>Delivered</span></li>	
											<?php }else if($row->zoho_sub_status == 'Delivered') {?>
												<li class="complete"><span>KYC in Progress</span></li>
    											<li class="complete"><span>KYC Completed</span></li>
    											<li class="complete"><span>Delivery Scheduled</span></li>							
    											<li class="complete"><span>Delivered</span></li>	
										    <?php }else{ ?>
    										    <li><span>KYC in Progress</span></li>
    										   	<li><span>KYC Completed</span></li>
    											<li><span>Delivery Scheduled</span></li>							
    											<li><span>Delivered</span></li>	
										    <?php }?>
										</ul>
									</div>
								</div>
							</li>
							<?php }?>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	

	<div id="manage-order" class="manage-order-modal product-modal white-popup-block mfp-hide">
		<div class="manage-orders-wrapper flex-full">
		    <form class="credit-form manage-order-form flex-full" id="case_form">
			    <h3>Manage Order</h3>
				<div class="FlowupLabels">
					<div class="fl_wrap service-request-wrapper">
						<label class='' for='service_request_type'>Service Request Type</label>
						<span class="service_request_type" id="service_request_type_name">Select Service Type</span>
						<ul id="service_request_type" class="service-type-listing">
							<li value="cancellation"><span>Cancel Order</span></li>
							<li value="full_extension"><span>Extend Tenure</span></li>
							<li value="request_pickup"><span>Request Order Pickup</span></li>
							<li value="repair"><span>Repair</span></li>
							<li value="replacement"><span>Replacement</span></li>
							<li value="upgrade"><span>Upgrade</span></li>
							<li value="installation"><span>Installation</span></li>
							<li value="relocation"><span>Relocation</span></li>
							<li value="buy"><span>Buy</span></li>
							<li value="change_bill_cycle"><span>Change Bill Cycle</span></li>
					    </ul>
					</div>
					<div class="fl_wrap service-request-wrapper d-none" id="pickup_type_div">    
				        <label class='' for='pickup_reason'>Pickup Type</label>
					    <select name="pickup_reason" class="fl_input focused" id="pickup_reason">
					        <!--<option value="default" selected="true">Pickup Reason</option>-->
					        <option value="Partial">Partial</option>
                            <option value="Full">Full</option>
                        </select>
					</div>
					<div class="fl_wrap service-request-wrapper d-none" id="pickup_reason_div">
					    <label class='' for='pickup_reason'>Pickup Reason</label>
					    <select name="pickup_reason" class="fl_input focused" id="pickup_reason">
					        <!--<option value=""  selected="true">Pickup Reason</option>-->
                            <option value="Products not needed anymore">Products not needed anymore</option>
                            <option value="Did not like products">Did not like products</option>
                            <option value="Faced problem in service">Faced problem in service</option>
                            <option value="Faced problems with products">Faced problems with products</option>
                            <option value="Switching to other provider">Switching to other provider</option>
                            <option value="Want to purchase things now">Want to purchase things now</option>
                            <option value="Moving to other city">Moving to other city</option>
                            <option value="Moving out of country">Moving out of country</option>
                        </select>
					</div>
					<div class="fl_wrap service-request-wrapper d-none"  id="Possible_Values">
					    <label class='' for='possible_value'>Cancellation Reason</label>
					    <select name="Possible_Values" class="fl_input focused" id="possible_value">
					        <!--<option value="default" selected disabled >Cancellation Reason</option>-->
                            <option>Wrong Items Selected</option>
                            <option>Late Delivery</option>
                            <option>Want To Buy Items</option>
                            <option>Items Not Required Anymore</option>
                            <option>Other</option>
                        </select>
					</div>
					<div class="fl_wrap service-request-wrapper w-100 d-none" id="tenure">
					    <label class='' for='get_tenure'>Extension Tenure(Months)</label>
					    <select class="fl_input focused" id="get_tenure">
                            <?php  for ($i=1; $i <= 24; $i++) { ?>
                            <option><?php echo $i; ?></option> <?php } ?>
                        </select>
					</div>
					<div class="fl_wrap service-request-wrapper w-100 d-none" id="date_picker">
						<label class='fl_label' for='Chose_date'>Preferred Pickup Date</label>
						<input type="text" class='fl_input'   id="Chose_date"  style="z-index: 10000;" >
					</div>
					<div class="fl_wrap service-request-wrapper w-100 d-none" id="allign_bill_cycle">
				       <div class="col-75">
                            <div class="col-lg-10 col-sm-10 col-md-10 addressinput">
                                <div class="checkdiv">
                                    <input id="allign_bill_cycle_check" name="allign_bill_cycle_check" type="checkbox">
                                    <label for="allign_bill_cycle_check" style="padding: 1px; padding-left: 30px;">Align Bill Cycle to 1st day of Month</label>
                                </div>
                            </div>
                        </div>
					</div>
					<div class="fl_wrap service-request-wrapper w-100 d-none" id="requested_date_picker">
						<label class='fl_label' for='requested_date'>Preferred Pickup Date</label>
					    <input  class="fl_input" type="text"   id="requested_date" readonly="true">
					</div>
					<div class="fl_wrap w-100">
						<label class='fl_label' for='description'>Description</label>
						<input class='fl_input' type='text' id='description' />
					</div>
				</div>
				<div class="submit-btn flex-full justify-content-center">
					<input type="button" onclick="Create_zoho_cash()" name="" value="Submit" class="explore-btn">
				</div>
			</form>
		</div>
	</div>
	<div id="change-payment-mode" class="change-payment-mode manage-order-modal product-modal white-popup-block mfp-hide">
		<div class="manage-orders-wrapper flex-full">
			<form class="credit-form manage-order-form flex-full" id="payment_case_form">
			    <h3>Change Payment Mode</h3>
				<div class="FlowupLabels">
					<div class="fl_wrap service-request-wrapper">
						<label class='' for='service_request_type'>Current Payment Mode</label>
						<input type="hidden" vlue="NA" id="ctype" readonly>
						<span id="ctype_back">Online</span>
					</div>
					<div class="fl_wrap service-request-wrapper">
					    <label class='' for='rtype'>Select Request Type</label>
						<!--<label class='' for='service_request_type'>Future Payment Mode</label>-->
						<!--<span class="service_request_type">NACH</span>-->
					    <select name="rtype" class="fl_input focused" id="rtype">
                            <!--<option value="select"  selected="true" disabled="true">Select Request Type</option>-->
                            <option value="NACH">NACH</option>
                            <option value="Enach">Enach</option>
                            <option value="Cheque">Cheque</option>
                            <option value="Online">Online</option>
                            <option value="SI On Card">SI On Card</option>
                            <option value="Cash">Cash</option> 
                            <option value="Mixed">Mixed</option>
                        </select>
					</div>
					<div class="fl_wrap w-100">
						<label class='fl_label' for='description'>Description</label>
						<input class='fl_input' type='text' id='tdescription' />
				    </div>
				</div>
				<div class="submit-btn flex-full justify-content-center">
					<input type="button"  onclick="Update_zoho_cash()" id="zoho_update_button" name="" value="Submit" class="explore-btn">
				</div>
			</form>
		</div>
	</div>
	
    <?php $this->load->view('site/templates/footer'); ?>
    </div>
<style>
.datepicker {z-index:999999999 !important;} 
</style>
<script type="text/javascript">
    var Selected_value = '';
    function zoho_submit(user_id,deal_id){
      $('#user_id').val(user_id);
      $('#deal_id').val(deal_id);
    }
    
    function zoho_update(deal_id,type,id){
      $('#deal_id').val(deal_id);
      if(type == '' || type == null){
        $('#ctype_back').html("NA");
        $('#ctype').val('NA');
      }else{  
          $('#ctype').val(type);
          $('#ctype_back').html(type)
      }
      $('#user_id').val(id);
    }

    function Create_zoho_cash(){
        // Selected_value = $('#type').val();
        var Pickup_Request = '';
        var tenure = '';
        var pickup_request_date = '';
        var Pickup_Request_Type = '';
        var pickup_reason = '';
        var requested_date = '';
        if(Selected_value == null){
            alert('Please Select Type');
            return false;
        }else if(Selected_value == 'request_pickup'){
            Pickup_Request_Type = $('#pickup_type').val();
            pickup_request_date = $('#Chose_date').val();
            pickup_reason = $('#pickup_reason').val();
            if(pickup_request_date == ''){
                alert('Please Select Date');
                return false;
            }
        }else if(Selected_value == 'change_bill_cycle'){
            if (!$('#allign_bill_cycle_check').is(":checked")){
                alert('Please Checkbox check');
                return false;
            }
        }
      
        if(Selected_value == 'full_extension'){
            tenure = $('#get_tenure').val();
        }
        if(Selected_value == 'repair' || Selected_value == 'replacement' || Selected_value == 'installation'){
            requested_date = $('#requested_date').val();
            if(requested_date == ''){
                alert('Please Select Date');
                return false;
            }
        }
          
        var user_id = $('#user_id').val();
        var deal_id = $('#deal_id').val();
        $('#zoho_submit_button').attr('disabled','disabled');
        $.ajax({
            url: baseURL + "site/order/zoho_submit",
            data: {
                'user_id':user_id,
                'deal_id':deal_id,
                'description':$('#description').val(),
                'Possible_Values':$('#possible_value').val(),
                'type':Selected_value,
                'Pickup_Request_Type':Pickup_Request_Type,
                'tenure':tenure,
                'pickup_request_date':pickup_request_date,
                'requested_date':requested_date,
                'pickup_reason':pickup_reason
            },                         
            type: 'post',
            beforeSend: function(){   
                $('#overlay').show();
            },
            success: function(result){
                $("#case_form").trigger('reset');
                $('#pickup_type_div').hide();
                $('#pickup_reason_div').hide();
                $('#Possible_Values').hide();
                $('#tenure').hide();
                $('#date_picker').hide();
                $('#requested_date_picker').hide();
                $('#overlay').hide();
                $('#allign_bill_cycle').hide();
                $('#zoho_submit_button').removeAttr('disabled','disabled');
                if(result.status){
                //   $('.mfp-bg mfp-ready').removeClass('mfp-bg mfp-ready');
                //   $('#manage-order').hide();  
                  sweetAlert({
                      title: 'Success',
                      text: result.msg,                         
                      type: "success",
                      showCancelButton: false,
                      closeOnConfirm: true,
                      animation: "slide-from-top",
                      showConfirmButton: true,      
                  });            
                }
                else{
                //   $('.mfp-bg mfp-ready').removeClass('mfp-bg mfp-ready');
                //   $('#manage-order').hide();  
                  sweetAlert({
                      title: 'warning',
                      text: result.msg,                         
                      type: "warning",
                      showCancelButton: false,
                      closeOnConfirm: true,
                      animation: "slide-from-top",
                      showConfirmButton: true,      
                  });
                }
              }
          });
        }

$(document).ready(function () {
    
    $('#service_request_type li').click(function(e){
        // alert("dfdffdf");
        var list_item = jQuery(this);
        $('#service_request_type li').removeClass('active');
        $(this).addClass('active');
        $('#service_request_type_name').html(jQuery('span', list_item).text());
        // console.log("selected values",  $('#service_request_type li').val())
        
        $('#Chose_date').val('');
        Selected_value = $(this).attr("value");
        if(Selected_value == 'cancellation'){
            $('#Possible_Values').removeClass('d-none');
        }else{
            $('#Possible_Values').addClass('d-none');
        }
    
        if(Selected_value == 'request_pickup'){
            $('#date_picker').removeClass('d-none');
            $('#pickup_type_div').removeClass('d-none');
            $('#pickup_reason_div').removeClass('d-none');
        }else{
            $('#date_picker').addClass('d-none');
            $('#pickup_type_div').addClass('d-none');
            $('#pickup_reason_div').addClass('d-none');
        }
      
        if(Selected_value == 'full_extension'){
            $('#tenure').removeClass('d-none');
        }else{
            $('#tenure').addClass('d-none');
        }
        
        if(Selected_value == 'repair' || Selected_value == 'replacement' || Selected_value == 'installation'){
            $('#requested_date_picker').removeClass('d-none');
        }else{
          $('#requested_date_picker').addClass('d-none');
        }
        
        if(Selected_value == 'change_bill_cycle'){
            $('#allign_bill_cycle').removeClass('d-none');
        }else{
            $('#allign_bill_cycle').addClass('d-none');
        }
    });
    
    var date = new Date();
    date.setDate(date.getDate()+1);
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    $('#Chose_date').datepicker({
        format: "yyyy-mm-dd",
        startDate :  date,
        minDate: today,
        autoclose: true
    }); 

    $('#requested_date').datepicker({
        format: "yyyy-mm-dd",
        startDate : date,
        minDate: today,
        autoclose: true
    });  
});
</script>
 <script src="<?php echo base_url()?>assets/js/Date_Picker.js"></script>
 <link href="<?php echo base_url()?>assets/css/datepicker.css" rel="stylesheet" type="text/css">
</html>