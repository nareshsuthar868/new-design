<!DOCTYPE html>
<html amp lang="en" style="height: 100% !important;">
<head>
	<title>Sign In page</title>
	<meta charset="utf-8">
	<link rel="canonical" href="product-listing.html">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
	<!-- CSS Files -->
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>css/bootstrap.min.css" defer/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/jquery.FlowupLabels.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/new_slick.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style-custom.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/responsive.css">
	
	<script src="<?php echo base_url(); ?>assets/bootstrap-sweetalert/dist/sweetalert.js" async defer></script>
    <link href="<?php echo base_url(); ?>assets/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css" defer/>
    <script src="<?php echo base_url(); ?>assets/bootstrap-sweetalert/dist/swalExtend.js" async defer></script>
    <link href="<?php echo base_url(); ?>assets/bootstrap-sweetalert/dist/swalExtend.css" rel="stylesheet" type="text/css" defer/>
	
	<!-- JS Files -->
    <script async src="<?php echo base_url(); ?>js/amp_v0.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap.js" ></script>
    <script src="<?php echo base_url(); ?>js/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo base_url(); ?>js/new_slick.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.FlowupLabels.js"></script>
    
</head>
<body style="height: 100% !important;" class="signin">
	<div class="signin-wrapper flex-full align-items-start align-content-start justify-content-end">
		<div class="signin-slider-wrapper flex-full">
			<div class="signin-slider w-100">
				<div class="slide">
					<img src="<?php echo base_url(); ?>images/innerpage-banner.jpg" alt="product">
				</div>
				<div class="slide">
					<img src="https://images.unsplash.com/photo-1547467601-e827558fc026?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1601&q=80" alt="product">
				</div>
				<div class="slide">
					<img src="https://images.unsplash.com/photo-1468174482686-1047396f13b3?ixlib=rb-1.2.1&auto=format&fit=crop&w=1050&q=80" alt="product">
				</div>
			</div>
		</div>
		<div class="signin-form-wrapper flex-full align-items-start align-content-start">
			<div class="signin-tab-wrapper flex-full content align-items-start align-content-start">
				<div class="signin-tab-inner-wrapper flex-full align-items-start align-content-start">
					<div class="signin-logo-part flex-full">
						<img src="<?php echo CDN_URL; ?>images/brand-logo.png" alt="logo">
					</div>
					<div class="tab-links-part flex-full">
						<ul class="tabs flex-full">
							<li class="tab-link current" data-tab="tab-1"><span>Sign In</span></li>
							<li class="tab-link" data-tab="tab-2" ><span onclick="resetForm()">Sign Up</span></li>
						</ul>
					</div>
					<div class="tab-content-part flex-full">
						<div id="tab-1" class="tab-content current">
						    <form method="post"  name="form-login" id="form-login" class="credit-form signin-form flex-full">
								<ul class="social-icons-listing flex-full">
									<li><a href="javascript:void(0)" class="linkedin" data-onload="true" 
                                    data-onsuccess="onLinkedInLoad" id="linkdinlog"><i class="icn icn-linkedin-white"></i></a></li>
									<li><a href="javascript:void(0)" onclick= "ClickLogin()" data-onload="true" class="google g-signin2" data-onsuccess="onSignIn"><i class="icn icn-google-white"></i></a></li>
									<li><a href="javascript:void(0)" class="facebook" onclick="fb_login()"><i class="icn icn-facebook-white"></i></a></li>
								</ul>
								<span class="or w-100">OR</span>
								<div class="signin-form-block flex-full FlowupLabels">
                                    <div class="alert alert-success" role="alert" style="display:none;" id="forgot_password">
                                        <p>We have sent password reset instruction on your registered email address.</p>
                                    </div>
                                    <div class="alert alert-danger" role="alert" style="display:none;" id="email_error">
                                        <p>Your email id not matched in our records.</p>
                                    </div>
									<!-- Sign In starts -->
									<div id="login-module">
									    
    									    <div class="fl_wrap form-grp flex-full">
        										<label class='fl_label' for='login_email'>Enter Email / Mobile Number</label>
        										<input class='fl_input' type='text' id='login_email' name="email" data-error="#loginEmail" oninput="checkLoginType()"/>
            										<a class="get-otp text-link" href="javascript:void(0)" id="getOtpBox" style="display:none;" onclick="$('#otpBox').show();regenerateOTP()">Get OTP <i class="icn-arrow-right"></i></a>
        									</div>
        									<div class="flex-full" id="emailError">
            										<span  class="error" id="loginEmail"></span>
            								</div>
        									
        									<div class="fl_wrap password-field form-grp flex-full" id="passwordBox" style="display:none;">
        										<label class='fl_label' for='login_password'>Enter Password</label>
        										<input class='fl_input' type='password' id='login_password' name="password" data-error="#loginPassword"/>
        										<a class="password-show" href="javascript:void(0)"><i class="icn"></i></a>
        										
        									</div>	
        									<div class="flex-full" id="passwordError">
            										<span  class="error" id="loginPassword"></span>
            								</div>
            								
            								<div class="fl_wrap otp-sent-fieldform-grp flex-full" id="otpBox" style="display:none;">
            										<label class='fl_label' for='otp'>Enter OTP</label>
            										<input class='fl_input' type='text' id='otp' maxlength="5" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" onkeyup="javascript: if (this.value.length == '5'){$('#signinbtn').show();}"/>
            										<span class="otp-timer" id="timer"></span>
            										<a class="get-otp text-link" href="javascript:void(0)" onclick="regenerateOTP()" style="display:none;" id="resendbtn">Resend <i class="icn-arrow-right"></i></a>
									        </div>
									       
            								
        									<div class="forgot-password-link flex-full justify-content-end">
        										<a class="text-link" href="javascript:void(0)" id="forgot-password-btn">Forgot password?</a>
        									</div>
        									
        									<input type="hidden" value="<?php echo base_url();?>" id="base_url">
                                            <input type="hidden" value="<?php echo $_SERVER['HTTP_REFERER'];?>" id="requested_url">
                                            <input type="text" id="otpField" style="display:none;" value="">
    									    <div class="submit-btn flex-full" style="display:none;" id="signinbtn">
        										<button id="signinsubmit123" name="signinsubmit"
                                                        onclick="verifyLoginDetails();" class="explore-btn" >Sign In</button>
                                            </div>
									</div>
									
									<!-- Sign In ends -->

									<!-- Sign In OTP starts -->
									<!--<span class="otp-message"><strong>One time password</strong> has been sent to your mobile, please enter the same to verify your mobile number</span>-->
									<!--<div class="fl_wrap form-grp flex-full">-->
									<!--	<label class='fl_label' for='mobile_number'>Enter Mobile Number</label>-->
									<!--	<input class='fl_input' type='text' id='mobile_number' value="93287 37865" />										-->
									<!--</div>-->
									<!--<div class="fl_wrap otp-sent-fieldform-grp flex-full">-->
									<!--	<label class='fl_label' for='otp'>Enter OTP</label>-->
									<!--	<input class='fl_input' type='text' id='otp'/>-->
									<!--	<span class="otp-timer">01:34</span>-->
									<!--	<a class="get-otp text-link" href="javascript:void(0)">Resend <i class="icn-arrow-right"></i></a>-->
									<!--</div> 								-->
									<!-- Sign In OTP ends -->

									

									<!-- Sign In reset password starts -->
									<!-- <div class="fl_wrap password-field form-grp flex-full">
										<label class='fl_label' for='reset_password'>Reset Password</label>
										<input class='fl_input' type='password' id='reset_password' />
										<a class="password-show" href="javascript:void(0)"><i class="icn"></i></a>
									</div>
									<div class="fl_wrap password-field form-grp flex-full">
										<label class='fl_label' for='confirm_password'>Confirm Password</label>
										<input class='fl_input' type='password' id='confirm_password' />
										<a class="password-show" href="javascript:void(0)"><i class="icn"></i></a>
									</div> -->
									<!-- Sign In reset password ends -->

									<!-- Sign In OTP starts -->
									<!-- <span class="otp-message"><strong>One time password</strong> has been sent to your mobile, please enter the same to verify your mobile number</span>
									<div class="fl_wrap otp-field form-grp flex-full">
										<label class='fl_label' for='get_otp'>Enter Email / Mobile Number</label>
										<input class='fl_input' type='text' id='get_otp' value="93287 37865"/>
										<a class="get-otp text-link" href="javascript:void(0)">Get OTP <i class="icn-arrow-right"></i></a>
									</div>
									<div class="fl_wrap password-field form-grp flex-full">
										<label class='fl_label' for='password4'>Enter Password</label>
										<input class='fl_input' type='password' id='password4'/>
									</div> -->
									<!-- Sign In OTP ends -->
                                    
								</div>
							</form>
							<!-- Sign In forgot password starts -->
									<!--<div id="forgot-password-module" style="display:none;">-->
									    <form id="forgot-pass" class="credit-form signin-form flex-full" style="display:none;">
									        <div class="signin-form-block flex-full FlowupLabels">
    									    <span class="otp-message">Please enter your registered email address.<br/>We will provide you password reset instruction.</span>
    									    <div class="fl_wrap form-grp flex-full">
        										<label class='fl_label' for='forgotemail'>Enter Email</label>
        										<input class='fl_input' type='email' id='forgotemail' name="forgotemail" data-error="#forgotemail-error"/>
    									    </div>	
    									    <div class="flex-full">
            										<span  class="error" id="forgotemail-error"></span>
            								</div>
    									    <div class="forgot-password-link flex-full justify-content-end">
        										<a class="text-link" href="javascript:void(0)" id="backto-signin-btn">Back</a>
        									</div>
    									    <div class="submit-btn flex-full">
        										<button id="forgotsubmit" name="forgotsubmit"
                                                        onclick="forgot_password();" class="explore-btn">Submit</button>
                                            </div>
                                            </div>
									    </form>
									<!--</div>-->
																	
									<!-- Sign In forgot password ends -->	
						</div>
						
						<div id="tab-2" class="tab-content">
							<form class="credit-form signin-form flex-full" name="sign-up" id="sign-up">
							    <div class="alert alert-danger" role="alert" id="error_msg" style="display: none;">
                                    
                                </div>
							    <!--<span id="error_msg" style="color: red;" style="display: none; height: 13px;width: 320px; "></span>-->
							    <div class="alert alert-success" role="alert" style="display:none;" id="success_registraion">
                                    <h4>Register Success</h4>
                                    <p>Your account has been created. We have sent you account activation link on your registered email address.</p>
                                </div>
							    <!--<div class="success-block popup-block display_none" id="success_registraion">-->
           <!--                         <div class="inner-block text-center">-->
           <!--                             <i class="material-icons">&#xE876;</i>-->
           <!--                             <h4>Register Success</h4>-->
           <!--                             <p>Your account has been created. We have sent you account activation link on your registered email address.</p>-->
           <!--                         </div>-->
           <!--                     </div>  -->
								<ul class="social-icons-listing flex-full">
									<li><a href="javascript:void(0)" class="linkedin"><i class="icn icn-linkedin-white"></i></a></li>
									<li><a href="javascript:void(0)" class="google"><i class="icn icn-google-white"></i></a></li>
									<li><a href="javascript:void(0)" class="facebook"><i class="icn icn-facebook-white"></i></a></li>
								</ul>
								<span class="or w-100">OR</span>
								<div class="FlowupLabels signin-form-block flex-full">
									<div class="fl_wrap form-grp flex-full">
										<label class='fl_label' for='user_name'>Enter Name</label>
										<input class='fl_input filled fullname' type='text' id='user_name' name="full_name" data-error="#errNm1"/>
									</div>
									<div class="flex-full">
										<span class="error" id="errNm1"></span>
									</div>
										<div class="fl_wrap form-grp flex-full">
										<label class='fl_label' for='signup_email'>Email Address</label>
										<input class='fl_input email' type='email' id='signup_email' name="signup_email" data-error="#errNm2"/>
									</div>
									<div class="flex-full">
										<span class="error" id="errNm2"></span>
									</div>
									<div class="fl_wrap password-field form-grp flex-full">
										<label class='fl_label' for='signup_password'>Enter Password</label>
										<input class='fl_input password' type='password' id='signup_password' name="signup_password"  data-error="#errNm4"/>
										<a class="password-show" href="javascript:void(0)"><i class="icn"></i></a>
									</div>
									<div class="flex-full">
										<span class="error" id="errNm4"></span>
									</div>
									<div class="form-grp flex-full">
										<div class="fl_wrap form-grp-inner flex-full">
											<label class='fl_label' for='phone_no'>Mobile Number</label>
											<input class='fl_input' type='text' id='phone_no' onkeypress='return restrictAlphabets(event)' onkeyup="verify_button()" name="mobile_number" data-error="#errNm3"/>
											<a class="get-otp text-link" href="javascript:void(0)" id="signup-get-otp" style="" onclick="generateOtpSignup();">Get OTP <i class="icn-arrow-right"></i></a>
											<span class="length" style="position:static !important;"><span>0</span> / 10</span>
										</div>
										<div class="fl_wrap form-grp-inner flex-full" style="display:none;" id="enterotpsignup">
											<label class='fl_label' for='otp_signup'>Enter OTP</label>
											<input class='fl_input' type='number' id='otp_signup' />
											<input type="text" value="" id="otpFieldSignup" style="display:none;">
                                            <span class="otp-timer" id="timer-signup"></span>
            								<a class="get-otp text-link" href="javascript:void(0)" onclick="generateOtpSignup();$('#resendbtn-signup').hide();" style="display:none;" id="resendbtn-signup">Resend <i class="icn-arrow-right"></i></a>
											
										</div>
									</div>
									<div class="flex-full">
    									<span class="error" id="errNm3"></span>
    								</div>
    								<div class="fl_wrap form-grp flex-full">
											<label class='fl_label' for='referral_code_signup'>Referral Code (optional)</label>
											<input class='fl_input' type='text' id='referral_code_signup' />
									</div>
								
									<input type='hidden' name='api_id' id="api_id"  value='<?php echo $social_login_session_array['social_login_unique_id'];?>' />
                                    <input type='hidden' name='thumbnail' id='thumbnail' value='<?php echo $social_login_session_array['social_image_name'];?>' />
                                    <input type='hidden' name='loginUserType' id='loginUserType' value='<?php if($social_login_session_array['loginUserType'] != '') echo $social_login_session_array['loginUserType']; else echo "normal";?>' />
									<div class="submit-btn flex-full">
										<button id="signupsubmit" name="signupsubmit"  onclick="return check_validation()" class="explore-btn">Sign Up</button>
									</div>
									<!-- <div class="form-grp flex-full">
										<input type="text" name="" class="input-grp filled" value="Pooja Karkhanis" placeholder="Enter Name">
									</div>
									<div class="form-grp flex-full">
										<div class="form-grp-inner flex-full">
											<input type="text" name="" class="input-grp" placeholder="Mobile Number">
											<span class="length">0 / 10</span>
										</div>
										<div class="form-grp-inner flex-full">
											<input type="text" name="" class="input-grp" placeholder="Referral Code (optional)">
										</div>
									</div>
									<div class="form-grp flex-full">
										<input type="email" name="" class="input-grp" placeholder="Email Address">
									</div>
									<div class="password-field form-grp flex-full">
										<input type="password" name="" class="input-grp" placeholder="Enter Password">
										<a class="password-show" href="javascript:void(0)"><i class="icn"></i></a>
									</div>
									<div class="submit-btn flex-full">
										<input type="submit" name="" value="Sign Up" class="explore-btn">
									</div> -->	
									
								</div>
							</form>
						</div>					
					</div>
				</div>
			</div>
		</div>
	</div>

<script type="text/javascript" src="//platform.linkedin.com/in.js" defer>
            api_key: 81tyo00okgh7jg
            authorize: true
            onLoad: onLinkedInLoad
            scope: r_basicprofile r_emailaddress
</script>
<script type="text/javascript">
jQuery(document).ready(function(){
    //jQuery("img.lazy").lazyload();


    jQuery("#linkdinlog").click(function(){
        IN.UI.Authorize().place(); 
        IN.Event.on(IN, "auth", one_click);
    });
});
</script>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#linkdin").click(function(){
        IN.UI.Authorize().place(); 
        IN.Event.on(IN, "auth", one_click);
    });
});
</script>

<script type="text/javascript">
var clicked=false;//Global Variable
    function one_click(){
        clicked=true;
    }
    // Setup an event listener to make an API call once auth is complete
    function onLinkedInLoad() {
        IN.Event.on(IN, "auth", getProfileData);          
        //return false;  
    }
    function logout(){
        IN.User.logout(onLogout);
    }
    function onLogout(){
        console.log('Logout successfully');
    }

    // Use the API call wrapper to request the member's basic profile data
    function getProfileData() {
        IN.API.Profile("me").fields("first-name", "last-name", "email-address","picture-url").result(function (data) {
            if (clicked) {  
                var userdata = data.values[0];
                var datasend = {};             
                datasend['email'] = userdata.emailAddress;           
                datasend['first_name'] = userdata.firstName;         
                datasend['last_name'] = userdata.lastName;           
                datasend['facebook_id'] = userdata.key; 
                datasend['login_type'] = "linkdin";
                if(userdata){
                    $.ajax({
                        url: "<?php echo base_url()?>site/user/sociallogin",
                        type: 'POST',
                        dataType: 'json',
                        data:  datasend,
                        success: function(x){
                            
                          //  window.location.href = x.url;
                          location.reload();
                        },
                    });
                }
            }
        });
    }
</script>
<script type="text/javascript">
    var baseURL = "<?php echo base_url(); ?>";
	var BaseURL = "<?php echo base_url(); ?>";
	
	var clicked=false;//Global Variable
    function ClickLogin(){
        clicked=true;
        console.log(clicked);
    }

    function onSignIn(googleUser) { 
        if (clicked) {  
            var profile = googleUser.getBasicProfile();     
            var datasend = {};                
            datasend['email'] = profile.getEmail();   
            datasend['gender'] = "";          
            datasend['first_name'] = profile.getName();           
            datasend['last_name'] = "";           
            datasend['facebook_id'] = profile.getId();
            datasend['name'] = profile.getName();
            datasend['login_type'] = "google";              
            datasend['str'] = '';
  
            if(profile.getEmail()){
                $.ajax({
                    url: "https://cityfurnish.com/site/user/sociallogin",
                    type: 'POST',
                    dataType: 'json',
                    data: datasend,
                    success: function(x){
                        //window.location.href = x.url;
                        location.reload();
                    }
                });
            }
        }
    }
	
	$(document).ready(function(){
	    $("#forgot-password-btn").click(function(){
	        $("#login-module").toggle("slow", function(){
	            if($("#login-module").is(':hidden')){
	                $("#forgot-pass").fadeIn();
	            }else{
	                $("#forgot-pass").fadeOut();
	            }
	        });
	    });
	    
	    $("#backto-signin-btn").click(function(){
	        $("#forgot-pass").toggle("slow", function(){
	            if($("#forgot-pass").is(':hidden')){
	                $("#login-module").fadeIn();
	            }else{
	                $("#login-module").fadeOut();
	            }
	        });
	    });
	    
		$('.signin-slider').slick({
			dots: true,
			arrows: false,
			slidesToShow: 1,
			infinite: true,
			autoplay: true,
			responsive: [		    
				{
				  breakpoint: 1025,
				  settings: {		        
					dots: true,
				  }
				}
			]
		});

		$('ul.tabs li').click(function(){
			var tab_id = $(this).attr('data-tab');

			$('ul.tabs li').removeClass('current');
			$('.tab-content').removeClass('current');

			$(this).addClass('current');
			$("#"+tab_id).addClass('current');
		});

		$(window).on("load",function(){
			$(".content").mCustomScrollbar({theme:"minimal-dark"});
		});
		
		$(".password-show").click(function(){
			if($(this).prev('input').attr('type') == 'password'){
		        $(this).prev('input').prop('type', 'text');
		    }else{
		        $(this).prev('input').prop('type', 'password');
		    }
		    
			$(this).children().toggleClass('icn-eye');
		});		

		$('.FlowupLabels').FlowupLabels({
			/*
			 * These are all the default values
			 * You may exclude any/all of these options
			 * if you won't be changing them
			 */
			
			// Handles the possibility of having input boxes prefilled on page load
			feature_onInitLoad: true, 
			
			// Class when focusing an input
			class_focused: 		'focused',
			// Class when an input has text entered
			class_populated: 	'populated'	
		});
		
		window.fbAsyncInit = function() {
            FB.init({
                 appId      : '183441015137603',
                //appId : '324383858071764',
                //appId : '145747126093085',
                cookie     : true,
                xfbml      : true,
                version    : 'v2.12'
            });
            FB.AppEvents.logPageView();   
        };
        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
		
		fb_login = function(str){ 
            FB.login(function(response) {
                if (response.authResponse) {
                    console.log('Welcome!  Fetching your information.... ');
                    access_token = response.authResponse.accessToken; //get access token
                    user_id = response.authResponse.userID; //get FB UID
                    FB.api('/me?fields=id,name,email,gender,first_name,last_name', function(response) {
                        var datasend = {};
                        datasend['id']  = response.id;             
                        datasend['email'] = response.email;   
                        datasend['gender'] = response.gender;         
                        datasend['first_name'] = response.first_name;         
                        datasend['last_name'] = response.last_name;           
                        datasend['facebook_id'] = response.id;
                        datasend['name'] = response.name;  
                        datasend['login_type'] = "facebook";               
                        datasend['str'] = str;              
                        if(response.id){
                            $.ajax({
                                url :  "<?php echo base_url()?>site/user/sociallogin",
                                type: 'POST',
                                dataType: 'json',
                                data: datasend,
                                success: function(x){
                                   // console.log(x);
                                    //window.location.href = x.url;
                                       location.reload();
                                }
                            });
                        }
                    });
        
                } else {
                    //user hit cancel button
                    console.log('User cancelled login or did not fully authorize.');
        
                }
            }, {
                scope: 'email, public_profile, user_friends'
            });
        } 
	});
</script>
<script>
    function checkLoginType()
    {
        
        var type = document.getElementById('login_email').value;
       document.getElementById('otpBox').style.display = 'none';
       
       
        if(type.indexOf('@') != -1)
        {
        
          if(type.indexOf('.') != -1)
          {
           
            document.getElementById('passwordBox').style.display = 'block';
            document.getElementById('getOtpBox').style.display = 'none';
            document.getElementById('otpBox').style.display = 'none';
            document.getElementById('signinbtn').style.display = 'block';
            document.getElementById('emailError').innerHTML = '';
          
          }
          else
          {
            document.getElementById('emailError').innerHTML = 'Invalid Email Account!';
         
            return false;
          }
        }
        else if(!isNaN(type))
        {
          if(type.length == 10 && type > 6000000000 && type < 9999999999)
          {
            
            document.getElementById('passwordBox').style.display = 'none';
            document.getElementById('getOtpBox').style.display = 'block';
            
            document.getElementById('emailError').innerHTML = '';
          }
          else if(type.length != 10 && type.length != '')
          {
              document.getElementById('emailError').innerHTML = 'Invalid Mobile Number!';
          }
          else if(type.length == '')
          {
               document.getElementById('emailError').innerHTML = '';
          }
          
        }
        else
        {
             
            document.getElementById('emailError').innerHTML = 'Invalid Email/Mobile Field!';
            return false;
        }
    }

function regenerateOTP(){
  disableResend();
  timer(30);
        $.ajax({
            url: baseURL + "site/User/send_otp",
            type: 'post',
            data: { send_to: $("#login_email").val() },
            success: function (data) {
                swal({ title: "Great!", text: "One time password Message is Sent", timer: 2000, imageUrl: "../images/thumbs-up.jpg" });
                otp = data.split('"success"');
                document.getElementById('otpField').value = otp[1];
            },
            error: function () {
                swal({ title: "Oops!", text: "We are facing technical error! :(", type: "error", timer: 2000, confirmButtonText: "Ok" });
                return false;
            }
        });

    }
    
    function generateOtpSignup()
    {
        $.ajax({
            url: baseURL + "site/User/send_otp",
            type: 'post',
            data: { send_to: $("#phone_no").val() },
            success: function (data) {
               var send_to =  $("#phone_no").val();
                if(send_to === '')
                {
                    $('#enterotpsignup').hide();                
                    swal({ title: "Warning!", text: " Mobile field is blank", timer: 2500, imageUrl: "../images/thumbs-up.jpg" });
                }else{
                    timer(30);
                    if($('phone_no').val != ''){$('#enterotpsignup').show();document.getElementById('timer-signup').innerHTML = '';}
                swal({ title: "Great!", text: "One time password Message is Sent", timer: 2000, imageUrl: "../images/thumbs-up.jpg" });
                otp = data.split('"success"');
                document.getElementById('otpFieldSignup').value = otp[1];
                 
                }
            },
            error: function () {
                swal({ title: "Oops!", text: "We are facing technical error! :(", type: "error", timer: 2000, confirmButtonText: "Ok" });
                return false;
            }
        });
    }

function disableResend()
{
 $("#getOtpBox").hide();
 $("#resendbtn").hide();
 timer(30);
  setTimeout(function() {
    // enable click after 1 second
//  $('#getOtpBox').show();
    //$('.disable-btn').prop('disabled', false);
  }, 60000); // 1 second delay
}
let timerOn = true;

function timer(remaining) {
  var m = Math.floor(remaining / 60);
  var s = remaining % 60;
  
  m = m < 10 ? '0' + m : m;
  s = s < 10 ? '0' + s : s;
  
  $login_val = $('#login_email').val;
  $signup_val = $('#phone_no').val;
  document.getElementById('timer').innerHTML = m + ':' + s;
//   if($login_val != '')
//   {
//     document.getElementById('timer').innerHTML = m + ':' + s;
//     document.getElementById('timer-signup').innerHTML = '';
//   }
//   else if($signup_val != '')
//   {
//     document.getElementById('timer-signup').innerHTML = m + ':' + s;
//     document.getElementById('timer').innerHTML = '';
//   }

  remaining -= 1;
  
  if(remaining >= 0 && timerOn) {
    setTimeout(function() {
        timer(remaining);
    }, 1000);
    return;
  }

  if(!timerOn) {
    // Do validate stuff here
   // console.log(timerOn);
    // document.getElementById('signinbtn').style.display = 'block';
    return;
  }
  
  // Do timeout stuff here
  document.getElementById('resendbtn').style.display = 'block';
//   if($('#phone_no').val != ''){ $('#resendbtn-signup').show();}
//   else{document.getElementById('resendbtn').style.display = 'block';}
  
  //alert('Timeout for OTP');
}

function restrictAlphabets(e){
	var x=e.which||e.keycode;
	if((x>=48 && x<=57) || x==8 || (x>=35 && x<=40)|| x==46)
		return true;
	else
		return false;
}
function verifyLoginDetails()
{
    if(document.getElementById('otp').value != '')
    {
        var otp = document.getElementById('otp').value;
        var matchOtp = document.getElementById('otpField').value;
        if(otp == matchOtp)
        {
            loginThroughMobile();
        }
        else
        {
            swal({ title: "Oops!", text: "OTP entered does not match. Try Again! :(", type: "error", timer: 2500, confirmButtonText: "Ok" });
                return false;
        }
    }
    else
    {
        login();
    }
}
function loginThroughMobile()
{
     $("#form-login").validate({
        submitHandler: function(e) {
            $("#base_url").val();
            var mobile = $("#login_email").val(),
                requested_url = $("#requested_url").val();
            $.ajax({
                method: "POST",
                url: baseURL + "site/user/login_user_mobile",
                data: {
                    mobile: mobile
                },
                success: function(e) {
                    if (e.status)
                        if (1 == e.key) {
                            var t = window.location.href;
                            // var t = window.history.back();
                            location.href = requested_url;
                        } else sweetAlert({
                            title: e.title,
                            text: e.message,
                            type: e.type,
                            showCancelButton: !1,
                            closeOnConfirm: !0,
                            animation: "slide-from-top",
                            showConfirmButton: !0
                        }, function(t) {
                            if (t)
                                if (1 == e.key) {
                                    var a = window.location.href;
                                    location.href = requested_url;
                                } 
                        })
                },
                error:function(error){
                    console.log(error);
                }
            })
        }
    })
}

function resetForm()
{
    document.getElementById("form-login").reset();
    document.getElementById("timer").innerHTML = '';
    
}
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>js/validation.js" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>assets/js/check_login.js"></script>
</body>
</html>