<?php $this->load->view('site/templates/header_inner'); ?>
<div class="page-wrapper flex-full">
	<!-- Breadcurmbs starts -->
	<div class="breadcrumbs flex-full">
		<div class="container">
			<ul class="breadcrumbs-listing flex-full align-items-center">
				<li class="root-link"><a href="<?php echo base_url(); ?>">Home</a></li>				
				<li class="current-link"><a href="javascript:void(0)">Wishlist</a></li>
			</ul>
		</div>
	</div>
	<!-- Breadcurmbs ends -->
	<!-- Inner page heading starts -->
	<section class="innerpage-heading flex-full justify-content-center">
		<div class="container">
			<div class="innerpage-heading-wrapper flex-full justify-content-center bg-img position-relative" style="background-image: url(images/innerpage-banner.jpg);">
				<div class="innerpage-description flex-full justify-content-center text-center">
					<h1>Wishlist</h1>
				</div>
			</div>
		</div>
	</section>
	<!-- Inner page heading ends -->

	<div class="wishlist-main search-result-main flex-full align-items-start align-content-start">
		<div class="container">
			<div class="search-result-wrapper flex-full align-items-start align-content-start">
			    <?php if($latest_offer->num_rows() > 0){ ?>
				<aside class="filter-area flex-full">
					<div class="advertise-block flex-full justify-content-center text-center">
						<h4><span>Order now and get</span><strong><?php echo $latest_offer->row()->price_text; ?> </strong><small>(Maximum Rs <?php echo $latest_offer->row()->max_discount; ?>/month)</small></h4>
						<div class="coupon-code-block flex-full justify-content-center text-center">
							<span>Coupon Code: </span>
							<div class="coupon-code"><?php echo $latest_offer->row()->coupon_code; ?></div>
						</div>
					</div>
				</aside>
				<?php } ?>
				<div class="product-area flex-full">
					<h2>Wishlist <span id="wishilist_count">(<?php echo $my_wished_products->num_rows(); ?> Items )</span></h2>
					<ul class="product-listing flex-full">
						<?php 
                        foreach ($my_wished_products->result() as $productListVal) {
                        $img = 'dummyProductImage.jpg';    
                        $Subproduct = 0;
                        if($productListVal->is_package && $productListVal->subproducts != ''){
                            $Subproduct = count(explode(',', $productListVal->subproducts)); 
                        }
                        $imgArr = explode(',', $productListVal->image);
                        if (count($imgArr) > 0) {
                            foreach ($imgArr as $imgRow) {
                                if ($imgRow != '') {
                                    $img = $pimg = $imgRow;
                                    break;
                                }
                            }
                        } ?>
						
						<li id="product_listing_<?php echo $productListVal->id; ?>">
							<div class="product-single flex-full position-relative">
								<div class="product-image flex-full position-relative">
									<a href="javascript:void(0)" class="flex-full position-relative h-100">
										<amp-img src="<?php echo CDN_URL; ?>images/product/Copressed Images/<?php echo $img; ?>" alt="Product Image" layout="responsive" width="300" height="225"></amp-img>
									    <?php if($productListVal->pq_quantity < 1){ ?>
                                            <span class="ribbon ribbon-top-right">Out of Stock</span>
                                        <?php }?>
                                        <?php if($productListVal->product_label != ''){ ?>
                                        <span class="<?php echo $productListVal->product_label ?>-label"><?php $label = str_replace("_"," ",$productListVal->product_label); echo ucwords($label);  ?></span>
                                        <?php }?>
										<span class="close" onclick="removeWhishlist(<?php echo $productListVal->id; ?>)"><i class="icn icn-close-black"></i></span>
									</a>
								</div>
								<div class="product-description flex-full">
									<div class="product-description-wrapper flex-full">
										<h3><a href="javascript:void(0)"><?php echo $productListVal->product_name; ?></a></h3>
										<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> <?php echo number_format($productListVal->sale_price); ?></strong> / mon</p>
									    <?php if($productListVal->pq_quantity > 0){ ?>
										    <a href="<?php echo base_url(); ?>things/<?php echo $productListVal->id; ?>/<?php echo $productListVal->seourl; ?>" class="move-cart">Move to Cart</a>
									    <?php }else{?>
								    	    <a href="<?php echo base_url(); ?>things/<?php echo $productListVal->id; ?>/<?php echo $productListVal->seourl; ?>" class="move-cart">Notify Me</a>
								    	<?php } ?>
									</div>
								</div>
							</div>
						</li>
						<?php } ?>   
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<?php  $this->load->view('site/templates/footer'); ?>
</body>
</html>