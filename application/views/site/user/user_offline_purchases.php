<?php
$this->load->view('site/templates/header_inner');
?>	
	<div class="page-wrapper flex-full">
		<div class="my-account-section flex-full">
			<div class="container">
				<div class="my-account-wrapper flex-full">
				    <?php $this->load->view('site/user/settings_sidebar');?>
					<div class="account-content-area flex-full align-items-start align-content-start">
						<h2 class="border-left">My Orders</h2>
						<ul class="my-orders-listing flex-full">
						 <?php foreach ($purchasesList->result() as $row){ 
						    ?>
						    <li>
								<div class="my-orders-box flex-full">
									<div class="order-details-wrapper flex-full">
										<ul class="product-desc-listing flex-full">
											<li>
												<h4>Order Number</h4>
												<span><?php echo $row->dealCodeNumber;?></span>
											</li>
											<li>
												<h4>Amountr</h4>
												<span><?php echo $row->total;?></span>
											</li>
											<li>
												<h4>Order Date</h4>
												<span><?php echo $row->created;?></span>
											</li>
											<li>
												<h4>Order Owner</h4>
												<span><?php echo $row->email;?></span>
											</li>
											<li>
												<h4>Options</h4>
												<a style="color:green;" target="_blank" href="view-purchase-offline/<?php echo $row->user_id;?>/<?php echo $row->dealCodeNumber;?>"><?php echo "View Order Details"; ?></a><br/>
											</li>											
										</ul>
									</div>
								</div>
							</li>
						<?php }?>
							</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
						    
						   
<?php $this->load->view('site/templates/footer'); ?>
</div>
</html>