<?php $this->load->view('site/templates/header_inner'); ?>
	<div class="page-wrapper flex-full">
		<div class="my-account-section flex-full">
			<div class="container">
				<div class="my-account-wrapper flex-full">
                    <?php $this->load->view('site/user/settings_sidebar');?>
					<div class="account-content-area flex-full align-items-start align-content-start">
						<h2 class="border-left">My Payments</h2>						
						<div class="my-payments-wrapper flex-full">
							<div class="pay-block flex-full justify-content-end">
								<span>Total Outstanding : <strong>&#x20B9; 0.00</strong></span>
								<a href="javascript:void(0)" class="explore-btn">Pay Now</a>
							</div>
							<div class="table-wrapper">
								<table>
									<tr>
										<th>Invoice Number</th>
										<th>Invoice Date</th>
										<th>Amount Paid</th>
										<th>Payment Date</th>
										<th>Payment Type</th>
									</tr>
									<?php foreach($payments as $key => $row){ ?>
									<tr>
										<td><span>  	<?php echo $row['invoice_numbers'];?></span></td>
										<td><span>  <?php  echo date("Y-m-d", strtotime($row['date'])); ?></span></td>
										<td><span>&#x20B9;  <?php echo $row['amount'];?></span></td>
										<td><span>2019-10-12 19:00:27</span></td>
										<td><span>      <?php echo $row['payment_type'];?></span></td>
									</tr>
									<?php } ?>
								
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
<?php $this->load->view('site/templates/footer'); ?>
</html>