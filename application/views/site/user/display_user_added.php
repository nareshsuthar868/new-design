<?php
$this->load->view('site/templates/header_inner');
?>
<div class="page_section_offset lightgryabg pageheight">
	<section class="innerbanner">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1>User Added Products</h1>
                    <ul class="breadcrumb">
                    	<li><a href="#">Home</a></li>
                    	<li class="active">Added Products</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
	<div class="container">
		<div class="row">
			<div class="profilecolumn">
                	<?php $this->load->view('site/user/settings_sidebar'); ?>
					<main class="col-lg-9 col-md-9 col-sm-9">
						<div class="profilerightedit">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php $userImg = 'user-thumb1.png';
								if ($userProfileDetails->row()->thumbnail != ''){
								    $userImg = $userProfileDetails->row()->thumbnail;
								} ?>

								<!-- <p class="fw_light m_bottom_14 p_top_4">
									<?php if ($userProfileDetails->row()->about != '') {
										echo $userProfileDetails->row()->about;
									} else {
										echo $userProfileDetails->row()->brand_description;
									}?>
										
								</p> -->
								<div class="col-lg-12">
									<h2 class="fw_light second_font color_dark tt_uppercase m_bottom_27">My Products</h2>
								</div>
								<?php if ($addedProductDetails->num_rows()>0 || $notSellProducts->num_rows()>0){?>
								<?php
								foreach ($addedProductDetails->result() as $productLikeDetailsRow){
								$imgName = 'dummyProductImage.jpg';
								$imgArr = explode(',', $productLikeDetailsRow->image);
								if (count($imgArr)>0){
									foreach ($imgArr as $imgRow){
										if ($imgRow != ''){
											$imgName = $imgRow;
											break;
										}
									}
								}
								?>
								<!--isotope item-->
								<div class="col-sm-6 col-md-4 col-lg-4">
									<figure class="useraddproduct">
										<div class="relative">
										<a  href="<?php echo 'things/'.$productLikeDetailsRow->id.'/'.url_title($productLikeDetailsRow->product_name);?>" class="productcatimg">
												<img src="https://d1eohs8f9n2nha.cloudfront.net/images/product/<?php echo $imgName;?>" alt="" class="c_image_1 tr_all">
										</a>
										</div>
										<figcaption class="bg_white relative p_bottom_0">
												<div class="addedname m_bottom_9">
													<a class="second_font sc_hover d_xs_block" href="<?php echo 'things/'.$productLikeDetailsRow->id.'/'.url_title($productLikeDetailsRow->product_name);?>"><?php echo $productLikeDetailsRow->product_name;?></a>
													
												</div>
												<div class="addedprice m_bottom_9">
													<b class="scheme_color d_block">Rs <?php echo $productLikeDetailsRow->sale_price;?></b>
												</div>
										</figcaption>
									</figure>
								</div>
							<?php }?>
                             </div>
                            
						    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							 	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							   		<hr class="margin-top-15">
						   			<h2>Yet Not Sell</h2>
						    	</div>
							  	<?php 
							  	foreach ($notSellProducts->result() as $productLikeDetailsRow){
									$imgName = 'dummyProductImage.jpg';
									$imgArr = explode(',', $productLikeDetailsRow->image);
									if (count($imgArr)>0){
										foreach ($imgArr as $imgRow){
											if ($imgRow != ''){
												$imgName = $imgRow;
												break;
											}
										}
									}
									$fancyClass = 'fancy';
									$fancyText = LIKE_BUTTON;
									if (count($likedProducts)>0 && $likedProducts->num_rows()>0){
										foreach ($likedProducts->result() as $likeProRow){
											if ($likeProRow->product_id == $productLikeDetailsRow->seller_product_id){
												$fancyClass = 'fancyd';$fancyText = LIKED_BUTTON;break;
											}
										}
									} ?>
								<!--isotope item-->
								<div class="col-sm-6 col-md-4 col-lg-4">
									<figure class="useraddproduct">
										<!--image & buttons & label-->
										<div class="relative">
											<a href="<?php echo 'user/'.$userProfileDetails->row()->user_name.'/things/'.$productLikeDetailsRow->seller_product_id.'/'.url_title($productLikeDetailsRow->product_name);?>" class="productcatimg">
											
												<img src="https://d1eohs8f9n2nha.cloudfront.net/images/product/<?php echo $imgName;?>" alt=""  class="c_image_1 tr_all">			
											</a>
										</div>
										<figcaption class="bg_white relative p_bottom_0">
											
												<div class="addedname m_bottom_9">
													<a class="second_font sc_hover d_xs_block" href="<?php echo 'user/'.$userProfileDetails->row()->user_name.'/things/'.$productLikeDetailsRow->seller_product_id.'/'.url_title($productLikeDetailsRow->product_name);?>"><?php echo $productLikeDetailsRow->product_name;?></a>
													
												</div>
												<div class="addedprice m_bottom_9">
													<b class="scheme_color d_block">Rs <?php echo $userProfileDetails->sale_price;?></b>
												</div>
											
										</figcaption>
									</figure>
								</div>
														
							<?php }}?>
						</div>
					    </div>
				</main>
			</div>
		</div>
	</div>
</div>

			<!--footer-->
				<?php
					$this->load->view('site/templates/footer');
				?>

		<!--back to top-->
		<button class="back_to_top animated button_type_6 grey state_2 d_block black_hover f_left vc_child tr_all"><i class="fa fa-angle-up d_inline_m"></i></button>

<!-- Section_start -->
<script type="text/javascript" src="js/site/profile_things.js"></script>
<script>
        $.infiniteshow({
            itemSelector:'#content ol.stream > li',
            streamSelector:'#content ol.stream',
            dataKey:'home-new',
            post_callback: function($items){ $('ol.stream').trigger('itemloaded') },
            prefetch:true,
            
            newtimeline:true
        })
        if($.browser.msie) $.infiniteshow.option('prepare',1000);
</script>
		
		<!--libs include-->
		<script src="plugins/jquery.appear.js"></script>
		<script src="plugins/isotope.pkgd.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>
		<script src="js/retina.min.js"></script>

		<!--theme initializer-->
		<script src="js/themeCore.js"></script>
		<script src="js/theme.js"></script>
	</body>
</html>