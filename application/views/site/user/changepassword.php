<?php
$this->load->view('site/templates/header_inner');
?>		<!--main content-->
			<div class="page_section_offset lightgryabg pageheight">
				<section class="innerbanner">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-12">
                      <h1>Change Password</h1>
                      <ul class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <!-- <li><a href="#">Account</a></li> -->
                        <li class="active">Change Password</li>
                      </ul>
                    </div>
                  </div>
                </div>
                </section>
                <div class="container">
					<div class="row">
					  <div class="profilecolumn">
                    	<?php 
						$this->load->view('site/user/settings_sidebar');
						?>
						<main class="col-lg-9 col-md-9 col-sm-9">
							<div class="profilerightedit">
									<div class="col-sm-offset-2 col-md-offset-2 col-lg-8 col-md-8 col-sm-8">
										<form  method="post" action="<?php echo base_url().'site/user_settings/change_user_password'?>" id="change_pass">
											<div class="txtprofile">
                                            	<label for="password">New Password</label>
                                                <input  type="password" name="pass" id="pass" class="form-control" placeholder="* * * * * *">
                                                <input class="next_url" type="hidden" name="next" value="<?php echo $next;?>"/>
                                            </div>
                                            <div class="txtprofile">
                                            	<label for="password">Confirm Password</label>
                                                <input  type="password" name="confirmpass" id="confirmpass" placeholder="* * * * * *"  class="form-control">
                                                <input class="next_url" type="hidden" name="next" value="<?php echo $next;?>"/>
												<?php if (validation_errors() != ''){?>
                                                    <div id="validationErr">
                                                        <script>setTimeout("hideErrDiv('validationErr')", 2000);</script>
                                                        <span class="d_inline_m second_font fs_medium color_red d_md_block"><?php echo validation_errors();?></span>
                                                    </div>
                                                    <?php }?>
                                                    <?php if($flash_data != '') { ?>
                                                    <div class="errorContainer" id="<?php echo $flash_data_type;?>">
                                                        <script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 2000);</script>
                                                        <span class="d_inline_m second_font fs_medium color_red d_md_block"><?php echo $flash_data;?></span>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 editsubmit">
                                            <button class="btn-submit"  id="save_password" onclick= "change_user_password();"><?php if($this->lang->line('change_password') != '') { echo stripslashes($this->lang->line('change_password')); } else echo "Change Password"; ?></button>
                                            </div>
                                       </form>
										</div>
							</div>
						</main>
					</div>
				</div>
			</div>
		<!--footer-->
				<?php
					$this->load->view('site/templates/footer');
				?>
				</div>

		<!--libs include-->
		<script src="plugins/jquery-ui.min.js"></script>
		<script src="plugins/isotope.pkgd.min.js"></script>
		<script src="plugins/jquery.appear.js"></script>
		<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
		<script src="plugins/twitter/jquery.tweet.min.js"></script><script src="plugins/flickr.js"></script>
		<script src="plugins/afterresize.min.js"></script>
		<script src="plugins/jackbox/js/jackbox-packed.min.js"></script>
		<script src="plugins/jquery.elevateZoom-3.0.8.min.js"></script>
		<script src="plugins/fancybox/jquery.fancybox.pack.js"></script>
		<script src="js/retina.min.js"></script>
		<script src="plugins/colorpicker/colorpicker.js"></script>

		<!--theme initializer-->
		<script src="js/themeCore.js"></script>
		<script src="js/theme.js"></script>
	</body>
</html>