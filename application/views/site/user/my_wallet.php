<?php $this->load->view('site/templates/header_inner'); ?>
<?php
$amount = 0;
if($wallets->num_rows() > 0){ 
    $amount =  $wallets->row()->topup_amount; 
} ?>
	<div class="page-wrapper flex-full">
		<div class="my-account-section flex-full">
			<div class="container">
				<div class="my-account-wrapper flex-full">
				    <?php 	$this->load->view('site/user/settings_sidebar');?>
					<div class="account-content-area flex-full align-items-start align-content-start">
						<h2 class="border-left">Cityfurnish Coins</h2>						
						<div class="cf-coins-wrapper flex-full">
							<div class="cf-coins-block flex-full align-items-start align-content-start">
								<div class="coin-balance-wrapper flex-full">
									<h3>Your Coin Balance :</h3>
									<div class="coin-balance flex-full">
										<span class="balace-amt"><i class="icn icn-coins-gold"></i>
										<?php echo $amount; ?>
										</span>
										<a class="add-coin explore-btn">Add Coins</a>
									</div>
									<form class="credit-form flex-full d-none" id="wallet_popup_form" action="site/user/topupmywallet" method="post" >
										<div class="FlowupLabels">
											<div class="fl_wrap">
												<label class="fl_label" for="topup_amount">Enter Amount</label>
												<!--&#x20B9;-->
												<input class="fl_input" type="text" id='topup_amount' name="topup_amount" value="" />
												<input type="submit" class="btn text-link"  onclick="SubmitPopupForm()" style="z-index: 99999;" value="Add">
											</div>													
										</div>
									</form>
									<div class="coins-info flex-full">
										<p>Your total Coins are worth <strong>&#x20B9; <?php echo $amount ?></strong><br/> Coins can be redeemed while making invoice payment or while placing order</p>
									</div>
								</div>
								<div class="coins-benefits-wrapper flex-full">
									<div class="coins-benefits-box flex-full justify-content-center text-center">
										<i class="icn icn-instant-checkout"></i>
										<h4>Instant Checkout</h4>
										<p>One-click, easy and fast checkout</p>
									</div>
									<div class="coins-benefits-box flex-full justify-content-center text-center">
										<i class="icn icn-more-benefits"></i>
										<h4>Much More Benifits</h4>
										<p>Benefits and offers on using CF Coins</p>
									</div>
								</div>
							</div>
							<div class="transations-wrapper flex-full">
								<h3>My Transactions (Debit)</h3>
								<div class="table-wrapper">
									<table>
										<tr>
											<th>Invoice Number</th>
											<th>Invoice Date</th>
											<th>Coins Used</th>
											<th>Transaction Date</th>
										</tr>
										<?php if($wallets_transaction->num_rows() > 0){
										    foreach($wallets_transaction->result() as $key => $value){
										     ?>
										<tr>
										    
											<td><span><?php echo $value->order_id; ?></span></td>
											<td><span><?php echo $value->created_at; ?></span></td>
											<td><span>- &#x20B9; <?php echo $value->amount; ?></span></td>
											<td><span><?php echo $value->created_at; ?></span></td>
										</tr>
										<?php } } ?>
									</table>
								</div>
							</div>
							
							<div class="transations-wrapper flex-full">
								<h3>My Transactions (Credit)</h3>
								<div class="table-wrapper">
									<table>
										<tr>
											<th>Transaction ID</th>
											<th>Credit Mode</th>
											<th>Coins Added</th>
											<th>Transaction Date</th>
										</tr>
										<?php if($wallets_transaction_credit->num_rows() > 0){
										    foreach($wallets_transaction_credit->result() as $key => $value){ ?>
										<tr>
										    
											<td><span><?php echo $value->txnid; ?></span></td>
											<td><span><?php if($value->payment_mode == 'CC'){ echo  'CC (Credit Card)'; }elseif($value->payment_mode == 'DC'){ echo 'DC (Debit Card)'; }else { echo $value->payment_mode ;} ?></span></td>
											<td><span>+ &#x20B9; <?php echo $value->amount; ?></span></td>
											<td><span><?php echo $value->created_at; ?></span></td>
										</tr>
										<?php } } ?>
									</table>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
<?php $this->load->view('site/templates/footer'); ?>
</html>