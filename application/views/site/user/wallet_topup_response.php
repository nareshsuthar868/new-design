<?php 
$this->load->view('site/templates/header_inner'); ?>
<!--main content-->

<?php 
  if($TopupConfirmation == 'Success'){ ?> 
    <div class="page-wrapper flex-full">
		<div class="thankyou-section flex-full">
			<div class="container">
				<div class="thankyou-wrapper flex-full justify-content-center text-center">
					<figure>
						<img src="images/cheers-vector.png" alt="cheers-vector">
					</figure>
					<h1 class="w-100 color-yellow">Cheers!!</h1>
					<h2 class="w-100">Thank you for your order, Money added to your wallet.</h2>
					<span class="message w-100">You will receive an email confirmation shortly at your registered email ID.</span>
				</div>
			</div>
		</div>
	</div>
    <?php 
     $this->output->set_header('refresh:5;url='.base_url().'wallet'); 
    }elseif($TopupConfirmation =='Failure'){ ?> 
        <div class="page_section_offset mobileheight"> 
        <!-- New  html -->
            <section class="thankyoupagerow">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12">
                            <img src="<?php echo CDN_URL; ?>images/cancel-order-icn.svg" />
                            <h1>Oops! Your transaction failed</h1>
                            <p>Please try again or call us on 8010845000, so that we can assist you..</p>
                        </div>
                    </div>
                </div>
            </section>
        <!-- New  html --> 
        </div>
    <?php 
    $this->output->set_header('refresh:5;url='.base_url().'wallet'); 
    } ?>

<!--footer-->
<?php $this->load->view('site/templates/footer'); ?>
</body>
</html>