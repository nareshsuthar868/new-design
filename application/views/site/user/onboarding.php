<?php
$this->load->view('site/templates/header_inner');
?>

			<!--main content-->
			<div class="page_section_offset" style="margin-bottom:150px;">
				<div class="container">
					<div class="row" style="margin-top:50px;">
						<aside class="col-lg-4 col-md-4 col-sm-4 p_top_4">
						</aside>
						<section class="col-lg-4 col-md-4 col-sm-4">
						<ul class="m_bottom_14">
							<li class="m_bottom_15">
								<h3 class="fw_light second_font color_dark m_bottom_14 tt_uppercase t_align_c m_bottom_10">Welcome <?php echo $userDetails->row()->full_name;?> </h3>
							</li>
							<li class="m_bottom_15">
								<h6 class="fw_light second_font color_dark m_bottom_14 t_align_c m_bottom_20"><?php if($this->lang->line('onboarding_get_started') != '') { echo stripslashes($this->lang->line('onboarding_get_started')); } else echo "Get started with "; ?> <?php echo $siteTitle;?>..</h4>
							</li>
							<!--<li class="m_bottom_15 t_align_c">
								<img src="images/site/onboarding1.png">
							</li>-->
							<li style="margin-top: 60px;margin-bottom:200px;">
								<button  onclick="window.location='<?php echo $next;?>'" class="btn-signin t_align_c tt_uppercase w_full second_font fs_medium button_type_2 lbrown tr_all" 
								style="border-style:solid;padding-top: 8px;padding-bottom: 8px;" >Get Started</button>
							</li>
						</section>
						<aside class="col-lg-4 col-md-4 col-sm-4 p_top_4">
						</aside>
					</div>
				</div>
			</div>
			<!--footer-->
				<?php
					$this->load->view('site/templates/footer');
				?>
		</div>

		<!--back to top-->
		<button class="back_to_top animated button_type_6 grey state_2 d_block black_hover f_left vc_child tr_all"><i class="fa fa-angle-up d_inline_m"></i></button>
		<!--libs include-->
		<script src="plugins/jquery.appear.js"></script>
		<script src="plugins/afterresize.min.js"></script>
		<!--theme initializer-->
		<script src="js/themeCore.js"></script>
		<script src="js/theme.js"></script>
	</body>
</html>