<!DOCTYPE html>
<html amp lang="en" style="height: 100% !important;">
<head>
	<title>Sign In page</title>
	<meta charset="utf-8">
	<link rel="canonical" href="product-listing.html">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
	<!-- CSS Files -->
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>css/bootstrap.min.css" defer/>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/jquery.FlowupLabels.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/new_slick.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style-custom.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/responsive.css">
	
	<script src="<?php echo base_url(); ?>assets/bootstrap-sweetalert/dist/sweetalert.js" async defer></script>
    <link href="<?php echo base_url(); ?>assets/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css" defer/>
    <script src="<?php echo base_url(); ?>assets/bootstrap-sweetalert/dist/swalExtend.js" async defer></script>
    <link href="<?php echo base_url(); ?>assets/bootstrap-sweetalert/dist/swalExtend.css" rel="stylesheet" type="text/css" defer/>
	
	<!-- JS Files -->
    <script async src="<?php echo base_url(); ?>js/amp_v0.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap.js" ></script>
    <script src="<?php echo base_url(); ?>js/jquery.mCustomScrollbar.min.js"></script>
    <script src="<?php echo base_url(); ?>js/new_slick.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.FlowupLabels.js"></script>
    
</head>
<body style="height: 100% !important;" class="signin">
	<div class="signin-wrapper flex-full align-items-start align-content-start justify-content-end">
		<div class="signin-slider-wrapper flex-full">
			<div class="signin-slider w-100">
				<div class="slide">
					<img src="<?php echo base_url(); ?>images/innerpage-banner.jpg" alt="product">
				</div>
				<div class="slide">
					<img src="<?php echo base_url(); ?>images/innerpage-banner.jpg" alt="product">
				</div>
				<div class="slide">
					<img src="<?php echo base_url(); ?>images/innerpage-banner.jpg" alt="product">
				</div>
			</div>
		</div>
		<div class="signin-form-wrapper flex-full align-items-start align-content-start">
			<div class="signin-tab-wrapper flex-full content align-items-start align-content-start">
				<div class="signin-tab-inner-wrapper flex-full align-items-start align-content-start">
					<div class="signin-logo-part flex-full">
						<img src="<?php echo CDN_URL; ?>images/brand-logo.png" alt="logo">
					</div>
					<div class="tab-links-part flex-full">
						<ul class="tabs flex-full">
							<li class="tab-link current" data-tab="tab-1"><span>Sign In</span></li>
							<li class="tab-link" data-tab="tab-2"><span>Sign Up</span></li>
						</ul>
					</div>
					<div class="tab-content-part flex-full">
						<div id="tab-1" class="tab-content current">
						    <form method="post" name="password_change" id="password_change" class="credit-form signin-form flex-full">
								<ul class="social-icons-listing flex-full">
									<li><a href="javascript:void(0)" class="linkedin"><i class="icn icn-linkedin-white"></i></a></li>
									<li><a href="javascript:void(0)" class="google"><i class="icn icn-google-white"></i></a></li>
									<li><a href="javascript:void(0)" class="facebook"><i class="icn icn-facebook-white"></i></a></li>
								</ul>
								<span class="or w-100">OR</span>
								<div class="signin-form-block flex-full FlowupLabels">
                                    <div class="alert alert-success" role="alert" style="display:none;" id="reset_password">
                                        <h4>Your Password Changed</h4>
                                                    <p>Your password has been changed Successfully.</p>
                                    </div>
                                    <div class="alert alert-danger" role="alert" style="display:none;" id="email_error">
                                        <p>Your email id not matched in our records.</p>
                                    </div>

									<!-- Sign In OTP starts -->
									<!-- <span class="otp-message"><strong>One time password</strong> has been sent to your mobile, please enter the same to verify your mobile number</span>
									<div class="fl_wrap form-grp flex-full">
										<label class='fl_label' for='mobile_number'>Enter Mobile Number</label>
										<input class='fl_input' type='text' id='mobile_number' value="93287 37865" />										
									</div>
									<div class="fl_wrap otp-sent-fieldform-grp flex-full">
										<label class='fl_label' for='otp'>Enter OTP</label>
										<input class='fl_input' type='text' id='otp'/>
										<span class="otp-timer">01:34</span>
										<a class="get-otp text-link" href="javascript:void(0)">Resend <i class="icn-arrow-right"></i></a>
									</div> -->									
									<!-- Sign In OTP ends -->

									

									<!-- Sign In reset password starts -->
									<div class="fl_wrap password-field form-grp flex-full">
										<label class='fl_label' for='chngpass'>Reset Password</label>
										<input class='fl_input' type='password' id='chngpass' name="chngpass" data-error="#chngpass-error" />
										<a class="password-show" href="javascript:void(0)"><i class="icn"></i></a>
									</div>
									<div class="flex-full">
            							<span  class="error" id="chngpass-error"></span>
            						</div>
									<div class="fl_wrap password-field form-grp flex-full">
										<label class='fl_label' for='cchngpass'>Confirm Password</label>
										<input class='fl_input' type='password' id='cchngpass' name="cchngpass" data-error="#cchngpass-error" />
										<a class="password-show" href="javascript:void(0)"><i class="icn"></i></a>
									</div>
									<div class="flex-full">
            							<span  class="error" id="cchngpass-error"></span>
            						</div>
            						<input type="hidden" name="user_id" id="user_id" value="<?php if(isset($id)){ echo $id[0]->id;} ?>">
									<input type="hidden" value="<?php echo base_url();?>" id="base_url">
                                            
    							    <div class="submit-btn flex-full">
        								<button id="save_password" name="save_password"
                                                        onclick="return check_value();" class="explore-btn">Sign In</button>
                                    </div>
									<!-- Sign In reset password ends -->

									<!-- Sign In OTP starts -->
									<!-- <span class="otp-message"><strong>One time password</strong> has been sent to your mobile, please enter the same to verify your mobile number</span>
									<div class="fl_wrap otp-field form-grp flex-full">
										<label class='fl_label' for='get_otp'>Enter Email / Mobile Number</label>
										<input class='fl_input' type='text' id='get_otp' value="93287 37865"/>
										<a class="get-otp text-link" href="javascript:void(0)">Get OTP <i class="icn-arrow-right"></i></a>
									</div>
									<div class="fl_wrap password-field form-grp flex-full">
										<label class='fl_label' for='password4'>Enter Password</label>
										<input class='fl_input' type='password' id='password4'/>
									</div> -->
									<!-- Sign In OTP ends -->
                                    
								</div>
							</form>	
						</div>
						
						<div id="tab-2" class="tab-content">
							<form class="credit-form signin-form flex-full" name="sign-up" id="sign-up">
							    <div class="alert alert-danger" role="alert" id="error_msg" style="display: none;">
                                    
                                </div>
							    <!--<span id="error_msg" style="color: red;" style="display: none; height: 13px;width: 320px; "></span>-->
							    <div class="alert alert-success" role="alert" style="display:none;" id="success_registraion">
                                    <h4>Register Success</h4>
                                    <p>Your account has been created. We have sent you account activation link on your registered email address.</p>
                                </div>
							    <!--<div class="success-block popup-block display_none" id="success_registraion">-->
           <!--                         <div class="inner-block text-center">-->
           <!--                             <i class="material-icons">&#xE876;</i>-->
           <!--                             <h4>Register Success</h4>-->
           <!--                             <p>Your account has been created. We have sent you account activation link on your registered email address.</p>-->
           <!--                         </div>-->
           <!--                     </div>  -->
								<ul class="social-icons-listing flex-full">
									<li><a href="javascript:void(0)" class="linkedin"><i class="icn icn-linkedin-white"></i></a></li>
									<li><a href="javascript:void(0)" class="google"><i class="icn icn-google-white"></i></a></li>
									<li><a href="javascript:void(0)" class="facebook"><i class="icn icn-facebook-white"></i></a></li>
								</ul>
								<span class="or w-100">OR</span>
								<div class="FlowupLabels signin-form-block flex-full">
									<div class="fl_wrap form-grp flex-full">
										<label class='fl_label' for='user_name'>Enter Name</label>
										<input class='fl_input filled fullname' type='text' id='user_name' name="full_name" data-error="#errNm1"/>
									</div>
									<div class="flex-full">
										<span class="error" id="errNm1"></span>
									</div>
									<div class="form-grp flex-full">
										<div class="fl_wrap form-grp-inner flex-full">
											<label class='fl_label' for='phone_no'>Mobile Number</label>
											<input class='fl_input' type='text' id='phone_no' onkeyup="Verify_button()" name="mobile_number" data-error="#errNm3"/>											
											<span class="length">0 / 10</span>
										</div>
										<div class="fl_wrap form-grp-inner flex-full">
											<label class='fl_label' for='referral_code_signup'>Referral Code (optional)</label>
											<input class='fl_input' type='text' id='referral_code_signup' />
										</div>
									</div>
									<div class="flex-full">
    									<span class="error" id="errNm3"></span>
    								</div>
									<div class="fl_wrap form-grp flex-full">
										<label class='fl_label' for='signup_email'>Email Address</label>
										<input class='fl_input email' type='email' id='signup_email' name="signup_email" data-error="#errNm2"/>
									</div>
									<div class="flex-full">
										<span class="error" id="errNm2"></span>
									</div>
									<div class="fl_wrap password-field form-grp flex-full">
										<label class='fl_label' for='signup_password'>Enter Password</label>
										<input class='fl_input password' type='password' id='signup_password' name="signup_password"  data-error="#errNm4"/>
										<a class="password-show" href="javascript:void(0)"><i class="icn"></i></a>
									</div>
									<div class="flex-full">
										<span class="error" id="errNm4"></span>
									</div>
									<input type='hidden' name='api_id' id="api_id"  value='<?php echo $social_login_session_array['social_login_unique_id'];?>' />
                                    <input type='hidden' name='thumbnail' id='thumbnail' value='<?php echo $social_login_session_array['social_image_name'];?>' />
                                    <input type='hidden' name='loginUserType' id='loginUserType' value='<?php if($social_login_session_array['loginUserType'] != '') echo $social_login_session_array['loginUserType']; else echo "normal";?>' />
									<div class="submit-btn flex-full">
										<button id="signupsubmit" name="signupsubmit"  onclick="return check_validation()" class="explore-btn">Sign Up</button>
									</div>
									<!-- <div class="form-grp flex-full">
										<input type="text" name="" class="input-grp filled" value="Pooja Karkhanis" placeholder="Enter Name">
									</div>
									<div class="form-grp flex-full">
										<div class="form-grp-inner flex-full">
											<input type="text" name="" class="input-grp" placeholder="Mobile Number">
											<span class="length">0 / 10</span>
										</div>
										<div class="form-grp-inner flex-full">
											<input type="text" name="" class="input-grp" placeholder="Referral Code (optional)">
										</div>
									</div>
									<div class="form-grp flex-full">
										<input type="email" name="" class="input-grp" placeholder="Email Address">
									</div>
									<div class="password-field form-grp flex-full">
										<input type="password" name="" class="input-grp" placeholder="Enter Password">
										<a class="password-show" href="javascript:void(0)"><i class="icn"></i></a>
									</div>
									<div class="submit-btn flex-full">
										<input type="submit" name="" value="Sign Up" class="explore-btn">
									</div> -->	
								</div>
							</form>
						</div>					
					</div>
				</div>
			</div>
		</div>
	</div>


<script type="text/javascript">
    var baseURL = 'http://45.113.122.221/';
	var BaseURL = 'http://45.113.122.221/';
	$(document).ready(function(){
	    $("#forgot-password-btn").click(function(){
	        $("#login-module").toggle("slow", function(){
	            if($("#login-module").is(':hidden')){
	                $("#forgot-pass").fadeIn();
	            }else{
	                $("#forgot-pass").fadeOut();
	            }
	        });
	    });
	    
	    $("#backto-signin-btn").click(function(){
	        $("#forgot-pass").toggle("slow", function(){
	            if($("#forgot-pass").is(':hidden')){
	                $("#login-module").fadeIn();
	            }else{
	                $("#login-module").fadeOut();
	            }
	        });
	    });
	    
		$('.signin-slider').slick({
			dots: true,
			arrows: false,
			slidesToShow: 1,
			infinite: true,
			autoplay: true,
			responsive: [		    
				{
				  breakpoint: 1025,
				  settings: {		        
					dots: true,
				  }
				}
			]
		});

		$('ul.tabs li').click(function(){
			var tab_id = $(this).attr('data-tab');

			$('ul.tabs li').removeClass('current');
			$('.tab-content').removeClass('current');

			$(this).addClass('current');
			$("#"+tab_id).addClass('current');
		});

		$(window).on("load",function(){
			$(".content").mCustomScrollbar({theme:"minimal-dark"});
		});
		
		$(".password-show").on('click', function(){
		    if($(this).prev('input').attr('type') == 'password'){
		        $(this).prev('input').prop('type', 'text');
		    }else{
		        $(this).prev('input').prop('type', 'password');
		    }
		    
			$(this).children().toggleClass('icn-eye');
		});		

		$('.FlowupLabels').FlowupLabels({
			/*
			 * These are all the default values
			 * You may exclude any/all of these options
			 * if you won't be changing them
			 */
			
			// Handles the possibility of having input boxes prefilled on page load
			feature_onInitLoad: true, 
			
			// Class when focusing an input
			class_focused: 		'focused',
			// Class when an input has text entered
			class_populated: 	'populated'	
		});
	});
</script>
<script src="<?php echo base_url(); ?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url(); ?>js/validation.js" type="text/javascript"></script>

    <script src="<?php echo base_url(); ?>assets/js/check_login.js"></script>
</body>
</html>