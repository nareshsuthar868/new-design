<aside class="account-sidebar flex-full align-items-start align-content-start">
	<div class="account-sidebar-wrapper flex-full align-items-start align-content-start h-100">
		<h1>My Account</h1>
		<div class="account-wrapper flex-full">
			<div class="account-dividers flex-full">
				<h3>Orders</h3>
				<ul class="account-links flex-full">
					<li class="<?php if ($this->uri->segment(1)== 'purchases') { echo "active"; } ?>" ><i class="icn icn-my-orders"></i><a href="purchases">My Orders</a></li>
					<li class="<?php if ($this->uri->segment(1)== 'payments') { echo "active"; } ?>"><i class="icn icn-my-payments"></i><a href="payments">My Payments</a></li>
					<li class="<?php if ($this->uri->segment(1)== 'invoices') { echo "active"; } ?>"><i class="icn icn-my-invoices"></i><a href="invoices">My Invoices</a></li>
				</ul>
			</div>
			<div class="account-dividers flex-full">
				<h3>Benifits</h3>
				<ul class="account-links flex-full">
					<li class="<?php if ($this->uri->segment(1)== 'wallet') { echo "active"; } ?>" ><i class="icn icn-cf-coins"></i><a href="wallet">CF Coins</a></li>
					<li class="<?php if ($this->uri->segment(1)== 'referral') { echo "active"; } ?>" ><i class="icn icn-referral-code"></i><a href="referral">Referral Code</a></li>						
				</ul>
			</div>
			<div class="account-dividers flex-full">
				<h3>Account</h3>
				<ul class="account-links flex-full">
					<li class="<?php if ($this->uri->segment(1)== 'usersettings') { echo "active"; } ?>"><i class="icn icn-profile-settings"></i><a href="usersettings">Profile Settings</a></li>
					<li class="<?php if ($this->uri->segment(1)== 'documentation') { echo "active"; } ?>"><i class="icn icn-documentation"></i><a href="documentation">Documentation</a></li>
					<li class="<?php if ($this->uri->segment(2)== 'shipping') { echo "active"; } ?>"><i class="icn icn-shipping-address"></i><a href="settings/shipping">Shipping Address</a></li>
				</ul>
			</div>
			<div class="account-dividers flex-full">					
				<ul class="account-links flex-full">
					<li><i class="icn icn-logout"></i><a href="logout">Logout</a></li>
				</ul>
			</div>
		</div>
	</div>
</aside>
<script>
    $(window).load(function() {
    	var win_width = $(window).width();
    	if(win_width < 600){
    		var ele_width=$("#prfmenu li").find(".current").position().left;
    
    		$(".pn-ProductNav").scrollLeft((ele_width - 100));
    	}
    });
</script>