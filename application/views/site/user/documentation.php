<?php
$this->load->view('site/templates/header_inner');
?>	
	<div class="page-wrapper flex-full">
		<div class="my-account-section flex-full">
			<div class="container">
				<div class="my-account-wrapper flex-full">
				 <?php $this->load->view('site/user/settings_sidebar');?>
					<div class="account-content-area flex-full align-items-start align-content-start">
						<div class="account-accordion flex-full">
							<div class="accordion">
								<div class="accordion-tab current">									
									<h3 class="accordion-title"><i class="title-icn icn icn-verify-credit-score"></i>Verify Credit Score</h3>
									<div class="accordion-content">
									      <input type="hidden" id="encodecode" value="<?php echo $encode; ?>"> 
										<div class="accordion-body" id="creditScorequestions">
									    	<?php if(empty($crifData) && empty($scoreData)){ ?>
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s.</p>
											<form class="credit-form flex-full" id="creditScoreForm">
												<div class="FlowupLabels">
													<div class="fl_wrap">
														<label class="fl_label" for="adhar_number">Aadhar Number</label>
														<input class="fl_input" type="text" id="adhar_number"  name="adhar_number" />
													</div>
													<div class="fl_wrap">
														<label class="fl_label" for="pan_number">PAN Number</label>
														<input class="fl_input" type="text" id="pan_number"  name="pan-number" />
													</div>
													<div class="fl_wrap">
														<label class="fl_label focused" for="dob">Date of Birth</label>
														<input class="fl_input" type="date" id="dob" />
													</div>
												</div>
												<div class="verify-block flex-full justify-content-center">
													<span class="w-100 text-center">Please provide atleast one of the number above</span>
													<a href="javacript:void(0)" class="explore-btn verify " onclick="initiateCibil()">Verify</a>
												</div>
												<div class="not-found-block flex-full justify-content-center">
													<span class="w-100 text-center">Sorry we could not find your Credit Score. Please proceed to the next step.</span>
													<span class="w-100 text-center d-none">Your Credit Score is  <strong>750 / 1000</strong></span>
													<a href="javacript:void(0)" class="explore-btn proceed">Proceed</a>
												</div>
											</form>
												<?php }else if(empty($scoreData)){
						      				        // echo "<pre>";
        						      				echo $crifData;
        						      			} else {
        						      				echo $scoreData;
        						      			} ?>
										</div>
									</div>
								</div>
								<div class="accordion-tab">									
									<h3 class="accordion-title"><i class="title-icn icn icn-kyc-documents"></i>KYC Documents</h3>
								    <div class="accordion-content">
										<div class="accordion-body">
											<p>We require some documents before delivery. Please upload documents using below form.</p>
											<?php 
												$productattributes = array('class' => 'credit-form kyc-document flex-full', 'id' => 'cibil_form', 'enctype' => 'multipart/form-data','name' => 'cibil_doc_form',);
												echo form_open_multipart('site/user_settings/savecibildocs',$productattributes);
												?>
												<div class="FlowupLabels">
													<div class="fl_wrap">
													<label class="label">Select Order:</label>
													<select name="order_id" class="fl_input focused">
														<?php foreach ($get_my_orders->result() as $key => $value) {
															echo '<option value='.$value->dealCodeNumber.'>#'.$value->dealCodeNumber.'</option>';
														} ?>
													</select>
												</div>
												</div>
												<div class="FlowupLabels">
													<?php
													foreach ($cibil_docs->result() as $key => $value) { ?>
														<div class="fl_wrap input-file-grp">
															<span class="label d-flex w-100"><?php echo $value->doc_name; ?></span>
															<input type="file" class="doc_file <?php echo $value->doc_id; ?>" name="<?php echo $value->doc_id; ?>" id="<?php echo $value->doc_id; ?>" data-error="#doc_file<?php echo $value->doc_id; ?>">
															<label class="input-file" for="<?php echo $value->doc_id; ?>">
																<i class="icn icn-upload"></i>
																<span>Drag & Drop (or) <a href="javascript:void(0)">Choose File</a></span>
															</label>
															<small class="d-flex note w-100">( Max 10MB )</small>
															<span class="error" id="doc_file<?php echo $value->doc_id; ?>"></span>		
														</div>	
													<?php } ?>
													<div class="fl_wrap">
						                              <label class="fl_label" for="linkdin_profile_url">Linkedin Profile URL</label>
						                              <input class="fl_input" name="linkdin_profile_url" type="text" id="linkdin_profile_url"  data-error="#linkdin_profile">
						                            	<span class="error" id="linkdin_profile" style="display: flex;"></span>		
						                            </div>
						                            <div class="fl_wrap">
						                              <label class="fl_label" for="special_remarks">Special Remarks</label>
						                              <input class="fl_input" name="special_remarks" type="text" id="special_remarks" data-error="#special_rm">
						                            	<span class="error" id="special_rm" style="display: flex;"></span>		
						                            </div>
												</div>
												<div class="kyc-info flex-full">
													<label class="label">Note:</label>
													<ul class="kyc-info-listing flex-full">
														<li><span>Your Linkedin profile helps our credit rating team to complete KYC faster. In case you don't have Linkedin profile, please provide your Facebook profile link. Don't worry, we will never post anything on your social profiles.</span></li>
														<li><span>Based on your credit rating and order value, we might request you to provide some more documents.</span></li>
													</ul>
												</div>
												<div class="submit-btn flex-full justify-content-center">
													<input type="submit" name="" class="explore-btn proceed" value="Submit">
												</div>												
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

 <script type="text/javascript"> 
    $('.doc_file').on('change', function(e) { 
        const size =  (this.files[0].size / 1024 / 1024).toFixed(2); 
        if (size > 10 || size < 0.20) { 
            alert("File must be between the size of 1-10 MB"); 
          	var $el = $(this);
      		$el.wrap('<form>').closest('form').get(0).reset();
	      	$el.unwrap();
        }
    }); 
</script> 
<?php
$this->load->view('site/templates/footer');
?>	
</body>
</html>