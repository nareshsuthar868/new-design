<?php $this->load->view('site/templates/header_inner'); ?>
 <input type="hidden" value="<?php echo base_url()?>site/user_settings/insertEdit_shipping_address" id="shiping_address">

 <input type="hidden" id="ship_id" />
 <input type="hidden" value="<?php echo $loginCheck ?>" id="user_id">
	<div class="page-wrapper flex-full">
		<div class="my-account-section flex-full">
			<div class="container">
				<div class="my-account-wrapper flex-full">
				    <?php $this->load->view('site/user/settings_sidebar');?>
					<div class="account-content-area flex-full align-items-start align-content-start">
						<div class="shipping-address-wrapper cart-main-section flex-full">
							<div class="container">
								<h2 class="border-left">Shipping Address</h2>
								<div class="cart-main-wrapper flex-full align-items-start align-content-start">
									<div class="default-address-block address-selection-block flex-full">
									    <?php
									        if($shippingList->row() > 0){
									    ?>
									            <h3>Default Address</h3>
        										<ul class="address-listing flex-full">
        										    <?php
        										        foreach($shippingList->result() as $row){
        										            if($row->primary == 'Yes'){
        										    ?>
        										                <li id="<?php echo $row->id;?>" aid="<?php echo $row->id;?>">
                    												<div class="address-box flex-full">
                    													<a href="javascript:void(0)"  onclick="get_shiping(<?php echo $row->id;?>);" class="edit"><i class="icn icn-pen" ></i></a>
                    													<a href="javascript:void(0)"  onclick="delete_shipping_address_cart(<?php echo $row->id;?>,1);" class="close"><i class="icn icn-close-black"></i></a>
                    													<h3><?php echo $row->full_name;?></h3>
                    													<address><?php echo $row->address1.',<br> '.$row->address2.','.$row->city.'-'.$row->state.',<br>'.$row->postal_code;?></address>
                    													<!--<a href="javascript:void(0)" class="address-label">Set as Default</a>-->
                    												</div>
                    											</li>  
        										    <?php
        										            }
        										        }
        										    ?>
        										</ul>
									    <?php
									        }
									    ?>

									</div>
									<?php
									    if($shippingList->row() > 0){
									?>
									        <div class="other-address-block address-selection-block flex-full">
        										<h3>Other Addresses</h3>
        										<ul class="address-listing flex-full save-address">
        										    <?php
        										        foreach($shippingList->result() as $row){
        										            if($row->primary != 'Yes'){
        										    ?>
        										                <li id="<?php echo $row->id;?>" aid="<?php echo $row->id;?>">
                    												<div class="address-box flex-full">
                    													<a href="javascript:void(0)"  onclick="get_shiping(<?php echo $row->id;?>);" class="edit"><i class="icn icn-pen"></i></a>
                    													<a href="javascript:void(0)" class="close"  onclick="delete_shipping_address_cart(<?php echo $row->id;?>,0);"><i class="icn icn-close-black" ></i></a>
                    													<h3><?php echo $row->full_name ?></h3>
                    													<address><?php echo $row->address1.',<br> '.$row->address2.','.$row->city.'-'.$row->state.',<br>'.$row->postal_code;?></address>
                    													<a href="javascript:void(0)" class="address-label" onclick="setDefaultAddress(<?php echo $row->id;?>)">Set as Default</a>
                    												</div>
                    											</li>
        										    <?php
        										            }
        										        }
        										    ?>
        										</ul>
        									</div>
									<?php
									    }
									?>
									<div class="add-address-block flex-full">								
										<h2 class="border-left">Add New Address</h2>
										<form class="address-form credit-form flex-full" id="address_form" >
									    	<div class="FlowupLabels">
											    <div class="fl_wrap">
										            <label class="fl_label" for="full_name">Full Name *</label>
										            <input class="fl_input" type="text" id="full_name" name="full_name" />
    										    </div>
        										<div class="fl_wrap">
        											<label class="fl_label" for="phone">Mobile number *</label>
        											<input class="fl_input" type="text" id="phone" />
        										</div>
        										<div class="fl_wrap">
        											<label class="fl_label" for="address1">Address line 1 *</label>
        											<input class="fl_input" type="text" id="address1" />
        										</div>
        										<div class="fl_wrap">
        											<label class='fl_label' for='address2'>Address line 2 *</label>
        											<input class='fl_input' type='text' id='address2' />
        										</div>
												<div class="fl_wrap">
													<!-- <label class='fl_label' for='city'>City *</label> -->
													<select class='fl_input' id='city'  onchange="change_city()">
												    	<option value="default" selected disabled>City</option>
        										        <?php
        												for($a=0;$a<count($city_merge);$a++) {
                                                                $cityname = ($city_merge[$a]['list_value'] == 'Delhi NCR') ? 'Delhi' : $city_merge[$a]['list_value'];
                                                            echo '<option value="'.$cityname.'">'.$cityname.'</option>';
                                                        }   
        												?>
													</select>
												</div>
												<div class="fl_wrap">
													<label class='fl_label' for='state'>State *</label> 
										        	<input class='fl_input' type='text' id='state' readonly />
												</div>
												<div class="fl_wrap">
													<label class='fl_label' for='postal_code'>Postal code *</label>
													<input class='fl_input' type='text' id='postal_code' />
												</div>										
											</div>
											<div class="default-checkbox checkbox-grp flex-full justify-content-center">
												<input type="checkbox" name="make_default" value="yes" id="make_default">
												<label for="make_default">Make this my default address</label>
											</div>
											<div class="form-submit-btn flex-full justify-content-center">
												<button type="button" onclick="addmyprofileaddress();" class="explore-btn" >Save</button>
												<div class="alert alert-success" id="div" style="display: none">
                  									<strong id="success_msg"></strong>
                								</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php $this->load->view('site/templates/footer'); ?>
</body>
</html>