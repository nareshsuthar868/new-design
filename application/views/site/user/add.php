<?php $this->load->view('site/templates/new_header'); ?>
<link rel="stylesheet" type="text/css" media="all" href="css/site/<?php
echo SITE_COMMON_DEFINE ?>timeline.css" />
			<div class="page_section_offset lightgryabg pageheight">
				<section class="innerbanner addproinner">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-12">
                      <h1>Add Product</h1>
                      <ul class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Add Product</li>
                      </ul>
                    </div>
                  </div>
                </div>
                </section>
                <div class="container">
					<div class="row">
					  <div class="profilecolumn">
						<?php $this->load->view('site/user/settings_sidebar'); ?>
						<main class="col-lg-9 col-md-9 col-sm-9 m_bottom_30 m_xs_bottom_10">
						   <section class="profilerightedit">
								<div>
									<div class="timeline <?php echo $viewhome; ?>">
										<div class="wrapper-content landing_page">
											 
												<!-- script to hide everything dropdown when clicking outside -->
												<div class="welcome" style="margin-top: 0;margin-bottom:40px;padding-top:40px;font-size:24px;"><h3>Add Products</h3></div>
													<div class="step1">
														<ul class="case <?php echo $force_login;?>" style="  margin-bottom: 70px;overflow:hidden">
															<li>
																<a href="add" class="mn-add-web">
																	<span class="icon-upload-cloud"></span> Add from Web
																</a>
															</li> 
															<li>
																<a href="add" class="mn-add-upload" >
																	<span class="icon-laptop"></span>
																	Upload from Computer
																</a>
														   </li>
														</ul>
													</div>
											 
										</div>
									</div>
								</div>
							</section>
						</main>
					</div>
				</div>
			</div>
		<!--footer-->
		</div>
<?php $this->load->view('site/templates/footer'); ?>
<!--libs include-->
<script src="plugins/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/site/landing_category.js"></script>
<script type="text/javascript">

function everythingView(val){
	if($('#everythinglist'+val).css('display')=='block'){
		$('#everythinglist'+val).hide('');	
	}else{
		$('#everythinglist'+val).show('');
	}
}
</script>

</body>
</html>