<?php
$this->load->view('site/templates/header_inner');
?>
<!--main content-->
<div class="page-wrapper flex-full">
            <!-- Inner page heading starts -->
            <section class="innerpage-heading customer-feedback-heading flex-full justify-content-center d-flex-xs">
                <div class="container">
                    <div class="innerpage-heading-wrapper flex-full justify-content-center position-relative" style="">
                        <div class="innerpage-description flex-full justify-content-center text-center">
                            <h1>Customer Feedback and Testimonials</h1>
                            <ul class="breadcrumbs-listing flex-full align-items-center justify-content-center">
                                <li class="root-link">
                                    <a href="javascript:void(0)">Home</a>
                                </li>
                                <li class="current-link">
                                    <a href="javascript:void(0)">Testimonials</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Inner page heading ends -->
            <section class="customer-feedback-section flex-full">
                <div class="container">
                    <div class="customer-feedback-wrapper">
                        <div class="grid">
                            <div class="grid-sizer"></div>
                            <?php
                                if($userproductList->num_rows()>0){
                                    foreach($userproductList->result() as $productListVal){
                                        $img = 'dummyProductImage.jpg';
            							$imgArr = explode(',', $productListVal->image);
            							if (count($imgArr)>0){
            								foreach ($imgArr as $imgRow){
            									if ($imgRow != ''){
            										$img = $pimg = $imgRow;
            										break;
            									}
            								}
            							}
            				?>
            				            <div class="grid-item">
                                            <div class="grid-item-inner">
                                                <div class="feedback-box">
                                                    <figure>
                                                        <?php
                                                            if(file_exists("images/product/".$img)){
                                                        ?>
                                                                <img src="<?php echo CDN_URL ?>images/product/<?php echo $img; ?>" alt="<?php echo $productListVal->product_name;?>">
                                                        <?php
                                                            }else{
                                                        ?>
                                                                <img src="<?php echo CDN_URL ?>images/product/dummyProductImage.jpg" alt="<?php echo $productListVal->product_name;?>">
                                                        <?php
                                                            }
                                                        ?>
                                                    </figure>
                                                    <div class="feedback-text">
                                                        <?php 
                                                            $name_city= explode(",",$productListVal->product_name);	
										                ?>
                                                        <h3 class="border-center"><?php
										if(isset($name_city[0])){
										 	echo $name_city[0]; 
										 } 
										 ?><span><?php
										if(isset($name_city[1])){
										 	echo ', '.$name_city[1]; 
										 } ?></span></h3>
                                                        <p><?php echo $productListVal->excerpt;?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
            				<?php
                                    }
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </section>
        </div>
			
		<!--footer-->
		<?php
		$this->load->view('site/templates/footer');
		?>
        <script src="js/masonry.pkgd.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.grid').masonry({
                    itemSelector: '.grid-item',
                    columnWidth: '.grid-sizer',
                    percentPosition: true
                });
            });
        </script>
	</body>
</html>