<?php $this->load->view('site/templates/header_inner'); ?>
<div class="page-wrapper flex-full">
		<!-- Breadcurmbs starts -->
		<div class="breadcrumbs flex-full">
			<div class="container">
				<ul class="breadcrumbs-listing flex-full align-items-center">
					<li class="root-link"><a href="javascript:void(0)">Home</a></li>
					<li class="child-link"><a href="javascript:void(0)">Home furniture</a></li>
					<li class="current-link"><a href="javascript:void(0)">Living room</a></li>
				</ul>
			</div>
		</div>
		<!-- Breadcurmbs ends -->

		<div class="search-result-main flex-full align-items-start align-content-start">
			<div class="container">				
				<div class="search-result-wrapper flex-full align-items-start align-content-start">
					<aside class="filter-area flex-full">
						<div class="advertise-block flex-full">
							<div class="advertise-text flex-full justify-content-center text-center">								
								<h4>Unable to find what you are looking for?</h4>
							</div>
							<div class="contact-text flex-full justify-content-center text-center">
								<h5>Talk to us:</h5>
								<span class="contact-number flex-full align-items-center justify-content-center"><i class="icn icn-phone-black"></i><a href="tel:8010845000">8010845000</a></span>
								<span class="timing">( 09:00AM - 09:00PM )</span>
							</div>
							<div class="advertise-img flex-full justify-content-center text-center">
								<img src="<?php echo base_url() ?>images/advertise-img.png" alt="advertise-img">
							</div>
						</div>
					</aside>
					<div class="product-area flex-full">
						<div class="search-result-text-wrapper flex-full">
							<h3>Showing Search Results for</h3>
							<form class="searchbar flex-full">
								<input type="text" name="search_web" value="<?php echo $search_query ?>">
								<i class="icn icn-close-dark search-close"></i>
							</form>
						</div>
						<div class="product-topbar flex-full">
						    <div class="sort-by flex-full align-items-center">
                                <label>Sort by : </label>
                                <select id="sortingdrop" onchange="sortProductWeb('<?php echo $cat_slug; ?>',this)"> 
                                    <option value="all" selected>Default</option>
                                    <option value="new">New</option>
                                    <option value="low_high">Price - Low-High</option>
                                    <option value="high_low">Price - High-Low</option>
                                </select>
                            </div>
						</div>
						<?php
						    if(!empty($result_product)){
						?>
						        <ul class="product-listing flex-full">
						            <?php
						                foreach($result_product as $productListVal){
						                    $imgArr = explode(',', $productListVal->image);                        
                                        if (count($imgArr) > 0) {
                                            foreach ($imgArr as $imgRow) {
                                                if ($imgRow != '') {
                                                    $img = $pimg = $imgRow;
                                                    break;
                                                }
                                            }
                                        }
						            ?>
						                    <li>
                								<div class="product-single flex-full position-relative">
                									<div class="product-image flex-full position-relative">
                										<a href="<?php echo base_url(); ?>things/<?php echo $productListVal->id; ?>/<?php echo $productListVal->seourl; ?>" class="flex-full position-relative h-100">
                											<amp-img src="<?php echo CDN_URL; ?>images/product/Copressed Images/<?php echo $img; ?>" alt="<?php echo $productListVal->product_name; ?>" layout="responsive" width="300" height="225">
                											    <noscript>
                                                <img src="<?php echo base_url(); ?>/images/ajax-loader/ajax-loader.gif" alt="Example" loading="lazy">
                                            </noscript>
                											</amp-img>
                											<!--<span class="new-label">New</span>-->
                											<span class="wishlist"><i class="icn icn-wishlist-fill-gray"></i></span>
                										</a>
                									</div>
                									<div class="product-description flex-full">
                										<div class="product-description-wrapper flex-full">
                											<h3><a href="<?php echo base_url(); ?>things/<?php echo $productListVal->id; ?>/<?php echo $productListVal->seourl; ?>" target="_blank"><?php echo $productListVal->product_name; ?></a></h3>
                											<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> <?php echo number_format($productListVal->sale_price); ?></strong> / mon</p>
                										</div>
                									</div>
                								</div>
                							</li>
						            <?php
						                }
						            ?>							
        						</ul>
						<?php
						    }
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('site/templates/footer'); ?>
</div>
<script src="plugins/owl-carousel/owl.carousel.min.js"></script> 
<script>
    $(function(){
        $(".search-close").on('click', function(){
            $(this).prev().val(""); 
        });
        
       $("input[name='search_web']").on('input', function(e){
           console.log(this.value);
           var search_query_web = this.value;
           $.ajax({
                url:baseURL + "site/product/search_product",
                type:'post',
                data:{
                    searchValue:search_query_web
                },
                dataType:'json',
                success:function(e){
                    let search_data = '';
                    
                    if(e.product.length > 0){
                        $.each(e.product, function(key, val){
                            search_data += `
                                <li>
                								<div class="product-single flex-full position-relative">
                									<div class="product-image flex-full position-relative">
                										<a href="${baseURL}things/${val.id}/${val.seourl}" class="flex-full position-relative h-100">
                											<amp-img src="${baseURL}images/product/Copressed Images/${val.image.split(',')[0]}" layout="responsive" width="300" height="225">
                											    <noscript>
                                                <img src="${baseURL}/images/ajax-loader/ajax-loader.gif" alt="Example" loading="lazy">
                                            </noscript>
                											</amp-img>
                											
                											<span class="wishlist"><i class="icn icn-wishlist-fill-gray"></i></span>
                										</a>
                									</div>
                									<div class="product-description flex-full">
                										<div class="product-description-wrapper flex-full">
                											<h3><a href="${baseURL}things/${val.id}/${val.seourl}" target="_blank">${val.product_name}</a></h3>
                											<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> ${val.sale_price}</strong> / mon</p>
                										</div>
                									</div>
                								</div>
                							</li>
                            `;
                        });
                        
                        if($("ul.product-listing").length){
                            $("ul.product-listing").html(search_data);
                        }else{
                            $(`<ul class="product-listing flex-full">${search_data}</ul>`).insertAfter("div.product-topbar");   
                        }
                    }
                    
                }
            });
       }); 
    });
    
    function sortProductWeb(cat_slug, filter_value){
        var filter_value = $(filter_value).val();
        var search_query_web = $("input[name='search_web']").val();
        $.ajax({
            url:baseURL + "site/product/search_product",
            type:'post',
            data:{
                searchValue:search_query_web,
                filter_value:filter_value
            },
            dataType:'json',
            success:function(e){
                let search_data = '';
                
                if(e.product.length > 0){
                    $.each(e.product, function(key, val){
                        search_data += `
                            <li>
                								<div class="product-single flex-full position-relative">
                									<div class="product-image flex-full position-relative">
                										<a href="${baseURL}things/${val.id}/${val.seourl}" class="flex-full position-relative h-100">
                											<amp-img src="${baseURL}images/product/Copressed Images/${val.image.split(',')[0]}" layout="responsive" width="300" height="225">
                											    <noscript>
                                                <img src="${baseURL}/images/ajax-loader/ajax-loader.gif" alt="Example" loading="lazy">
                                            </noscript>
                											</amp-img>
                											
                											<span class="wishlist"><i class="icn icn-wishlist-fill-gray"></i></span>
                										</a>
                									</div>
                									<div class="product-description flex-full">
                										<div class="product-description-wrapper flex-full">
                											<h3><a href="${baseURL}things/${val.id}/${val.seourl}" target="_blank">${val.product_name}</a></h3>
                											<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> ${val.sale_price}</strong> / mon</p>
                										</div>
                									</div>
                								</div>
                							</li>
                            `;
                        });
                        
                        if($("ul.product-listing").length){
                            $("ul.product-listing").html(search_data);
                        }else{
                            $(`<ul class="product-listing flex-full">${search_data}</ul>`).insertAfter("div.product-topbar");   
                        }
                    }
                    
                }
            });
    }
</script>
</body>
</html>
