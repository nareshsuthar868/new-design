<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * This model contains all db functions related to Cart Page
 * @author Teamtweaks
 *
 */
class Cart_model extends My_Model {

    public function view_sub_product_details_join($prdId=''){
        //echo "123-".$prdId;exit;
        $select_qry = "select a.*,b.attr_name as attr_type from ".SUBPRODUCT." a join ".PRODUCT_ATTRIBUTE." b on a.attr_id = b.id where a.product_id = '".$prdId."'";
        
        return $attList = $this->ExecuteQuery($select_qry);
    }
    
    public function add_cart($dataArr = '') {
        $this->db->insert(PRODUCT, $dataArr);
    }

    public function edit_cart($dataArr = '', $condition = '') {
        $this->db->where($condition);
        $this->db->update(PRODUCT, $dataArr);
    }

    public function view_cart($condition = '') {
        return $this->db->get_where(PRODUCT, $condition);
    }

    public function mani_cart_view($userid = '') {
        $mobiledetect = new Mobile_Detect();
        if ($mobiledetect->isMobile() || $mobiledetect->isTablet() || $mobiledetect->isAndroidOS()) {
            $is_mobile = 1;
        } else {
            $is_mobile = 0;
        }
        $MainShipCost = 0;
        $MainTaxCost = 0;
        $cartQty = 0;
            
        $country_query = $this->db->get('fc_country');

        $shipVal = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $userid));//12140, 
        $new_coupon_code = $this->get_coupon_value($userid);
        
        if ($shipVal->num_rows() > 0) {

            $shipValID = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $userid, 'primary' => 'Yes'));
            $dataArr = array('shipping_id' => $shipValID->row()->id);
            $condition = array('user_id' => $userid);
            $this->cart_model->update_details(FANCYYBOX_TEMP, $dataArr, $condition);
            $ShipCostVal = $this->cart_model->get_all_details(COUNTRY_LIST, array('country_code' => $shipValID->row()->country));
            
            $MainShipCost = $ShipCostVal->row()->shipping_cost;
            $MainTaxCost = $ShipCostVal->row()->shipping_tax;
            $dataArr2 = array('shipping_cost' => $MainShipCost, 'tax' => $MainTaxCost);
            $this->cart_model->update_details(SHOPPING_CART, $dataArr2, $condition);
        }
        
        $GiftValue = '';
        $CartValue = '';
        $SubscribValue = '';

        $giftSet = $this->cart_model->get_all_details(GIFTCARDS_SETTINGS, array('id' => '1'));
        $giftRes = $this->cart_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $userid));

        $SubcribRes = $this->minicart_model->get_all_details(FANCYYBOX_TEMP, array('user_id' => $userid));
         $get_user = $this->minicart_model->get_all_details(USERS, array('id' => $userid));

        $this->db->select('a.*,b.product_name,b.is_addon,b.addons,b.quantity as mqty,b.seourl,b.image,b.id as prdid,b.price as orgprice,b.ship_immediate,c.attr_name as attr_type,d.attr_name');
        $this->db->from(SHOPPING_CART . ' as a');
        $this->db->join(PRODUCT . ' as b', 'b.id = a.product_id');
        $this->db->join(SUBPRODUCT . ' as d', 'd.pid = a.attribute_values', 'left');
        $this->db->join(PRODUCT_ATTRIBUTE . ' as c', 'c.id = d.attr_id', 'left');
        $this->db->where('a.user_id = ' . $userid);
        $cartVal = $this->db->get();

        $this->db->select('discountAmount,couponID,couponCode,coupontype');
        $this->db->from(SHOPPING_CART);
        $this->db->where('user_id = ' . $userid);
        $discountQuery = $this->db->get();

        $disAmt = $discountQuery->row()->discountAmount;

        /*         * Lanuage Transalation* */

        if ($this->lang->line('cart_product') != '')
            $cart_product = stripslashes($this->lang->line('cart_product'));
        else
            $cart_product = "Product";

        if ($this->lang->line('giftcard_price') != '')
            $giftcard_price = stripslashes($this->lang->line('giftcard_price'));
        else
            $giftcard_price = "Product Rental";

        if ($this->lang->line('product_quantity') != '')
            $product_quantity = stripslashes($this->lang->line('product_quantity'));
        else
            $product_quantity = "Quantity";


        if ($this->lang->line('purchases_total') != '')
            $purchases_total = stripslashes($this->lang->line('purchases_total'));
        else
            $purchases_total = "Total";

        if ($this->lang->line('product_remove') != '')
            $product_remove = stripslashes($this->lang->line('product_remove'));
        else
            $product_remove = "Remove";

        if ($this->lang->line('giftcard_reci_name') != '')
            $giftcard_reci_name = stripslashes($this->lang->line('giftcard_reci_name'));
        else
            $giftcard_reci_name = "Recipient name";

        if ($this->lang->line('giftcard_rec_email') != '')
            $giftcard_rec_email = stripslashes($this->lang->line('giftcard_rec_email'));
        else
            $giftcard_rec_email = "Recipient e-mail";

        if ($this->lang->line('giftcard_message') != '')
            $giftcard_message = stripslashes($this->lang->line('giftcard_message'));
        else
            $giftcard_message = "Message";

        if ($this->lang->line('choose_shipping_address') != '')
            $choose_shipping_address = stripslashes($this->lang->line('choose_shipping_address'));
        else
            $choose_shipping_address = "Choose Your Shipping Address";

        if ($this->lang->line('delete_this_address') != '')
            $delete_this_address = stripslashes($this->lang->line('delete_this_address'));
        else
            $delete_this_address = "Delete this address";

        if ($this->lang->line('addnew_shipping_address') != '')
            $addnew_shipping_address = stripslashes($this->lang->line('addnew_shipping_address'));
        else
            $addnew_shipping_address = "Add new shipping address";

        if ($this->lang->line('continue_payment') != '')
            $continue_payment = stripslashes($this->lang->line('continue_payment'));
        else
            $continue_payment = "Continue to Payment";

        if ($this->lang->line('checkout_item_total') != '')
            $checkout_item_total = stripslashes($this->lang->line('checkout_item_total'));
        else
            $checkout_item_total = "Item total";

        if ($this->lang->line('update') != '')
            $update = stripslashes($this->lang->line('update'));
        else
            $update = "Update";

        if ($this->lang->line('referrals_shipping') != '')
            $referrals_shipping = stripslashes($this->lang->line('referrals_shipping'));
        else
            $referrals_shipping = "Shipping";

        if ($this->lang->line('shipping_speed') != '')
            $shipping_speed = stripslashes($this->lang->line('shipping_speed'));
        else
            $shipping_speed = "Shipping Speed";

        if ($this->lang->line('items_in_shopping') != '')
            $items_in_shopping = stripslashes($this->lang->line('items_in_shopping'));
        else
            $items_in_shopping = "item(s) in cart";

        if ($this->lang->line('order_from') != '')
            $order_from = stripslashes($this->lang->line('order_from'));
        else
            $order_from = "Order from";

        if ($this->lang->line('merchant') != '')
            $merchant = stripslashes($this->lang->line('merchant'));
        else
            $merchant = "Merchant";

        if ($this->lang->line('header_ship_within') != '')
            $header_ship_within = stripslashes($this->lang->line('header_ship_within'));
        else
            $header_ship_within = "Ships within 1-3 business days";

        if ($this->lang->line('shopping_cart_empty') != '')
            $shopping_cart_empty = stripslashes($this->lang->line('shopping_cart_empty'));
        else
            $shopping_cart_empty = "Your Shopping Cart is Empty";

        if ($this->lang->line('dont_miss') != '')
            $dont_miss = stripslashes($this->lang->line('dont_miss'));
        else
            $dont_miss = "Don`t miss out on awesome sales right here on";

        if ($this->lang->line('shall_we') != '')
            $shall_we = stripslashes($this->lang->line('shall_we'));
        else
            $shall_we = "Let`s fill that cart, shall we?";

        if ($this->lang->line('checkout_tax') != '')
            $checkout_tax = stripslashes($this->lang->line('checkout_tax'));
        else
            $checkout_tax = "Tax";

        if ($this->lang->line('gift') != '')
            $gift = stripslashes($this->lang->line('gift'));
        else
            $gift = "Gift";


        if ($this->lang->line('order_is_gift') != '')
            $order_is_gift = stripslashes($this->lang->line('order_is_gift'));
        else
            $order_is_gift = "This order is a gift";

        if ($this->lang->line('coupon_codes') != '')
            $coupon_codes = stripslashes($this->lang->line('coupon_codes'));
        else
            $coupon_codes = "Coupon Codes";

        if ($this->lang->line('checkout_order') != '')
            $checkout_order = stripslashes($this->lang->line('checkout_order'));
        else
            $checkout_order = "Order";

        if ($this->lang->line('ship_to') != '')
            $ship_to = stripslashes($this->lang->line('ship_to'));
        else
            $ship_to = "Ship to";

        if ($this->lang->line('note_to') != '')
            $note_to = stripslashes($this->lang->line('note_to'));
        else
            $note_to = "Note to";

        if ($this->lang->line('apply') != '')
            $apply = stripslashes($this->lang->line('apply'));
        else
            $apply = "Apply";

        if ($this->lang->line('optional') != '')
            $optional = stripslashes($this->lang->line('optional'));
        else
            $optional = "Optional";

        if ($this->lang->line('note_here') != '')
            $note_here = stripslashes($this->lang->line('note_here'));
        else
            $note_here = "You can leave a personalized note here";

        if ($this->lang->line('Enter coupon code here') != '')
            $have_coupon_code = stripslashes($this->lang->line('Enter coupon code here'));
        else
            $have_coupon_code = "Enter coupon code here";

        if ($this->lang->line('retail_price') != '')
            $retail_price = stripslashes($this->lang->line('retail_price'));
        else
            $retail_price = "Retail price";

        /*         * Lanuage Transalation* */


        //$resultCart = $cartVal->result_array();
        /*         * **************************** Gift Card Displays ************************************* */

        if ($giftRes->num_rows() > 0) {

            $GiftValue .= '<div id="GiftCartTable" style="display:block;">
			<form method="post" name="giftSubmit" id="giftSubmit" class="continue_payment" enctype="multipart/form-data" action="checkout/gift" >
				<div class="cart-payment-wrap cart-note"><span class="cart-payment-top"><b></b></span><div class="table-cart-wrap"><table class="table-cart">
				<thead><tr><th width="51%" colspan="2" class="product">' . $cart_product . '</th><th width="18%">' . $giftcard_price . '</th><th width="15%">' . $product_quantity . '</th><th width="21%">' . $purchases_total . '</th></tr></thead></table>';
            $giftAmt = 0;
            $g = 0;

            foreach ($giftRes->result() as $giftRow) {
                $GiftValue .= '<div id="giftId_' . $g . '" style="display:block;"><table class="table-cart"><tbody><tr class="first">
                                    <td rowspan="2" class="thumnail"><img src="' . GIFTPATH . $giftSet->row()->image . '" alt="' . $giftSet->row()->title . '"><a href="javascript:delete_gift(' . $giftRow->id . ',' . $g . ')" class="remove_gift_card" cid="66577">' . $product_remove . '</a></td>
                                    <td class="title"><a href=""><b>' . $giftSet->row()->title . '</b></a><br></td>
                                    <td class="price">' . $this->data['currencySymbol'] . number_format($giftRow->price_value, 2, '.', '') . '</td>
                                    <td class="price">1</td>
                                    <td class="total">' . $this->data['currencySymbol'] . number_format($giftRow->price_value, 2, '.', '') . '</td>
				</tr>
                <tr>
            		<td class="optional" colspan="4"><div class="relative"><span></span>
						<ul class="optional-list">
							<li><span class="option-tit">' . $giftcard_reci_name . ':</span><span class="option-txt">' . $giftRow->recipient_name . '</span></li>
							<li><span class="option-tit">' . $giftcard_rec_email . ':</span><span class="option-txt">' . $giftRow->recipient_mail . '</span></li>
    	                    <li><span class="option-tit">' . $giftcard_message . ':</span><span class="option-txt">' . $giftRow->description . '</span></li>
						</ul>
						</div>
					</td>
				</tr></tbody></table></div>';
                $giftAmt = $giftAmt + $giftRow->price_value;
                $g++;
            }

            $GiftValue .= '</div>
			<input name="gift_cards_amount" id-"gift_cards_amount" value="' . number_format($giftAmt, 2, '.', '') . '" type="hidden">
			<input name="checkout_type" id-"checkout_type" value="giftpurchase" type="hidden">
			<div class="cart-payment" id="giftcard-cart-payment"><input type="hidden"><span class="bg-cart-payment"></span>
		    <dl class="cart-payment-order">
			   	<dt>' . $checkout_order . '</dt><dd><ul>
				<li class="first"><span class="order-payment-type">' . $checkout_item_total . '</span><span class="order-payment-usd"><b>' . $this->data['currencySymbol'] . '<span id="item_total">' . number_format($giftAmt, 2, '.', '') . '</span></b> ' . $this->data['currencyType'] . '</span></li>
				<li class="total"><span class="order-payment-type"><b>Total</b></span><span class="order-payment-usd"><b>' . $this->data['currencySymbol'] . '<span id="total_item">' . number_format($giftAmt, 2, '.', '') . '</span></b> ' . $this->data['currencyType'] . '</span></li>
				</ul>
		      	</dd>
			</dl>
		    <button type="submit" class="btn" id="button-submit-giftcard">' . $continue_payment . '</button>
		  	</div></div></form></div>';
        }

        /*         * **************************** Subscribe Card Displays ************************************* */
        if ($SubcribRes->num_rows() > 0) {

            $SubscribValue .= '<div id="SubscribeCartTable" style="display:block;">
			<form method="post" name="SubscribeSubmit" id="SubscribeSubmit" class="continue_payment" enctype="multipart/form-data" action="site/cart/Subscribecheckout">
				<div class="cart-payment-wrap cart-note"><span class="cart-payment-top"><b></b></span><div class="table-cart-wrap"><table class="table-cart">
			<thead><tr><th width="51%" colspan="2" class="product">' . $cart_product . '</th><th width="18%">' . $giftcard_price . '</th><th width="15%">' . $product_quantity . '</th><th width="21%">' . $purchases_total . '</th></tr></thead></table>
				';
            $SubscribAmt = 0;
            $subcribSAmt = 0;
            $subcribTAmt = 0;
            $SubgrantAmt = 0;
            $s = 0;

            foreach ($SubcribRes->result() as $SubcribRow) {
                $SubscribValue .= '<div id="SubscribId_' . $s . '" style="display:block;"><table class="table-cart"><tbody><tr class="first">
					<td rowspan="2" class="thumnail"><img src="' . FANCYBOXPATH . $SubcribRow->image . '" alt="' . $SubcribRow->name . '"><a href="javascript:delete_subscribe(' . $SubcribRow->id . ',' . $s . ')" class="remove_gift_card" cid="66577">' . $product_remove . '</a></td>
					<td class="title"><a href=""><b>' . $SubcribRow->name . '</b></a><br></td>
					<td class="price">' . $this->data['currencySymbol'] . number_format($SubcribRow->price, 2, '.', '') . '</td>
					<td class="price">1</td>
					<td class="total">' . $this->data['currencySymbol'] . number_format($SubcribRow->price, 2, '.', '') . '</td>
				</tr>
				</tbody></table></div>';
                $SubscribAmt = $SubscribAmt + $SubcribRow->price;
                $s++;
            }

            $subcribSAmt = $MainShipCost;
            $subcribTAmt = ($SubscribAmt * 0.01 * $MainTaxCost);
            $SubgrantAmt = $SubscribAmt + $subcribSAmt + $subcribTAmt;


            $SubscribValue .= '</div>
			 <div class="cart-payment" id="merchant-cart-payment">
		    <input type="hidden">
		    <span class="bg-cart-payment"></span>
		    <dl class="cart-payment-ship">
		      <dt>' . $ship_to . '</dt>
		      <dd>
			<select id="address-cart" class="select-round select-shipping-addr" onchange="SubscribeChangeAddress(this.value);">
				  <option value="" id="address-select">' . $choose_shipping_address . '</option>
			';

            foreach ($shipVal->result() as $Shiprow) {
                if ($Shiprow->primary == 'Yes') {
                    $optionsValues = 'selected="selected"';
                    $ChooseVal = $Shiprow->full_name . '<br>' . $Shiprow->address1 . '<br>' . $Shiprow->city . ' ' . $Shiprow->state . ' ' . $Shiprow->postal_code . '<br>' . $Shiprow->country . '<br>' . $Shiprow->phone;
                    $ship_id = $Shiprow->id;
                } else {
                    $optionsValues = '';
                }
                $SubscribValue .= '<option ' . $optionsValues . ' value="' . $Shiprow->id . '" l1="' . $Shiprow->full_name . '" l2="' . $Shiprow->address1 . '" l3="' . $Shiprow->city . ' ' . $Shiprow->state . ' ' . $Shiprow->postal_code . '" l4="' . $Shiprow->country . '" l5="' . $Shiprow->phone . '">' . $Shiprow->full_name . '</option>';
            }

            if ($is_mobile == 1) {
                $csspatch = "$('#quick_view').attr('style','display:block !important;')";
            } else {
                $csspatch = '';
            }
            $SubscribValue .= '</select>
			<input type="hidden" name="SubShip_address_val" id="SubShip_address_val" value="' . $ship_id . '" />
			
			<p class="default_addr"><span id="SubChg_Add_Val">' . $ChooseVal . '</span></p>
			<span style="color:#FF0000;" id="Ship_Sub_err"></span>
			<a href="javascript:void(0);" class="delete_addr" onclick="shipping_cart_address_delete();">' . $delete_this_address . '</a>
			
			<a href="javascript:void(0);" class="add_addr add_" onclick="shipping_address_cart();' . $csspatch . '">' . $addnew_shipping_address . '</a>

		      </dd>
			</dl>

			   <dl class="cart-payment-order">
		      <dt>' . $checkout_order . '</dt>
		      <dd>
			<ul>
			  <li class="first">
			    <span class="order-payment-type">' . $checkout_item_total . '</span>
			    <span class="order-payment-usd"><b>' . $this->data['currencySymbol'] . '<span id="SubCartAmt">' . number_format($SubscribAmt, 2, '.', '') . '</span></b> ' . $this->data['currencyType'] . '</span>
			  </li>
			  <li>
			    <span class="order-payment-type">' . $referrals_shipping . '</span>
			    <span class="order-payment-usd"><b>' . $this->data['currencySymbol'] . '<span id="SubCartSAmt">' . number_format($subcribSAmt, 2, '.', '') . '</span></b> ' . $this->data['currencyType'] . '</span>
			  </li>
			  <li>
			    <span class="order-payment-type">' . $checkout_tax . ' (<span id="SubTamt">' . $MainTaxCost . '</span>%) of ' . $this->data['currencySymbol'] . $SubscribAmt . '</span>
			    <span class="order-payment-usd"><b>' . $this->data['currencySymbol'] . '<span id="SubCartTAmt">' . number_format($subcribTAmt, 2, '.', '') . '</span></b> ' . $this->data['currencyType'] . '</span>
			  </li></ul>';

            $SubscribValue .= '
			  <ul>
			 <li class="total">
			    <span class="order-payment-type"><b>Total</b></span>
			    <span class="order-payment-usd"><b>' . $this->data['currencySymbol'] . '<span id="SubCartGAmt">' . number_format($SubgrantAmt, 2, '.', '') . '</span></b> ' . $this->data['currencyType'] . '</span>
			  </li>
			</ul>
		      </dd>
	              
		    </dl>
			
		    <input name="user_id" value="' . $userid . '" type="hidden">
			<input name="subcrib_amount" id="subcrib_amount" value="' . number_format($SubscribAmt, 2, '.', '') . '" type="hidden">
			<input name="subcrib_ship_amount" id="subcrib_ship_amount" value="' . number_format($subcribSAmt, 2, '.', '') . '" type="hidden">
			<input name="subcrib_tax_amount" id="subcrib_tax_amount" value="' . number_format($subcribTAmt, 2, '.', '') . '" type="hidden">
			<input name="subcrib_total_amount" id="subcrib_total_amount" value="' . number_format($SubgrantAmt, 2, '.', '') . '" type="hidden">
		    <input type="submit" class="btn" name="SubscribePayment" id="button-submit-merchant" value="' . $continue_payment . '" />
		    
		  </div>
		</div>
	</form></div>';
        }


        /* ***************************** Cart Displays ************************************* */
        //print_r($ShipCostVal);exit;
        //print_r($ShipCostVal->result_array());exit;
        if ($cartVal->num_rows() > 0) {
            $CartValue .= '
                    <div class="col-lg-12 col-md-12 col-sm-12 cartcol" id="sectional">
                    <form class="continue_payment" name="cartSubmit" method="post" id="cartSubmit" enctype="multipart/form-data" action="site/cart/cartcheckout">
                        <div class="setsection">
                          <div class="payment-title">
                                <div class="icon_number"> <span class="number">01</span> <i class="material-icons doneicn">done</i></div>Cart Summary
                          </div>
                          <div class="form-section">
                            <div class="frmcart"> 
                              <div class="table-responsive hidden-xs">
                                <table class="table table-bordered">
                                  <thead>
                                    <tr class="bg_grey_light_2 second_font">
                                      <th class="product"> <b>Product</b> </th>
                                      <th> <b>Quantity</b> </th>
                                      <th> <b>Advance Rental</b> </th>
                                      <th> <b>Refundable Deposit</b> </th>
                                      <th> <b>Tenure Duration</b> </th>
                                      <th>&nbsp; </th>
                                    </tr>
                                  </thead>
                                  <tbody>';
                                $cartAmt = 0;
                                $cartShippingAmt = 0;
                                $cartTaxAmt = 0;
                                $cartDiscountAmt = 0;
                                $cartDisAmt = 0;
                                $s = 0;
                                $prod_id_array = array();
                                $prod_addons_array = array();
                                
                                foreach ($cartVal->result() as $CartRow) {
                                    $curdate = date('Y-m-d');
                                    $newImg = @explode(',', $CartRow->image);
                                    $tenure = $this->cart_model->view_sub_product_details_join($CartRow->prdid);

                                    $res_array = $this->cart_model->check_is_addon_prod($CartRow->product_id);

                                    $isaddon = $res_array[0]['is_addon'];
                                    if ($isaddon == 1) {
                                        $yes_addon_prod_id[] = $res_array[0]['id'];
                                    }
                                    $addons_ids = $res_array[0]['addons'];
                                    if ($addons_ids != NULL) {
                                        $addons_array[] = $res_array[0]['addons'];
                                    }
                    $CartValue .= '<tr id="cart-row-'.$CartRow->id.'">
                                    <td>
                                        <a href="'.base_url().'things/'.$CartRow->prdid.'/'.url_title($CartRow->product_name, '-').'"> 
                                            <span class="pro-thumb"> 
                                                <img src="' . PRODUCTPATH . $newImg[0] . '" alt="' . $CartRow->product_name . '" /> 
                                            </span> 
                                            <span class="hidden-xs"><strong>' . $CartRow->product_name . '</strong>';
                                                if ($CartRow->attr_name != '' || $CartRow->attr_type != '') {
                                                    $CartValue .= '<br>' . $CartRow->attr_type . ' / ' . $CartRow->attr_name . '';
                                                }
                    $CartValue .= '</span> </a> 
                                    </td>

                                    <td class="title-mobile"> 
                                              <a class="titlemobile" href="#"><span><strong>' . $CartRow->product_name . '</strong>';
                                                if ($CartRow->attr_name != '' || $CartRow->attr_type != '') {
                                                    $CartValue .= '<br>' . $CartRow->attr_type . ' / ' . $CartRow->attr_name . '';
                                                }
                    $CartValue .= '</span></a>
                                      <a href="javascript:void(0);" onclick="javascript:delete_cart(' . $CartRow->id . ',' .  $CartRow->id . ')" class="cartdelete visible-xs">
                                            <i class="material-icons">close</i></a>  
                                      <span class="leftqutyw">Quantity</span>
                                    
                                    <div class="quantity d_inline_b">
                                          <button type="button" class="btn-number qtyplus" data-type="plus" data-field="quant[' . $s . ']">
                                            <i class="material-icons">add_circle_outline</i>
                                        </button>
                                          <input type="text" name="quant[' . $s . ']" class="form-control input-number" value="' . $CartRow->quantity . '" data-mqty="' . $CartRow->mqty. '" id="quantity' . $s . '" min="1" max="' . $CartRow->mqty. '">
                                          <button type="button" class="btn-number qtyminus" data-type="minus" data-field="quant[' . $s . ']">
                                          <i class="material-icons">remove_circle_outline</i>
                                            
                                        </button>  
                                    </div>
                                            
                                        </td>

                                        <td class="price">
                                            <span class="leftqutyw">Advance Rental</span>
                                            <strong id="cart-price"><i class="fa fa-inr" aria-hidden="true"></i> '. $CartRow->price . '</strong>
                                        </td>

                                    <td class="price"><span class="leftqutyw">Refundable Deposit</span>
                                        <strong><i class="fa fa-inr" aria-hidden="true"></i> '. $CartRow->product_shipping_cost . '</strong>
                                    </td>

                                    <td class="mobilelast">
                                        <span class="leftqutyw">Tenure Duration</span>
                                        <div class="quantity d_inline_b">';
                                    if($CartRow->is_addon != 1){     
                                        $CartValue .= '<select name="attribute_values'.$s.'" id="attr_name'.$s.'">';
                                                foreach ($tenure->result_array() as $tenr){ 
                                                    if( $tenr['pid'] == $CartRow->attribute_values){ $selected = 'selected';} else{ $selected = '';}
                                                    $CartValue .= '<option '.$selected.' value="'.$tenr['pid'].'|'.$tenr['attr_price'].'">'. $tenr['attr_name'].'</option>';                                                
                                                };
                                        $CartValue .= ' </select>';
                                    }else{
                                         $CartValue .= '-';
                                    }
                                    $CartValue .= '</div>
                                    </td>

                                    <td>
                                        <a onclick="javascript:update_cart(' . $CartRow->id . ',' . $s . ',' . $CartRow->product_id . ')" class="update_quantity">
                                           Update
                                        </a>
                                        <a href="javascript:void(0);" onclick="javascript:delete_cart(' . $CartRow->id . ',' . $CartRow->id . ')" class="cartdelete">
                                            <i class="material-icons">close</i>
                                        </a>
                                    </td>

                                    </tr>';
                                    $qu = $this->db->query("SELECT maximumamount FROM " . COUPONCARDS . " WHERE id='" . $CartRow->couponID . "' LIMIT 1");
                                    $rr = $qu->result();
                                    $cartAmt = $cartAmt + (($CartRow->price - $CartRow->discountAmount + ($CartRow->price * 0.01 * $CartRow->product_tax_cost)) * $CartRow->quantity);
                                    $cartShippingAmt = $cartShippingAmt + ($CartRow->product_shipping_cost * $CartRow->quantity);
                                    $cartTaxAmt = $cartTaxAmt + ($CartRow->product_tax_cost * $CartRow->quantity);
                                    $cartQty = $cartQty + $CartRow->quantity;
                                    $cartDiscountAmt = $cartDiscountAmt + ($CartRow->discountAmount * $CartRow->quantity);

                                    $s++;                                    
                                    if ($rr[0]->maximumamount != 0) {
                                        If ($cartDiscountAmt > $rr[0]->maximumamount) {
                                            $cartAmt = $cartAmt + $cartDiscountAmt - $rr[0]->maximumamount;
                                            $cartDiscountAmt = $rr[0]->maximumamount;
                                        }
                                    }
                                }
                                $addon_array = implode(',', $addons_array);
                                $aa = explode(',', $addon_array);

                                $i = 0;
                                foreach ($yes_addon_prod_id as $addon_prod_id) {
                                    if (!in_array($addon_prod_id, $aa)) {
                                        $i++;
                                    }
                                }
                                $CartValue .= "<input type='hidden' id='parent_count' value='" . $i . "'>";
                            

                                $cartSAmt = $cartShippingAmt;
                                $cartTAmt = ($cartAmt * 0.01 * $MainTaxCost);
                                $grantAmt = $cartAmt + $cartSAmt + $cartTAmt;
                                $disAmt = $cartDiscountAmt;
                    $CartValue .= '</tbody>
                            </table>
                          </div>
                            

                            <div class="cart-table visible-xs">';
                                $j = 0;
                                    foreach ($cartVal->result() as $CartRow) {
                                    //print_r($CartRow);exit;
                                        $curdate = date('Y-m-d');
                                        $newImg = @explode(',', $CartRow->image);
                                        $tenure = $this->cart_model->view_sub_product_details_join($CartRow->prdid);
                                        $res_array = $this->cart_model->check_is_addon_prod($CartRow->product_id);
                                        $isaddon = $res_array[0]['is_addon'];
                                        if ($isaddon == 1) {
                                            $yes_addon_prod_id[] = $res_array[0]['id'];
                                        }
                                        $addons_ids = $res_array[0]['addons'];
                                        if ($addons_ids != NULL) {
                                            $addons_array[] = $res_array[0]['addons'];
                                        }
                $CartValue .= '<div class="tblrow" id="mobile_cart_row_'.$CartRow->id.'">  
                                    <div class="tblceleft">
                                        <a href="'.base_url().'things/'.$CartRow->prdid.'/'.url_title($CartRow->product_name, '-').'"> <span class="pro-thumb"> <img src="' . PRODUCTPATH . $newImg[0] . '" alt="' . $CartRow->product_name . '"  alt="Alexa Single Bedroom Set"> </span> <span class="hidden-xs"><strong>' . $CartRow->product_name . '</strong><br>';
                                              if ($CartRow->attr_name != '' || $CartRow->attr_type != '') {
                                                  $CartValue .= '<br>' . $CartRow->attr_type . ' / ' . $CartRow->attr_name . '';
                                              }
                                          $CartValue .= '</span>
                                        </a>   
                                    </div>
                                    <div class="tblcellright">
                                        <a href="#" class="titlemobile"><span><strong>' . $CartRow->product_name . '</strong>';if ($CartRow->attr_name != '' || $CartRow->attr_type != '') {                                                      
                                                  }
                                                  $CartValue .= '</span></a>
                                        <a class="cartdelete visible-xs" href="javascript:void(0);" onclick="javascript:delete_cart(' . $CartRow->id . ',' . $CartRow->id . ')"><i class="material-icons">close</i></a>                                    
                                        
                                        <div class="price">
                                            <span class="leftqutyw">Rental</span><strong><i aria-hidden="true" class="fa fa-inr"></i> ' . $this->data['currencySymbol'] . $CartRow->price . '</strong>
                                        </div>
                                        
                                        <div class="price">
                                          <span class="leftqutyw">Deposit</span><strong><i aria-hidden="true" class="fa fa-inr"></i> ' . $this->data['currencySymbol'] . $CartRow->product_shipping_cost . '</strong>
                                        </div>                                            
                                        
                                        <div class="price" style="display:none"><a onclick="javascript:update_cart_mobile(' . $CartRow->id . ',' . $j . ',' . $CartRow->product_id . ')"  class="btnupdate">
                                            <i class="material-icons">cached</i>Update</a>
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="tblecellprice">
                                        <div class="headtitle">
                                            <span class="leftqutyw">Quantity</span>
                                            <div class="quantity d_inline_b">
                                                    <button onclick="update_cart_mobile(' . $CartRow->id . ',' . $j . ',' . $CartRow->product_id .', 0)" type="button" class="btn-number qtyminus" disabled="disabled" data-type="minus" data-field="quant[' . $j . ']">
                                                        <i class="material-icons">remove_circle_outline</i>
                                                    </button>
                                                    <input type="text" name="quant[' . $j . ']" class="form-control input-number" value="' . $CartRow->quantity . '" data-mqty="' . $CartRow->mqty. '" id="quantity-mobile' . $j . '" min="1" max="' . $CartRow->mqty. '">
                                                    <button onclick="update_cart_mobile(' . $CartRow->id . ',' . $j . ',' . $CartRow->product_id .',1)" type="button" class="btn-number qtyplus" data-type="plus" data-field="quant[' . $j . ']">
                                                        <i class="material-icons">add_circle_outline</i>
                                                    </button>
                                            </div>
                                    </div>
                                        <div class="mobilelast">';
                                            if($CartRow->is_addon != 1){
                                                $CartValue .= '<span class="leftqutyw">Tenure</span>
                                            <div class="quantity d_inline_b">';                                                   
                                                    $CartValue .= '<select onchange="update_cart_mobile(' . $CartRow->id . ',' . $j . ',' . $CartRow->product_id . ')""name="attribute_values'.$j.'" id="attri_name'.$j.'">';
                                                        foreach ($tenure->result_array() as $tenr){                                            
                                                            if( $tenr['pid'] == $CartRow->attribute_values){ $selected = 'selected';} else{ $selected = '';}
                                                              $CartValue .= '<option '.$selected.' value="'.$tenr['pid'].'|'.$tenr['attr_price'].'">'. intval($tenr['attr_name']).'</option>';                                                
                                                          }
                                                    
                                                $CartValue .= ' </select>';
                                                    
                                    $CartValue .= '</div>';
                                                              }
                                $CartValue .= '</div>

                                        <!--<div class="price">
                                            <a onclick="javascript:update_cart_mobile(42230,0,3820)" class="btnupdate"><i class="material-icons">cached</i> Update</a></div>
                                        </div>--> 
                                      </div>
                                </div>';
                                      $j++;} 
                $CartValue .= '</div>


                              <div class="row">
                                      <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12 margin-bottom-30">
                                        <div class="getcoupontitle"><i class="material-icons">loyalty</i>Have a coupon code to redeem?</div>';
                                        if ($disAmt > 0) {
                                            
                                        $CartValue .= '<div class="applycoupenecol">
                                            <input autocomplete="off" id="is_coupon" name="is_coupon" class="form-control coupon-code" value="' . $new_coupon_code . '" readonly="readonly" placeholder="' . $have_coupon_code . '" '
                                             .'data-sid="616001" type="text" placeholder="Type coupon code">                                            
                                            <input class="btn-check btn-blue-apply apply-coupon" id="CheckCodeButton" type="button" onclick="javascript:checkRemove();" value="Remove" style="cursor:pointer;">
                                            </div>
                                            <span id="CouponErr"></span>';
                                        }else{                                            
                                            $CartValue .= '<div class="applycoupenecol">
                                            <input autocomplete="off" id="is_coupon" name="is_coupon" class="form-control text coupon-code" placeholder="' . $have_coupon_code . '" data-sid="616001" type="text">
                                            <input id="CheckCodeButton" type="button" class="btn-check btn-blue-apply apply-coupon" onclick="javascript:checkCode();" value="Apply Coupon" style="cursor:pointer;" />
                                            </div>
                                            <span id="CouponErr"></span>';
                                        }
                                        
                                       $CartValue .= ' <div class="notetextarea">
                                                <label><strong>Note</strong>Leave us a note if you have any special request..</label>
                                            <textarea class="form-control" name="note" data-id="cart-note-1192557-616001"></textarea>
                                        </div>
                                      </div>
                                      <div class="col-sm-6 col-md-6 col-lg-6 col-xs-12">
                                        <div class="cart-totalcol">
                                                <div class="carttitle"><strong>Cart Total</strong></div>
                                            <div class="carttotalrow">
                                                <span class="pull-left"> Advance Rental</span>
                                                <span class="pull-right text-right price"><strong><i class="fa fa-inr" aria-hidden="true"></i> <span id="CartAmt">' . number_format($cartAmt, 2, '.', '') . '</strong></span>
                                            </div>
                                            <div class="carttotalrow">
                                                <span class="pull-left">Refundable Deposit</span>
                                                <span class="pull-right text-right price"><strong><i class="fa fa-inr" aria-hidden="true"></i> <span id="CartSAmt">' . number_format($cartShippingAmt, 2, '.', '') . ' </strong></span>
                                            </div>';
                                       
                                            if ($disAmt > 0) {
                                                $grantAmt = $grantAmt;
                                                $CartValue .= '
                                                    <div class="carttotalrow">
                                                        <span class="pull-left">Discount</span>
                                                        <span class="pull-right text-righ pricet"><strong><i class="fa fa-inr" aria-hidden="true"></i> <span id="disAmtVal">' . number_format($disAmt, 2, '.', '') . ' </strong></span>
                                                    </div>';
                                            } else {
                                                $CartValue .= '
                                                    <div class="carttotalrow">
                                                        <span class="pull-left">Discount</span>
                                                        <span class="pull-right text-righ pricet"><strong><i class="fa fa-inr" aria-hidden="true"></i> <span id="disAmtVal">' . number_format($disAmt, 2, '.', '') . ' </strong></span>
                                                    </div>';
                                            }                                             
                                            $CartValue .= '<div class="carttotalrow last">
                                                <span class="pull-left"><strong>Total</strong></span>
                                                <span class="pull-right text-right price"><strong><i class="fa fa-inr" aria-hidden="true"></i> <span id="CartCartGAmt">' . number_format($grantAmt, 2, '.', '') . '</strong></span>
                                            </div>
                                        </div>
                                        <input name="user_id" value="' . $userid . '" type="hidden">
                                        <input name="cart_amount" id="cart_amount" value="' . number_format($cartAmt, 2, '.', '') . '" type="hidden">
                                        <input name="cart_ship_amount" id="cart_ship_amount" value="' . number_format($cartSAmt, 2, '.', '') . '" type="hidden">
                                        <input name="cart_tax_amount" id="cart_tax_amount" value="' . number_format($cartTAmt, 2, '.', '') . '" type="hidden">
                                        <input name="cart_tax_Value" id="cart_tax_Value" value="' . number_format($MainTaxCost, 2, '.', '') . '" type="hidden">
                                        <input name="cart_total_amount" id="cart_total_amount" value="' . number_format($grantAmt, 2, '.', '') . '" type="hidden">
                                        <input name="discount_Amt" id="discount_Amt" value="' . number_format($disAmt, 2, '.', '') . '" type="hidden">';
                                        if ($disAmt > 0) {
                                            $CartValue .= '<input name="CouponCode" id="CouponCode" value="' . $discountQuery->row()->couponCode . '" type="hidden">
                                            <input name="Coupon_id" id="Coupon_id" value="' . $discountQuery->row()->couponID . '" type="hidden">
                                            <input name="couponType" id="couponType" value="' . $discountQuery->row()->coupontype . '" type="hidden">';
                                        } else {
                                            $CartValue .= '<input name="CouponCode" id="CouponCode" value="" type="hidden">
                                            <input name="Coupon_id" id="Coupon_id" value="0" type="hidden">
                                            <input name="couponType" id="couponType" value="" type="hidden">';
                                        }
                                  $CartValue .= '</div>
                              </div>
                              <div class="btncenter">
                                <button class="btn-std form-nav-next btn-check" type="button">Proceed</button>
                              </div>
                              </div>
                          </div>
                        </div>
                        <div class="setsection section-2">
                        <div class="payment-title">
                        <div class="icon_number"> <span class="number">02</span> <i class="material-icons doneicn">done</i></div>Delivery Address Details</div>
                        <div class="form-section addressdetail">
                        
                            <input type="hidden" name="user_id" id="user_id" value="'.$userid.'"/>
                            <div class="selectaddress">
                                <div class="radiocol">
                                    <input type="radio" name="primary" id="newaddress"/ checked="checked">
                                        <label for="newaddress" class="newadd">New Address</label>
                                </div>
                                <div class="radiocol">
                                    <input type="radio" name="primary" id="saveaddress"/>
                                        <label for="saveaddress" class="saveadd">Saved Addresses</label>
                                </div>
                            </div>
                            <div class="addressfrm">
                            
                                <div class="col-lg-6 col-sm-6 col-md-6 addressinput">
                                   <input name="full_name" id="full_name" class="form-control" type="text" placeholder="Full Name" value="'.$get_user->row()->full_name.'">
                                </div>
                                <div class="col-lg-6 col-sm-6 col-md-6 addressinput">
                                   <input name="address1" id="address1" class="form-control" type="text" placeholder="Address 1">
                                </div>
                                 <div class="col-lg-6 col-sm-6 col-md-6 addressinput">
                                   <input name="address2" id="address2" class="form-control" type="text" placeholder="Address 2">
                                </div>
                                 <div class="col-lg-6 col-sm-6 col-md-6 addressinput">
                                   <select name="city" id="city" class="form-control">
                                        <option value="default">Select City</option>
                                        <option value="Delhi">Delhi</option>
                                        <option value="Noida">Noida</option>
                                        <option value="Gurgaon">Gurgaon</option>
                                        <option value="Pune">Pune</option>
                                        <option value="Mumbai">Mumbai</option>
                                        <option value="Bangalore">Bangalore</option>
                                        <option value="Ghaziabad">Ghaziabad</option>
                                        <option value="Faridabad">Faridabad</option>
                                        <option value="Hyderabad">Hyderabad</option>
                                        <option value="Others">Others</option>
                                   </select>
                                </div>
                                 <div class="col-lg-6 col-sm-6 col-md-6  addressinput">
                                   <input name="state" id="state" class="form-control" type="text" placeholder="State">
                                </div>
                                  <div class="col-lg-6 col-sm-6 col-md-6 addressinput">
                                   <input name="postal_code" id="postal_code" class="form-control only-number" type="text" placeholder="Postal Code">
                                </div>
                                 <div class="col-lg-6 col-sm-6 col-md-6  addressinput">
                                   <input name="phone" id="phone" class="form-control only-number" type="text" placeholder="Contact Number">
                                </div>
                                <div class="col-lg-6 col-sm-6 col-md-6  addressinput">
                                   <input name="Alter_phone" id="Alter_phone" class="form-control only-number" type="text" placeholder="Alternate Mobile Number">
                                </div>';
                               
                              
                               
                               
                              
                                $CartValue .= '<div class="col-lg-6 col-sm-6 col-md-6 addressinput">
                                    <div class="checkdiv">
                                        <input id="check" name="set_default" type="checkbox" value="true">
                                        <label for="check">Make this my primary shipping address</label>
                                    </div>                                   
                                </div>
                                <div class="btncenter col-lg-12">
                                <button class="btn-std form-nav-prev btn-check" type="button">Prev</button>
                                    <button class="btn-std btn-check" type="button" id="button-submit-merchant" onclick="addnewaddress();" >proceed</button>
                                </div>
                            
                            </div>
                            <div class="saveaddresscol save-address">';   
                                if($shipVal->num_rows() > 0){
                                    $c = 0;
                                    $Shiprow->id = 0;
                                    foreach ($shipVal->result() as $Shiprow) { 

                                        if($Shiprow->primary == 'Yes'){ $checked = 'checked';}else{ $checked = '';}
                                        $CartValue .= '
                                        <div class="col-lg-6 col-sm-6 col-md-6">
                                            <div class="defaultaddress">
                                                <strong>Address</strong>
                                                <div class="radiocol">
                                                    <input type="radio" '.$checked.' name="Ship_address_val" id="add'.$Shiprow->id.'" value="'.$Shiprow->id.'"  />
                                                    <label for="add'.$Shiprow->id.'"></label>
                                                </div>
                                                <div class="addresstext">
                                                    <p>'. $Shiprow->full_name .'<br /> '.$Shiprow->address1.', '.$Shiprow->address2.'<br /> '.$Shiprow->city.', '.$Shiprow->state.', India<br /> '.$Shiprow->postal_code.'</br>Phone: '.$Shiprow->phone.'</p>
                                                </div>
                                            </div>
                                        </div>';
                                        $c++;
                                    }
                                }
                                $CartValue .= 
                                '<input type="hidden" name="prod_duration" id="prod_duration" value="' . $CartRow->attr_name . '" />
                                <p class="default_addr m_bottom_10 m_top_10 m_left_10"><span id="Chg_Add_Val">' . $ChooseVal . '</span></p>
                                <span style="color:#FF0000;" id="Ship_err"></span>
                                <div class="btncenter submit-form-button col-lg-12">';
                                    
                                    if($shipVal->num_rows() >= 1){
                                        $CartValue .= '<button class="address-btn btn-std form-nav-prev btn-check" type="button">Prev</button><button class="btn-std form-nav-next btn-check" type="button" onclick="get_default_address('.$c.');">Proceed</button>';
                                    }else{
                                        $CartValue .= '<h3 id="empty-address" class="empty-address">Please Add Your Shipping Address</h3>';
                                        $CartValue .= '<button class="address-btn btn-std form-nav-prev btn-check" type="button">Prev</button><button disabled id="save-add" class="btn-std form-nav-next btn-check" type="button" onclick="get_default_address('.$c.');">Proceed</button>';
                                    }
                                $CartValue .= '</div>
                        </div>
                    </div>
                </div>
                <div class="setsection">
                <input type="hidden" id="coupon_value" value="'. $CartRow->id .'" >
          <div class="payment-title"><div class="icon_number"> <span class="number">03</span> <i class="material-icons doneicn">done</i></div>Order Summary</div>
          <div class="form-section orderdetail" style="display: none;">
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="delivaryaddlocate">
                    <span class="iconlocate"><i class="material-icons">room</i> <strong>Your Delivery Address</strong></span>
                    <label id="address_name"></label>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="delivaryaddlocate">
         <span class="iconlocate"><i class="holyoffreicn"><img src="images/done-icn-green.svg" alt="done"></i><strong id="coupon_code">'. $new_coupon_code .'</strong> - Offer Applied</span>
                <ul>
                        <li type="1">Your Rental Payment Will Begin From The<br> Date of Delivery.</li>
                        <li type="1">Once the order has been placed, you will be required to share address and ID proof documents.</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12">
                <div class="cart-totalcol">
                    <div class="carttitle"><strong>Order Detail</strong></div>
                    <div class="carttotalrow">
                        <span class="pull-left"> Advance Rental</span>
                        <span class="pull-right text-right price"><strong><i class="fa fa-inr" aria-hidden="true"></i> <span id="CartAmt1">' . number_format($cartAmt, 2, '.', '') . '</strong></span>
                    </div>
                    <div class="carttotalrow">
                        <span class="pull-left">Refundable Deposit</span>
                        <span class="pull-right text-right price"><strong><i class="fa fa-inr" aria-hidden="true"></i> <span id="CartSAmt1">' . number_format($cartShippingAmt, 2, '.', '') . '</strong></span>
                    </div>
                     <div class="carttotalrow last">
                        <span class="pull-left"><strong>Total</strong></span>
                        <span class="pull-right text-right price"><strong><i class="fa fa-inr" aria-hidden="true"></i>  <span id="CartCartGAmt1">' . number_format($grantAmt, 2, '.', '') . '</strong></span>
                    </div>
                    
                </div>
            </div>
            <div class="col-xs-12 col-lg-12">
                <div class="dividercart"></div> 
                    
            </div>
            <div class="ordercartrow">';
            foreach ($cartVal->result() as $CartRow) {
                 $newImg = explode(',', $CartRow->image);
                 if($CartRow->attr_name == ''){ $slesh = ''; }else { $slesh = '/'; }
             
            $CartValue .=   '<div class="ordercartpad" id="image_tab'.$CartRow->id.'">
                                <div class="cartsumthumb">
                                    <img  src="images/product/'.$newImg[0] .'" alt="' . $CartRow->product_name . '" > 
                                </div>
                                <div class="cartsumdetail">
                                    <p><strong>'. $CartRow->product_name . '</strong></p>
                                    <span><strong><i class="fa fa-inr" aria-hidden="true"></i>'.$CartRow->price.'</strong>'.$slesh.''.$CartRow->attr_name.'</span>
                                </div>
                            </div>';

         };
          $CartValue .=  '<div class="btncenter">
                <button class="btn-std form-nav-prev btn-check" type="button">Prev</button>
                <input class="btn-std  btn-check" type="submit" id="form_submit_button" value="Proceed To Payment">
                   
            </div> 
          </div>
        </div>
         </form>
     </div>';
        }
        
        $countVal = $giftRes -> num_rows() + $cartQty + $SubcribRes -> num_rows();
        
        if($countVal > 0 ){
            $CartDisp = $GiftValue.$SubscribValue.$CartValue.'<div id="empty_msg" class="emptycartpage" style="display: none;">
          <img src="'. base_url() .'images/empty-cart-icn.svg" alt="Empaty cart">
          <h1>Your Cart is Empty</h1>
          <p>Looks like you have not chosen any product yet</p>
        </div>';
        }else {
            $CartDisp = '
             <div class="emptycartpage" style="padding-top:40px;">
          <img src="'. base_url() .'images/empty-cart-icn.svg" alt="Empaty cart">
          <h1>Your Cart is Empty</h1>
          <p>Looks like you have not chosen any product yet</p>
        </div>';
        }

        return $CartDisp;
    }

    public function mani_gift_total($userid = '') {

        $giftRes = $this->cart_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $userid));
        $giftAmt = 0;
        if ($giftRes->num_rows() > 0) {

            foreach ($giftRes->result() as $giftRow) {
                $giftAmt = $giftAmt + $giftRow->price_value;
            }
        }
        $SubcribRes = $this->cart_model->get_all_details(FANCYYBOX_TEMP, array('user_id' => $userid));
        $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $countVal = $giftRes->num_rows() + $SubcribRes->num_rows() + $cartVal->num_rows();

        return number_format($giftAmt, 2, '.', '') . '|' . $countVal;
    }

    public function mani_subcribe_total($userid = '') {

        $SubcribRes = $this->cart_model->get_all_details(FANCYYBOX_TEMP, array('user_id' => $userid));
        $SubcribAmt = 0;
        $SubcribSAmt = 0;
        $SubcribTAmt = 0;
        $SubcribTotalAmt = 0;
        if ($SubcribRes->num_rows() > 0) {

            foreach ($SubcribRes->result() as $SubscribRow) {
                $SubcribAmt = $SubcribAmt + $SubscribRow->price;
            }
            $SubcribSAmt = $SubcribRes->row()->shipping_cost;
            $SubcribTAmt = $SubcribRes->row()->tax;
            $SubcribTotalAmt = $SubcribAmt + $SubcribSAmt + $SubcribTAmt;
        }
        $giftRes = $this->cart_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $userid));
        $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $countVal = $SubcribRes->num_rows() + $giftRes->num_rows() + $cartVal->num_rows();

        return number_format($SubcribAmt, 2, '.', '') . '|' . number_format($SubcribSAmt, 2, '.', '') . '|' . number_format($SubcribTAmt, 2, '.', '') . '|' . number_format($SubcribTotalAmt, 2, '.', '') . '|' . $countVal;
    }

    public function mani_cart_total($userid = '') {

        $giftRes = $this->cart_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $userid));
        $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $SubcribRes = $this->cart_model->get_all_details(FANCYYBOX_TEMP, array('user_id' => $userid));
        $cartAmt = 0;
        $cartShippingAmt = 0;
        $cartTaxAmt = 0;
        $cartMiniMainCount = 0;
        $cartDiscAmt = 0;

        if ($cartVal->num_rows() > 0) {
            foreach ($cartVal->result() as $CartRow) {
                $qu = $this->db->query("SELECT maximumamount FROM " . COUPONCARDS . " WHERE id='" . $CartRow->couponID . "' LIMIT 1");
                $rr = $qu->result();
                $cartAmt = $cartAmt - ($CartRow->discountAmount * $CartRow->quantity) + ((($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
                $cartMiniMainCount = $cartMiniMainCount + $CartRow->quantity;
                $cartShippingAmt = $cartShippingAmt + ( $CartRow->product_shipping_cost * $CartRow->quantity );
                $cartTaxAmt = $cartTaxAmt + ($cartAmt * 0.01 * $CartRow->tax);
                $cartDiscAmt = $cartDiscAmt + ($CartRow->discountAmount * $CartRow->quantity);
            if ($rr[0]->maximumamount != 0) {
                If ($cartDiscAmt > $rr[0]->maximumamount) {
                    $cartAmt = $cartAmt + $cartDiscAmt - $rr[0]->maximumamount;
                    $cartDiscAmt = $rr[0]->maximumamount;
                }
            }
            }

            //$cartSAmt = $cartVal->row()->shipping_cost;
            //$cartTAmt = $cartAmt * 0.01 * $cartVal->row()->tax;
            $grantAmt = $cartAmt + $cartShippingAmt + $cartTaxAmt;
        }

        $countVal = $giftRes->num_rows() + $SubcribRes->num_rows() + $cartMiniMainCount;

        //$this->db->select('discountAmount');
        //$this->db->where('user_id',$userid);
        //$query = $this->db->get(SHOPPING_CART);
        //if($query->row()->discountAmount !=''){
        //	$grantAmt = $grantAmt - $query->row()->discountAmount;
        //}

        return number_format($cartAmt, 2, '.', '') . '|' . number_format($cartShippingAmt, 2, '.', '') . '|' . number_format($cartTaxAmt, 2, '.', '') . '|' . number_format($grantAmt, 2, '.', '') . '|' . $countVal . '|' . number_format($cartDiscAmt, 2, '.', '');
    }

    public function mani_cart_coupon_sucess($userid = '') {

        $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $cartAmt = 0;
        $cartShippingAmt = 0;
        $cartTaxAmt = 0;
        $cartDisc = 0;

        if ($cartVal->num_rows() > 0) {
            $k = 0;
            foreach ($cartVal->result() as $CartRow) {
                $qu = $this->db->query("SELECT maximumamount FROM " . COUPONCARDS . " WHERE id='" . $CartRow->couponID . "' LIMIT 1");
                $rr = $qu->result();
                $cartAmt = $cartAmt - ($CartRow->discountAmount * $CartRow->quantity) + (( ($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
                $newCartInd[] = $CartRow->indtotal;
                $cartDisc = $cartDisc + ($CartRow->discountAmount * $CartRow->quantity);
                $shippingCost = $shippingCost + ($CartRow->product_shipping_cost * $CartRow->quantity);
                $k = $k + 1;
                if ($rr[0]->maximumamount != 0) {
                    If ($cartDisc > $rr[0]->maximumamount) {
                        $cartAmt = $cartAmt + $cartDisc - $rr[0]->maximumamount;
                        $cartDisc = $rr[0]->maximumamount;
                    }
                }
            }
            $cartSAmt = $shippingCost;
            $cartTAmt = $cartAmt * 0.01 * $cartVal->row()->tax;
            $grantAmt = $cartAmt + $cartSAmt + $cartTAmt;
        }

        //$this->db->select('discountAmount');
        //$this->db->from(SHOPPING_CART);
        //$this->db->where('user_id = '.$userid);
        //$query = $this->db->get();
        $newAmtsValues = @implode('|', $newCartInd);


        //if($query->row()->discountAmount !=''){
        //	$grantAmt = $grantAmt - $query->row()->discountAmount;
        //}
        // $grantAmt = $grantAmt - $cartDisc;

        return number_format($cartAmt, 2, '.', '') . '|' . number_format($cartSAmt, 2, '.', '') . '|' . number_format($cartTAmt, 2, '.', '') . '|' . number_format($grantAmt, 2, '.', '') . '|' . number_format($cartDisc, 2, '.', '') . '|' . $k . '|' . $newAmtsValues;
    }

    public function view_cart_details($condition = '') {
        $select_qry = "select p.*,u.full_name,u.user_name,u.thumbnail from " . PRODUCT . " p LEFT JOIN " . USERS . " u on u.id=p.user_id " . $condition;
        $cartList = $this->ExecuteQuery($select_qry);
        return $cartList;
    }

    public function view_atrribute_details() {
        $select_qry = "select * from " . ATTRIBUTE . " where status='Active'";
        return $attList = $this->ExecuteQuery($select_qry);
    }

    public function Check_Code_Val($Code = '', $amount = '', $shipamount = '', $userid = '') {

        $code = $Code;
        $amount = $amount;
        $amountOrg = $amount;
        $ship_amount = $shipamount;

        $CoupRes = $this->cart_model->get_all_details(COUPONCARDS, array('code' => $code, 'card_status' => 'not used', 'status' => 'Active'));
        $GiftRes = $this->cart_model->get_all_details(GIFTCARDS, array('code' => $code));
        $ShopArr = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $excludeArr = array('code', 'amount', 'shipamount');


        if ($CoupRes->num_rows() > 0) {

            //$PayVal = $this->cart_model->get_all_details(PAYMENT,array( 'user_id' => $userid, 'coupon_id' => $CoupRes->row()->id, 'status'=>'Paid' ));
            //if($PayVal->num_rows() == 0){

            if ($ShopArr->row()->couponID == 0) {

                if ($CoupRes->row()->quantity > $CoupRes->row()->purchase_count) {

                    $today = getdate();
                    $tomorrow = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
                    $currDate = date("Y-d-m", $tomorrow);
                    $couponExpDate = $CoupRes->row()->dateto;

                    $curVal = (strtotime($couponExpDate) < time());
                    if ($curVal != '') {
                        echo '5';
                        exit();
                    }

                    if ($CoupRes->row()->coupon_type == "shipping") {
                        $totalamt = number_format($amount - $ship_amount, 2, '.', '');
                        $discount = '0';

                        $dataArr = array('discountAmount' => $discount,
                            'couponID' => $CoupRes->row()->id,
                            'couponCode' => $code,
                            'coupontype' => 'Free Shipping',
                            'is_coupon_used' => 'Yes',
                            'shipping_cost' => 0,
                            'total' => $totalamt);
                        $condition = array('user_id' => $userid);
                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                        echo 'Success|' . $CoupRes->row()->id . '|Shipping';
                        exit();
                    }elseif($CoupRes->row()->coupon_type == "category") {
                        $newAmt = $amount;
                        $catAry = @explode(',', $CoupRes->row()->category_id);
                        foreach ($ShopArr->result() as $shopRow) {
                            $shopCatArr = '';

                            $shopCatArr = @explode(',', $shopRow->cate_id);

                            $combArr = array_merge($catAry, $shopCatArr);
                            $combArr1 = array_unique($combArr);
                            if (count($combArr) != count($combArr1)) {

                                if ($CoupRes->row()->price_type == 2) {
                                    if ($CoupRes->row()->maxtenure != '' || $CoupRes->row()->mintenure != '') {
                                        $q = $this->db->query("SELECT attr_name FROM " . SUBPRODUCT . " WHERE pid='" . $shopRow->attribute_values . "' LIMIT 1");
                                        if ($q->num_rows() > 0) {
                                            $r = $q->result();
                                        }
                                        $ex = explode(" ", $r[0]->attr_name);
                                        $max = explode(" ", $CoupRes->row()->maxtenure);
                                        $min = explode(" ", $CoupRes->row()->mintenure);
                                        if ($max[0] >= $ex[0] || $min[0] <= $ex[0]) {
                                            $percentage = $CoupRes->row()->price_value;
                                            $amountOrg = $shopRow->indtotal;
                                            $prodPrice = $shopRow->price;
                                            $discount = ($percentage * 0.01) * $prodPrice;
                                            if ($CoupRes->row()->maximumamount != 0) {
                                                if ($discount > $CoupRes->row()->maximumamount) {
                                                    $discount = $CoupRes->row()->maximumamount;
                                                }
                                            }
                                            $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                            $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                            $dataArr = array('discountAmount' => $discount,
                                                'couponID' => $CoupRes->row()->id,
                                                'couponCode' => $code,
                                                'coupontype' => 'Category',
                                                'is_coupon_used' => 'Yes',
                                                'indtotal' => $IndAmt);
                                            $condition = array('id' => $shopRow->id);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);

                                            $dataArr1 = array('total' => $TotalAmt);
                                            $condition1 = array('user_id' => $userid);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                        } else {
                                            echo '7';
                                            exit();
                                        }
                                    } elseif ($CoupRes->row()->maxamountf != 0 || $CoupRes->row()->minamountf != 0) {
                                        $totalamtrespected = $newAmt - $ship_amount;
                                        if ($CoupRes->row()->maxamountf >= $totalamtrespected || $CoupRes->row()->minamountf <= $totalamtrespected) {
                                            $percentage = $CoupRes->row()->price_value;
                                            $amountOrg = $shopRow->indtotal;
                                            $prodPrice = $shopRow->price;
                                            $discount = ($percentage * 0.01) * $prodPrice;
                                            if ($CoupRes->row()->maximumamount != 0) {
                                                if ($discount > $CoupRes->row()->maximumamount) {
                                                    $discount = $CoupRes->row()->maximumamount;
                                                }
                                            }
                                            $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                            $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                            $dataArr = array('discountAmount' => $discount,
                                                'couponID' => $CoupRes->row()->id,
                                                'couponCode' => $code,
                                                'coupontype' => 'Category',
                                                'is_coupon_used' => 'Yes',
                                                'indtotal' => $IndAmt);
                                            $condition = array('id' => $shopRow->id);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);

                                            $dataArr1 = array('total' => $TotalAmt);
                                            $condition1 = array('user_id' => $userid);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                        } else {
                                            echo '6';
                                            exit();
                                        }
                                    } else {
                                        $percentage = $CoupRes->row()->price_value;
                                        $amountOrg = $shopRow->indtotal;
                                        $prodPrice = $shopRow->price;
                                        $discount = ($percentage * 0.01) * $prodPrice;
                                        if ($CoupRes->row()->maximumamount != 0) {
                                            if ($discount > $CoupRes->row()->maximumamount) {
                                                $discount = $CoupRes->row()->maximumamount;
                                            }
                                        }
                                        $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                        $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                        $dataArr = array('discountAmount' => $discount,
                                            'couponID' => $CoupRes->row()->id,
                                            'couponCode' => $code,
                                            'coupontype' => 'Category',
                                            'is_coupon_used' => 'Yes',
                                            'indtotal' => $IndAmt);
                                        $condition = array('id' => $shopRow->id);
                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);

                                        $dataArr1 = array('total' => $TotalAmt);
                                        $condition1 = array('user_id' => $userid);
                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                    }
                                } elseif ($CoupRes->row()->price_type == 1) {
                                    if ($CoupRes->row()->maxtenure != '' || $CoupRes->row()->mintenure != '') {
                                        $q = $this->db->query("SELECT attr_name FROM " . SUBPRODUCT . " WHERE pid='" . $shopRow->attribute_values . "' LIMIT 1");
                                        if ($q->num_rows() > 0) {
                                            $r = $q->result();
                                        }
                                        $ex = explode(" ", $r[0]->attr_name);
                                        $max = explode(" ", $CoupRes->row()->maxtenure);
                                        $min = explode(" ", $CoupRes->row()->mintenure);
                                        if ($max[0] >= $ex[0] && $min[0] <= $ex[0]) {
                                            $totalamtrespected = $newAmt - $ship_amount;
                                            if ($CoupRes->row()->minamountf <= $totalamtrespected) {
                                                $discount = $CoupRes->row()->price_value;
                                                $amountOrg = $shopRow->indtotal;
                                                if ($amountOrg > $discount) {
                                                    $amountOrg = number_format($amountOrg - $discount, 2, '.', '');
                                                    $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                                    $dataArr = array('discountAmount' => $discount,
                                                        'couponID' => $CoupRes->row()->id,
                                                        'couponCode' => $code,
                                                        'coupontype' => 'Category',
                                                        'is_coupon_used' => 'Yes',
                                                        'indtotal' => $amountOrg);
                                                    $condition = array('id' => $shopRow->id);
                                                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                                    $dataArr1 = array('total' => $TotalAmt);
                                                    $condition1 = array('user_id' => $userid);
                                                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                                } else {
                                                    echo '7';
                                                    exit();
                                                }
                                            } else {
                                                echo '7';
                                                exit();
                                            }
                                        } else {
                                            echo '7';
                                            exit();
                                        }
                                    }
                                }
                            }
                        }
                        echo 'Success|' . $CoupRes->row()->id . '|Category';
                        exit();
                    } elseif ($CoupRes->row()->coupon_type == "product") {
                        $PrdArr = @explode(',', $CoupRes->row()->product_id);
                        $newAmt = $amount;
                        foreach ($ShopArr->result() as $shopRow) {

                            $shopPrd = $shopRow->product_id;

                            if (in_array($shopPrd, $PrdArr) == 1) {

                                if ($CoupRes->row()->price_type == 2) {
                                    if ($CoupRes->row()->maxtenure != '' || $CoupRes->row()->mintenure != '') {
                                        $q = $this->db->query("SELECT attr_name FROM " . SUBPRODUCT . " WHERE pid='" . $shopRow->attribute_values . "' LIMIT 1");
                                        if ($q->num_rows() > 0) {
                                            $r = $q->result();
                                        }
                                        $ex = explode(" ", $r[0]->attr_name);
                                        $max = explode(" ", $CoupRes->row()->maxtenure);
                                        $min = explode(" ", $CoupRes->row()->mintenure);
                                        if ($max[0] >= $ex[0] && $min[0] <= $ex[0]) {
                                            if ($CoupRes->row()->maxamountf != 0 || $CoupRes->row()->minamountf != 0) {
                                                $totalamtrespected = $newAmt - $ship_amount;
                                                if ($CoupRes->row()->maxamountf >= $totalamtrespected || $CoupRes->row()->minamountf <= $totalamtrespected) {
                                                    $percentage = $CoupRes->row()->price_value;
                                                    $amountOrg = $shopRow->indtotal;
                                                    $prodPrice = $shopRow->price;
                                                    $discount = ($percentage * 0.01) * $prodPrice;
                                                    if ($CoupRes->row()->maximumamount != 0) {
                                                        //$discount = ($shopRow->quantity>1) ? ($discount * $shopRow->quantity) : $discount;
                                                        if ($discount > $CoupRes->row()->maximumamount) {
                                                            $discount = $CoupRes->row()->maximumamount;
                                                        }
                                                    }
                                                    $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                                    $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                                    $dataArr = array('discountAmount' => $discount,
                                                        'couponID' => $CoupRes->row()->id,
                                                        'couponCode' => $code,
                                                        'coupontype' => 'Product',
                                                        'is_coupon_used' => 'Yes',
                                                        'indtotal' => $IndAmt);
                                                    $condition = array('id' => $shopRow->id);

                                                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                                    $dataArr1 = array('total' => $TotalAmt);
                                                    $condition1 = array('user_id' => $userid);
                                                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                                } else {
                                                    echo '6';
                                                    exit();
                                                }
                                            } else {
                                                $percentage = $CoupRes->row()->price_value;
                                                $amountOrg = $shopRow->indtotal;
                                                $prodPrice = $shopRow->price;
                                                $discount = ($percentage * 0.01) * $prodPrice;
                                                if ($CoupRes->row()->maximumamount != 0) {
                                                    //$discount = ($shopRow->quantity>1) ? ($discount * $shopRow->quantity) : $discount;
                                                    if ($discount > $CoupRes->row()->maximumamount) {
                                                        $discount = $CoupRes->row()->maximumamount;
                                                    }
                                                }
                                                $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                                $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                                $dataArr = array('discountAmount' => $discount,
                                                    'couponID' => $CoupRes->row()->id,
                                                    'couponCode' => $code,
                                                    'coupontype' => 'Product',
                                                    'is_coupon_used' => 'Yes',
                                                    'indtotal' => $IndAmt);
                                                $condition = array('id' => $shopRow->id);

                                                $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                                $dataArr1 = array('total' => $TotalAmt);
                                                $condition1 = array('user_id' => $userid);
                                                $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                            }
                                        } else {
                                            echo '6';
                                            exit();
                                        }
                                    } elseif ($CoupRes->row()->maxamountf != 0 || $CoupRes->row()->minamountf != 0) {
                                        $totalamtrespected = $newAmt - $ship_amount;
                                        if ($CoupRes->row()->maxamountf >= $totalamtrespected || $CoupRes->row()->minamountf <= $totalamtrespected) {
                                            $percentage = $CoupRes->row()->price_value;
                                            $amountOrg = $shopRow->indtotal;
                                            $prodPrice = $shopRow->price;
                                            $discount = ($percentage * 0.01) * $prodPrice;
                                            if ($CoupRes->row()->maximumamount != 0) {
                                                //$discount = ($shopRow->quantity>1) ? ($discount * $shopRow->quantity) : $discount;
                                                if ($discount > $CoupRes->row()->maximumamount) {
                                                    $discount = $CoupRes->row()->maximumamount;
                                                }
                                            }
                                            $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                            $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                            $dataArr = array('discountAmount' => $discount,
                                                'couponID' => $CoupRes->row()->id,
                                                'couponCode' => $code,
                                                'coupontype' => 'Product',
                                                'is_coupon_used' => 'Yes',
                                                'indtotal' => $IndAmt);
                                            $condition = array('id' => $shopRow->id);

                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                            $dataArr1 = array('total' => $TotalAmt);
                                            $condition1 = array('user_id' => $userid);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                        } else {
                                            echo '6';
                                            exit();
                                        }
                                    } else {
                                        $percentage = $CoupRes->row()->price_value;
                                        $amountOrg = $shopRow->indtotal;
                                        $prodPrice = $shopRow->price;
                                        $discount = ($percentage * 0.01) * $prodPrice;
                                        if ($CoupRes->row()->maximumamount != 0) {
                                            //$discount = ($shopRow->quantity>1) ? ($discount * $shopRow->quantity) : $discount;
                                            if ($discount > $CoupRes->row()->maximumamount) {
                                                $discount = $CoupRes->row()->maximumamount;
                                            }
                                        }
                                        $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                        $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                        $dataArr = array('discountAmount' => $discount,
                                            'couponID' => $CoupRes->row()->id,
                                            'couponCode' => $code,
                                            'coupontype' => 'Product',
                                            'is_coupon_used' => 'Yes',
                                            'indtotal' => $IndAmt);
                                        $condition = array('id' => $shopRow->id);

                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                        $dataArr1 = array('total' => $TotalAmt);
                                        $condition1 = array('user_id' => $userid);
                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                    }
                                } elseif ($CoupRes->row()->price_type == 1) {
                                    if ($CoupRes->row()->maxtenure != '' || $CoupRes->row()->mintenure != '') {
                                        $q = $this->db->query("SELECT attr_name FROM " . SUBPRODUCT . " WHERE pid='" . $shopRow->attribute_values . "' LIMIT 1");
                                        if ($q->num_rows() > 0) {
                                            $r = $q->result();
                                        }
                                        $ex = explode(" ", $r[0]->attr_name);
                                        $max = explode(" ", $CoupRes->row()->maxtenure);
                                        $min = explode(" ", $CoupRes->row()->mintenure);
                                        if ($max[0] >= $ex[0] && $min[0] <= $ex[0]) {
                                            $totalamtrespected = $newAmt - $ship_amount;
                                            if ($CoupRes->row()->minamountf <= $totalamtrespected) {
                                                $discount = $CoupRes->row()->price_value;
                                                $amountOrg = $shopRow->indtotal;
                                                if ($amountOrg > $discount) {
                                                    $amountOrg = number_format($amountOrg - $discount, 2, '.', '');
                                                    $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                                    $dataArr = array('discountAmount' => $discount,
                                                        'couponID' => $CoupRes->row()->id,
                                                        'couponCode' => $code,
                                                        'coupontype' => 'Category',
                                                        'is_coupon_used' => 'Yes',
                                                        'indtotal' => $amountOrg);
                                                    $condition = array('id' => $shopRow->id);
                                                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                                    $dataArr1 = array('total' => $TotalAmt);
                                                    $condition1 = array('user_id' => $userid);
                                                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                                } else {
                                                    echo '7';
                                                    exit();
                                                }
                                            } else {
                                                echo '7';
                                                exit();
                                            }
                                        } else {
                                            echo '7';
                                            exit();
                                        }
                                    }
                                }
                            }
                        }
                        echo 'Success|' . $CoupRes->row()->id . '|Product';
                        exit();
                    } else if ($CoupRes->row()->coupon_type == "seller" || $CoupRes->row()->coupon_type == "exclusive") {
                        $coupUserIdArr = @explode(',', $CoupRes->row()->user_id);
                        $newAmt = $amount;
                        foreach ($ShopArr->result() as $shopRow) {
                            $sellerId = $shopRow->sell_id;
                            if (in_array($sellerId, $coupUserIdArr) == 1) {
                                if ($CoupRes->row()->price_type == 2) {
                                    if ($CoupRes->row()->maxtenure != '' || $CoupRes->row()->mintenure != '') {
                                        $q = $this->db->query("SELECT attr_name FROM " . SUBPRODUCT . " WHERE pid='" . $shopRow->attribute_values . "' LIMIT 1");
                                        if ($q->num_rows() > 0) {
                                            $r = $q->result();
                                        }
                                        $ex = explode(" ", $r[0]->attr_name);
                                        $max = explode(" ", $CoupRes->row()->maxtenure);
                                        $min = explode(" ", $CoupRes->row()->mintenure);
                                        if ($max[0] >= $ex[0] || $min[0] <= $ex[0]) {
                                            $percentage = $CoupRes->row()->price_value;
                                            $amountOrg = $shopRow->indtotal;
                                            $prodPrice = $shopRow->price;
                                            $discount = ($percentage * 0.01) * $prodPrice;
                                            if ($CoupRes->row()->maximumamount != 0) {
                                                if ($discount > $CoupRes->row()->maximumamount) {
                                                    $discount = $CoupRes->row()->maximumamount;
                                                }
                                            }
                                            $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                            $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                            $dataArr = array('discountAmount' => $discount,
                                                'couponID' => $CoupRes->row()->id,
                                                'couponCode' => $code,
                                                'coupontype' => 'Seller',
                                                'is_coupon_used' => 'Yes',
                                                'indtotal' => $IndAmt);
                                            $condition = array('id' => $shopRow->id);

                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                            $dataArr1 = array('total' => $TotalAmt);
                                            $condition1 = array('user_id' => $userid);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                        } else {
                                            echo '7';
                                            exit();
                                        }
                                    } elseif ($CoupRes->row()->maxamountf != 0 || $CoupRes->row()->minamountf != 0) {
                                        $totalamtrespected = $newAmt - $ship_amount;
                                        if ($CoupRes->row()->maxamountf >= $totalamtrespected || $CoupRes->row()->minamountf <= $totalamtrespected) {
                                            $percentage = $CoupRes->row()->price_value;
                                            $amountOrg = $shopRow->indtotal;
                                            $prodPrice = $shopRow->price;
                                            $discount = ($percentage * 0.01) * $prodPrice;
                                            if ($CoupRes->row()->maximumamount != 0) {
                                                if ($discount > $CoupRes->row()->maximumamount) {
                                                    $discount = $CoupRes->row()->maximumamount;
                                                }
                                            }
                                            $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                            $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                            $dataArr = array('discountAmount' => $discount,
                                                'couponID' => $CoupRes->row()->id,
                                                'couponCode' => $code,
                                                'coupontype' => 'Seller',
                                                'is_coupon_used' => 'Yes',
                                                'indtotal' => $IndAmt);
                                            $condition = array('id' => $shopRow->id);

                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                            $dataArr1 = array('total' => $TotalAmt);
                                            $condition1 = array('user_id' => $userid);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                        } else {
                                            echo '6';
                                            exit();
                                        }
                                    } else {
                                        $percentage = $CoupRes->row()->price_value;
                                        $amountOrg = $shopRow->indtotal;
                                        $prodPrice = $shopRow->price;
                                        $discount = ($percentage * 0.01) * $prodPrice;
                                        if ($CoupRes->row()->maximumamount != 0) {
                                            if ($discount > $CoupRes->row()->maximumamount) {
                                                $discount = $CoupRes->row()->maximumamount;
                                            }
                                        }
                                        $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                        $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                        $dataArr = array('discountAmount' => $discount,
                                            'couponID' => $CoupRes->row()->id,
                                            'couponCode' => $code,
                                            'coupontype' => 'Seller',
                                            'is_coupon_used' => 'Yes',
                                            'indtotal' => $IndAmt);
                                        $condition = array('id' => $shopRow->id);

                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                        $dataArr1 = array('total' => $TotalAmt);
                                        $condition1 = array('user_id' => $userid);
                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                    }
                                } elseif ($CoupRes->row()->price_type == 1) {

                                    $discount = $CoupRes->row()->price_value;
                                    $amountOrg = $shopRow->indtotal;
                                    if ($amountOrg > $discount) {
                                        $newDisAmt = number_format($amountOrg - $discount, 2, '.', '');
                                        $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                        $dataArr = array('discountAmount' => $discount,
                                            'couponID' => $CoupRes->row()->id,
                                            'couponCode' => $code,
                                            'coupontype' => 'Seller',
                                            'is_coupon_used' => 'Yes',
                                            'indtotal' => $newDisAmt);

                                        $condition = array('id' => $shopRow->id);
                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                        $dataArr1 = array('total' => $TotalAmt);
                                        $condition1 = array('user_id' => $userid);
                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                    } else {
                                        echo '7';
                                        exit();
                                    }
                                }
                            }
                        }
                        echo 'Success|' . $CoupRes->row()->id . '|Seller';
                        exit();
                    } else {

                        if ($CoupRes->row()->price_type == 2) {
                            if ($CoupRes->row()->maxtenure != '' || $CoupRes->row()->mintenure != '') {
                                $shopRow = $ShopArr->result();
                                $q = $this->db->query("SELECT attr_name FROM " . SUBPRODUCT . " WHERE pid='" . $shopRow[0]->attribute_values . "' LIMIT 1");
                                if ($q->num_rows() > 0) {
                                    $r = $q->result();
                                }
                                $ex = explode(" ", $r[0]->attr_name);
                                $max = explode(" ", $CoupRes->row()->maxtenure);
                                $min = explode(" ", $CoupRes->row()->mintenure);
                                if ($max[0] >= $ex[0] || $min[0] <= $ex[0]) {
                                    $percentage = $CoupRes->row()->price_value;
                                    $discount = ($percentage * 0.01) * $amount;
                                    if ($discount > $CoupRes->row()->maximumamount) {
                                        $discount = $CoupRes->row()->maximumamount;
                                    }
                                    $totalAmt = number_format($amount - $discount, 2, '.', '');

                                    $dataArr = array('discountAmount' => $discount,
                                        'couponID' => $CoupRes->row()->id,
                                        'couponCode' => $code,
                                        'coupontype' => 'Cart',
                                        'is_coupon_used' => 'Yes',
                                        'total' => $totalAmt);
                                    $condition = array('user_id' => $userid);

                                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);

                                    echo 'Success|' . $CoupRes->row()->id;
                                    exit();
                                } else {
                                    echo '8';
                                    exit();
                                }
                            } elseif ($CoupRes->row()->maxamountf != 0 || $CoupRes->row()->minamountf != 0) {
                                $totalamtrespected = $amount - $ship_amount;
                                if ($CoupRes->row()->maxamountf >= $totalamtrespected || $CoupRes->row()->minamountf <= $totalamtrespected) {
                                    $percentage = $CoupRes->row()->price_value;
                                    $discount = ($percentage * 0.01) * $amount;
                                    if ($discount > $CoupRes->row()->maximumamount) {
                                        $discount = $CoupRes->row()->maximumamount;
                                    }
                                    $totalAmt = number_format($amount - $discount, 2, '.', '');

                                    $dataArr = array('discountAmount' => $discount,
                                        'couponID' => $CoupRes->row()->id,
                                        'couponCode' => $code,
                                        'coupontype' => 'Cart',
                                        'is_coupon_used' => 'Yes',
                                        'total' => $totalAmt);
                                    $condition = array('user_id' => $userid);

                                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);

                                    echo 'Success|' . $CoupRes->row()->id;
                                    exit();
                                } else {
                                    echo '6';
                                    exit();
                                }
                            } else {
                                
                                $percentage = $CoupRes->row()->price_value;
                                
                                $discount = ($percentage * 0.01) * $amount;
                                
                                $totalAmt = number_format($amount - $discount, 2, '.', '');

                                $dataArr = array('discountAmount' => $discount,
                                    'couponID' => $CoupRes->row()->id,
                                    'couponCode' => $code,
                                    'coupontype' => 'Cart',
                                    'is_coupon_used' => 'Yes',
                                    'total' => $totalAmt);
                                $condition = array('user_id' => $userid);

                                $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);

                                /* foreach($ShopArr->result() as $shopRow){
                                  $amountOrg = $shopRow->indtotal;
                                  $discount = ($percentage * 0.01) * $amountOrg;
                                  $IndAmt = number_format($amountOrg - $discount,2,'.','');

                                  $dataArr = array('indtotal' => $IndAmt);
                                  $condition =array('id' => $shopRow->id);
                                  $this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr,$condition);
                                  } */

                                echo 'Success|' . $CoupRes->row()->id;
                                exit();
                            }
                        } elseif ($CoupRes->row()->price_type == 1) {

                            $discount = $CoupRes->row()->price_value;
                            if ($amount > $discount) {
                                $amountOrg = number_format($amount - $discount, 2, '.', '');
                                $dataArr = array('discountAmount' => $discount,
                                    'couponID' => $CoupRes->row()->id,
                                    'couponCode' => $code,
                                    'coupontype' => 'Cart',
                                    'is_coupon_used' => 'Yes',
                                    'total' => $amountOrg);
                                $condition = array('user_id' => $userid);
                                
                                $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                /* $newDisAmt = ($discount / $ShopArr->num_rows());
                                  foreach($ShopArr->result() as $shopRow){
                                  $amountOrg = $shopRow->indtotal;
                                  $IndAmt = number_format($amountOrg - $newDisAmt,2,'.','');
                                  $dataArr = array('indtotal' => $IndAmt);
                                  $condition =array('id' => $shopRow->id);
                                  $this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr,$condition);
                                  } */


                                echo 'Success|' . $CoupRes->row()->id;
                                exit();
                            } else {
                                echo '7';
                                exit();
                            }
                        }
                    }
                } else {
                    echo '6';
                    exit();
                }
            } else {
                echo '2';
                exit();
            }


            //}else{
            //	echo '2';
            //	exit();
            //}
        } elseif ($GiftRes->num_rows() > 0) {

            $curGiftVal = (strtotime($GiftRes->row()->expiry_date) < time());
            if ($curGiftVal != '') {
                echo '8';
                exit();
            }

            if ($GiftRes->row()->price_value > $GiftRes->row()->used_amount) {

                $NewGiftAmt = $GiftRes->row()->price_value - $GiftRes->row()->used_amount;
                if ($amount > $NewGiftAmt) {
                    $amountOrg = $amountOrg - $NewGiftAmt;

                    $dataArr = array('discountAmount' => $NewGiftAmt,
                        'couponID' => $GiftRes->row()->id,
                        'couponCode' => $code,
                        'coupontype' => 'Gift',
                        'is_coupon_used' => 'Yes',
                        'total' => $amountOrg);
                    $condition = array('user_id' => $userid);
                    $this->cart_model->update_details(SHOPPING_CART, $dataArr, $condition);



                    /* $newDisAmt = ($NewGiftAmt / $ShopArr->num_rows());
                      //echo '<pre>'; print_r($ShopArr->result_array());
                      foreach($ShopArr->result() as $shopRow){
                      $IndAmt = number_format($shopRow->indtotal - $newDisAmt,2,'.','');
                      $dataArr = array('indtotal' => $IndAmt);
                      $condition =array('id' => $shopRow->id);
                      $this->cart_model->update_details(SHOPPING_CART,$dataArr,$condition);
                      //echo '<pre>'.$this->db->last_query();
                      } */
                } else {
                    $dataArr = array('discountAmount' => $amountOrg,
                        'couponID' => $GiftRes->row()->id,
                        'couponCode' => $code,
                        'coupontype' => 'Gift',
                        'is_coupon_used' => 'Yes',
                        'total' => '0');
                    $condition = array('user_id' => $userid);
                    $this->cart_model->update_details(SHOPPING_CART, $dataArr, $condition);

                    /* $newDisAmt = ($amountOrg / $ShopArr->num_rows());

                      foreach($ShopArr->result() as $shopRow){
                      $amountOrg = $shopRow->indtotal;
                      $IndAmt = number_format($amountOrg - $newDisAmt,2,'.','');
                      $dataArr = array('indtotal' => '0');
                      $condition =array('id' => $shopRow->id);
                      $this->cart_model->update_details(SHOPPING_CART,$dataArr,$condition);
                      } */
                }

                echo 'Success|' . $GiftRes->row()->id . '|Gift';
                exit();
            } else {
                echo '2';
                exit();
            }
        } else {
            echo '1';
            exit();
        }
    }

    public function Check_Code_Val_Remove($userid = '') {

        $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $cartAmt = 0;
        if ($cartVal->num_rows() > 0) {
            foreach ($cartVal->result() as $CartRow) {
                $cartAmt = $cartAmt + (($CartRow->product_shipping_cost + ($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
            }
            $cartVal1 = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
            $excludeArr = array('code');
            foreach ($cartVal1->result() as $CartRow1) {
                $dataArr = array('discountAmount' => 0,
                    'couponID' => 0,
                    'indtotal' => ($CartRow1->price + $CartRow1->product_shipping_cost) * $CartRow1->quantity,
                    'total' => $cartAmt,
                    'couponCode' => '',
                    'coupontype' => '',
                    'is_coupon_used' => 'No');
                $condition = array('id' => $CartRow1->id);
                $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
            }
            return;
        }
    }

    public function addPaymentCart($userid = '') {


        $this->db->select('a.*,b.city,b.state,b.country,b.postal_code');
        $this->db->from(SHOPPING_CART . ' as a');
        $this->db->join(SHIPPING_ADDRESS . ' as b', 'a.user_id = b.user_id and a.user_id = "' . $userid . '" and b.id="' . $this->input->post('Ship_address_val') . '"');
        $AddPayt = $this->db->get();


        if ($this->session->userdata('randomNo') != '') {
            $delete = 'delete from ' . PAYMENT . ' where dealCodeNumber = "' . $this->session->userdata('randomNo') . '" and user_id = "' . $userid . '" ';
            $this->ExecuteQuery($delete, 'delete');
            $dealCodeNumber = $this->session->userdata('randomNo');
        } else {
            $dealCodeNumber = mt_rand();
        }

        $insertIds = array();
        foreach ($AddPayt->result() as $result) {

            if ($this->input->post('is_gift') == '') {
                $ordergift = 0;
            } else {
                $ordergift = 1;
            }

            $sumTotal = number_format((($result->price + $result->product_shipping_cost + ($result->product_tax_cost * 0.01 * $result->price)) * $result->quantity), 2, '.', '');

            $uncleanNote = $this->input->post('note');
            $cleanNote = mysqli_escape_string($uncleanNote);

            $insert = ' insert into ' . PAYMENT . ' set
								product_id = "' . $result->product_id . '",
								sell_id = "' . $result->sell_id . '",								
								price = "' . $result->price . '",
								quantity = "' . $result->quantity . '",
								indtotal = "' . $result->indtotal . '",
								shippingcountry = "' . $result->country . '",
								shippingid = "' . $this->input->post('Ship_address_val') . '",
								shippingstate = "' . $result->state . '",
								shippingcity = "' . $result->city . '",
								shippingcost = "' . $this->input->post('cart_ship_amount') . '",
								tax = "' . $this->input->post('cart_tax_amount') . '",
								product_shipping_cost = "' . $result->product_shipping_cost . '",
								product_tax_cost = "' . $result->product_tax_cost . '",																												
								coupon_id  = "' . $result->couponID . '",
								discountAmount = "' . $this->input->post('discount_Amt') . '",
								couponCode  = "' . $result->couponCode . '",
								coupontype = "' . $result->coupontype . '",
								sumtotal = "' . $sumTotal . '",
								user_id = "' . $result->user_id . '",
								created = now(),
								dealCodeNumber = "' . $dealCodeNumber . '",
								status = "Pending",
								payment_type = "",
								attribute_values = "' . $result->attribute_values . '",
								shipping_status = "Pending",
								total  = "' . $this->input->post('cart_total_amount') . '", 
								note = "' . $cleanNote . '", 
								order_gift = "' . $ordergift . '", 
								inserttime = "' . time() . '"';

            $insertIds[] = $this->cart_model->ExecuteQuery($insert, 'insert');
        }

        $query = $this->db->query("SELECT full_name,email FROM " . USERS . " WHERE id='" . $result->user_id . "' LIMIT 1");
        $userdet = $query->result();
        $paymtdata = array(
            'randomNo' => $dealCodeNumber,
            'randomIds' => $insertIds,
            'sessprice' => $sumTotal,
            'cartusername' => $userdet[0]->full_name,
            'cartuseremail' => $userdet[0]->email,
            'cartuserid' => $result->user_id
        );
        $this->session->set_userdata($paymtdata);

        return $insertIds;
    }

    public function addPaymentSubscribe($userid = '') {

        if ($this->session->userdata('InvoiceNo') != '') {
            $InvoiceNo = $this->session->userdata('InvoiceNo');
        } else {
            $InvoiceNo = mt_rand();
        }

        $paymtdata = array('InvoiceNo' => $InvoiceNo);
        $this->session->set_userdata($paymtdata);

        $dataArr = array('invoice_no' => $InvoiceNo,
            'shipping_id' => $this->input->post('SubShip_address_val'),
            'shipping_cost' => $this->input->post('subcrib_ship_amount'),
            'tax' => $this->input->post('subcrib_tax_amount'),
            'total' => $this->input->post('subcrib_total_amount'),
        );
        $condition = array('user_id' => $userid);
        $this->cart_model->update_details(FANCYYBOX_TEMP, $dataArr, $condition);


        return;
    }

    public function check_is_addon_prod($product_id) {
        $this->db->select('id,addons,is_addon');
        $this->db->from(PRODUCT);
        $this->db->where('status', 'Publish');
        $this->db->where('id', $product_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_price($product_id)
    {
       $this->db->select('*');
       $this->db->from(SUBPRODUCT);
       $this->db->where('product_id',$product_id);
       $this->db->where('attr_name','24 Months');
       $query = $this->db->get();
       return $query->result()[0];
    }
    public function get_ship_value($id)
    {
        $this->db->select('*');
        $this->db->from('fc_shipping_address');
        $this->db->where('id',$id);
        $data = $this->db->get();
        return $data->result()[0];
    }
    public function get_coupon_value($id)
    {
        $this->db->select('couponCode');
        $this->db->from('fc_shopping_carts');
        $this->db->where('user_id',$id);
        $this->db->where('couponCode != ""');
        $data = $this->db->get();
        return $data->row()->couponCode;
    }
}

?>
