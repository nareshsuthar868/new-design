<?php $this->load->view('site/templates/header_inner'); ?>
  <script type="text/javascript">
        $(function(){
          $('#mobile_footer').css('display','none');
          $('textarea').each(function(){
                  $(this).val($(this).val().trim());
              }
          );
        });
</script>
<style>
    .sizecol strong.qutycoldiv {
    display: inline-block;
    margin-right: 10px;
}
</style>

	<div class="page-wrapper flex-full">
		<!-- Breadcurmbs starts -->
		<div class="breadcrumbs flex-full">
			<div class="container">
				<ul class="breadcrumbs-listing flex-full align-items-center">
					<li class="root-link"><a href="<?php echo BASE_URL?>">Home</a></li>
					<li class="child-link"><a href="javascript:void(0)">Home furniture</a></li>
					<li class="child-link"><a href="javascript:void(0)">Living room</a></li>
					<li class="current-link"><a href="javascript:void(0)"><?php echo $productDetails->row()->product_name;?></a></li>
				</ul>
			</div>
		</div>
		<!-- Breadcurmbs ends -->
		<!-- Product detail main starts -->
		<section class="product-detail-main flex-full">
			<div class="container">
				<div class="product-detail-main-wrapper flex-full align-items-start align-content-start">
					<div class="product-slider-wrapper flex-full align-items-start align-content-start">
						<!-- product thumbnail slider starts -->
						<div class="product-slider flex-full">
							<div class="slider-for w-100">
							    <?php
							        $productImgs = explode(',', $productDetails->row()->image);
							        foreach ($productImgs as $productImage){
							            if(!empty($productImage)){
							    ?>
							            <div class="slide">
									        <amp-img src="<?php echo CDN_URL; ?>images/product/<?php echo $productImage; ?>" alt="Product Image" width="800" height="600"></amp-img>
								        </div>
							    <?php
							            }
							        }
							    ?>
							</div>
							<div class="slider-nav w-100 d-none-xs">
								<?php
							        foreach ($productImgs as $productImage){
							            if(!empty($productImage)){
							    ?>
							            <div class="slide">
									        <amp-img src="<?php echo CDN_URL; ?>images/product/<?php echo $productImage; ?>" alt="Product Image" width="125" height="94"></amp-img>
								        </div>
							    <?php
							            }
							        }
							    ?>
							</div>
						</div>
						<!-- product thumbnail slider ends -->
						<input type="hidden" id="user_id" value="<?php if(isset($_SESSION['fc_session_user_id'])){ echo $_SESSION['fc_session_user_id']; } ?>">
						<!-- product main info starts -->
						<div class="product-main-info flex-full">
							<h1><?php echo $productDetails->row()->product_name;?> <a href="javascript:void(0)" class="d-none-xs"><i class="icn icn-share"></i></a></h1>
							<!-- Range slider (for mobile) starts -->
							<?php
							    $tenureArray = array();
								$sliderData = array();
                                foreach (array_reverse($PrdAttrVal->result_array()) as $tenureData) {
                                    $tenureArray[(int)$tenureData['attr_name']]['price'] = $tenureData['attr_price'];
                                    $tenureArray[(int)$tenureData['attr_name']]['pid'] = $tenureData['pid'];
                                    $tenureArray[(int)$tenureData['attr_name']]['attr_name'] = $tenureData['attr_name'];
                                    $sliderData[] = (int)$tenureData['attr_name'];
                                }
                                $cat_id = explode(',', $productDetails->row()->category_id);
                                sort($sliderData);
							?>
							<div class="w-100 range-slider d-none d-block-xs">
							    <input type="text" id="tenureslider" class="js-range-slider" name="my_range" value="<?php
                           if(in_array(288,$cat_id)){  echo min(array_keys($tenureArray)); } else {  echo max(array_keys($tenureArray)); }  ?>"
							        data-type="single"
							        data-min="6"
							        data-max="36"  
							        data-grid="true"
							    />   
							</div>
							<!-- Range slider (for mobile) ends -->
							<div class="product-duration flex-full justify-content-center text-center position-relative d-none-xs">
								<label>Choose your Duration</label>
								<span class="duration-box"><p id="selectedTenure"><?php echo end($tenureArray)['attr_name'] ?></p><i class="icn-arrow-bottom"></i></span>
								<ul class="duration-dropdown flex-full">
									<li>
										
										<span class="dropdown-heading">Duration</span>
										<span class="dropdown-heading"><?php echo ucfirst(substr(explode(" ", end($tenureArray)['attr_name'])[1], 0, -1)).'ly' ?> Rent</span>
									</li>
									<?php
									    if(!empty($tenureArray)){
									        foreach($tenureArray as $monthKey => $rentValue){
									?>
									            <li onchange="setmyTenure('<?php echo $rentValue['attr_name'] ?>','<?php echo $rentValue['pid'] ?>')">
            										<input type="radio" name="duration-radio" id="<?php echo strtolower(explode(" ", $rentValue['attr_name'])[1]) ?>-<?php echo explode(" ", $rentValue['attr_name'])[0] ?>">
            										<label for="<?php echo strtolower(explode(" ", $rentValue['attr_name'])[1]) ?>-<?php echo explode(" ", $rentValue['attr_name'])[0] ?>">
            											<span><?php echo $rentValue['attr_name'] ?></span>
            											<span>&#x20B9; <?php echo $rentValue['price'] ?> <small>/ <?php echo strtolower(explode(" ", $rentValue['attr_name'])[1]) ?></small></span>
            										</label>
            									</li>
									<?php
									        }
									    }
									?>									
								</ul>
							</div>
							<!-- product price starts -->
							<div class="product-price-info flex-full justify-content-center text-center align-items-start align-content-start">
								<div class="product-price flex-full justify-content-center text-center">
									<label><?php echo ucfirst(substr(explode(" ", end($tenureArray)['attr_name'])[1], 0, -1)).'ly' ?> Rent <small>(Inclusive of Taxes)</small></label>
									<span class="price" id="tenure_price">&#x20B9; <?php echo $productDetails->row()->sale_price; ?></span>
								</div>
								<div class="refundable-price flex-full justify-content-center text-center">
									<label>Refundable Deposit</label>
									<span class="refundable-deposit">&#x20B9; <?php echo $productDetails->row()->shipping_cost; ?></span>
								</div>
							</div>
							<!-- product price ends -->
							<!-- product buttons starts -->
							<div class="product-actions flex-full justify-content-center text-center d-none-xs">
								<span class="free-delivery">Free Delivery and Setup in <?php
								    switch($productDetails->row()->shipping){
								        case '1':
								            echo stripslashes($this->lang->line('shipping_in_option_first'));
								            break;
								        case '2':
								            echo stripslashes($this->lang->line('shipping_in_option_second'));
								            break;
								        case '3':
								            echo stripslashes($this->lang->line('shipping_in_option_third'));
								            break;
								        case '4':
								            echo stripslashes($this->lang->line('shipping_in_option_fourth'));
								            break;
								        case '5':
								            echo stripslashes($this->lang->line('shipping_in_option_fifth'));
								            break;
								        case '6':
								            echo stripslashes($this->lang->line('shipping_in_option_sixth'));
								            break;
								        case '7':
								            echo stripslashes($this->lang->line('shipping_in_option_hours'));
								            break;
								        default:
								            echo '72 hours';
								    }
								?></span>
								<?php
                                          $quantity = (isset($_SESSION['prcity']) && $_SESSION['prcity'] != '') ? $cityQuantity[$_SESSION['prcity']] : '0';
                                        ?>
								<div class="product-btns flex-full justify-content-center text-center">
									<?php
										if($breadcrump->seourl != 'office-furniture-rental' && $productDetails->row()->category_id != "274"){
								        if($loginCheck != ''){
								            if($quantity > 0){
								    ?>
								                <a href="javascript:void(0)" class="addtocart product-btn" onclick="ajax_add_cart('<?php echo $PrdAttrVal->num_rows(); ?>');">Add to Cart</a>
								            
								            <?php if($checkProductLike->num_rows() == 1){ ?>
								             <a href="javascript:void(0);" class="addtowishlist product-btn" onclick="addtowhishlist('<?php echo $productDetails->row()->id ?>')">Remove From Wishlist</a>
								            <?php } else{ ?>
    								            <a href="javascript:void(0);" class="addtowishlist product-btn" onclick="addtowhishlist('<?php echo $productDetails->row()->id ?>')">Add to Wishlist</a>
								            <?php }?>
								    <?php
								            }else{
								    ?>
								                <a href="javascript:void(0)" class="addtocart product-btn addCartDisable">Out of Stock</a>
										<!-- <a href="javascript:void(0)" class="addtowishlist product-btn">Add to Wishlist</a>-->
										<?php  if($loginCheck != '')
                                         {
                                        ?>
										 <a href="javascript:void(0)" class="addtowishlist product-btn" id="notifyEnable" onclick="notifyMe('<?php echo $tenureData['product_id']; ?>','<?php echo $loginCheck;?>','<?php echo $_SESSION['prcity'];?>');">
										 Notify Me</a>
										 
										 <a href="javascript:void(0)" class="addtowishlist product-btn addCartDisable" id="notifyDisable" style="display:none;">
										 Notify Me</a>
										
										 
										 
										 
									<?php } 
									else
									{
									    ?>
										  <a href="<?php echo base_url() ?>user_sign_up" class="addtowishlist product-btn" >Notify Me1</a>
								    <?php
									}
								        }
								    ?>
								    <?php
								        }else{
								            if($quantity > 0){
								    ?>
								                <a href="<?php echo base_url() ?>user_sign_up" class="addtocart product-btn">Add to Cart</a>
								            
								            <a href="<?php echo base_url() ?>user_sign_up" class="addtowishlist product-btn">Add to Wishlist</a>
								    <?php
								            }else{
								    ?>
								                <a href="<?php echo base_url() ?>user_sign_up" class="addtocart product-btn addCartDisable">Out of Stock</a>
								            
								            <a href="<?php echo base_url() ?>user_sign_up" class="addtowishlist product-btn">
										 Notify Me</a>
								    <?php
								            }
								    
										}
									}else{
										if($loginCheck != ''){
									?>
											<a href="javascript:void(0);" id="enquire-now" class="addtowishlist product-btn">
											ENQUIRE NOW</a>
									<?php
										}else{
									?>
											<a href="<?php echo base_url() ?>user_sign_up" class="addtowishlist product-btn">
											ENQUIRE NOW</a>
									<?php
										}
									}
								    ?>
									
									  
									
									<input type="hidden" name="product_id" id="product_id" value="<?php echo $productDetails->row()->id;?>">
                                    <input type="hidden" name="cateory_id" id="cateory_id" value="<?php echo $productDetails->row()->category_id;?>">                
                                    <input type="hidden" name="sell_id" id="sell_id" value="<?php echo $productDetails->row()->user_id;?>">
                                    <input type="hidden" name="price" id="price" value="<?php echo $productDetails->row()->sale_price;?>">
                                    <input type="hidden" name="product_shipping_cost" id="product_shipping_cost" value="<?php echo $productDetails->row()->shipping_cost;?>"> 
                                    <input type="hidden" name="product_tax_cost" id="product_tax_cost" value="<?php echo $productDetails->row()->tax_cost;?>">
                                    <input type="hidden" name="attribute_values" id="attribute_values" value="<?php echo $attrValsSetLoad; ?>">
                                    <input type="hidden" name="attr_name_id" id="attr_name_id" value="<?php echo $tenureArray[24]['pid'] ?>">
                                    <input type="hidden" id="product_name" value="<?php echo $productDetails->row()->product_name;?>">
                                   <input type="hidden" name="quant[2]" id="quantity" data-city="<?php echo (isset($_SESSION['prcity'])) ? $_SESSION['prcity'] : ''; ?>" data-mqty="<?php echo $quantity; ?>" class="form-control input-number" value="1" min="1" max="<?php echo $quantity; ?>">                             
                                   <input type="hidden" id="selected_tenure"  value="<?php echo explode(' ', end($tenureArray)['attr_name'])[0] ?>">
								</div>
							</div>
							<!-- product buttons ends -->
							<!-- coupon box starts -->
							<div class="coupon-box flex-full justify-content-center text-center">
							    <?php foreach($site_offers->result() as $key => $value){
							        $firstline = unserialize($value->description);
							    ?>
								<p><span><?php echo $firstline[0]; ?>* -</span>Coupon code : <strong><?php echo $value->coupon_code; ?></strong></p>
							    <?php if($key == 1){ break; } }?>
								<p><i class="icn icn-percentage"></i><a href="<?php echo base_url() ?>pages/offers" target="_BLANK">More Offers</a><i class="icn-arrow-right"></i></p>
							</div>
							<!-- coupon box ends -->
							<!-- product specs tabbing (for mobile) starts -->
							<div class="product-specs-tabs flex-full d-none d-block-xs">
								<ul class="tabs">
									<li class="tab-link current" data-tab="tab-1">Specifications</li>
									<li class="tab-link" data-tab="tab-2">Features</li>				
								</ul>
								<div id="tab-1" class="tab-content current">
									<ul class="items-info-listing flex-full">
										<li>
											<h4><i class="icn icn-brand"></i> Brand</h4>
											<p>LG / Samsung / Whirpool / Phillips </p>
										</li>
										<li>
											<h4><i class="icn icn-brand"></i> Dimensions</h4>
											<p>93 cm (w) x 74 cm (h) </p>
										</li>
										<li>
											<h4><i class="icn icn-brand"></i> Material</h4>
											<p>Wooden and Fabric</p>
										</li>
										<li>
											<h4><i class="icn icn-brand"></i> Colour</h4>
											<p>Grey</p>
										</li>
									</ul>
								</div>
								<div id="tab-2" class="tab-content">
									<ul class="features-listing flex-full">
										<li><span>Cushioned seat</span></li>
										<li><span>Adjustable height & backrest</span></li>
										<li><span>Wheeled base</span></li>
										<li><span>Metal frame</span></li>
										<li><span>Handle Shape may vary</span></li>
									</ul>
								</div>
							</div>
							<!-- product specs tabbing (for mobile) ends -->
							<?php
							    if(count($subProductsArray) > 0 ){
							?>
							    <!-- items included slider (for mobile) starts -->
    							<div class="items-included-slider-section flex-full d-none d-block-xs">
    								<h2 class="product-detail-heading">Items Included (<?php echo count(unserialize($productDetails->row()->subproduct_quantity)) ?> items)</h2>
    								<div class="items-included-slider">
    								    <?php
        								    $product_quantity = unserialize($productDetails->row()->subproduct_quantity);
        								    for ( $i = 0; $i < count($subProductsArray); $i++ ) {
        								        $image = explode(',', $subProductsArray[$i]->image);
        								?>
        								        <div class="slide">
            										<div class="items-included-box flex-full">
            											<figure class="flex-full">
            												<amp-img src="<?php echo CDN_URL; ?>images/product/<?php echo $image[0];?>" alt="Product Image" width="302" height="219" layout="responsive"></amp-img>
            											</figure>
            											<div class="items-info flex-full align-items-start align-content-start">
            												<h3><?php echo $subProductsArray[$i]->product_name ;?></h3>
            												<ul class="items-info-listing flex-full">
            													<li>
            														<h4><i class="icn icn-brand"></i> Brand</h4>
            														<p>LG / Samsung / Whirpool / Phillips </p>
            													</li>
            													<li>
            														<h4><i class="icn icn-brand"></i> Dimensions</h4>
            														<p>93 cm (w) x 74 cm (h) </p>
            													</li>
            													<li>
            														<h4><i class="icn icn-brand"></i> Material</h4>
            														<p>Wooden and Fabric</p>
            													</li>
            													<li>
            														<h4><i class="icn icn-brand"></i> Colour</h4>
            														<p>Grey</p>
            													</li>
            												</ul>
            											</div>
            										</div>
            									</div>
        								<?php
        								    }
        								?>
    								</div>
    							</div>
    							<!-- items included slider (for mobile) ends -->
							<?php
							    }
							?>
							
							<!-- product kyc info starts -->
							<ul class="product-info flex-full">
								<li>
									<i class="icn icn-kyc-documents"></i>
									<p>KYC Documents to be submitted before Delivery</p>
								</li>
								<li>
									<i class="icn icn-free-delivery"></i>
									<p>Free Cancellation before Delivery</p>
								</li>
								<li>
									<i class="icn icn-free-quality-product"></i>
									<p>All Products are in Mint Condition</p>
								</li>
							</ul>
							<!-- product kyc info ends -->
						</div>
						<!-- product main info ends -->
					</div>
					<div class="product-items-wrapper flex-full align-items-start align-content-start">
					    <!-- items included starts -->
        				<div class="items-included flex-full d-none-xs">
						    <div class="product-specs spce-features flex-full">	
									<div class="contact-us-info flex-full align-items-start align-content-start">
										<div class="queries-block flex-full">
											<h2 class="product-detail-heading">Specifications</h2>
									<ul class="items-info-listing flex-full">
									    <?php
									        if($productDetails->row()->weight != ''){
									    ?>
									            <li>
        											<h4><i class="icn icn-dimensions"></i> Weight</h4>
        											<p><?php echo $productDetails->row()->weight; ?></p>
        										</li>
									    <?php
									        }
									    ?>
										<?php
									        if($productDetails->row()->brand != ''){
									    ?>
									            <li>
        											<h4><i class="icn icn-brand"></i> Brand</h4>
        											<p><?php echo $productDetails->row()->brand; ?></p>
        										</li>
									    <?php
									        }
									    ?>
									    <?php
									        if($productDetails->row()->material != ''){
									    ?>
									            <li>
        											<h4><i class="icn icn-material"></i> Material</h4>
        											<p><?php echo $productDetails->row()->material; ?></p>
        										</li>
									    <?php
									        }
									    ?>
									    <?php
									        if($productDetails->row()->colour != ''){
									    ?>
									            <li>
        											<h4><i class="icn icn-colour"></i> Colour</h4>
        											<p><?php echo $productDetails->row()->colour; ?></p>
        										</li>
									    <?php
									        }
									    ?>
									</ul>
										</div>
										<div class="contact-block flex-full">
											<h2 class="product-detail-heading">Features</h2>

											<ul class="kyc-info-listing flex-full">
												<li>
												  <span>Cushioned seat </span>
												</li>
												<li>
												  <span>Adjustable height & backrest</span>
												</li>
												<li>
													<span>Wheeled base</span>
												  </li>
												  <li>
													<span>Metal frame</span>
												  </li>
												  <li>
													<span>Handle Shape may vary</span>
												  </li>												  
											  </ul>											
										</div>
									</div>
								</div>
						<?php
						    if(count($subProductsArray) > 0 ){
						?>
						        
        							<h2 class="product-detail-heading">Items Included ( <?php echo count(unserialize($productDetails->row()->subproduct_quantity)) ?>  items)</h2>
        							<ul class="items-included-listing flex-full">
        							    <?php
        							        $product_quantity = unserialize($productDetails->row()->subproduct_quantity);
        							        for ( $i = 0; $i < count($subProductsArray); $i++ ) {
        							            $image = explode(',', $subProductsArray[$i]->image);
        							    ?>
        							            <li>
                									<div class="items-included-box flex-full">
                										<figure class="flex-full">
                											<amp-img src="<?php echo CDN_URL; ?>images/product/<?php echo $image[0];?>" alt="Product Image" width="302" height="219"></amp-img>
                										</figure>
                										<div class="items-info flex-full align-items-start align-content-start">
                											<h3><?php echo $subProductsArray[$i]->product_name ;?></h3>
                											<ul class="items-info-listing flex-full">
																<?php
																	if(!empty($subProductsArray[$i]->brand)){
																?>
																		<li>
																			<h4><i class="icn icn-brand"></i> Brand</h4>
																			<p><?php echo $subProductsArray[$i]->brand ?> </p>
																		</li>
																<?php
																	}
																?>
																<?php
																	if(!empty($subProductsArray[$i]->weight)){
																?>
																		<li>
																			<h4><i class="icn icn-dimensions"></i> Dimensions</h4>
																			<p><?php echo $subProductsArray[$i]->weight ?> </p>
																		</li>
																<?php
																	}
																?>
																<?php
																	if(!empty($subProductsArray[$i]->material)){
																?>
																		<li>
																			<h4><i class="icn icn-material"></i> Material</h4>
																			<p><?php echo $subProductsArray[$i]->material ?> </p>
																		</li>
																<?php
																	}
																?>
																<?php
																	if(!empty($subProductsArray[$i]->colour)){
																?>
																		<li>
																			<h4><i class="icn icn-colour"></i> Colour</h4>
																			<p><?php echo $subProductsArray[$i]->colour ?> </p>
																		</li>
																<?php
																	}
																?>
                												<!--<li>-->
                												<!--	<h4><i class="icn icn-brand"></i> Material</h4>-->
                												<!--	<p>Wooden and Fabric</p>-->
                												<!--</li>-->
                												<!--<li>-->
                												<!--	<h4><i class="icn icn-brand"></i> Colour</h4>-->
                												<!--	<p>Grey</p>-->
                												<!--</li>-->
                											</ul>
                										</div>
                									</div>
                								</li>
        							    <?php
        							        }
        							    ?>
        							</ul>
        						
						<?php
						    }
						?>
						</div>
        				<!-- items included ends -->
						
						<!-- recommended products starts -->
						<?php
						    if(!empty($recommended_product_details_array)){
						?>
						        <div class="recommend-products flex-full">
        							<h2 class="product-detail-heading">Recommended Products with <br/>Erica Sofa Set - 5 Seater</h2>
        							<ul class="recommend-products-listing flex-full">
        							    <?php
        							        foreach($recommended_product_details_array as $recommended_product){
        							            $imgArr = explode(',', $recommended_product->image);                        
                                                if (count($imgArr) > 0) {
                                                    foreach ($imgArr as $imgRow) {
                                                        if ($imgRow != '') {
                                                            $img = $pimg = $imgRow;
                                                            break;
                                                        }
                                                    }
                                                }
        							    ?>
        							            <li>
                									<div class="recommend-products-box flex-full">
                										<a href="<?php echo base_url() ?>things/<?php echo $recommended_product->id; ?>/<?php echo $recommended_product->seourl; ?>" class="flex-full">
                											<amp-img src="<?php echo CDN_URL; ?>images/product/Copressed Images/<?php echo $img; ?>" alt="<?php echo $recommended_product->product_name ?>" width="181" height="132"></amp-img>
                										</a>
                										<div class="recommend-product-info flex-full">
                											<h4><a href="<?php echo base_url() ?>things/<?php echo $recommended_product->id; ?>/<?php echo $recommended_product->seourl; ?>"><?php echo $recommended_product->product_name ?></a></h4>
                											<span>&#x20B9; <?php echo $recommended_product->sale_price ?> <small>/ mon</small></span>
                										</div>
                									</div>
                								</li>
        							    <?php
        							        }
        							    ?>
        							</ul>
        						</div>      
						<?php
						    }
						?>
						<!-- recommended products ends -->
					</div>
				</div>
			</div>
			
		</section>
		<!-- Product detail main ends -->
		<!-- Related products starts -->
		<?php
		    if(!empty($you_might_also_like_details_array)){
		?>
		        <section class="related-product flex-full">
        			<div class="container">
        				<div class="related-product-wrapper flex-full">
        					<h2 class="product-detail-heading">You might also like</h2>
        					<div class="related-product-slider w-100">
        					    <?php
        					        foreach($you_might_also_like_details_array as $like_products){
        					            $imgArr = explode(',', $like_products->image);                        
                                        if (count($imgArr) > 0) {
                                            foreach ($imgArr as $imgRow) {
                                                if ($imgRow != '') {
                                                    $img = $pimg = $imgRow;
                                                    break;
                                                }
                                            }
                                        }
        					    ?>
        					            <div class="slide">
                							<div class="product-single flex-full position-relative">
                								<div class="product-image flex-full position-relative">
                									<a href="<?php echo base_url() ?>things/<?php echo $like_products->id; ?>/<?php echo $like_products->seourl; ?>" class="flex-full position-relative h-100">
                										<amp-img src="<?php echo CDN_URL; ?>images/product/Copressed Images/<?php echo $img; ?>" alt="Product Image" layout="responsive" width="300" height="225"></amp-img>
                									</a>
                									<span class="new-label">New</span>
                									<span class="wishlist"><i class="icn icn-wishlist-fill-gray"></i></span>
                								</div>
                								<div class="product-description flex-full">
                									<div class="product-description-wrapper flex-full">
                										<h3><a href="<?php echo base_url() ?>things/<?php echo $like_products->id; ?>/<?php echo $like_products->seourl; ?>"><?php echo $like_products->product_name ?></a></h3>
                										<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> <?php echo $like_products->sale_price ?></strong> / mon</p>
                									</div>
                								</div>
                							</div>
                						</div>
        					    <?php
        					        }
        					    ?>
        					</div>
        				</div>
        			</div>
        		</section>
		<?php
		    }
		?>
		<!-- Related products ends -->
		<!-- Testimonial starts -->
		<?php
		    $get_customer_review = get_customer_review();
		    if(!empty($get_customer_review)){
		?>
		        <section class="testimonial-section flex-full">
        			<div class="testimonial-wrapper flex-full">
        				<h2 class="product-detail-heading">What our Customers say</h2>
        				<div class="testimonial-slider w-100">
        				    <?php
        				        foreach($get_customer_review as $customer_review){
        				    ?>
        				            <div class="slide">
                						<div class="testimonial-content-wrapper flex-full align-items-center align-content-center">
                							<figure class="flex-full">
                								<amp-img src="<?php echo base_url() ?>images/customer_feedback/<?php echo $customer_review->product_image ?>" alt="Product Image" width="228" height="229"></amp-img>
                							</figure>
                							<div class="testimonial-content flex-full">
                								<p><?php echo $customer_review->customer_comment ?></p>
                								<span class="testimonial-name flex-full justify-content-end align-items-center"><strong><?php echo $customer_review->customer_name ?>,</strong> <?php echo $customer_review->customer_place ?></span>
                							</div>
                						</div>
                					</div>
        				    <?php
        				        }
        				    ?>
        				</div>
        				<span class="view-all-link flex-full justify-content-center text-center align-items-center">
        					<a href="<?php echo base_url('reviews-testimonials/all') ?>">View All</a>
        					<i class="icn-arrow-right"></i>
        				</span>
        			</div>			
        		</section>
		<?php
		    }
		?>
		<!-- Testimonial ends -->
		<div class="product-action-bar flex-full d-none d-block-xs">
			<div class="flex-full">
				<div class="monthly-rent-box flex-full align-items-center align-content-center">
					<label>Monthly Rent</label>
					<span class="price" id="price_mobile">&#x20B9; 2375.00</span>
				</div>
				<div class="refundable-deposit-box flex-full align-items-center align-content-center">
					<label>Refundable Deposit</label>
					<span class="price" id="refundable_deposit">&#x20B9; <?php echo $productDetails->row()->shipping_cost; ?></span>
				</div>
				<div class="addtocart-box flex-full align-items-center align-content-center">
				    <?php
				        if($loginCheck != ''){
				    ?>
				            <a href="javascript:void(0)" class="addtocart-btn" onclick="ajax_add_cart('<?php echo $PrdAttrVal->num_rows(); ?>');">Add to Cart</a>
				    <?php
				        }else{
				    ?>
				            <a href="<?php echo base_url() ?>user_sign_up" class="addtocart-btn">Add to Cart</a>
				    <?php
				        }
				    ?>
				</div>
			</div>
		</div>
	</div>
	
	
	<script>
	
	    var $range = $("#tenureslider");
		var tenureData = <?php echo json_encode($tenureArray); ?>;
        var SelectedTenure = '<?php echo $this->session->userdata('first_tenure'); ?>';
        if(SelectedTenure == ''){
          SelectedTenure  = Object.keys(tenureData).length;
        }else{
          var new_tenure = Object.keys(tenureData);
          SelectedTenure = new_tenure.indexOf(''+SelectedTenure+'');
          if(SelectedTenure < 0){
             SelectedTenure  = Object.keys(tenureData).length;
          }
        }
	    function setmyTenure(tenure,pid){
	        tenure = tenure.split(" ");
            $('#attr_name_id').val(pid);
            $('#attribute_values').val(pid);
            $('#selectedTenure').html(tenure[0]+' '+tenure[1]);
            $('#selected_tenure').val(tenure[0]);
            $("#tenure_price").text(tenureData[tenure[0]].price);
	    }
	    if($(window).width() < 900){
	        $range.ionRangeSlider({
                type: "double",
                skin: "round",
                values: <?php echo json_encode($sliderData); ?>,
                grid: true,
                from: SelectedTenure,
                onStart: updateInput,
                onChange: updateInput
            });
            
            function updateInput (data) {
                var url = '<?php echo current_url(); ?>';
                console.log(data.from_value);
                $('#selected_tenure').val(data.from_value);
                $('#price').val(tenureData[data.from_value]['price']);
                $("#tenure_price").text(tenureData[data.from_value]['price']);
                $("#price_mobile").html('&#x20B9;' + tenureData[data.from_value]['price']);
                $('#SalePrice').html(tenureData[data.from_value]['price']);
                $('#attr_name_id').val(tenureData[data.from_value]['pid']);
            } 
	    }
	    
	</script>
	
	<!-- Footer starts -->
    <?php $this->load->view('site/templates/footer'); ?>
	</div>
	</body>
</html>