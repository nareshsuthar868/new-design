<?php $this->load->view('site/templates/header_inner'); ?>
<!-- Filter/sort bar (for mobile) starts -->
<div class="mobile-filterbar flex-full">
    <div class="mobile-filterbar-wrapper flex-full">
        <span id="filter-link">Filter</span>
        <span id="sort-link">Sort</span>
    </div>
</div>
<!-- Filter/sort bar (for mobile) ends -->

<!-- Filterbox (for mobile) starts -->
<input type="hidden" id="user_id" value="<?php if(isset($_SESSION['fc_session_user_id'])){ echo $_SESSION['fc_session_user_id']; } ?>">
<input type="hidden" id="cat_slug" value="<?php echo $cat_slug; ?>">
<div class="mobile-filterbox flex-full align-items-start align-content-start">
    <div class="mobile-filterbox-wrapper flex-full position-relative h-100 align-items-start align-content-start">
        <span class="close"><i class="icn icn-close-dark"></i></span>
        <ul class="tabs">
            <li class="tab-link current" data-tab="tab-1">Filter</li>
            <li class="tab-link" data-tab="tab-2">Sort</li>             
        </ul>

        <div id="tab-1" class="tab-content current">
            <ul class="tags-listing flex-full justify-content-end">
                <?php
                if(!empty($Cutfilter)){
                    foreach($Cutfilter as $key => $val){ ?>
                        <li  class="custome_<?php echo $val->filter_tag; ?>"><span class="tag firstcapital"><?php echo $val->filter_name; ?><a href="javascript:void(0)" onclick='removefilter("<?php echo $val->filter_tag; ?>");'><i class="icn icn-close-blue"></i></a></span></li>
                <?php   }  } ?>
            </ul>
            <div class="category-box flex-full" id="Customefilters_mobile">
                <?php 
                    if(!empty($filters)){
                        foreach ($filters as $key => $value) { ?>
                            <div class="checkbox-grp">
                                <input type="checkbox" name="" id="mobile_<?php echo $value->filter_tag; ?>" value="<?php echo $value->filter_tag; ?>">
                                <label for="mobile_<?php echo $value->filter_tag; ?>"><?php echo $value->filter_name; ?></label>
                            </div>
                        <?php }
                    }else{
                            echo '<div class="checkbox-grp"> <input type="checkbox" name="" id="'.$this->uri->segment(2,0).'_all" value="'.$this->uri->segment(2,0).'_all" checked><label for="'.$this->uri->segment(2,0).'_all">All</label> </div>';
                    }
                ?>
            </div>
            <a href="javascript:void(0)" onclick="resetFilter()" class="reset-btn">Reset</a>
        </div>
        <div id="tab-2" class="tab-content">
            <ul class="sort-listing flex-full">
                <li><a href="javascript:void(0)" onclick="sortProductlisting('<?php echo $cat_slug; ?>','<?php echo $this->uri->segment(2,0); ?>','all','xs')">All</a></li>
                <li><a href="javascript:void(0)" onclick="sortProductlisting('<?php echo $cat_slug; ?>','<?php echo $this->uri->segment(2,0); ?>','new','xs')">New</a></li>
                <li><a href="javascript:void(0)" onclick="sortProductlisting('<?php echo $cat_slug; ?>','<?php echo $this->uri->segment(2,0); ?>','low_high','xs')">Price - Low-High</a></li>
                <li><a href="javascript:void(0)" onclick="sortProductlisting('<?php echo $cat_slug; ?>','<?php echo $this->uri->segment(2,0); ?>','high_low','xs')">Price - High-Low</a></li>
            </ul>
        </div>          
    </div>
</div>
<!-- Filterbox (for mobile) ends -->

<!-- Categories (for mobile) starts -->
<div class="mobile-category flex-full " id="mobileCatSlider">
    <?php
    $c_status_count = 0;
    foreach ($listSubCat as $subCategory){ 
        if($subCategory->subcat_sub_cat_status == 'Active') {
            $c_status_count++;
        }
    } if($c_status_count != 0){ ?>
    <script>
        $(document).ready(function (){ 
            $(document.body).addClass("product-listing");
        });
    </script>
    <?php } ?>
    <div class="mobile-category-slider">
    <?php if($c_status_count != 0) {
            if($listSubCat[0]->subcat_seourl != ''){ 
                foreach ($listSubCat as $subCategory){ 
                    if($subCategory->subcat_sub_cat_status == 'Active'){
                        if($subCategory->subcat_sub_cat_image != ''){
                            $catImage = CDN_URL . 'images/category/' . $subCategory->subcat_sub_cat_image;
                        }else{
                            $catImage = CDN_URL . 'images/wishlist_img_1.jpg';
                        }
                    ?> 
                    <div class="slide"  onclick="onCategoryChange('<?php echo $cat_slug; ?>','<?php echo $subCategory->subcat_seourl; ?>','c')">
                        <div class="mobile-category-content flex-full justify-content-center">
                            <i class="icn icn-dining-room"></i>
                            <span><?php echo $subCategory->subcat_sub_cat_name; ?></span>
                        </div>
                    </div>
    <?php } } } } ?>
    </div>
</div>
<!-- Categories (for mobile) ends -->
<div class="page-wrapper flex-full">
    <!-- Breadcurmbs starts -->
    <div class="breadcrumbs flex-full">
        <div class="container">
            <ul class="breadcrumbs-listing flex-full align-items-center">
                  <?php echo trim($breadCumps); ?>
                <!--<li class="root-link"><a href="javascript:void(0)">Home</a></li>-->
                <!--<li class="child-link"><a href="javascript:void(0)">Home furniture</a></li>-->
                <!--<li class="current-link"><a href="javascript:void(0)">Living room</a></li>-->
            </ul>
        </div>
    </div>
    <!-- Breadcurmbs ends -->
    <!-- Inner page heading starts -->
    <section class="innerpage-heading flex-full justify-content-center">
        <div class="container">
            <div class="innerpage-heading-wrapper flex-full justify-content-center bg-img position-relative" style="background-image: url(images/innerpage-banner.jpg);">
                <div class="innerpage-description flex-full justify-content-center text-center">
                    <h1 id="HeaderHeading"><?php echo $page_heading; ?><i class="icn icn-arrow-up-gray" id="clickToggle"></i></h1>
                    <p class="innerpage-text content-expand" id="HeaderDescription"><?php echo strip_tags($page_description); ?></p>
                </div>
            </div>
        </div>
    </section>
    <!-- Inner page heading ends -->

    <!-- Product listing main starts -->
    <section class="product-listing-main flex-full">
        <div class="container">
            <div class="product-listing-main-wrapper flex-full align-items-start align-content-start">
                <aside class="filter-area flex-full">
                    <div class="filter-box category-block flex-full">
                        <h2>Categories</h2>
                        <div class="category-box flex-full">
                            <div class="accordion">
                                <?php
                                    foreach ($categoriesTree as $key => $row) {
                                        if ($row->cat_name != '' && $row->cat_name != 'Our Picks' ) {
                                            if($row->image != ''){
                                                $catImage = CDN_URL . 'images/category/' . $row->image;
                                            }else{
                                                $catImage = base_url() . 'images/wishlist_img_1.jpg';
                                            }
            
                                    if (isset($row->sub_categories)) {
                                        if($row->cat_name != 'Addon' && $row->cat_name != 'Fitness'){
                                            foreach ($row->sub_categories as $key => $value) {
                                                if($value->seourl == $this->uri->segment(2,0) ){
                                                    $parent_url = $row->seourl;
                                                }
                                			}   
                                        ?>
                                        <!--current-->
                                    <div class="accordion-tab <?php if($parent_url == $row->seourl || $this->uri->segment(2,0) == $row->seourl ) { echo "current"; } ?>">
                                         <!--onclick="location.href='<?php echo base_url(); ?><?php echo $cat_slug; ?>/<?php echo $row->seourl; ?>'"-->
                                        <h3 class="accordion-title"  onclick="onCategoryChange('<?php echo $cat_slug; ?>','<?php echo $row->seourl; ?>','p')"><?php echo $row->cat_name; ?><i class="icn icn-arrow-bottom"></i></h3>
                                        <div class="accordion-content">
                                            <div class="accordion-body">
                                                <?php foreach ($row->sub_categories as $subCat){ ?>
                                                <div class="radio-grp">
                                                    <!--onclick="location.href='<?php echo base_url(); ?><?php echo $cat_slug; ?>/<?php echo $subCat->seourl; ?>'"-->
                                                    <input type="radio" name="category" id="<?php echo $subCat->seourl; ?>"  onclick="onCategoryChange('<?php echo $cat_slug; ?>','<?php echo $subCat->seourl; ?>')"   <?php if($this->uri->segment(2,0) == $subCat->seourl) { echo "checked";  }else {  } ?> >
                                                    <label for="<?php echo $subCat->seourl; ?>"><?php echo $subCat->cat_name; ?></label>
                                                </div>
                                                <?php } ?>
                                               
                                            </div>
                                        </div>
                                    </div>
                 
                                 <?php } ?>
                                 <?php } else if($row->cat_name != 'Cred') { ?>
                                    <div class="accordion-tab <?php if($this->uri->segment(2,0) == $row->seourl){ echo "current"; } ?>">
                                        <h3 class="accordion-title" onclick="onCategoryChange('<?php echo $cat_slug; ?>','<?php echo $row->seourl; ?>','p')" ><?php echo $row->cat_name; ?> <i class="icn icn-arrow-bottom"></i></h3>
                                        <div class="accordion-content">
                                            <div class="accordion-body">
                                                <!--<div class="radio-grp">-->
                                                <!--    <input type="radio" name="category" id="<?php echo $row->seourl; ?>" onclick="onCategoryChange('<?php echo $cat_slug; ?>','<?php echo $row->seourl; ?>')"  <?php if($this->uri->segment(2,0) == $row->seourl) { echo "checked";  }?> >-->
                                                <!--    <label for="<?php echo $row->seourl; ?>"> <?php echo $row->cat_name; ?></label>-->
                                                <!--</div>-->
                                            </div>
                                        </div>
                                    </div>                                   
                                <?php } } } ?>

                            </div>
                        </div>
                    </div>
                    <div class="filter-box filter-block flex-full">
                        <h2>Filters <a href="javascript:void(0)" class="reset-small" style="display:none;" onclick="resetFilter()">Reset</a></h2>                         
                        <div class="category-box flex-full" id="Customefilters">
                            <?php 
                            if(!empty($filters)){
                            foreach ($filters as $key => $value) { ?>
                                    <div class="checkbox-grp">
                                        <input type="checkbox" name="" id="<?php echo $value->filter_tag; ?>" value="<?php echo $value->filter_tag; ?>" <?php if(in_array($value->filter_tag,$cutome_filter['filter'])){ echo "checked";} ?>>
                                        <label for="<?php echo $value->filter_tag; ?>"><?php echo $value->filter_name; ?></label>
                                    </div>
                            <?php }
                            }else{
                            echo '<div class="checkbox-grp"> <input type="checkbox" name="" id="'.$this->uri->segment(2,0).'_all" value="'.$this->uri->segment(2,0).'_all" checked><label for="'.$this->uri->segment(2,0).'_all">All</label> </div>';
                            }
                            ?>
                        </div>
                    </div>
                </aside>
                <?php if ($productList->num_rows() > 0) { ?>	
                <div class="product-area flex-full">
                    <div class="product-topbar flex-full">
                        <div class="sort-by flex-full align-items-center">
                            <div class="form-inline">
                                <label>Sort by : </label>
                                <span>Default <i class="icn-arrow-bottom"></i></span>
                                <ul class="sort-dropdown" style="display: none;">
                                    <li class="selected" onclick="sortProductlisting('<?php echo $cat_slug; ?>','<?php echo $this->uri->segment(2,0); ?>',this)" data-attribute="all">Default</li>
                                    <li onclick="sortProductlisting('<?php echo $cat_slug; ?>','<?php echo $this->uri->segment(2,0); ?>',this)" data-attribute="new">New</li>
                                    <li onclick="sortProductlisting('<?php echo $cat_slug; ?>','<?php echo $this->uri->segment(2,0); ?>',this)" data-attribute="low_high">Price - Low-High</li>
                                    <li onclick="sortProductlisting('<?php echo $cat_slug; ?>','<?php echo $this->uri->segment(2,0); ?>',this)" data-attribute="high_low">Price - High-Low</li>
                                </ul>
                            </div>
                            <!-- <select id="sortingdrop" onchange="sortProductlisting('<?php echo $cat_slug; ?>','<?php echo $this->uri->segment(2,0); ?>',this)"> 
                                <option value="all" selected>Default</option>
                                <option value="new">New</option>
                                <option value="low_high">Price - Low-High</option>
                                <option value="high_low">Price - High-Low</option>
                            </select> -->
                            <!--<span><i class="icn icn-arrow-bottom"></i></span>-->
                            <!--<span>Popularity <i class="icn icn-arrow-bottom"></i></span>-->
                        </div>
                        <ul class="tags-listing flex-full justify-content-end">
                        <?php
                            if(!empty($Cutfilter)){
                                foreach($Cutfilter as $key => $val){       ?>
                                
                                  <li  class="custome_<?php echo $val->filter_tag; ?>"><span class="tag firstcapital"><?php echo $val->filter_name; ?><a href="javascript:void(0)" onclick='removefilter("<?php echo $val->filter_tag; ?>");'><i class="icn icn-close-blue"></i></a></span></li>
                            <?php   }  } ?>
                        </ul>
                    </div>
                    <ul class="product-listing<?php if($category_type == 'Packages' || $category_type == 'Combos'){ echo ' combo'; } ?> flex-full" id="product_main_container">
                        <?php 
                        foreach ($ResponseProductList as $productListVal) {
                        $img = 'dummyProductImage.jpg';    
                        $Subproduct = 0;
                        if($productListVal->is_package && $productListVal->subproducts != ''){
                            $Subproduct = count(explode(',', $productListVal->subproducts)); 
                        }
                        $imgArr = explode(',', $productListVal->image);                        
                        if (count($imgArr) > 0) {
                            foreach ($imgArr as $imgRow) {
                                if ($imgRow != '') {
                                    $img = $pimg = $imgRow;
                                    break;
                                }
                            }
                        } ?>
                        <li>
                            <div class="product-single flex-full align-content-start position-relative">
                                <div class="product-image flex-full position-relative" id="DynamicWishlist_<?php echo $productListVal->id;  ?>">
                                    <a href="<?php echo base_url(); ?>things/<?php echo $productListVal->id; ?>/<?php echo $productListVal->seourl; ?>" class="flex-full position-relative h-100" target="_blank">
                                        <amp-img  src="<?php echo CDN_URL; ?>images/product/Copressed Images/<?php echo $img; ?>" alt="<?php echo $productListVal->product_name; ?>" layout="responsive" width="300" height="225">
                                            <noscript>
                                                <img src="<?php echo base_url(); ?>/images/ajax-loader/ajax-loader.gif" alt="Example" loading="lazy">
                                            </noscript>
                                        </amp-img>
                                         <?php if($productListVal->is_sold){ ?>
                                        <div class="ribbon ribbon-top-right"><span>Out of Stock</span></div>																					
									
                                    <?php }?>
                                    </a>
                                    <?php if($productListVal->product_label != ''){ ?>
                                        <span class="<?php echo $productListVal->product_label ?>-label"><?php $label = str_replace("_"," ",$productListVal->product_label); echo ucwords($label);  ?></span>
                                        <?php } if($productListVal->like == '1'){ ?>
                                            <span class="wishlist" onclick="addtowhishlist('<?php echo $productListVal->id; ?>')"><i  id="whished_<?php echo $productListVal->id;  ?>" class="icn icn-wishlist-fill-red"></i></span>
                                        <?php } else { ?>
                                            <span class="wishlist"  onclick="addtowhishlist('<?php echo $productListVal->id; ?>')"><i id="whished_<?php echo $productListVal->id;  ?>" class="icn icn-wishlist-fill-gray"></i></span>
                                        <?php }?>
                                </div>
                                <div class="product-description flex-full">
                                    <div class="product-description-wrapper flex-full">
                                        <h3><a href="<?php echo base_url(); ?>things/<?php echo $productListVal->id; ?>/<?php echo $productListVal->seourl; ?>"  target="_blank"><?php echo $productListVal->product_name; ?></a></h3>
                                        <p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> <?php echo number_format($productListVal->sale_price); ?></strong> / mon
                                            <?php
                                                if($category_type != 'Packages' && $category_type != 'Combos'){
                                            ?>
                                                    <span class="items-count" onclick="openSubBox('<?php echo $productListVal->id; ?>')"><?php if($Subproduct > 0){ echo '+'.$Subproduct.' item'; } ?> </span>
                                            <?php
                                                }
                                            ?>
                                        </p>
                                    </div>
                                    <?php
                                        if($category_type != 'Packages' && $category_type != 'Combos'){
                                    ?>
                                            <div class="more-items-expand flex-full" style="display: none;" id="subProductList_<?php echo $productListVal->id;  ?>">
                                                <span class="items-count w-100"><?php echo '+'.$Subproduct.' item'; ?></span>
                                                <ul class="items-lisitng flex-full">
                                                    <?php 
                                                    for ($i= 0; $i < count($productListVal->subproductimages); $i++) { 
                                                        $product_images = explode(',',$productListVal->subproductimages[$i]->image);
                                                    ?>
                                                    <li><a href="javascript:void(0)"><img src="<?php echo base_url(); ?>images/product/Copressed Images/<?php echo $product_images[0]; ?>" alt="Product Image"></a></li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                    <?php
                                        }else{
                                            if($Subproduct > 0){
                                    ?>
                                                <div class="included-items-block flex-full">
                                                    <h4><?php echo $Subproduct ?> Items Included</h4>
                                                    <ul class="included-items-listing flex-full">
                                                    <?php 
                                                        for ($i= 0; $i < count($productListVal->subproductimages); $i++) { 
                                                            $product_images = explode(',',$productListVal->subproductimages[$i]->image);
                                                    ?>
                                                                <li>
                                                                    <a href="javascript:void(0)">
                                                                        <amp-img src="<?php echo base_url(); ?>images/product/Copressed Images/<?php echo $product_images[0]; ?>" alt="Product Image"
                                                                                    width="78" height="59"></amp-img>
                                                                    </a>
                                                                </li>
                                                    <?php
                                                            }
                                                    ?>
                                                    </ul>
                                                </div>
                                    <?php
                                            }
                                        }
                                    ?>
                                   
                                </div>
                            </div>
                        </li>
                        <?php } ?>                          
                    </ul>
                </div>
               <?php }  else {
        $txt = "'t";
            echo '<div class="product-area flex-full">
                    <div class="container">
                         <div class="thankyou-wrapper flex-full justify-content-center text-center">
        					<figure>
        						<img src="images/cheers-vector.png" alt="cheers-vector">
        					</figure>
        					<h1 class="w-100 color-red">Product not found</h1>					
        					<span class="message w-100">We don'.$txt.' provide '.$listSubCat[0]->cat_name.' products '.$selected_city.'</span>
        				</div>
                    </div>
                </div>';  }?>
            </div>
        </div>
        
    </section>
    <!-- Product listing main etarts -->
</div>
<input type="hidden" id="listing_page" value="yes">
<input type="hidden" id="category_slug" value="<?php echo $this->uri->segment(2,0); ?>">
<input type="hidden" id="parent_slug" value="<?php echo $parent_url; ?>">
<?php $this->load->view('site/templates/footer'); ?>
</div>
<script src="plugins/owl-carousel/owl.carousel.min.js"></script> 
<script>
    $(document).ready(function () {
        var count  = '<?php echo count($ResponseProductList); ?>';
        if(count < 12){
            // setTimeout(function(){ get_data(); }, 1000);
        }
        
        $('.left_arrow').click(function() {
            $('.seo_description').show('slow');
            $('.right_arrow').show()
            $('.left_arrow').hide();
        });
    
        $('.right_arrow').click(function() {
            $('.seo_description').hide('slow');
            $('.right_arrow').hide()
            $('.left_arrow').show();
        });
        
        var totalItems = $('.item').length;
        if (totalItems <= 7) {
            var isNav = false;
        }
        else {
            var isNav = true;
        }

        if($('.owl-carousel').length > 0) {
            $('.owl-carousel').owlCarousel({
                loop: false,
                nav: isNav,
                navText: ["<i class='material-icons'>keyboard_arrow_left</i>", "<i class='material-icons'>keyboard_arrow_right</i>"],
                margin: 0,
                mouseDrag: false,
                pullDrag: false,
                touchDrag: true,
                autoplay: false,
                responsive: {
                    768: {
                        items: 5
                    },
                    1000: {
                        items: 6,
                    },
                    1200: {
                        items: 7
                    }
                }
            });
        }
    });
</script>
</body>
</html>
