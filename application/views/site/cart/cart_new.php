<?php $this->load->view('site/templates/header_inner'); ?>
	<div class="page-wrapper flex-full">
	    <?php if($cartVal->num_rows() > 0){ ?>
		<div id="cart-1" class="product-cart fixed-rental-plans-section flex-full">
			<div class="fixed-rental-plans-wrapper flex-full align-items-center align-content-center">
				<div class="fixed-rental-plans-steps flex-full">
					<div class="container">
						<ul class="fixed-rental-plans-steps-listing flex-full justify-content-between">
							<li class="active"><span>Cart</span></li>
							<li class=""><span>Address</span></li>
							<?php if($get_user->row()->is_offline_user == 0 ||  $get_user->row()->is_offline_user == '0'){ ?>
							<li><span>Payment</span></li>
							<?php }?>
						</ul>
					</div>
				</div>
				<div class="cart-main-section flex-full">
					<div class="container">
						<div class="cart-main-wrapper flex-full align-items-start align-content-start">
							<div class="order-summary-block flex-full">
								<h2 class="border-left">Order Summary</h2>
								<ul class="order-summary-listing flex-full">
								    <?php
								    $cartAmt = 0;
                                    $cartShippingAmt = 0;
                                    $cartTaxAmt = 0;
                                    $cartDiscountAmt = 0;
                                    $cartDisAmt = 0;
                                    $MainTaxCost = 0;
                                    $appliedCoupon ='';
                                    $s = 0;
								    foreach ($cartVal->result() as $CartRow) {
								      $newImg = @explode(',', $CartRow->image);?>
									<li id="cart-row-<?php echo $CartRow->id; ?>">
										<div class="order-summary-box flex-full">
											<div class="product-img flex-full">
												<amp-img src="<?php echo  PRODUCTPATH.$newImg[0]; ?>" alt="Product Image" width="204" height="160"></amp-img>
											</div>
											<div class="product-desc flex-full align-items-start align-content-start">
												<a href="javascript:void(0)"  onclick="javascript:delete_cart('<?php echo $CartRow->id ?>','<?php echo $CartRow->id ?>')" class="close"><i class="icn icn-close-black"></i></a>
												<h3><?php echo $CartRow->product_name; ?></h3>
												<ul class="product-desc-listing flex-full">
													<li>
														<h4>Quantity</h4>
														<span><?php echo $CartRow->quantity; ?> item</span>
													</li>
													<li>
														<h4>Monthly Rent</h4>
														<span>&#x20B9; <?php echo number_format($CartRow->price,2, '.', ''); ?></span>
													</li>
													<li>
														<h4>Refundable Deposit</h4>
														<span>&#x20B9; <?php echo number_format($CartRow->product_shipping_cost,2, '.', ''); ?></span>
													</li>
												</ul>
											</div>
										</div>
									</li>
									<input type="hidden" name="prod_duration" id="prod_duration" value="<?php echo $CartRow->attr_name ?>" />
									<?php
    									$qu = $this->db->query("SELECT cart_page_label,maximumamount FROM " . COUPONCARDS . " WHERE id='" . $CartRow->couponID . "' LIMIT 1");
                                        $rr = $qu->result();
                                         $quantity +=  $CartRow->quantity;
                                        $cart_amount_without_discount += $CartRow->price  * $CartRow->quantity;
                                        $cart_shipping_amount_without_discount += ( $CartRow->price + $CartRow->product_shipping_cost) * $CartRow->quantity;
                                        $cartAmt = $cartAmt + (($CartRow->price - $CartRow->discountAmount + ($CartRow->price * 0.01 * $CartRow->product_tax_cost)) * $CartRow->quantity);
                                        $advance_rental =  $advance_rental + ((($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
                                        $cartShippingAmt = $cartShippingAmt + ($CartRow->product_shipping_cost * $CartRow->quantity);
                                        $cartTaxAmt = $cartTaxAmt + ($CartRow->product_tax_cost * $CartRow->quantity);
                                        $cartQty = $cartQty + $CartRow->quantity;
                                        $cartDiscountAmt = $cartDiscountAmt + ($CartRow->discountAmount * $CartRow->quantity);
                                        $s++;                                    
                                        if ($rr[0]->maximumamount != 0) {
                                            if($cartDiscountAmt > $rr[0]->maximumamount) {
                                                $cartAmt = $cartAmt + $cartDiscountAmt - $rr[0]->maximumamount;
                                                $cartDiscountAmt = $rr[0]->maximumamount;
                                            }
                                        }
                                        
                                        if($CartRow->couponCode != ''){
                                            $appliedCoupon = $CartRow->couponCode;
                                        }
									} 
								// 	print_r($cartAmt);exit;
								    $cartSAmt = $cartShippingAmt;
                                    $cartTAmt = ($cartAmt * 0.01 * $MainTaxCost);
                                    $grantAmt = $cartAmt + $cartSAmt + $cartTAmt;
                                    $disAmt = $cartDiscountAmt;
                                    
                                    if(isset($_SESSION['Applied_Walled'])){
                                          $grantAmt = $grantAmt - $_SESSION['Applied_Walled'];
                                    }
									
									
									?>
								</ul>
								<span class="proceed-btn flex-full justify-content-end" data-tab="cart-2"><a href="javascript:void(0)" class="explore-btn">Proceed</a></span>
							</div>
							<aside class="cart-sidebar flex-full align-items-start align-content-start">
								<div class="cart-sidebar-block order-detail-block flex-full">
									<h2 class="border-left">Order Detail</h2>
									<ul class="product-desc-listing flex-full">
										<li>
											<h4>Duration</h4>
											<span>24 Months</span>
										</li>
										<li>
											<h4>Advance Rental</h4>
											<span id="CartAmt">&#x20B9; <?php echo number_format($cartAmt, 2, '.', ''); ?></span>
										</li>
										<li>
											<h4>Refundable Deposit</h4>
											<span id="CartSAmt">&#x20B9; <?php echo number_format($cartSAmt, 2, '.', ''); ?></span>
										</li>
										<li>
											<h4>Discount</h4>
											<span id="disAmtVal">&#x20B9; <?php echo number_format($disAmt, 2, '.', ''); ?></span>
										</li>
										<li>
											<h4>Delivery</h4>
											<span>Free</span>
										</li>
										<li>
											<h4>Total</h4>
											<span id="CartCartGAmt">&#x20B9; <?php echo number_format($grantAmt, 2, '.', ''); ?></span>
										</li>
									</ul>
								</div>
								<div class="cart-sidebar-block coupon-block flex-full">
									<h2 class="border-left">Apply Coupon</h2>
									<?php if ($disAmt > 0) { ?>
									<form class="apply-coupon-form flex-full">
										<input type="text" name="is_coupon" id="is_coupon"  value="<?php echo $appliedCoupon; ?>" class="coupon-input">
										<input type="button" onclick="javascript:checkRemove();"  id="CheckCodeButton" value="Remove" class="explore-btn coupon-btn">
										<span id="CouponErr"></span><br>
                                        <span id="CouponMessage" style="color:green"><?php echo $rr[0]->cart_page_label ?></span>
									</form>
									<?php }else{ ?>
									<form class="apply-coupon-form flex-full">
									    <input type="text"  name="is_coupon" id="is_coupon" class="coupon-input" placeholder="COUPON CODE">
									    <input type="button" onclick="javascript:checkCode();"  id="CheckCodeButton" value="Apply" class="explore-btn coupon-btn">
									    <span id="CouponErr"></span><br>
                                        <span id="CouponMessage"></span>
								    </form>
								    <?php } ?>
									<ul class="coupons-lisitng flex-full">
									    <?php 
									    if($coupon_codes->num_rows() > 0){ 
									        foreach($coupon_codes->result_array() as $key => $value){ 
									            $shortDescription = unserialize($value['description']);
									        ?>
									            
        										<li>
        											<div class="coupon-text-wrapper flex-full">
        												<h4>Flat <?php echo $value['price_text']; ?> <span>for <?php echo str_replace('in',' ',$value['price_below_text'] ); ?></span></h4>
        												<p><?php if(array_key_exists("1",$shortDescription)){  echo $shortDescription[0].', '.$shortDescription[1]; } ?></p>
        											</div>
        											<?php if(strtolower($appliedCoupon) == strtolower($value['coupon_code'])) { ?>
        											    <span class="coupon-code explore-btn" data-coupon="<?php echo $value['coupon_code']; ?>"><?php echo $value['coupon_code']; ?></span>
        											<?php }else{ ?>
        											    <span class="apply-coupon explore-btn" data-coupon="<?php echo $value['coupon_code']; ?>"><?php echo $value['coupon_code']; ?></span>
        											<?php }?>
        										</li>
									    <?php } }  ?>
										<!--<li>-->
										<!--	<div class="coupon-text-wrapper flex-full">-->
										<!--		<h4>Flat 50% Off <span>for first 3 months</span></h4>-->
										<!--		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>-->
										<!--	</div>-->
										<!--	<a class="apply-coupon explore-btn" href="javascript:void(0)">Apply</a>-->
										<!--</li>-->
										<!--<li>-->
										<!--	<div class="coupon-text-wrapper flex-full">-->
										<!--		<h4>Flat 50% Off <span>for first 3 months</span></h4>-->
										<!--		<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>-->
										<!--	</div>-->
										<!--	<span class="coupon-code explore-btn">OFF50</span>-->
										<!--</li>-->
									</ul>
								</div>
								<div class="cart-sidebar-block product-info-block flex-full">
									<ul class="product-info flex-full">
										<li>
											<i class="icn icn-kyc-documents"></i>
											<p>KYC Documents to be submitted before Delivery</p>
										</li>
										<li>
											<i class="icn icn-free-delivery"></i>
											<p>Free Cancellation before Delivery</p>
										</li>
										<li>
											<i class="icn icn-free-upgrade"></i>
											<p>All Products are in Mint Condition</p>
										</li>
									</ul>
								</div>
								<div class="free-service-block flex-full">
									<h2>Your Free Sevices *</h2>
									<ul class="free-service-listing flex-full">
										<li>
											<i class="icn icn-free-delivery"></i>
											<span>Free Delivery</span>
										</li>
										<li>
											<i class="icn icn-free-pickup"></i>
											<span>Free Pickup</span>
										</li>
										<li>
											<i class="icn icn-free-install"></i>
											<span>Free Install</span>
										</li>
										<li>
											<i class="icn icn-free-upgrade"></i>
											<span>Free Upgrade</span>
										</li>
										<li>
											<i class="icn icn-free-relocation"></i>
											<span>Free Relocation</span>
										</li>
										<li>
											<i class="icn icn-free-damage-waiver"></i>
											<span>Damage Waiver</span>
										</li>
									</ul>
								</div>
								
							</aside>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php if($get_user->row()->is_offline_user == 0 ||  $get_user->row()->is_offline_user == '0'){ ?>
		<div id="cart-2" class="address-selection fixed-rental-plans-section flex-full d-none">
			<div class="fixed-rental-plans-wrapper flex-full align-items-center align-content-center">
				<div class="fixed-rental-plans-steps flex-full">
					<div class="container">
						<ul class="fixed-rental-plans-steps-listing flex-full justify-content-between">
							<li class="complete"><span>Cart</span></li>
							<li class="active"><span>Address</span></li>
							<li><span>Payment</span></li>							
						</ul>
					</div>
				</div>
				<div class="cart-main-section flex-full">
					<div class="container">
						<div class="cart-main-wrapper flex-full align-items-start align-content-start">
							<aside class="address-selection-block flex-full">
								<h2 class="border-left">Choose Address</h2>
								<ul class="address-listing flex-full save-address">
								<?php 
								    if($shipVal->num_rows() > 0){
                                    $c = 0;
                                    $Shiprow->id = 0;
                                    $sflag = '1';
                                    $default_id = 0;
                                    $noaddresserror = ''; 
                                    foreach ($shipVal->result() as $Shiprow) { 
                                        $city_id = '';
                                        if($Shiprow->city == 'Delhi' || $Shiprow->city == 'Faridabad') {
                                            $city_id = '45';
                                        } else if($Shiprow->city == 'Bangalore') {
                                            $city_id = '46';
                                        } else if($Shiprow->city == 'Pune') {
                                            $city_id = '47';
                                        } else if($Shiprow->city == 'Mumbai') {
                                            $city_id = '48';
                                        } else if($Shiprow->city == 'Gurgaon') {
                                            $city_id = '49';
                                        } else if($Shiprow->city == 'Ghaziabad') {
                                            $city_id = '50';
                                        } else if($Shiprow->city == 'Noida') {
                                            $city_id = '50';
                                        }

                                        if($city_id == $_SESSION['prcity']) {   $sflag = '1'; $checked = 'selected'; } else { $checked = ''; }
                                       
                                        if($Shiprow->primary == 'Yes'){ 
                                           $default_id = $Shiprow->id;
                                        }
                                    
                                        
                                        ?>
                                    	<li>
                                    	    <div class="address-box flex-full <?php if($Shiprow->primary == 'Yes'){ echo 'selected'; } ?>" data-current-add-id="<?php echo $Shiprow->id; ?>">
    											<i class="icn <?php if($Shiprow->primary == 'Yes'){ echo 'icn-correct-red selected'; } ?>"></i>
    											<h3><?php echo $Shiprow->full_name; ?></h3>
    											<address><?php echo $Shiprow->address1.', '.$Shiprow->address2.'<br /> '.$Shiprow->city.', '.$Shiprow->state.', India<br /> '.$Shiprow->postal_code; ?></address>
    											<span class="address-label default"><?php if($Shiprow->primary == 'Yes'){ echo 'Default'; } ?></span>
    										</div>
    									</li>
                                       <?php
                                       $c++;
                                    }
                                }?>

								</ul>
								<?php  
								    if($shipVal->num_rows() >= 1 && $sflag == '1'){
                                        $dyanmicButton = '<a href="javascript:void(0)"  id="address_proceed_button" data-address='.$default_id.'  data-tab="cart-3" class="explore-btn" onclick="get_default_address('.$c.');">Proceed</a>';
                                    } else {
                                        $noaddresserror = '<div><h3 id="empty-address" class="empty-address" style="color:red">Please Add Your Shipping Address</h3></div>';
                                        $dyanmicButton = '<a href="javascript:void(0)"data-tab="cart-3" class="proceed-btn explore-btn" style=" pointer-events: none; cursor: default; opacity: 0.6;">Proceed</a>';
                                    }
                                    ?>
                                <?php echo $noaddresserror; ?>
								<div class="steps-navigation flex-full">
									<a href="javascript:void(0)" data-tab="cart-1"  class="back-btn explore-btn">Back</a>
									<?php echo $dyanmicButton; ?>
									<!--<a href="javascript:void(0)" data-tab="cart-3" class="proceed-btn explore-btn">Proceed</a>-->
								</div>								
							</aside>
							<div class="add-address-block flex-full">								
								<h2 class="border-left">Add New Address</h2>
								<form class="address-form credit-form flex-full" id="addnewAddress">
								    <input type="hidden" name="country" id="country" value="IN"/>
								    <input type="hidden" name="user_id" id="user_id" value="<?php echo $this->data['common_user_id']; ?>"/>
                                    <input type="hidden" name="current_city" id="current_city" value="<?php echo $_SESSION['prcity'] ?>"/>
									<div class="FlowupLabels">
										<div class="fl_wrap">
											<label class='fl_label' for='full_name'>Full Name *</label>
											<input class='fl_input' type='text' id='full_name' />
										</div>
										<div class="fl_wrap">
											<label class='fl_label' for='phone'>Mobile number *</label>
											<input class='fl_input' type='text' id='phone' />
										</div>
										<div class="fl_wrap">
											<label class='fl_label' for='address1'>Address line 1 *</label>
											<input class='fl_input' type='text' id='address1' />
										</div>
										<div class="fl_wrap">
											<label class='fl_label' for='address2'>Address line 2 *</label>
											<input class='fl_input' type='text' id='address2' />
										</div>
										<div class="fl_wrap">
											<!-- <label class='fl_label' for='city'>City *</label> -->
											<select class='fl_input' id='city'  onchange="change_city()">
												<option value="default" selected disabled>City</option>
										        <?php
												for($a=0;$a<count($city_merge);$a++) {
                                                        $cityname = ($city_merge[$a]['list_value'] == 'Delhi NCR') ? 'Delhi' : $city_merge[$a]['list_value'];
                                                    echo '<option value="'.$cityname.'">'.$cityname.'</option>';
                                                }   
												?>
											</select>
										</div>
										<div class="fl_wrap">
											<label class='fl_label' for='state'>State *</label> 
											<input class='fl_input' type='text' id='state' readonly />
										</div>
										<div class="fl_wrap">
											<label class='fl_label' for='postal_code'>Postal code *</label>
											<input class='fl_input' type='text' id='postal_code' />
										</div>										
									</div>
									<div class="default-checkbox checkbox-grp flex-full justify-content-center">
										<input type="checkbox" value="yes" name="make-default" id="make-default">
										<label for="make-default">Make this my default address</label>
									</div>
									<div class="form-submit-btn flex-full justify-content-center">
										<input type="button" onclick="addnewaddress();" name="" class="explore-btn" value="Save">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="cart-3" class="product-payment fixed-rental-plans-section flex-full d-none">
			<div class="fixed-rental-plans-wrapper flex-full align-items-center align-content-center">
				<div class="fixed-rental-plans-steps flex-full">
					<div class="container">
						<ul class="fixed-rental-plans-steps-listing flex-full justify-content-between">
							<li class="complete"><span>Cart</span></li>
							<li class="complete"><span>Address</span></li>
							<li class="active"><span>Payment</span></li>							
						</ul>
					</div>
				</div>
				<div class="cart-main-section flex-full">
					<div class="container">
						<div class="cart-main-wrapper flex-full align-items-start align-content-start">
							<div class="order-confirmation-wrapper flex-full align-items-start align-content-start">
								<div class="order-summary-column-1 order-summary-column flex-full">
									<h2 class="border-left">Order Summary</h2>
									<ul class="order-summary-listing flex-full">
									 <?php  
									    foreach ($cartVal->result() as $CartRow) {
								        $newImg = @explode(',', $CartRow->image);?>
										<li>
											<div class="order-summary-box flex-full">
												<div class="product-img flex-full">
													<img src="<?php echo  base_url().PRODUCTPATH.$newImg[0]; ?>" alt="Product Image" width="106" height="84">
												</div>
												<div class="product-desc flex-full align-items-start align-content-start">
													<h3><?php echo $CartRow->product_name; ?></h3>
													<span class="qty">Quantity <?php echo $CartRow->quantity; ?></span>
												</div>
											</div>
										</li>
										<?php } ?>
									</ul>
								</div>
								<div class="order-summary-column-2 order-summary-column flex-full">
									<h2 class="border-left">Order Details</h2>
									<div class="cart-sidebar-block order-detail-block flex-full">
										<ul class="product-desc-listing flex-full">
											<li>
												<h4>Duration</h4>
												<span>24 Months</span>
											</li>
											<li>
												<h4>Advance Rental</h4>
												<span id="CartAmt1">&#x20B9; <?php echo number_format($cartAmt, 2, '.', ''); ?></span>
											</li>
											<li>
												<h4>Refundable Deposit</h4>
											    <span id="CartSAmt1">&#x20B9; <?php echo number_format($cartSAmt, 2, '.', ''); ?></span>
											</li>
											<li>
												<h4>Coupon Discount</h4>
												<span id="3rdStepDiscount">&#x20B9; <?php echo number_format($disAmt, 2, '.', ''); ?></span>
											</li>
											<li>
												<h4>Coins Redeemed</h4>
												<span class="color-red"  id="CoinsRedeemed">-&#x20B9; <?php echo $_SESSION['Applied_Walled'] > 0 ? $_SESSION['Applied_Walled'] : 0.00; ?></span>
											</li>
											<li>
												<h4>Delivery</h4>
												<span>Free</span>
											</li>
											<li>
												<h4>Total</h4>
												<span id="CartCartGAmt1">&#x20B9; <?php echo number_format($grantAmt, 2, '.', ''); ?></span>
											</li>
										</ul>
									</div>
									<!-- coin balance Redeem -->
									<?php  if($coin_balance->num_rows() > 0) { ?>
									<div class="cf-balance-block flex-full">
										<h3>Your available CF Coins Balance is</h3>
										<span class="coins-amt"><i class="icn icn-coins-gold"></i>
										    <?php echo $coin_balance->row()->topup_amount - $_SESSION['Applied_Walled']; ?>
										</span>
                                            <div class="coins-redeemed flex-full" id="redeemcoinDiv">
											    <label>Coins to be Redeemed</label>
										<?php
										  if(isset($_SESSION['Applied_Walled'])){ ?>
											    <input type="text" id="wallet_amount" name="" value="<?php echo $_SESSION['Applied_Walled']; ?>">
										        <span class="redeem-btn flex-full justify-content-center"><a href="javascript:void(0)" class="explore-btn" onclick="RemoveAppliedWalletAmount('<?php echo $_SESSION['Applied_Walled'] ?>','<?php echo $coin_balance->row()->topup_amount; ?>')">Remove</a></span>
                                           <?php }else{ ?>
											    <input type="text" id="wallet_amount" name="" value="">
										        <span class="redeem-btn flex-full justify-content-center"><a href="javascript:void(0)" onclick="VerifyWalletAndApply()" class="explore-btn">Redeem</a></span>
                                        <?php } ?>
										    </div>
										    <div id="wallet_message"></div>
									</div>
									<?php } else{ ?>
									<!-- coin balance 0 -->
									<div class="cf-balance-block flex-full">
										<h3>Your available CF Coins Balance is</h3>
										<span class="coins-amt"><i class="icn icn-coins-gold"></i>0</span>
										<div class="coins-redeemed disabled flex-full">
											<label>Coins to be Redeemed</label>
											<input type="text" name="" value="0">
										</div>
										<span class="redeem-btn disabled flex-full justify-content-center"><a href="javascript:void(0)" class="explore-btn">Redeem</a></span>
									</div>
									<?php } ?>
									<!-- coin balance remove -->
									<div class="cf-balance-block flex-full d-none">
										<h3>Your available CF Coins Balance is</h3>
										<span class="coins-amt"><i class="icn icn-coins-gold"></i>200</span>
										<div class="coins-redeemed disabled flex-full">
											<label>Coins to be Redeemed</label>
											<input type="text" name="" value="0">
										</div>
										<span class="redeem-btn flex-full justify-content-center"><a href="javascript:void(0)" class="explore-btn">Remove</a></span>
									</div>
								</div>
								<div class="order-summary-column-3 order-summary-column flex-full">
									<h2 class="border-left">Delivery Details</h2>
									<ul class="address-listing flex-full">
										<li id="dynamicSelectedAddress" style="pointer-events:none">
											<div class="address-box selected flex-full" style="pointer-events:none;">
												<h3><i class="icn icn-location"></i>Neha Khanna</h3>
												<address>11/3 Service Rd Mangammanapalya, <br/>Popular Colony, Bommanahalli, <br/>Bengaluru, Karnataka - 560068</address>												
											</div>
										</li>
									</ul>
									<div class="delivery-info flex-full">
										<ul class="delivery-info-listing flex-full">
											<li><span>Your Rental Payment Will Begin From The Date of Delivery.</span></li>
											<li><span>Once the order has been placed, you might be required to share few documents for KYC.<i class="icn icn-help"></i>
												<div class="help-box flex-full">
													<ul class="delivery-info-listing flex-full">
														<li><span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </span></li>
														<li><span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </span></li>
														<li><span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </span></li>
														<li><span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </span></li>
														<li><span>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </span></li>
													</ul>
												</div>
											</span></li>
										</ul>

									</div>
									<div class="opt-checkbox flex-full">
										<input type="checkbox" name="" id="whatsapp-notification" checked>
										<label for="whatsapp-notification">Opt for Whatsapp notification</label>
									</div>
								</div>
							</div>
							<div class="payment-method-wrapper flex-full">
								<h2 class="border-left">Payment Method</h2>
								<form class="payment-form flex-full" name="cartSubmit" method="post" id="cartSubmit" enctype="multipart/form-data" action="site/cart/cartcheckout" >
									<h3>Choose your preffered Payment Method</h3>
									<div class="payment-options-wrapper flex-full">
										<div class="payment-radio radio-grp flex-full">
											<input type="radio" name="payment-radio" value="is_nach" id="card">
											<label for="card">Credit / Debit Card</label>
										</div>
										<div class="payment-radio radio-grp flex-full">
											<input type="radio" name="payment-radio" id="netbanking">
											<label for="netbanking">Netbanking</label>
										</div>
										<div class="payment-radio radio-grp flex-full">
											<input type="radio" name="payment-radio" id="others">
											<label for="others">Others</label>
										</div>
									</div>
									<div class="standing-instruction flex-full">
										<div class="opt-checkbox flex-full">
											<input type="checkbox" name="is_si_selected" id="whatsapp-notification1" >
											<label for="whatsapp-notification1">Opt for Standing Instructions</label>
										</div>
										<ul class="delivery-info-listing flex-full">
											<li><span> Get 100 CF coins and gift vouchers worth &#x20B9;2500.</span></li>
											<li><span> No hassle of reminders and Payment Delays</span></li>
											<li><span> No hassle of reminders and Payment Delays</span></li>
										</ul>
									</div>
									<input type="hidden" name="Ship_address_val" id="Ship_address_val">
									<div class="steps-navigation flex-full">
										<a href="javascript:void(0)" data-tab="cart-2"  class="back-btn explore-btn">Back</a>
										<input type="submit" name="" id="form_submit_button" value="Make Payment" class="explore-btn">
									</div>
									
									<!-- <span class="submit-btn flex-full">	
										
									</span>									 -->
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	
		<?php } else { ?>
		<div id="cart-2" class="address-selection fixed-rental-plans-section flex-full d-none">
			<div class="fixed-rental-plans-wrapper flex-full align-items-center align-content-center">
				<div class="fixed-rental-plans-steps flex-full">
					<div class="container">
						<ul class="fixed-rental-plans-steps-listing flex-full justify-content-between">
							<li class="complete"><span>Cart</span></li>
							<li class="active"><span>Address</span></li>
						</ul>
					</div>
				</div>
				<div class="cart-main-section flex-full">
					<div class="container">
						<div class="cart-main-wrapper flex-full align-items-start align-content-start">
							<div class="add-address-block flex-full">								
								<h2 class="border-left">User Details and Address</h2>
								<form class="address-form credit-form flex-full"  name="offlineUserForm" id="offlineUserForm">
									<div class="FlowupLabels">
										<div class="fl_wrap">
											<label class='fl_label' for='offline_full_name'>Full Name *</label>
											<input class='fl_input' name="offline_full_name" id="offline_full_name" type='text' />
										</div>
										
										<div class="fl_wrap">
											<label class='fl_label' for='offline_email'>Email *</label>
											<input name="offline_email" id="offline_email" class="fl_input" type="text" />
										</div>
										<div class="fl_wrap">
											<label class='fl_label' for='offline_mobile'>Mobile number *</label>
											<input class='fl_input' type='text' name="offline_mobile" id="offline_mobile" />
										</div>
										<div class="fl_wrap">
											<label class='fl_label' for='offline_Alter_phone'>Alternate Mobile number </label>
											<input class='fl_input' type='text' name="offline_mobile"  name="offline_Alter_phone" id="offline_Alter_phone" />
										</div>
										<div class="fl_wrap">
											<label class='fl_label' for='offline_address1'>Address line 1 *</label>
											<input class='fl_input' type='text' name="offline_address1" id="offline_address1" />
										</div>
										<div class="fl_wrap">
											<label class='fl_label' for='offline_address2'>Address line 2 </label>
											<input class='fl_input' type='text' name="offline_address2" id="offline_address2" />
										</div>
										<div class="fl_wrap">
											<!-- <label class='fl_label' for='city'>City *</label> -->
											<select class='fl_input' id='city' name="offline_city"  onchange="change_city()">
												<option value="default" selected disabled>City</option>
										        <?php
												for($a=0;$a<count($city_merge);$a++) {
                                                        $cityname = ($city_merge[$a]['list_value'] == 'Delhi NCR') ? 'Delhi' : $city_merge[$a]['list_value'];
                                                    echo '<option value="'.$cityname.'">'.$cityname.'</option>';
                                                }   
												?>
											</select>
										</div>
										<div class="fl_wrap">
											<label class='fl_label' for='state'>State *</label> 
											<input class='fl_input' type='text'  name="offline_state" id='state' readonly />
										</div>
										<div class="fl_wrap">
											<label class='fl_label' for='offline_postal_code'>Postal code *</label>
											<input class='fl_input' type='text' name="offline_postal_code" id="offline_postal_code" />
										</div>	
										<div class="fl_wrap">
											<label class='fl_label' for='rental_amount'>Customer Paid Amount</label>
											<input class='fl_input' type='text' name="rental_amount" id="rental_amount" />
										</div>	
									</div>
									<div class="form-submit-btn flex-full justify-content-center">
									   	<a href="javascript:void(0)" data-tab="cart-1"  class="back-btn explore-btn">Back</a>
										<input type="submit" onclick="return CreateUserAndPlaceOrder()" id="button-submit-merchant" class="explore-btn" value="Proceed">
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php }}?>
	</div>
	
	<input name="user_id" value="<?php echo $this->data['common_user_id']; ?>" type="hidden">
	<input name="cart_amount" id="cart_amount" value="<?php echo number_format($cartAmt, 2, '.', ''); ?>" type="hidden">
    <?php if ($disAmt > 0) { ?>
      <input name="CouponCode" id="CouponCode" value="<?php echo $cartVal->row()->couponCode; ?>" type="hidden">
        <input name="Coupon_id" id="Coupon_id" value="<?php echo $cartVal->row()->couponID; ?>" type="hidden">
        <input name="couponType" id="couponType" value="<?php echo $cartVal->row()->coupontype; ?>" type="hidden">
    <?php } else { ?>
        <input name="CouponCode" id="CouponCode" value="" type="hidden">
        <input name="Coupon_id" id="Coupon_id" value="0" type="hidden">
        <input name="couponType" id="couponType" value="" type="hidden">
    <?php } ?>
 
	<script>
	    function checkCode(){
	        alert("i have clicked");
	    }
	</script>

<?php $this->load->view('site/templates/footer'); ?>
</div>
</html>