<?php
//$this->load->view('site/templates/header_new');
$this->load->view('site/templates/header_inner');
?>

<div class="page_section_offset mobileheight"> 
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
           <br><br>
            <aside class="col-lg-4 col-md-4 col-sm-4 p_top_4">
            </aside>
            <section class="col-lg-4 col-md-4 col-sm-4">
                <h2 class="fw_light second_font color_dark m_bottom_27 tt_uppercase t_align_c">Payment Success</h2>
            </section>
            <div style="clear:both;"></div>
            <section class="col-lg-12">
            <div class="payment_success" align="center">
                <h3 style="color:green">Thank you! Your transaction is successful.</h3><br>
                <p>You can note down the following details for future reference.</p><br>
                <table>
                	<tr>
	                	<td align="left" style="padding-right:3px;">Transaction Reference Number:</td><td align="left"><?php echo $txnRefNumber; ?></td>
	                </tr>
	                <tr>
	                	<td align="left" style="padding-right:3px;">Transaction Date:</td><td align="left"><?php date_default_timezone_set('Asia/Kolkata'); ?><?php echo date("F j, Y, g:i a"); ?></td>
	                </tr>
	                <tr>
	                	<td align="left" style="padding-right:3px;">PG Transaction ID:</td><td align="left"><?php echo $citrusTxnID; ?></td>
	                </tr>
	                <tr>
	                	<td align="left" style="padding-right:3px;">Amount Paid:</span><td align="left"><?php echo $amountPaid; ?>&nbsp;<?php echo CITRUS_CURRENCY_TYPE; ?></td>
	                </tr>
	        </table>
	        <br>
                <p>Transaction details have been emailed to - <b><i><?php echo $userEmail; ?></i></b></p><br> 
            </div> 
            </section>               
        </div>
    </div>
</div>
<?php
$this->load->view('site/templates/footer');
?>
