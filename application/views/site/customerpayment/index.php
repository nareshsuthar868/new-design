<?php $this->load->view('site/templates/header_inner'); ?>

<div class="page-wrapper customer-payments-page flex-full">
	<!-- Inner page heading starts -->
	<section class="innerpage-heading flex-full justify-content-center">
		<div class="container">
			<div class="innerpage-heading-wrapper flex-full justify-content-center bg-img position-relative" style="background-image: url(images/innerpage-banner.jpg);">
				<div class="innerpage-description flex-full justify-content-center text-center">
					<h1>Pay your Dues</h1>
					<ul class="breadcrumbs-listing flex-full align-items-center justify-content-center">
						<li class="root-link"><a href="<?php echo base_url(); ?>">Home</a></li>				
						<li class="child-link"><a href="javascript:void(0)">Help</a></li>
						<li class="current-link"><a href="javascript:void(0)">Customer Payment</a></li>
					</ul>						
				</div>
			</div>
		</div>
	</section>
	<!-- Inner page heading ends -->
	<div class="customer-payments-main flex-full">
		<div class="container"> 
			<div class="customer-payments-main-wrapper flex-full align-items-start align-content-start">
				<aside class="payment-box flex-full align-items-start align-content-start">
					<div class="payment-amount flex-full justify-content-center text-center">
						<label class="w-100">Your Outstanding Amount: <span class="price w-100">&#x20B9; 2375.00</span></label>						
						<a class="explore-btn" href="javascript:void(0)">Check Details</a>
					</div>
					<div class="payment-info flex-full justify-content-center text-center">
						<p>Opt for the standing insrtuctions to avoid hassle of manual payment every month.</p>
					</div>
					<div class="standing-instruction flex-full nach_field" style="display: none;border-top: 1px dashed #c9c9c9; margin-top: 25px; padding-top: 25px;">
						<div class="opt-checkbox flex-full">
							<input type="checkbox" name="" id="whatsapp-notification1" checked="">
							<label for="whatsapp-notification1">Opt for Whatsapp
								notification</label>
						</div>
						<ul class="delivery-info-listing flex-full">
							<li><span>Get 100 CF coins and gift vouchers worth Rs <?php echo $voucher_data->row()->reward; ?></span></li>
							<li><span> No hassle of reminders and Payment Delays</span></li>
							<li><span> No hassle of reminders and Payment Delays</span></li>
						</ul>
					</div>
				</aside>
				<div class="customer-payment-form-wrapper flex-full">
					<form class="customer-payment-form credit-form flex-full" method="post"  action="customerpayment/CustomerPayment" id="customer_payment_form">
						<div class="radio-grp-wrapper flex-full">
							<div class="radio-grp">
								<input type="radio" name="payment_option" value="one_time_payment" id="one_time_payment" onchange="change_payment_type('one_time_payment')" checked>
								<label for="one_time_payment">One Time Payment</label>
							</div>
							<div class="radio-grp">
								<input type="radio" name="payment_option" value="standing_instructions" id="standing_instructions" onchange="change_payment_type('standing_instructions')">
								<label for="standing_instructions">Standing Instructions</label>
							</div>
						</div>
						<div class="FlowupLabels">
							<div class="form-grp flex-full">
								<div class="fl_wrap form-grp-inner">
									<label class='fl_label' for='user_first_name'>First name *</label>
									<input class='fl_input' type='text' id='user_first_name' name="user_first_name" />
								</div>
								<div class="fl_wrap form-grp-inner">
									<label class='fl_label' for='user_last_name'>Last Name (Optional)</label>
									<input class='fl_input' type='text' id='user_last_name' name="user_last_name" />
								</div>
							</div>							
							<div class="fl_wrap form-grp flex-full">
								<label class='fl_label' for='user_email'>Registerd Email (associated with your account at cityfurnish.com)</label>
								<input class='fl_input' type='email' id='user_email' name="user_email" value="<?php if(!empty($userEmail)){ echo $userEmail; }else{ echo set_value('user_email');} ?>" <?php if(!empty($userID)){ echo "readonly"; } ?> />
							</div>
							<div class="form-grp flex-full">
								<div class="fl_wrap form-grp-inner">
									<label class='fl_label' for='user_amount'>Amount (Rs.)</label>
									<input class='fl_input' type='text' id='user_amount' name="user_amount" />
								</div>
								<div class="fl_wrap form-grp-inner">
									<label class='fl_label' for='user_invoice_number'>Invoice Number (Optional)</label>
									<input class='fl_input' type='text' id='user_invoice_number' name="user_invoice_number" />
								</div>
							</div>							
							<div class="fl_wrap form-grp flex-full">
								<label class='fl_label' for='user_notes'>Notes (Optional)</label>
								<input class='fl_input' type='text' id='user_notes' name="user_notes" />
							</div>
					        <div class="fl_wrap form-grp nach_field" style="display:none">
                                <label>Select Recurring Months</label>
                                <select name="recurring_months" id="recurring_months" class="fl_input">
                                <?php
                                    for ($i= 1; $i <= 24 ; $i++) { 
                                        echo "<option value=".$i.">".$i." Months</option>";
                                    }
                                ?>
                                </select>
                            </div>
                            <div class="fl_wrap form-grp nach_field" style="display:none">
                                <div class="paymentradio cart-payment" id="payment_modeRadio">
                                    <label style="margin-bottom: 15px; margin-top: 15px;">Select Payment Mode for Standing Instructions</label>
									<div class="radio-grp-wrapper flex-full">
										<div class="radio-grp">
											<input type="radio" id="credit_card" name="payment_mode" value="credit_cart">
											<label for="credit_card">Credit Card</label>
										</div>
										<div class="radio-grp">
											<input type="radio" id="debit_card" name="payment_mode" value="debit_cart">
											<label for="debit_card">Debit Card</label>
										</div>
										<div class="radio-grp">
											<input type="radio" id="net_banking" name="payment_mode" value="net_banking">
											<label for="net_banking">Net Banking</label>
										</div>
									</div>
                                </div>
                            </div>
							<div class="fl_wrap form-grp nach_field" id="si_debit_banks" style="display:none">
								<!--<label>Select Bank </label>-->
								<label style="font-size:10px;">(In case your bank is not listed, please choose some other payment mode)</label>
                                <select name="debit_card_bank" id="debit_card_bank" class="fl_input">
                                    <option selected value="none">Select Bank</option>
                                    <option>ICICI Bank</option>
                                    <option>Kotak Bank</option> 
                                    <option>Canara Bank</option> 
                                    <option>Citibank</option>
                                    <option>Corporation Bank</option>   
                                    <option>Dena Bank</option>   
                                    <option>Standard Chartered Bank</option>   
                                    <option>Deutsche Bank</option>   
                                </select>
							</div>
						</div>
						<div class="submit-btn flex-full justify-content-center">
							<input type="submit" name="" class="explore-btn proceed" value="Proceed">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$this->load->view('site/templates/footer');
?>
</div>
</body>
</html>