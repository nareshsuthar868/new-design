<?php
$this->load->view('site/templates/header_inner');
?>

<div class="page_section_offset">
  <section class="innerbanner">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <h1>Pay Now</h1>
          <ul class="breadcrumb">
            <li><a href="<?php echo  base_url();?>">Home</a></li>
             <li><a href="#">Help</a></li>
            <li class="active">Customer Payment</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <div class="container">
    <div class="row ">
      <section class="col-lg-4 col-md-4 col-sm-6 col-sm-offset-3 col-md-offset-4"> 
        <!--<h2 class="fw_light second_font color_dark m_bottom_27 tt_uppercase t_align_c">Pay Now</h2>-->
        <form method="post" action="customerpayment/check_user" class="frm clearfix" id="customer_payment_form">
          <div class="frmpayment">
            <div class="m_bottom_15">
              <label>First Name</label>
              <input type="text"  name="user_first_name" id="user_first_name" class="form-control" autofocus="autofocus" value="<?php echo set_value('user_first_name'); ?>">
            </div>
            <div class="m_bottom_15">
              <label>Last Name (Optional)</label>
              <input  type="text"  name="user_last_name" id="user_last_name" class="form-control" value="<?php echo set_value('user_last_name'); ?>">
            </div>
            <div class="m_bottom_15">
              <label>Email (associated with your account at cityfurnish.com)</label>
              <input required type="email" name="user_email" id="user_email" class="form-control" value="<?php if(!empty($userEmail)){ echo $userEmail; }else{ echo set_value('user_email');} ?>" <?php if(!empty($userID)){ echo "readonly"; } ?>>
            </div>
            <div class="m_bottom_15">
              <label>Amount (Rs.)</label>
              <input type="text" name="user_amount" id="user_amount" class="form-control" value="<?php echo set_value('user_amount'); ?>">
            </div>
            <div class="m_bottom_15">
              <label>Invoice Number (Optional)</label>
              <input type="text" name="user_invoice_number" id="user_invoice_number" class="form-control" value="<?php echo set_value('user_invoice_number'); ?>">
            </div>
            <div class="m_bottom_15">
              <label>Notes (Optional)</label>
              <input type="text" name="user_notes" id="user_notes" class="form-control" value="<?php echo set_value('user_notes'); ?>">
              <?php if(validation_errors() != ''){?>
                  <br>
                  <div id="validationErr" class="alert_box warning m_bottom_10 relative fw_light"> 
                    <!--<script>setTimeout("hideErrDiv('validationErr')", 3000);</script>--> 
                    <span class="d_inline_m second_font fs_medium color_red d_md_block">
                        <?php //echo validation_errors();?></span> 
                  </div>
              <?php }?>
            </div>
            <div> 
              <!--<button class="t_align_c tt_uppercase w_full second_font d_block fs_medium button_type_2 lbrown tr_all">Pay Now</button>-->
              <input type="submit" name="submit" value="Proceed to Pay" class="btn-check"/>
             <!--  <button type="submit"  class="btn-check" id="submit" onclick="return customerpayment()">Proceed to Pay</button> -->
            </div>
          </div>
        </form>
      </section>
    </div>
  </div>
</div>
<?php
$this->load->view('site/templates/footer');
?>
