<?php $this->load->view('site/templates/header_inner'); ?>
	<div class="page-wrapper flex-full">
	    <?php
	        if(!empty($banners)){
	    ?>
	            <section class="home-banner flex-full bg-img position-relative">
        			<div class="banner-slider h-100 w-100 slider-for" id="banner-slider">
        			    <?php
        			        foreach($banners as $banner){
        			    ?>
        			            <div class="slide bg-img" style="background-image: url(<?php echo base_url(); ?>images/banner/<?php echo $banner->image ?>);"></div>
        			    <?php
        			        }
        			    ?>
        			</div>			
        			<div class="home-banner-wrapper flex-full container align-items-center">
        				<div class="banner-text flex-full">
        					<h1><strong>Premium Furniture</strong> at Affordable Monthly Rentals</h1>
        					<a href="javascript:void(0)" class="explore-btn">Explore</a>
        				</div>
        				<div class="banner-tags-wrapper flex-full justify-content-center d-none-xs">
        					<div class="banner-tags-listing flex-full justify-content-center slider-nav" id="banner-tags-listing">
        					    <?php
                			        foreach($banners as $banner){
                			    ?>
                			            <div class="slide banner-tags"><span><?php echo $banner->text ?></span></div>
                			    <?php
                			        }
                			    ?>
        					</div>
        				</div>
        			</div>
        		</section>	
	    <?php
	        }
	    ?>
			
		<!-- main features section starts -->
		<section class="main-features flex-full">
			<div class="container">
				<div class="main-features-wrapper flex-full">
					<ul class="main-features-listing flex-full justify-content-center align-items-start align-content-start">
					    <?php foreach ($categoriesTree as $row) { if ($row->cat_name == 'Packages' || $row->cat_name == 'Home Furniture' || $row->cat_name == 'Electronics' || $row->cat_name == 'Office Furniture' || $row->cat_name == 'Fitness') { 
					    if($row->image != ''){ $catImage = CDN_URL . 'images/category/' . $row->image;}
					    else{ $catImage = CDN_URL . 'images/wishlist_img_1.jpg';
					    } ?>
						<li>
							<a href="<?php echo base_url(); ?><?php echo $cat_slug; ?>/<?php echo $row->seourl; ?>" class="features-box flex-full justify-content-center text-center">
								<!-- <i class="icn icn-combos"></i> -->
								<img src="<?php echo $catImage; ?>" alt="<?php echo $row->cat_name; ?>">
								<span><?php echo $row->cat_name; ?></span>
							</a>
						</li>
						<?php } } ?>
					</ul>
				</div>				
			</div>
		</section>
		<!-- main features section ends -->
		<!-- home offer (for mobile) section starts -->
		<div class="home-offer flex-full d-none d-flex-xs">
			<div class="home-offer-wrapper flex-full">
				<small class="w-100">Get your First</small>
				<h2>2 Months<span class="text-uppercase w-100">Rent free</span></h2>
			</div>
			<div class="know-more flex-full">
				<a href="javascript:void(0)">Know more</a>
				<span class="w-100">*Limited period offer.</span>
			</div>
		</div>
		<!-- home offer (for mobile) section ends -->
		<!-- fixed rental plans starts -->
		<!-- <section class="rental-palns-section flex-full">
			<div class="container">
				<div class="rental-plans-wrapper flex-full">
					<div class="scrollable-items flex-full align-items-start align-content-start">
						<div class="rental-items-listing mCustomScrollbar content w-100" style="max-height: 364px;overflow-y: auto;">
							<ul class="flex-full">
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="<?php echo CDN_URL; ?>images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="<?php echo CDN_URL; ?>images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="<?php echo CDN_URL; ?>images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="<?php echo CDN_URL; ?>images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="<?php echo CDN_URL; ?>images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="<?php echo CDN_URL; ?>images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="<?php echo CDN_URL; ?>images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="<?php echo CDN_URL; ?>images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="<?php echo CDN_URL; ?>images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="rental-box-area flex-full align-items-start align-content-start">
						<ul class="rental-box-listing flex-full">
							<li>
								<div class="rental-box flex-full justify-content-center align-items-center">
									<img src="<?php echo CDN_URL; ?>images/rental-items2.png" alt="rental items">
								</div>
							</li>
							<li>
								<div class="rental-box flex-full justify-content-center align-items-center">
									<img src="<?php echo CDN_URL; ?>images/rental-items3.png" alt="rental items">
								</div>
							</li>
							<li>
								<div class="rental-box flex-full justify-content-center align-items-center">
									<img src="<?php echo CDN_URL; ?>images/rental-items2.png" alt="rental items">
								</div>
							</li>
							<li>
								<div class="rental-box flex-full justify-content-center align-items-center">
									<img src="<?php echo CDN_URL; ?>images/rental-items3.png" alt="rental items">
								</div>
							</li>
							<li>
								<div class="rental-box flex-full justify-content-center align-items-center">
									<img src="<?php echo CDN_URL; ?>images/rental-items2.png" alt="rental items">
								</div>
							</li>
							<li>
								<div class="rental-box flex-full justify-content-center align-items-center">
									<img src="<?php echo CDN_URL; ?>images/rental-items3.png" alt="rental items">
								</div>
							</li>							
						</ul>
					</div>
					<div class="rental-heading flex-full align-items-center align-content-center">
						<h2 class="w-100">Fixed Rental Plans</h2>
						<p class="w-100">Lorem Ipsum is simply dummy text of the printing and typesetting </p>
						<div class="rental-plans flex-full">
							<div class="rental-radio-grp flex-full">
								<input type="radio" name="rental-plan" id="rent-4999" checked>
								<label for="rent-4999">&#x20B9;<strong>4999/</strong>month</label>
							</div>
							<div class="rental-radio-grp flex-full">
								<input type="radio" name="rental-plan" id="rent-3999">
								<label for="rent-3999">&#x20B9;<strong>3999/</strong>month</label>
							</div>
							<div class="rental-radio-grp flex-full">
								<input type="radio" name="rental-plan" id="rent-1999">
								<label for="rent-1999">&#x20B9;<strong>1999/</strong>month</label>
							</div>
						</div>
						<a href="javascript:void(0)" class="explore-btn">Explore</a>
					</div>
				</div>
			</div>
		</section> -->
		<!-- fixed rental plans ends -->
		<!-- home service starts -->
		<!-- <section class="services-section flex-full">
			<div class="container">
				<div class="services-wrapper flex-full">
					<h2 class="w-100 text-center">You are Covered</h2>
					<div class="service-tab flex-full align-items-center align-content-center">
						<div class="tab-links-part flex-full">
							<ul class="tabs flex-full">
								<li class="tab-link current" data-tab="tab-1"><span>Free Relocation</span></li>
								<li class="tab-link" data-tab="tab-2"><span>Free Swap</span></li>
								<li class="tab-link" data-tab="tab-3"><span>Quality of items</span></li>
								<li class="tab-link" data-tab="tab-4"><span>Damage Waiver</span></li>
							</ul>
						</div>
						<div class="tab-content-part flex-full">
							<div id="tab-1" class="tab-content current">
								<figure class="flex-full justify-content-center">							
									<img src="<?php echo CDN_URL; ?>images/service-vector.webp" alt="service-vector">
								</figure>
							</div>
							<div id="tab-2" class="tab-content">
								<figure class="flex-full justify-content-center">							
									<img src="<?php echo CDN_URL; ?>images/service-vector.webp" alt="service-vector">
								</figure>
							</div>
							<div id="tab-3" class="tab-content">
								<figure class="flex-full justify-content-center">							
									<img src="<?php echo CDN_URL; ?>images/service-vector.webp" alt="service-vector">
								</figure>
							</div>
							<div id="tab-4" class="tab-content">
								<figure class="flex-full justify-content-center">							
									<img src="<?php echo CDN_URL; ?>images/service-vector.webp" alt="service-vector">
								</figure>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section> -->
		<!-- home service ends -->
		<!-- trending products starts -->
		<section class="trending-products-section flex-full">
			<div class="container">
				<div class="trending-products-wrapper flex-full">
					<h2 class="w-100 text-center">Trending Products</h2>
					<div class="trending-products-tab flex-full">
						<div class="tab-links-part flex-full">
							<ul class="trending flex-full justify-content-center">
							   <?php 
							   $tab = 5;
							   $count=0; foreach ($bestSelling as $sellingCat){ if($sellingCat->cat_name != 'Addon' && $sellingCat->cat_name != 'Office Furniture' && $sellingCat->cat_name != 'All' && $sellingCat->cat_name != 'Fitness' && $sellingCat->cat_name != 'Cred'){ ?>
								<li class="tab-link <?php echo ($count == 0) ? 'current' : ''; ?>" data-tab="tab-<?php echo $tab; ?>"><span><?php echo $sellingCat->cat_name; ?></span></li>
								<!--<li class="tab-link" data-tab="tab-6"><span>Electronics</span></li>-->
								<!--<li class="tab-link" data-tab="tab-7"><span>Kids Furniture</span></li>-->
								<?php } ?>
                                <?php $tab++; $count++;} ?>
							</ul>
						</div>
						<div class="tab-content-part flex-full">
						 <?php 
						 $tab = 5;
						 $count=0; 
						 foreach ($bestSelling as $sellingCat){
						  //   if($sellingCat->cat_name == 'Our Picks'){
						 ?>
						        <div id="tab-<?php echo $tab; ?>" class="tab-content trending-content  <?php echo ($count == 0) ? 'current' : ''; ?> ">
							    <div class="trending-product-slider w-100">
							    <?php 
							    if(!empty($sellingCat->categoryproducts)){ 
							        $count = 0; 
							        foreach ($sellingCat->categoryproducts as $productDetails){
							                if($productDetails->image != ''){
							                    $proImage = explode(',', $productDetails->image)[0]; 
							                }
							    ?>
									<div class="slide">
										<div class="product-single flex-full position-relative">
											<div class="product-image flex-full position-relative">
												<a href="javascript:void(0)" class="flex-full position-relative h-100">
													<amp-img src="<?php echo CDN_URL; ?>images/product/Copressed Images/<?php echo $proImage; ?>" alt="Product Image" layout="responsive" width="300" height="183"></amp-img>
												</a>
											</div>
											<div class="product-description flex-full">
												<div class="product-description-wrapper flex-full">
													<h3><a href="<?php echo base_url(); ?>things/<?php echo $productDetails->id; ?>/<?php echo $productDetails->seourl ;?>"><?php echo $productDetails->product_name; ?></a></h3>
													<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i>  <?php echo $productDetails->price; ?></strong> / mon</p>
												</div>
											</div>
										</div>
									</div>
								<?php $count++; } } ?>
								</div>
							</div>
						 <?php
						  //   }
						  
						 ?>
							<?php $tab++; }  ?>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- trending products ends -->

		<section id="" class="benefits-section flex-full">
        <div class="container">
          <h2 class="w-100 text-center border-center">Benefits</h2>
          <ul class="benefits-listing flex-full">
            <li>
              <i class="icn icn-free-delivery"></i>
              <span>Free Delivery & Install</span>
            </li>
            <li>
              <i class="icn icn-free-relocation"></i>
              <span>Free Relocation</span>
            </li>
            <li>
              <i class="icn icn-free-upgrade"></i>
              <span>Free Upgrade</span>
            </li>
            <li>
              <i class="icn icn-free-quality-product"></i>
              <span>Quality products in Mint condiition</span>
            </li>
            <li>
              <i class="icn icn-free-damage-waiver"></i>
              <span>Damage Waiver</span>
            </li>
          </ul>
        </div>
      </section>

		<!-- Testimonial starts -->
		<?php if(!empty($clreviewList)){ ?>
		<section class="testimonial-section flex-full home-testimonial">
			<div class="container">
				<div class="testimonial-wrapper flex-full align-items-center">
					<h2>What our Customer's say..</h2>
					<div class="home-testimonial-slider w-100">
					    <?php foreach ($clreviewList as $review){ $reImage=''; if($review->image != ''){$reImage = explode(',', $review->image)[0]; }?>
						<div class="slide">
							<div class="testimonial-content-wrapper flex-full align-items-center align-content-center">
								<figure class="flex-full">
									<amp-img src="<?php echo CDN_URL; ?>images/product/<?php echo $reImage; ?>" alt="Product Image" width="360" height="227"></amp-img>
								</figure>
								<div class="testimonial-content flex-full">
									<p><?php echo substr($review->excerpt,0,300 ); if(strlen($review->excerpt) > 300){ echo '....';}?></p>
									<span class="testimonial-name flex-full justify-content-center align-items-center"><strong><?php $custArray=explode(',',$review->product_name);if(isset($custArray[0])){ echo $custArray[0]; }  ?></strong><?php if(isset($custArray[0])){ echo $custArray[1]; } ?> </span>
								</div>
							</div>
						</div>
					    <?php } ?>
					</div>				
				</div>			
			</div>
		</section>
		<?php  } ?>
		<!-- Testimonial ends -->
		
	</div>

<?php $this->load->view('site/templates/footer'); ?>
</div>
<script>
    $(".banner_slides").on('click', function(){
        console.log($(this)); 
    });
</script>
</body>
</html>