<?php
$this->load->view('site/templates/header_new');

?>
<script type="text/javascript">
var error_message = "<?php if(isset($_SESSION['user-error-msg'])){ echo $_SESSION['user-error-msg']; unset($_SESSION['user-error-msg']); }?>";
if(error_message){
    sweetAlert('Error!',error_message, 'error');
}
var success_message = "<?php if(isset($_SESSION['user-success-msg'])){ echo $_SESSION['user-success-msg']; unset($_SESSION['user-success-msg']); }?>";
if(success_message){
    sweetAlert('Success!',success_message, 'success');
}

</script>
<script type="text/javascript">
 
  var key = $('#key').val();
   var logout = $('#logout').val();
   if(key =='key'){
                    toastr.options = {
                        "positionClass": "toast-bottom-right", 
                    }
                    toastr.info('You Are Logged In........', 'Welcome To City Furnish');
                }

                if(logout =='logout')
                {
                  toastr.options = {
                        "positionClass": "toast-bottom-right",
                    }

                    toastr.warning('You Are Logged Out........', 'Thank You');

                }
</script>


<?php if (!$this->session->userdata("prcity")) { ?>
<div id="my-Modal" class="modal fade citymodel" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">        
       <h5>Select City</h5>
      </div>
        <div class="modal-body">
                <div class="col-xs-3">
                    <a href="javascript:void(0)" onclick="setcity(45)">
                        <div class="lgcityicn">
                          <img src="<?php echo CDN_URL; ?>/images/lg-city-delhi.png" alt="Delhi" /></div>
                        <span>Delhi NCR</span>
                    </a>  
                </div>
                <div class="col-xs-3">
                    <a href="javascript:void(0)" onclick="setcity(46)">
                        <div class="lgcityicn">
                          <img src="<?php echo CDN_URL; ?>/images/lg-city-banglore.png" alt="Bangalore" /></div>
                        <span>Bangalore</span>
                    </a>
                </div>
                <div class="col-xs-3">
                    <a href="javascript:void(0)" onclick="setcity(47)">
                        <div class="lgcityicn"><img src="<?php echo CDN_URL; ?>/images/lg-city-pune.png" alt="Pune" /></div>
                        <span>Pune</span>
                    </a>
                </div>
                <div class="col-xs-3">
                    <a href="javascript:void(0)" onclick="setcity(48)">
                        <div class="lgcityicn"><img src="<?php echo CDN_URL; ?>/images/lg-city-mumbai.png" alt="Mumbai" /></div>
                        <span>Mumbai</span>
                    </a>
                </div>
            </div>
    </div>

  </div>
</div>
<?php } ?>
<!-- header top banner -->
<section class="hedertopbanner">
  <figure> <img src="<?php echo CDN_URL; ?>images/top-slider-banner.jpg" alt="Product-banner">
    <figcaption class="captionheadertitle">
      <h1><span>I Live on rent.. So does </span> my furniture</h1>
      <a href="<?php echo base_url() ?>shopby/rental-packages" class="btn btn-default">Explore Offerings</a> </figcaption>
  </figure>
  
  <div class="header_top_part visible-xs">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12"> 
            <p> <span><img src="<?php echo CDN_URL; ?>images/top-percent-icn.svg" alt="percenticn"></span> <span>SUMMER OFFER - UPTO 100% OFF ON FIRST MONTH RENT! OFFER ENDING TONIGHT. <a style="color:white;" href="<?php echo base_url(); ?>pages/offers"> <u> KNOW MORE..</u></a></span> </p>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- header top banner --> 
<!-- product catagory section -->
<section class="productcategoryrow" id="productdown">
  <section class="container">
    <section class="row">
      <h2>Popular Categories</h2>
      <?php //print_r($categoriesTree);exit;
      foreach ($categoriesTree as $row) {
          // if ($row->cat_name != '' && $row->cat_name != 'Our Picks' && $row->cat_name != 'Addon' && $row->cat_name != 'Office Furniture' && $row->cat_name != 'Fitness') {
          if ($row->cat_name == 'Packages' || $row->cat_name == 'Home Furniture' || $row->cat_name == 'Electronics') {
              if($row->image != ''){
                  $catImage = CDN_URL . 'images/category/' . $row->image;
              }else{
                  $catImage = CDN_URL . 'images/wishlist_img_1.jpg';
              }
          ?>
        <aside class="col-sm-6 col-md-4 col-lg-4 col-xs-4 productcat">
          <figure><a  href="<?php echo base_url(); ?>shopby/<?php echo $row->seourl; ?>">
            <img src="<?php echo $catImage; ?>" alt="<?php echo $row->cat_name; ?>"></a></figure>
          <article><a  href="<?php echo base_url(); ?>shopby/<?php echo $row->seourl; ?>">
            <h3><?php echo $row->cat_name; ?></h3>
            <p><?php echo strip_tags(substr($row->page_description,0,100))."."; ?></p></a>
             <a href="<?php echo base_url(); ?>shopby/<?php echo $row->seourl; ?>" class="btn btn-primary hidden-xs">RENT NOW</a> 
          </article>
        </aside>
      <?php
        }
      } ?>
    </section>
  </section>
</section>
<!-- product catagory section --> 

<!-- new collecton section -->
<?php if($caption_list->visible == 'yes') { ?>
<section class="section_offset nwcollectionbg hidden-xs">
  <section class="overlyablackbg">
    <div class="container">
      <div class="row">
        <h2><?php echo $caption_list->heading; ?></h2>
        <aside class="whitecentercol">
          <article class="bgwhitetrans">
<h4>Celebrate IPL with us</h4>
            <h4>
             <?php echo $caption_list->caption;?> <strong><?php echo $caption_list->discount."%"; ?></strong> Off MONTHLY</h4>
            <a href="<?php echo $caption_list->url; ?>" class="btn-default">Rent Now</a> </article>
        </aside>
      </div>
    </div>
  </section>
</section>
<?php }?>

<!--best selling section-->
<?php if(!empty($bestSelling)){ ?>
        <section class="section_offset bestsellingrow hidden-xs">
            <div class="container">
              <div class="row"> 
                <!-- new tab section -->
                <h2>Trending Products</h2>
                <ul class="nav nav-tabs">
                <?php $count = 0;  //print_r($bestSel);exit;
                    foreach ($bestSelling as $sellingCat){ 
                      if($sellingCat->cat_name != 'Addon' && $sellingCat->cat_name != 'Office Furniture' && $sellingCat->cat_name != 'All' && $sellingCat->cat_name != 'Fitness'){ ?> 
                      <li class="<?php echo ($count == 0) ? 'active' : ''; ?>">
                          <a data-toggle="tab" href="#<?php echo $sellingCat->id; ?>"><?php echo $sellingCat->cat_name; ?></a>
                      </li>
                      <?php } ?>
                    <?php $count++;} ?>
                </ul>
                <div class="tab-content">
                    <?php $count = 0; foreach ($bestSelling as $sellingCat){ ?>
                    <div id="<?php echo $sellingCat->id; ?>" class="tab-pane fade in <?php echo ($count == 0) ? 'active' : ''; ?>"> 
                      <?php if(!empty($sellingCat->categoryproducts)){ 
                        $count = 0;
                        foreach ($sellingCat->categoryproducts as $productDetails){
                            if($count >= 3)break;
                            $count++;
                            $proImage = '';
                            if($productDetails->image != ''){
                                $proImage = explode(',', $productDetails->image)[0];
                            } ?>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 hidden-xs">
                                <a href="<?php echo base_url(); ?>things/<?php echo $productDetails->id; ?>/<?php echo url_title($productDetails->product_name, '-'); ?>">
                                  <figure class="relative wrapper scale_image_container m_bottom_30 r_image_container"> 
                                    <img src="<?php echo CDN_URL; ?>images/product/Copressed Images/<?php echo $proImage; ?>" alt="<?php echo $productDetails->product_name; ?>"> 
                                    <figcaption class="caption_type_1 tr_all">
                                      <div class="caption_inner"> 
                                        <strong><?php echo $productDetails->product_name; ?></strong> 
                                        <strong>&#8377; <?php echo $productDetails->price; ?></strong>
                                        <!--<p>(Belle queen bed, One mattress)</p>-->
                                      </div>
                                    </figcaption>
                                  </figure>
                                </a>
                            </div>
                        <?php } ?>
                      <?php } ?>
                    </div>
                    <?php $count++;} ?>
                </div>
              </div>
            </div>
         </section>
<?php } ?>
<!--best selling section-->

<!--best selling section Mobile-->
<?php if(!empty($bestSelling)){ ?>
        <section class="section_offset bestsellingrow bestsellingrow-mobile visible-xs">
            <div class="container">
              <div class="row"> 
                <!-- new tab section -->
                <h2>Trending Products</h2>
                 <div class="tabcenterslider">
                  <?php 
                  foreach ($bestSelling as $sellingCat){
                     if($sellingCat->cat_name == 'Packages') { 
                      //print_r($sellingCat->categoryproducts);
                      foreach ($sellingCat->categoryproducts as $cp) { 
                        $image = explode(',', $cp->image); ?>
                        <div>
                         <div> 
                          <a href="<?php echo base_url()?>things/<?php echo $cp->id ;?>/<?php echo $cp->seourl ;?>">
                          <figure class="relative wrapper scale_image_container"> 
                            <img src="<?php echo CDN_URL; ?>images/product/<?php echo $image[0]; ?>" alt="<?php echo $sellingCat->product_name?>"> 
                            <!--caption-->
                            <figcaption class="caption_type_1 tr_all">
                              <div class="caption_inner"> <strong><?php echo $cp->product_name?></strong> <strong>&#8377; <?php echo $cp->sale_price?></strong>
                                <p><?php echo $cp->meta_title?></p>
                              </div>
                            </figcaption>
                          </figure>
                          </a> 
                        </div>
                       </div>
                      <?php  } 
                    } 
                   } ?>
                 </div>
               </div>
            </div>
         </section>
<?php } ?>
<!-- Best selling section Mobile -->

<!-- our promise scection -->
<div class="section_offset ourpromiserow">
  <div class="container p_bottom_10 t_align_c">
    <div class="row sh_container p_bottom_10">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2>Our Promise</h2>
        <!-- <h4 class="scheme_color_red m_bottom_20 second_font" style="font-size:1.1em">You make yourself at home, leave other things to us</h4>--> 
      </div>
    </div>
    <div class="row">
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-3 m_bottom_30 ourpromisecol">
        <section class="item_represent type_2  bg_grey_light_2">
          <div class="d_inline_m icon_wrap_1"> <i class="icon"><img src="<?php echo CDN_URL; ?>images/mint-condition1.svg" alt="Mint Conditon"/></i> </div>
          <div class="description">
            <p class="margin-bottom-10"><b class="nw_scheme_color">Mint <br />Condition</b></p>
            <small>All our products delivered are as good as new and we don't reuse soft furnishing.</small> </div>
        </section>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-3 m_bottom_30 ourpromisecol">
        <section class="item_represent type_2  bg_grey_light_2">
          <div class="d_inline_m icon_wrap_1"><i class="icon smw"><img src="<?php echo CDN_URL; ?>images/free-delivery-setup.svg" alt="Free Delivery Setup"/></i></div>
          <div class="description">
            <p class="margin-bottom-10"><b class="nw_scheme_color">Free Delivery & Setup</b></p>
            <small>We will deliver and Setup it for you within 72
            hours without any additional charge.</small> </div>
        </section>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-3 m_bottom_30 ourpromisecol">
        <section class="item_represent type_2  bg_grey_light_2">
          <div class="d_inline_m icon_wrap_1"><i class="icon"><img src="<?php echo CDN_URL; ?>images/money-back-guarantee.svg" alt="Money Back Guarantee"/></i></div>
          <div class="description">
            <p class="margin-bottom-10"><b class="nw_scheme_color">Money Back Guarantee</b></p>
            <small>100% money back guarantee if you 
            don't like our products.</small> </div>
        </section>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-3 m_bottom_30 ourpromisecol">
        <section class="item_represent type_2  bg_grey_light_2">
          <div class="d_inline_m icon_wrap_1"><i class="icon"><img src="<?php echo CDN_URL; ?>images/free-swap.svg" alt="Free Swap"></i></div>
          <div class="description">
            <p class="margin-bottom-10"><b class="nw_scheme_color">Free <br />Swap</b></p>
            <small>Renovate your home every year by upgrading to
            new designs. Delivery and installation is free.</small> </div>
        </section>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-3 m_bottom_30 ourpromisecol">
        <section class="item_represent type_2  bg_grey_light_2">
          <div class="d_inline_m icon_wrap_1"><i class="icon"><img src="<?php echo CDN_URL; ?>images/free-location.svg" alt="Free Relocation"/></i></div>
         <div class="description">
            <p class="margin-bottom-10"><b class="nw_scheme_color">Free Relocation</b></p>
            <small>Relocating to a new city where we operate?
            We will help you relocate for free.</small> </div>
        </section>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-3 m_bottom_30 ourpromisecol">
        <section class="item_represent type_2  bg_grey_light_2">
          <div class="d_inline_m icon_wrap_1"><i class="icon"><img src="<?php echo CDN_URL; ?>images/free-maintenance.svg" alt="Free Maintenance"/></i></div>
         <div class="description">
            <p class="margin-bottom-10"><b class="nw_scheme_color">Free Maintenance</b></p>
            <small>Normal wear and tear is expected while you use
            our products. We don't charge for that.</small> </div>
        </section>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-3 m_bottom_30 ourpromisecol hidden-xs">
        <section class="item_represent type_2  bg_grey_light_2">
          <div class="d_inline_m icon_wrap_1"><i class="icon"><img src="<?php echo CDN_URL; ?>images/rent-to-buy-option.svg" alt="Rent to Buy Option"/></i></div>
          <div class="description">
            <p class="margin-bottom-10"><b class="nw_scheme_color">Rent to Buy Option</b></p>
            <small>Feel like buying our products? Talk to us we will
            give you a deal you will not be able to refuse.</small> </div>
        </section>
      </div>
      <div class="col-lg-3 col-md-3 col-sm-4 col-xs-3 m_bottom_30 ourpromisecol hidden-xs">
        <section class="item_represent type_2  bg_grey_light_2">
          <div class="d_inline_m icon_wrap_1"><i class="icon"><img src="<?php echo CDN_URL; ?>images/easy-payment-terms.svg" alt="Easy Payment Terms"/></i></div>
          <div class="description">
            <p class="margin-bottom-10"><b class="nw_scheme_color">Easy Payment Terms</b></p>
            <small>We accept all payment methods - credit cards,
            debit cards, net banking and checks.</small> </div>
        </section>
      </div>
    </div>
  </div>
</div>
<!-- our promise scection -->


<!-- new collecton section --> 

<!-- got any query section -->
<section class="section_offset gotqueryrow text-center">
  <section class="container">
    <div class="row">
      <div class="cols-sm-12 col-md-12 col-lg-12">
        <h2>Got Any Queries?</h2>
      </div>
      <div class="centerquerycontent">
        <aside class="col-sm-5 col-md-5 col-lg-5">
            <div class="largeiconborder"> <img src="<?php echo CDN_URL; ?>images/got-query-icn.svg" alt="Got Any Queries" /> </div>
        </aside>
        <aside class="col-sm-7 col-md-7 col-lg-7 text-left">
          <div class="panel-group" id="accordion">
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"> What is the minimum tenure for renting? <i class="more-less hidden-xs expand-up"></i> <i class="more-less visible-xs expand-down"></i> </a> </div>
              </div>
              <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">The minimum tenure for renting varies from product to product. If your required period is not covered on our product page, we will be happy to discuss your requirement and fulfill the same.</div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"> Is there a contract? What are the terms? <i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse2" class="panel-collapse collapse">
                  <div class="panel-body">Yes, you are required to sign a contract at the time of delivery. The contract will include the basic terms of renting furniture in simple words. You can view sample of the same <a href="<?php echo base_url(); ?>pages/rental-agreement"><u>here</u></a>.</div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"> When and how do I get my refundable deposit back?<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse3" class="panel-collapse collapse">
                <div class="panel-body">Once the products are picked up from your place, they undergo quality check by the QC team at the warehouse. If found damaged, then repair charges will be deducted from your refundable deposit. Also, if there is any amount due towards early termination charges or rental, same will be adjusted from your deposit, balance if any you will have to pay for the same. The refund will get processed within 7 working days after pickup and after that it will take 7-10 more days to get reflected in your account.</div>
              </div>
            </div>
          </div>
            <a href="<?php echo base_url() ?>pages/faq" class="btn-check">Check All faqs</a> </aside>
      </div>
    </div>
  </section>
</section>
<!-- got any query section --> 

<!--Customers-->
<?php if(!empty($clreviewList)){ ?>
<section class="section_offset clientreviewsrow text-center">
    <div class="container">
        <div class="col-xs-12">
            <h2>Client Reviews</h2>
        </div>
        <div class="clientslider">
            <?php 
            foreach ($clreviewList as $review){ 
                $reImage = '';
                if($review->image != ''){
                    $reImage = explode(',', $review->image)[0];
                }?>
                <div>
                    <div class="testimonialreviewcol">	
                        <article class="frame_container">
                            <figure> 
                                <div class="usrimg">
                                  <span>
                                      <img src="<?php echo CDN_URL; ?>images/product/<?php echo $reImage; ?>" alt="" class="tr_all scale_image">
                                  </span>
                                </div>
                                <figcaption>
                                    <div class="post_excerpt m_bottom_15 t_align_c relative">
                                        <p><?php echo substr($review->excerpt,0,200 ); if(strlen($review->excerpt) > 200){ echo '....';}?></p>
                                        <?php 
                                        $custArray = explode(',',$review->product_name);
                                        if(isset($custArray[0])){
                                            echo "<b>".$custArray[0]."</b>";
                                        }
                                        if(isset($custArray[1])){
                                            echo "<span class='nw_scheme_gray'>, ".$custArray[1]."</span>";
                                        }
                                        ?>
                                    </div>
                                </figcaption>
                            </figure>
                        </article>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="text-center margin-top-30"> <a href="<?php echo base_url(); ?>reviews-testimonials/all" class="btn-check">Check all Testimonials</a> </div>
    </div>
</section>
<?php } ?>
<!--Customers-->

<!--Coverages-->
<section class="section_offset medialogorow hidden-xs">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <h2 class="nw_scheme_color">Media also loves us</h2>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-center">
        <figure> <a href="http://yourstory.com/2016/02/cityfurnish/" rel="nofollow" target="_blank" class="d_block scale_image_container wrapper color_white"> <img src="<?php echo CDN_URL; ?>images/YourStory_Media-Logo.png" alt="Yourstory coverage for furniture rental" class="tr_all scale_image"> </a> </figure>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-center">
        <figure> <a href="http://techstory.in/cityfurnish-acquires-funding-24122015/" rel="nofollow" target="_blank" class="d_block scale_image_container wrapper color_white"> <img src="<?php echo CDN_URL; ?>images/techstory.png" alt="Techstory coverage" class="tr_all scale_image"> </a> </figure>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-center">
        <figure> <a href="http://www.iamwire.com/2015/12/cityfurnish-set-revolutionise-demand-furniture-rental-industry-india/129073" target="_blank" rel="nofollow" class="d_block scale_image_container wrapper color_white"> <img src="<?php echo CDN_URL; ?>images/logo-iamwire.png" alt="Iamwire coverage" class="tr_all scale_image"> </a> </figure>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-center">
        <figure> <a href="https://www.vccircle.com/exclusive-citrus-pays-gupta-backs-furniture-rental-venture-cityfurnish/" rel="nofollow" target="_blank" class="d_block scale_image_container wrapper color_white"> <img src="<?php echo CDN_URL; ?>images/vccircle-com-squarelogo.png" alt="VCcircle coverage" class="tr_all scale_image"> </a> </figure>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-center">
        <figure> <a href="http://inc42.com/flash-feed/cityfurnish-raises-seed-fund/" rel="nofollow" target="_blank" class="d_block scale_image_container wrapper color_white"> <img src="<?php echo CDN_URL; ?>images/Inc42.png" alt="Inc42 Coverage" class="tr_all scale_image"> </a> </figure>
      </div>
      <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 text-center">
        <figure> <a href="http://bizztor.com/cityfurnish-raises-seed-funding-from-citrus-pays-cofounder-jitendra-gupta/" rel="nofollow" target="_blank" class="d_block scale_image_container f_left wrapper color_white"> <img src="<?php echo CDN_URL; ?>images/Bizztor1.png" alt="Bizztor coverage" class="tr_all scale_image"> </a> </figure>
      </div>
    </div>
  </div>
</section>
<!--footer-->
<?php
//$this->load->view('site/templates/sub_footer');
$this->load->view('site/templates/footer');
?>
</div>
</body>
</html>