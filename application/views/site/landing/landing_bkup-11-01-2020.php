<?php $this->load->view('site/templates/header_inner'); ?>
	<div class="page-wrapper flex-full">
		<section class="home-banner flex-full bg-img position-relative">
			<div class="banner-slider h-100 w-100 slider-for" id="banner-slider">
				<div class="slide bg-img" style="background-image: url(images/newdesign_images/banner-image.png);">
				</div>
				<div class="slide bg-img" style="background-image: url(images/newdesign_images/banner-image.png);">
				</div>
				<div class="slide bg-img" style="background-image: url(images/newdesign_images/banner-image.png);">
				</div>
				<div class="slide bg-img" style="background-image: url(images/newdesign_images/banner-image.png);">
				</div>
			</div>			
			<div class="home-banner-wrapper flex-full container align-items-center">
				<div class="banner-text flex-full">
					<h1><strong>Premium Furniture</strong> at Affordable Monthly Rentals</h1>
					<a href="javascript:void(0)" class="explore-btn">Explore</a>
				</div>
				<div class="banner-tags-wrapper flex-full justify-content-center d-none-xs">
					<div class="banner-tags-listing flex-full justify-content-center slider-nav" id="banner-tags-listing">
						<div class="slide banner-tags"><span>Explore Monthly Rentals</span></div>
						<div class="slide banner-tags"><span>First Two Months Free!</span></div>
						<div class="slide banner-tags"><span>15% Discount on First Order</span></div>
						<div class="slide banner-tags"><span>&#x20B9; 1000/- Off on Three Months Tenure</span></div>
					</div>
				</div>
			</div>
		</section>		
		<!-- main features section starts -->
		<section class="main-features flex-full">
			<div class="container">
				<div class="main-features-wrapper flex-full">
					<ul class="main-features-listing flex-full justify-content-center align-items-start align-content-start">
					    <?php
					        foreach($categoriesTree as $row){
					            if($row->cat_name == 'Packages' || $row->cat_name == 'Home Furniture' || $row->cat_name == 'Electronics' || $row->cat_name == 'Office Furniture' || $row->cat_name == 'Fitness'){
					                if($row->image != ''){
					                    $catImage = CDN_URL . 'images/category/' . $row->image;
					                }else{
					                    $catImage = CDN_URL . 'images/wishlist_img_1.jpg';
					                }
					    ?>
					                <li>
            							<a href="<?php echo base_url(); ?><?php echo $cat_slug; ?>/<?php echo $row->seourl; ?>" class="features-box flex-full justify-content-center text-center">
            								<i class="icn icn-combos"></i>
            								<span><?php echo $row->cat_name; ?></span>
            							</a>
            						</li>
					    <?php
					            }
					        }
					    ?>
						<!--<li>-->
						<!--	<a href="javascript:void(0)" class="features-box flex-full justify-content-center text-center">-->
						<!--		<i class="icn icn-combos"></i>-->
						<!--		<span>Combos</span>-->
						<!--	</a>-->
						<!--</li>-->
						<!--<li>-->
						<!--	<a href="javascript:void(0)" class="features-box flex-full justify-content-center text-center">-->
						<!--		<i class="icn icn-home-furniture"></i>-->
						<!--		<span>Home furniture</span>-->
						<!--	</a>-->
						<!--</li>-->
						<!--<li>-->
						<!--	<a href="javascript:void(0)" class="features-box flex-full justify-content-center text-center">-->
						<!--		<i class="icn icn-office-furniture"></i>-->
						<!--		<span>Office Furniture</span>-->
						<!--	</a>-->
						<!--</li>-->
						<!--<li>-->
						<!--	<a href="javascript:void(0)" class="features-box flex-full justify-content-center text-center">-->
						<!--		<i class="icn icn-kids-furniture"></i>-->
						<!--		<span>Kids Furniture</span>-->
						<!--	</a>-->
						<!--</li>-->
						<!--<li>-->
						<!--	<a href="javascript:void(0)" class="features-box flex-full justify-content-center text-center">-->
						<!--		<i class="icn icn-electronics"></i>-->
						<!--		<span>Electronics</span>-->
						<!--	</a>-->
						<!--</li>-->
						<!--<li>-->
						<!--	<a href="javascript:void(0)" class="features-box flex-full justify-content-center text-center">-->
						<!--		<i class="icn icn-Fitness"></i>-->
						<!--		<span>Fitness</span>-->
						<!--	</a>-->
						<!--</li>						-->
					</ul>
				</div>				
			</div>
		</section>
		<!-- main features section ends -->
		<!-- home offer (for mobile) section starts -->
		<div class="home-offer flex-full d-none d-flex-xs">
			<div class="home-offer-wrapper flex-full">
				<small class="w-100">Get your First</small>
				<h2>2 Months<span class="text-uppercase w-100">Rent free</span></h2>
			</div>
			<div class="know-more flex-full">
				<a href="javascript:void(0)">Know more</a>
				<span class="w-100">*Limited period offer.</span>
			</div>
		</div>
		<!-- home offer (for mobile) section ends -->
		<!-- fixed rental plans starts -->
		<section class="rental-palns-section flex-full">
			<div class="container">
				<div class="rental-plans-wrapper flex-full">
					<div class="scrollable-items flex-full align-items-start align-content-start">
						<div class="rental-items-listing mCustomScrollbar content w-100" style="max-height: 364px;overflow-y: auto;">
							<ul class="flex-full">
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="images/newdesign_images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="images/newdesign_images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="images/newdesign_images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="images/newdesign_images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="images/newdesign_images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="images/newdesign_images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="images/newdesign_images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="images/newdesign_images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
								<li>
									<div class="rental-items flex-full justify-content-center align-items-center">
										<img src="images/newdesign_images/rental-items1.png" alt="Rental Items" />
									</div>
								</li>
							</ul>
						</div>
					</div>
					<div class="rental-box-area flex-full align-items-start align-content-start">
						<ul class="rental-box-listing flex-full">
							<li>
								<div class="rental-box flex-full justify-content-center align-items-center">
									<img src="images/newdesign_images/rental-items2.png" alt="rental items">
								</div>
							</li>
							<li>
								<div class="rental-box flex-full justify-content-center align-items-center">
									<img src="images/newdesign_images/rental-items3.png" alt="rental items">
								</div>
							</li>
							<li>
								<div class="rental-box flex-full justify-content-center align-items-center">
									<img src="images/newdesign_images/rental-items2.png" alt="rental items">
								</div>
							</li>
							<li>
								<div class="rental-box flex-full justify-content-center align-items-center">
									<img src="images/newdesign_images/rental-items3.png" alt="rental items">
								</div>
							</li>
							<li>
								<div class="rental-box flex-full justify-content-center align-items-center">
									<img src="images/newdesign_images/rental-items2.png" alt="rental items">
								</div>
							</li>
							<li>
								<div class="rental-box flex-full justify-content-center align-items-center">
									<img src="images/newdesign_images/rental-items3.png" alt="rental items">
								</div>
							</li>							
						</ul>
					</div>
					<div class="rental-heading flex-full align-items-center align-content-center">
						<h2 class="w-100">Fixed Rental Plans</h2>
						<p class="w-100">Lorem Ipsum is simply dummy text of the printing and typesetting </p>
						<div class="rental-plans flex-full">
							<div class="rental-radio-grp flex-full">
								<input type="radio" name="rental-plan" id="rent-4999" checked>
								<label for="rent-4999">&#x20B9;<strong>4999/</strong>month</label>
							</div>
							<div class="rental-radio-grp flex-full">
								<input type="radio" name="rental-plan" id="rent-3999">
								<label for="rent-3999">&#x20B9;<strong>3999/</strong>month</label>
							</div>
							<div class="rental-radio-grp flex-full">
								<input type="radio" name="rental-plan" id="rent-1999">
								<label for="rent-1999">&#x20B9;<strong>1999/</strong>month</label>
							</div>
						</div>
						<a href="javascript:void(0)" class="explore-btn">Explore</a>
					</div>
				</div>
			</div>
		</section>
		<!-- fixed rental plans ends -->
		<!-- home service starts -->
		<section class="services-section flex-full">
			<div class="container">
				<div class="services-wrapper flex-full">
					<h2 class="w-100 text-center">You are Covered</h2>
					<div class="service-tab flex-full align-items-center align-content-center">
						<div class="tab-links-part flex-full">
							<ul class="tabs flex-full">
								<li class="tab-link current" data-tab="tab-1"><span>Free Relocation</span></li>
								<li class="tab-link" data-tab="tab-2"><span>Free Swap</span></li>
								<li class="tab-link" data-tab="tab-3"><span>Quality of items</span></li>
								<li class="tab-link" data-tab="tab-4"><span>Damage Waiver</span></li>
							</ul>
						</div>
						<div class="tab-content-part flex-full">
							<div id="tab-1" class="tab-content current">
								<figure class="flex-full justify-content-center">							
									<img src="images/newdesign_images/service-vector.png" alt="service-vector">
								</figure>
							</div>
							<div id="tab-2" class="tab-content">
								<figure class="flex-full justify-content-center">							
									<img src="images/newdesign_images/service-vector.png" alt="service-vector">
								</figure>
							</div>
							<div id="tab-3" class="tab-content">
								<figure class="flex-full justify-content-center">							
									<img src="images/newdesign_images/service-vector.png" alt="service-vector">
								</figure>
							</div>
							<div id="tab-4" class="tab-content">
								<figure class="flex-full justify-content-center">							
									<img src="images/newdesign_images/service-vector.png" alt="service-vector">
								</figure>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- home service ends -->
		<!-- trending products starts -->
		<section class="trending-products-section flex-full">
			<div class="container">
				<div class="trending-products-wrapper flex-full">
					<h2 class="w-100 text-center">Trending Products</h2>
					<div class="trending-products-tab flex-full">
						<div class="tab-links-part flex-full">
							<ul class="trending flex-full justify-content-center">
								<li class="tab-link current" data-tab="tab-5"><span>Home Furniture</span></li>
								<li class="tab-link" data-tab="tab-6"><span>Electronics</span></li>
								<li class="tab-link" data-tab="tab-7"><span>Kids Furniture</span></li>
							</ul>
						</div>
						<div class="tab-content-part flex-full">
							<div id="tab-5" class="tab-content trending-content current ">
								<div class="trending-product-slider w-100">
									<div class="slide">
										<div class="product-single flex-full position-relative">
											<div class="product-image flex-full position-relative">
												<a href="javascript:void(0)" class="flex-full position-relative h-100">
													<amp-img src="images/newdesign_images/product.jpg" alt="Product Image" layout="responsive" width="300" height="183"></amp-img>
												</a>
											</div>
											<div class="product-description flex-full">
												<div class="product-description-wrapper flex-full">
													<h3><a href="javascript:void(0)">Cortez Sofa Set</a></h3>
													<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> 700</strong> / mon</p>
												</div>
											</div>
										</div>
									</div>
									<div class="slide">
										<div class="product-single flex-full position-relative">
											<div class="product-image flex-full position-relative">
												<a href="javascript:void(0)" class="flex-full position-relative h-100">
													<amp-img src="images/newdesign_images/product.jpg" alt="Product Image" layout="responsive" width="300" height="183"></amp-img>
												</a>
											</div>
											<div class="product-description flex-full">
												<div class="product-description-wrapper flex-full">
													<h3><a href="javascript:void(0)">Cortez Sofa Set</a></h3>
													<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> 700</strong> / mon <span class="items-count">+2 item</span></p>
												</div>
											</div>
											<div class="more-items-expand flex-full">
												<span class="items-count w-100">+2 item</span>
												<ul class="items-lisitng flex-full">
													<li><a href="javascript:void(0)"><img src="images/newdesign_images/product.jpg" alt="Product Image"></a></li>
													<li><a href="javascript:void(0)"><img src="images/newdesign_images/product.jpg" alt="Product Image"></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="slide">
										<div class="product-single flex-full position-relative">
											<div class="product-image flex-full position-relative">
												<a href="javascript:void(0)" class="flex-full position-relative h-100">
													<amp-img src="images/newdesign_images/product.jpg" alt="Product Image" layout="responsive" width="300" height="183"></amp-img>
												</a>
											</div>
											<div class="product-description flex-full">
												<div class="product-description-wrapper flex-full">
													<h3><a href="javascript:void(0)">Cortez Sofa Set</a></h3>
													<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> 700</strong> / mon</p>
												</div>
											</div>
										</div>
									</div>
									<div class="slide">
										<div class="product-single flex-full position-relative">
											<div class="product-image flex-full position-relative">
												<a href="javascript:void(0)" class="flex-full position-relative h-100">
													<amp-img src="images/newdesign_images/product.jpg" alt="Product Image" layout="responsive" width="300" height="183"></amp-img>
												</a>
											</div>
											<div class="product-description flex-full">
												<div class="product-description-wrapper flex-full">
													<h3><a href="javascript:void(0)">Cortez Sofa Set</a></h3>
													<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> 700</strong> / mon</p>
												</div>
											</div>
										</div>
									</div>
									<div class="slide">
										<div class="product-single flex-full position-relative">
											<div class="product-image flex-full position-relative">
												<a href="javascript:void(0)" class="flex-full position-relative h-100">
													<amp-img src="images/newdesign_images/product.jpg" alt="Product Image" layout="responsive" width="300" height="183"></amp-img>
												</a>
											</div>
											<div class="product-description flex-full">
												<div class="product-description-wrapper flex-full">
													<h3><a href="javascript:void(0)">Cortez Sofa Set</a></h3>
													<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> 700</strong> / mon</p>
												</div>
											</div>
										</div>
									</div>									
								</div>
							</div>
							<div id="tab-6" class="tab-content trending-content">
								<div class="trending-product-slider w-100">
									<div class="slide">
										<div class="product-single flex-full position-relative">
											<div class="product-image flex-full position-relative">
												<a href="javascript:void(0)" class="flex-full position-relative h-100">
													<amp-img src="images/newdesign_images/product.jpg" alt="Product Image" layout="responsive" width="300" height="183"></amp-img>
												</a>
											</div>
											<div class="product-description flex-full">
												<div class="product-description-wrapper flex-full">
													<h3><a href="javascript:void(0)">Cortez Sofa Set</a></h3>
													<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> 700</strong> / mon</p>
												</div>
											</div>
										</div>
									</div>
									<div class="slide">
										<div class="product-single flex-full position-relative">
											<div class="product-image flex-full position-relative">
												<a href="javascript:void(0)" class="flex-full position-relative h-100">
													<amp-img src="images/newdesign_images/product.jpg" alt="Product Image" layout="responsive" width="300" height="183"></amp-img>
												</a>
											</div>
											<div class="product-description flex-full">
												<div class="product-description-wrapper flex-full">
													<h3><a href="javascript:void(0)">Cortez Sofa Set</a></h3>
													<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> 700</strong> / mon <span class="items-count">+2 item</span></p>
												</div>
											</div>
											<div class="more-items-expand flex-full">
												<span class="items-count w-100">+2 item</span>
												<ul class="items-lisitng flex-full">
													<li><a href="javascript:void(0)"><img src="images/newdesign_images/product.jpg" alt="Product Image"></a></li>
													<li><a href="javascript:void(0)"><img src="images/newdesign_images/product.jpg" alt="Product Image"></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="slide">
										<div class="product-single flex-full position-relative">
											<div class="product-image flex-full position-relative">
												<a href="javascript:void(0)" class="flex-full position-relative h-100">
													<amp-img src="images/newdesign_images/product.jpg" alt="Product Image" layout="responsive" width="300" height="183"></amp-img>
												</a>
											</div>
											<div class="product-description flex-full">
												<div class="product-description-wrapper flex-full">
													<h3><a href="javascript:void(0)">Cortez Sofa Set</a></h3>
													<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> 700</strong> / mon</p>
												</div>
											</div>
										</div>
									</div>																	
								</div>
							</div>
							<div id="tab-7" class="tab-content trending-content">
								<div class="trending-product-slider w-100">
									<div class="slide">
										<div class="product-single flex-full position-relative">
											<div class="product-image flex-full position-relative">
												<a href="javascript:void(0)" class="flex-full position-relative h-100">
													<amp-img src="images/newdesign_images/product.jpg" alt="Product Image" layout="responsive" width="300" height="183"></amp-img>
												</a>
											</div>
											<div class="product-description flex-full">
												<div class="product-description-wrapper flex-full">
													<h3><a href="javascript:void(0)">Cortez Sofa Set</a></h3>
													<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> 700</strong> / mon</p>
												</div>
											</div>
										</div>
									</div>
									<div class="slide">
										<div class="product-single flex-full position-relative">
											<div class="product-image flex-full position-relative">
												<a href="javascript:void(0)" class="flex-full position-relative h-100">
													<amp-img src="images/newdesign_images/product.jpg" alt="Product Image" layout="responsive" width="300" height="183"></amp-img>
												</a>
											</div>
											<div class="product-description flex-full">
												<div class="product-description-wrapper flex-full">
													<h3><a href="javascript:void(0)">Cortez Sofa Set</a></h3>
													<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> 700</strong> / mon <span class="items-count">+2 item</span></p>
												</div>
											</div>
											<div class="more-items-expand flex-full">
												<span class="items-count w-100">+2 item</span>
												<ul class="items-lisitng flex-full">
													<li><a href="javascript:void(0)"><img src="images/newdesign_images/product.jpg" alt="Product Image"></a></li>
													<li><a href="javascript:void(0)"><img src="images/newdesign_images/product.jpg" alt="Product Image"></a></li>
												</ul>
											</div>
										</div>
									</div>
									<div class="slide">
										<div class="product-single flex-full position-relative">
											<div class="product-image flex-full position-relative">
												<a href="javascript:void(0)" class="flex-full position-relative h-100">
													<amp-img src="images/newdesign_images/product.jpg" alt="Product Image" layout="responsive" width="300" height="183"></amp-img>
												</a>
											</div>
											<div class="product-description flex-full">
												<div class="product-description-wrapper flex-full">
													<h3><a href="javascript:void(0)">Cortez Sofa Set</a></h3>
													<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> 700</strong> / mon</p>
												</div>
											</div>
										</div>
									</div>
									<div class="slide">
										<div class="product-single flex-full position-relative">
											<div class="product-image flex-full position-relative">
												<a href="javascript:void(0)" class="flex-full position-relative h-100">
													<amp-img src="images/newdesign_images/product.jpg" alt="Product Image" layout="responsive" width="300" height="183"></amp-img>
												</a>
											</div>
											<div class="product-description flex-full">
												<div class="product-description-wrapper flex-full">
													<h3><a href="javascript:void(0)">Cortez Sofa Set</a></h3>
													<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> 700</strong> / mon</p>
												</div>
											</div>
										</div>
									</div>
									<div class="slide">
										<div class="product-single flex-full position-relative">
											<div class="product-image flex-full position-relative">
												<a href="javascript:void(0)" class="flex-full position-relative h-100">
													<amp-img src="images/newdesign_images/product.jpg" alt="Product Image" layout="responsive" width="300" height="183"></amp-img>
												</a>
											</div>
											<div class="product-description flex-full">
												<div class="product-description-wrapper flex-full">
													<h3><a href="javascript:void(0)">Cortez Sofa Set</a></h3>
													<p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> 700</strong> / mon</p>
												</div>
											</div>
										</div>
									</div>									
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- trending products ends -->
		<!-- Testimonial starts -->
		<section class="testimonial-section flex-full home-testimonial">
			<div class="container">
				<div class="testimonial-wrapper flex-full align-items-center">
					<h2>What our Customer’s say..</h2>
					<div class="home-testimonial-slider w-100">
						<div class="slide">
							<div class="testimonial-content-wrapper flex-full align-items-center align-content-center">
								<figure class="flex-full">
									<amp-img src="images/newdesign_images/product.jpg" alt="Product Image" width="360" height="227"></amp-img>
								</figure>
								<div class="testimonial-content flex-full">
									<p>The service for delivery and setting up was great. The whole period from transaction to delivery has been timely and smooth. Great experience!!</p>
									<span class="testimonial-name flex-full justify-content-center align-items-center"><strong>Nikhil Deore</strong> Bangalore</span>
								</div>
							</div>
						</div>
						<div class="slide">
							<div class="testimonial-content-wrapper flex-full align-items-center align-content-center">
								<figure class="flex-full">
									<amp-img src="images/newdesign_images/product.jpg" alt="Product Image" width="360" height="227"></amp-img>
								</figure>
								<div class="testimonial-content flex-full">
									<p>The service for delivery and setting up was great. The whole period from transaction to delivery has been timely and smooth. Great experience!!</p>
									<span class="testimonial-name flex-full justify-content-center align-items-center"><strong>Nikhil Deore</strong> Bangalore</span>
								</div>
							</div>
						</div>
						<div class="slide">
							<div class="testimonial-content-wrapper flex-full align-items-center align-content-center">
								<figure class="flex-full">
									<amp-img src="images/newdesign_images/product.jpg" alt="Product Image" width="360" height="227"></amp-img>
								</figure>
								<div class="testimonial-content flex-full">
									<p>The service for delivery and setting up was great. The whole period from transaction to delivery has been timely and smooth. Great experience!!</p>
									<span class="testimonial-name flex-full justify-content-center align-items-center"><strong>Nikhil Deore</strong> Bangalore</span>
								</div>
							</div>
						</div>
						<div class="slide">
							<div class="testimonial-content-wrapper flex-full align-items-center align-content-center">
								<figure class="flex-full">
									<amp-img src="images/newdesign_images/product.jpg" alt="Product Image" width="360" height="227"></amp-img>
								</figure>
								<div class="testimonial-content flex-full">
									<p>The service for delivery and setting up was great. The whole period from transaction to delivery has been timely and smooth. Great experience!!</p>
									<span class="testimonial-name flex-full justify-content-center align-items-center"><strong>Nikhil Deore</strong> Bangalore</span>
								</div>
							</div>
						</div>
					</div>				
				</div>			
			</div>
		</section>
		<!-- Testimonial ends -->
		
	</div>

<?php $this->load->view('site/templates/footer'); ?>
</div>
</body>
</html>