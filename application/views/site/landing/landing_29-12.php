<?php
$this->load->view('site/templates/header_new');
?>
		<link rel="stylesheet" type="text/css" media="all" href="plugins/layerslider/css/layerslider.css">
			<div>
				<div class="layerslider hide_on_mobile" style="width:100%;height:500px;left:0px;">
					<div class="ls-slide" data-ls="transition2d: all;">
						<!--slide image-->
						<img src="images/banner_furniture_new.jpg" alt="Furniture rental banner for web" class="ls-bg">
						<!--slide layers-->
						<h1 class="ls-l color_white fw_thin" style="left:930px;top:100px;font-size:3.000em;line-height:1.2em; rgba(51,51,51,.6)" data-ls="offsetxin:-100;delayin:600;easingin:easeOutBack;durationin:700;"><b>I Live on rent..<br>So does<br>my furniture</b></h1>
						<!--<h2 class="ls-l color_white fw_light tt_uppercase" style="left:240px;top:310px;font-size:1.5em;text-shadow:2px 1px 0px rgba(51,51,51,.6);" data-ls="offsetxin:-100;delayin:400;easingin:easeOutBack;durationin:700;">Rent premium furniture and home appliances on easy terms</h2>-->
						<a href="shopby/rental-packages" class="ls-l button_type_2 lbrown tr_all fs_medium tt_uppercase" style="border-color: white;left:950px;font-size:1.2em;top:320px;border-radius:5px;">Explore Offerings</a><br/>
						
						<!--<a href="shopby/rental-packages" class="button_type_2 lbrown tr_all fs_medium tt_uppercase" style="background-color:rgb(214, 169, 22);border-radius:5px;left:910px;font-size:1.2em;top:320px;border-radius:5px;">Explore Offerings</a>-->
						

<br/>
						<a style="display:none;" id="tirggeronload" href="javascript:void(0);" data-popup="#subscribe_popup2" data-popup-transition-in="bounceInUp" data-popup-transition-out="bounceOutUp" class="ls-l button_type_2 lbrown tr_all fs_medium tt_uppercase" style="baackground-color:rgb(214, 169, 22);left: 316px;font-size:1.2em;top:368px;border-radius:5px;">Selec City</a>

						</div>
				</div>
			</div>

			<!--Banner Mobile-->
			<section class="section_offset mobile-content" style="padding:0 0 0;">
							<!--banner-->
							<figure class="relative wrapper r_image_container">
								<img src="images/banner_mobile.jpg" alt="Furniture and home appliances on rent banner" class="tr_all scale_image">
								<!--caption-->
								<figcaption class="caption_type_1 tr_all" style="top:30%;">
									<div class="caption_inner t_align_c" style="background: rgba(34,34,38,.4);">
										<h1 class="color_white m_bottom_5 tt_uppercase" style="font-size:14px;">I live on rent.. So does my furniture..</h1>
										<p class="color_light_2">Rent premium furniture and home appliances</p>
									</div>
								</figcaption>
							</figure>
			</section>
			
			<!--Mobile Buttons-->
				<section class="container m_bottom_5 m_xs_bottom_0 m_xs_top_0 mobile-content">
					<div class="row m_xs_bottom_0 m_xs_top_0">
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="padding-left:5px;padding-right:5px">
							<div style="text-align:center;padding-top:5px;">
								<a href="shopby/rental-packages" class="button_type_2 lbrown d_block tr_all fs_medium tt_uppercase" style="baackground-color:rgb(214, 169, 22);font-size:1.1em; border-radius: 5px;">Packages</a>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="padding-left:5px;padding-right:5px">
							<div style="text-align:center;padding-top:5px;">
								<a href="shopby/furniture-rental" class="button_type_2 lbrown d_block tr_all fs_medium tt_uppercase" style="baackground-color:rgb(214, 169, 22);font-size:1.1em;border-radius: 5px;">Furniture</a>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="padding-left:5px;padding-right:5px">
							<div style="text-align:center;padding-top:5px;">
								<a href="shopby/rent-electronics" class="button_type_2 lbrown d_block tr_all fs_medium tt_uppercase" style="baackground-color:rgb(214, 169, 22);font-size:1.1em;border-radius: 5px;">Appliances</a>
							</div>
						</div>
						<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6" style="padding-left:5px;padding-right:5px">
							<div style="text-align:center;padding-top:5px;">
								<a href="shopby/fitness-equipments-on-rent" class="button_type_2 lbrown d_block tr_all fs_medium tt_uppercase" style="baackground-color:rgb(214, 169, 22);font-size:1.1em;border-radius: 5px;">Fitness</a>
							</div>
						</div>						
					</div>
				</section>
	

									<table class="w_full m_top_10 mobile-content">
										<tbody>
<tr class="w_full">
																				<td colspan="3" class="color_white" style="background-color:#F2583E;">
																					<div class="m_top_2 m_bottom_6"><span class="fw_light d_inline_m m_right_5"><i class="fa fa-star fw_default fs_medium t_align_c" style="margin-right: 35px;
    margin-top: 1px;"></i> 100% discount on last month's rent! <a class="sc_hover d_inline_b tt_lowercase" style="color:white;" href="pages/offers"> Know more..    </a></div>
																				</td>
																			</tr>
</tbody>
</table>


			
			<!--main content-->
			<section class="section_offset hide_on_mobile" style="padding-top:0;box-shadow: 0px 2px 5px #ddd; margin-bottom: 10px;">
				<div class="container">
												<div class="row sh_container p_bottom_10 t_align_c mobile-content">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													<h3 class="scheme_color m_bottom_10 second_font m_top_20 tt_uppercase" style="font-size:1.8em">LIVE BETTER ! LIVE FREE !</h3>
													<h4 class="scheme_color m_bottom_20 second_font" style="font-size:1.0em">Rent premium furniture and appliances on easy terms.. </h4>
												</div>
												</div>

				<!--<div  style="color:white;margin-top:10px;height:30px;">
					<div class="container">
						<div class="row">
						<div class="col-lg-2 col-md-2 col-sm-2">
</div>
							<div class="col-lg-8 col-md-8 col-sm-8 t_xs_align_c" style="background-color:#F2583E;line-height: 30px;">
								<ul class="hr_list second_font si_list tt_uppercase hide_on_mobile">
									<li style="text-align: center;font-size: 12px;
    float: none;"><i class="fa fa-star fw_default fs_medium" style="margin-right: 5px;
    margin-top: 1px;"></i> Limited Period Offer - 100% discount on last month's rent! Offer ending soon. <a class="sc_hover d_inline_b tt_lowercase" style="color:white;" href="pages/offers"> Know more..</a></li>
								</ul>
							</div>
						<div class="col-lg-2 col-md-2 col-sm-2">
</div>
						</div>
					</div>
				</div>-->


					<div class="row" style="margin-top:20px;">
						<div class="col-lg-4 col-md-4 col-sm-4">
							<!--banner-->
							<figure class="relative wrapper scale_image_container m_bottom_30 r_image_container">
								<img src="images/Appliances.jpg" alt="Home Appliances on rent" class="tr_all scale_image">
								<!--caption-->
								<figcaption class="caption_type_1 tr_all">
									<div class="caption_inner">
										<h3 class="color_white second_font fw_light m_bottom_5 fs_sm_default">Home Appliances</h3>
										<p class="color_light color_light_2">Branded Home Appliances for every need<br><a href="shopby/rent-electronics" class="color_lbrown color_white_hover">Check our home appliances here..</a></p></p>
									</div>
								</figcaption>
							</figure>
							<!--banner-->
							<a href="#" class="banner_type_2 scheme_color m_xs_bottom_30 d_block">
								<span class="bg_scheme_color inner color_white t_align_c d_block">
									<span class="second_font tt_uppercase fw_light m_bottom_11 d_block fs_big_2 ba_title" style="line-height:30px;">Free Delivery & Installation</span>
									<span class="fw_light m_bottom_15 d_block ba_title_2">Delivery and installation is on us.</span>
								</span>
							</a>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4">
							<!--banner-->
							<figure class="relative wrapper scale_image_container r_image_container m_xs_bottom_30">
								<img src="images/home_furniture.jpg" alt="Furniture on rent" class="tr_all scale_image">
								<!--caption-->
								<figcaption class="caption_type_1 tr_all">
									<div class="caption_inner">
										<h3 class="color_white second_font m_bottom_5 fs_sm_default">Move in Ready Furniture</h3>
										<p class="color_light color_light_2">Delivery and installation in 72 hrs.<br><a href="shopby/furniture-rental" class="color_lbrown color_white_hover">Check our furniture here..</a></p>
									</div>
								</figcaption>
							</figure>
						</div>
						<div class="col-lg-4 col-md-4 col-sm-4">
							<!--banner-->
							<div class="banner_type_2 color_lbrown m_bottom_30">
								<div class="bg_lbrown inner color_white t_align_c">
									<h1 class="second_font fw_light m_bottom_10 tt_uppercase">Quality Assurance</h1>
									<p class="fw_light m_bottom_15">All our product are made of quality Sheesham wood. We provide 100% money back guarantee if you don't like our products.</p>
								</div>
							</div>
							<!--banner-->
							<figure class="relative wrapper scale_image_container r_image_container">
								<img src="images/Office_furniture3.jpg" alt="Office furniture on rent" class="tr_all scale_image">
								<!--caption-->
								<figcaption class="caption_type_1 tr_all">
									<div class="caption_inner">
										<h3 class="color_white second_font fw_light m_bottom_5 fs_sm_default">Office Furniture</h3>
										<p class="color_light color_light_2">Save your capital, furnish your office with us.<br><a href="shopby/office-furniture-rental" class="color_lbrown color_white_hover">Check sample pacakges here..</a></p>
									</div>
								</figcaption>
							</figure>
						</div>
					</div>
				</div>
			</section>

			<!--tabs Featured-->
			<?php
			if (count($productDetails) > 0)
			{
				$productArr = $productDetails;
			?>
			<div class="section_offset" style="box-shadow: rgba(0, 0, 0, 0.14902) 0px 0px 15px;    margin-bottom: 10px;">
									<div class="container p_bottom_10 t_align_c" style="width:100%;">
												<div class="row sh_container p_bottom_10">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													<h3 class="scheme_color m_bottom_10 second_font" style="font-size:1.7em">Packages for every need</h3>
													<h4 class="m_bottom_20 scheme_color_red" style="font-size:1.1em">Explore our packages and choose the one you like.. </h4>
												</div>
												</div>
									</div>
				<div class="container">
							<div class="row">
						<div class="owl-carousel" data-nav="featured_" data-owl-carousel-options='{
									"stagePadding" : 15,
									"margin" : 30,
									"responsive" : {
											"0" : {
												"items" : 1
											},
											"320" : {
												"items" : 2
											},
											"550" : {
												"items" : 3
											},
											"992" : {
												"items" : 4
											}
										}
									}'>					
							<?php
									for ($i = 0; $i < 4; $i = $i + 1)
									{
											if (isset($productArr[$i]->id))
											{
													$imgArr = explode(',', $productArr[$i]->image);
													$img = 'dummyProductImage.jpg';
													foreach($imgArr as $imgVal)
													{
															if ($imgVal != '')
															{
																	$img = $imgVal;
																	break;
															}
													}
								$prodLink = "things/" . $productArr[$i]->id . "/" . url_title($productArr[$i]->product_name, '-');
							?>
							<!--owl item-->
							<div class="animated hidden" data-animation="fadeInDown" data-animation-delay="200">
								<!--product-->
								<div class="frame_container qv_container">
											<!--product-->
											<figure class="product_item relative c_image_container frame_container t_sm_align_c r_image_container">
												<!--image & buttons & label-->
												<div class="relative">
													<a href="<?php echo $prodLink;?>" class="d_block">
														<img src="images/small/<?php echo $img; ?>" alt="<?php echo $productArr[$i]->product_name; ?>" class="c_image_1 tr_all scale_image" >
														<img src="images/small/<?php echo $img; ?>" alt="<?php echo $productArr[$i]->product_name; ?>" class="c_image_2 tr_all scale_image">
													</a>
												</div>
												<figcaption class="bg_white relative t_align_c hide_on_mobile">
													<ul>
														<li>
															<a class="second_font sc_hover d_xs_block m_bottom_10" href="<?php echo $prodLink; ?>"><?php echo $productArr[$i]->product_name; ?></a>
														</li>
														<li class=" color_light fs_large second_font t_sm_align_c m_bottom_5">
															<div class="fs_ex_small m_bottom_5">Rent starts from</div>
															<b class="scheme_color d_block"><?php echo $currencySymbol; echo $productArr[$i]->sale_price; ?></b>
														</li>
													</ul>
												</figcaption>
											</figure>
								</div>
							</div>
							<?php }} ?>
							</div>
							</div>
						<div style="text-align:center;padding-top:30px;">
						<a href="shopby/rental-packages" class="button_type_2 lbrown tr_all fs_medium tt_uppercase" style="baackground-color:rgb(214, 169, 22);font-size:1.1em;border-radius:5px;">See all furniture packages</a>
						</div>
				</div>
			</div>
			<?php }?>
			
			<!--tabs add ons
			<div class="section_offset" style="    box-shadow: 0px 2px 5px #ddd;    margin-bottom: 10px;">
											<div class="container p_bottom_10 t_align_c" style="width:100%;">
												<div class="row sh_container p_bottom_10">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
														<h3 class="scheme_color m_bottom_10 second_font" style="font-size:1.7em">Enrich your package</h3>
														<h4 class="scheme_color_red m_bottom_20 second_font" style="font-size:1.1em">Bouquet of add on products to choose from .. </h4>
													</div>
												</div>
											</div>

				<div class="container">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
									<div class="row">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 w_mxs_full m_xs_bottom_30">
											<figure class="product_item relative c_image_container frame_container t_sm_align_c r_image_container">
												<div class="relative">
														<img src="images/product/Pillows_Set.jpg" alt="Rent Pillows" class="c_image_1 tr_all">
												</div>
											</figure>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 w_mxs_full m_xs_bottom_30">
											<figure class="product_item relative c_image_container frame_container t_sm_align_c r_image_container">
												<div class="relative">
														<img src="images/product/Bedsheet1.jpg" alt="Bedsheets and Pillow Covers" class="c_image_1 tr_all">
												</div>
											</figure>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 w_mxs_full m_xs_bottom_30">
											<figure class="product_item relative c_image_container frame_container t_sm_align_c r_image_container">
												<div class="relative">
														<img src="images/product/Cushions.jpg" alt="Cushions" class="c_image_1 tr_all">
												</div>
											</figure>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 hide_on_mobile">								
									<div class="row">
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 w_mxs_full m_xs_bottom_30">
											<figure class="product_item relative c_image_container frame_container t_sm_align_c r_image_container">
												<div class="relative">
														<img src="images/product/home_appliance_3_2.png" alt="Home appliances on rent" class="c_image_1 tr_all">
												</div>
											</figure>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 w_mxs_full m_xs_bottom_30">
											<figure class="product_item relative c_image_container frame_container t_sm_align_c r_image_container">
												<div class="relative">
														<img src="images/product/Kurlon-Klassic-5-Inch-Coir-SDL643865069-1-294d51.jpg" alt="Rent Mattress" class="c_image_1 tr_all">
												</div>
											</figure>
										</div>
										<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 w_mxs_full m_xs_bottom_30">
											<figure class="product_item relative c_image_container frame_container t_sm_align_c r_image_container">
												<div class="relative">
														<img src="images/product/Belle_Bedside_Table2.jpg" alt="Rent Bedside Table" class="c_image_1 tr_all">
												</div>
											</figure>
										</div>										
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4">
								<div class="row">
									</div>
								</div>
							</div>

				</div>
				
			</div> -->

						<div class="section_offset" style="box-shadow: rgba(0, 0, 0, 0.14902) 0px 0px 15px;    margin-bottom: 10px;">
				<div class="container p_bottom_10 t_align_c">
					<div class="row sh_container p_bottom_10">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<h3 class="scheme_color m_bottom_10 second_font" style="font-size:1.7em">Our Promise</h3>
							<h4 class="scheme_color_red m_bottom_20 second_font" style="font-size:1.1em">You make yourself at home, leave other things to us</h4>
						</div>
					</div>
	
						<div class="row">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 m_bottom_10 m_xs_bottom_0" style="padding-left:5px;padding-right:5px">
										<section class="item_represent m_bottom_3 type_2 h_inherit t_sm_align_c bg_grey_light_2 tr_delay t_align_c">
											<div class="d_inline_m m_xs_bottom_0 color_lbrown icon_wrap_1 t_align_c vc_child"><i class="fa fa-certificate d_inline_m"></i></div>
											<div class="description d_inline_m lh_medium" style="display:inline;">
												<p class="second_font scheme_color m_bottom_2"><b>Mint Condition</b></p>
												<small class="hide_on_mobile">All our products delivered to you are as good as new <br>and we don't reuse soft furnishing</small>
											</div>
										</section>								
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 m_bottom_10 m_xs_bottom_0" style="padding-left:5px;padding-right:5px">							
										<section class="item_represent m_bottom_3 type_2 h_inherit t_sm_align_c bg_grey_light_2 tr_delay t_align_c">
											<div class="d_inline_m m_xs_bottom_0 color_lbrown icon_wrap_1 t_align_c vc_child"><i class="fa fa-truck d_inline_m"></i></div>
											<div class="description d_inline_m lh_medium"  style="display:inline;">
												<p class="second_font scheme_color m_bottom_2"><b>Free Delivery & Setup</b></p>
												<small class="hide_on_mobile">We will deliver and Setup it for you within 72 <br>hours without any additional charge</small>
											</div>
										</section>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 m_bottom_10 m_xs_bottom_0 hide_on_mobile" style="padding-left:5px;padding-right:5px">
										<section class="item_represent m_bottom_3 type_2 h_inherit t_sm_align_c bg_grey_light_2 tr_delay t_align_c">
											<div class="d_inline_m m_xs_bottom_0 color_lbrown icon_wrap_1 t_align_c vc_child"><i class="fa fa-money d_inline_m"></i></div>
											<div class="description d_inline_m lh_medium"  style="display:inline;">
												<p class="second_font scheme_color m_bottom_2"><b>Money Back Guarantee</b></p>
												<small class="hide_on_mobile">100% money back guarantee if you <br>don't like our products</small>
											</div>
										</section>							
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 m_bottom_10 m_xs_bottom_0" style="padding-left:5px;padding-right:5px">
										<section class="item_represent m_bottom_3 type_2 h_inherit t_sm_align_c bg_grey_light_2 tr_delay t_align_c">
											<div class="d_inline_m m_xs_bottom_0 color_lbrown icon_wrap_1 t_align_c vc_child"><i class="fa fa-exchange d_inline_m"></i></div>
											<div class="description d_inline_m lh_medium"  style="display:inline;">
												<p class="second_font scheme_color m_bottom_2"><b>Free Swap</b></p>
												<small class="hide_on_mobile">Renovate your home every year by upgrading to <br>new designs. Delivery and installation is free</small>
											</div>
										</section>							
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 m_bottom_10 m_xs_bottom_0" style="padding-left:5px;padding-right:5px">
										<section class="item_represent m_bottom_3 type_2 h_inherit t_sm_align_c bg_grey_light_2 tr_delay t_align_c">
											<div class="d_inline_m m_xs_bottom_0 color_lbrown icon_wrap_1 t_align_c vc_child"><i class="fa fa-plane d_inline_m"></i></div>
											<div class="description d_inline_m lh_medium"  style="display:inline;">
												<p class="second_font scheme_color m_bottom_2"><b>Free Relocation</b></p>
												<small class="hide_on_mobile">Relocating to a new city where we operate?<br>We will help you relocate for free</small>
											</div>
										</section>							
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 m_bottom_10 m_xs_bottom_0" style="padding-left:5px;padding-right:5px">
										<section class="item_represent m_bottom_3 type_2 h_inherit t_sm_align_c bg_grey_light_2 tr_delay t_align_c">
											<div class="d_inline_m m_xs_bottom_0 color_lbrown icon_wrap_1 t_align_c vc_child"><i class="fa fa-wrench d_inline_m"></i></div>
											<div class="description d_inline_m lh_medium"  style="display:inline;">
												<p class="second_font scheme_color m_bottom_2"><b>Free Maintenance</b></p>
												<small class="hide_on_mobile">Normal wear and tear is expected while you use <br>our products. We don't charge for that</small>
											</div>
										</section>							
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 m_bottom_10 m_xs_bottom_0 hide_on_mobile" style="padding-left:5px;padding-right:5px">
										<section class="item_represent m_bottom_3 type_2 h_inherit t_sm_align_c bg_grey_light_2 tr_delay t_align_c">
											<div class="d_inline_m m_xs_bottom_0 color_lbrown icon_wrap_1 t_align_c vc_child"><i class="fa fa-shopping-cart d_inline_m"></i></div>
											<div class="description d_inline_m lh_medium"  style="display:inline;">
												<p class="second_font scheme_color m_bottom_2"><b>Rent to Buy Option</b></p>
												<small class="hide_on_mobile">Feel like buying our products? Talk to us we will <br>give you a deal you will not be able to refuse</small>
											</div>
										</section>							
							</div>							
						
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 m_bottom_10" style="padding-left:5px;padding-right:5px">
										<section class="item_represent m_bottom_3 type_2 h_inherit t_sm_align_c bg_grey_light_2 tr_delay t_align_c">
											<div class="d_inline_m m_xs_bottom_0 color_lbrown icon_wrap_1 t_align_c vc_child"><i class="fa fa-credit-card d_inline_m"></i></div>
											<div class="description d_inline_m lh_medium"  style="display:inline;">
												<p class="second_font scheme_color m_bottom_2"><b>Easy Payment Terms</b></p>
												<small class="hide_on_mobile">We accept all payment methods - credit cards,<br> debit cards, net banking and checks</small>
											</div>
										</section>						
							</div>							
						</div>	

				</div>
		</div>
		

			<!--Customers-->
			<div class="section_offset" style="    box-shadow: 0px 2px 5px #ddd;    margin-bottom: 10px;">
									<div class="container p_bottom_10 t_align_c" style="width:100%;">
												<div class="row sh_container p_bottom_10">
													<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													<h3 class="scheme_color m_bottom_10 second_font" style="font-size:1.7em">Customer's Reviews</h3>
													<h4 class="scheme_color_red m_bottom_20 second_font" style="font-size:1.1em">Customer's feedback keep us going..</h4>
												</div>
												</div>
											
									</div>
				<section class="container m_bottom_5">
					<div class="row">
						<div class="owl-carousel" data-nav="blog_" data-owl-carousel-options='{
							"stagePadding" : 15,
							"margin" : 30,
							"responsive" : {
									"0" : {
										"items" : 1
									},
									"470" : {
										"items" : 2
									},
									"992" : {
										"items" : 3
									}
								}
							}'>
							<!--post-->

							<div class="animated hidden" data-animation="fadeInDown" data-animation-delay="200" style="box-shadow: 0 8px 15px rgba(0, 0, 0, 0.35);height:360px;background:white;">
								<article class="frame_container">
									<figure class="relative">
										<img src="images/David.jpg" alt="Customer Testimonial David" class="tr_all scale_image">
										<figcaption>
											<div class="clearfix">
												<!--post excerpt-->
												<div class="post_excerpt m_bottom_15 t_align_c" style="margin-top:15px;width:100%;">
													<h5 class="second_font m_bottom_13"><b>David R, Bangalore</b></h5>
													<div class="fw_light fs_large lh_medium m_bottom_14 type_2 relative">
														<i>The team was quick and efficient. Delivery on time.</i><br>
														<a class="scheme_color_red" href="reviews-testimonials/all" style="font-size:.8em;">Read more..</a>
													</div>
												</div>
											</div>
										</figcaption>
									</figure>
								</article>
							</div>

							<!--post-->

							<div class="animated hidden" data-animation="fadeInDown" data-animation-delay="200" style="box-shadow: 0 8px 15px rgba(0, 0, 0, 0.35);height:360px;background:white;">
								<article class="frame_container">
									<figure class="relative">
										<img src="images/product/Raghu.jpg" alt="Customer Testimonial Raghu" class="tr_all scale_image">
										<figcaption>
											<div class="clearfix">
												<!--post excerpt-->
												<div class="post_excerpt m_bottom_15 t_align_c" style="margin-top:15px;width:100%;">
													<h5 class="second_font m_bottom_13"><b>Raghu Rao, Bangalore</b></h5>
													<div class="fw_light fs_large lh_medium m_bottom_14 type_2 relative">
														<i>Great quality and service. Thanks you for your efficiency which made the experience a pleasure.</i><br>
														<a  class="scheme_color_red" href="reviews-testimonials/all" style="font-size:.8em;">Read more..</a>
													</div>
												</div>
											</div>
										</figcaption>
									</figure>
								</article>
							</div>

							<!--post-->

							<div class="animated hidden" data-animation="fadeInDown" data-animation-delay="200" style="box-shadow: 0 8px 15px rgba(0, 0, 0, 0.35);height:360px;background:white;">
								<article class="frame_container">
									<figure class="relative">
										<img src="images/product/Akshay_Singh.jpg" alt="Customer Testimonial Akshay for appliances rental" class="tr_all scale_image">
										<figcaption>
											<div class="clearfix">
												<!--post excerpt-->
												<div class="post_excerpt m_bottom_15 t_align_c" style="margin-top:15px;margin-left: 5px;margin-right: 5px;width:100%;">
													<h5 class="second_font m_bottom_13"><b>Akshay Singh, Bangalore</b></h5>
													<div class="fw_light fs_large lh_medium m_bottom_14 type_2 relative">
														<i>Very happy with the service and quality of the products delivered. Hope you will give us best in class service in the future as well.</i><br>
														<a class="scheme_color_red"  href="reviews-testimonials/all" style="font-size:.8em;">Read more..</a>
													</div>
												</div>
											</div>
										</figcaption>
									</figure>
								</article>
							</div>

							<!--post-->

							<div class="animated hidden" data-animation="fadeInDown" data-animation-delay="200" style="box-shadow: 0 8px 15px rgba(0, 0, 0, 0.35);height:360px;background:white;">
								<article class="frame_container">
									<figure class="relative">
										<img src="images/product/Suyash.jpg" alt="bed on rent" class="tr_all scale_image">
										<figcaption>
											<div class="clearfix">
												<!--post excerpt-->
												<div class="post_excerpt m_bottom_15 t_align_c" style="margin-top:15px;margin-left: 5px;margin-right: 5px;width:100%;">
													<h5 class="second_font m_bottom_13"><b>Suyash Pratap Singh, Delhi</b></h5>
													<div class="fw_light fs_large lh_medium m_bottom_14 type_2 relative">
														<i>Good deal provided by Cityfurnish team. Swift installation of products. Helpful and approachable attitude of the team.</i><br>
														<a class="scheme_color_red"  href="reviews-testimonials/all" style="font-size:.8em;">Read more..</a>
													</div>
												</div>
											</div>
										</figcaption>
									</figure>
								</article>
							</div>
						</div>
					</div>
						<div style="text-align:center;padding-top:30px;">
							<a href="reviews-testimonials/all" class="button_type_2 lbrown tr_all fs_medium tt_uppercase" style="baackground-color:rgb(214, 169, 22);font-size:1.1em;border-radius:5px;">See all Testimonials</a>
						</div>
				</section>

			</div>

			<!--Coverages-->
				<div class="section_offset" style="    box-shadow: 0px 2px 5px #ddd;    margin-bottom: 10px;">
					<div class="container p_bottom_10 t_align_c" style="width:100%;">
						<div class="row sh_container p_bottom_10">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<h4 class="scheme_color m_bottom_10" style="font-size:1.7em">Media also loves us</h4>
								<h4 class="scheme_color_red m_bottom_20 second_font" style="font-size:1.1em">We are featured in.. </h4>
							</div>
						</div>
					</div>

					<section class="container ">
						<div class="row">
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
									<figure>
										<a href="http://yourstory.com/2016/02/cityfurnish/" target="_blank" class="d_block scale_image_container f_left m_right_20 photoframe wrapper color_white">
											<img src="images/YourStory_Media-Logo.png" alt="Yourstory coverage for furniture rental" class="tr_all scale_image" width=150px heigh=100px>
										</a>
									</figure>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">								
									<figure>
										<a href="https://www.vccircle.com/exclusive-citrus-pays-gupta-backs-furniture-rental-venture-cityfurnish/" target="_blank" class="d_block scale_image_container f_left m_right_20 photoframe wrapper color_white">
											<img src="images/vccircle-com-squarelogo.png" alt="VCcircle coverage" class="tr_all scale_image" width=150px heigh=100px>
										</a>
									</figure>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">								
									<figure>
										<a href="http://techstory.in/cityfurnish-acquires-funding-24122015/" target="_blank" class="d_block scale_image_container f_left m_right_20 photoframe wrapper color_white">
											<img src="images/techstory.png" alt="Techstory coverage" class="tr_all scale_image" width=150px heigh=100px>
										</a>
									</figure>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">									
									<figure>
										<a href="http://inc42.com/flash-feed/cityfurnish-raises-seed-fund/" target="_blank" class="d_block scale_image_container f_left m_right_20 photoframe wrapper color_white">
											<img src="images/Inc42.png" alt="Inc42 Coverage" class="tr_all scale_image" width=150px heigh=100px>
										</a>
									</figure>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">									
									<figure>
										<a href="http://bizztor.com/cityfurnish-raises-seed-funding-from-citrus-pays-cofounder-jitendra-gupta/" target="_blank" class="d_block scale_image_container f_left m_right_20 photoframe wrapper color_white">
											<img src="images/Bizztor1.png" alt="Bizztor coverage" class="tr_all scale_image" width=150px heigh=100px>
										</a>
									</figure>
								</div>
								<div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">									
									<figure>
										<a href="http://www.iamwire.com/2015/12/cityfurnish-set-revolutionise-demand-furniture-rental-industry-india/129073" target="_blank" class="d_block scale_image_container f_left m_right_20 photoframe wrapper color_white">
											<img src="images/logo-iamwire.png" alt="Iamwire coverage" class="tr_all scale_image" width=150px heigh=100px>
										</a>
									</figure>
								</div>
						</div>
				</section>
			</div>

	
		<!--footer-->
		<?php
				$this->load->view('site/templates/sub_footer');
				$this->load->view('site/templates/footer');
		?>
	</div>

		<!--back to top-->
		<button class="back_to_top animated button_type_6 grey state_2 d_block black_hover f_left vc_child tr_all"><i class="fa fa-angle-up d_inline_m"></i></button>
		<button data-popup="#subscribe_popup" data-popup-transition-in="bounceInUp" data-popup-transition-out="bounceOutUp"></button>
		<!--popup-->
		<div class="init_popup" id="subscribe_popup2">
			<div class="popup init banner_type_2 color_lbrown ">
				<section class="inner color_white" style="background-color:#e0e0e0">
					<h3 class="second_font fw_light m_bottom_27 t_align_c" style="color:#333333!important">Select City</h2>
					<div class="row color_white">

						<button id="autobuton" class="color_white fs_medium button_type_2 d_inline_b bg_transparent bg_white_h scheme_color bg_white_hover tr_all" onclick="setcity(45)">Delhi NCR</button>

						<button id="autobuton" class="color_white fs_medium button_type_2 d_inline_b bg_transparent bg_white_h scheme_color_font bg_white_hover tr_all" onclick="setcity(46)">Bangalore</button>

						<button id="autobuton" class="color_white fs_medium button_type_2 d_inline_b bg_transparent bg_white_h scheme_color_font bg_white_hover tr_all" onclick="setcity(47)">Pune</button>

						<button id="autobuton" class="color_white fs_medium button_type_2 d_inline_b bg_transparent bg_white_h scheme_color_font bg_white_hover tr_all" onclick="setcity(48)">Mumbai</button>

					</div>
				</section>
				<!--<button class="close_popup fw_light color_white fs_large color_dark_hover tr_all" style="top: -19px;" onclick="$('#hukakak').remove();">x</button>-->
			</div>
		</div>
		<!--popup-->
		<!--libs include-->
		<script src="plugins/layerslider/js/greensock.js"></script>
		<script src="plugins/layerslider/js/layerslider.kreaturamedia.jquery.js"></script>
		<!--<script src="plugins/layerslider/js/layerslider.transitions.js"></script>-->
		<script src="plugins/jquery.appear.min.js"></script>
		<!--<script src="plugins/jquery.elevateZoom-3.0.8.min.js"></script>-->
		<!--<script src="plugins/jquery.easytabs.min.js"></script>-->
		<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>
		<!--<script src="js/retina.min.js"></script>-->

		<!--theme initializer-->
		<script src="js/themeCore.min.js"></script>
		<script src="js/theme.min.js"></script>
		<script>
			function setcity(v){
				$.ajax({
					url: 'https://cityfurnish.nachmundiye.com/site/ajaxhandler/setcity',
					data: 'v='+v,
					dataType: 'json',
					type: 'POST',
					success: function(r){
						if(r.ok==false){
							alert("Exactly what are you trying to do??");
						}
						if(r.ok==true){
							window.location.reload();
						}						
					}
				});	
			}
		<?php if(!$this->session->userdata("prcity")){ ?>
		$(document).ready(function(){
			setTimeout(function(){ $('#tirggeronload').trigger('click'); }, 2000);			
		});
		<?php } ?>
		</script>
		<style id="hukakak">
<?php if(!$this->session->userdata("prcity")){ ?>
<?php if($is_mobile==1){ ?>
#subscribe_popup2 { display:block!important; }
#subscribe_popup2 .popup{    left: 0%;
    top: 40%;}
<?php } ?>
<?php } ?>
		#subscribe_popup2 #autobuton{ background-color: #fff!important;color: #333333!important; }
		</style>

	</body>
</html>