<script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
<script src="<?php echo base_url()?>assets/js/check_login.js"></script>

<div id="overlay" style="display: none;">
    <div class="loader-inner line-scale-pulse-out-rapid">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>

<footer role="contentinfo" style="display: none;">
    <div class="container hidden-xs">
        <div class="row">
            <div class="m_bottom_30 footerlogosec">
                <figure class="m_bottom_15">
                    <img src="<?php echo CDN_URL; ?>images/logo-stick.png" alt="footer logo" />
                </figure>
                <div class="m_bottom_15">
                    <div class="color_light">
                      <p>Cityfurnish is revolutionizing the furniture industry by providing quality furniture and home appliances on easy monthly rental. With the immense focus on product quality and customer service, we strive to become most preferred name in furniture industry by customer's choice.</p>
                    </div>
                </div>
            </div>
            <div class="footerlink m_bottom_30">
                <h5 class="tt_uppercase  m_bottom_15 nw_scheme_color">Help</h5>
                <ul class="second_font vr_list_type_1 with_links">
                    <li><a href="<?php echo base_url();?>pages/contact-us">Contact us</a></li>
                    <li><a href="<?php echo base_url();?>pages/how-it-works">How It Works?</a>  </li>
                    <li><a href="<?php echo base_url();?>pages/faq">FAQs</a></li>
                    <li><a href="<?php echo base_url();?>reviews-testimonials/all">Customer Reviews</a></li>
                    <li><a href="customerpayment" target="_blank">Customer Payment</a></li>
                </ul>
            </div>
            <div class="footerlink m_bottom_30">
                <h5 class="tt_uppercase  m_bottom_15 nw_scheme_color">Information</h5>
                <ul class="second_font vr_list_type_1 with_links">
                    <li><a href="<?php echo base_url();?>blog/" target="_blank">Blog</a></li>
                    <!--<li><a target="_blank" href="http://vior.in" rel="nofollow" >Clearance Sale</a></li>-->
                    <li><a href="<?php echo base_url();?>pages/offers">Offers</a></li>
                    <li><a href="<?php echo base_url();?>pages/careers">We are hiring</a></li>
                    <li><a href="<?php echo base_url();?>pages/friends-and-partners">Friends & Partners</a></li>
                    
                </ul>
            </div>
            <div class="footerlink last m_bottom_30">
                <h5 class="tt_uppercase  m_bottom_15 nw_scheme_color">POLICIES</h5>
                <ul class="second_font vr_list_type_1 with_links">
                    <li><a href="<?php echo base_url();?>pages/terms-of-use">Terms of use</a></li>
                    <li><a href="<?php echo base_url();?>pages/privacy-policy">Privacy policy</a></li>
                    <li><a href="<?php echo base_url();?>pages/refer-a-friend">Referral Terms of use</a></li>
                    <li><a href="<?php echo base_url();?>pages/rental-agreement">Sample Rental Agreement</a></li>
                </ul>
            </div>
            <div class="new_scroll">
                <!--<ul id="results" /> -->
            </div>
            <div class="quickcontactcol m_bottom_30">
                <div class="footerquickcontact">
                    <h5 class="m_bottom_15 nw_scheme_color">QUICK CONTACT</h5>
                    <ul class="contactquick">
                        <li>
                            <a href="mailto:hello@cityfurnish.com">
                                <i class="material-icons">mail_outline</i>
                                <span>hello@cityfurnish.com</span>
                            </a>
                        </li>
                        <li>
                            <a href="tel:8010845000">
                                <i class="material-icons">phone</i>
                                <span>8010845000</span>
                            </a>
                        </li>
                    </ul>
                    <ul class="socialicon">
                        <li>
                            <a href="https://www.facebook.com/cityFurnishRental" rel="nofollow" target="_blank">
                                <!--<i class="fa fa-facebook fs_large"></i>-->
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/CityFurnish" rel="nofollow" target="_blank" class="twitter">
                                <!--<i class="fa fa-twitter fs_large"></i>-->
                            </a>
                        </li>
                        <li>
                            <a href="https://plus.google.com/+cityfurnish" rel="nofollow" target="_blank" class="google">
                                <!--<i class="fa fa-google-plus fs_large"></i>-->
                            </a>
                        </li>
                        <li>
                            <a href="https://in.pinterest.com/cityfurnish/" rel="nofollow" target="_blank" class="pintrest">
                                <!--<i class="fa fa-pinterest-p fs_large"></i>-->
                            </a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/cityfurnish?trk=biz-companies-cym" rel="nofollow" target="_blank" class="linkedin">
                                <!--<i class="fa fa-linkedin fs_large"></i>-->
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-lg-12 col-sm-12 col-md-12">
                <hr class="divider_grey m_bottom_25">
            </div>
            <article class="col-lg-12 col-md-12 col-sm-12 text-center">
                <!--<h6 class="nw_fontw_dark tt_uppercase m_bottom_15">Quality Furniture and Branded Appliances on Rent</h6>-->
                <article class="qulitysec">
                   <?php  if($meta_keyword !=''){ ?>
                            <div class="footerbottom">
                        <?php echo $meta_keyword; 
                    echo '</div>'; 
                   echo  '<hr class="divider_grey margin-top-25 m_bottom_30">';}
                        ?>
                    <div class="row">
                        <div class="col-sm-6 col-lg-6 col-xs-12  m_bottom_25 packageslink">
                            <h6 class="nw_fontw_dark tt_uppercase m_bottom_15">Furniture Rental Packages </h6>
                            <ul>
                                <li>
                                    <a href="<?php echo base_url()?><?php echo $cat_slug; ?>/living-room-furniture-on-rent">Living Room</a>
                                </li>
                                <li>
                                     <a href="<?php echo base_url()?><?php echo $cat_slug; ?>/bedroom-furniture-on-rent">BedRoom</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?><?php echo $cat_slug; ?>/study-room-furniture-rental">Study Room</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?><?php echo $cat_slug; ?>/storage-on-rent">Storage</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?><?php echo $cat_slug; ?>/rental-packages">Full Home</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?><?php echo $cat_slug; ?>/appliances-rental">Home Appliances</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url()?><?php echo $cat_slug; ?>/office-furniture-rental">Office Furniture</a>
                                </li>
                                 <li>
                                    <a href="<?php echo base_url()?><?php echo $cat_slug; ?>/fitness-equipments-on-rent">Fitness</a>
                                </li>
                            </ul>
                        </div>
                       <div class="col-sm-6 col-lg-6 col-xs-12  m_bottom_25 packageslink">
                            <h6 class="nw_fontw_dark tt_uppercase m_bottom_15">MOST POPULAR SEARCH TERMS</h6>
                            <ul>
                                <a href="<?php echo base_url();?>">Furniture on rent</a>,
                                <a href="<?php echo base_url();?>bangalore/furniture-rental">Furniture on rent in Bangalore</a>,
                                <a href="<?php echo base_url();?>delhi/furniture-rental">Furniture on rent in Delhi NCR</a>,
                                <a href="<?php echo base_url();?>gurgaon/furniture-rental">Furniture on rent in Gurgaon</a>,
                                <a href="<?php echo base_url();?>ghaziabad-noida/furniture-rental">Furniture on rent in Noida & Ghaziabad</a>,                                
                                <a href="<?php echo base_url();?>mumbai/furniture-rental">Furniture on rent in Mumbai</a>,
                                <a href="<?php echo base_url();?>shopby/rental-packages">Furniture on rent in Pune</a>,
                                <a href="<?php echo base_url();?>mumbai/appliances-rental">Home appliances on rent in Mumbai</a>,
                                <a href="<?php echo base_url();?>bangalore/appliances-rental">Rent home appliances in Bangalore</a>
                                <a href="<?php echo base_url();?>pune/appliances-rental">Home appliances on rent in Pune</a>,
                                <a href="<?php echo base_url();?>delhi/appliances-rental">Rent home appliances in Delhi NCR</a>
                                <a href="<?php echo base_url();?>gurgaon/appliances-rental">Home appliances on rent in Gurgaon</a>,
                                <a href="<?php echo base_url();?>ghaziabad-noida/appliances-rental">Rent home appliances in Ghaziabad & Noida</a>                                
                            </ul>
                        </div>
                    </div>
                    <!--<hr class="divider_grey  m_bottom_30">-->
                    <div class="row">
                        <div class="col-lg-12 citylinks">
                            <h6 class="nw_fontw_dark tt_uppercase m_bottom_15">Cities in which you can Rent Furniture</h6>
                            <ul>
                                <li><a href="javascript:void(0)">Bengaluru</a></li>
                                <li><a href="javascript:void(0)">Pune</a></li>
                                <li><a href="javascript:void(0)">Gurugram</a></li>
                                <li><a href="javascript:void(0)">Mumbai</a></li>
                                <li><a href="javascript:void(0)">Delhi</a></li>
                                <li><a href="javascript:void(0)">Noida</a></li>
                                 <li><a href="javascript:void(0)">Ghaziabad</a></li>
                                <li><a href="javascript:void(0)">Navi Mumbai</a></li>
                                <li><a href="javascript:void(0)">Thane</a></li>
                            </ul>
                        </div>
                    </div>
                </article>
            </article>
        </div>
    </div>
    <?php
    $this->load->view('site/templates/script_files_new');
    ?>
</footer>
<section class="copyright" id="mobile_footer" style="display: none;">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6 hidden-xs">
                <p>&copy; Copyright <?php echo date('Y'); ?>
                    <strong class="nw_theme_color">Cityfurnish</strong>. All Rights Reserved.</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 headertopcontact visible-xs">
                <a href="mailto:hello@cityfurnish.com">
                    <i class="material-icons">mail_outline</i>
                    <span>hello@cityfurnish.com</span>
                </a>
                <a href="tel:8010845000">
                    <i class="material-icons">phone</i>
                    <span>8010845000</span>
                </a>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-6 cityright hidden-xs">
                <p>Soon coming to: Hyderabad and Chennai</p>
            </div>
        </div>
    </div>
</section>

<!-- Footer starts -->
<span class="flex-full d-none d-flex-xs justify-content-center" style="background:red !important;">
	<a href="javascript:void(0)" class="footer-expand">
		<i class="icn-arrow-bottom"></i>
	</a>
</span>

<footer class="flex-full">
	<div class="footer-main flex-full">
		<div class="container">
			<div class="footer-main-wrapper flex-full justify-content-between">
				<div class="footer-widget d-flex w-auto flex-wrap">
					<h4>Categories</h4>
					<div class="footer-links-wrapper flex-full">
						<ul class="footer-links flex-full">
						    <?php
						        foreach($categoriesTree as $row){
						            if(isset($row->sub_categories)){
						                if($row->cat_name != 'Addon' && $row->cat_name != 'Fitness'){
						    ?>
						                    <li><a href="<?php echo base_url(); ?><?php echo $cat_slug; ?>/<?php echo $row->seourl; ?>"><?php echo $row->cat_name; ?></a></li>
						    <?php
						                }
						            }else if($row->cat_name != 'Cred' && $row->cat_name != 'Credr2'){
						    ?>
						                <li><a href="<?php echo base_url(); ?><?php echo $cat_slug; ?>/<?php echo $row->seourl; ?>"><?php echo $row->cat_name; ?></a></li>
						    <?php
						            }
						        }
						    ?>
						</ul>
					</div>
				</div>
				<div class="footer-widget d-flex w-auto flex-wrap">
					<h4>Cityfurnish</h4>					
					<div class="footer-links-wrapper flex-full">
						<ul class="footer-links flex-full">
							<li><a href="pages/about">About Us</a></li>
							<li><a href="referral">Refer a Friend</a></li>
							<li><a href="pages/careers">Career</a></li>
							<li><a href="javascript:void(0)">Media</a></li>
							<li><a href="pages/contact-us">Contact Us</a></li>
							<li><a href="javascript:void(0)">Our Benifits</a></li>
							<li><a href="javascript:void(0)">Friends & Partners</a></li>
						</ul>
					</div>
				</div>
				<div class="footer-widget d-flex w-auto flex-wrap">
					<h4>Information</h4>
					<div class="footer-links-wrapper flex-full">
						<ul class="footer-links flex-full">
							<li><a href="blog/">Blog</a></li>
							<li><a href="pages/faq">FAQ</a></li>
							<li><a href="javascript:void(0)">Documents required</a></li>
							<li><a href="javascript:void(0)">Our KYC Process</a></li>
						</ul>
					</div>
				</div>
				<div class="footer-widget d-flex w-auto flex-wrap">
					<h4>Terms and Conditions</h4>
					<div class="footer-links-wrapper flex-full">
						<ul class="footer-links flex-full">
							<li><a href="javascript:void(0)">Shipping policies</a></li>
							<li><a href="javascript:void(0)">Cancellation & modification</a></li>
							<li><a href="javascript:void(0)">Return and refund</a></li>
							<li><a href="pages/privacy-policy">Privacy Policy</a></li>
							<li><a href="pages/terms-of-use">Terms & Conditions</a></li>
						</ul>
					</div>
				</div>
				<div class="footer-widget d-flex w-auto flex-wrap">
					<h4>Need Help?</h4>
					<div class="footer-links-wrapper flex-full">
						<ul class="footer-links footer-contact flex-full">
							<li>
								<i class="icn icn-phone"></i>
								<a href="tel:8010845000">8010845000</a>
							</li>
							<li>
								<i class="icn icn-email"></i>
								<a href="mailto:hello@cityfurnish.com">hello@cityfurnish.com</a>
							</li>						
						</ul>
						<ul class="social-links flex-full">
							<li><a href="javascript:void(0)"><i class="icn icn-facebook"></i></a></li>
							<li><a href="javascript:void(0)"><i class="icn icn-google-plus"></i></a></li>
							<li><a href="javascript:void(0)"><i class="icn icn-linkedin"></i></a></li>
							<li><a href="javascript:void(0)"><i class="icn icn-pinterest"></i></a></li>
							<li><a href="javascript:void(0)"><i class="icn icn-twitter"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-copyright flex-full">
		<div class="container">
			<div class="footer-copyright-wrapper flex-full justify-content-center">
				<span>&copy; Copyright 2020 Cityfurnish. All Rights Reserved.</span>
			</div>
		</div>
	</div>
	<div class="mobile-footer flex-full d-none">
		<ul class="footer-links footer-contact flex-full">
			<li>
				<i class="icn icn-phone-white"></i>
				<a href="tel:8010845000">8010845000</a>
			</li>
			<li>
				<i class="icn icn-email-white"></i>
				<a href="mailto:hello@cityfurnish.com">hello@cityfurnish.com</a>
			</li>						
		</ul>
	</div>
</footer>
<!-- Footer ends -->

<button class="back_to_top scrollToTop">
    <i class="material-icons d_inline_m">keyboard_arrow_up</i>
</button>
<!-- serarch popup model -->
<script type="text/javascript">
$('.searchicn').click(function() {
  $('#search_value').val('');
  $('#finalResult').empty();

});

</script>

<script type="text/javascript">
        var pro_id = '';
        var page = '';
        var price = 0;
        if(jQuery('.product_item').length > 0 && jQuery('.layerslider').length == 0){
            page = 'category';
        }
        else if(jQuery('.product_preview').length > 0){
            var pro_id = jQuery('#product_id').val();
            page = 'product';
            price = jQuery('#SalePrice').text().trim();
        }
        else if(window.location.href.indexOf('/cart') != -1){
            var pro_id = new Array();
                        jQuery('.table-cart tr.first a img').each(function(){
                                var temp_id = jQuery(this).parent().attr('href').split('/')[1];
                                pro_id.push(temp_id);
                        });
            page = 'cart';
            price = jQuery('#CartGAmt').text().trim();
        }
        else if(window.location.pathname == '/'){
            page = 'home';
        }
        else{
            page = 'other';
        }
        var google_tag_params = {
                              dynx_itemid: pro_id,
                              dynx_pagetype: page,
                              dynx_totalvalue: parseFloat(price)
                              };
</script>

<script type="text/javascript" src="//platform.linkedin.com/in.js" defer>
            api_key: 81tyo00okgh7jg
            authorize: true
            onLoad: onLinkedInLoad
            scope: r_basicprofile r_emailaddress
</script>

<script type="text/javascript">
jQuery(document).ready(function(){
    //jQuery("img.lazy").lazyload();
    $( 'p:empty' ).remove();
    jQuery('#myModal').on('shown.bs.modal', function() {
        jQuery("#search_value").focus();
    })


    jQuery("#linkdinlog").click(function(){
        IN.UI.Authorize().place(); 
        IN.Event.on(IN, "auth", one_click);
    });
});
</script>
<script type="text/javascript">
jQuery(document).ready(function(){
    jQuery("#linkdin").click(function(){
        IN.UI.Authorize().place(); 
        IN.Event.on(IN, "auth", one_click);
    });
});
</script>

<script type="text/javascript">
var clicked=false;//Global Variable
    function one_click(){
        clicked=true;
    }
    // Setup an event listener to make an API call once auth is complete
    function onLinkedInLoad() {
        IN.Event.on(IN, "auth", getProfileData);          
        //return false;  
    }
    function logout(){
        IN.User.logout(onLogout);
    }
    function onLogout(){
        console.log('Logout successfully');
    }

    // Use the API call wrapper to request the member's basic profile data
    function getProfileData() {
        IN.API.Profile("me").fields("first-name", "last-name", "email-address","picture-url").result(function (data) {
            if (clicked) {  
                var userdata = data.values[0];
                var datasend = {};             
                datasend['email'] = userdata.emailAddress;           
                datasend['first_name'] = userdata.firstName;         
                datasend['last_name'] = userdata.lastName;           
                datasend['facebook_id'] = userdata.key; 
                datasend['login_type'] = "linkdin";
                if(userdata){
                    $.ajax({
                        url: "<?php echo base_url()?>site/user/sociallogin",
                        type: 'POST',
                        dataType: 'json',
                        data:  datasend,
                        success: function(x){
                            
                          //  window.location.href = x.url;
                          location.reload();
                        },
                    });
                }
            }
        });
    }
</script>
<script>
window.fbAsyncInit = function() {
    FB.init({
         appId      : '183441015137603',
        //appId : '324383858071764',
        //appId : '145747126093085',
        cookie     : true,
        xfbml      : true,
        version    : 'v2.12'
    });
    FB.AppEvents.logPageView();   
};
(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
   
function fb_login(str){ 
    FB.login(function(response) {
        if (response.authResponse) {
            console.log('Welcome!  Fetching your information.... ');
            access_token = response.authResponse.accessToken; //get access token
            user_id = response.authResponse.userID; //get FB UID
            FB.api('/me?fields=id,name,email,gender,first_name,last_name', function(response) {
                var datasend = {};
                datasend['id']  = response.id;             
                datasend['email'] = response.email;   
                datasend['gender'] = response.gender;         
                datasend['first_name'] = response.first_name;         
                datasend['last_name'] = response.last_name;           
                datasend['facebook_id'] = response.id;
                datasend['name'] = response.name;  
                datasend['login_type'] = "facebook";               
                datasend['str'] = str;              
                if(response.id){
                    $.ajax({
                        url :  "<?php echo base_url()?>site/user/sociallogin",
                        type: 'POST',
                        dataType: 'json',
                        data: datasend,
                        success: function(x){
                           // console.log(x);
                            //window.location.href = x.url;
                               location.reload();
                        }
                    });
                }
            });

        } else {
            //user hit cancel button
            console.log('User cancelled login or did not fully authorize.');

        }
    }, {
        scope: 'email, public_profile, user_friends'
    });
} 

$(function(){
    //   $('#sign_up_button *').attr('disabled', 'disabled');
   // $( 'p:empty' ).remove();
    $('p').each(function() {
        var $this = $(this);
        if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
            $this.remove();
    });
});
 
</script>

<script type="text/javascript">
var dataLayer = dataLayer || [];

$(document).ready(function(){
    //on click of header menu how it works
    $(document).on('click', '.dropdown-menu li a', function(){
        var url = $(this).attr('href');
        if(!$(this).parent().parent().parent().hasClass('visible-xs')) {
            url = '<?php echo base_url(); ?>'+$(this).attr('href');
        }

        dataLayer.push({
            'event': 'eventsToSend',
            'eventCategory': 'Click',
            'eventAction': 'HowItWorksClick',
            'eventLabel': url
        });
    })

    //on product detail page select add on button click
    $(document).on('click', '.addonbtncol a.addonbtn', function(){
        var url = '<?php echo current_url(); ?>';

        dataLayer.push({
            'event': 'eventsToSend',
            'eventCategory': 'Click',
            'eventAction': 'SelectAddonClick',
            'eventLabel': url
        });
    })

    //on remove product from cart
    $(document).on('click', '.frmcart a.cartdelete', function(){
        var proName = $(this).data('name');

        dataLayer.push({
            'event': 'eventsToSend',
            'eventCategory': 'Click',
            'eventAction': 'DeleteFromCartClick',
            'eventLabel': proName
        });
    })

    //on client review next previous button click
    $(document).on('click', '.clientslider .slick-arrow', function(){
        var proName = $(this).text();
        var url = '<?php echo current_url(); ?>';
        
        dataLayer.push({
            'event': 'eventsToSend',
            'eventCategory': 'Click',
            'eventAction': proName,
            'eventLabel': url
        });
    });

})
</script>

<input type="hidden" id="my_email" value="<?php echo $_SESSION['session_user_email']; ?>">
<script src="<?php echo base_url(); ?>js/easing.jquery.js"></script> 
<script src="<?php echo base_url(); ?>js/readmore.js"></script> 
<script src="<?php echo base_url(); ?>js/slick.js"  defer></script> 
<script src="<?php echo base_url(); ?>js/jquery.sliderTabs.js"></script>
<script src="<?php echo base_url(); ?>js/bootstrap-slider.js"></script> 
<script src="<?php echo base_url(); ?>js/custom-function.js"></script>  