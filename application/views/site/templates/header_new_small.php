<!doctype html>
<html lang="en">
	<head>
		<?php if($this->config->item('google_verification')){ echo stripslashes($this->config->item('google_verification')); }
		if ($meta_title != '' and $meta_title != $title){?>
		<title><?php echo $meta_title;?></title>
		<?php } elseif ($heading != ''){?>
		<title><?php echo $heading;?></title>
		<?php }else {?>
		<title><?php echo $title;?></title>
		<?php }?>
		<meta name="Title" content="<?php echo $meta_title;?>" />

		<meta charset="utf-8">
		<!--add responsive layout support-->
		<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!--meta info-->
		<meta name="author" content="">
		<meta name="description" content="<?php echo $meta_description; ?>" />
		<meta property="og:image" content="<?php echo base_url();?>images/product/<?php echo $this->data['meta_image'];?>" />
		<base href="<?php echo base_url(); ?>" />
		<!--include favicon-->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>images/logo/<?php echo $fevicon;?>">
        <?php if (is_file('google-login-mats/index.php'))
		{
			require_once 'google-login-mats/index.php';
		}?>
		<!--fonts include -->
		<!--<link href='css/Roboto.min.css' rel='stylesheet' type='text/css'>-->
		<!--<link href='css/Roboto_slab.css' rel='stylesheet' type='text/css'>-->
		<!--stylesheet include-->
		<link rel="stylesheet" type="text/css" media="all" href="plugins/fancybox/jquery.fancybox.min.css">
		<link rel="stylesheet" type="text/css" media="all" href="plugins/owl-carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" type="text/css" media="all" href="plugins/jackbox/css/jackbox.min.css">
		<link rel="stylesheet" type="text/css" media="all" href="css/animate.min.css">
		<link rel="stylesheet" type="text/css" media="all" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="all" href="css/style.css">
		<!--[if lte IE 10]>
			<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
			<link rel="stylesheet" type="text/css" media="screen" href="plugins/jackbox/css/jackbox-ie9.css">
		<![endif]-->
		<!--head libs-->
		<!--[if lte IE 8]>
			<style>
				#preloader{display:none !important;}
			</style>
		<![endif]-->
		<script src="js/jquery-2.1.1.min.js"></script>
		<script src="js/modernizr.min.js"></script>
<a href="https://plus.google.com/+Cityfurnish" rel="publisher"></a>
<a href="https://plus.google.com/+Cityfurnish?rel=author"></a>
<!-- start Mixpanel --><script type="text/javascript">(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
mixpanel.init("54b221e752447d804f0b1a444eba1c8e");</script><!-- end Mixpanel -->


<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '253630718384826', {
em: 'insert_email_variable,'
});
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=253630718384826&ev=PageView&noscript=1"
/></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
				<script>
				  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

				  ga('create', 'UA-71251009-1', 'auto');
				  ga('send', 'pageview');

				</script>

	</head>
	<body class="sticky_menu">
		<input id="user_location" <?php if($this->session->userdata('location') == ''){
		echo "value=1";}else{echo "value=0";} ?> hidden></input>
		<!-- External Links -->
		<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
		 
		<!--layout-->
		<div class="wide_layout db_centered bg_white">
			<!--[if (lt IE 9) | IE 9]>
				<div class="bg_red" style="padding:5px 0 12px;">
				<div class="container" style="width:1170px;"><div class="row wrapper"><div class="clearfix color_white" style="padding:9px 0 0;float:left;width:80%;"><i class="fa fa-exclamation-triangle f_left m_right_10" style="font-size:25px;"></i><b>Attention! This page may not display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer browsing experience.</b></div><div class="t_align_r" style="float:left;width:20%;"><a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode" class="button_type_1 d_block f_right lbrown tr_all second_font fs_medium" target="_blank" style="margin-top:6px;">Update Now!</a></div></div></div></div>
			<![endif]-->
			<header role="banner" class="w_inherit">
<div class=" t_xs_align_c">
					<div class="container">
						<div class="d_table w_full d_xs_block">
							<div class="col-lg-12 col-md-12 col-sm-12 d_table_cell d_xs_block f_none v_align_m d_xs_align_c t_align_c">
								<!--logo-->
								<a href="<?php echo base_url();?>" class="d_inline_b">
								<img src="images/logo/<?php echo $logo;?>" alt="Cityfurnish Furniture Rental logo">
								</a>
							</div>
						</div>
					</div>
				</div>
				<div class="header_middle_part t_xs_align_c">
					<div class="container">
						<div class="d_table w_full d_xs_block">
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 d_table_cell d_xs_block f_none v_align_m m_xs_bottom_15 d_xs_align_c t_align_l t_xs_align_c">
								<p class="second_font fs_small"><i class="fa fa-phone color_dark fs_large" style="margin-top:1px;"></i> <a class="sc_hover d_inline_b" href="pages/contact-us"> 8010845000</a>&nbsp|&nbsp
									<i class="fa fa-envelope color_dark" style="margin-top:1px;"></i> <a href="mailto:hello@cityfurnish.com" class="sc_hover d_inline_b"> hello@cityfurnish.com</a></p>
				
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 d_table_cell d_xs_block f_none v_align_m m_xs_bottom_15 d_xs_align_c t_align_c">

								<p class="second_font fs_small">Serving Delhi-NCR, Mumbai, Bangalore, Pune</p>

							</div>
							<div class="col-lg-3 col-md-3 col-sm-3  col-xs-12 d_table_cell d_xs_block f_none v_align_m navigation-test d_xs_align_c t_xs_align_c">
								<div class="clearfix" id="navigation-test">
									<div class="clearfix f_right f_xs_none d_xs_inline_b m_xs_bottom_15 t_xs_align_l">
									</div>
								<nav class="d_inline_b t_xs_align_c f_right xs_float_n">
									<ul class="hr_list second_font si_list fs_small d_xs_align_c">
                                        <?php if ($loginCheck != ''){ ?>
                                        <li><a class="sc_hover tr_delay" href="settings">My Account</a></li>
                                        <li><a class="sc_hover tr_delay" href="purchases">Order History</a></li>
										<li><a class="sc_hover tr_delay" href="<?php echo 'user/'.$userDetails->row()->user_name;?>">Wishlist</a></li>
                                        <li><a class="sc_hover tr_delay" href="logout">Logout</a></li>
                                        <?php }else  { ?>
                                        <li><a class="sc_hover tr_delay" href="login">Login</a></li>
										<li><a class="sc_hover tr_delay" href="signup">Signup</a></li>
                                        <?php } ?>
									</ul>
								</nav>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="header_bottom_part bg_white w_inherit" style="padding-bottom:1px;">
					<div class="container">
						<hr class="divider_black">
						<div class="row">
							<div class="col-lg-12 col-md-12 clearfix t_sm_align_c">
								<ul class="hr_list si_list shop_list f_right f_sm_none d_sm_inline_b t_sm_align_l">
								<?php if ($loginCheck != ''){?>
                                	<li>
										<a href="<?php echo 'user/'.$userDetails->row()->user_name;?>" class="color_lbrown_hover vc_child">
											<span class="d_inline_m">
												<button class="tooltip_container"><i class="fa fa-heart fs_large"></i>
												<sup id="likes_count"><?php echo $userDetails->row()->likes;?></sup>
												<span class="tooltip top fs_small color_white hidden animated" data-show="fadeInDown" data-hide="fadeOutUp">Check Your Wishlist</span></button>
											</span>
										</a>
									</li>
									<!--shopping cart-->
									<?php echo $MiniCartViewSet; ?><?php }?>                                
								</ul>
							</div>
						</div>
					</div>
				</div>
			</header>
		<!--Page Js-->
		<script src="js/site/capture_user_location.js"></script>