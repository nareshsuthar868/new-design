<!doctype html>
<html lang="en">
	<head>

	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-PF4G2HJ');</script>
<!-- End Google Tag Manager -->

		<?php if($this->config->item('google_verification')){ echo stripslashes($this->config->item('google_verification')); }
			if ($meta_title != '' and $meta_title != $title){?>
				<title><?php echo $meta_title;?></title>
			<?php } elseif ($heading != ''){?>
				<title><?php echo $heading;?></title>
			<?php }else {?>
				<title><?php echo $title;?></title>
		<?php }?>
		<meta name="Title" content="<?php echo $meta_title;?>" />
		<meta charset="utf-8">
		<!--add responsive layout support-->
		<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<!--meta info-->
		<meta name="author" content="">
		<meta name="description" content="<?php echo $meta_description; ?>" />
		<meta property="og:image" content="<?php echo base_url();?>images/product/<?php echo $this->data['meta_image'];?>" />
		<meta name="google-signin-client_id" content="107071284342-nmcgkjbbpaf3ilg2g9i0qe8v5nmp035p.apps.googleusercontent.com">
		<base href="<?php echo base_url(); ?>" />
		<!--include favicon-->
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>images/logo/<?php echo $fevicon;?>">
        <?php if (is_file('google-login-mats/index.php'))
		{
			require_once 'google-login-mats/index.php';
		}?>

		<!--<link href='css/Roboto_slab.css' rel='stylesheet' type='text/css'>-->
		<!--stylesheet include-->
		<link rel="stylesheet" type="text/css" media="all" href="plugins/owl-carousel/assets/owl.carousel.min.css">
		<link rel="stylesheet" type="text/css" media="all" href="plugins/fancybox/jquery.fancybox.min.css">
		<link rel="stylesheet" type="text/css" media="all" href="plugins/jackbox/css/jackbox.min.css">
		<link rel="stylesheet" type="text/css" media="all" href="css/animate.min.css">
		<link rel="stylesheet" type="text/css" media="all" href="css/bootstrap.min.css">
		<!--<link rel="stylesheet" type="text/css" media="all" href="css/bootstrap_new.min.css">-->
		<link rel="stylesheet" type="text/css" media="all" href="css/style.css">
		<!--[if lte IE 10]>
			<link rel="stylesheet" type="text/css" media="screen" href="css/ie.css">
			<link rel="stylesheet" type="text/css" media="screen" href="plugins/jackbox/css/jackbox-ie9.css">
		<![endif]-->
		<!--head libs-->
		<!--[if lte IE 8]>
			<style>
				#preloader{display:none !important;}
			</style>
		<![endif]-->
		<script src="https://apis.google.com/js/platform.js" async defer></script>
		
		<script src="js/jquery-2.1.1.min.js"></script>
		<script src="js/modernizr.min.js"></script>
		
		<a href="https://plus.google.com/+Cityfurnish" rel="publisher"></a>
		<a href="https://plus.google.com/+Cityfurnish?rel=author"></a>
		<!-- start Mixpanel<script type="text/javascript">(function(e,a){if(!a.__SV){var b=window;try{var c,l,i,j=b.location,g=j.hash;c=function(a,b){return(l=a.match(RegExp(b+"=([^&]*)")))?l[1]:null};g&&c(g,"state")&&(i=JSON.parse(decodeURIComponent(c(g,"state"))),"mpeditor"===i.action&&(b.sessionStorage.setItem("_mpcehash",g),history.replaceState(i.desiredHash||"",e.title,j.pathname+j.search)))}catch(m){}var k,h;window.mixpanel=a;a._i=[];a.init=function(b,c,f){function e(b,a){var c=a.split(".");2==c.length&&(b=b[c[0]],a=c[1]);b[a]=function(){b.push([a].concat(Array.prototype.slice.call(arguments,
		0)))}}var d=a;"undefined"!==typeof f?d=a[f]=[]:f="mixpanel";d.people=d.people||[];d.toString=function(b){var a="mixpanel";"mixpanel"!==f&&(a+="."+f);b||(a+=" (stub)");return a};d.people.toString=function(){return d.toString(1)+".people (stub)"};k="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config reset people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
		for(h=0;h<k.length;h++)e(d,k[h]);a._i.push([b,c,f])};a.__SV=1.2;b=e.createElement("script");b.type="text/javascript";b.async=!0;b.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";c=e.getElementsByTagName("script")[0];c.parentNode.insertBefore(b,c)}})(document,window.mixpanel||[]);
		mixpanel.init("54b221e752447d804f0b1a444eba1c8e");</script>end Mixpanel -->


<!-- Facebook Pixel Code
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '202224353551343'); // Insert your pixel ID here.
fbq('track', 'PageView');
//fbq('track', 'Purchase', {value: '0.00', currency:'INR'});
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=202224353551343&ev=PageView&noscript=1"
/></noscript>
End Facebook Pixel Code -->


				<script>
				  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
				  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
				  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

				  ga('create', 'UA-71251009-1', 'auto');
				  ga('send', 'pageview');

				</script>

	<script type="text/javascript" src="//platform.linkedin.com/in.js">
    api_key: 81mqk6e8q9at6c
    authorize: true
    onLoad: onLinkedInLoad
    scope: r_basicprofile r_emailaddress
</script>

<script>

var clicked=false;//Global Variable
function ClickLogin()
{
    clicked=true;
}

    function onSignIn(googleUser) {	
if (clicked) {	
      var profile = googleUser.getBasicProfile();  	  
      var datasend = {};				
                datasend['email'] = profile.getEmail();	  
                datasend['gender'] = ""; 		  
                datasend['first_name'] = profile.getName(); 		  
                datasend['last_name'] = ""; 		  
                datasend['facebook_id'] = profile.getId();
                datasend['name'] = profile.getName();				
                datasend['str'] = '';
  
				if(profile.getEmail()){
					$.ajax({
						url: "https://cityfurnish.com/site/user/sociallogin",
						type: 'POST',
						dataType: 'json',
						data: datasend,
						success: function(x){
							window.location.href = x.url;
						}
					});
				}
}
	
    };
	

</script>
<script type="text/javascript">
    // Setup an event listener to make an API call once auth is complete
    function onLinkedInLoad() {
        IN.Event.on(IN, "auth", getProfileData);
    }
    
    // Use the API call wrapper to request the member's profile data
    function getProfileData() {
        IN.API.Profile("me").fields("id", "first-name", "last-name", "headline", "location", "picture-url", "public-profile-url", "email-address").result(displayProfileData).error(onError);
    }
	function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
  }


    // Handle the successful return from the API call
    function displayProfileData(data){
		signOut();
        var user = data.values[0];
		document.getElementById('profileData').style.display = 'block';
		var linkdinesend = {};				
                linkdinesend['email'] = user.emailAddress;	  
                linkdinesend['gender'] = ""; 		  
                linkdinesend['first_name'] = user.firstName; 		  
                linkdinesend['last_name'] = user.lastName; 		  
                linkdinesend['facebook_id'] = user.id;
                linkdinesend['name'] = user.firstName+' '+user.lastName;				
                linkdinesend['str'] = '';
				//alert(user.emailAddress);
				if(user.emailAddress){
					$.ajax({
						url: "https://cityfurnish.com/site/user/sociallogin",
						type: 'POST',
						dataType: 'json',
						data: linkdinesend,
						success: function(x){
							window.location.href = x.url;
						}
					});
				}
				
				
        /* document.getElementById("picture").innerHTML = '<img src="'+user.pictureUrl+'" />';
        document.getElementById("name").innerHTML = user.firstName+' '+user.lastName;
        document.getElementById("intro").innerHTML = user.headline;
        document.getElementById("email").innerHTML = user.emailAddress;
        document.getElementById("location").innerHTML = user.location.name;
        document.getElementById("link").innerHTML = '<a href="'+user.publicProfileUrl+'" target="_blank">Visit profile</a>'; */
       
    }

    // Handle an error response from the API call
    function onError(error) {
        console.log(error);
    }
    
    // Destroy the session of linkedin
    function logout(){
        IN.User.logout(removeProfileData);
    }
    
    // Remove profile data from page
    function removeProfileData(){
        document.getElementById('profileData').remove();
    }
	
	$( document ).ready(function() {
		$( ".sociallogin_logout" ).click(function() {
			//setTimeout(function(){  }, 5000);
		  logout();
		  signOut();
		  setTimeout(function(){  }, 2000);
		 return true;
		});
	});
</script>			
		
	</head>
	<body class="sticky_menu">
				
				
<!-- Google Tag Manager (noscript)-->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PF4G2HJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

		<!--layout-->
		<div class="wide_layout db_centered bg_white">
			<!--[if (lt IE 9) | IE 9]>
				<div class="bg_red" style="padding:5px 0 12px;">
				<div class="container" style="width:1170px;"><div class="row wrapper"><div class="clearfix color_white" style="padding:9px 0 0;float:left;width:80%;"><i class="fa fa-exclamation-triangle f_left m_right_10" style="font-size:25px;"></i><b>Attention! This page may not display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer browsing experience.</b></div><div class="t_align_r" style="float:left;width:20%;"><a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode" class="button_type_1 d_block f_right lbrown tr_all second_font fs_medium" target="_blank" style="margin-top:6px;">Update Now!</a></div></div></div></div>
			<![endif]-->
			<header role="banner" class="w_inherit">
				<div class="header_top_part" style="background-color:#F2583E;color:white">
					<div class="container">
						<div class="row">

							<div class="col-lg-12 col-md-12 col-sm-12 t_xs_align_c">
								<ul class="hr_list second_font si_list tt_uppercase hide_on_mobile">
									<li style="text-align: center;font-size: 12px;
    float: none;"><i class="fa fa-star fw_default fs_medium" style="margin-right: 5px;
    margin-top: 1px;"></i> Limited Period Offer - 100% discount on last month's rent! Offer ending soon. <a class="sc_hover d_inline_b tt_lowercase" style="color:white;" href="pages/offers"> <u>Know more..</u></a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<hr>			

				<div class="header_bottom_part bg_white w_inherit" style="box-shadow: 0px 2px 5px #ddd;">
					<div class="container" style="width: 98%;">
						<div class="row">
							<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 clearfix t_sm_align_c hide_on_mobile" style="z-index: 40;padding-top: 5px;">
								<ul class="hr_list si_list shop_list f_right f_sm_none d_sm_inline_b t_sm_align_l">
											<a href="<?php echo base_url();?>" class="d_inline_b">
												<img src="images/logo/<?php echo $logo;?>" alt="Cityfurnish Furniture Rental logo">
											</a>
								</ul>
							</div>
							<div class="col-lg-6 col-md-6 col-xs-6 hide_on_mobile" style="padding-left:5px;padding-right:0px;">
								<nav role="navigation" class="d_xs_none">
									<ul class="main_menu hr_list second_font fs_medium" style="position: absolute; color:white;">
								
										<!--<li  style="padding-top:10px;">
											<a class="tt_uppercase tr_delay scheme_color_font">Offerings <i class="fa fa-caret-down tr_inherit d_inline_m m_left_5"></i></a>
											<ul class="sub_menu bg_grey_light tr_all scheme_color_font">
												<?php 
													foreach ($mainCategories->result() as $row){
														 if ($row->cat_name != '' && $row->cat_name != 'Our Picks'){
												?>
													<li class="current"><a href="shopby/<?php echo $row->seourl;?>"><?php echo $row->cat_name;?></a></li>

												<?php 
													}}
												?>
											</ul>
										</li>-->
												<?php 
													foreach ($mainCategories->result() as $row){
														 if ($row->cat_name != '' && $row->cat_name != 'Our Picks'){
												?>											
													<li  style="padding-top:10px;">
												
																<a class="tt_uppercase tr_delay scheme_color_font" href="shopby/<?php echo $row->seourl;?>">
																	<?php echo $row->cat_name;?>
																</a>
													</li>
												<?php 
													}}
												?>													
										
									<!--	<li  style="padding-top:10px;">
											<a class="tt_uppercase tr_delay scheme_color_font">Help <i class="fa fa-caret-down tr_inherit d_inline_m m_left_5"></i></a>
											<ul class="sub_menu bg_grey_light tr_all">

												
										<li >
											<a class="tr_delay" href="pages/how-it-works">How it works</a>
										</li>
										<li>
													<a class="tr_delay" href="pages/faq">FAQs</a>
												</li>
												<li>
													<a class="tr_delay" href="pages/contact-us">Contact Us</a>
												</li>
												<li>
													<a class="tr_delay" href="reviews-testimonials/all">Customer Reviews</a>
												</li>
												<li>
													<a class="tr_delay" href="pages/rental-agreement">Rental Agreement</a>
												</li>
												<li>
													<a class="tr_delay" href="pages/careers">Jobs</a>
												</li>
												<li>
													<a class="tr_delay" href="blog">Blog</a>
												</li>
											</ul>
										</li>

										<!--<li  style="padding-top:10px;">
											<a class="tt_uppercase tr_delay scheme_color_font" href="pages/offers">Discount Coupons</a>-->
										</li>-->
										<li  style="padding-top:10px;">
											<a class="tt_uppercase tr_delay scheme_color_font"  target="_blank" style="color:#F2583E;" href="http://vior.in">Clearance Sale</a>
										</li>

									</ul>
								</nav>

							</div>
							<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 mobile-content" style="padding-left:5px;padding-right:0px;">

								<button id="mobile_menu_button" class="f_left vc_child d_xs_block db_xs_left d_none m_bottom_10 m_top_15 bg_lbrown color_white tr_all"><i class="fa fa-navicon d_inline_m"></i></button>
											<div class="mobile-content" style="margin-top:10px;margin-bottom:10px;"><a href="<?php echo base_url();?>" class="d_inline_b">
												<img src="images/logo/<?php echo $fevicon;?>" alt="Cityfurnish Furniture Rental logo">
											</a></div>
								<nav role="navigation" class="d_xs_none">
									<ul class="main_menu hr_list second_font fs_medium" style="position: absolute; color:white;">
								
										<li  style="padding-top:10px;background-color:white;">
											<a class="tt_uppercase tr_delay scheme_color_font">Offerings <i class="fa fa-caret-down tr_inherit d_inline_m m_left_5"></i></a>
											<ul class="sub_menu bg_grey_light tr_all scheme_color_font">
												<?php 
													foreach ($mainCategories->result() as $row){
														 if ($row->cat_name != '' && $row->cat_name != 'Our Picks'){
												?>
													<li class="current"><a href="shopby/<?php echo $row->seourl;?>"><?php echo $row->cat_name;?></a></li>

												<?php 
													}}
												?>
											</ul>
										</li>
										<li  style="padding-top:10px;background-color:white;">
											<a class="tt_uppercase tr_delay scheme_color_font">Help <i class="fa fa-caret-down tr_inherit d_inline_m m_left_5"></i></a>
											<ul class="sub_menu bg_grey_light tr_all">

												<li>
													<a class="tr_delay" href="pages/faq">FAQs</a>
												</li>
												<li>
													<a class="tr_delay" href="pages/contact-us">Contact Us</a>
												</li>
												<li>
													<a class="tr_delay" href="reviews-testimonials/all">Customer Reviews</a>
												</li>
												<li>
													<a class="tr_delay" href="pages/rental-agreement">Rental Agreement</a>
												</li>
												<li>
													<a class="tr_delay" href="pages/careers">Jobs</a>
												</li>
												<li>
													<a class="tr_delay" href="blog">Blog</a>
												</li>
											</ul>
										</li>
										<!--<li  style="padding-top:10px;background-color:white;">
											<a class="tt_uppercase tr_delay scheme_color_font" href="pages/offers">Discount Coupons</a>-->
										</li>
										<li  style="padding-top:10px;background-color:white;">
											<a class="tt_uppercase tr_delay scheme_color_font" target="_blank" href="http://vior.in">Clearance Sale</a>
										</li>

									</ul>
								</nav>

							</div>
							<!-- <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 clearfix t_sm_align_l mobile-content" style="z-index: 40;padding-top: 5px;">
								<ul class="hr_list si_list shop_list f_sm_none d_sm_inline_b t_sm_align_l">
											<a href="<?php echo base_url();?>" class="d_inline_b">
												<img src="images/logo/<?php echo $fevicon;?>" alt="Cityfurnish Furniture Rental logo">
											</a>
								</ul>
							</div> -->
							
							<?php
							//echo "<pre>";
							//$_SESSION['prcity'] = '45';
							//print_r($_SESSION['prcity']);
							//echo "</pre>";
							//exit;
							?>
							<div class="col-lg-4 col-md-4 col-xs-8 clearfix t_xs_align_r"  style="z-index:40;">
									<ul class=" hr_list shop_list f_right second_font fs_medium f_sm_none d_sm_inline_b t_sm_align_l scheme_color_font"  style="padding-top:10px;">
                                        <li><i class="fa fa-map-marker fs_large d_inline_m hide_on_mobile"></i>
											<select class="select-round select-shipping-addr select_title scheme_color_font tt_uppercase relative tr_all" style='background: transparent;   border: none;' onchange="setcity(this.value)">												
												<option value="45" <?php if($_SESSION['prcity'] && $_SESSION['prcity']=='45') echo "selected"; ?>>Delhi Ncr</option>
												<option value="46" <?php if($_SESSION['prcity'] && $_SESSION['prcity']=='46') echo "selected"; ?>>Bangalore</option>
												<option value="47" <?php if($_SESSION['prcity'] && $_SESSION['prcity'] =='47') echo "selected"; ?>>Pune</option>
												<option value="48" <?php if($_SESSION['prcity'] && $_SESSION['prcity']=='48') echo "selected"; ?>>Mumbai</option>
											</select>
										</li>

                                        <?php if ($loginCheck != ''){ ?>
										<li class="hide_on_mobile" style="padding-top:12px;" ><a class="tt_uppercase tr_delay scheme_color_font" href="settings">Me</a></li>
                                        <li class="hide_on_mobile" style="padding-top:11px;"><a class="tt_uppercase tr_delay scheme_color_font sociallogin_logout" href="logout">Logout</a></li>
                                        <?php }else  { ?>
                                        <li style="padding-top:11px;"><a class="tt_uppercase tr_delay scheme_color sociallogin_logout" href="login" onclick="logout(); signOut();">Login/Register</a></li>
                                        <?php } ?>
										<?php if ($loginCheck != ''){?>
											<?php echo $MiniCartViewSet; ?>
										<!--	<li>
												<a href="<?php echo 'user/'.$userDetails->row()->user_name;?>" class="color_lbrown_hover vc_child">
													<span class="d_inline_m">
														<button class="tooltip_container"><i class="fa fa-heart fs_large"></i>
														<sup id="likes_count"><?php echo $userDetails->row()->likes;?></sup>
														<span class="tooltip top fs_small color_white hidden animated" data-show="fadeInDown" data-hide="fadeOutUp">Your Wishlist</span></button>
													</span>
													
												</a>
											</li>-->

										<?php }?>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</header>
			
		<!--Page Js-->
		<script src="js/site/header_new_searchbox.js"></script>
		<!--<script src="js/site/capture_user_location.js"></script>-->