
<!DOCTYPE html>
<html amp lang="en" style="height: 100% !important;">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="canonical" href="http://45.113.122.221/">

        <?php
        if ($this->config->item('google_verification')) {
            echo stripslashes($this->config->item('google_verification'));
        }
        if ($meta_title != '' and $meta_title != $title) {
            ?>
            <title><?php echo $meta_title; ?></title>
        <?php } elseif ($heading != '') { ?>
            <title><?php echo $heading; ?></title>
        <?php } else { ?>
            <title><?php echo $title; ?></title>
<?php } ?>
        <meta name="Title" content="<?php echo $meta_title; ?>" />
        

        <!--add responsive layout support-->
        <!-- <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> -->
        <!--<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">-->
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
        <!--meta info-->
        <meta name="author" content="">
        <meta name="description" content="<?php echo $meta_description; ?>" />
        <meta property="og:image" content="<?php echo base_url(); ?>images/product/<?php echo $this->data['meta_image']; ?>" />
        <base href="<?php echo base_url(); ?>" />
        <!--include favicon-->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url(); ?>images/logo/<?php echo $fevicon; ?>">

        <?php
        if (is_file('google-login-mats/index.php')) {
            require_once 'google-login-mats/index.php';
        }
        ?>

        <style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>

        <!--<link href='css/Roboto_slab.css' rel='stylesheet' type='text/css'>-->
        <!--stylesheet include-->
        <!--<link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet" defer/>-->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" defer/>
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>css/bootstrap.min.css" defer/>
        <!--<link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url(); ?>css/style.css" defer/>-->
        <script src="<?php echo base_url()?>assets/bootstrap-sweetalert/dist/sweetalert.js" async defer></script>
        <link href="<?php echo base_url()?>assets/bootstrap-sweetalert/dist/sweetalert.css" rel="stylesheet" type="text/css" defer/>
        <script src="<?php echo base_url() ?>assets/bootstrap-sweetalert/dist/swalExtend.js" async defer></script>
        <link href="<?php echo base_url() ?>assets/bootstrap-sweetalert/dist/swalExtend.css" rel="stylesheet" type="text/css" defer/>

        <!--<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/slick.css" />-->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/new_slick.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/ion.rangeSlider.min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/jquery.FlowupLabels.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/magnific-popup.css">
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style-custom.css" />
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/responsive.css" />
        <!--<link rel="stylesheet" type="text/css" href="css/ion.rangeSlider.min.css" />-->
    
        <script async src="<?php echo base_url(); ?>js/amp_v0.js"></script>
        <script src="<?php echo base_url(); ?>js/jquery-2.1.1.min.js" ></script>
        <script src="<?php echo base_url(); ?>js/modernizr.min.js" ></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.js" ></script>
        <script src="<?php echo base_url(); ?>js/jquery.FlowupLabels.js"></script>
        <script src="<?php echo base_url(); ?>js/ion.rangeSlider.min.js"></script>
        <!--<script src="<?php echo base_url(); ?>js/slick.min.js" ></script>-->
        <script src="<?php echo base_url(); ?>js/new_slick.min.js" ></script>
        <script src="<?php echo base_url(); ?>js/jquery.magnific-popup.min.js"></script/>
        <script src="<?php echo base_url(); ?>js/sticky-kit.min.js"></script>
        <!--<script src="<?php echo base_url(); ?>js/jquery.mCustomScrollbar.min.js"></script>-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/malihu-custom-scrollbar-plugin/3.1.5/jquery.mCustomScrollbar.min.js"></script>  
        <script src="<?php echo base_url(); ?>js/custom.js"></script>
        
        
          <!-- Lazy load images  -->
        <script src="<?php echo base_url()?>assets/js/jquery.unveil.js"></script>
        <script>
            $(function() {
                $('#mobileCatSlider').css({
                    height: 'auto',
                    visibility: 'visible'
                });
                $("img").unveil(400, function() {
                  $(this).load(function() {
                    this.style.opacity = 1;
                  });
                });
            });
        </script>
    <a href="https://plus.google.com/+Cityfurnish" rel="publisher"></a>
    <a href="https://plus.google.com/+Cityfurnish?rel=author"></a>
    

</head>
<body class="hi">
    <!-- Google Tag Manager (noscript)-->
    <noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PF4G2HJ"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) --> 

    <!--layout-->
    <div class="site">
        <!--[if (lt IE 9) | IE 9]>
                <div class="bg_red" style="padding:5px 0 12px;">
                <div class="container" style="width:1170px;"><div class="row wrapper"><div class="clearfix color_white" style="padding:9px 0 0;float:left;width:80%;"><i class="fa fa-exclamation-triangle f_left m_right_10" style="font-size:25px;"></i><b>Attention! This page may not display correctly.</b> <b>You are using an outdated version of Internet Explorer. For a faster, safer browsing experience.</b></div><div class="t_align_r" style="float:left;width:20%;"><a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode" class="button_type_1 d_block f_right lbrown tr_all second_font fs_medium" target="_blank" style="margin-top:6px;">Update Now!</a></div></div></div></div>
        <![endif]-->
       
        <!-- Header starts -->
        <input type="hidden" name="selected_city_name" id="selected_city_name" value="<?php echo $cat_slug; ?>">
        <header class="flex-full">
            <!-- logo part starts -->
            <div class="logo-part flex-full">
                <div class="container">
                    <div class="logo-part-wrapper flex-full justify-content-between align-items-center">
                        <div class="header-location d-flex w-auto flex-wrap">
                            <div class="location d-flex w-auto flex-wrap align-items-center">
                                <i class="icn icn-map"></i>
                                <!--<span>Banglore</span>-->
                                 <?php
                                //  print_r($_SESSION['cityslug']);exit;
                                       if ($_SESSION['prcity'] && $_SESSION['prcity'] == '45') {
                                           echo "<span>Delhi</span>";
                                       } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '46') {
                                           echo "<span>Bangalore</span>";
                                       } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '47') {
                                           echo "<span>Pune</span>";
                                       } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '48') {
                                           echo "<span>Mumbai</span>";
                                       } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '49') {
                                           echo "<span>Ghaziabad/Noida</span>";
                                       } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '50') {
                                           echo "<span>Gurgaon</span>";
                                       } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '51') {
                                           echo "<span>Hyderabad</span>";
                                       } else {
                                           echo "<span>Select city</span>";
                                       }
                                       ?>
                                <i class="arrow-bottom"></i>
                            </div>
                        </div>
                        <div class="logo d-flex w-auto">
                            <a href="<?php echo base_url(); ?>" class="d-flex w-auto">                         
                                <amp-img src="<?php echo CDN_URL; ?>images/brand-logo.png" alt="Cityfurnish" height="39" width="160"></amp-img>
                                <!--<img src="<?php echo CDN_URL; ?>images/brand-logo.png" alt="Cityfurnish">-->
                            </a>
                        </div>
                        <div class="header-icons d-flex w-auto flex-wrap">
                            <div class="search-bar"><input type="text" name="search" placeholder="Search" value=""><i class="icn icn-close-dark" id="search-close"></i>
    							<ul class="search-bar-listing flex-full" style="display:none;">
    								
    							</ul>
						    </div>
                            <ul class="header-icons-listing d-flex w-auto flex-wrap">
                            	<li>
								    <a href="javascript:void(0)" id="header-search">
                                    <i class="fa fa-search fa-lg" aria-hidden="true"></i>
								        <i class="icn icn-search"></i>
								    </a>
							    </li>
                                <li>
                                    <a href="<?php echo base_url() ?>wishlist" class="carticn">
                                        <i class="icn icn-wishlist"></i>
                                            <?php if ($loginCheck != '') {?>
                                        <span id="whishlist_count_header"><?php echo $itemsInwishlist->num_rows(); ?></span>
                                        <?php } ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)" class="carticn">
                                        <i class="icn icn-cart"></i>
                                        <?php
                                            if ($loginCheck != '') {
                                                if($itemsIncart > 0){
                                        ?>
                                                    <span id="new_count"><?php echo count($itemsIncart); ?></span>
                                        <?php
                                                }
                                            }
                                        ?>
                                    </a>
                                </li>
                                <?php if ($loginCheck != '') {?>
                                <li class="dropdown showdesktop hidden-xs"><a href="javascript:void(0);"><i class="icn icn-profile"></i></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="purchases">My Orders</a></li>
                                        <li><a href="payments">My Payments</a></li>
                                        <li><a href="invoices">My Invoices</a></li>
                                        <li><a href="usersettings">Profile Setting</a></li>             <li><a href="logout">Logout</a></li>
                                    </ul>
                                </li>
                                <?php } else { ?>
                                <li>
                                    <a href="<?php echo base_url() ?>user_sign_up">
                                        <i class="icn icn-profile"></i>
                                    </a>
                                    <!--<a href="javascript:void(0);" data-toggle="modal" data-target="#myModal3">-->
                                    <!--    <i class="icn icn-profile"></i>-->
                                    <!--</a>-->
                                </li>
                                    <?php }?>
                            </ul>
                        </div>
                        <!-- Mini Cart section starts -->
                		<div class="cart-main-section minicart-section flex-full align-items-start align-content-start d-none">
                			<div class="minicart-section-wrapper flex-full h-100 align-items-start align-content-start">
                				<a><span class="close close-minicart"><i class="icn icn-close-black"></i></span></a>
            				    <div class="minicart-main flex-full h-100 align-items-start align-content-start position-relative">
            					    <h2 class="border-left">Order Summary</h2>
                					<div class="order-summary-scroll mCustomScrollbar content">
                					        <ul class="order-summary-listing flex-full" id="min-cart-order-summary">
						                        <?php
                                                    if (!empty($itemsIncart)) {
                                                        $subTotal = 0;
                                                        $s = 0;
                                                        foreach ($itemsIncart as $item) {
                                                            $qu = $this->db->query("SELECT maximumamount FROM " . COUPONCARDS . " WHERE id='" . $item->couponID . "' LIMIT 1");
                                                            $rr = $qu->result();
                                                            $cartDiscountAmt = $cartDiscountAmt + ($item->discountAmount * $item->quantity);
                                                            if ($rr[0]->maximumamount != 0) {
                                                                if ($cartDiscountAmt > $rr[0]->maximumamount) {
                                                                $cartAmt = $cartAmt + $cartDiscountAmt - $rr[0]->maximumamount;
                                                                $cartDiscountAmt = $rr[0]->maximumamount;
                                                                }
                                                            }
                                                            // $advance_rental = $advance_rental + ($item->price  * $item->quantity)-($item->discountAmount * $item->quantity);
                                                            $product_shipping_cost = $product_shipping_cost + ($item->product_shipping_cost * $item->quantity);
                                                            
                                                            // $subTotal += ($item->product_shipping_cost * $item->quantity) + ($item->price * $item->quantity) - $item->discountAmount;
                                                            $cartAmt = $cartAmt + (($item->price - $item->discountAmount + ($item->price * 0.01 * $item->product_tax_cost)) * $item->quantity);
                                                            
                                                            $rental_amount =  $rental_amount  + ((($item->product_tax_cost * 0.01 * $item->price ) + $item->price) * $item->quantity);
                                                            
                                                            $cartTAmt = ($cartAmt * 0.01 * 0);
                                                            $grantAmt = $cartAmt + $product_shipping_cost + $cartTAmt;
                                                            $attrValue = $item->attr_name;
                                                            $cartprodImg = @explode(',', $item->image); ?>
                                							<li id="min-cart-product-<?php echo $s ?>">
                                								<div class="order-summary-box flex-full">
                                									<div class="product-img flex-full">
                                										<img src="<?php echo CDN_URL; ?>images/product/<?php echo $cartprodImg[0]; ?>" alt="Product Image" width="110" height="86"></img>
                                									</div>
                                									<div class="product-desc flex-full align-items-start align-content-start">
                                										<a href="javascript:void(0)" class="delete" onclick="javascript:delete_cart(<?php echo $item->id; ?>,<?php echo $s ?>)"><i class="icn icn-delete"></i></a>
                                										<h3><?php echo $item->product_name; ?></h3>
                                										<ul class="product-desc-listing flex-full">
                                										    <br><br>
                                											<li>
                                												<h4>Quantity</h4>
                                												<span><?php echo $item->quantity; ?> item</span>
                                											</li>
                                											<li>
                                												<h4>Monthly Rent</h4>
                                												<span>&#x20B9; <?php echo  $item->price; ?></span>
                                											</li>
                                										</ul>
                                									</div>
                                								</div>
                                							</li>
                                                        <?php 
                                                                $s++;
                                                                }  
                                                                // print_r($itemsIncart);
                                                                if($itemsIncart[0]->discount_on_si != ''){
                                                                $cartAmt  = $cartAmt - $itemsIncart[0]->discount_on_si;
                                                                $grantAmt = $cartAmt + $product_shipping_cost + $cartTAmt;  
                                                                } ?>
                            			    </ul>
						
			                        </div>
                					<div class="checkout-wrapper flex-full" id="mini-cart-checkout-wrapper">
                						<ul class="product-desc-listing flex-full">
                							<li>
                								<h4>Duration</h4>
                								<span id="min-cart-duration"><?php echo $attrValue; ?></span>
                							</li>
                							<li>
                								<h4>Advance Rental</h4>
                								<span id="min-cart-advance">&#x20B9; <?php echo  number_format($rental_amount, 2, '.', ''); ?></span>
                							</li>
                							<li>
                								<h4>Refundable Deposit</h4>
                								<span id="min-cart-refundable-deposit">&#x20B9; <?php echo  number_format($product_shipping_cost, 2, '.', ''); ?></span>
                							</li>							
                							<li>
                								<h4>Sub Total</h4>
                								<span id="min-cart-subtotal">&#x20B9; <?php echo  number_format($grantAmt, 2, '.', ''); ?></span>
                							</li>
                						</ul>
                						<span class="submit-btn flex-full justify-content-center">
                							<a href="<?php echo base_url('cart') ?>" class="explore-btn">Checkout</a>
                						</span>
					                </div>
                			        <?php } else { ?>
                                            <div class="emptycartbar">
                                                <img src="<?php echo CDN_URL; ?>images/empty_cart.webp" alt="Empaty cart">
                                                <h5>Your Cart is Empty</h5>
                                                <p>Looks like you haven't chosen <br> any product yet</p>
                                            </div>
                                        </ul>
                                    </div>
                                    <div class="checkout-wrapper flex-full" id="mini-cart-checkout-wrapper" style="display:none;">
                						
					                </div>
                                    <?php } ?>
				                </div>
			                </div>			
		                </div>
                        <!-- Mini Cart section ends -->
                        <!-- Office Furniture Enquiry Form section starts -->
                        <div class="enquire-Now cart-main-section enquire-now-section flex-full align-items-start align-content-start">
                            <div class="enquire-now-section-wrapper flex-full h-100 align-items-start align-content-start">
                            <span class="close"><i class="icn icn-close-black"></i></span>
                            <div class="minicart-main flex-full h-100 align-items-start align-content-start position-relative">
                                <h2 class="border-left">Office Furniture Enquiry</h2>
                                <div class="enquire-now-scroll">
                                                
                                <form class="signin-form credit-form flex-full" name="office_furniture_inquiry" id="office_furniture_inquiry">
                                    <div class="FlowupLabels" style="width: 95%;">
                                        <div class="fl_wrap">
                                            <label class="fl_label" for="inquiry_user_name">What's your name? *</label>
                                            <input class="fl_input" type="text" id="inquiry_user_name" name="inquiry_user_name" data-error="#inquiry_user_name_error">
                                            <div  class="error" id="inquiry_user_name_error"></div>
                                        </div>
                                        <div class="fl_wrap">
                                            <label class="fl_label" for="inquiry_user_mobile">Enter Mobile *</label>
                                            <input class="fl_input" type="text" id="inquiry_user_mobile" name="inquiry_user_mobile" data-error="#inquiry_user_mobile_error">
                                            <div  class="error" id="inquiry_user_mobile_error"></div>
                                        </div>
                                        <div class="fl_wrap">
                                            <label class="fl_label" for="inquiry_user_email">Enter Email *</label>
                                            <input class="fl_input" type="email" id="inquiry_user_email" name="inquiry_user_email" data-error="#inquiry_user_email_error">
                                            <div  class="error" id="inquiry_user_email_error"></div>
                                        </div>						
                                        <div class="fl_wrap populated">
                                            <!-- <label class='fl_label' for='city'>City *</label> -->
                                            <select class="fl_input" id="inquiry_user_city" name="inquiry_user_city" data-error="#inquiry_user_city_error">
                                                <option class="fl_label">Your City *</option>
                                                <option>Delhi</option>
                                                <option>Mumbai</option>
                                                <option>Pune</option>
                                                <option>Banglore</option>
                                                <option>Gurgaon</option>
                                                <option>Hydrabad</option>
                                                <option>Chennai</option>
                                                <option>Gaziabad/Noida</option>
                                            </select>
                                            <div class="error" id="inquiry_user_city_error"></div>
                                        </div>		
                                        
                                        <div class="fl_wrap">
                                            <label class="fl_label" for="inquiry_user_remarks">Your Requirements *</label>
                                            <input class="fl_input" type="text" id="inquiry_user_remarks" maxlength="500" name="inquiry_user_remarks" data-error="#inquiry_user_remarks_error">
                                            <div  class="error" id="inquiry_user_remarks_error"></div>
                                        </div>												
                                    </div>					
                                    <!-- <div class="form-submit-btn flex-full justify-content-center">
                                        <input type="submit" name="" class="explore-btn" value="Save">
                                    </div> -->
                                    <span class="submit-btn flex-full justify-content-center">
                                        <button type="submit" class="explore-btn" onclick="SubmitAddOfficeFurnishtureRequest()" id="requestSubmitButton">Submit</button>
                                    </span>
                                </form>
                                
                                </div>
                                <!-- <div class="checkout-wrapper flex-full">
                                <ul class="product-desc-listing flex-full">
                                    <li>
                                    <h4>Duration</h4>
                                    <span>24 Months</span>
                                    </li>
                                    <li>
                                    <h4>Advance Rental</h4>
                                    <span>&#x20B9; 2667.00</span>
                                    </li>
                                    <li>
                                    <h4>Refundable Deposit</h4>
                                    <span>&#x20B9; 6700.00</span>
                                    </li>
                                    <li>
                                    <h4>Sub Total</h4>
                                    <span>&#x20B9; 9596.00</span>
                                    </li>
                                </ul>
                                <span class="submit-btn flex-full justify-content-center">
                                    <input type="submit" name="" value="Submit" class="explore-btn" />
                                </span>
                                </div> -->
                            </div>
                            </div>
                        </div>
                        <!-- Office Furniture Enquiry Form section ends -->
                    </div>
                </div>
            </div>
            <!-- logo part ends -->
            <!-- navigation part starts -->
            <div class="navigation-part flex-full">
                <div class="container">
                    <div class="navigation-wrapper flex-full">
                        <ul class="flex-full justify-content-center">
                            <?php
                                // echo '<pre>';
                                // print_r($categoriesTree);die;
                                foreach ($categoriesTree as $row){
                                    if ($row->cat_name != '' && $row->cat_name != 'Our Picks' ){
                                        if($row->image != ''){
                                            $catImage = CDN_URL . 'images/category/' . $row->image;
                                        }else{
                                            $catImage = base_url() . 'images/wishlist_img_1.jpg';
                                        }
                                        if (isset($row->sub_categories)){
                                            if($row->cat_name != 'Addon' && $row->cat_name != 'Fitness'){
                            ?>
                                                <li><a href="<?php echo base_url(); ?><?php echo $cat_slug; ?>/<?php echo $row->seourl; ?>"><?php echo $row->cat_name; ?></a></li>
                            <?php
                                            }
                                        }else if($row->cat_name != 'Cred' && $row->cat_name != 'Credr2'){
                            ?>
                                            <li><a href="<?php echo base_url(); ?><?php echo $cat_slug; ?>/<?php echo $row->seourl; ?>"><?php echo $row->cat_name; ?></a></li>
                            <?php
                                        }
                                    }
                                }
                            ?>
                            <!--<li><a href="javascript:void(0)">Products</a></li>-->
                            <!--<li><a href="javascript:void(0)">Combos</a></li>-->
                            <!--<li><a href="javascript:void(0)">Fixed Rental Plans</a></li>-->
                            <!--<li><a href="javascript:void(0)">Corporate Orders</a></li>-->
                            <li><a href="pages/bulkorder">Corporate Orders</a></li>
                            <li><a href="pages/offers">Offers</a></li>
                            <li class="dropdown showdesktop hidden-xs"><a href="javascript:void(0)">Help</a>
                                <ul class="dropdown-menu">
                                    <li><a href="pages/how-it-works"><img src="http://localhost/new-design/images/how-it-works.svg">&nbsp; How It Works?</a></li>
                                    <li><a href="customerpayment" target="_blank"><img src="http://localhost/new-design/images/customer-review.svg">&nbsp; Customer Payment</a></li>
                                    <li><a href="pages/faq"><img src="http://localhost/new-design/images/fa-qs.svg">&nbsp; FAQs</a></li>
                                    <li><a href="reviews-testimonials/all"><img src="http://localhost/new-design/images/customer-review.svg">&nbsp; Customer Reviews</a></li>
                                    <li><a href="pages/careers"><img src="http://localhost/new-design/images/we-are-hiring.svg">&nbsp; We are hiring</a></li>
                                    <li><a href="https://45.113.122.221/blog/" target="_blank"><img src="http://localhost/new-design/images/blog.svg">&nbsp; Blog</a></li>
                                    <li><a href="pages/contact-us"><img src="http://localhost/new-design/images/contact.svg">&nbsp; Contact us</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- navigation part ends -->
            <!-- location part starts -->
            <div class="location-part flex-full">
                <div class="container">
                    <div class="location-part-wrapper flex-full justify-content-center text-center position-relative">
                        <?php if ($this->session->userdata("prcity")) { ?>
                        <span class="close"><i class="icn icn-close-dark"></i></span>
                        <?php }else{ ?>
                        <span class="close"><i class="icn icn-close-dark"></i></span>
                        <?php }?>
                        <h2><i class="icn icn-map"></i> Select City</h2>
                        <ul class="city-lisitng flex-full justify-content-center">
                             <?php foreach($cityList->result() as $val) { ?>
                                    <?php
                                        if($val->list_value_seourl == 'delhincr') {
                                            $imgname = 'delhi';
                                        } else if($val->list_value_seourl == 'bangalore') {
                                            $imgname = 'bangalore';
                                        } else {
                                            $imgname = $val->list_value_seourl;
                                        }
                                        // print_r($imgname);exit;
                                    ?>
                                <li>
                                    <a href="javascript:void(0)"  onclick="setcity('<?php echo $val->id; ?>')" class="city-box flex-full <?php if($_SESSION['cityslug'] == $imgname) { echo "selected"; } ?>">
                                        <!-- <i class="icn icn-delhi">
                                            <img src="<?php echo CDN_URL; ?>images/<?php echo $imgname.'.svg'; ?>" alt="<?php echo $val->list_value; ?>">
                                        </i> -->
                                        <i class="icn icn-<?php echo $imgname ?>"></i>
                                   
                                        <span class="city-name"><?php echo $val->list_value; ?></span>
                                    </a>
                                </li>
                            <?php } ?>
                           
                        </ul>
                    </div>      
                </div>
            </div>
            <!-- location part ends -->
            <!-- logo part (for mobile) starts -->
            <div class="mobile-logo-part flex-full">
                <div class="logo-part-wrapper flex-full justify-content-between align-items-center">
                    <div class="logo d-flex w-auto flex-wrap align-items-center">
                        <div class="menu-mobile">
                            <span></span>
                        </div>
                        <a href="http://45.113.122.221/" class="d-flex w-auto">                         
                            <amp-img src="<?php echo base_url('images/mobile-logo-white.png') ?>" alt="Cityfurnish" height="25" width="28"></amp-img>
                        </a>
                    </div>
                    <div class="mobile-page-heading">
                        <h2><?php echo $page_heading; ?></h2>
                    </div>
                    <div class="header-icons d-flex w-auto flex-wrap">
                        <div class="header-location d-flex w-auto flex-wrap">
                          <div class="location d-flex w-auto flex-wrap align-items-center">
                            <i class="icn icn-map"></i>
                            <?php
                                //  print_r($_SESSION['cityslug']);exit;
                                       if ($_SESSION['prcity'] && $_SESSION['prcity'] == '45') {
                                           echo "<span>Delhi</span>";
                                       } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '46') {
                                           echo "<span>Bangalore</span>";
                                       } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '47') {
                                           echo "<span>Pune</span>";
                                       } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '48') {
                                           echo "<span>Mumbai</span>";
                                       } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '49') {
                                           echo "<span>Ghaziabad/Noida</span>";
                                       } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '50') {
                                           echo "<span>Gurgaon</span>";
                                       } else if ($_SESSION['prcity'] && $_SESSION['prcity'] == '51') {
                                           echo "<span>Hyderabad</span>";
                                       } else {
                                           echo "<span>Select city</span>";
                                       }
                                       ?>
                            <i class="arrow-bottom"></i>
                          </div>
                        </div>
                        <ul class="header-icons-listing d-flex w-auto flex-wrap"> 
                            <?php
                                if($loginCheck != ''){
                            ?>
                                    <li>
                                        <a href="<?php echo base_url('cart') ?>" class="m_carticn">
                                            <i class="icn icn-cart-white"></i>
                                            <?php
                                                if($itemsIncart > 0){
                                            ?>
                                                    <span id="new_count"><?php echo count($itemsIncart); ?></span>
                                            <?php
                                                }
                                            ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url('usersettings') ?>">
                                            <i class="icn icn-profile-white"></i>
                                        </a>
                                    </li>
                            <?php
                                }else{
                            ?>
                                    <li>
                                        <a href="<?php echo base_url() ?>user_sign_up">
                                            <i class="icn icn-cart-white"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url() ?>user_sign_up">
                                            <i class="icn icn-profile-white"></i>
                                        </a>
                                    </li>
                            <?php
                                }
                            ?>
                        </ul>
                    </div>              
                </div>
            </div>
            <!-- logo part (for mobile) ends -->
            
            <!-- navigation part (for mobile) starts -->
            
            <div class="mobile-navigation flex-full bg-img align-content-start align-items-start" style="background-image: url(images/innerpage-banner.jpg);">
                <div class="mobile-navigation-wrapper flex-full">
                    <span class="close"><i class="icn icn-close-black"></i></span>
                    <ul class="mobile-navigation-part flex-full">
                       
                    <?php
                        foreach ($categoriesTree as $row) {
                            if ($row->cat_name != '' && $row->cat_name != 'Our Picks') {
                                $active = '';
                                if (strpos(current_url(), "/" . $row->seourl) !== false) {
                                    $active = 'active';
                                }
                                if (isset($row->sub_categories)) {
                                    if ($row->cat_name != 'Addon') {?>
                                    <li><i class="icn icn-home-furniture"></i><a onclick="onCategoryChange('<?php echo $cat_slug; ?>','<?php echo $row->seourl; ?>','p')"  href="javascript:void(0)"><?php echo $row->cat_name; ?></a>
                                        <ul class="sub-menu flex-full">
                                            <?php foreach ($row->sub_categories as $subCat) { ?>
                                                <li><i class="icn icn-office-furniture"></i><a onclick="onCategoryChange('<?php echo $cat_slug; ?>','<?php echo $subCat->seourl; ?>','p')"  href="javascript:void(0)" ><?php echo $subcatImage . $subCat->cat_name; ?></a></li>
                                            <?php } ?>
                                        </ul>
                                </li>
                            <?php } ?>
                            <?php } else if ($row->cat_name != 'Cred') { ?>
                            <!--<ul class="mobile-navigation-part flex-full">-->
                                <li><i class="icn icn-combos"></i><a onclick="onCategoryChange('<?php echo $cat_slug; ?>','<?php echo $row->seourl; ?>','p')" href="javascript:void(0)"><?php echo $row->cat_name; ?></a></li>
                            <!--</ul>-->
                             <?php } } } ?>

                    </ul>
                     <ul class="mobile-navigation-part flex-full">
                        <li><i class="icn icn-combos"></i><a href="javascript:void(0)">Combos</a></li>
                        <li><i class="icn icn-rental-plans"></i><a href="javascript:void(0)">Fixed rental plans</a></li>                
                    </ul>
                    <?php
                        if($loginCheck != ""){
                    ?>
                            <ul class="mobile-navigation-part flex-full">
                                <li>
                                    <i class="icn icn-combos"></i>
                                    <a href="usersettings">Account Setting</a>
                                    <ul class="sub-menu flex-full">
                                        <li><i class="icn icn-combos"></i><a href="purchases">My Orders</a></li>
                                        <li><i class="icn icn-combos"></i><a href="payments">My Payments</a></li>
                                        <li><i class="icn icn-combos"></i><a href="invoices">My Invoices</a></li>
                                        <li><i class="icn icn-combos"></i><a href="wallet">CF Coins</a></li>
                                        <li><i class="icn icn-combos"></i><a href="referral">Referral Code</a></li>
                                        <li><i class="icn icn-combos"></i><a href="usersettings">Profile Settings</a></li>
                                        <li><i class="icn icn-combos"></i><a href="documentation">Documentation</a></li>
                                        <li><i class="icn icn-combos"></i><a href="settings/shipping">Shipping Address</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <i class="icn icn-combos"></i>
                                    <a href="logout">Logout</a>
                                </li>
                            </ul>
                    <?php
                        }
                    ?>
                    <ul class="mobile-navigation-part flex-full">
                        <li><i class="icn icn-corportate-orders"></i><a href="javascript:void(0)">Corportate Orders</a></li>
                        <li><i class="icn icn-offers"></i><a href="javascript:void(0)">Offers</a></li>
                        <li><i class="icn icn-help"></i><a href="javascript:void(0)">Help</a></li>
                    </ul>
                </div>
            </div>
                              
                              
            <!--<div class="mobile-navigation flex-full bg-img align-content-start align-items-start" style="background-image: url(images/innerpage-banner.jpg);">-->
            <!--    <div class="mobile-navigation-wrapper flex-full">-->
            <!--        <span class="close"><i class="icn icn-close-black"></i></span>-->
            <!--        <ul class="mobile-navigation-part flex-full">-->
            <!--            <li><i class="icn icn-home-furniture"></i><a href="javascript:void(0)">Home Furniture</a>-->
            <!--                <ul class="sub-menu flex-full">-->
            <!--                    <li><i class="icn icn-office-furniture"></i><a href="javascript:void(0)">Living Room</a></li>-->
            <!--                    <li><i class="icn icn-office-furniture"></i><a href="javascript:void(0)">Bed Room</a></li>-->
            <!--                    <li><i class="icn icn-office-furniture"></i><a href="javascript:void(0)">Dining Room</a></li>-->
            <!--                    <li><i class="icn icn-office-furniture"></i><a href="javascript:void(0)">Study Room</a></li>-->
            <!--                </ul>-->
            <!--            </li>-->
            <!--            <li><i class="icn icn-office-furniture"></i><a href="javascript:void(0)">Office Furniture</a></li>-->
            <!--            <li><i class="icn icn-electronics"></i><a href="javascript:void(0)">Electronics</a>-->
            <!--                <ul class="sub-menu flex-full">-->
            <!--                    <li><i class="icn icn-office-furniture"></i><a href="javascript:void(0)">Home Appliances</a></li>                       -->
            <!--                </ul>-->
            <!--            </li>-->
            <!--            <li><i class="icn icn-fitness"></i><a href="javascript:void(0)">Fitness</a></li>-->
            <!--            <li><i class="icn icn-kids-furniture"></i><a href="javascript:void(0)">Kids Furniture</a></li>-->
            <!--        </ul>-->
            <!--        <ul class="mobile-navigation-part flex-full">-->
            <!--            <li><i class="icn icn-combos"></i><a href="javascript:void(0)">Combos</a></li>-->
            <!--            <li><i class="icn icn-rental-plans"></i><a href="javascript:void(0)">Fixed rental plans</a></li>                -->
            <!--        </ul>-->
            <!--        <ul class="mobile-navigation-part flex-full">-->
            <!--            <li><i class="icn icn-corportate-orders"></i><a href="javascript:void(0)">Corportate Orders</a></li>-->
            <!--            <li><i class="icn icn-offers"></i><a href="javascript:void(0)">Offers</a></li>-->
            <!--            <li><i class="icn icn-help"></i><a href="javascript:void(0)">Help</a></li>-->
            <!--        </ul>-->
            <!--    </div>-->
            <!--</div>-->
            <!-- navigation part (for mobile) ends -->
        </header>
        <!-- Header ends -->

        

        <!--Page Js--> 
        <script src="js/site/header_new_searchbox.js"></script> 
        <script>
            function setcity(v) {
                $.ajax({
                    url: '<?php echo base_url(); ?>site/ajaxhandler/setcity',
                    data: 'v=' + v,
                    dataType: 'json',
                    type: 'POST',
                    success: function (r) {
                        if (r.ok == false) {
                            alert("Exactly what are you trying to do??");
                        }
                        if (r.ok == true) {
                            window.location.reload();
                        }
                    }
                });
            }
        </script>
        <script>

            var clicked = false;//Global Variable
            function ClickLogin()
            {
                clicked = true;
            }

            function onSignIn(googleUser) {
                if (clicked) {
                    var profile = googleUser.getBasicProfile();
                    var datasend = {};
                    datasend['email'] = profile.getEmail();
                    datasend['gender'] = "";
                    datasend['first_name'] = profile.getName();
                    datasend['last_name'] = "";
                    datasend['facebook_id'] = profile.getId();
                    datasend['name'] = profile.getName();
                    datasend['login_type'] = "google";
                    datasend['str'] = '';

                    if (profile.getEmail()) {
                        $.ajax({
                            url: "<?php echo base_url(); ?>site/user/sociallogin",
                            type: 'POST',
                            dataType: 'json',
                            data: datasend,
                            success: function (x) {
                                location.reload();
                            }
                        });
                    }
                }

            }
            ;


        </script>
        <?php if (!$this->session->userdata("prcity")) { ?>
        <script>
            $(document).ready(function(){
    		    $(".location-part").slideDown();
    		    $("body").addClass('overlay');
    	    })
        </script>
    <?php }?>
        <meta name="google-signin-client_id" content="1065795218106-s2m2k3s28ch432hn8gp669pjjn7esr7d.apps.googleusercontent.com">
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        
