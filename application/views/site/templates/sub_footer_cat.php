<footer role="contentinfo" class="p_top_0">
				<div class="section_offset bg_grey_light_2 m_bottom_38">
					<div class="container m_bottom_13 m_top_10">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 m_xs_bottom_30">
								<h2 class="scheme_color tt_uppercase m_bottom_20" style="text-align:center;"><?php echo $meta_title; ?></h2>
								<hr class="divider m_bottom_10">
								<p  style="text-align:center;"><?php echo $meta_keyword; ?></p>
							</div>
						</div>
					</div>
				</div>
</footer>