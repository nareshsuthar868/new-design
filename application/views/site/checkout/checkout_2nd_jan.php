<?php
$this->load->view('site/templates/header_new_small');
?>
<style>
.razorpay-payment-button{ display:none; }
.mobile-content{ display:none; }
</style>
<!--main content-->
<div class="page_section_offset">
	<div class="container">
		<div class="row">
			<aside class="col-lg-2 col-md-2 col-sm-2 p_top_4">
			</aside>
			<section class="col-lg-8 col-md-8 col-sm-8">
				<?php if($flash_data != '') { ?>
				<div class="errorContainer" id="<?php echo $flash_data_type;?>"> 
				  <script>setTimeout("hideErrDiv('<?php echo $flash_data_type;?>')", 3000);</script>
				  <p><span><?php echo $flash_data;?></span></p>
				</div>
				<?php } ?>
						<?php $paypalProcess = unserialize($paypal_ipn_settings['settings']); 
							
							$checkAmt = @explode('|',$checkoutViewResults);
							$this->session->set_userdata(array('cartamount'=>(number_format($checkAmt[3],2,'.','') * 100)));	
						?>				
				<form action="https://cityfurnish.com/site/justpay/getrazorvals" method="POST">
<!-- Note that the amount is in paise = 50 INR -->
<script
    src="https://checkout.razorpay.com/v1/checkout.js"
    data-key="rzp_test_hwdYJPYjsEDyp6"
    data-amount="<?php echo (number_format($checkAmt[3],2,'.','') * 100); ?>"
    data-buttontext="Pay with Razorpay"
    data-name="CityFurnish"
    data-description="Cart Items"
    data-image="https://cityfurnish.com/images/logolarge.jpg"
    data-prefill.name="<?php echo $this->session->userdata('cartusername'); ?>"
    data-prefill.email="<?php echo $this->session->userdata('cartuseremail'); ?>"
    data-theme.color="#F37254"
></script>
<input type="hidden" value="Hidden Element" name="hidden">
</form>
<!--<style>
.razorpay-payment-button{ display:none; }
</style>-->
<!-- / wrapper-content -->
		</section>
						<aside class="col-lg-2 col-md-2 col-sm-2 p_top_2">
						</aside>
					</div>
				</div>
			</div>
			<!--footer-->
				<?php
				$this->load->view('site/templates/sub_footer');
				$this->load->view('site/templates/footer');
		?>
		</div>

		<!--back to top-->
		<button class="back_to_top animated button_type_6 grey state_2 d_block black_hover f_left vc_child tr_all"><i class="fa fa-angle-up d_inline_m"></i></button>

		<!--libs include-->
		<script src="plugins/jquery.appear.js"></script>
		<script src="plugins/owl-carousel/owl.carousel.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>
		<script src="plugins/jackbox/js/jackbox-packed.min.js"></script>
		<script src="js/retina.min.js"></script>
		<script src="plugins/colorpicker/colorpicker.js"></script>

		<!--theme initializer-->
		<script src="js/themeCore.js"></script>
		<script src="js/theme.js"></script>

<!-- Google Code for Purchase Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 867888766;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "hQXcCLP5iWwQ_tzrnQM";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<script>
setTimeout(function(){ $('.razorpay-payment-button').trigger('click'); }, 1000);
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/867888766/?label=hQXcCLP5iWwQ_tzrnQM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>


	</body>
</html>