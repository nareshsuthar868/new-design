<?php
$this->load->view('site/templates/header_inner');
?>
<!--main content-->

<div class="page_section_offset"> 
  <!-- New  html -->
  <section class="innerbanner">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
          <h1>Rental Agreement</h1>
          <ul class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active"> Rental Agreement</li>
          </ul>
        </div>
      </div>
    </div>
  </section>
  <!--<section class="rentalsubrow">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12 text-left">
          <h2 class="text-center"><strong>Subscription Agreement</strong></h2>
          <p>This Subscription Agreement ("Agreement") is made and deemed executed on the date _____________ ("Subscription Date")by the Company at _____________ ("City").</p>
          <p><strong>By and Between</strong></p>
          <p>Magneto Home Private Limited having its registered office at B-29, DHARAM NARAYAN KA HATHA PAOTA, JODHPUR RAJSTHAN 342001 IINDIA ("Company"). </p>
          <p>And</p>
          <p>The individuals identified as _____________________________________________ ("Subscriber").</p>
          <p>The Company and the Subscriber shall individually be referred to as 'Party' and collectively as 'Parties'.</p>
          <p><strong>Whereas</strong></p>
          <p>The Company is in the business of offering Solutions with respect to home furniture, office furniture, soft furnishings and fitness equipments on subscription basis.
            The Subscriber has approached the Company to subscribe to the products and services offered by the Company, and the Company has agreed to provide the same to the Subscriber.</p>
          <p> The Parties hereto wish to enter into this Agreement in order to record their mutual understanding. </p>
        </div>
      </div>
    </div>
  </section>-->
  <section class="rentaldocrow">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <div class="panel-group" id="accordion">
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse23"> 1. Subscription Agreement <i class="more-less hidden-xs expand-up"></i> <i class="more-less visible-xs expand-down"></i> </a> </div>
              </div>
              <div id="collapse23" class="panel-collapse collapse in">
                <div class="panel-body">
                  
          <p>This Subscription Agreement ("Agreement") is made and deemed executed on the date _____________ ("Subscription Date")by the Company at _____________ ("City").</p>
          <p><strong>By and Between</strong></p>
          <p>Magneto Home Private Limited having its registered office at B-29, DHARAM NARAYAN KA HATHA PAOTA, JODHPUR RAJSTHAN 342001 IINDIA ("Company"). </p>
          <p>And</p>
          <p>The individuals identified as _____________________________________________ ("Subscriber").</p>
          <p>The Company and the Subscriber shall individually be referred to as 'Party' and collectively as 'Parties'.</p>
          <p><strong>Whereas</strong></p>
          <p>The Company is in the business of offering Solutions with respect to home furniture, office furniture, soft furnishings and fitness equipments on subscription basis.
            The Subscriber has approached the Company to subscribe to the products and services offered by the Company, and the Company has agreed to provide the same to the Subscriber.</p>
          <p> The Parties hereto wish to enter into this Agreement in order to record their mutual understanding. </p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"> 2. Definition <i class="more-less expand-down"></i> </a> </div>
              </div>
              <div id="collapse1" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>In this Agreement, unless the context otherwise requires, the following capitalized words and expressions shall bear the meaning ascribed to them here-in-below:</p>
                  <p>"Solution" means the product and services offered by company as part of subscription programme.</p>
                  <p>"Service" means and includes delivery and installation of furniture, furnishing and fitness equipments.</p>
                  <p>"Initial Subscription Period" means the period of ____ months from Subscription Date or such period as amended from time to time on the basis of customer request.</p>
                  <p>"Security Deposit" means the amount deposited with the Company as per the solution provided by the company under the Subscription Programme.</p>
                  <p>"Subscriber" means any person who subscribes for the solution.</p>
                  <p>"Subscription" means the non-exclusive and non-transferable permission by the Company to experience the solution for personal consumption to the Subscriber.</p>
                  <p>"Subscription Fee" means monthly fee charged by the Company towards Subscription of aSolution by the Subscriber.</p>
                  <p>"Subscription Period" shall mean the period for which the Subscriber has taken the Subscription including the Initial Subscription Period.
                    "Subscription Program Documents" shall mean and include this Agreement, invoice and any other document as may be provided by the Company.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"> 3. Subscription of Solution <i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>The particular terms and conditions of Subscription of aSolution by a Subscriber shall be as per the prevailing Subscription Programme.</p>
                  <p>The Solution and any part thereof shall always be the property of the Company and the Subscriber shall return the same to the Company on Termination or expiry of Subscription Period.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"> 4. Subscription Fees <i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse3" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>Any Person can subscribe to the Solution for a Subscription Fee as per the prevailing Subscription Programme. Subscription Fees shall be paid within 5 days of due date as per subscription programme.</p>
                  <p>The Subscription Fee is inclusive of taxes, such as VAT, as applicable.</p>
                  <p>Delayed payment of Subscription Fee beyond due date shall attract a penal interest of 10% of due amount per month from the date of default till the time the Subscription Fee is paid. The minimum interest levied would be Rs. 300 per month of delay.</p>
                  <p>Non-payment of Subscription Fee within 2 (Two) weeks of due date could result in termination of this Agreement, removal of the Solution or part thereof and other legal procedures/remedies that the Company may at its sole discretion, decide to proceed with.</p>
                  <p>The Company shall levy a charge of Rs. 400/- for each cheque return/bounce or ECS/Standing Instructions dishonour.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">5. Security Deposit <i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse4" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>Every Subscriber shall deposit the prescribed refundable, interest free Security Deposit as indicated in the prevailing Subscription Programme for the Solution prior to the Subscription Date.</p>
                  <p>The Security Deposit shall be refunded after the Company has taken possession of all the products delivered as part of the Solution or part thereof and issue a Pick Up docket.</p>
                  <p>Within 7 (seven) working days from the date of issue of Pick Up dockets, the Company shall process for refund of Security Deposit, subject to deduction of damages, unpaid Subscription Fees and any other deductions as applicable.</p>
                  <p>Security deposit does not include any monthly subscription fee. It is simply a security deposit which takes care of the damages (if any) and any default in payments.</p>
                  <p>Company reserves the right to charge additional security deposit; or ask for additional documents in case of a high value order.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">6. Subscription Date, Use and Obligations <i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse5" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>The Company shall install/deliver the Solution or part thereof within 7 (Seven) days or any other mutually agreed period at the delivery address provided by the Subscriber from the receipt of the Security Deposit and a delivery receipt be issued by the Company. The date of delivery receipt shall be the Subscription Date.</p>
                  <p>The Solution or part thereof shall be used by the Subscriber for personal purposes or where applicable, for the purposes of its executives and his/her family members, servants and guests, without in any way creating right / title interest in the Solution or part thereof except as mentioned in this Agreement.</p>
                  <p>The Company shall repair or exchange the items in the Solution if the Subscriber faces any technical problems within 7 (seven) days of the receipt of such complaint in writing.</p>
                  <p>On or before expiry of Initial Subscription Period, Subscriber might request for an extension of subscription. The Subscription can be renewed at mutually agreed terms by the Parties.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">7. Joint Subscription<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse6" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>If the Subscription has been jointly subscribed, one of them at their mutual understanding shall be designated as the Primary Subscriber and the other(s) shall be treated as Secondary Subscriber.</p>
                  <p>The Primary Subscriber shall be the point of contact for the Company and any benefit accumulated under the Subscription Programme shall be offered and provided to the Subscriber.</p>
                  <p>In the event of Joint subscription, the accumulated benefit could be assigned by the Primary Subscriber to an identified Secondary Subscriber with the consent of the Company.</p>
                  <p>Notwithstanding the Clause 6, all joint Subscribers shall be liable for payment of the Subscription Fees, Security deposit or any damages or deductions arising under this Agreement.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">8. Delivery and Pickup<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse7" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>The Subscriber or his/her representative has to be present at the agreed date and time.If a 'mutually scheduled' delivery or pickup needs to be rescheduled due to non availability of the Subscriber, additional logistics costs incurred shall be charged to the Subscriber</p>
                  <p>Any delay in pick-up of products due to unavailability of Subscriber may attract additional charges depending upon delay duration</p>
                  <p>Though we conduct detailed quality checks before delivery, the Subscriber is expected to check if there are any damages at the time of delivery and report the same to representative of the Company and photos shall be captured of the same.</p>
                  <p>Photos of the Subscriber will be taken with the items delivered for Company's records. Subscriber is expected to allow Company's representatives to take the required photographs.</p>
                  <p>Subscriber shall ensure the entry of delivery vehicle inside the premises. Additionally, subscribershall arrange for the permission to use the elevator.In case an elevator isn't available at the delivery location, Subscriber shall inform the Company about the same before schedules delivery date. The Companyshallcharge for the labor involved for picking up and conveying the items via stairs. The charges shall be discussed with the Subscriber while scheduling the delivery or at the time of delivery.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">9. Maintenance<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse8" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>Maintenance of electrical appliances and fitness equipments shall be taken care of by the Company for the entire tenure of the contract. This does not cover damages or breakdowns due to mishandling.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">10. Inspection Rights<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse9" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>The Company or its designated representative(s) shall have the right to visit the Premise and inspect the various components constituting the Solution or part thereof by providing a 24 hours' notice, either through phone or email, to the Subscriber.</p>
                  <p>The Subscriber shall provide reasonable access to their Premise to enable the Company or its designated representatives to carry out the inspection and/or service and maintenance to the Solution or part thereof.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">11. Termination<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse10" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>Subject to this Agreement, this Agreement can be terminated during the Subscription Period (but only after the expiry of Initial Subscription Period) in following manner</p>
                  <p>Subscriber may terminate the Subscription after giving a 30 days written notice/email with the payment of applicable charges. </p>
                  <p>Following charges will be applicable for early termination</p>
                  <p>Termination before minimum tenure of 3 months: Full 3 month's rental due with rates as per 3 months tenure. For fitness products, minimum tenure being 4 weeks, for any cancellation before 4 weeks rental for 4 weeks will be payable</p>
                  <p>Termination after 3 months: Difference in monthly rental rates between contract tenure (the tenure selected at order placement) and actual tenure. Rental rate applicable will be as per actual tenure. e.g. if contract tenure selected at order placement was 12 months and subscription is terminated in 4 months, rental rates for 4 month duration will be applicable for rental difference calculation</p>
                  <p>In addition, if customer has availed an offer which is not applicable for actual tenure, amount equivalent to offer amount will also be payable.</p>
                  <p>The Company shall terminate the Subscription after giving a 10 days written notice/email to the Subscriber upon the Subscriber defaulting on the payment of Subscription Fees for one (1) consecutive months and has not paid till the expiry of the 10 days' notice period.</p>
                  <p>The Company may terminate the Subscription with immediate effect if</p>
                  <p>The Subscriber has been found to transfer or license the Subscription in its own name.</p>
                  <p>The Subscriber has transferred, assigned or leased the Subscription without Company's approval</p>
                  <p>The Subscriber has transferred the products to some other location without taking prior consent from the company</p>
                  <p>The Subscriber does not give reasonable access to their Premise to enable the Company or its designated representatives to carry out the inspection and/or service and maintenance to the Solution or part thereof</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse11">12. Effect of Termination<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse11" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>On Termination of the Subscription,</p>
                  <p>The Company shall refund the Security Deposit to the Subscriber after deducting due amount, charges, or damages, if any, in accordance with Clause 4 above.</p>
                  <p>The Subscriber shall return the items in same condition as taken (normal wear and tear accepted) and Company shall remove the items from the premises of the Subscriber at its own cost.</p>
                  <p>The Subscriber agrees to pay for any damage to, loss of, or any theft (disappearance) of items, regardless of cause or fault. Item damaged beyond repair will be paid for at its Market Price.</p>
                  <p>The damage shall be ascertained by comparing against the delivery note signed bySubscriber and the photographs taken on delivery and return pickup day and the extent of damage shall be ascertained by using the definition of damages given in Annexure 1. This damage cost shall be mitigated from the refundable deposit paid by the Subscriber.</p>
                  <p>The damages will be ascertained by the Company and its decision shall be final in this regard. The Company may waive the damages up to the value of INR 1,000 (One Thousand Rupees) only.</p>
                  <p>Refund amount shall be transferred to account from where initial deposit was received</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse12">13. Duty of Subscriber<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse12" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>The Subscriber shall not, use or attempt to use the Solution or any part thereof or permit any person to provide Solution or any part thereof to any third party by way of trade or otherwise.</p>
                  <p>The charges shall be exempted for normal wear and tear. However, if any item in the home Furniture or Home Décor component of the Solution is broken, torn, stained or is damaged during the Subscription Period, the Subscriber shall be responsible for compensating the Company in this regard.</p>
                  <p>During the Subscription Period, the Subscriber is solely responsible for the component of the Solution which must remain at the Premises and shall not be moved to another location without the prior written consent of the Company. Relocation of the Solution or part thereof or reinstallation of the Solution shall be as per the prevailing Subscription Programme.</p>
                  <p>Subscriber shall take reasonable good care of the Solution or part thereof and not sell, sub-hire, assign, convey, transfer or create any rights in relation to the Solution or part thereof to another person.</p>
                  <p>Subscriber may lease the house furnished with Solution only after the written approval of the Company.</p>
                  <p>Further, the Subscriber shall not alter, modify, re-design, re-colour, re-polish, dismantle, re-assemble, change any of the Solution or part thereof, without prior written permission of the Company.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse13">14. Intellectual Property Rights<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse13" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>The Intellectual Property Rights ("IPR") including trademarks, copyright, design and any other intellectual property in the Solution belongs to the Company. Solution and any other related materials / service via it and IPR therein may not be copied, distributed, published, licensed, used or reproduced in any way.</p>
                  <p>Subscriber shall do all reasonable endeavors to safeguard IPR of the Company in the Solution and perform no act which violates the IPR of the Company and report promptly to the Company if any third party violates or claims IPR in the Solution in knowledge of the Subscriber and co-operate in any enforcement or other protective action taken by the Company.</p>
                  <p>Subscriber shall not, and shall ensure that any other Person shall not use any reverse engineering, recompilation or disassembly techniques or similar methods to determine any design, concepts, construction method or other aspects of the Solution, or part thereof for any purpose.</p>
                  <p>Subscriber shall not make or attempt to make any alterations, modification, additions or enhancements through any means to the Solution or permit the whole or any part of the Solution to be combined with or become incorporated in any other program or Solution.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse14">15. Confidentiality<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse14" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>Subscriber shall keep confidential the terms and conditions of this Agreement and all information disclosed by the Company to the Subscriber in relation to or in connection to this Agreement including the intellectual property rights in the Solution.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse15">16. Disclaimer and Hold Harmless<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse15" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>The Subscriber agrees that it is the Subscriber's responsibility to always read the label and/or user manual before using the Solution.</p>
                  <p>THE COMPANY DISCLAIMS ALL WARRANTIES, EITHER EXPRESS OR IMPLIED, STATUTORY OR OTHERWISE, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR PARTICULAR PURPOSE WITH RESPECT TO THE SOLUTION OFFERED.</p>
                  <p>In no event shall the Company or its owners, affiliates, employees, contractors, officers, or agents be liable for any damages including, without limitation, incidental and consequential damages, personal injury/wrongful death, lost profits, or damages resulting from the Solution, whether based on warranty, contract, tort, or any other legal theory.</p>
                  <p>The Company shall not be liable for any damages, compensation etc. incurred by the Subscriber or any third Party because of Solution.</p>
                  <p>The Liability of the Company arising under or in relation to this Agreement shall not be more than one month Subscription Fees.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse16">17. Breach of terms<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse16" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>In the event of breach of any of the covenants and conditions to be observed and performed by the Subscriber hereunder, the Company may at their option terminate this Agreement by giving the Subscriber a prior ten (10) days' notice in writing, specifying the breach complained thereof and requiring its remedy and this Agreement would stand terminated on the expiry of said notice period, unless the Subscriber would have remedied or repaired the said breach before the expiry of the said notice period. Further, the Company shall retain its right to claim damages and/or deduct the damages from the Security Deposit or encash the undated cheques.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse17">18. Indemnity<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse17" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>Subscriber shall always indemnify, defend and hold harmless the Company from any third party claims including but not limited to by the landlord/owner of the Premises of trespass, damage to the premises, claim of criminal or civil nature or any loss, damage, attorney fees incurred in course/arising out of or in connection with this Agreement.</p>
                  <p>The Subscriber agrees to hold the Company indemnified and harmless from any and all claims, demands, rights, lawsuits, causes of action, obligations, controversies, debts, costs, expenses (including but not limited to attorneys' fees), damages, judgments, losses and liabilities of whatever kind or nature, fixed or contingent, in law, equity or otherwise, whether known or unknown, whether or not apparent or concealed arising out the Solution.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse18">19. Force Majeure<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse18" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>Upon the occurrence of any of the following events, including but not limited to fire, accident, riots, flood, earthquake, storm, terrorist activities, war, Acts of God, any governmental or municipal action (beyond the control of the Parties), prohibition or restriction, which in any way results in making the Solution or part thereof unfit, the Company shall have the right to terminate this Agreement forthwith and claim asset or value of the asset as defined by the company in books of account.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse19">20. Statutory Action<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse19" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>If the Subscriber is dispossessed from the usage of Solution or part thereof as a result of any legal proceeding or action against the Company in respect to the Solution or part thereof for the breach by the Company of any law, regulation, rules, bye-laws in force in India, the Agreement shall stand terminated from the date of dispossession of the Subscriber.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse20">21. Dispute Resolution and Arbitration<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse20" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>In the event of a dispute or difference of any nature whatsoever between the Parties, the same shall be, as far as possible, be resolved through negotiations and in the event of failure of dispute resolution by negotiations, the dispute shall be referred to Arbitration. </p>
                  <p>Either Party to this agreement can refer the dispute for resolution to a sole arbitrator or in case of disagreement in the appointment of the arbitrator, then to three arbitrators, of which each Party shall nominate one and the third arbitrator shall be appointed by the said two arbitrators. The decision of the Arbitral Tribunal shall be final and binding on both the parties. The venue of arbitration shall be Gurgaon and the Arbitration proceedings shall be conducted in accordance with provisions of the Arbitration and Conciliation Act, 1996 or any subsequent </p>
                  <p>modifications thereto. The proceeding shall be in English. Each party shall bear and pay its own costs and expenses in connection with the arbitration proceedings unless the arbitrators direct otherwise.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse21">22. Miscellaneous<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse21" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>Notice: Any notice to be served on the other Party shall be sent to the address mentioned in this Agreement or as updated by the Parties from time to time through pre-paid recorded delivery or through email and shall be deemed to have been received by the addressee within 48 hours of posting.</p>
                  <p>Assignment: The Company may assign any of its rights under this Agreement to any person or entity without the prior written consent of the Subscriber. The Subscriber may assign any of its rights under this Agreement to any person or entity only upon receipt of prior written consent of the Company.</p>
                  <p>Amendments: No change, modification, or termination of any of the terms, provisions, or conditions of this Agreement shall be effective unless made in writing and signed or initialed by all signatories to this Agreement.</p>
                  <p>Survival: Termination of this Agreement shall not affect those provisions hereof that by their nature are intended to survive such termination including Clause 4, 9, 11, 12, 13, 14, 15, 17, 18, 19.</p>
                  <p>Governing Law and Jurisdiction: This Agreement shall be governed and construed in accordance with the laws of India in relation to any legal action or proceedings to enforce this Agreement. The Parties irrevocably submit to the exclusive jurisdiction of any competent courts situated at Gurgaon and waive any objection to such proceedings on grounds of venue or on the grounds that the proceedings have been brought in an inconvenient forum.</p>
                  <p>Severability: If any paragraph, sub-paragraph, or provision of this Agreement, or the application of such paragraph, sub-paragraph, or provision, is held invalid or excessively broad by a court of competent jurisdiction, the remainder of this Agreement, and the application of such paragraph, sub-paragraph, or provision to Persons, or circumstances other than those with respect to which it is held invalid shall not be affected.</p>
                  <p>Entire Agreement: This Agreement along invoice and other subscription program documents constitutes the entire agreement between the Parties with respect to the matters contained herein and supersedes any and all prior and contemporaneous agreements, negotiations, correspondence, undertakings and communications of the parties, oral or written, with respect to the subject matter of this Agreement.</p>
                  <p>The Subscriber shall inform at least ten (10) days before the expiry of Initial Subscription Period to the Company about his/her discontinuance of the Subscription upon expiryof Initial Subscription Period. If no such information is provided, the Solution will be automatically extended for life time without any further action/information from the Subscriber.</p>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <div class="panel-title"> <a data-toggle="collapse" data-parent="#accordion" href="#collapse22">23. Annexure 1<i class="more-less expand-down"></i></a> </div>
              </div>
              <div id="collapse22" class="panel-collapse collapse">
                <div class="panel-body">
                  <p>Damage shall be defined as follows:</p>
                  <p>Minor scratches (below 1mm in width and depth, and 2cm in length) on wooden furniture shall be ignored as they are considered 'normal wear and tear'.</p>
                  <p>Minor chips and breakage in timber (below 5mm in width, 1mm in depth and 1cm in length) shall be ignored, while those above the said dimensions shall be charged for.</p>
                  <p>Any damage which is a result of raw material or manufacturing defects shall not be chargeable.</p>
                  <p>Any damage that results in the product being unusable shall result in the value of the product being charged to the subscriber.</p>
                  <p>Any damage that results in the replaceable part of the product being unusable shall result in the value of the part being charged to the subscriber.</p>
                  <p>Tear in upholstery shall result in charge towards replacement of upholstery. Opening up of a stitched joint shall not be chargeable.</p>
                  <p>Stains on upholstery which are not removable via dry cleaning shall result in a charge for upholstery replacement.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  
  <!-- New  html --> 
</div>
<!--footer-->
<?php
$this->load->view('site/templates/footer');
?>
</div>
</body></html>