<?php
$this->load->view('site/templates/header_inner');
?>
<div class="page-wrapper customer-payments-page flex-full">
	<!-- Inner page heading starts -->
	<section class="innerpage-heading flex-full justify-content-center">
		<div class="container">
			<div class="innerpage-heading-wrapper flex-full justify-content-center bg-img position-relative" style="background-image: url(images/innerpage-banner.jpg);">
				<div class="innerpage-description flex-full justify-content-center text-center">
					<h1>Frequently Asked Questions</h1>
					<ul class="breadcrumbs-listing flex-full align-items-center justify-content-center">
						<li class="root-link"><a href="javascript:void(0)">Home</a></li>				
						<li class="child-link"><a href="javascript:void(0)">Help</a></li>
						<li class="current-link"><a href="javascript:void(0)">FAQ</a></li>
					</ul>						
				</div>
			</div>
		</div>
	</section>
	<!-- Inner page heading ends -->
	<div class="faq-main flex-full">
		<div class="container"> 
			<div class="faq-main-wrapper flex-full align-items-start align-content-start">
				<div class="faq-tab flex-full align-items-center align-content-center">
					<div class="tab-links-part flex-full">
						<ul class="tabs flex-full">
							<li class="tab-link current" data-tab="tab-1"><span>Delivery</span></li>
							<li class="tab-link" data-tab="tab-2"><span>Service</span></li>
							<li class="tab-link" data-tab="tab-3"><span>Returns and Cancellation</span></li>
							<li class="tab-link" data-tab="tab-4"><span>Payment and Billing</span></li>
						</ul>
					</div>
					<div class="tab-content-part flex-full">
						<div id="tab-1" class="tab-content current">
							<div class="referral-code-faq account-accordion flex-full">
								<div class="accordion">
									<div class="accordion-tab current">
										<h3 class="accordion-title">What is the minimum tenure for renting?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>The minimum tenure for renting varies from product to product. If your required period is not covered on our product page, we will be happy to discuss your requirement and fulfill the same.</p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">Is there a contract? What are the terms?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>Yes, you are required to sign a contract at the time of delivery. The contract will include the basic terms of renting furniture in simple words. You can view sample of the same <a href="pages/rental-agreement"><u>here</u></a></p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">How can I terminate the contract?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>The contract can be terminated by providing us 2 weeks prior notice and paying applicable additional charges. Following charges will be applicable to early termination. Termination before minimum tenure of 3 months: Full 3 month's rental due with rates as per 3 months tenure. For fitness products, minimum tenure being 4 weeks, for any cancellation before 4 weeks of rental; the rental amount  for 4 weeks will be payable. If terminated after 3 months; one month extra rent will be charged. For example:  if the contract tenure selected at order placement was 12 months and the subscription is terminated within 4 months, rental of 5 months will be charged.</p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">What is the delivery timeline?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>We typically deliver the products within 72 hours of KYC process completion. However, delivery time might vary from product to product and the same is mentioned on the product page. If you need any product before mentioned delivery time, we will be happy to discuss the possibility. Please ensure submitting all your required KYC documents after placing the order, failing to do so might delay the delivery process.</p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">Who will deliver and setup the products?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>Our team will be visiting your place at your convenient time to deliver and install products at the delivery address.  Extra charges would be levied for delivery and installation for more than 3 floors, without lift. The same has to be paid by the customer in cash at the time of delivery.</p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">What if my society does not allow delivery vehicles inside?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>Please note that you should ensure the entry of delivery vehicle inside the premises. Most of the times, it is not allowed to park the delivery vehicles on the road. Please also ensure that you have completed the required documents and payment work mandatory with the landlord, we have observed that sometimes customers do not have permission to shift into the new house.</p>
											</div>
										</div>
									</div>									
									<div class="accordion-tab">
										<h3 class="accordion-title">What if my building does not have a lift?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>In case you do not have a lift or permission to use the lift at your premises, extra labor charges will be applicable to carry the products via stairs; amount will depend on the order size and floor level.</p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">What if I am not at home at the time of delivery?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>After you place the order, our representative will give you a call to book a delivery slot. In case you are not at home, you have to appoint a representative (flatmate/friend/relative) to collect the order and sign the contract on your behalf. Lessee or his/her representative has to be present at the agreed date and time. Otherwise extra shipping costs, INR 900 will be charged. Also, Lessee representative should carry an original/soft copy of Lessee ID proof at the time of accepting the delivery</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="tab-2" class="tab-content">
							<div class="referral-code-faq account-accordion flex-full">
								<div class="accordion">
									<div class="accordion-tab current">
										<h3 class="accordion-title">What sort of documentation is involved?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>You are required to provide following documents after placing the order.
                                                <li><strong>ID Proof -</strong> Company ID Card/Student ID Card</li>
                                               <li><strong>Permanent Address Proof -</strong> Passport/Aadhaar/Voter ID Card/Driving License</li>
                                                <li><strong> Delivery Address Proof -</strong> In case delivery address is different from permanent address,please provide us rental agreement and contact details of your landlord.</li>
                                                <li><strong>Bank Statement -</strong> Last 3 Months</li></p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">Where do you guys operate?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>We are currently operating in Delhi, Gurgaon, Noida, Pune, Mumbai and Bangalore. Soon we will launch our services in other major cities as well.</p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">Can we add or delete products from your packages? Will the prices be adjusted?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>You can make your own package by choosing the individual products. Still if you need any assistance, we are here to help.</p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">Can the contract be transferred?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>Yes, the contracts can be transferred. In that case a new contract will be created for remaining tenure and you will have to bear the additional cost incurred in relocating the furniture (If any).</p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">How can I place a bulk order?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>For any bulk order or special requirements, call our customer care or drop us a mail. A dedicated account manager will call you and take care of your requirements.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="tab-3" class="tab-content">
							<div class="referral-code-faq account-accordion flex-full">
								<div class="accordion">
									<div class="accordion-tab current">
										<h3 class="accordion-title">What is the return process?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>Our team will call you one week before expiry of contract and to arrange pick up as per your convenience. Your deposit refund will be processed within 7 days of pick up after adjusting for any damage/issue.</p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">When and how do I get my refundable deposit back?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>Once the products are picked up from your place, they undergo a  quality check by the QC team at the warehouse. If found damaged, then repair charges will be deducted from your refundable deposit. Also, if there is any amount due towards early termination charges or rental, the same will be adjusted from your deposit, balance if any you will have to pay for the same. The refund will get processed within 7 working days after pickup and after that it will take 7-10 more days to get reflected in your account.</p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">What is the return policy in case the product is damaged?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>If product is found damaged at the time of delivery and setup, we will will arrange the replacement of the same.</p>
											</div>
										</div>
									</div>								
								</div>
							</div>
						</div>
						<div id="tab-4" class="tab-content">
							<div class="referral-code-faq account-accordion flex-full">
								<div class="accordion">
									<div class="accordion-tab current">
										<h3 class="accordion-title">How much do I need to pay and when?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>You need to pay first month's rental and security deposit in advance. For remaining monthly rental, we will collect post dated cheques at the time of delivery of product. You can also use NACH facility for monthly rental payment. Monthly rent to be paid within 5 days after receipt of invoice. Late payment charges at the rate of 10% of the amount due per month (and a minimum of Rs 300) will be payable after a period of 5 days. Cityfurnish, reserves the right to cancel the subscription and pickup of products if amount due is not paid within 2 weeks of due date.</p>
											</div>
										</div>
									</div>	
									<div class="accordion-tab">
										<h3 class="accordion-title">What is the mode of payment?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>At the time of placing the order, you can pay first rental and security deposit by Debit /Credit / Net Banking/ Citrus Wallet. For remaining duration, our team will collect post-dated cheques from you at the time of delivery. You can also use NACH facility or online payment mode for monthly rental payment.</p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">How to register for auto debit of monthly rental?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>You can register for auto debit of monthly payment by providing standing instructions on credit card or ENACH on bank account while placing order or later at any time. Amount will be deducted monthly for tenure of your subscription. You can cancel auto debit anytime by simply contacting our team. Standing instructions and ENACH will get cancelled automatically on tenure expiry or order cancellation.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php  ?>

			<!--footer-->
				<?php
					$this->load->view('site/templates/footer');
				?>
		</div>

		
		<!--libs include-->
		<script src="plugins/jquery.appear.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>

		<!--theme initializer-->
		<script src="js/themeCore.min.js"></script>
		<script src="js/theme.min.js"></script>
	</body>
</html>