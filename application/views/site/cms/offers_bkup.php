<?php
$this->load->view('site/templates/header_inner');
?>
<div class="page_section_offset">
  <div class="offertopheader">
  	<div class="offerslider">
    	<div>
        	<img src="<?php echo base_url(); ?>images/offerthumb/offer-slider1.jpg" alt="slider1" />
        	<div class="offercaption"><strong>Start Renting with Us</strong><br />Go Rent Free for First 2 Months</div>
        </div>
<!--        <div>
        	<img src="<?php echo base_url(); ?>images/offerthumb/offer-slider1.jpg" alt="slider2" />
        	<div class="offercaption"><strong>35% Cashback</strong><br />on first Product</div>	
        </div>
        <div>
        	<img src="<?php echo base_url(); ?>images/offerthumb/offer-slider1.jpg" alt="slider2" />
        	<div class="offercaption"><strong>30% Cashback</strong><br />on first Product</div>
        </div>-->
    </div>
  </div>
  <div class="container">
    <section class="offer-row">
      <div class="row">
       <!-- <h2>Offers and Discounts</h2>-->
        <div class="col-lg-6 col-md-6 col-sm-6 m_bottom_30">
            <div class="pricing_table relative">
              <div class="offerthumbleft">
              	<img src="<?php echo base_url(); ?>images/offerthumb/ipl_offer1.jpg" alt="offer" />
              </div>
              <div class="offerdetailright"> 
                  <div class="graycolfirst"><span>Max Discount Applicable <strong>Rs 1,000</strong> in first month</span> </div>
                  <ul>
                    <li>Coupon Code: <b>SHORT3M</b></li>
                    <li>First month's rent OFF on furniture and home appliances</li>
                    <li>Applicable on min 3 months tenure</li>
                    <li class="text-center"><a href="<?php echo base_url();?><?php echo $cat_slug; ?>/IPL-special" class="btn-primary">1 Month Rent Off</a></li>
                  </ul>
              </div>
            </div>
        </div>
        <!--<div class="col-lg-6 col-md-6 col-sm-6 m_bottom_30">
            <div class="pricing_table relative">
              <div class="offerthumbleft">
              	<img src="<?php echo base_url(); ?>images/offerthumb/offer2upd.jpg" alt="offer" />
              </div>
              <div class="offerdetailright"> 
                  <div class="graycolfirst"><span>Max Discount Applicable <strong>Rs 3,000</strong> in first month</span> </div>
                  <ul>
                    <li>Coupon Code: <b>DEAL6M</b></li>
                    <li>First month's rent OFF on furniture and home appliances</li>
                    <li>Applicable on min 6 months tenure</li>
                    <li class="text-center">
                    	<a href="<?php echo base_url();?><?php echo $cat_slug; ?>/furniture-rental" class="btn-primary">1 Month Rent Off</a>
                    </li>
                    
                  </ul>
              </div>
              
            </div>
        </div>-->
        <div class="col-lg-6 col-md-6 col-sm-6 m_bottom_30">
            <div class="pricing_table relative">
              <div class="offerthumbleft">
              	<img src="<?php echo base_url(); ?>images/offerthumb/offer3.jpg" alt="offer" />
                
              </div>
              <div class="offerdetailright"> 
                  <div class="graycolfirst"><span>Max Discount Applicable <strong>Rs 2,000/mon</strong> in first 2 months</span> </div>
                  <ul>
                    <li>Coupon Code: <b>LONG9M</b></li>
                    <li>First 2 month's rent OFF on furniture and home appliances</li>
                    <li>Applicable on min 9 months tenure</li>
                    <li class="text-center">
                    	<a href="<?php echo base_url();?><?php echo $cat_slug; ?>/furniture-rental" class="btn-primary">2 Months Rent Off</a>
                    </li>
                    
                  </ul>
              </div>
              
            </div>
        </div>
       <!-- <div class="col-lg-6 col-md-6 col-sm-6 m_bottom_30">
            <div class="pricing_table relative">
              <div class="offerthumbleft">
              	<img src="<?php echo base_url(); ?>images/offerthumb/offer1upd.jpg" alt="offer" />
              </div>
              <div class="offerdetailright"> 
                  <div class="graycolfirst"><span>Max Discount Applicable <strong>Rs 10,000</strong> in first month</span> </div>
                  <ul>
                    <li>Coupon Code: <b>DEAL12M</b></li>
                    <li>First month's rent OFF on furniture and home appliances</li>
                    <li>Applicable on min 12 months tenure</li>
                    <li class="text-center"><a href="<?php echo base_url();?><?php echo $cat_slug; ?>/furniture-rental" class="btn-primary">1 month rent Off</a></li>
                  </ul>
              </div>
            </div>
        </div>	-->	
        <div class="col-lg-6 col-md-6 col-sm-6 m_bottom_30">
            <div class="pricing_table relative">
              <div class="offerthumbleft">
              	<img src="<?php echo base_url(); ?>images/offerthumb/offer5.jpg" alt="offer" />
                
              </div>
              <div class="offerdetailright"> 
                  <div class="graycolfirst graycol-last">
                   <span>
                  	<strong>15% Discount</strong> (On your first order)
                   </span> 
                  </div>
                  <ul>
                    <li>Coupon Code: <b>WELCOME</b></li>
                    <li class="listofferhei">Applicable on furniture and appliances.</li>
                    <li>Applicable on all tenures</li>
                    <li class="text-center"><a href="<?php echo base_url();?><?php echo $cat_slug; ?>/furniture-rental" class="btn-primary">on every month's rent</a></li>
                    
                  </ul>
              </div>
              
            </div>
        </div>
         <div class="col-lg-12">
          <div class="voucher-rw">
            <div class="voucherleft">
              <img src="images/voucher-left-pic.jpg" alt="voucher">
            </div>
            <div class="vouchercontent">
              <div class="priceround">
                <img src="images/ruppes.svg">    
                <span><?php echo $voucher_data->row()->reward; ?></span>
              </div>  
              <div class="righticntitle">
              <span class="gifticn"><img src="images/voucher-icn.svg"></span>
              <div class="titlevocher">
                <h3><?php echo $voucher_data->row()->voucher_title; ?></h3>
                <h4><?php echo $voucher_data->row()->voucher_label; ?></h4>
              </div>
              </div>
            </div>
            <div class="bottomsubtitle">
              <p>This will be irrespective of any other coupon code used in order.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-12 stepacc">
           <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <i class="more-less expand-down"></i>Steps to Redeem Vouchers</a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                          <?php echo $voucher_data->row()->steps_desc; ?>
                    </div>
                </div>
            </div>
         </div><!-- panel-group -->
      </div>
        <!--<div class="col-lg-6 col-md-6 col-sm-6 m_bottom_30">
            <div class="pricing_table relative">
              <div class="offerthumbleft">
              	<img src="<?php echo base_url(); ?>images/offerthumb/offer4.jpg" alt="offer" />
              </div>
              <div class="offerdetailright"> 
                  <div class="graycolfirst"><span>On Erica Range <strong>Flat 20% Off</strong> (No Maximum Limit)</span> </div>
                  <ul>
                    <li>Coupon Code: <b>ERICA20</b></li>
                    <li>Flat 20% off on Erica Range</li>
                    <li>Applicable on all tenures</li>
                    <li class="text-center">
                    	<a href="<?php echo base_url();?><?php echo $cat_slug; ?>/furniture-rental" class="btn-primary">On Every Month's Rent</a>
                    </li>
                    
                  </ul>
              </div>
              
            </div>
        </div>	-->
        <div class="col-lg-12 col-md-12 col-sm-12 termsoffer">
        <h3 class="m_bottom_20">Terms &amp; Conditions</h3>
        <ul>
          <li>Only one offer can be availed per order.</li>
          <li>Offers cannot be used more than once by a customer or on the same delivery address.</li>
          <li>These offers are not applicable in case of early closure of the order.</li>
          <li>Offers are applicable on furniture and appliances only.</li>
          <li>Offers are not applicable on fitness and add-on products</li>
          <li>These offers cannot be clubbed with referral program benefits.</li>
          <li>It takes us 15 days to process cash back (wherever applicable).</li>
          <li>You can avail these offers by simply applying the respective coupon code while checkout.</li>
          <li>For more detail about availing these offers, get in touch with our customer care team.</li>
          
        </ul>
      </div>
      </div>
    </section>
    
    
  </div>
</div>
<!--footer-->
<?php
$this->load->view('site/templates/footer');
?>
</div>
<script>
	$('.offerslider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		infinite: true,
	});
</script>
</body>
</html>
