<?php
$this->load->view('site/templates/header_inner');
?>

<div class="page-wrapper customer-payments-page flex-full">
	<!-- Inner page heading starts -->
	<section class="innerpage-heading flex-full justify-content-center">
		<div class="container">
			<div class="innerpage-heading-wrapper flex-full justify-content-center bg-img position-relative" style="background-image: url(images/innerpage-banner.jpg);">
				<div class="innerpage-description flex-full justify-content-center text-center">
					<h1>Contact Us</h1>
					<ul class="breadcrumbs-listing flex-full align-items-center justify-content-center">
						<li class="root-link"><a href="javascript:void(0)">Home</a></li>				
						<li class="child-link"><a href="javascript:void(0)">Help</a></li>
						<li class="current-link"><a href="javascript:void(0)">Contact Us</a></li>
					</ul>						
				</div>
			</div>
		</div>
	</section>
	<!-- Inner page heading ends -->
	<div class="contact-us-main flex-full">
		<div class="container"> 
			<div class="contact-us-main-wrapper flex-full align-items-start align-content-start">
				<div class="contact-us-info flex-full align-items-start align-content-start">
					<div class="queries-block flex-full">
						<h2>Have Queries?</h2>
						<span>Read the most frequently asked questions here</span>
						<ul class="queries-listing flex-full">
							<li>
								<h3>What is the minimum tenure for renting?</h3>
								<p>The minimum tenure for renting varies from product to product. If your required period is not covered on our product page, we will be happy to discuss your requirement and fulfill the same.</p>
							</li>
							<li>
								<h3>Is there a contract? What are the terms?</h3>
								<p>Yes, you are required to sign a contract at the time of delivery. The contract will include the basic terms of renting furniture in simple words. You can view sample of the same <a href="pages/rental-agreement">here</a>.</p>
							</li>
						</ul>
						<a href="<?php echo base_url('pages/faq') ?>" class="explore-btn">Check FAQ</a>
					</div>
					<div class="contact-block flex-full">
						<h2>Still can not find an answer?</h2>
						<span>Feel free to contact us with service questions, business proposals or media inquiries.</span>
						<ul class="contact-listing flex-full">
							<li>
								<i class="icn icn-service-request"></i>									
								<div class="contact-listing-wrapper flex-full">
									<h3>Raise a Service Request</h3>
									<p>Log request against your order from My Account section</p>
								</div>
							</li>
							<li>
								<i class="icn icn-phone"></i>									
								<div class="contact-listing-wrapper flex-full">
									<h3>Phone</h3>
									<p><a href="tel:+(91) 8010845000">+(91) 8010845000</a> <small>( 09:00AM - 09:00PM )</small></p>
								</div>
							</li>
							<li>
								<i class="icn icn-service-request"></i>									
								<div class="contact-listing-wrapper flex-full">
									<h3>Email</h3>
									<p><a href="mailto:hello@cityfurnish.com">hello@cityfurnish.com</a></p>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="offices-main flex-full">
					<h2>Our Offices</h2>
					<span>Cityfurnish India Private Limited</span>
					<ul class="offices-listing flex-full">
						<li>
							<div class="offices-box flex-full">
								<h3>Gurgaon Office</h3>
								<address>525-527, Block D, JMD Megapolis, Sohna Road, <br/>Sector 48, Gurgaon, Haryana , 122018</address>
							</div>
						</li>
						<li>
							<div class="offices-box flex-full">
								<h3>Noida Office</h3>
								<address>#72, Shahadra Sector-141,<br/>Noida, UP</address>
							</div>
						</li>
						<li>
							<div class="offices-box flex-full">
								<h3>Bangalore Office</h3>
								<address>2nd Floor, D, 310, 6th Cross Rd, 1st Block , <br/> Koramangala Bengaluru, Karnataka - 560035</address>
							</div>
						</li>
						<li>
							<div class="offices-box flex-full">
								<h3>Pune Office</h3>
								<address>03A, 4th Floor, City Vista, Kharadi, Pune, <br/> Maharashtra - 411014</address>
							</div>
						</li>
						<li>
							<div class="offices-box flex-full">
								<h3>Mumbai Office</h3>
								<address>Yadav Nagar, Chandivali, Powai Mumbai,<br/> Maharashtra - 400072</address>
							</div>
						</li>
						<li>
							<div class="offices-box flex-full">
								<h3>Bangalore Office</h3>
								<address>2nd Floor, D, 310, 6th Cross Rd, 1st Block , <br/> Koramangala Bengaluru, Karnataka - 560035</address>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
	<?php
$this->load->view('site/templates/footer');
?></div>

<!-- WhatsHelp.io widget -->
<!-- <script type="text/javascript">
	    (function () {
	        var options = {
	            whatsapp: "9871309994", // WhatsApp number
	            company_logo_url: "//static.whatshelp.io/img/flag.png", // URL of company logo (png, jpg, gif)
	            greeting_message: "Hello, how may we help you? Just send us a message now to get assistance.", // Text of greeting message
	            call_to_action: "Message us", // Call to action
	            position: "left", // Position may be 'right' or 'left'
	        };
	        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
	        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
	        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
	        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
	    })();
	</script> -->								
<!-- /WhatsHelp.io widget -->

		<!--libs include-->
		<script src="plugins/jquery.appear.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>

		<!--theme initializer-->
		<script src="js/themeCore.min.js"></script>
		<script src="js/theme.min.js"></script>
		<style>
			input[type="radio"]{ display: block !important; }
			fs_ex_large{ color: red; }
		</style>
		<script>
			$(document).ready(function(){
				setTimeout(function(){ $('#removealkala').fadeOut(); }, 2000);
			});
		</script>
	</body>
</html>