<?php
$this->load->view('site/templates/header_inner');
?>
<div class="page-wrapper flex-full">
	<section class="corporate-orders-main flex-full">
		<div class="corportate-heading flex-full">
			<div class="container">
				<div class="corportate-heading-wrapper flex-full justify-content-center text-center">
					<h1 class="border-center">Corporate Orders</h1>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				</div>
			</div>
		</div>
		<div class="corporate-needs-block flex-full">
			<div class="container">
				<div class="corporate-needs-block-wrapper flex-full align-items-start align-content-start">
					<div class="corporate-needs-left flex-full">
						<h2>Fulfilling all your corporate needs</h2>
						<ul class="corporate-needs-listing flex-full"> 
							<li>
								<div class="corportate-needs-wrapper flex-full">
									<figure>
										<img src="images/product.jpg" alt="corportate-order">
									</figure>
									<h3>Hospitality</h3>
									<p>Furniture for Hotels and Restaurants</p>
								</div>
							</li>
							<li>
								<div class="corportate-needs-wrapper flex-full">
									<figure>
										<img src="images/product.jpg" alt="corportate-order">
									</figure>
									<h3>Office</h3>
									<p>Office Furniture & Other Equipments</p>
								</div>
							</li>
							<li>
								<div class="corportate-needs-wrapper flex-full">
									<figure>
										<img src="images/product.jpg" alt="corportate-order">
									</figure>
									<h3>Co-living & co-working</h3>
									<p>Furniture for Co-work & Co- living</p>
								</div>
							</li>
						</ul>
						<div class="custom-made-products flex-full">
							<figure>
								<img src="images/cheers-vector.png" alt="custom-made-products">
							</figure>
							<div class="custom-made-products-wrapper flex-full">
								<h3>Custom Made Products</h3>
								<p>Need something unique? We will make it for you. We provide custom made furniture as per the demand of your project</p>
								<span class="call"><i class="icn icn-phone-line-black"></i>Call: <a href="tel:8010845000">8010845000</a></span>
							</div>
						</div>
					</div>
					<aside class="enquire-block flex-full">
						<h2>Enquire</h2>
						<p>Mention your requirements in brief & we will get back to you promptly</p>
						<h3 id="response_msg" style="display: none;">You enquiry successfully send to our team.</h3>
						<form class="corporate-orders-form credit-form flex-full" id="bulk_order_form" name="bulk_order_form">
							<div class="FlowupLabels">
								<div class="fl_wrap">
									<label class='fl_label' for='User_Name'>Full Name</label>
									<input class='fl_input' type='text' name="User_Name" id='User_Name' />
								</div>
								<div class="fl_wrap">
									<label class='fl_label' for='Email'>Email Id</label>
									<input class='fl_input' type='email' name="Email" id='Email' />
								</div>
								<div class="fl_wrap">
									<label class='fl_label' for='Phone'>Mobile number</label>
									<input class='fl_input' type='text' name="Phone" id='Phone' />
								</div>
								<div class="fl_wrap">
									<label class='fl_label' for='City'>City</label>
									<input class='fl_input' type='text' name="City" id='City' />
								</div>
								<div class="fl_wrap">
									<label class='fl_label' for='Message'>Message</label>
									<input class='fl_input' type='text' name="Message" id='Message' />
								</div>
							</div>
							<div class="submit-btn">
								<input type="submit" onclick="BulkOrder()" id="bulk_order_button" value="Submit" class="explore-btn">
							</div>
						</form>
					</aside>
				</div>
			</div>
		</div>
		<?php
		    if($completed_projects->num_rows() > 0){
		?>
		        <div class="completed-projects-block flex-full">
        			<div class="container">
        				<div class="completed-projects-wrapper flex-full justify-content-center text-center">
        					<h2>Completed Projects</h2>
        					<ul class="completed-projects-listing flex-full">
        					    <?php
        					        foreach($completed_projects->result() as $project){
        					    ?>
        					            <li>
                							<div class="completed-projects flex-full">
                								<figure>
                									<img src="<?php echo base_url() ?>images/completed_project/<?php echo $project->image ?>" alt="completed products">
                								</figure>
                								<h4><?php echo $project->company_name ?>, <span><?php echo $project->location ?></span></h4>
                							</div>
                						</li>
        					    <?php
        					        }
        					    ?>
        					</ul>
        				</div>
        			</div>
        		</div>
		<?php
		    }
		?>
	</section>
</div>
<?php
$this->load->view('site/templates/footer');
?>
</div>
 </body>
</html>