<?php
$this->load->view('site/templates/header_inner');
?>
<div class="page-wrapper customer-payments-page flex-full">
	<!-- Inner page heading starts -->
	<section class="innerpage-heading flex-full justify-content-center">
		<div class="container">
			<div class="innerpage-heading-wrapper flex-full justify-content-center bg-img position-relative" style="background-image: url(images/innerpage-banner.jpg);">
				<div class="innerpage-description flex-full justify-content-center text-center">
					<h1>Privacy Policy</h1>
					<ul class="breadcrumbs-listing flex-full align-items-center justify-content-center">
						<li class="root-link"><a href="<?php echo base_url() ?>">Home</a></li>
						<li class="current-link"><a href="javascript:void(0)">Privacy Policy</a></li>
					</ul>						
				</div>
			</div>
		</div>
	</section>
	<!-- Inner page heading ends -->
	<div class="privacy-main flex-full">
		<div class="container">
			<div class="privacy-main-wrapper flex-full">
				<p>CITYFURNISH INDIA PRIVATE LIMITED owns and operates <a href="<?php echo base_url() ?>">http://www.cityfurnish.com</a> and understands how important your personal privacy is to you, and we want you to be aware of the kinds of information we collect, how we collect it, how we use it and with whom we share it. When you submit information to us through the Site, you acknowledge and agree to the terms and conditions of this Privacy Policy. We may revise this Privacy Policy from time to time by updating the information posted on the Site. Amendments to the Privacy Policy will be effective when posted. You should therefore periodically visit this page to review the current Privacy Policy, so that you are aware of any such revisions.</p>
				<p>This Privacy Policy covers the information for Cityfurnish India Private Limited("Cityfurnish" and/or "We") collects from the user(s) ("User(s)" and/or "You") of <a href="<?php echo base_url() ?>">www.cityfurnish.com</a> ("Website") This Privacy Policy should be read in conjunction and together with the User Agreement of the Website available on <a href="<?php echo base_url('pages/privacy-policy') ?>">http://www.cityfurnish.com/privacy_policy.html</a>. Personal Information of a User(s) is collected if the User(s) registers with the Website, accesses the Website or take any action on the Website. Here are our privacy principles:</p>
				<p>Providing information to us is your choice.</p>
				<p>You can choose to have the Personal Information provided by You deleted.</p>
				<p>You always have the ability to opt-out of receiving communications from us.</p>
				<h3>Information Collection and Use</h3>
				<p>The Personal Information which You may provide to us and/or which we may collect is or could be the following:</p>
				<p>Your registration details which may include the password provided by You. You may note that We adopt reasonable security measures to protect Your password from being exposed or disclosed to anyone including the Cityfurnish.</p>
				<p>Your shipping, billing, tax registration, and other information pertaining to Your sale or purchase transaction on the Website.</p>
				<p>Your transaction details with the other users of the Website. Your usage behavior of the Website.</p>
				<p>Details of the computer system or computer network which You use to visit the Website and undertake any activity on the Website.</p>
				<p>Our primary purposes in collecting information from You are to allow You to use the Website and various features and services offered by the Cityfurnish on or in relation to the Website; contact you for any services provided by Cityfurnish or its affiliates or its various service providers or Cityfurnish business partners and advertisers; to record Your information and details as permitted and required under applicable laws, statutory direction or judiciary orders; to serve various promotion materials and advertising materials to you; and such other uses as provided in the User Agreement and this Privacy Policy. We may also use the information for transactional emails or to provide You with information, direct marketing, online and offline advertising and other materials regarding products, services and other offers from time to time in connection with the Cityfurnish or its parent, subsidiary and affiliated companies ("Cityfurnish Entities") and its joint ventures.</p>
			</div>
		</div>
	</div>
</div>
<?php  ?>

			<!--footer-->
				<?php
					$this->load->view('site/templates/footer');
				?>
		</div>

		
		<!--libs include-->
		<script src="plugins/jquery.appear.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>

		<!--theme initializer-->
		<script src="js/themeCore.min.js"></script>
		<script src="js/theme.min.js"></script>
	</body>
</html>