<?php
$this->load->view('site/templates/header_inner');
?>
<!--main content-->

<div class="page-wrapper flex-full">
	<!-- Inner page heading starts -->
	<section class="innerpage-heading flex-full justify-content-center d-flex-xs">
		<div class="container">
			<div class="innerpage-heading-wrapper flex-full justify-content-center bg-img position-relative" style="background-image: url(images/innerpage-banner.jpg);">
				<div class="innerpage-description flex-full justify-content-center text-center">
					<h1>Rental Agreement</h1>
					<ul class="breadcrumbs-listing flex-full align-items-center justify-content-center">
						<li class="root-link"><a href="javascript:void(0)">Home</a></li>
						<li class="current-link"><a href="javascript:void(0)">Rental Agreement</a></li>
					</ul>						
				</div>
			</div>
		</div>
	</section>
	<!-- Inner page heading ends -->
	<div class="privacy-main rental-agreement-main flex-full">
		<div class="container">
			<div class="privacy-main-wrapper flex-full">
				<p>This Subscription Agreement ("Agreement") is made and deemed executed on the date _____________ ("Subscription Date")by the Company at _____________ ("City").</p>
				<h3>By and Between</h3>
				<p>Cityfurnish India Private Limited having its registered office at Flat No 31, Ekta Govt. Employees Co- operative Group, Sector 10A , Gurgaon , Haryana 122001 INDIA ("Company").</p>
				<h3>And</h3>
				<p>The individuals identified as _____________________________________________ ("Subscriber"). The Company and the Subscriber shall individually be referred to as 'Party' and collectively as 'Parties'.</p>
				<h3>Whereas</h3>
				<p>
				<p>The Company is in the business of offering Solutions with respect to home furniture, office furniture, soft furnishings and fitness equipments on subscription basis.</p>
				<p>The Subscriber has approached the Company to subscribe to the products and services offered by the Company, and the Company has agreed to provide the same to the Subscriber.</p>
				<p><strong>The Parties hereto wish to enter into this Agreement in order to record their mutual understanding.</strong></p>
				<h3>1. Definition</h3>
				<p><strong>In this Agreement, unless the context otherwise requires, the following capitalized words and expressions shall bear the meaning ascribed to them here-in-below:</strong></p>
				<ul>				
					<li>"Solution" means the product and services offered by company as part of subscription programme.</li>
					<li>“Service” means and includes delivery and installation of furniture, furnishing and fitness equipments.</li>
					<li>“Initial Subscription Period” means the period of ____ months from Subscription Date or such period as amended from time to time on the basis of customer request.</li>
					<li>"Security Deposit" means the amount deposited with the Company as per the solution provided by the company under the Subscription Programme.</li>
					<li>"Subscriber" means any person who subscribes for the solution.</li>
					<li>"Subscription" means the non-exclusive and non-transferable permission by the Company to experience the solution for personal consumption to the Subscriber.</li>
					<li>"Subscription Fee" means monthly fee charged by the Company towards Subscription of aSolution by the Subscriber.</li>
					<li>"Subscription Period" shall mean the period for which the Subscriber has taken the Subscription including the Initial Subscription Period.</li>
					<li>"Subscription Program Documents" shall mean and include this Agreement, invoice and any other document as may be provided by the Company.</li>
				</ul>
			</div>
		</div>
	</div>
</div>
<!--footer-->
<?php
$this->load->view('site/templates/footer');
?>
</div>
</body></html>