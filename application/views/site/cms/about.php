<?php
$this->load->view('site/templates/header_inner');
?>
<div class="page-wrapper flex-full">
	<div class="about-us-page flex-full align-items-start align-content-starts">
		<section class="about-banner bg-img flex-full" style="background-image: url(images/innerpage-banner.jpg);">
			<div class="container">
				<div class="about-banner-wrapper flex-full">
					<h1>The engine that drives us forward with conviction</h1>
					<ul class="breadcrumbs-listing flex-full align-items-center">
						<li class="root-link"><a href="javascript:void(0)">Home</a></li>
						<li class="current-link"><a href="javascript:void(0)">About Us</a></li>
					</ul>
				</div>
			</div>
		</section>
		<section class="about-team-section flex-full">
			<div class="container">
				<div class="about-team-wrapper flex-full justify-content-center text-center">
					<div class="about-us-info flex-full text-center">
						<p>Furlenco is a different furniture company.</p>
						<p>We enable the urban Indian to live better today in their homes, by giving them award-winning furniture and home decor on rent. All of our furniture is designed in-house by experts. </p>
						<p>Our furniture is inspired by the Indian urban user and how they live in their homes.</p>
						<p>Our designs are user-led and inspired by their evolving needs and growing lifestyle.</p>
					</div>
					<div class="about-team-block flex-full">
						<h2 class="border-center">Founding Team</h2>
						<span>The engine that drives us forward with conviction, vision and purpose</span>
						<ul class="team-listing flex-full justify-content-center">
							<li>
								<figure>
									<img src="images/product.jpg" alt="team">
								</figure>
								<span>Saurabh Gupta</span>
							</li>
							<li>
								<figure>
									<img src="images/product.jpg" alt="team">
								</figure>
								<span>Vinit Jain</span>
							</li>
							<li>
								<figure>
									<img src="images/product.jpg" alt="team">
								</figure>
								<span>Neerav Jain</span>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section class="cf-life-section flex-full justify-content-center text-center">
			<h2 class="border-center">Life at CityFurnish</h2>
			<div class="cf-life-image-wrapper flex-full">
				<div class="cf-life-wrapper flex-full">
					<div class="image-group flex-full">
						<figure>
							<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
						</figure>
						<figure>
							<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
						</figure>
					</div>
					<div class="image-group flex-full">
						<figure>
							<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
						</figure>
					</div>
				</div>
				<div class="cf-life-wrapper flex-full">
					<div class="image-group flex-full">
						<figure>
							<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
						</figure>
						<figure>
							<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
						</figure>
					</div>
					<div class="image-group flex-full">
						<figure>
							<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
						</figure>
					</div>
				</div>
				<div class="cf-life-wrapper flex-full">
					<div class="image-group flex-full">
						<figure>
							<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
						</figure>
						<figure>
							<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
						</figure>
					</div>
					<div class="image-group flex-full">
						<figure>
							<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
						</figure>
					</div>
				</div>				
			</div>
			<div class="about-us-info flex-full text-center">				
				<p>We are a team of goofballs - comprising of developers, curators of ideas and intelligent designs, and operations specialists. But, that doesn't mean we play with our work. When it's time to bring in the forces, we are at our very best - and we deliver.	We are a team of goofballs - comprising of developers, curators of ideas and intelligent designs, and operations specialists. But, that doesn't mean we play with our work. When it's time to bring in the forces, we are at our very best - and we deliver.</p>
			</div>
		</section>
		<section class="investors-section flex-full"> 
			<div class="container">
				<div class="investors-wrapper flex-full justify-content-center text-center">
					<h2 class="border-center">Investors & Advisors</h2>
					<span>Advising us are some of the best angel investors and venture capitalists.</span>
					<ul class="investors-logo-listing flex-full">
						<li>
							<img src="images/logo.png" alt="investors logo">
						</li>
						<li>
							<img src="images/logo.png" alt="investors logo">
						</li>
						<li>
							<img src="images/logo.png" alt="investors logo">
						</li>
						<li>
							<img src="images/logo.png" alt="investors logo">
						</li>
						<li>
							<img src="images/logo.png" alt="investors logo">
						</li>
						<li>
							<img src="images/logo.png" alt="investors logo">
						</li>
					</ul>
					<div class="about-us-info flex-full text-center">
						<p>Our backers include Kalaari Capital, Blume Ventures, Rajan Anandan, Sunil Kalra, Rob de Heus, Thijs Gitmans, Shailesh Rao, Bharathram Thothadri.</p>
					</div>
				</div>
			</div>
		</section>
		<section class="media-section flex-full">
			<div class="container">
				<div class="media-wrapper flex-full justify-content-center text-center">
					<h2 class="border-center">Media</h2>
					<ul class="media-logo-listing flex-full justify-content-center text-center">
						<li>
							<img src="images/logo.png" alt="media logo">
						</li>
						<li>
							<img src="images/logo.png" alt="media logo">
						</li>
						<li>
							<img src="images/logo.png" alt="media logo">
						</li>
						<li>
							<img src="images/logo.png" alt="media logo">
						</li>
						<li>
							<img src="images/logo.png" alt="media logo">
						</li>
						<li>
							<img src="images/logo.png" alt="media logo">
						</li>
						<li>
							<img src="images/logo.png" alt="media logo">
						</li>
						<li>
							<img src="images/logo.png" alt="media logo">
						</li>
					</ul>
				</div>
			</div>
		</section>
		<section class="accolades-section flex-full">
			<div class="container">
				<div class="accolades-wrapper flex-full justify-content-center text-center">
					<h2 class="border-center">Accolades</h2>
					<div class="about-us-info flex-full text-center">
						<p>Our backers include Kalaari Capital, Blume Ventures, Rajan Anandan, Sunil Kalra, Rob de Heus, Thijs Gitmans, Shailesh Rao, Bharathram Thothadri.  Our backers include Kalaari Capital, </p>
						<p>Our backers include Kalaari Capital, Blume Ventures, Rajan Anandan, Sunil Kalra, Rob de Heus, Thijs Gitmans, Shailesh Rao, Bharathram Thothadri.  Our backers include Kalaari Capital, </p>
						<p>Our backers include Kalaari Capital, Blume Ventures, Rajan Anandan, Sunil Kalra, Rob de Heus, Thijs Gitmans, Shailesh Rao, Bharathram Thothadri.  Our backers include Kalaari Capital, </p>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
<?php  ?>

			<!--footer-->
				<?php
					$this->load->view('site/templates/footer');
				?>
		</div>
	</body>
</html>