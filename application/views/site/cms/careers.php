<?php
$this->load->view('site/templates/header_inner');
?>
<div class="page-wrapper flex-full">	
	<div class="career-page flex-full">
		<section class="career-banner bg-img flex-full" style="background-image: url(images/innerpage-banner.jpg);">
			<div class="container">
				<div class="career-banner-wrapper flex-full">					
					<h1>Don’t Just Dream, <span>Do. With Us.</span></h1>
					<a href="javascript:void(0)" class="explore-btn">Find Your team</a>
					<ul class="breadcrumbs-listing flex-full align-items-center">
						<li class="root-link"><a href="<?php echo base_url(); ?>">Home</a></li>
						<li class="current-link"><a href="javascript:void(0)">We are Hiring</a></li>
					</ul>
				</div>
			</div>
		</section>
		<section class="our-believe-section flex-full">
			<div class="container">
				<div class="our-believe-wrapper flex-full justify-content-center text-center">
					<h3 class="subtitle">We Believe In</h3>
					<h2 class="border-center">Action. Community. Quality.</h2>
					<div class="believe-info flex-full">
						<p>At Cityfurnish, we're building an on-demand furniture and appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high.</p>
						<p>At Cityfurnish, we're building an on-demand furniture and appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high.</p>
					</div>
				</div>
			</div>
		</section>
		<section class="cf-life-section flex-full">
			<div class="container">
				<div class="cf-life-section-wrapper flex-full justify-content-center text-center">
					<h3 class="subtitle">Life at CityFurnish</h3>
					<h2 class="border-center">Fun. Family. Lerning.</h2>
					<div class="believe-info flex-full">
						<p>If an exciting challenge is what gets you up in the morning and keeps you up at night,  we should talk. Our offices are filled with doers like you. Want coworkers who are as committed, curious, and enthusiastic as you? Join the Fiverr team.If an </p>
					</div>
					<div class="cf-life-image-wrapper flex-full">
						<div class="cf-life-col cf-life-col1 flex-full">
							<div class="cf-life-col-wrapper flex-full">
								<figure>
									<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
								</figure>
							</div>
							<div class="cf-life-col-wrapper flex-full">
								<figure>
									<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
								</figure>
							</div>
							<div class="cf-life-col-wrapper flex-full">
								<figure>
									<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
								</figure>
							</div>
						</div>
						<div class="cf-life-col cf-life-col2 flex-full">
							<div class="cf-life-col-inner flex-full">
								<div class="cf-life-col-inner-wrapper flex-full">
									<div class="cf-life-col-wrapper flex-full">
										<figure>
											<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
										</figure>
									</div>
									<div class="cf-life-col-wrapper flex-full">
										<figure>
											<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
										</figure>
									</div>
								</div>
								<div class="cf-life-col-inner-wrapper flex-full">
									<div class="cf-life-col-wrapper flex-full">
										<figure>
											<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
										</figure>	
									</div>
								</div>
							</div>
							<div class="cf-life-col-inner flex-full">
								<div class="cf-life-col-wrapper flex-full">
									<figure>
										<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
									</figure>
								</div>
							</div>							
						</div>
						<div class="cf-life-col cf-life-col3 flex-full">
							<div class="cf-life-col-wrapper flex-full">
								<figure>
									<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
								</figure>
							</div>
							<div class="cf-life-col-wrapper flex-full">
								<figure>
									<img src="images/innerpage-banner.jpg" alt="life at cityfurnish">
								</figure>
							</div>							
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="our-location-section flex-full">
			<div class="container">
				<div class="our-location-wrapper flex-full justify-content-center text-center">
					<h2 class="border-center">Our Locations</h2>
					<ul class="location-listing flex-full justify-content-center text-center">
						<li>
							<span class="location-box">Delhi</span>
						</li>
						<li>
							<span class="location-box">Bangalore</span>
						</li>
						<li>
							<span class="location-box">Pune</span>
						</li>
						<li>
							<span class="location-box">Gurgaon</span>
						</li>
						<li>
							<span class="location-box">Mumbai</span>
						</li>
						<li>
							<span class="location-box">Noida</span>
						</li>
					</ul>
				</div>
			</div>
		</section>		
		<section class="vacancies-section referral-code-faq account-accordion flex-full">
			<div class="container">
				<div class="vacancies-wrapper flex-full justify-content-center text-center">
					<h2 class="border-center">Vacancies</h2>
					<div class="accordion">
						<div class="accordion-tab">
							<h3 class="accordion-title">Marketing</h3>
							<div class="accordion-content">
								<div class="accordion-body">
									<ul class="openings-listing flex-full">
										<li>
											<h4>1. Sr. Executive</h4>
										</li>
										<li>
											<h4>2. Jr. Executive</h4>
											<div class="job-description flex-full">
												<p>At Cityfurnish, we're building an on-demand furniture and appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high. Cityfurnish, appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high. an on-demand furniture and appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high. dia with quality products and an obsession with keeping customer service levels ridiculously high. an on-demand furniture and appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high.</p>
											</div>
										</li>
										<li>
											<h4>3. Operation Head</h4>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="accordion-tab">
							<h3 class="accordion-title">Technology</h3>
							<div class="accordion-content">
								<div class="accordion-body">
									<ul class="openings-listing flex-full">
										<li>
											<h4>1. Sr. Executive</h4>
										</li>
										<li>
											<h4>2. Jr. Executive</h4>
											<div class="job-description flex-full">
												<p>At Cityfurnish, we're building an on-demand furniture and appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high. Cityfurnish, appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high. an on-demand furniture and appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high. dia with quality products and an obsession with keeping customer service levels ridiculously high. an on-demand furniture and appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high.</p>
											</div>
										</li>
										<li>
											<h4>3. Operation Head</h4>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="accordion-tab current">
							<h3 class="accordion-title">Operations</h3>
							<div class="accordion-content">
								<div class="accordion-body">
									<ul class="openings-listing flex-full">
										<li>
											<h4>1. Sr. Executive</h4>
										</li>
										<li>
											<h4>2. Jr. Executive</h4>
											<div class="job-description flex-full">
												<p>At Cityfurnish, we're building an on-demand furniture and appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high. Cityfurnish, appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high. an on-demand furniture and appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high. dia with quality products and an obsession with keeping customer service levels ridiculously high. an on-demand furniture and appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high.</p>
											</div>
										</li>
										<li>
											<h4>3. Operation Head</h4>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="accordion-tab">
							<h3 class="accordion-title">Costumer Relationship</h3>
							<div class="accordion-content">
								<div class="accordion-body">
									<ul class="openings-listing flex-full">
										<li>
											<h4>1. Sr. Executive</h4>
										</li>
										<li>
											<h4>2. Jr. Executive</h4>
											<div class="job-description flex-full">
												<p>At Cityfurnish, we're building an on-demand furniture and appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high. Cityfurnish, appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high. an on-demand furniture and appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high. dia with quality products and an obsession with keeping customer service levels ridiculously high. an on-demand furniture and appliances startup that is delighting many customers across India with quality products and an obsession with keeping customer service levels ridiculously high.</p>
											</div>
										</li>
										<li>
											<h4>3. Operation Head</h4>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<span class="drop-resume w-100">Please drop your resume at <a href="mailto:hr@cityfurnish.com">hr@cityfurnish.com</a></span>
				</div>
			</div>
		</section>
	</div>
</div>


	<?php
$this->load->view('site/templates/footer');
?>
</body>
</html>


