<?php
$this->load->view('site/templates/header_inner');
?>
<div class="page-wrapper flex-full">
		<div class="thankyou-section page-not-found-section flex-full">
			<div class="container">
				<div class="thankyou-wrapper flex-full justify-content-center text-center">
					<figure>
						<img src="images/cheers-vector.png" alt="cheers-vector">
					</figure>
					<h1 class="w-100 color-red">Page not found</h1>					
					<span class="message w-100">Sorry, but the page your looking for is not found <br/> Please make sure you have typed the correct URL.</span>
				</div>
			</div>
		</div>
	</div>
<?php  ?>

			<!--footer-->
				<?php
					$this->load->view('site/templates/footer');
				?>
		</div>

		
		<!--libs include-->
		<script src="plugins/jquery.appear.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>

		<!--theme initializer-->
		<script src="js/themeCore.min.js"></script>
		<script src="js/theme.min.js"></script>
	</body>
</html>