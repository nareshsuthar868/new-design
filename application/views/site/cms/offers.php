<?php
$this->load->view('site/templates/header_inner');
?>

<div class="page-wrapper flex-full">
		<div class="gradient-banner flex-full bg-img position-relative" style="background-image: url(images/innerpage-banner.jpg);">
			<div class="container">
				<div class="gradient-banner-wrapper flex-full">
					<!-- Breadcurmbs starts -->
					<div class="breadcrumbs flex-full">
						<ul class="breadcrumbs-listing flex-full align-items-center">
							<?php echo trim($breadCumps); ?>
						</ul>
					</div>
					<!-- Breadcurmbs ends -->
					<h1>Offers<span>just for you!</span></h1>
				</div>
			</div>
		</div>
		<div class="offers-section flex-full">
			<div class="container">
				<ul class="offers-listing flex-full justify-content-center">
				    <?php foreach($offer_listing->result() as $key => $value){
				        $dynamicDescription = unserialize($value->description);
				    ?>
					<li>
						<div class="offer-box flex-full">
							<h2><?php echo $value->price_text; ?> <span> <?php echo $value->price_below_text; ?></span></h2>
							<label class="w-100 text-center">Coupon code:</label>
							<div class="coupon-code" id="couponCode"><?php echo $value->coupon_code; ?> <a href="javascript:void(0)" onclick="copyToClipboard('<?php echo $value->coupon_code ?>')"><i class="icn icn-copy"></i></a></div>
							<div class="info-listing flex-full">
							    <?php foreach($dynamicDescription as $val){
							     echo '<span>- '.$val.'</span>';
							    }?>
								<!--<span>- 1 Month Rent Off</span>-->
								<!--<span>- First month's rent OFF on furniture and home appliances</span>-->
								<!--<span>- Applicable on min 3 months tenure</span>-->
							</div>
						</div>
					</li>
				
					<?php } ?>
				</ul>
				<div class="discount-info flex-full text-center justify-content-center">
					<div class="discount-info-wrapper flex-full text-center justify-content-center">
						<h3 class="border-center">&#x20B9;<?php echo $voucher_data->row()->reward; ?> <?php echo $voucher_data->row()->voucher_title; ?> <span><?php echo $voucher_data->row()->voucher_label; ?>*</span></h3>
						<p>* This will be irrespective of any other coupon code used in order.</p>
						<p><?php echo $voucher_data->row()->steps_desc; ?></p>
						<!--<p>* Voucher details will be mailed to you after placing the order</p>-->
					</div>
				</div>
				<div class="tnc-block flex-full">
					<h4>Terms and Conditions</h4>
					<ul class="tnc-listing flex-full">
						<li>- Only one offer can be availed per order.</li>
						<li>- Offers cannot be used more than once by a customer or on the same delivery address.</li>
						<li>- These offers are not applicable in case of early closure of  the order.</li>
						<li>- Offers are applicable on furniture and appliances only.</li>
						<li>- Offers are not applicable on fitness and add-on products</li>
						<li>- These offers cannot be clubbed with referral program benefits.</li>
						<li>- It takes us 15 days to process cash back (wherever applicable).</li>
						<li>- You can avail these offers by simply applying the respective coupon code while checkout.</li>
						<li>- For more detail about availing these offers, get in touch with our customer care team.</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
<!--footer-->
<?php
$this->load->view('site/templates/footer');
?>
</div>
<script>
       function copyToClipboard(text) {
            var textArea = document.createElement( "textarea" );
            textArea.value = text;
            document.body.appendChild( textArea );
            textArea.select();
            try {
              var successful = document.execCommand( 'copy' );
              var msg = successful ? 'successful' : 'unsuccessful';
              console.log('Copying text command was ' + msg);
              alert("Copied");
            } catch (err) {
              console.log('Oops, unable to copy');
            }
            document.body.removeChild( textArea );
          }
        $( '#btnCopyToClipboard' ).click( function()
        {
            var clipboardText = "";
            clipboardText = $( '#txtKeyw' ).val();
            copyTextToClipboard( clipboardText );
        });
	$('.offerslider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		infinite: true,
	});
	
</script>
</body>
</html>
