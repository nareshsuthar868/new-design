<?php
$this->load->view('site/templates/header_inner');
?>
			<div class="page-wrapper flex-full">
			<div class="my-account-section flex-full">
				<div class="container">
					<div class="my-account-wrapper flex-full">
					<?php 
					$this->load->view('site/user/settings_sidebar');
					?>
						<div class="account-content-area flex-full align-items-start align-content-start">
							<div class="referral-code-wrapper flex-full align-items-start align-content-start">
								<div class="referral-code-info flex-full">
									<h2 class="big-text">Have your friends pay for your furniture</h2>
									<p>Share your referral code with friends, both you and your friend get</p>
									<span class="get-coins"><i class="icn icn-coins-gold"></i> 1000 CF Coins</span>
									<div class="referral-code flex-full">
										<label>Your referral code:</label>
										<span class="coupon-code">CF139142764 <a href="javascript:void(0)"><i
													class="icn icn-copy"></i></a></span>
									</div>
									<div class="referral-code-share flex-full">
										<label>Share Code Via :</label>
										<ul class="social-icons-listing flex-full">
											<li><a href="javascript:void(0)" class="facebook"><i
														class="icn icn-facebook-white"></i></a></li>
											<li><a href="javascript:void(0)" class="linkedin"><i
														class="icn icn-linkedin-white"></i></a></li>
											<li><a href="javascript:void(0)" class="google"><i
														class="icn icn-google-white"></i></a></li>
										</ul>
									</div>
								</div>
								<div class="referral-code-vector flex-full">
									<img src="images/referral-vector.png" alt="referral code" />
								</div>
							</div>
							<div class="referral-code-process-block flex-full">
								<h2 class="border-left">How It Works</h2>
								<p>Become our brand ambassador and earn CF Coins everytime you refer someone</p>
								<ul class="referral-code-process-listing flex-full">
									<li>
										<div class="referral-code-process flex-full">
											<figure>
												<img src="images/how-it-works-vector.png" alt="how it works">
											</figure>
											<span>01</span>
											<p>Share your Referral Code with Family and Friends</p>
										</div>
									</li>
									<li>
										<div class="referral-code-process flex-full">
											<figure>
												<img src="images/how-it-works-vector.png" alt="how it works">
											</figure>
											<span>02</span>
											<p>Your Friend uses your Referral Code while placing the Order</p>
										</div>
									</li>
									<li>
										<div class="referral-code-process flex-full">
											<figure>
												<img src="images/how-it-works-vector.png" alt="how it works">
											</figure>
											<span>03</span>
											<p>You and your Friend both get <strong>1000 CF Coins</strong></p>
										</div>
									</li>
									<li>
										<div class="referral-code-process flex-full">
											<figure>
												<img src="images/how-it-works-vector.png" alt="how it works">
											</figure>
											<span>04</span>
											<p>Use CF Coins against Future Payments or your next Orders</p>
										</div>
									</li>
								</ul>
							</div>
							<div class="referral-code-faq account-accordion flex-full">
								<h2 class="border-left">All You Want to Know About It..</h2>
								<div class="accordion">
									<div class="accordion-tab current">
										<h3 class="accordion-title">Who can Refer?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting
													industry. Lorem Ipsum has been the industry's standard Lorem Ipsum
													is simply dummy text of the printing and typesetting industry. Lorem
													Ipsum is simply dummy text of the printing and typesetting industry.
													Lorem Ipsum has been the industry's standard Lorem Ipsum is simply
													dummy text of the printing and typesetting industry. </p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">How can I Refer?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting
													industry. Lorem Ipsum has been the industry's standard Lorem Ipsum
													is simply dummy text of the printing and typesetting industry. Lorem
													Ipsum is simply dummy text of the printing and typesetting industry.
													Lorem Ipsum has been the industry's standard Lorem Ipsum is simply
													dummy text of the printing and typesetting industry. </p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">How can I use referral Code?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting
													industry. Lorem Ipsum has been the industry's standard Lorem Ipsum
													is simply dummy text of the printing and typesetting industry. Lorem
													Ipsum is simply dummy text of the printing and typesetting industry.
													Lorem Ipsum has been the industry's standard Lorem Ipsum is simply
													dummy text of the printing and typesetting industry. </p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">Is there a limit on benifits?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting
													industry. Lorem Ipsum has been the industry's standard Lorem Ipsum
													is simply dummy text of the printing and typesetting industry. Lorem
													Ipsum is simply dummy text of the printing and typesetting industry.
													Lorem Ipsum has been the industry's standard Lorem Ipsum is simply
													dummy text of the printing and typesetting industry. </p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">Can I use my own Referal Code?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting
													industry. Lorem Ipsum has been the industry's standard Lorem Ipsum
													is simply dummy text of the printing and typesetting industry. Lorem
													Ipsum is simply dummy text of the printing and typesetting industry.
													Lorem Ipsum has been the industry's standard Lorem Ipsum is simply
													dummy text of the printing and typesetting industry. </p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">How can I use referral Code?</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting
													industry. Lorem Ipsum has been the industry's standard Lorem Ipsum
													is simply dummy text of the printing and typesetting industry. Lorem
													Ipsum is simply dummy text of the printing and typesetting industry.
													Lorem Ipsum has been the industry's standard Lorem Ipsum is simply
													dummy text of the printing and typesetting industry. </p>
											</div>
										</div>
									</div>
									<div class="accordion-tab">
										<h3 class="accordion-title">Other Terms and Conditions</h3>
										<div class="accordion-content">
											<div class="accordion-body">
												<p>Lorem Ipsum is simply dummy text of the printing and typesetting
													industry. Lorem Ipsum has been the industry's standard Lorem Ipsum
													is simply dummy text of the printing and typesetting industry. Lorem
													Ipsum is simply dummy text of the printing and typesetting industry.
													Lorem Ipsum has been the industry's standard Lorem Ipsum is simply
													dummy text of the printing and typesetting industry. </p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
			<!--footer-->
				<?php
					$this->load->view('site/templates/footer');
				?>
		</div>


		<!--libs include-->
		<script src="plugins/jquery.appear.min.js"></script>
		<script src="plugins/jquery.easytabs.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>

		<!--theme initializer-->
		<script src="js/themeCore.min.js"></script>
		<script src="js/theme.min.js"></script>
	</body>
</html>