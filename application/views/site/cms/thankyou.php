<?php
$this->load->view('site/templates/header_inner');
?>
<!--main content-->

<div class="page_section_offset mobileheight"> 
  <!-- New  html -->
  <section class="thankyoupagerow">
    <div class="container">
      <div class="row">
       	<div class="col-xs-12">
        	<img src="<?php echo base_url(); ?>images/thankyou-thumb.svg" />
            <h1>Thank you for your order</h1>
            <p>You will receive an email confirmation shortly at your registered email ID.</p>
        </div>
      </div>
    </div>
  </section>
  <!-- New  html --> 
  
</div>
<!--footer-->
<?php
$this->load->view('site/templates/footer');
?>
<!--footer-->
</div>

		<script>
			$(window).on("load", function() {
				var win_width = $(window).width();
				//alert(win_width);
				if(win_width < 767){
					$('.bg_white').addClass('fullheight');	
				}
			});
		</script>

</body>
</html>