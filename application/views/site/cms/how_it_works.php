<?php
$this->load->view('site/templates/header_inner');
?>
			<!--main content-->
			<div class="page_section_offset">
				<section class="innerbanner">
					<div class="container">
						<div class="row" >
							<div class="col-lg-12"> 
						 	 	<h1>How it works</h1>
                           	 	<ul class="breadcrumb">
                                	<li><a href="#">Home</a></li>
                                	<li class="active">How It works</li>
                            	</ul>
                            	<h2>Rental Process</h2>
							 	<p>Following is the process of placing order on Cityfurnish. Alternatively you can also call our customer care to place an order.</p>
                        	</div>
                    	</div>
                	</div>
                </section>
                <div class="container">
					<div class="row">
						<section class="col-lg-12 col-md-12 col-sm-12 howitsworktab">
						<!-- how it works new tab -->
                        	<ul class="nav nav-tabs">
                            		<li class="active">
		                            <a href="#place" data-toggle="tab" aria-expanded="true">
		                            <img src="images/place-order-icn.svg" alt="place" />
		                            </a><i class="material-icons">keyboard_backspace</i></li>
		                            <li><a href="#Furniture" data-toggle="tab" aria-expanded="false"><img src="<?php echo base_url(); ?>images/kyc-process-icn.svg" alt="place" /></a><i class="material-icons">keyboard_backspace</i></li>
		                            <li class=""><a href="#Interior" data-toggle="tab" aria-expanded="false"><img src="<?php echo base_url(); ?>images/delivery-and-installation-icn.svg" alt="place" /></a><i class="material-icons">keyboard_backspace</i></li>
		                            <li class=""><a href="#Lighting" data-toggle="tab" aria-expanded="false"><img src="<?php echo base_url(); ?>images/pickup-and-refund.svg" alt="place" /></a>
		                        	</li>

		                        	<!--   <li class=""><a href="#Extensions" data-toggle="tab" aria-expanded="false"><img src="<?php echo base_url(); ?>images/refresh.svg" alt="place" / style="width: 121px; height: 58px"></a>
		                        	  	<i class="material-icons">keyboard_backspace</i>
		                        	</li>
		                        	 <li class=""><a href="#RenttoBuy" data-toggle="tab" aria-expanded="false"><img src="<?php echo base_url(); ?>images/best_buy.svg" alt="place" / style="width: 135px; height: 70px"></a>
		                        	 	<i class="material-icons">keyboard_backspace</i>
		                        	</li>
		                        	 <li class=""><a href="#PickupandRefund" data-toggle="tab" aria-expanded="false"><img src="<?php echo base_url(); ?>images/Circle_creditcard.png" alt="place" / style="width: 90px; height: 60px"></a>
		                        	</li> -->
							</ul>
							
							<div class="tab-content">
                            	<div class="tab-pane fade active in" id="place">
                             		<h3>Placing an Order</h3>	
                              		<ul>
                                		<li>Explore our packages and products</li>
                                		<li>Choose rental tenure</li>
                                		<li>Add product to shopping cart and proceed to checkout</li>
                                		<li>Check your payable rental & security deposit and apply discount coupon (If you have one)</li>
                                		<li>Proceed to payment page. We support all payment methods - CC, DC & Netbanking</li>
							  		</ul>
                            	</div>
                            	<div class="tab-pane fade" id="Furniture">
                            		<h3>KYC Process</h3>
                            	     <p class="m_bottom_13">As part of KYC, you are required to mail us following documents on hello@cityfurnish.com before the delivery. Alternatively you can also whatsapp these documents to us on 9899837999</p>
									<ul class="m_bottom_13">
										<li>ID Proof,Company ID Card/Student ID Card (If applicable)</li>
										<li>Permanent Address Proof,Passport/Aadhaar/Voter ID Card/Driving License</li>
										<li>Delivery Address Proof,In case delivery address is different from permanent address, please provide us rental agreement/Electricity Bill/Internet Bill and contact details of your landlord.</li>
										<li>Cheques/NACH for monthly rent,Post dated cheques for monthly rental payment or NACH </li>
									</ul>
									<p class="m_bottom_13">Please note that you provide these documents after placing the order, failing to provide these documents might result in cancellation of order.</p>
                            	</div>
                            	<div class="tab-pane fade" id="Interior">
                            		 <h3>Delivery and Installation</h3>
                            		<p class="m_bottom_13">Delivery and installation is absolutely free for all our products and packages. You are requested to do following at the time of delivery,</p>
									<ul class="m_bottom_13">
										<li>Check all products carefully and report any damage/scratch in delivery challan. Though we do thorough quality check before shipping the product, there might be minor damage during the delivery</li>
										<li>Sign rental agreement brought by our delivery team and keep one copy of the same with you and hand over second copy to our team</li>
										<li>Hand over post dated cheques to our team for remaining rental tenure or fill up the NACH form brought by our delivery team</li>
										<li>Fill in feedback form brought by our team, it will help us improve our services</li>
									</ul>
										<p class="m_bottom_13">You are expected to be present at the agreed date and time for taking delivery with cheques. Failing to do so might incur extra shipping cost or cheque/NACH collection charges.</p>									
                            	</div>
                            	<div class="tab-pane fade" id="Lighting">
                            		 <h3>Payment</h3>
									<ul class="m_bottom_13">
										<li>Hand over post-dated rental cheques to our delivery team. All cheques to be drawn in favor of "MAGNETO HOME PRIVATE LIMITED".</li>
										<li>Your post-dated rental cheques are en-cashed on agreed dates.</li>
										<li>In case of check bounce, Rs 400 will be charged as check bounce charges and Rs 400 as collection charges for new checks.</li>
									</ul>
                            	</div>
                            	<!-- <div class="tab-pane fade" id="Extensions">
                            		 <h3>Extensions</h3>
									<ul class="m_bottom_13">
										<li>You can easily extend the tenure of rental by calling our customer care team. Our team will collect cheques for extended period from your home.</li>		
									</ul>
                            	</div>
                            	<div class="tab-pane fade" id="RenttoBuy">
                            		 <h3>Rent to Buy</h3>
									<ul class="m_bottom_13">
										<li>Contact our customer care if you want to own the product you are using and we will provide you the differential amount to be paid to buy the product</li>		
									</ul>
                            	</div>
                            	<div class="tab-pane fade" id="PickupandRefund">
                            		 <h3>Pickup and Refund</h3>
                            		  <p class="m_bottom_13">Once your rental tenure is over, our team will call to schedule the pickup date and time. Our team will take pictures of any scratches and damages observed at the time of pickup and will bring the same into your notice</p>
									<ul class="m_bottom_13">
										<li><i class="fa fa-check fs_ex_small"></i>Minor scratches on furniture will be ignored because they are considered as normal wear and tear</li>
										<li><i class="fa fa-check fs_ex_small"></i>Repairable damages will be charged on actual</li>
										<li><i class="fa fa-check fs_ex_small"></i>Any damage beyond repair will be charged on actual market price.</li>
										<li><i class="fa fa-check fs_ex_small"></i>After deducting the required charges (if any), your security deposit will be refunded within 7 working days after pickup. The refund will be done in the same account from which you did the payment. Once refund is processed, it usually takes 7-10 working days to get reflected in you bank account</li>

									</ul>
                            	</div> -->
                            </div>							
						</section>
					</div>
				</div>
			</div>
			<!--footer-->
				<?php
					$this->load->view('site/templates/footer');
				?>
		</div>

		<!--back to top-->
		
		<!--libs include-->
		<script src="plugins/jquery.appear.min.js"></script>
		<script src="plugins/jquery.easytabs.min.js"></script>
		<script src="plugins/afterresize.min.js"></script>

		<!--theme initializer-->
		<script src="js/themeCore.min.js"></script>
		<script src="js/theme.min.js"></script>
	</body>
</html>