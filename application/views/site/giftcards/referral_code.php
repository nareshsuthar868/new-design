<?php $this->load->view('site/templates/header_inner.php');
$social_media_message = $referral_setting->row()->social_message;
$new_text = str_replace('CODE_HERE', $referral_code->row()->referral_code, $social_media_message);
$next_1 = str_replace('AMOUNT_HERE', round($referral_setting->row()->amount), $new_text);
?>
<script>
function Share_with_facebook(){
  FB.ui({
  method: 'share',
  mobile_iframe: true,
  href: 'https://cityfurnish.com/',
  quote:"<?php  echo $next_1; ?> #WhyBuy",
  }, function(response){});
}
</script>
<script type="text/javascript" src="//platform.linkedin.com/in.js">
  api_key: 81tyo00okgh7jg
  authorize: true
//   onLoad: onLinkedInLoad1

</script>

<script>
    jQuery(document).ready(function(){
    jQuery("#share_button").click(function(){
        IN.UI.Authorize().place(); 
        shareContent();
        // IN.Event.on(IN, "auth", shareContent);
    });
});
</script>

<script type="text/javascript">
  // Setup an event listener to make an API call once auth is complete
  var share_post = false;
    function onLinkedInLoad1() {
        // alert("ffff");
        if(share_post){
            // alert(share_post);
            IN.Event.on(IN, "auth", shareContent);
        }
    }

  // Handle the successful return from the API call
  function onSuccess(data) {
      $('#overlay').hide();
       sweetAlert({
                title: "Shared Success",
                text: 'Code shared successfully',                        
                type: "success",
                showCancelButton: false,
                closeOnConfirm: true,
                animation: "slide-from-top",
                showConfirmButton: true,      
        }); 
  }

  // Handle an error response from the API call
  function onError(error) {
      $('#overlay').hide();
        sweetAlert({
                title: "Alredy Shared",
                text: 'You already share this code on linkedin', 
                type: "success",
                showCancelButton: false,
                closeOnConfirm: true,
                animation: "slide-from-top",
                showConfirmButton: true,      
        });
  }

  // Use the API call wrapper to share content on LinkedIn
  function shareContent() {
    // Build the JSON payload containing the content to be shared
    var payload = { 
      "comment": "<?php echo $next_1; ?>#WhyBuy  cityfurnish.com ", 
      "visibility": { 
        "code": "anyone"
      } 
    };

    IN.API.Raw("/people/~/shares?format=json")
      .method("POST")
      .body(JSON.stringify(payload))
      .result(onSuccess)
      .error(onError);
  }
  
  function send_post(){
      $('#overlay').show();
      share_post = true;
      shareContent();
  }

</script>

	<div class="page-wrapper flex-full">
		<div class="my-account-section flex-full">
			<div class="container">
				<div class="my-account-wrapper flex-full">
				    <?php $this->load->view('site/user/settings_sidebar'); ?>
					<div class="account-content-area flex-full align-items-start align-content-start">
						<div class="referral-code-wrapper flex-full align-items-start align-content-start">
							<div class="referral-code-info flex-full">
								<h2 class="big-text">Have your friends pay for your furniture</h2>
								<p>Share your referral code with friends, both you and your friend get</p>
								<span class="get-coins"><i class="icn icn-coins-gold"></i> <?php echo round($referral_setting->row()->amount); ?> CF Coins</span>
								<div class="referral-code flex-full">
									<label>Your referral code:</label>
									<span class="coupon-code"><?php echo $referral_code->row()->referral_code; ?><a href="javascript:void(0)"  onclick="copyToClipboard('<?php echo $referral_code->row()->referral_code; ?>')"><i class="icn icn-copy"></i></a></span>
								</div>
								<div class="referral-code-share flex-full">
									<label>Share Code Via :</label>
									<ul class="social-icons-listing flex-full">
										<li><a href="javascript:Share_with_facebook()" target="_blank" class="facebook"><i class="icn icn-facebook-white"></i></a></li>
										<li><a href="javascript:void(0)" class="linkedin"><i class="icn icn-linkedin-white"></i></a></li>
										<li><a href="javascript:void(0)" class="google"><i class="icn icn-google-white"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="referral-code-vector flex-full">
								<img src="<?php echo CDN_URL; ?>images/referral-vector.png" alt="referral code" />
							</div>
						</div>
						<div class="referral-code-process-block flex-full">
							<h2 class="border-left">How It Works</h2>
							<p>Become our brand ambassador and earn CF Coins everytime you refer someone</p>
							<ul class="referral-code-process-listing flex-full">
								<li>
									<div class="referral-code-process flex-full">
										<figure>
											<img src="<?php echo CDN_URL; ?>images/how-it-works-vector.png" alt="how it works">
										</figure>
										<span>01</span>
										<p>Share your Referral Code with Family and Friends</p>
									</div>
								</li>
								<li>
									<div class="referral-code-process flex-full">
										<figure>
											<img src="<?php echo CDN_URL; ?>images/how-it-works-vector.png" alt="how it works">
										</figure>
										<span>02</span>
										<p>Your Friend uses your Referral Code while placing the Order</p>
									</div>
								</li>
								<li>
									<div class="referral-code-process flex-full">
										<figure>
											<img src="<?php echo CDN_URL; ?>images/how-it-works-vector.png" alt="how it works">
										</figure>
										<span>03</span>
										<p>You and your Friend both get <strong>1000 CF Coins</strong></p>
									</div>
								</li>
								<li>
									<div class="referral-code-process flex-full">
										<figure>
											<img src="<?php echo CDN_URL; ?>images/how-it-works-vector.png" alt="how it works">
										</figure>
										<span>04</span>
										<p>Use CF Coins against Future Payments or your next Orders</p>
									</div>
								</li>
							</ul>
						</div>
						<div class="referral-code-faq account-accordion flex-full">
							<h2 class="border-left">All You Want to Know About It..</h2>
							<div class="accordion">
								<div class="accordion-tab current">
									<h3 class="accordion-title">Who can Refer?</h3>
									<div class="accordion-content">
										<div class="accordion-body">
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard Lorem Ipsum is simply dummy text of the printing  and typesetting industry. </p>
										</div>
									</div>
								</div>
								<div class="accordion-tab">
									<h3 class="accordion-title">How can I Refer?</h3>
									<div class="accordion-content">
										<div class="accordion-body">
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard Lorem Ipsum is simply dummy text of the printing  and typesetting industry. </p>
										</div>
									</div>
								</div>
								<div class="accordion-tab">
									<h3 class="accordion-title">How can I use referral Code?</h3>
									<div class="accordion-content">
										<div class="accordion-body">
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard Lorem Ipsum is simply dummy text of the printing  and typesetting industry. </p>
										</div>
									</div>
								</div>
								<div class="accordion-tab">
									<h3 class="accordion-title">Is there a limit on benifits?</h3>
									<div class="accordion-content">
										<div class="accordion-body">
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard Lorem Ipsum is simply dummy text of the printing  and typesetting industry. </p>
										</div>
									</div>
								</div>
								<div class="accordion-tab">
									<h3 class="accordion-title">Can I use my own Referal Code?</h3>
									<div class="accordion-content">
										<div class="accordion-body">
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard Lorem Ipsum is simply dummy text of the printing  and typesetting industry. </p>
										</div>
									</div>
								</div>
								<div class="accordion-tab">
									<h3 class="accordion-title">How can I use referral Code?</h3>
									<div class="accordion-content">
										<div class="accordion-body">
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard Lorem Ipsum is simply dummy text of the printing  and typesetting industry. </p>
										</div>
									</div>
								</div>
								<div class="accordion-tab">
									<h3 class="accordion-title">Other Terms and Conditions</h3>
									<div class="accordion-content">
										<div class="accordion-body">
											<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard Lorem Ipsum is simply dummy text of the printing  and typesetting industry. </p>
										</div>
									</div>
								</div>								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
      function copyToClipboard(text) {
        var textArea = document.createElement( "textarea" );
        textArea.value = text;
        document.body.appendChild( textArea );
        textArea.select();
        try {
          var successful = document.execCommand( 'copy' );
          var msg = successful ? 'successful' : 'unsuccessful';
          console.log('Copying text command was ' + msg);
          alert("Copied");
        } catch (err) {
          console.log('Oops, unable to copy');
        }
        document.body.removeChild( textArea );
      }
    $( '#btnCopyToClipboard' ).click( function()
    {
        var clipboardText = "";
        clipboardText = $( '#txtKeyw' ).val();
        copyTextToClipboard( clipboardText );
    });
    </script>
<?php $this->load->view('site/templates/footer');?>
</body>
</html>