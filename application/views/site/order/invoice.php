<!DOCTYPE html>
<html amp lang="en" style="height: 100% !important;">

<head>
    <title>Invoice</title>
    <meta charset="utf-8">
    <link rel="canonical" href="product-listing.html">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url();?>images/cf_Favicon.png">
    <!-- CSS Files -->
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>css/ion.rangeSlider.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>css/jquery.mCustomScrollbar.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>css/jquery.FlowupLabels.css">
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>css/magnific-popup.css">
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>css/style-custom.css">
    <link rel="stylesheet" type="text/css" href="<?php echo  base_url();?>css/responsive.css">
    
    <script async src="<?php echo  base_url();?>js/amp_v0.js"></script>
    <script src="<?php echo  base_url();?>js/jquery-2.1.1.min.js" ></script>
</head>

<body style="height: 100% !important;" class="address">
    <div class="site">
        <!-- Header starts -->
        <header class="flex-full">
            <!-- logo part starts -->
            <div class="logo-part flex-full" style="box-shadow: 0 8px 6px -7px black;">
                <div class="container" style="padding: 5px 15px;">
                    <div class="logo-part-wrapper flex-full justify-content-between align-items-center">                        
                        <div class="logo d-flex w-auto">
                            <a href="javascript:void(0)" class="d-flex w-auto">
                                <amp-img src="<?php echo base_url();?>images/brand-logo.png" alt="Cityfurnish" width="160" height="39"></amp-img>
                                <!-- <img src="images/logo.png"> -->
                            </a>
                        </div>                        
                    </div>
                </div>
            </div>
            <!-- logo part ends -->
           
            <!-- logo part (for mobile) starts -->
            <div class="mobile-logo-part flex-full">
                <div class="logo-part-wrapper flex-full justify-content-between align-items-center">
                    <div class="logo d-flex w-auto flex-wrap align-items-center">
                        <div class="menu-mobile">
                            <span></span>
                        </div>
                        <a href="javascript:void(0)" class="d-flex w-auto">
                            <amp-img src="images/mobile-logo-white.png" alt="Cityfurnish" height="25" width="28">
                            </amp-img>
                        </a>
                    </div>
                    <div class="mobile-page-heading">
                        <h2>Invoice</h2>
                    </div>
                    <div class="header-icons d-flex w-auto flex-wrap">
                        <ul class="header-icons-listing d-flex w-auto flex-wrap">                            
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="icn icn-profile-white"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- logo part (for mobile) ends -->
            <!-- navigation part (for mobile) starts -->
            <div class="mobile-navigation flex-full bg-img align-content-start align-items-start"
                style="background-image: url(images/innerpage-banner.jpg);">
                <div class="mobile-navigation-wrapper flex-full">
                    <span class="close"><i class="icn icn-close-black"></i></span>
                    <ul class="mobile-navigation-part flex-full">
                        <li><i class="icn icn-home-furniture"></i><a href="javascript:void(0)">Home Furniture</a>
                            <ul class="sub-menu flex-full">
                                <li><i class="icn icn-office-furniture"></i><a href="javascript:void(0)">Living Room</a>
                                </li>
                                <li><i class="icn icn-office-furniture"></i><a href="javascript:void(0)">Bed Room</a>
                                </li>
                                <li><i class="icn icn-office-furniture"></i><a href="javascript:void(0)">Dining Room</a>
                                </li>
                                <li><i class="icn icn-office-furniture"></i><a href="javascript:void(0)">Study Room</a>
                                </li>
                            </ul>
                        </li>
                        <li><i class="icn icn-office-furniture"></i><a href="javascript:void(0)">Office Furniture</a>
                        </li>
                        <li><i class="icn icn-electronics"></i><a href="javascript:void(0)">Electronics</a>
                            <ul class="sub-menu flex-full">
                                <li><i class="icn icn-office-furniture"></i><a href="javascript:void(0)">Home
                                        Appliances</a></li>
                            </ul>
                        </li>
                        <li><i class="icn icn-fitness"></i><a href="javascript:void(0)">Fitness</a></li>
                        <li><i class="icn icn-kids-furniture"></i><a href="javascript:void(0)">Kids Furniture</a></li>
                    </ul>
                    <ul class="mobile-navigation-part flex-full">
                        <li><i class="icn icn-combos"></i><a href="javascript:void(0)">Combos</a></li>
                        <li><i class="icn icn-rental-plans"></i><a href="javascript:void(0)">Fixed rental plans</a></li>
                    </ul>
                    <ul class="mobile-navigation-part flex-full">
                        <li><i class="icn icn-corportate-orders"></i><a href="javascript:void(0)">Corportate Orders</a>
                        </li>
                        <li><i class="icn icn-offers"></i><a href="javascript:void(0)">Offers</a></li>
                        <li><i class="icn icn-help"></i><a href="javascript:void(0)">Help</a></li>
                    </ul>
                </div>
            </div>
            <!-- navigation part (for mobile) ends -->
        </header>
        <!-- Header ends -->

        <div class="page-wrapper flex-full">
            <div class="address-selection fixed-rental-plans-section flex-full">
                <div class="fixed-rental-plans-wrapper flex-full align-items-center align-content-center">
                    <div class="container">
                        <p class="cust_Name">
                            <strong>Hi <?php echo strtoupper($purchaseList->row()->full_name); ?> </strong> 
                            <span>
                                Thank you for your order.
                            </span>
                        </p>                     
                    </div>

                    <div class="container">
                         <div class="flex-full">   
                        <table bgcolor="#ffffff" style="margin: 20px 0px;">
                            <tr>
                                <td class="border-left" style="padding: 0 0 0;width: 100%;color: #000000;font-weight: 700;font-size: 16px;padding-right: 10px;font-family: 'Nunito', sans-serif; padding-bottom: 10px;">
                                    Order Confirmation
                                </td>
                            </tr>                            
                            <tr>
                                <td style="padding: 10px 0 20px; width: 100%;">
                                    <table style="width: 100%;table-layout: fixed;margin: 0 auto;border-collapse: collapse;">
                                        <tr>
                                            <td style="font-size: 12px;color: #000000;font-family: 'Nunito', sans-serif;font-weight: 400;padding: 0;">Order Id : #<?php echo $purchaseList->row()->dealCodeNumber; ?></td>
                                            <td style="font-size: 12px;color: #000000;font-family: 'Nunito', sans-serif;font-weight: 400;text-align: right;padding: 0;">Order Date : <?php echo date("F j, Y g:i a",strtotime($purchaseList->row()->created)); ?></td>
                                        </tr>                                    
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                 </div>              

                 <div class="cart-main-section flex-full">
                    <div class="container">
                        <div class="cart-main-wrapper flex-full align-items-start align-content-start">
                            <div class="order-summary-block flex-full">
                                <h2 class="border-left">Order Summary</h2>
                                <ul class="order-summary-listing flex-full">
                                    <?php 
                                    $disTotal =0; $grantTotal = 0;$address = '';
                                    if($purchaseList->row()->address1 != ''){
                                        $address = $purchaseList->row()->address1.', '.$purchaseList->row()->city.' , '.$purchaseList->row()->state.' - '.$purchaseList->row()->postal_code ;
                                    }else if($purchaseList->row()->address2 != ''){
                                        $address = $purchaseList->row()->address2.', '.$purchaseList->row()->city.' , '.$purchaseList->row()->state.' - '.$purchaseList->row()->postal_code ;
                                    }
                                    foreach($purchaseList->result() as $key => $cartRow){ 
                                        // echo "<pre>";
                                        // print_r($cartRow);
                                        $InvImg = @explode(',',$cartRow->image);
                                        $unitPrice = ($cartRow->price*(0.01*$cartRow->product_tax_cost))+$cartRow->price * $cartRow->quantity;
                                        $unitDeposit =  $cartRow->product_shipping_cost * $cartRow->quantity;
                                        $grandDeposit = $grandDeposit + $unitDeposit;
                                        $uTot = $unitPrice + $unitDeposit;
                                    	
                                    	$grantTotal = $grantTotal + $uTot;
                                    	$new_deposite +=  $unitDeposit;
                                    	$new_rental +=  $unitPrice;
                                    	$new_total += $uTot;
                                        if($cartRow->attr_name != '' || $cartRow->attr_type != ''){ $atr = $cartRow->attr_name; }else{ $atr = '';}
                                    ?>
                                    <li>
                                        <div class="order-summary-box flex-full">
                                            <div class="product-img flex-full">
                                                <amp-img src="<?php echo base_url().PRODUCTPATH.$InvImg[0]; ?>" alt="Product Image" width="204"
                                                    height="160"></amp-img>
                                            </div>
                                            <div
                                                class="product-desc flex-full align-items-start align-content-start">                                               
                                                <h3><?php echo stripslashes($cartRow->product_name); ?></h3>
                                                <ul class="product-desc-listing flex-full">
                                                    <li>
                                                        <h4>Quantity</h4>
                                                        <span><?php echo $cartRow->quantity; ?> item</span>
                                                    </li>
                                                    <li>
                                                        <h4>Monthly Rent</h4>
                                                        <span>&#x20B9; <?php echo number_format($unitPrice,2,'.','');  ?></span>
                                                    </li>
                                                    <li>
                                                        <h4>Refundable Deposit</h4>
                                                        <span>&#x20B9; <?php echo number_format($unitDeposit,2,'.',''); ?></span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                    <?php }
                                    	$private_total = $grantTotal - $purchaseList->row()->discountAmount;
                                    	$private_total = $private_total + $purchaseList->row()->tax;
                                    ?>
                                </ul>                          
                            </div>
                            <aside class="cart-sidebar flex-full align-items-start align-content-start">
                                <div class="cart-sidebar-block order-detail-block flex-full">
                                    <h2 class="border-left">Shipping Details</h2>   
                                    <ul class="address-listing flex-full" style="margin: 20px 0 0px;">
                                        <li class="shipping-Details">
                                            <div class="address-box flex-full">
                                                <h3><i class="icn icn-location"></i><?php echo strtoupper($purchaseList->row()->full_name); ?></h3>
                                                <address class="shipping-address"><?php echo stripslashes($address); ?></address>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="cart-sidebar-block order-detail-block flex-full">
                                    <h2 class="border-left">Order Detail</h2>
                                    <ul class="product-desc-listing flex-full">
                                        <li>
                                            <h4>Duration</h4>
                                            <span><?php echo $atr; ?></span>
                                        </li>
                                        <li>
                                            <h4>Advance Rental</h4>
                                            <span>&#x20B9; <?php echo number_format($new_rental,2,'.',''); ?></span>
                                        </li>
                                        <li>
                                            <h4>Refundable Deposit</h4>
                                            <span>&#x20B9; <?php echo number_format($new_deposite,2,'.',''); ?></span>
                                        </li>
                                        <li>
                                            <h4>Discount</h4>
                                            <span>&#x20B9; <?php echo number_format($purchaseList->row()->discountAmount,'2','.','') ?></span>
                                        </li>
                                        <li>
                                            <h4>Delivery</h4>
                                            <span>Free</span>
                                        </li>
                                        <li>
                                            <h4>Total</h4>
                                            <span>&#x20B9; <?php echo number_format($private_total,'2','.',''); ?></span>
                                        </li>
                                    </ul>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>                    
                </div>
            </div>
        </div>
<?php $this->load->view('site/templates/footer'); ?>
    </div>
   
</body>

</html>