<!DOCTYPE html>
<html>

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>EPC Calculator</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/epc_styles.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>css/epc_style.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/css/select2.min.css">
</head>

<body>
    <div class="container calculator-form">
        <div class="contact-image">
            <img src="https://d1eohs8f9n2nha.cloudfront.net/images/logo-stick.png" alt="CityFurnish" />
        </div>
        <h3 data-toggle="tooltip" title="Early Pickup Charges Calculator">EPC Calculator</h3>
        <form method="post" action="<?php echo base_url('epc_calculator/calculate_ecp') ?>" name="epc_form">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" name="firstName" class="form-control" placeholder="First Name *" value="" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="text" name="lastName" class="form-control" placeholder="Last Name *" value="" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="email" name="txtEmail" class="form-control" placeholder="Email Address *" value=""/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <input type="number" name="securityDeposit" class="form-control" placeholder="Security Deposit *" value="" required/>
                    </div>
                </div>
            </div>

            <hr>
            <div class="row">
                <div class="col-md-12">
                    <strong>Select Product</strong>
                    <div class="form-group">                    
                        <select class="form-control" name="product[]" multiple="multiple">
                            <option disabled>Select Product</option>
                            <?php
                                if($products->num_rows() > 0){
                                    foreach($products->result() as $row){
                            ?>
                                        <option value="<?php echo $row->id ?>"><?php echo $row->product_name ?></option>          
                            <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <strong>Tenure Avail Coupon</strong>
                    <div class="form-group">                    
                        <select class="form-control" name="coupanCodeTenure">
                            <option disabled selected>Select Coupan</option>
                            <?php
                                if($couponCardsList->num_rows() > 0){
                                    foreach($couponCardsList->result() as $row){
                            ?>
                                        <option value="<?php echo $row->id ?>"><?php echo $row->code ?></option>          
                            <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <strong>Product Actual Coupon</strong>
                    <div class="form-group">                    
                        <select class="form-control" name="coupanCodeActual">
                            <option disabled selected>Select Coupan</option>
                            <?php
                                if($couponCardsList->num_rows() > 0){
                                    foreach($couponCardsList->result() as $row){
                            ?>
                                        <option value="<?php echo $row->id ?>"><?php echo $row->code ?></option>          
                            <?php
                                    }
                                }
                            ?>
                        </select>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="range-slider monthslider">
                            <strong>Tenure Avail for (In Months)</strong>
                            <input type="hidden" id="tenureslider" name="tenureslider" class="tenure-range-slider irs-hidden-input" value="24" tabindex="-1" readonly="">
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <strong>Product Actual Used for</strong>
                    <div class="form-group">
                        <div class="col-md-6 pl-0">
                            <input type="number" name="productActualUsedMonths" class="form-control" placeholder="Months" value="" min="0" max="24" required/>
                        </div>
                        <div class="col-md-6 pr-0">
                            <input type="number" name="productActualUsedDays" class="form-control" placeholder="Days" value="" min="0" max="29"/>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <strong>Tenure Amount (Per Month) </strong>
                        <input type="text" name="tenureAmount" class="form-control" placeholder="[(Original Rental*month)-coupon]" value=""/>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <strong>Actual Product Amount (Per Month) </strong>
                        <input type="text" name="productAmount" class="form-control" placeholder="[(Actual Rental*month)-coupon]" value="" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <strong>Actual Percentage </strong>
                        <input type="text" name="actualPercentage" class="form-control" placeholder="TBD" value="" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <strong>Product Damage Amount (if any)</strong>
                        <input type="number" name="damageAmount" min="0" class="form-control" placeholder="Damage Amount" value="" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <strong>Additional Discount</strong>
                        <input type="number" name="additionalDiscount" min="0" class="form-control" placeholder="Additional Discount" value="" />
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <strong>EPC</strong>
                        <input type="text" name="epc" class="form-control" placeholder="(Actual Amount - Original Amount)" value="" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <strong>Final Amount</strong>
                        <input type="number" name="waiverAmount" class="form-control" placeholder="TBD" value="" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <strong>Refundable Amount</strong>
                        <input type="number" name="refundableAmount" class="form-control" placeholder="Refundable Amount" value="" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="submit" name="btnSubmit" class="btnContact btn btn-default" value="Calculate" />
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="button" name="btnSubmit" class="btnContact btn btn-default" value="Save" />
                    </div>
                </div>
            </div>

            <!-- <div class="form-group">
                        <input type="text" name="txtEmail" class="form-control" placeholder="Email Address *" value="" required/>
                    </div>
                    <div class="form-group">
                        <input type="number" name="txtEmail" class="form-control" placeholder="Security Deposit *" value="" required/>
                    </div>
                    <div class="form-group">
                        <input type="number" name="txtEmail" class="form-control" placeholder="Coupon Amount *" value="" required/>
                    </div>
                    <div class="form-group">
                        <input type="text" name="txtPhone" class="form-control" placeholder="Your Phone Number *" value="" />
                    </div>
                    <div class="form-group">
                        <input type="submit" name="btnSubmit" class="btnContact" value="Send Message" />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <textarea name="txtMsg" class="form-control" placeholder="Your Message *" style="width: 100%; height: 150px;"></textarea>
                    </div>
                </div>
            </div> -->
        </form>
    </div>
    
    <script type="text/javascript" src="<?php echo base_url() ?>js/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/bootstrap.js"></script>
    <script type="text/javascript" src="<?php echo base_url() ?>js/ion.rangeSlider.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.12/js/select2.min.js"></script>
    <script>
        var dataLayer = dataLayer || [];
        var coupanDetailsTenure = [];
        var coupanDetailsActual = [];
        var tempTenureData = {};
        var $range = $(".tenure-range-slider");
        var tenureData = {};
        $(function(){
            $("select[name='product[]']").select2();
        });
        
        $("select[name='product[]']").on('change', function(){
            let product_id = $(this).val();
            $.ajax({
                url:"<?php echo base_url() ?>epc_calculator/get_sub_product_values/",
                method:'post',
                data:{
                    'product_ids':product_id
                },
                dataType:'json',
                success:function(data){
                    // console.log(data);
                    tenureData = {};
                    data.SubPrdVal.forEach(function(subProduct){
                        subProduct.forEach(function(proVal){
                            if(tenureData.hasOwnProperty(proVal.attr_name.split(" ")[0])){
                                tenureData[proVal.attr_name.split(" ")[0]] = {
                                    "price" : tenureData[proVal.attr_name.split(" ")[0]].price + parseInt(proVal.attr_price),
                                    "pid" : proVal.pid
                                }; 
                                console.log('if');
                            }else{
                                tenureData[proVal.attr_name.split(" ")[0]] = {
                                    "price" : parseInt(proVal.attr_price),
                                    "pid" : proVal.pid
                                }; 
                                console.log('else');
                            }
                            
                        })
                        // console.log(subProduct);
                        
                        
                        // tempTenureData[subProduct.attr_name].pid = subProduct.pid;
                        // console.log(subProduct.attr_name);
                    });
                    // tenureData = tempTenureData;
                    console.log(tenureData);
                },
                error:function(error){
                    console.log(error);
                }
           }); 
        });

        
        
        var SelectedTenure = '';
        if(SelectedTenure == ''){
          SelectedTenure  = Object.keys(tenureData).length;
        }else{
          var new_tenure = Object.keys(tenureData);
          SelectedTenure = new_tenure.indexOf(''+SelectedTenure+'');
          if(SelectedTenure < 0){
             SelectedTenure  = Object.keys(tenureData).length;
          }
        }
        
        $("select[name='coupanCodeTenure']").on('change', function(){
            let coupanCodeTenure = $(this).val();
            $.ajax({
                url:'<?php echo base_url() ?>epc_calculator/get_coupan_details/' + coupanCodeTenure,
                method:'get',
                dataType:'json',
                success:function(data){
                    coupanDetailsTenure = data.coupan_data;
                    console.log(coupanDetailsTenure);
                },
                error:function(error){
                    console.log(error);
                }
            }); 
        });
        
        $("select[name='coupanCodeActual']").on('change', function(){
            let coupanCodeActual = $(this).val();
            $.ajax({
                url:'<?php echo base_url() ?>epc_calculator/get_coupan_details/' + coupanCodeActual,
                method:'get',
                dataType:'json',
                success:function(data){
                    coupanDetailsActual = data.coupan_data;
                    console.log(coupanDetailsActual);
                },
                error:function(error){
                    console.log(error);
                }
            }); 
        });
        
        var finalRent = 0;
        var actualPercentage = 0;
        var earlyPickupCharges = 0;
        var productActualUsedMonths = 0;
        var finalRentAmt = 0;
        var rentApplicable = 0;
        
        $("select[name='coupanCodeTenure']").on('change', function(){
            let coupanCodeTenure = $(this).val();
            $.ajax({
                url:'<?php echo base_url() ?>epc_calculator/get_coupan_details/' + coupanCodeTenure,
                method:'get',
                dataType:'json',
                success:function(data){
                    coupanDetailsTenure = data.coupan_data;
                    console.log(coupanDetailsTenure);
                },
                error:function(error){
                    console.log(error);
                }
            }); 
        });
        
        $("select[name='coupanCodeActual']").on('change', function(){
            let coupanCodeActual = $(this).val();
            $.ajax({
                url:'<?php echo base_url() ?>epc_calculator/get_coupan_details/' + coupanCodeActual,
                method:'get',
                dataType:'json',
                success:function(data){
                    coupanDetailsActual = data.coupan_data;
                    console.log(coupanDetailsActual);
                },
                error:function(error){
                    console.log(error);
                }
            }); 
        });
        
        $("input[name='productActualUsedMonths']").on('input', function(){
            productActualUsedMonths = parseInt($(this).val());
            rentApplicable = productActualUsedMonths;
            updateRent(productActualUsedMonths, rentApplicable);
        });
        
        $("input[name='productActualUsedDays']").on('input', function(){
            let productActualUsedDays = 0
            let newApplicableRent = 0;
            productActualUsedDays = $("input[name='productActualUsedDays']").val();
            productActualUsedDays = parseInt(productActualUsedDays);
            if(productActualUsedDays > 15){
                newApplicableRent = rentApplicable + 1;
                updateRent(productActualUsedMonths, newApplicableRent);
            }
            productActualUsedMonths = parseInt(productActualUsedMonths);
            let finalRentInDays = 0;
            if(productActualUsedDays == ''){
                productActualUsedDays = 0;
            }
            if(productActualUsedDays < 31){
                //1 Month rent
                let rentOneMonths = 0;
                if(newApplicableRent > 0){
                    rentOneMonths = tenureData[newApplicableRent]['price'];
                }else{
                    rentOneMonths = tenureData[productActualUsedMonths]['price'];
                }
                
                let rentOneDay = rentOneMonths / 30;
                finalRentInDays = rentOneDay * productActualUsedDays;
                damageAmount = $("input[name='damageAmount']").val(); 
                if(damageAmount == ""){
                    damageAmount = 0;
                }
                damageAmount = parseInt(damageAmount);
                finalRentAmt = earlyPickupCharges + (finalRentInDays) + damageAmount;
                // $("input[name='epc']").val(finalRentAmt);
            } 
        });
        
        $("input[name='damageAmount']").on('input', function(){
            let damageAmount = 0;
            damageAmount = $("input[name='damageAmount']").val(); 
            if(damageAmount == ''){
                damageAmount = 0;
            }
            // if(finalRentAmt > 0){
            //     let totalAmtWithDanage = finalRentAmt + parseInt(damageAmount);
            //     $("input[name='epc']").val(totalAmtWithDanage);
            // }else{
            //     let totalAmtWithDanage = earlyPickupCharges + parseInt(damageAmount);
            //     $("input[name='epc']").val(totalAmtWithDanage);
            // }
            
        });
        
        function updateRent(productActualUsedMonths, rentApplicable){
            console.log(rentApplicable);
            let discountOnTenure = 0;
            let afterDiscountTenure = 0;
            let discountOnActualProduct = 0;
            let afterDiscountActualProduct = 0;
            let tenureAmountForUsedMonths = 0;
            let actualAmountForUsedMonths = 0;
            let amountLeftAfterCoupanOffTenure = 0;
            let amountLeftAfterCoupanOffActual = 0;
            let coupanFlatOffTenure = 0;
            let coupanFlatOffActual = 0;
            
            // if(productActualUsedMonths < 3){
            //     alert('Tenure can not be less then 3 months');
            //     $("input[name='productActualUsedMonths']").val('');
            // }
            if(productActualUsedMonths > 24){
                $("input[name='productActualUsedMonths']").val('');
            }
            if(productActualUsedMonths >= 3 && productActualUsedMonths <= 24){
                if(typeof tenureData[rentApplicable] != undefined){
                    let obj = rentApplicable in tenureData;
                    if(obj){
                        //No rent calculation required.
                        finalRent = tenureData[rentApplicable]['price'];
                        $("input[name='productAmount']").val(finalRent);
                        let tenureAmount = $("input[name='tenureAmount']").val();
                        tenureAmount = parseInt(tenureAmount);
                        productActualUsedMonthsForTenure = parseInt(productActualUsedMonths);
                        if(coupanDetailsTenure.length != 0){
                            //for percentage discount
                            if(coupanDetailsTenure[0]['price_type'] == 2){
                                if(coupanDetailsTenure[0]['recurring_invoice_tenure'] > 0){
                                    if(tenureAmount > coupanDetailsTenure[0]['maximumamount']){
                                        amountLeftAfterCoupanOffTenure = tenureAmount - parseInt(coupanDetailsTenure[0]['maximumamount']);
                                    }
                                    productActualUsedMonthsForTenure = productActualUsedMonths - coupanDetailsTenure[0]['recurring_invoice_tenure'];
                                }else{
                                    discountOnTenure = (tenureAmount * coupanDetailsTenure[0]['price_value']) / 100;
                                    afterDiscountTenure = tenureAmount - discountOnTenure;
                                }
                            }else{
                                coupanFlatOffTenure = coupanDetailsTenure[0]['price_value'];
                            }
                        }
                        if(afterDiscountTenure > 0){
                            tenureAmountForUsedMonths = afterDiscountTenure * productActualUsedMonthsForTenure;
                        }else{
                            tenureAmountForUsedMonths = parseInt(tenureAmount) * productActualUsedMonthsForTenure;
                        }
                        
                        productActualUsedMonthsForActual = parseInt(productActualUsedMonths);
                        if(coupanDetailsActual.length != 0){
                            //for percentage discount
                            if(coupanDetailsActual[0]['price_type'] == 2){
                                if(coupanDetailsActual[0]['recurring_invoice_tenure'] > 0){
                                    if(finalRent > coupanDetailsActual[0]['maximumamount']){
                                        amountLeftAfterCoupanOffActual = finalRent - coupanDetailsActual[0]['maximumamount'];
                                    }
                                    productActualUsedMonthsForActual = productActualUsedMonths - coupanDetailsActual[0]['recurring_invoice_tenure'];
                                }else{
                                    discountOnActualProduct = (finalRent * coupanDetailsActual[0]['price_value']) / 100;
                                    afterDiscountActualProduct = finalRent - discountOnActualProduct;
                                }
                            }else{
                                coupanFlatOffActual = coupanDetailsActual[0]['price_value'];
                            }
                        }
                        if(afterDiscountActualProduct > 0){
                            actualAmountForUsedMonths = afterDiscountActualProduct * parseInt(productActualUsedMonthsForActual);
                        }else{
                            actualAmountForUsedMonths = finalRent * parseInt(productActualUsedMonthsForActual);
                        }
                        
                        if(amountLeftAfterCoupanOffTenure > 0 || amountLeftAfterCoupanOffActual > 0){
                            earlyPickupCharges = (actualAmountForUsedMonths + amountLeftAfterCoupanOffActual) - (tenureAmountForUsedMonths + amountLeftAfterCoupanOffTenure);
                            // console.log(actualAmountForUsedMonths);
                            // $("input[name='epc']").val(earlyPickupCharges);
                            actualPercentage = parseInt((earlyPickupCharges) * 100) / parseInt(tenureAmount);
                            $("input[name='actualPercentage']").val(actualPercentage);
                        }else{
                            if(coupanFlatOffTenure > 0 || coupanFlatOffActual > 0){
                                if(tenureAmountForUsedMonths < coupanDetailsTenure[0]['minamountf']){
                                    coupanFlatOffTenure = 0;
                                }
                                if(actualAmountForUsedMonths < coupanDetailsActual[0]['minamountf']){
                                    coupanFlatOffActual = 0;
                                }
                                earlyPickupCharges = (actualAmountForUsedMonths - coupanFlatOffActual) - (tenureAmountForUsedMonths - coupanFlatOffTenure);
                                // console.log(actualAmountForUsedMonths);
                                // $("input[name='epc']").val(earlyPickupCharges);
                                actualPercentage = parseInt((earlyPickupCharges) * 100) / parseInt(tenureAmount);
                                $("input[name='actualPercentage']").val(actualPercentage);
                            }else{
                                earlyPickupCharges = actualAmountForUsedMonths - tenureAmountForUsedMonths;
                                // console.log(actualAmountForUsedMonths);
                                // $("input[name='epc']").val(earlyPickupCharges);
                                actualPercentage = parseInt((earlyPickupCharges) * 100) / parseInt(tenureAmount);
                                $("input[name='actualPercentage']").val(actualPercentage);
                            }
                        }
                        
                    }else{
                        //additional month calculation required.
                        /*
                        *If tenure is less then 3 months
                        */
                        if(productActualUsedMonths < 3){
                            finalRent = tenureData[3]['price'];
                            $("input[name='productAmount']").val(finalRent);
                            let tenureAmount = $("input[name='tenureAmount']").val();
                            actualPercentage = parseInt(finalRent * 100) / parseInt(tenureAmount);
                            $("input[name='actualPercentage']").val(actualPercentage);
                        }else if(productActualUsedMonths > 12 && productActualUsedMonths < 15){
                            finalRent = tenureData[12]['price'];
                            $("input[name='productAmount']").val(finalRent);
                            let tenureAmount = $("input[name='tenureAmount']").val();
                            tenureAmount = parseInt(tenureAmount);
                            actualAmountForUsedMonths = finalRent * productActualUsedMonths;
                            tenureAmountForUsedMonths = tenureAmount * productActualUsedMonths;
                            earlyPickupCharges = actualAmountForUsedMonths - tenureAmountForUsedMonths;
                                // console.log(actualAmountForUsedMonths);
                                // $("input[name='epc']").val(earlyPickupCharges);
                            actualPercentage = parseInt((earlyPickupCharges) * 100) / parseInt(tenureAmount);
                            $("input[name='actualPercentage']").val(actualPercentage);
                        }else if(productActualUsedMonths >= 15 && productActualUsedMonths < 22){
                            finalRent = tenureData[18]['price'];
                            $("input[name='productAmount']").val(finalRent);
                            let tenureAmount = $("input[name='tenureAmount']").val();
                            tenureAmount = parseInt(tenureAmount);
                            actualAmountForUsedMonths = finalRent * productActualUsedMonths;
                            tenureAmountForUsedMonths = tenureAmount * productActualUsedMonths;
                            earlyPickupCharges = actualAmountForUsedMonths - tenureAmountForUsedMonths;
                                // console.log(actualAmountForUsedMonths);
                                // $("input[name='epc']").val(earlyPickupCharges);
                            actualPercentage = parseInt((earlyPickupCharges) * 100) / parseInt(tenureAmount);
                            $("input[name='actualPercentage']").val(actualPercentage);
                        }else if(productActualUsedMonths >= 22 && productActualUsedMonths < 24){
                            finalRent = tenureData[24]['price'];
                            $("input[name='productAmount']").val(finalRent);
                            let tenureAmount = $("input[name='tenureAmount']").val();
                            tenureAmount = parseInt(tenureAmount);
                            actualAmountForUsedMonths = finalRent * productActualUsedMonths;
                            tenureAmountForUsedMonths = tenureAmount * productActualUsedMonths;
                            earlyPickupCharges = actualAmountForUsedMonths - tenureAmountForUsedMonths;
                                // console.log(actualAmountForUsedMonths);
                                // $("input[name='epc']").val(earlyPickupCharges);
                            actualPercentage = parseInt((earlyPickupCharges) * 100) / parseInt(tenureAmount);
                            $("input[name='actualPercentage']").val(actualPercentage);
                        }
                        
                    }
                    console.log(obj);
                    // console.log(tenureData[productActualUsedMonths]); 
                }    
            }else if(productActualUsedMonths < 3){
                finalRent = tenureData[3]['price'];
                $("input[name='productAmount']").val(finalRent);
                let tenureAmount = $("input[name='tenureAmount']").val();
                actualPercentage = parseInt(finalRent * 100) / parseInt(tenureAmount);
                $("input[name='actualPercentage']").val(actualPercentage);
            }
        }
        
        $range.ionRangeSlider({
            type: "single",
            values: [3,4,5,6,7,8,9,10,11,12,18,24],
            grid: true,
            from: SelectedTenure,
            onStart: updateInput,
            onChange: updateInput
        });
        
        function updateInput (data) {
            var url = 'https://cityfurnish.com/things/3955/hugo-single-bed';
            if(coupanDetailsTenure.length != 0){
                console.log(coupanDetailsTenure);
                //for percentage discount
                if(coupanDetailsTenure[0]['price_type'] == 2){
                    if(coupanDetailsTenure[0]['recurring_invoice_tenure'] > 0){
                        
                    }
                }
            }
            console.log(data.from_value);
            $('#selected_tenure').val(data.from_value);
            if(tenureData.hasOwnProperty(data.from_value)){
                $("input[name='tenureAmount']").val(tenureData[data.from_value]['price']);    
            }
            
            // $('#SalePrice').html(tenureData[data.from_value]['price']);
            // $('#attr_name_id').val(tenureData[data.from_value]['pid']);
            
            dataLayer.push({
              'event': 'eventsToSend',
              'eventCategory': 'MonthRangeSlider',
              'eventAction': data.from_value,
              'eventLabel': url
            });
        }
        
        $("form[name='epc_form']").on('submit', function(e){
            e.preventDefault();
            $.ajax({
                url:$(this).attr('action'),
                method:$(this).attr('method'),
                data:$(this).serialize(),
                dataType:'json',
                success:function(data){
                    console.log(data);
                    
                    $("input[name='epc']").val(data.data['earliyPickupCharges']);
                    $("input[name='waiverAmount']").val(data.data['waiverAmount']);
                    $("input[name='refundableAmount']").val(data.data['refundableAmount']);
                },
                error:function(error){
                    console.log(error);
                }
            });
        });
    </script>
</body>

</html>