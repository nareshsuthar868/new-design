                <article class="content item-editor-page">
                    <div class="title-block">
                        <h3 class="title"> Add new User<span class="sparkline bar" data-type="bar"></span> </h3>
                    </div>
                    <form name="item" action="" method="post">
                        <div class="card card-block">
                            <div class="form-group row"> <label class="col-sm-2 form-control-label text-xs-right">
				Admin Name:
			</label>
                                <div class="col-sm-10"> <input type="text" class="form-control boxed" placeholder="" name="admin_name" required> </div>
                            </div>
                            <div class="form-group row"> <label class="col-sm-2 form-control-label text-xs-right">
				Email:
			</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control boxed" placeholder="" name="email" required>
                                </div>
                            </div>                            
							<div class="form-group row"> <label class="col-sm-2 form-control-label text-xs-right">
				For Topics:
			</label>
                                <div class="col-sm-10">
                                   <select class="form-control boxed" name="topic[]" multiple required>
										<?php foreach($topics as $row){ ?>
											<option value="<?php echo $row->ID; ?>"><?php echo $row->t_title; ?></option>
										<?php } ?>
                                   </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10 col-sm-offset-2"> <button type="submit" class="btn btn-primary">
					Submit
				</button> </div>
                            </div>
                        </div>
					<input type="hidden" name="submit" value="submit" />	
                    </form>
                </article>