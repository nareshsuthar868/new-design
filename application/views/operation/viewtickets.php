                <article class="content items-list-page">
				<?php if(!empty($tickets)){ ?>
                    <div class="card items">
                        <ul class="item-list striped">
                            <li class="item item-list-header hidden-sm-down">
                                <div class="item-row">
                                    <div class="item-col item-col-header item-col-title">
                                        <div> <span>Email</span> </div>
                                    </div>                                   
									<div class="item-col item-col-header item-col-sales">
                                        <div> <span>Type</span> </div>
                                    </div>
                                    <div class="item-col item-col-header item-col-date">
                                        <div> <span>Published</span> </div>
                                    </div>                                    
									<div class="item-col item-col-header item-col-date">
                                        <div> <span>Status</span> </div>
                                    </div>									
									<div class="item-col item-col-header item-col-date">
                                        <div> <span>View</span> </div>
                                    </div>
                                </div>
                            </li>
					<?php foreach($tickets as $row){ ?>
                            <li class="item">
                                <div class="item-row">
                                    <div class="item-col fixed pull-left item-col-title">
                                        <div class="item-heading">Email</div>
                                        <div>
											<h4 class="item-title"> <?php echo $row->t_email; ?> </h4>
                                        </div>
                                    </div>
									<div class="item-col item-col-sales">
                                        <div class="item-heading">Type</div>
                                        <div> <?php echo $row->t_title; ?> </div>
                                    </div>
                                    <div class="item-col item-col-date">
                                        <div class="item-heading">Published</div>
                                        <div class="no-overflow"> <?php echo date('d F, Y',strtotime($row->adddateTime)); ?> </div>
                                    </div>
									<div class="item-col item-col-sales">
                                        <div class="item-heading">Status</div>
                                        <div> 
											<select class="form-control" onchange="changestatus(this.value,<?php echo $row->ID; ?>)">
												<option value="NEW" <?php if($row->t_status=='NEW'){ echo "selected"; } ?>>NEW</option>
												<option value="In Progress" <?php if($row->t_status=='In Progress'){ echo "selected"; } ?>>In Progress</option>
												<option value="Completed" <?php if($row->t_status=='Completed'){ echo "selected"; } ?>>Completed</option>
											</select>
										</div>
                                    </div>
									<div class="item-col item-col-date">
                                        <div class="item-heading">View</div>
                                        <div class="no-overflow"> <a href="<?php echo BASE_URL; ?>ticketinfo/<?php echo $row->ID; ?>">View</a> </div>
                                    </div>
                                </div>
                            </li>
					<?php } ?>
                        </ul>
                    </div>
				<?php } ?>
                </article>
