<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to user management 
 * @author Teamtweaks
 *
 */

class Commission extends MY_Controller {

	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('bdpartner_model');
	 }

    public function index(){

    }

    public function show_commissions(){
    	 
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$this->data['heading'] = 'Commissions listing';
			$this->load->view('bdadmin/order/commission',$this->data);
		}
    }

    public function get_commissions(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$start = (isset($_GET['start']))? intval($_GET['start']) : 0;
		    $length = (isset($_GET['length']) && intval($_GET['length']) > 0 )? $_GET['length'] : 0;
			$orderIndex = (isset($_GET['order'][0]['column']))? $_GET['order'][0]['column'] : 0; 
	    	$orderType = (isset($_GET['order'][0]['dir']))? $_GET['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_GET['search']['value']))? $_GET['search']['value'] : NULL;
	        
			// print_r($orderType);exit;
	        $data = $this->bdpartner_model->getCommissions(NULL,$length,$start,$orderIndex,$orderType,$searchValue);
	        $TotalCount = $this->bdpartner_model->getCommissions('count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_GET['draw']), "recordsTotal" => count($TotalCount), "recordsFiltered" => count($TotalCount));
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}
    }

    public function get_users_leads(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$this->data['heading'] = 'Lead listing';
			$myLeads = $this->bdpartner_model->get_bduser_leads($this->input->post('user_id'));
			$respo_array = array('status' => 200 ,'message' => 'Payout Listing', 'data' => $myLeads->result());
    		echo json_encode($respo_array);
		}	
    	// print_r($this->input->post('user_id'));exit;
    }

  	public function bd_insentive(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$this->data['heading'] = 'BD List';
			$this->load->view('bdadmin/order/bd_users_insentive',$this->data);
		}
	}

	//Show listing of bd/RM users
	public function get_bd_users(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$start = (isset($_GET['start']))? intval($_GET['start']) : 0;
		    $length = (isset($_GET['length']) && intval($_GET['length']) > 0 )? $_GET['length'] : 0;
			$orderIndex = (isset($_GET['order'][0]['column']))? $_GET['order'][0]['column'] : 0; 
	    	$orderType = (isset($_GET['order'][0]['dir']))? $_GET['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_GET['search']['value']))? $_GET['search']['value'] : NULL;
	        
	        $data = $this->bdpartner_model->getBDexecuters(NULL,$length,$start,$orderIndex,$orderType,$searchValue);
	        $TotalCount = $this->bdpartner_model->getBDexecuters('count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_GET['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}
	}
	public function get_revenue(){
		$user_id = $this->input->post('user_id'); 
// 		$user = $this->bdpartner_model->get_all_details(USERS,array('id' =>$user_id));
        // if($user->row()->user_type != 'BD Executive'){
        //     $result = array('message' => $this->lang->line('invalid_access'),'status_code' => 400, 'data' =>new stdclass() );
        //     $this->sendResponse($result,false);
        //     return;
        // }
        $ReturnArray = array('total_revenue' => 0,'my_revenue' => 0,'broker_revenue' => 0,'total_orders' => 0, 'my_orders' => 0,'broker_orders' => 0);

        $get_my_insentive = $this->bdpartner_model->get_my_revenue($user_id);
        if($get_my_insentive->num_rows() > 0){
            $ReturnArray['my_revenue'] = $get_my_insentive->row()->rental_amount ? $get_my_insentive->row()->rental_amount : 0;
        }
        $my_users = $this->bdpartner_model->get_bdexecuter_users($user_id);
        if($my_users->num_rows() > 0){
            $get_broker_insentive = $this->bdpartner_model->get_broker_revenue($my_users->row());
            if($get_broker_insentive->num_rows() > 0){
                 $ReturnArray['broker_revenue'] = $get_broker_insentive->row()->rental_amount ?  $get_broker_insentive->row()->rental_amount : 0;
            }
            $get_broker_order_count = $this->bdpartner_model->get_broker_order_count($my_users->row());
            if($get_broker_order_count->num_rows() > 0){
                 $ReturnArray['broker_orders'] = $get_broker_order_count->num_rows() ?  $get_broker_order_count->num_rows() : 0;
            }
        }
        // order details 
        $get_my_order_count = $this->bdpartner_model->get_my_order_count($user_id);
         if($get_my_order_count->num_rows() > 0){
            $ReturnArray['my_orders'] = $get_my_order_count->num_rows() ? $get_my_order_count->num_rows() : 0;
        }
        $ReturnArray['total_revenue'] = $ReturnArray['my_revenue'] + $ReturnArray['broker_revenue'];
        $ReturnArray['total_orders'] = $ReturnArray['my_orders'] + $ReturnArray['broker_orders'];
    	header('Content-Type: application/json');
		echo json_encode($ReturnArray);exit; 
        // print_r($ReturnArray);exit;
	}

}
