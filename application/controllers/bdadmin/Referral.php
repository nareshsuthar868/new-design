<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to user management 
 * @author Teamtweaks
 *
 */

class Referral extends MY_Controller {

	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('bdpartner_model');
    }
    
    /**
     * 
     * This function loads the users list page
     */
   	public function index(){	
   		// print_r("expression");exit;
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			// redirect('bdadmin/order/display_referral_list');
			$this->load->view('bdadmin/order/display_referral_list',$this->data);
		}
	}

	public function get_referral_listing(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$start = (isset($_REQUEST['start']))? intval($_REQUEST['start']) : 0;
		    $length = (isset($_REQUEST['length']) && intval($_REQUEST['length']) > 0 )? $_REQUEST['length'] : 0;
			$orderIndex = (isset($_REQUEST['order'][0]['column']))? $_REQUEST['order'][0]['column'] : 0; 
	    	$orderType = (isset($_REQUEST['order'][0]['dir']))? $_REQUEST['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_REQUEST['search']['value']))? $_REQUEST['search']['value'] : NULL;
	        
			// print_r($orderType);exit;
	        $data = $this->bdpartner_model->getReferralDetails(NULL,$length,$start,$orderIndex,$orderType,$searchValue);
	        $TotalCount = $this->bdpartner_model->getReferralDetails('count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_REQUEST['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}
	}

	public function get_myreferral(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$myRefferal = $this->bdpartner_model->get_my_referral($_POST['user_id']);
			header('Content-Type: application/json');
			echo json_encode($myRefferal->result());exit;
		}
	}

}

/* End of file users.php */
/* Location: ./application/controllers/admin/users.php */