<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to sub-admin management 
 * @author Teamtweaks
 *
 */

class Subadmin extends MY_Controller {

	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('bdpartner_model');
		if ($this->checkBDPrivileges('subadmin',$this->privStatus) == FALSE){
			redirect('bdadmin');
		}
    }
    
	/**
	 * 
	 * This function loads the subadmin users list
	 */
	public function display_sub_admin(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$this->data['heading'] = 'Sub Admin Users List';
			$condition = array();
			$this->data['admin_users'] = $this->bdpartner_model->get_all_details(BD_SUBADMIN,$condition);
			$this->load->view('bdadmin/subadmin/display_subadmin',$this->data);
		}
	}
	
	/**
	 * 
	 * This function change the subadmin user status
	 */
	public function change_subadmin_status(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$mode = $this->uri->segment(4,0);
			$adminid = $this->uri->segment(5,0);
			$status = ($mode == '0')?'Inactive':'Active';
			$newdata = array('status' => $status);
			$condition = array('id' => $adminid);
			$this->bdpartner_model->update_details(BD_SUBADMIN,$newdata,$condition);
			$this->setErrorMessage('success','Sub Admin Status Changed Successfully');
			redirect('bdadmin/subadmin/display_sub_admin');
		}
	}
	
	/**
	 * 
	 * This function loads the add subadmin form 
	 */
	public function add_sub_admin_form(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$this->data['heading'] = 'Add Subadmin';
			$condition = array();
			$this->load->view('bdadmin/subadmin/add_subadmin',$this->data);
		}
	}
	
	/**
	 * 
	 * This function insert and edit a subadmin and his privileges
	 */
	public function insertEditSubadmin(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$subadminid = $this->input->post('subadminid');
			$admin_name = $this->input->post('admin_name');
			$admin_password = md5($this->input->post('admin_password'));
			$email = $this->input->post('email');
			if ($subadminid == ''){
				$condition = array('email' => $email);
				$duplicate_admin= $this->bdpartner_model->get_all_details(BD_ADMIN,$condition);
				if ($duplicate_admin->num_rows() > 0){
					$this->setErrorMessage('error','Admin email already exists');
					redirect('bdadmin/subadmin/add_sub_admin_form');
				}else {
					$duplicate_email = $this->bdpartner_model->get_all_details(BD_SUBADMIN,$condition);
					if ($duplicate_email->num_rows() > 0){
						$this->setErrorMessage('error','Sub Admin email already exists');
						redirect('bdadmin/subadmin/add_sub_admin_form');
					}else {
						$condition = array('admin_name' => $admin_name);
						$duplicate_adminname = $this->bdpartner_model->get_all_details(BD_ADMIN,$condition);
						if ($duplicate_adminname->num_rows() > 0){
							$this->setErrorMessage('error','Admin name already exists');
							redirect('bdadmin/subadmin/add_sub_admin_form');
						}else {
							$duplicate_name = $this->bdpartner_model->get_all_details(BD_SUBADMIN,$condition);
							if ($duplicate_name->num_rows() > 0){
								$this->setErrorMessage('error','Sub Admin name already exists');
								redirect('bdadmin/subadmin/add_sub_admin_form');
							}
						}
					}
				}
			}
			$excludeArr = array("email","subadminid","admin_name","admin_password");

			$privArr = array();
			foreach ($this->input->post() as $key => $val){
				if (!in_array($key, $excludeArr)){
					$privArr[$key] = $val;
				}
			}
			
			$inputArr = array('privileges' => serialize($privArr));
			$datestring = "%Y-%m-%d";
			$time = time();
			if ($subadminid == ''){
				$admindata = array(
					'admin_name'	=>	$admin_name,
					'admin_password'	=>	$admin_password,
					'email'	=>	$email,
					'created'	=>	mdate($datestring,$time),
					'modified'	=>	mdate($datestring,$time),
					'admin_type'	=>	'sub',
					'is_verified'	=>	'Yes',
					'status'	=>	'Active'
				);
			}else {
				$admindata = array('modified' =>	mdate($datestring,$time));
			}
			$dataArr = array_merge($admindata,$inputArr);
			$condition = array('id' => $subadminid);
			$this->bdpartner_model->add_edit_subadmin($dataArr,$condition);
			if ($subadminid == ''){
				$this->setErrorMessage('success','Subadmin added successfully');
			}else {
				$this->setErrorMessage('success','Subadmin updated successfully');
			}
			redirect('bdadmin/subadmin/display_sub_admin');
		}
	}
	
	/**
	 * 
	 * This function loads the edit subadmin form
	 */
	public function edit_subadmin_form(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$this->data['heading'] = 'Edit Subadmin';
			$adminid = $this->uri->segment(4,0);
			$condition = array('id' => $adminid);
			$this->data['admin_details'] = $this->bdpartner_model->get_all_details(BD_SUBADMIN,$condition);
			if ($this->data['admin_details']->num_rows() == 1){
				$this->data['privArr'] = unserialize($this->data['admin_details']->row()->privileges);
				if (!is_array($this->data['privArr'])){
					$this->data['privArr'] = array();
				}
				$this->load->view('bdadmin/subadmin/edit_subadmin',$this->data);
			}else {
				redirect('bdadmin');
			}
		}
	}
	
	/**
	 * 
	 * This function loads the subadmin view page
	 */
	public function view_subadmin(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$this->data['heading'] = 'View Subadmin';
			$adminid = $this->uri->segment(4,0);
			$condition = array('id' => $adminid);
			$this->data['admin_details'] = $this->bdpartner_model->get_all_details(BD_SUBADMIN,$condition);
			if ($this->data['admin_details']->num_rows() == 1){
				$this->data['privArr'] = unserialize($this->data['admin_details']->row()->privileges);
				if (!is_array($this->data['privArr'])){
					$this->data['privArr'] = array();
				}
				$this->load->view('bdadmin/subadmin/view_subadmin',$this->data);
			}else {
				redirect('bdadmin');
			}
		}
	}
	
	/**
	 * 
	 * This function delete the subadmin record from db
	 */
	public function delete_subadmin(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$subadmin_id = $this->uri->segment(4,0);
			$condition = array('id' => $subadmin_id);
			$this->bdpartner_model->commonDelete(BD_SUBADMIN,$condition);
			$this->setErrorMessage('success','Subadmin deleted successfully');
			redirect('bdadmin/subadmin/display_sub_admin');
		}
	}
	
	/**
	 * 
	 * This function change the subadmin status, delete the subadmin record
	 */
	public function change_subadmin_status_global(){
		if(count($_POST['checkbox_id']) > 0 &&  $_POST['statusMode'] != ''){
			$this->bdpartner_model->activeInactiveCommon(BD_SUBADMIN,'id');
			if (strtolower($_POST['statusMode']) == 'delete'){
				$this->setErrorMessage('success','Subadmin records deleted successfully');
			}else {
				$this->setErrorMessage('success','Subadmin records status changed successfully');
			}
			redirect('bdadmin/subadmin/display_sub_admin');
		}
	}
}

/* End of file subadmin.php */
/* Location: ./application/controllers/admin/subadmin.php */