<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to user management 
 * @author Teamtweaks
 *
 */

class Setting extends MY_Controller {

	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('bdpartner_model');
    }

    public function show_comission_rates(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {

			$this->data['heading'] = 'Comission List';
			$this->data['comission_list'] = $this->bdpartner_model->get_all_details(COMISSIONS,array());
			$this->load->view('bdadmin/adminsettings/comission_rates',$this->data);
		}
    }

    public function addNewComission(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$insert_array['tenure']  = $this->input->post('tenure').' Months';
			$insert_array['comission']  = $this->input->post('comission');
			if($this->input->post('type') == 'add'){
				$this->bdpartner_model->simple_insert(COMISSIONS,$insert_array);
			}else{
				$this->bdpartner_model->update_details(COMISSIONS,$insert_array,array('id' => $this->input->post('id')));
			}
			echo json_encode(array('status' => 'success'));
			exit;
		}	
    }

    public function deleteComissions(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			
			$this->bdpartner_model->commonDelete(COMISSIONS,array('id' => $this->input->post('id')));
			echo json_encode(array('status' => 'success'));
			exit;
		}	
    }

    public function cms_pages(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$this->data['heading'] = 'CMS Pages';
			$this->load->view('bdadmin/cms/cms_listing',$this->data);
		}	
    }

    public function get_cms_pages(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$start = (isset($_GET['start']))? intval($_GET['start']) : 0;
		    $length = (isset($_GET['length']) && intval($_GET['length']) > 0 )? $_GET['length'] : 0;
			$orderIndex = (isset($_GET['order'][0]['column']))? $_GET['order'][0]['column'] : 0; 
	    	$orderType = (isset($_GET['order'][0]['dir']))? $_GET['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_GET['search']['value']))? $_GET['search']['value'] : NULL;
	        
			// print_r($orderType);exit;
	        $data = $this->bdpartner_model->get_cmslisting(NULL,$length,$start,$orderIndex,$orderType,$searchValue);
	        $TotalCount = $this->bdpartner_model->get_cmslisting('count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_GET['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}		
    }

    public function edit_cms(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$this->data['heading'] = 'Edit Cms';
			$cms_id = $this->uri->segment(4,0);
			$this->data['cms_details'] = $this->bdpartner_model->get_all_details(BD_CMS,array('id' => $cms_id));
			if ($this->data['cms_details']->num_rows() == 1){
				$this->load->view('bdadmin/cms/edit_cms',$this->data);
			} else {
				redirect('bdadmin');
			}
		}
    }

    public function updateCms(){
     	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {

			$this->bdpartner_model->update_details(BD_CMS,array('page_name' => $this->input->post('page_name'),'page_content' => $this->input->post('page_content'),'updated_date' => date("Y/m/d")),array('id' => $this->input->post('cms_id')));
			$this->setErrorMessage('success','Cms updated successfully');
			redirect('bdadmin/setting/cms_pages');
	
		}	
    }


    public function offers_page(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {

			$this->data['heading'] = 'Offer List';
			// $this->data['offers'] = $this->bdpartner_model->get_all_details(DB_OFFERS,array());
			$this->load->view('bdadmin/cms/offer',$this->data);
		}
    }

     public function get_offer_listing(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$start = (isset($_GET['start']))? intval($_GET['start']) : 0;
		    $length = (isset($_GET['length']) && intval($_GET['length']) > 0 )? $_GET['length'] : 0;
			$orderIndex = (isset($_GET['order'][0]['column']))? $_GET['order'][0]['column'] : 0; 
	    	$orderType = (isset($_GET['order'][0]['dir']))? $_GET['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_GET['search']['value']))? $_GET['search']['value'] : NULL;
	        
			// print_r($orderType);exit;
	        $data = $this->bdpartner_model->get_offerlisting(NULL,$length,$start,$orderIndex,$orderType,$searchValue);
	        $TotalCount = $this->bdpartner_model->get_offerlisting('count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_GET['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}		
    }

    // public function add_offer()


    public function add_new_offers(){
		if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {

			$this->data['heading'] = 'Add New Offer';
			$this->load->view('bdadmin/cms/add_offers',$this->data);
		}
    }

    public function addInsertOffer(){
    	if($this->input->post('offer_id') == ''){
    		$this->bdpartner_model->commonInsertUpdate(BD_OFFERS,'insert',array(),array());
    		// redirect('bdadmin/setting/offers_page');
    	}else{
    		$excludeArr = array("offer_id");
			$this->bdpartner_model->commonInsertUpdate(BD_OFFERS,'update',$excludeArr,array(),array('id' => $this->input->post('offer_id')));
    	}
		redirect('bdadmin/setting/offers_page');
    }

    public function edit_offer(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$this->data['heading'] = 'Edit Offer';
			$offer_id = $this->uri->segment(4,0);
			$this->data['offer_details'] = $this->bdpartner_model->get_all_details(BD_OFFERS,array('id' => $offer_id));
			if ($this->data['offer_details']->num_rows() == 1){
				$this->load->view('bdadmin/cms/edit_offer',$this->data);
			} else {
				redirect('bdadmin');
			}
		}
    }

    public function delete_offer(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$offer_id = $this->uri->segment(4,0);
			$offer = $this->bdpartner_model->get_all_details(BD_OFFERS,array('id' => $offer_id));
			if ($offer->num_rows() > 0){
				$this->bdpartner_model->commonDelete(BD_OFFERS,array('id' => $offer_id));
				redirect('bdadmin/setting/offers_page');
			} else {
				redirect('bdadmin');
			}
		}	
    }
    
        
    public function get_client_response_status(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$this->data['heading'] = 'Status For Customer Response On Lead';
			$this->load->view('bdadmin/adminsettings/client_response_listing',$this->data);
		}	
    }

    public function get_listing_for_status(){
        if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		}else {
			$start = (isset($_GET['start']))? intval($_GET['start']) : 0;
		    $length = (isset($_GET['length']) && intval($_GET['length']) > 0 )? $_GET['length'] : 0;
			$orderIndex = (isset($_GET['order'][0]['column']))? $_GET['order'][0]['column'] : 0; 
	    	$orderType = (isset($_GET['order'][0]['dir']))? $_GET['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_GET['search']['value']))? $_GET['search']['value'] : NULL;
	        
	        $data = $this->bdpartner_model->get_lead_client_notify(NULL,$length,$start,$orderIndex,$orderType,$searchValue);
	        $TotalCount = $this->bdpartner_model->get_lead_client_notify('count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_GET['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}
    }

    public function edit_client_response_status(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$this->data['heading'] = 'Edit Status';
			$status_id = $this->uri->segment(4,0);
			$this->data['status_details'] = $this->bdpartner_model->get_all_details(LEAD_RESPONSE_STATUS,array('id' => $status_id));
			if ($this->data['status_details']->num_rows() == 1){
				$this->load->view('bdadmin/adminsettings/edit_client_response_status',$this->data);
			} else {
				redirect('bdadmin');
			}
		}
    }

    public function update_client_response_status(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$this->data['heading'] = 'Edit Status';
			$this->bdpartner_model->commonInsertUpdate(LEAD_RESPONSE_STATUS,'update',array('id'),array(),array('id' => $this->input->post('id')));
			redirect('bdadmin/setting/get_client_response_status');	
		}
    }
    
     public function brochers(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$this->data['heading'] = 'Brochure Listing';
			$this->data['brochers_listing'] = $this->bdpartner_model->get_all_details(BD_BROCHERS,array());
			$this->load->view('bdadmin/brocher/brocher_listing',$this->data);
		}
    }

    public function add_brochers(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$this->data['heading'] = 'Add Brochure';
			$this->load->view('bdadmin/brocher/add_brocher',$this->data);
		}	
    }

    public function addInsertBorcher(){
    	  //load form validation library
        $this->form_validation->set_rules('brocher_name', 'Brocher Name', 'required');
        if($this->form_validation->run() == true){
        	$FormData = array();  
            $FormData['brocher_name'] = $this->input->post('brocher_name');
	        if($this->input->post('status_checkbox') == '1' || $this->input->post('status_checkbox') == 1){
	        	$FormData['status']  = '1';
	        }else{
	        	$FormData['status']  = '0';
	        }
            //upload file to directory
            if($_FILES['brocher']){
	            $config['upload_path']   = 'uploaded/brochers/';
	            $config['allowed_types'] = 'pdf';
	            $config['max_size']   = 1024;
	            $config['file_name']  = rand().strtolower(str_replace(' ', '_', $this->input->post('brocher_name')));
	            $this->load->library('upload', $config);
	            if($this->upload->do_upload('brocher')){
	                $uploadData = $this->upload->data();
	                $uploadedFile = $uploadData['file_name']; 
	                $FormData['brocher_file'] = $uploadedFile;
	            	$brocher_details = $this->bdpartner_model->get_all_details(BD_BROCHERS,array('id' => $this->input->post('id')));
	            	if(file_exists("uploaded/brochers/".$brocher_details->row()->brocher_file)){
	            		unlink("uploaded/brochers/".$brocher_details->row()->brocher_file);  
	            	}
	            }
            }

            if($this->input->post('id')){
            	if($this->input->post('status_checkbox') == '1'){
					$checkMultipleActive = $this->bdpartner_model->get_all_details(BD_BROCHERS,array('id !=' => $this->input->post('id'),'status' => '1'));
					if($checkMultipleActive->num_rows() > 0){
            			$this->setErrorMessage('error','Only once brochure can be active at a time');
            			 redirect('bdadmin/setting/brochers');
            			
					}
            	}
            	$this->bdpartner_model->update_details(BD_BROCHERS,$FormData,array('id' => $this->input->post('id')));
            	$this->setErrorMessage('success','Brochure update successfully');
            }else{
            	if($this->input->post('status_checkbox') == '1'){
            		$checkMultipleActive = $this->bdpartner_model->get_all_details(BD_BROCHERS,array('status' => '1'));
            		if($checkMultipleActive->num_rows() > 0){
            			$FormData['status']  = '0';
            		}
            	}
				$this->bdpartner_model->simple_insert(BD_BROCHERS,$FormData);
				$this->setErrorMessage('success','Brochure inserted successfully');
            }            
        }
       redirect('bdadmin/setting/brochers');
    }

    public function edit_brochure(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$id  = $this->uri->segment(4,0);
			$this->data['heading'] = 'Add Brochure';
			$this->data['BrochureDetails'] = $this->bdpartner_model->get_all_details(BD_BROCHERS,array('id' => $id));
			$this->load->view('bdadmin/brocher/edit_brochure',$this->data);
		}	
    }

    public function delete_brochure(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$borchure_id = $this->uri->segment(4,0);
			$brochure = $this->bdpartner_model->get_all_details(BD_BROCHERS,array('id' => $borchure_id));
			if ($brochure->num_rows() > 0){
				if($brochure->row()->status == '0'){
					if(file_exists("uploaded/brochers/".$brochure->row()->brocher_file)){
	            		unlink("uploaded/brochers/".$brochure->row()->brocher_file);  
	            	}
					$this->bdpartner_model->commonDelete(BD_BROCHERS,array('id' => $borchure_id));
				}else{
					$this->setErrorMessage('error','Acitve brochure cannot be deleted');
				}
				redirect('bdadmin/setting/brochers');
			} else {
				redirect('bdadmin');
			}
		}		
    }

    public function app_versions(){
        if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$this->data['heading'] = 'App Versions';
			$this->data['version_list'] = $this->bdpartner_model->get_all_details(APP_VERSIONS,array());
			$this->load->view('bdadmin/adminsettings/app_version_listing',$this->data);
		}	
    }

    public function edit_app_version(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$id  = $this->uri->segment(4,0);
			$this->data['heading'] = 'Update Version';
			$this->data['button_text'] = 'Update';
			$versionRow = $this->bdpartner_model->get_all_details(APP_VERSIONS,array('id' => $id));
			// print_r($versionRow->row());exit;
			$this->data['app_version'] = $versionRow->row();
			$this->load->view('bdadmin/adminsettings/add_edit_version',$this->data);
		}	
    }

    public function add_version(){
    	if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$this->data['heading'] = 'Add Version';
			$this->data['app_version'] = array();
			$this->data['button_text'] = 'Add';
			$this->load->view('bdadmin/adminsettings/add_edit_version',$this->data);
		}		
    }
    
    public function add_update_appversions(){
    	  if ($this->checkBDLogin('A') == ''){
			redirect('bdadmin');
		} else {
			$data['app_v'] = trim($this->input->post('app_v'));
			$data['force_update'] = $this->input->post('force_update');
			if($this->input->post('id')){
				$this->bdpartner_model->update_details(APP_VERSIONS,$data,array('id' => $this->input->post('id')));
			}else{
				$this->bdpartner_model->simple_insert(APP_VERSIONS,$data);
			}
			redirect('bdadmin/setting/app_versions');
		}		
    }
    
}
?>