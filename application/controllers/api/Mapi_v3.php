<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// use \Firebase\JWT\JWT;

// require (BASE_URL.'libraries/REST_Controller.php');
/**
 * device_id,os_is,access_token  are presents in headers in all apis,Expcept login and Forgot password APIS 
 * Parameters are passed as json object with api name as key
 * All apis first validate access_token and also checks if user is blocked or not
 * All api authentication logic is in auth_model
 * All apis model function has same name as api name removing _method
 */
class Mapi_v3 extends MY_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->library(array('encrypt','form_validation','Mobile_Detect'));
        $this->load->helper(array('cookie','date','form','email','jwt','authorization'));
        $this->load->model(array('user_model','product_model','mapi_model'));
        $this->lang->load('api_lang', 'mapi');
        $c_fun = $this->uri->segment(3);
        $this->data['bd_user'] = array();
        $restricted_function = array('login','otp_verification_success','otplogin','save_sign_up','save_douments','SocailLogin','forgot_password','get_cities','check_invite_code','send_lead_submittion_notitification');
        if (!in_array($c_fun, $restricted_function)) {
           $this->data['bd_user'] =  $this->validate_token();
        }
        $this->data['baseUrl'] = base_url();
        // $this->load->helper(['jwt', 'authorization']); 
    }


 

    //Validation Function
    /*
        @params params* : array of required parametrs  
    */
    function validateRequest($params=array(),$requestedData=array()){
        $_POST = $requestedData;
        foreach($params as $key=>$val){
            $this->form_validation->set_rules($key, $val, 'trim|required');                   
        }

        if ($this->form_validation->run() == FALSE){
            return $this->form_validation->error_array();
        }else{
            return FALSE;
        }
    }


    function SocailLogin(){
        // print_r($_POST);exit;
        $_POST = $this->input->post();
        $requiredParams = array("email"=>"email");

        $isValid = $this->validateRequest($requiredParams,$_POST);
         // print_r($isValid);exit;
        
        if ($isValid)
        {            
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());

            $this->sendResponse($response,false);
            return;
        }


        $Userstatus = 'Pending';
        $query = $this->db->query("SELECT * FROM ".USERS." WHERE email='".$this->input->post('email')."' LIMIT 1");
        if($query->num_rows() > 0){
            $result['ok'] = true;
            $row = $query->result();
            $partner_query = $this->db->query("SELECT * FROM ".PARTNER_USERS." WHERE user_id='".$row[0]->id."' LIMIT 1");
            $access_token = '';

            $user_data  = array(
                'id' => $row[0]->id,
                'email' => $row[0]->email,
                'full_name' => $row[0]->full_name,
                'access_token' => $access_token
                );

            if($partner_query->num_rows() > 0){
                $partener_details = $partner_query->row();
                $access_token = AUTHORIZATION::generateToken($this->input->post('email'));
                $user_data['access_token'] = $access_token;
                $user_data['user_type'] = $query->row()->user_type;
                $user_data['status'] = $partener_details->user_status;
                $user_data['mobile'] = $partener_details->user_mobile;
                if($partener_details->profile_pic){
                    $user_data['profile_pic'] = $this->data['baseUrl'].'images/partner_app/profile_pic/'.$partener_details->profile_pic;
                }
                // if($partener_details->user_status  == 'Pending'){
                //     $response = array("message"=>$this->lang->line('profile_pending'),"status_code"=>400,"data"=>new stdclass());
                //     $this->sendResponse($response,false);
                //     return;
                // }
                $this->user_model->update_details(PARTNER_USERS,array('access_token' => $access_token),array('user_id' => $row[0]->id));
            }
            // print_r($user_data);
            $datestring = "%Y-%m-%d %h:%i:%s";
            $time = time();
            $newdata = array(
               'last_login_date' => mdate($datestring,$time),
               'last_login_ip' => $this->input->ip_address()
            );
            $condition = array('id' => $row[0]->id);
            $this->user_model->update_details(USERS,$newdata,$condition);
            $result = array('message' => 'Success','status_code' => 200, 'data' => $user_data );

        }else{
            $this->db->query(
                "INSERT INTO ".USERS." SET 
                loginUserType='".$this->input->post('login_type')."',
                full_name='".$this->input->post('first_name')." ".$this->input->post('last_name')."',
                email='".$this->input->post('email')."',
                status='Active',
                password='".md5(rand(0,10000))."',
                user_name='".$this->input->post('first_name')."".rand(1,10000)."',
                last_login_date=now(),
                created=now(),
                modified=now(),
                last_logout_date=now(),
                user_type='broker',
                birthday='".date('Y-m-d')."'"
            );
           
            $insert_id = $this->db->insert_id();
            $query = $this->db->query("SELECT * FROM ".USERS." WHERE id='".$insert_id."' LIMIT 1");
            $row = $query->result();
            $partner_query = $this->db->query("SELECT * FROM ".PARTNER_USERS." WHERE user_id='".$row[0]->id."' LIMIT 1");
            $access_token = '';
              $user_data  = array(
                'id' => $row[0]->id,
                'email' => $row[0]->email,
                'full_name' => $row[0]->full_name,
                'access_token' => $access_token
                );

            if($partner_query->num_rows() > 0){
                $partener_details = $partner_query->row();
                $access_token = AUTHORIZATION::generateToken($this->input->post('email'));
                $user_data['access_token'] = $access_token;
                $user_data['user_type'] = $query->row()->user_type;
                $user_data['status'] = $partener_details->user_status;
                $user_data['mobile'] = $partener_details->user_mobile;
                if($partener_details->profile_pic){
                    $user_data['profile_pic'] = $this->data['baseUrl'].'images/partner_app/profile_pic/'.$partener_details->profile_pic;
                }
                if($partener_details->user_status  == 'Pending'){
                    $response = array("message"=> $this->lang->line('profile_pending'),"status_code"=>400,"data"=>new stdclass());
                    $this->sendResponse($response,false);
                    return;
                }
                $this->user_model->update_details(PARTNER_USERS,array('access_token' => $access_token),array('user_id' => $row[0]->id));
            }
            $datestring = "%Y-%m-%d %h:%i:%s";
            $time = time();
            $newdata = array(
               'last_login_date' => mdate($datestring,$time),
               'last_login_ip' => $this->input->ip_address()
            );
            $condition = array('id' => $row[0]->id);
            $this->user_model->update_details(USERS,$newdata,$condition);
            $result = array('message' => $this->lang->line('success'),'status_code' => 200, 'data' => $user_data );
        }
        
        $this->sendResponse($result,false);

    }

    /**
     * Validate form inputs
     * This function validates form inputs data
     * @param array params   
    **/
    function validateFormData($params=array(),$requestedData=array()){
        $_POST = $requestedData;
        foreach($params as $key=>$val){
            $this->form_validation->set_rules($key, $key, $val);                   
        }

        if ($this->form_validation->run() == FALSE){
            return $this->form_validation->error_array();
        }else{
            return FALSE;
        }
    }

    /*
        General Functions statrs
    */
    private function encryptData($data){
        $encryptedData = $this->encryption->encrypt($data);
        
        return $encryptedData;
    }

    private function decryptData($data){
        
        $decryptedData = $this->encryption->decrypt($data);
        
        return json_decode($decryptedData,1);
    }
    /**
        * This function creates response send to the client
        * @param array|stdclass $data
        * @param bool $doEncrypt
    **/


    public function login(){
        $_POST = $this->input->post();
        
        $requiredParams = array("email"=>"Email","password" => "Password");

        $isValid = $this->validateRequest($requiredParams,$_POST);
        
        if ($isValid)
        {    
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());

            $this->sendResponse($response,false);
            return;   
        }
        
        $Userstatus = 'Pending';
        $query = $this->db->query("SELECT * FROM ".USERS." WHERE email='".$this->input->post('email')."' LIMIT 1");
            if($query->num_rows() > 0){
                // print_r("expression");exit;
                $row_data = $query->row();
                $new_password =  md5($this->input->post('password'));
                if($new_password == $row_data->password){
                    $access_token = '';
                    $partner_query = $this->db->query("SELECT * FROM ".PARTNER_USERS." WHERE user_id='".$row_data->id."' LIMIT 1");
                    $user_data  = array(
                        'id' => $row_data->id,
                        'email' => $row_data->email,
                        'full_name' => $row_data->full_name,
                        'access_token' => $access_token,
                        'is_profile_updated' => false,
                        'is_invite_code' => false
                    );
                    $CheckInviteCode = $this->mapi_model->get_all_details(BD_INVITE_CODE,array('user_id' => $row_data->id));
                        if($CheckInviteCode->num_rows() > 0){
                            $user_data['is_invite_code'] = true;
                        }
                    if($partner_query->num_rows() > 0){
                        $partener_details = $partner_query->row();
                        $access_token = AUTHORIZATION::generateToken($this->input->post('email').mt_rand());
                        $user_data['access_token'] = $access_token;
                        $user_data['user_type'] = $row_data->user_type;
                        $user_data['status'] = $partener_details->user_status;
                        $user_data['mobile'] = $partener_details->user_mobile;
                        if($partener_details->profile_pic){
                            $user_data['profile_pic'] = $this->data['baseUrl'].'images/partner_app/profile_pic/'.$partener_details->profile_pic;
                        }

                        if($row_data != 'BD Executive' && $partener_details->poi != '' || $partener_details->bank_details != ''){
                            $user_data['is_profile_updated'] = true;
                        }

                     
                        
                        // if($partener_details->user_status  == 'Pending'){
                        //     $response = array("message"=>$this->lang->line('profile_pending'),"status_code"=>400,"data"=>new stdclass());
                        //     $this->sendResponse($response,false);
                        //     return;
                        // }
                        $this->user_model->update_details(PARTNER_USERS,array('access_token' => $access_token,'device_token' => $this->input->post('device_token')),array('user_id' => $row_data->id));
                    }
                    $result = array('message' => $this->lang->line('login_success'),'status_code' => 200, 'data' => $user_data );
                              
                }else{
                    $user_data  = array(
                        'email' => $_POST['email'],
                        'password' => $_POST['password']
                    );
                    $result = array('message' => $this->lang->line('text_invalid_creds'),'status_code' => 400, 'data' => $user_data );
                }
            }else{
                $user_data  = array(
                        'email' => $_POST['email'],
                        'password' => $_POST['password']
                    );
                $result = array('message' => $this->lang->line('invalid_user'),'status_code' => 400, 'data' => $user_data );
               
            }
        
        $this->sendResponse($result,false);
    }

    // check invite code 
    public function check_invite_code(){
        $_POST = $this->input->post();
        $requiredParams = array("invite_code" => "Invite Code");
        $isValid = $this->validateRequest($requiredParams,$_POST);
        if ($isValid)
        {    
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;   
        } 

        $CheckInviteCode = $this->mapi_model->get_all_details(PARTNER_USERS,array('bd_invite_code' => $this->input->post('invite_code')));
        if($CheckInviteCode->num_rows() == 0){
            $response = array("message"=>$this->lang->line('invalid_invite_code'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;               
        }else{
            $response = array("message"=>$this->lang->line('invalid_code_valid'),"status_code"=>200,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return; 
        }
    }



    public function checkUserExists(){
        $_POST = $this->input->post();

        $requiredParams = array("email" => "email");

        $isValid = $this->validateRequest($requiredParams,$_POST);
        
        if ($isValid)
        {    
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());

            $this->sendResponse($response,false);
            return;   
        }

        $query = $this->db->query("SELECT * FROM ".USERS." WHERE email='".$this->input->post('email')."' LIMIT 1");
            if($query->num_rows() > 0){
                $user_data  = array(
                        'email' => $_POST['email'],
                );
                $result = array('message' => $this->lang->line('user_exists'),'status_code' => 400, 'data' => $user_data );
                // print_r("user elready exists");exit;
            }else{
                  $result = array('message' =>  $this->lang->line('sing_up_success'),'status_code' => 200, 'data' => array() );
            }
        $this->sendResponse($result,false);
    }


    public function save_sign_up(){
        $_POST = $this->input->post();
        if($_POST['is_sing_up'] == 'yes'){
            $requiredParams = array("email" => "email","password"=>"Password");
        }else{
            $requiredParams = array("full_name"=>"Full Name","email" => "email","password"=>"Password","mobile_number" => "Mobile Number");
        }

        $isValid = $this->validateRequest($requiredParams,$_POST);
        if ($isValid)
        {    
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;   
        }

        if($_POST['is_sing_up'] == 'yes'){
            $checkNormalUser = $this->mapi_model->get_all_details(USERS,array('email' => $_POST['email'],'password' => md5($_POST['password'])));
            if($checkNormalUser->num_rows() > 0){
                $CheckInviteCode = $this->mapi_model->get_all_details(PARTNER_USERS,array('bd_invite_code' => $_POST['bd_invite_code']));
                if($CheckInviteCode->num_rows() == 0){
                    $response = array("message"=>$this->lang->line('invalid_invite_code'),"status_code"=>400,"data"=>new stdclass());
                    $this->sendResponse($response,false);
                    return;               
                }else{
                    $this->mapi_model->update_details(USERS,array('user_type' => $_POST['user_type']),array('id' => $checkNormalUser->row()->id ));
                    $this->mapi_model->simple_insert(BD_INVITE_CODE,array('invite_code' => $_POST['bd_invite_code']),array('user_id' => 
                        $checkNormalUser->row()->id));
                    $this->send_registration_mail($last_insert_id); 
                    $user_data  = array(
                            'user_id' => $checkNormalUser->row()->id,
                            'user_type' => $_POST['user_type']
                    );
                    $result = array('message' => $this->lang->line('registration_success'),'status_code' => 200, "data"=> $user_data );
                    $this->sendResponse($result,false);
                    return;
                }
            }else{
                $response = array("message"=>$this->lang->line('text_invalid_creds'),"status_code"=>400,"data"=> new stdclass());
                $this->sendResponse($response,false);
                return;   
            }
        }

        $data = $this->mapi_model->check_details_exists($_POST);
        if($data->num_rows() > 0){
            $response = array("message"=>$this->lang->line('email_phone_exists'),"status_code"=>400,"data"=> new stdclass());
            $this->sendResponse($response,false);
            return;   
        }else{
            $CheckInviteCode = $this->mapi_model->get_all_details(PARTNER_USERS,array('bd_invite_code' => $_POST['bd_invite_code']));
            if($CheckInviteCode->num_rows() == 0){
                $response = array("message"=>$this->lang->line('invalid_invite_code'),"status_code"=>400,"data"=>new stdclass());
                $this->sendResponse($response,false);
                return;               
            }else{
                $response =  $this->db->query(
                        "INSERT INTO ".USERS." SET loginUserType='',
                        full_name='".$this->input->post('full_name')."',
                        email='".$this->input->post('email')."',
                        status='Active',
                        password='".md5($this->input->post('password'))."',
                        user_name='".$this->input->post('full_name')."',
                        last_login_date=now(),
                        city='".$this->input->post('broker_city')."',
                        created=now(),
                        modified=now(),
                        last_logout_date=now(),
                        birthday='".date('Y-m-d')."',
                        phone_no='".$this->input->post('mobile_number')."'"
                );

                if($response){
                    $last_insert_id  =  $this->db->insert_id();
                    $user_data  = array(
                            'user_id' => $last_insert_id,
                            'is_invite_code' => true
                    );
                    $this->mapi_model->simple_insert(BD_INVITE_CODE,array('invite_code' => $_POST['bd_invite_code'],'user_id' => $last_insert_id));
                    $this->send_registration_mail($last_insert_id);
                    $result = array('message' => $this->lang->line('registration_success'),'status_code' => 200, "data"=> $user_data );
                }else{
                    $result = array('message' => $this->lang->line('something_wrong') ,'status_code' => 400, 'data' => new stdclass());
                }
                $this->sendResponse($result,false);
                return;
            }
        }
    }

    public function send_registration_mail($user_id){
        $userDetails = $this->mapi_model->get_all_details(USERS,array('id' => $user_id)); 
        $uid = $userDetails->row()->id;
        $email = $userDetails->row()->email;

        $newsid = '25';
        $template_values=$this->mapi_model->get_newsletter_template($newsid);
        $subject = 'From: '.$this->config->item('email_title').' - '.$template_values['news_subject'];
        $searchArray = array("%user_name", "%dynamic_data");
        $replaceArray = array($userDetails->row()->full_name, ' ');
        $new_message .=  str_replace($searchArray, $replaceArray ,$template_values['news_descrip'] );
        $message .= '<!DOCTYPE HTML>
            <html>
            <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            
            <meta name="viewport" content="width=device-width"/><body>';
        $message = $new_message;

        $message .= '</body>
            </html>';
        if($template_values['sender_name']=='' && $template_values['sender_email']==''){
            $sender_email=$this->data['siteContactMail'];
            $sender_name=$this->data['siteTitle'];
        }else{
            $sender_name=$template_values['sender_name'];
            $sender_email=$template_values['sender_email'];
        }

        $email_values = array('mail_type'=>'html',
                            'from_mail_id'=>$sender_email,
                            'mail_name'=>$sender_name,
                            'to_mail_id'=>$email,
                            'subject_message'=> $template_values['news_subject'],
                            'body_messages'=>$message,
                            'mail_id'=>'register mail'
                            );
        $email_send_to_common = $this->product_model->common_email_send($email_values); 

        $mobile_number = '91'.$userDetails->row()->phone_no;
        $otp = rand (1000,9999);
        $fields = [
            'apikey' => SMS_KEY,
            'msg' => "Welcome ".$userDetails->row()->full_name." to Cityfurnish Partner app. Thanks for signing up.",
            'sid' => 'CITYFN',
            'msisdn' => $mobile_number,
            'fl' => 0
        ];
        $this->load->helper('sms');
        $response = $this->send_sms($fields);
    }

       //Update user password 
    public function change_password(){
        $_POST = $this->input->post();
        $requiredParams = array("old_password"=>"Old password","new_password" => "New Password");
        $isValid = $this->validateRequest($requiredParams,$_POST);
        if ($isValid)
        {    
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;   
        }
        $userDetails = $this->mapi_model->get_all_details(USERS,array('id' => $this->data['bd_user']->user_id));
        if($userDetails->num_rows() > 0){
            if(md5($_POST['old_password']) == $userDetails->row()->password){
                $this->mapi_model->update_details(USERS,array('password' => md5($_POST['new_password'])),array('id' => $this->data['bd_user']->user_id));
                $response = array("message" => $this->lang->line('password_change_success') , "status_code" => 200 , "data" => new stdclass());
                $this->sendResponse($response,false);
                return;  
            }else{
                $response = array("message"=> $this->lang->line('old_password_not_match'),"status_code"=>400,"data"=>new stdclass());
                $this->sendResponse($response,false);
                return;          
            } 
        }
    }

    public function save_douments(){
        $_POST = $this->input->post();
        $requiredParams = array("user_id"=>"User ID","user_type" => "User Type");

        $isValid = $this->validateRequest($requiredParams,$_POST);

        if ($isValid)
        {
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());

            $this->sendResponse($response,false);
            return;
        }
         $query = $this->db->query("SELECT * FROM ".PARTNER_USERS." WHERE user_id='".$_POST['user_id']."' LIMIT 1");
            if($query->num_rows() > 0){
                $user_data  = array('user_id' => $_POST['user_id']);
                $result = array('message' => $this->lang->line('user_exists'),'status_code' => 400, 'data' => $user_data );
            }else {
                

                $query = $this->db->query("SELECT * FROM ".USERS." WHERE id='".$_POST['user_id']."' LIMIT 1");
                if($query->num_rows() == 0){
                    $user_data  = array('user_id' => $_POST['user_id']);
                    $result = array('message' => $this->lang->line('text_user_not_found'),'status_code' => 400, 'data' => $user_data );
                    $this->sendResponse($result,false);
                    return;
                }

                
                if($_POST['invite_code']){
                    $CheckInviteCode = $this->mapi_model->get_all_details(PARTNER_USERS,array('bd_invite_code' => $_POST['invite_code']));
                    if($CheckInviteCode->num_rows() == 0){
                        $response = array("message"=>$this->lang->line('invalid_invite_code'),"status_code"=>400,"data"=>new stdclass());
                        $this->sendResponse($response,false);
                        return;   
                    }else{
                        $this->mapi_model->simple_insert(BD_INVITE_CODE,array('invite_code' => $_POST['invite_code'] ,'user_id' => $_POST['user_id'] ));
                    }
                }

                if(isset($_FILES['poi'])){
                    $file = $this->uploadDocuments(count($_FILES['poi']['name']),'poi',$_POST['user_id'].rand(1111, 9999),'./images/partner_app/poi');
                    $insert_data['poi'] = (!empty($file)) ? implode(",", $file) : '';
                }

                if(isset($_FILES['visiting_card'])){
                    $file = $this->uploadDocuments(count($_FILES['visiting_card']['name']),'visiting_card',$_POST['user_id'].rand(1111, 9999),'./images/partner_app/visiting_card');
                    $insert_data['visiting_card'] = (!empty($file)) ? implode(",", $file) : '';
                }

                if(isset($_FILES['profile_pic'])){
                    $insert_data['profile_pic'] = $this->uploadDocuments(count($_FILES['profile_pic']['name']),'profile_pic',$_POST['user_id'].rand(111, 999),'./images/partner_app/profile_pic');
                }

                if(isset($_FILES['cancelled_cheque'])){
                    $insert_data['cancelled_cheque'] = $this->uploadDocuments(count($_FILES['cancelled_cheque']['name']),'cancelled_cheque',$_POST['user_id'].rand(111, 999),'./images/partner_app/cancelled_cheque');
                }

                // Create a token
                $token = AUTHORIZATION::generateToken($_POST['user_id']);
                
                $InviteCode =  $this->mapi_model->get_all_details(BD_INVITE_CODE,array('user_id' => $_POST['user_id']));
                if($InviteCode->num_rows() > 0){
                    $BdexecutiveDetails = $this->mapi_model->get_all_details(PARTNER_USERS,array('bd_invite_code' => $InviteCode->row()->invite_code));
                    if($BdexecutiveDetails->num_rows() > 0){
                        $insert_data['bdexecuter_id'] = $BdexecutiveDetails->row()->user_id;                    
                    }
                }

                $insert_data['user_status'] = 'Pending';
                $insert_data['access_token'] = $token;
                $insert_data['user_id'] = $_POST['user_id'];
                $insert_data['bank_details'] = $_POST['bank_details'];
                // $insert_data['pan_card_number'] = $_POST['pan_card_number'];
                $insert_data['employee_id'] = $_POST['employee_id'];
                $excludeArr = array("email","password","full_name","last_name");

                $this->user_model->update_details(USERS,array('user_type' => $_POST['user_type']),array('id' => $_POST['user_id']));  
                $insert_data = $this->user_model->simple_insert(PARTNER_USERS,$insert_data);  
                if($insert_data){
                    $user_data  = array(
                        'id' => $_POST['user_id'],
                        'email' => $_POST['email'],
                        'full_name' => $_POST['full_name'],
                        'status' => 'Pending',
                        'access_token' => $token
                    );
                    $result = array('message' => $this->lang->line('registration_success'),'status_code' => 200, 'data' => $user_data );
                }else{
                    $user_data  = array(
                        'email' => $_POST['email'],
                    );
                    $result = array('message' => $this->lang->line('something_wrong'),'status_code' => 400, 'data' => $user_data );

                }
            }
        $this->sendResponse($result,false);
    }



    //Update user profile
    public function update_profile(){
        if(!empty($this->data['bd_user'])){
            $query = $this->db->query("SELECT * FROM ".USERS." WHERE id='".$this->data['bd_user']->user_id."' LIMIT 1");
            if($query->num_rows() == 0){
                $user_data  = array(
                        'user_id' => $this->data['bd_user']->user_id,
                );
                $result = array('message' => $this->lang->line('text_user_not_found'),'status_code' => 400, 'data' => $user_data );       
                $this->sendResponse($result,false);
                return;                 
            }
            if(isset($_FILES['poi'])){
                $file = $this->uploadDocuments(count($_FILES['poi']['name']),'poi',$this->data['bd_user']->user_id.rand(1111, 9999),'./images/partner_app/poi');
                $update_data['poi'] = (!empty($file)) ? implode(",", $file) : '';
            }

            if(isset($_FILES['visiting_card'])){
                $file = $this->uploadDocuments(count($_FILES['visiting_card']['name']),'visiting_card',$this->data['bd_user']->user_id.rand(1111, 9999),'./images/partner_app/visiting_card');
                $update_data['visiting_card'] = (!empty($file)) ? implode(",", $file) : '';
            }

            if(isset($_FILES['profile_pic'])){
                $update_data['profile_pic'] = $this->uploadDocuments(count($_FILES['profile_pic']['name']),'profile_pic',$this->data['bd_user']->user_id.rand(111, 999),'./images/partner_app/profile_pic');
            }

            if(isset($_FILES['cancelled_cheque'])){
                $update_data['cancelled_cheque'] = $this->uploadDocuments(count($_FILES['cancelled_cheque']['name']),'cancelled_cheque',$this->data['bd_user']->user_id.rand(111, 999),'./images/partner_app/cancelled_cheque');
            }

            if(isset($_POST['bank_details'])){
                 $update_data['bank_details'] = $_POST['bank_details'];
            }
            if(!empty($update_data)){
                $this->user_model->update_details(PARTNER_USERS,$update_data,array('user_id' => $this->data['bd_user']->user_id)); 
            }

            // $this->user_model->update_details(PARTNER_USERS,$update_data,array('user_id' => $this->data['bd_user']->user_id)); 
            $result = array('message' => $this->lang->line('profile_update_success'),'status_code' => 200, 'data' => new stdclass());
            $this->sendResponse($result,false);
            return;
        }else{
            $response = array("message"=> $this->lang->line('something_wrong'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;      
        }
    }

    //get offer listin
    public function get_offers(){
        $all_offers = $this->mapi_model->get_all_details(BD_OFFERS,array());
        if($all_offers->num_rows() > 0){
            $result = array('message' => $this->lang->line('offer_listing'),'status_code' => 200, 'data' => $all_offers->result());
            $this->sendResponse($result,false);
            return;
        }else{
            $response = array("message"=> $this->lang->line('no_offer_found'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;   
        }
    }

    public function otplogin(){
        $_POST = $this->input->post();
        $requiredParams = array("mobile_number"=>"Mobile Number");

        $isValid = $this->validateRequest($requiredParams,$_POST);
        
        if ($isValid)
        {    
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());

            $this->sendResponse($response,false);
            return;   
        }
        // $partner_data = $this->mapi_model->get_user_details(array('user.phone_no'=>$this->input->post('mobile_number'),'user.user_type !=' => ''));
        $Userstatus = 'Pending';
        $query = $this->db->query("SELECT * FROM ".USERS." WHERE phone_no='".$this->input->post('mobile_number')."' LIMIT 1");

        if($query->num_rows() > 0){
            $mobile_number = '91'.$this->input->post('mobile_number');
            $otp = rand (1000,9999);
            // $fields = [
            //     'apikey' => SMS_KEY,
            //     'msg' => "Your Verification Code From CityFurnish is ".$otp.".",
            //     'sid' => 'CITYFN',
            //     'msisdn' => $mobile_number,
            //     'fl' => 0,
            //     'gwid' => 2
            // ];
            // $this->load->helper('sms');
            // $response = $this->send_sms($fields);
            
            $fields = [
                'username' => VALUE_FIRST_USER_NAME,
                'password' => VALUE_FIRST_PASSWORD,
                'text' => "Your Verification Code From CityFurnish is ".$otp.".",
                'to' => $mobile_number,
                'from' => VALUE_FIRST_FROM,
            ];
            $response = $this->send_sms_new($fields);
            $user_data = array(
                        'mobile_number' => $mobile_number
                        // 'otp' => $otp
                    );
            if ($response) {
                $check_otp_exists = $this->mapi_model->get_all_details(APP_OTP,array('mobile_number' => $mobile_number ));
                if($check_otp_exists->num_rows() > 0){
                    $this->mapi_model->update_details(APP_OTP,array('otp' => $otp ),array('mobile_number' => $mobile_number));    
                }else{
                    $this->mapi_model->simple_insert(APP_OTP,array('mobile_number' => $mobile_number,'otp' => $otp ));
                }
               // $_SESSION['partner_otp'] = $otp;
               $result = array('message' => $this->lang->line('otp_success'),'status_code' => 200, 'data' => $user_data );
            }else{
                $result = array('message' => $this->lang->line('something_wrong'),'status_code' => 400, 'data' => array() );
            }
            $this->sendResponse($result,false);
        }else{
            $user_data  = array(
                    'mobile_number' => $this->input->post('mobile_number'),
            );
            $response = array('message' => $this->lang->line('text_user_not_found'),'status_code' => 400, 'data' => $user_data );
            $this->sendResponse($response,false);
            return;            
        }

    }


      //verify user input otp 
    public function otp_verification_success(){
        $_POST = $this->input->post();
        $requiredParams = array("mobile_number"=>"Mobile Number",'otp' => 'OTP');

        $isValid = $this->validateRequest($requiredParams,$_POST);
        
        if ($isValid)
        {    
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());

            $this->sendResponse($response,false);
            return;   
        }

        $check_otp_exists = $this->mapi_model->get_all_details(APP_OTP,array('mobile_number' => '91'.$_POST['mobile_number'] ));
        if($check_otp_exists->num_rows() == 0){
            $response = array("message"=> $this->lang->line('Invalid_otp'),"status_code"=>400,"data" => new stdclass());
            $this->sendResponse($response,false);
            return;     
        }else{
            if($check_otp_exists->row()->otp != $this->input->post('otp')){
                $response = array("message"=> $this->lang->line('Invalid_otp'),"status_code"=>400,"data" => new stdclass());
                $this->sendResponse($response,false);
                return;      
            }else{
                $partner_data = $this->mapi_model->get_user_details(array('user.phone_no'=>$this->input->post('mobile_number'),'user.user_type !=' => ''));
                if($partner_data->num_rows() > 0){
                    $token = '';
                    $partner_user = $partner_data->row();
                    $user_data  = array(
                        'id' =>  $partner_user->id,
                        'email' => $partner_user->email,
                        'full_name' =>  $partner_user->full_name,
                        'access_token' => $token,
                        'is_profile_updated' => false,
                        'is_invite_code' => false
                    );
                    $partner_query = $this->db->query("SELECT * FROM ".PARTNER_USERS." WHERE user_id='".$partner_user->id."' LIMIT 1");
                    if($partner_query->num_rows() > 0){;
                        $partener_details = $partner_query->row();
                        $token = AUTHORIZATION::generateToken($partner_user->email.mt_rand());
                        $user_data['access_token'] = $token;
                        $user_data['user_type'] = $partner_user->user_type;
                        $user_data['status'] = $partener_details->user_status;
                        $user_data['mobile'] = $partner_user->phone_no;
                        if($partener_details->profile_pic){
                            $user_data['profile_pic'] = $this->data['baseUrl'].'images/partner_app/profile_pic/'.$partener_details->profile_pic;
                        }
                        
                        if($partner_data->user_type != 'BD Executive' && $partener_details->poi != '' || $partener_details->bank_details != ''){
                            $user_data['is_profile_updated'] = true;
                        }

                        $CheckInviteCode = $this->mapi_model->get_all_details(BD_INVITE_CODE,array('user_id' =>  $partner_user->id));
                        if($CheckInviteCode->num_rows() > 0){
                            $user_data['is_invite_code'] = true;
                        }
                        // if($check_status->user_status  == 'Pending'){
                        //     $response = array("message"=> $this->lang->line('profile_pending'),"status_code"=>400,"data"=>new stdclass());
                        //     $this->sendResponse($response,false);
                        //     return;
                        // }
                        $this->user_model->update_details(PARTNER_USERS,array('access_token' => $token,'device_token' => $this->input->post('device_token')),array('user_id' => $partner_user->id));
                    }else{
                        $result = array('message' => $this->lang->line('text_user_not_found'),'status_code' => 400, 'data' =>new stdclass() );
                        $this->sendResponse($response,false);
                        return;
                    }
                  
            
                    $result = array('message' => $this->lang->line('registration_success'),'status_code' => 200, 'data' => $user_data );
                }
                else{
                    // $partner_user = $partner_data->row();
                    // $user_data  = array(
                    //     'id' => $partner_user->id,
                    //     'email' => $partner_user->email,
                    //     'full_name' => $partner_user->full_name,
                    //     'status' => 'Pending',
                    //     'access_token' => '',
                    //     'mobile'=> $this->input->post('mobile_number')

                    // );
                    $result = array('message' => $this->lang->line('text_user_not_found'),'status_code' => 400, 'data' =>new stdclass() );
                }
                $this->sendResponse($result,false);
            }
        }
    }


    public function forgot_password(){
        $_POST = $this->input->post();
        $requiredParams = array("email"=>"Email");
        $isValid = $this->validateRequest($requiredParams,$_POST);
        if ($isValid)
        {    
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());

            $this->sendResponse($response,false);
            return;   
        }else{
             $query = $this->db->query("SELECT * FROM ".USERS." WHERE email='".$this->input->post('email')."' LIMIT 1");
            if($query->num_rows() > 0){
                $token = mt_rand();
                $email = $this->input->post('email');
                $data['token'] = $token;
                $data['check_current_time'] =   date("Y-m-d h:i:s");
                $result =  $this->user_model->store_token_date($data,$email);
                $get_token = $this->user_model->get_token($email);
                    $link = base_url()."reset-password/".$get_token[0]->token; 
                    $check = '<a href='.$link.' style="display:inline-block;text-decoration:none;font-weight:bold;margin-top:30px;">
                <img src="http://180.211.99.165/design/cityfurnish/email/images/reset-pass-btn.png" />
                </a>';
                $response = $this->send_user_password($check,$query);
                $result = array('message' => $this->lang->line('reset_password_link_success'),'status_code' => 200);

                
            }else{
                $user_data  = array(
                        'email' => $_POST['email'],
                    );
                $result = array('message' => $this->lang->line('text_user_not_found'),'status_code' => 400, 'user_data' => $user_data );
                
            }

        }
        $this->sendResponse($result,false);

    }

    public function send_user_password($pwd='',$query){
        $newsid='5';
        $template_values=$this->user_model->get_newsletter_template_details($newsid);

        $adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo']);

        extract($adminnewstemplateArr);
        $subject = 'From: '.$this->config->item('email_title').' - '.$template_values['news_subject'];
        $registeration =   file_get_contents('./newsletter/fogot_password.html');
        
        $message .=  str_replace('dynamicforgpasslink', $pwd,$registeration );
        if($template_values['sender_name']=='' && $template_values['sender_email']==''){
            $sender_email=$this->config->item('site_contact_mail');
            $sender_name=$this->config->item('email_title');
        }else{
            $sender_name=$template_values['sender_name'];
            $sender_email=$template_values['sender_email'];
        }
        
        $email_values = array('mail_type'=>'html',
                            'from_mail_id'=>$sender_email,
                            'mail_name'=>$sender_name,
                            'to_mail_id'=>$query->row()->email,
                            'subject_message'=>'Password Reset link',
                            'body_messages'=>$message,
                            'mail_id'=>'forgot',
                            'dynamicforgpasslink' => $pwd
                            );
            $email_send_to_common = $this->product_model->common_email_send($email_values);
                            
    }




    public function uploadDocuments($count = 1,$file,$file_name,$path){
        $config['overwrite'] = FALSE;
        $config['allowed_types'] = 'jpg|jpeg|gif|png';
        $config['max_size'] = 2000;
        $config['upload_path'] = $path;
        $config['file_name'] = $file_name; 
        $this->load->library('upload', $config);
        if (!is_dir($path)) {
            mkdir($path);
        }

        if($file == 'poi' || $file == 'visiting_card'){
            $ImageName = array();
            $logoDetails = $this->do_upload_multiple_files($file,$config);
            foreach ($logoDetails as $fileDetails){
                $ImageName[] = $fileDetails['file_name'];
            }
        }else{
            $this->upload->initialize($config);
            if($this->upload->do_upload($file)){
                $file = $this->upload->data();
                $ImageName = $file['file_name'];
            }   
        }        
        return $ImageName;
    }



    public function do_upload_multiple_files($fieldName, $options)
    {
        $response = array();
        $files = $_FILES;
        if (!is_dir($options[0]['upload_path'])) {
            mkdir($options[0]['upload_path']);
        }
        $cpt = count($_FILES[$fieldName]['name']);
        for ($i=0; $i<$cpt; $i++) {
            $_FILES[$fieldName]['name']= $files[$fieldName]['name'][$i];
            $_FILES[$fieldName]['type']= $files[$fieldName]['type'][$i];
            $_FILES[$fieldName]['tmp_name']= $files[$fieldName]['tmp_name'][$i];
            $_FILES[$fieldName]['error']= $files[$fieldName]['error'][$i];
            $_FILES[$fieldName]['size']= $files[$fieldName]['size'][$i];
    
            $this->load->library('upload');
            $this->upload->initialize($options);
    
            //upload the image
            if (!$this->upload->do_upload($fieldName)) {
                $response['errors'] = $this->upload->display_errors();
            } else {
                $response[] = $this->upload->data();
            }
        }
    
        return $response;
    }


    public function send_sms($fields){
        unset($fields['apikey']);
        $curl = curl_init();
        $endpoint = 'http://www.smslane.com/vendorsms/pushsms.aspx';
        $url = $endpoint . '?' . http_build_query($fields);
        $url .= '&apikey=uBM6NMuIJFRj3qEQ8Jqx%2b2Aj%2b0mLrg5fe2zZ83keNuw%3d';
        curl_setopt($curl, CURLOPT_URL,  $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        // print_r($response);exit;
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
          return 0;
        } else {
          return 1;
        }
  }
  
    public function send_sms_new($fields){
        $curl = curl_init();
        $endpoint = 'http://www.myvaluefirst.com/smpp/sendsms';
        $url = $endpoint . '?' . http_build_query($fields);
       
        curl_setopt($curl, CURLOPT_URL,  $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        print_r($response);exit;
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
          return 0;
        } else {
          return 1;
        }
    }

    // Sibmit a new lead request 
    public function captured_new_lead(){
        $_POST = $this->input->post();
        $requiredParams = array("phone_no"=>"Mobile Number");
        $isValid = $this->validateRequest($requiredParams,$_POST);
        if ($isValid)
        {    
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;   
        }
        
        $this->form_validation->set_rules('email', 'Email Field', 'valid_email');
        if ($this->form_validation->run() == FALSE)
        {
            $response = array("message"=>'Pleae enter valid email',"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;   
            
        }
        
        $CheckDuplicate = $this->mapi_model->check_lead_exists(BD_LEADS,$_POST['email'],$_POST['phone_no']);
        if($CheckDuplicate->num_rows() > 0){
            $Lead_info = $CheckDuplicate->row();
            if($Lead_info->email == $_POST['email']){
                $result = array('message' =>$this->lang->line('email_exists'),'status_code' => 400, 'data' =>array() );
            }else {
                $result = array('message' => $this->lang->line('mobile_exists'),'status_code' => 400, 'data' =>array() );
            }
            $this->sendResponse($result,false);
            return;
        }else{
            $CheckDuplicate = $this->mapi_model->check_lead_exists(USERS,$_POST['email'],$_POST['phone_no']);
            if($CheckDuplicate->num_rows() > 0){
                $Lead_info = $CheckDuplicate->row();
                if($Lead_info->email == $_POST['email']){
                    $result = array('message' =>$this->lang->line('email_exists'),'status_code' => 400, 'data' =>array() );
                }else {
                    $result = array('message' => $this->lang->line('mobile_exists'),'status_code' => 400, 'data' =>array() );
                }
                $this->sendResponse($result,false);
                return;
            }
        }
        
        if($_POST['broker_user_id']){
            $get_rm_details = $this->mapi_model->get_all_details(PARTNER_USERS,array('user_id' => $_POST['broker_user_id']));
            $broker_details = $this->mapi_model->get_all_details(USERS,array('id' => $get_rm_details->row()->bdexecuter_id));
            $dataArr = array('bd_user_id' => $_POST['broker_user_id']);   
            $excludeArr = array('broker_user_id'); 
        }else{
            $get_rm_details = $this->mapi_model->get_all_details(PARTNER_USERS,array('user_id' => $this->data['bd_user']->user_id ));
            $broker_details = $this->mapi_model->get_all_details(USERS,array('id' => $get_rm_details->row()->bdexecuter_id));
            $dataArr = array('bd_user_id' => $this->data['bd_user']->user_id);
        }
        $RmID = $broker_details->row()->id;
        // print_r($broker_details->row());exit;

        $response = $this->product_model->commonInsertUpdate(BD_LEADS,'insert',$excludeArr,$dataArr);
        if ($response) {
            if($broker_details->num_rows() > 0){
                $this->send_lead_submittion_notitification(ucfirst($broker_details->row()->full_name),$broker_details->row()->phone_no,$_POST['phone_no']);
                $this->send_app_notification($RmID,$_POST['first_name'].' '.$_POST['last_name'],$_POST['phone_no'],$dataArr);
            }
            $result = array('message' => $this->lang->line('lead_submit_success'),'status_code' => 200, 'data' =>array() );
        }else{
            $result = array('message' => $this->lang->line('something_wrong'),'status_code' => 400, 'data' => array() );
        }
        $this->sendResponse($result,false);
    }

    public function send_app_notification($rm_id,$lead_name,$lead_mobile_number,$broker_id){
        $get_rm_details = $this->mapi_model->get_all_details(PARTNER_USERS,array('user_id' => $rm_id));
        $broker_details = $this->mapi_model->get_all_details(USERS,array('id' => $broker_id['bd_user_id']));
        $msg = ucfirst($broker_details->row()->full_name).' have added a new lead where lead name is '.ucfirst($lead_name).' and mobile number is '.$lead_mobile_number;
        $this->mapi_model->send_app_notification($get_rm_details->row()->device_token,$msg);
        $this->mapi_model->simple_insert(BD_NOTIFICATION, array('user_id' => $get_rm_details->row()->user_id,'message' =>$msg ));
    }


    public function send_lead_submittion_notitification($bdname = 'NA',$mobile = 'NA',$customer_mobile){
        $msg = 'You can further reach out to '.$bdname.' on mobile no. '.$mobile.' for anything related to renting furniture, appliances, and Fitness Equipment\'s from Cityfurnish.com';
        $mobile_number = '91'.$customer_mobile;
        $fields = [
            'apikey' => SMS_KEY,
            'msg' => $msg,
            'sid' => 'CITYFN',
            'msisdn' => $mobile_number,
            'fl' => 0
        ];
        $this->load->helper('sms');
        $response = $this->send_sms($fields);  
    }

    // Get lead listing 
    public function get_leads(){
        $_POST = $this->input->post();
        $requiredParams = array("offset"=>"Offset","status" => "Status");
        $isValid = $this->validateRequest($requiredParams,$_POST);
        if ($isValid)
        {           
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;   
        }
        $is_bd_login = null;
        $check_login_is_bd = $this->mapi_model->get_all_details(USERS,array('id' => $this->data['bd_user']->user_id, 'user_type' => 'BD Executive'));
        if($check_login_is_bd->num_rows() > 0){
            $is_bd_login =  $this->data['bd_user']->user_id;
        }
        if($this->input->post('user_id') != ''){
            $leadsCount = $this->mapi_model->get_leads('count',$_POST['limit'],$_POST['offset'],array('bd_user_id' => $this->input->post('user_id')),$_POST['status'],$_POST['search_value']);
            $leads = $this->mapi_model->get_leads(null,$_POST['limit'],$_POST['offset'],array('bd_user_id' => $this->input->post('user_id')),$_POST['status'],$_POST['search_value']);
            $get_lead_owner_name = $this->mapi_model->get_all_details(USERS,array('id' => $this->input->post('user_id')));
            foreach($leads as $value) {
                $value->bd_user_id = $get_lead_owner_name->row()->full_name;
            }
            // $result = array('message' => $this->lang->line('lead_listing'),'status_code' => 200, 'data' =>$leads );
            // $this->sendResponse($result,false);
            // return;
            $NewData = array('lead_count' => $leadsCount[0]->count,'leads' => $leads);
            $result = array('message' => $this->lang->line('lead_listing'),'status_code' => 200, 'data' =>$NewData );
            $this->sendResponse($result,false);
            return;
        }else{
            if(!empty($this->data['bd_user'])){
                $leadsCount = $this->mapi_model->get_leads('count',$_POST['limit'],$_POST['offset'],array('bd_user_id' => $this->data['bd_user']->user_id ),$_POST['status'],$_POST['search_value'],$is_bd_login);
                $leads = $this->mapi_model->get_leads(null,$_POST['limit'],$_POST['offset'],array('bd_user_id' => $this->data['bd_user']->user_id ),$_POST['status'],$_POST['search_value'],$is_bd_login);
                foreach($leads as $value) {
                    $get_lead_owner_name = $this->mapi_model->get_all_details(USERS,array('id' => $value->bd_user_id));
                    $value->bd_user_id = $get_lead_owner_name->row()->full_name;
                }
                $NewData = array('lead_count' => $leadsCount[0]->count,'leads' => $leads);
                $result = array('message' => $this->lang->line('lead_listing'),'status_code' => 200, 'data' =>$NewData );
                $this->sendResponse($result,false);
                return;
            }else{
                $response = array("message"=> $this->lang->line('something_wrong'),"status_code"=>400,"data"=>new stdclass());
                $this->sendResponse($response,false);
                return;      
            }
        }
    } 
 

    public function get_myprofile(){
        if(!empty($this->data['bd_user'])){
            $data = $this->mapi_model->get_my_profile($this->data['bd_user']->user_id);
            $UserData = $data->row();
            $base_url = $this->data['baseUrl'];
            //base_url();
            if($data->row()->profile_pic){
                $UserData->profile_pic = $base_url.'images/partner_app/profile_pic/'.$data->row()->profile_pic;
            }
            $Poimages = [];
            if($data->row()->poi){
                $poi_images = explode(',', $data->row()->poi);
                foreach ($poi_images as $key => $value) {
                   $Poimages[$key] = $base_url.'images/partner_app/poi/'.$value;
                }
                // $UserData->profile_pic = base_url().'images/partner_app/profile_pic';
            }
            $UserData->poi = $Poimages;
            $VisitingCard = [];
            if($data->row()->visiting_card){
                $PCImages = explode(',', $data->row()->visiting_card);
                foreach ($PCImages as $key => $value) {
                   $VisitingCard[$key] = $base_url.'images/partner_app/visiting_card/'.$value;
                }
            }
            $UserData->visiting_card = $VisitingCard;
            if($data->row()->visiting_card){
               $UserData->cancelled_cheque = $base_url.'images/partner_app/cancelled_cheque/'.$data->row()->cancelled_cheque;
            }
            $is_bank_added = false;

            if($data->row()->bank_details){
                $bank_details = (array) json_decode($data->row()->bank_details);
                if($bank_details['bank_name']  != '' ){
                    $is_bank_added = true;
                }elseif($bank_details['account_holder_name']  != ''){
                    $is_bank_added = true;
                }elseif($bank_details['account_number']  != ''){
                    $is_bank_added = true;
                }elseif($bank_details['ifsc_code']  != ''){
                    $is_bank_added = true;
                }else{
                     $is_bank_added = false;
                }
                // print_r($bank_details);exit;   
            }

            $UserData->is_bank_added = $is_bank_added;

            $result = array('message' => $this->lang->line('my_profile'),'status_code' => 200, 'data' =>$UserData );
            $this->sendResponse($result,false);
            return;
            // echo base_url();exit;
        }else{
            $response = array("message"=> $this->lang->line('something_wrong'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;      
        }
    }

    public function get_myearning(){
        if(!empty($this->data['bd_user'])){
            $data['total_paid'] = $this->mapi_model->get_my_earning('paid',$this->data['bd_user']->user_id);
            $data['total_pending'] = $this->mapi_model->get_my_earning('pending',$this->data['bd_user']->user_id);
            $result = array('message' => $this->lang->line('my_profile'),'status_code' => 200, 'data' =>$data );
            $this->sendResponse($result,false);
            return;
        }else{
            $response = array("message"=> $this->lang->line('something_wrong'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;      
        }
    }


    //get rate card details 
    public function get_rate_cards(){
        $commission_board = $this->mapi_model->get_all_details(COMISSIONS,array());
        if($commission_board->num_rows() > 0){
            $html = '<html><head><head><style>#customers {border-collapse: collapse;width: 100%;}#customers td, #customers th {border: 1px solid #ddd;padding: 8px;}#customers tr:nth-child(even){background-color: #f2f2f2;}#customers tr:hover {background-color: #a3c8c1;}#customers th {padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #a4c7c3;color: white;}</style></head></head><body><table id=customers><thead><th>Tenure</th><th>Commission</th></thead><tbody>';
            foreach ($commission_board->result() as $key => $value) {
                $html .= '<tr><td>'.$value->tenure.'</td><td>'.$value->comission.'%</td></tr>';
            }
            $html .='</tbody></table></body></html>';

            $response = array("message"=>$this->lang->line('commissions'),"status_code"=>200,"data"=> $html);
            $this->sendResponse($response,false);
            exit;
        }else{
            $response = array("message"=> $this->lang->line('something_wrong'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;         
        }
    }

    public function get_ranking_board(){
        if(!empty($this->data['bd_user'])){
            // $this->data['bd_user']->user_id
            $ranking_board = $this->mapi_model->get_top_ranking();
            $RankingData = $ranking_board->result();
            foreach ($RankingData as $key => $value) {
                if($value->profile_pic){
                    $RankingData[$key]->profile_pic = $this->data['baseUrl'].'images/partner_app/profile_pic/'.$value->profile_pic;
                }
                $RankingData[$key]->Itsme = 0;
                if($value->id == $this->data['bd_user']->user_id){
                    $RankingData[$key]->Itsme = 1;   
                }
            }

            $result = array('message' => $this->lang->line('ranking_board'),'status_code' => 200, 'data' =>$RankingData );
            $this->sendResponse($result,false);
            return;
        }else{
            $response = array("message"=> $this->lang->line('something_wrong'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;      
        }

    }



    public function validate_token()
    {
        $access_token = $this->input->request_headers();
        // print_r($access_token);exit;
        if (empty($access_token['Auth-Token'])) {
            $response = array("message"=>"Access token missing","status_code"=>401,"data"=>new stdclass());
            $this->sendResponse($response,false);
            exit;
        } else {
            $ci =& get_instance();
            $ci->load->database();
            $sql = "select * from fc_partner_users where access_token='".$access_token['Auth-Token']."' AND user_id='".$access_token['User-Id']."'";
            $q = $ci->db->query($sql);
            if ($q->num_rows() == 0) {
                $response = array("message"=>"Access token missing","status_code"=>401 ,"data"=>new stdclass());
                $this->sendResponse($response,false);
                exit;
            }else{
                return $q->row();
            }
        }
        try {
            $token = JWT::decode($access_token['Auth-Token'], $this->config->item('jwt_key'), array('HS256'));

            $hoursDiff = (time() - $token->iat)/3600;
            
            if ($hoursDiff > $this->config->item('expire_time')) {
                $response = array("message"=>"Access token expired","status_code"=>401 ,"data"=>new stdclass());
                $this->sendResponse($response,false);
                exit;
            }
            //set decoded data in parameters to use it later.
        } catch (Exception $e) {
                $response = array("message"=>"Invalid access token","status_code"=>401 ,"data"=>new stdclass());
                $this->sendResponse($response,false);
                exit;
        }
    }


    public  function sendResponse($data,$doEncrypt=true){   
        $code = (!empty($data['code']) ? $data['code']:$data['status_code']);
        unset($data['code']);
        $response = $data;
        if($doEncrypt == true){
            $response = json_encode($response);
            $response = $this->encryptData($response);
        }
        
        // $this->response(array("response"=>$response),$code); 
        echo json_encode($response,$code);       
    }

    
    public function get_cms(){
        $type = $_GET['type'];
        $cms_details = $this->mapi_model->get_all_details(BD_CMS,array('cms_slug' => $type));
        if($cms_details->num_rows() > 0){
            $html = '<html><head><style>.page-title {text-align: center;}</style><body><div class=page-title><h2>'.$cms_details->row()->page_name.'</h2></div>'.$cms_details->row()->page_content.'</body></head></html>';
            $response = array("message"=>$this->lang->line('cms_detail'),"status_code"=>200,"data"=> $html);
            $this->sendResponse($response,false);
            exit;
        }else{
            $response = array("message"=> $this->lang->line('something_wrong'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;         
        }
    }

    //get bc executer user
    public function get_bdexecuter_users(){
        if(!empty($this->data['bd_user'])){
            $user = $this->mapi_model->get_all_details(USERS,array('id' => $this->data['bd_user']->user_id));
            if($user->row()->user_type != 'BD Executive'){
                $result = array('message' => $this->lang->line('invalid_access'),'status_code' => 400, 'data' =>$RankingData );
                $this->sendResponse($result,false);
                return;
            }
            $userListing = array();
            $my_users = $this->mapi_model->get_all_bdexecuter_users($this->data['bd_user']->user_id,$this->input->post('offset'),$this->input->post('limit'));
            if($my_users->num_rows() > 0){

                $total_count = 0;
                foreach ($my_users->result_array() as $key => $value) {
                    $userListing[$key]['user_id'] = $value['user_id'];
                    $userListing[$key]['profile_pic']  = $value['profile_pic'];
                    $userListing[$key]['full_name']  = $value['full_name'];
                    $userListing[$key]['email']  = $value['email'];
                    $userListing[$key]['phone_no'] = $value['phone_no'];
                    $get_lead_count = $this->mapi_model->get_delivered_lead_count($value['user_id']);
                    $userListing[$key]['total_lead'] = $get_lead_count->total_lead; 
                    $total_count += $get_lead_count->total_lead;

                } 
            }

            $response_array  = array('user_list' => $userListing, 'total_delivered' => $total_count);
            $result = array('message' => $this->lang->line('under_bd_users_list'),'status_code' => 200, 'data' =>$response_array );
            $this->sendResponse($result,false);
            return;
        }else{
            $response = array("message"=> $this->lang->line('something_wrong'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;      
        }
    }

    //get broker listing while adding new lead
       public function get_broker_list(){
        if(!empty($this->data['bd_user'])){
            $user = $this->mapi_model->get_all_details(USERS,array('id' => $this->data['bd_user']->user_id));
            if($user->row()->user_type != 'BD Executive'){
                $result = array('message' => $this->lang->line('invalid_access'),'status_code' => 400, 'data' =>new stdclass() );
                $this->sendResponse($result,false);
                return;
            }
            $userListing = array();
            $my_users = $this->mapi_model->get_all_bdexecuter_users($this->data['bd_user']->user_id,0,1000);
            // print_r($my_users->result_array());exit;
            if($my_users->num_rows() > 0){
                $total_count = 0;
                foreach ($my_users->result_array() as $key => $value) {
                    $userListing[$key]['user_id'] = $value['user_id'];
                    $userListing[$key]['full_name']  = $value['full_name'];
                    $userListing[$key]['phone_no'] = $value['phone_no'];
                }
            }
            $result = array('message' => $this->lang->line('under_bd_users_list'),'status_code' => 200, 'data' =>$userListing );
            $this->sendResponse($result,false);
            return;
        }else{
            $response = array("message"=> $this->lang->line('something_wrong'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;      
        } 
    }


    
     //get city listing
    public function get_cities(){
        $city_array = $this->mapi_model->get_all_cities();
        $new_cities = array();
        foreach ($city_array->result_array() as $key => $value) {  
            array_push($new_cities,$value['list_value']);
        }
        $result = array('message' => $this->lang->line('under_bd_users_list'),'status_code' => 200, 'data' =>$new_cities );
        $this->sendResponse($result,false);
        return;
    }

            //get city listing
    public function get_cities_product_listing(){
        $_POST = $this->input->post();
        $requiredParams = array("offset"=>"Offset",'limit' => 'Limit');
        $isValid = $this->validateRequest($requiredParams,$_POST);
        if ($isValid)
        {           
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;   
        }
        $city_array = $this->mapi_model->get_all_cities();
        $products = $this->getProducts($this->input->post('city'),$_POST['limit'],$_POST['offset']);
        if($products->num_rows() > 0){
            $CFProducts = $products->result_array();
            foreach ($CFProducts as $key => $value) {
                $TenurePrice = $this->mapi_model->view_subproduct_details_join($value['id']);
                $product_images =  explode(',', $value['image']);
                if(!empty($product_images)){
                    $CFProducts[$key]['image'] = $this->data['baseUrl'].'images/product/'.$product_images[0];
                }else{
                    $CFProducts[$key]['image'] = $this->data['baseUrl'].'images/product/dummyProductImage.jpg';
                }
                if(count($TenurePrice) > 0) {
                    $CFProducts[$key]['price'] = $TenurePrice;
                }else{
                    $CFProducts[$key]['price'] = array(array('attr_name' => '24 Months','attr_price' => $value['price']));
                }
            }
            $ProductCount = $this->mapi_model->get_all_details(BD_VIRTUAL_CART,array('user_id' => $this->data['bd_user']->user_id));         
            $return_array = array('product_count' => $ProductCount->num_rows(),'cities' => $city_array->result() , 'products' => $CFProducts);
            $result = array('message' => $this->lang->line('under_bd_users_list'),'status_code' => 200, 'data' => $return_array );
            $this->sendResponse($result,false);
            return;
        }
        
    }

    public function getProducts($city = '',$limit = 10,$offset = NULL){
        $searchCriteria = 'all';
        if ($searchCriteria != 'all') {
            $condition = " where c.seourl = '" . $searchCriteria . "'";
            $catID = $this->mapi_model->getCategoryValues(' c.*,sbc.id as subcat_id,sbc.seourl as subcat_seourl,sbc.cat_name as subcat_sub_cat_name,sbc.image as subcat_sub_cat_image,sbc.status as subcat_sub_cat_status', $condition);
            $listSubCat = $catID->result();
            if ($listSubCat[0]->subcat_seourl == '') {
                if ($listSubCat[0]->rootID != '') {
                    $parentCat = $this->mapi_model->getParentCategories($listSubCat[0]->rootID);
                }
            }
            $this->data['listSubCat'] = $listSubCat;
            $this->data['parentCat'] = $parentCat;
            $listSubCatSelBox = '<select style="display: none;" class="shop-select sub-category selectBox" edge="true">
                  <option value="">' . $catID->row()->cat_name . '</option>';
            foreach ($listSubCat as $listSub) {
                $listSubCatSelBox .= '<option value="' . base_url() . $urlVal . '/' . $listSub->subcat_seourl . '">' . $listSub->subcat_sub_cat_name . '</option>';
            }
            $listSubCatSelBox .= '</select>';

            $searchCriteriaBreadCumpArr = @explode('/', trim($searchCriteriaBreadCumpFinal[0]));

            if (count($searchCriteriaBreadCumpArr) > 1) {
                $link_str = base_url() . 'shopby';
                for ($i = 0; $i < count($searchCriteriaBreadCumpArr); $i++) {
                    if ($searchCriteriaBreadCumpArr[$i]) {
                        $condition = " where c.seourl = '" . $searchCriteriaBreadCumpArr[$i] . "' limit 0,1";
                        $Paging = $this->mapi_model->getCategoryValues(' c.*', $condition);
                        $link_str .= '/' . $Paging->row()->seourl;
                        $breadCumps .= '<li><a href="' . $link_str . '">' . $Paging->row()->cat_name . '</a></li>';
                    }
                }
            } else {
                $cat_name = $this->mapi_model->get_cat_name($catID->row()->rootID);
                if (!empty($cat_name->row())) {
                    $breadCumps .= '<li><a href="' . base_url() . $cat_slug. '/' . $cat_name->row()->seourl . '">' . $cat_name->row()->cat_name . '</a></li>';
                }
                $breadCumps .= '<li><a href="' . base_url() . $cat_slug. '/' . $catID->row()->seourl . '">' . $catID->row()->cat_name . '</a></li>';
            }
        } else {
            $sortArr1 = array('field' => 'cat_position', 'type' => 'asc');
            $sortArr = array($sortArr1);
            $_SESSION['sMainCategories'] = $this->mapi_model->get_all_details(CATEGORY, array('rootID' => '0', 'status' => 'Active'), $sortArr);
            
            $urlVal = str_replace('/all', '', $urlVal);
            $listSubCatSelBox = '<select style="display: none;" class="shop-select sub-category selectBox" edge="true">
             <option value="">All Category</option>';
            if ($_SESSION['sMainCategories'] != '') {
                $sortArr1 = array('field' => 'cat_position', 'type' => 'asc');
                $sortArr = array($sortArr1);
                $_SESSION['sMainCategories'] = $this->mapi_model->get_all_details(CATEGORY, array('rootID' => '0', 'status' => 'Active'), $sortArr);
            }

            foreach ($_SESSION['sMainCategories']->result() as $listSub) {
                if ($listSub->cat_name != 'Our Picks') {
                    $listSubCatSelBox .= '<option value="' . base_url() . $urlVal . '/' . $listSub->seourl . '">' . $listSub->cat_name . '</option>';
                }
            }
            $listSubCatSelBox .= '</select>';
        }



        if ($searchCriteria != 'all') {
            $whereCond = ' where FIND_IN_SET("' . $catID->row()->id . '",p.category_id) ' . $whereCond . ' and p.quantity>0 and p.status="Publish" and u.group="Seller" and u.status="Active"';

            if ($this->input->get('sort_by_location')) {
                $whereCond .= $sortbylocation;
            }
            $userWherCond .= ' where FIND_IN_SET("' . $catID->row()->id . '",p.category_id) ' . $userWherCond . ' and p.status="Publish" and p.sale_price>0 and u.status="Active" and p.status="Publish" and p.global_visible=1';
            $searchProd = $whereCond . ' order by product_order asc ' . $limitPaging . ' ';
            $userWherCond = $userWherCond . ' ' . $orderBy . ' ' . $limitPaging . ' ';
            $totalProd = $whereCond . ' ' . $orderBy . ' ' . $limitPaging . ' ';
        } else {
            $whereCond = ' where p.id != "" ' . $whereCond . ' and p.quantity>0 and p.status="Publish" and u.group="Seller" and u.status="Active"';
            if ($this->input->get('sort_by_location')) {
                $whereCond .= $sortbylocation;
            }
            $userWherCond .= ' where p.id !="" ' . $userWherCond . ' and p.status="Publish" and u.status="Active" and p.status="Publish" and p.sale_price>0 and p.global_visible=1';
            $searchProd = $whereCond . ' ' . $orderBy . ' ' . $limitPaging . ' ';
            $userWherCond = $userWherCond . ' ' . $orderBy . ' ' . $limitPaging . ' ';
            $totalProd = $whereCond . ' ' . $orderBy . ' ' . $limitPaging . ' ';
        }

        if ($city != '') {
            $whereCond .= ' and FIND_IN_SET("' . $city . '",p.list_value)';
            $searchProd .= ' and FIND_IN_SET("' . $city. '",p.list_value)';
        }

         $productList = $this->mapi_model->searchProductListing($searchProd,$limit,$offset);
         return $productList;
    }

     //get insentive amounts 
    public function get_insentive(){
        if(!empty($this->data['bd_user'])){
            $user = $this->mapi_model->get_all_details(USERS,array('id' => $this->data['bd_user']->user_id));
            if($user->row()->user_type != 'BD Executive'){
                $result = array('message' => $this->lang->line('invalid_access'),'status_code' => 400, 'data' =>new stdclass() );
                $this->sendResponse($result,false);
                return;
            }
            $ReturnArray = array('total_revenue' => 0,'my_revenue' => 0,'broker_revenue' => 0,'total_orders' => 0, 'my_orders' => 0,'broker_orders' => 0);

            $fillerArray = array('month' => date('m'),'year' => date('Y'));
            if($_GET['year'] || $_GET['month']){
                $fillerArray['month'] = $_GET['month'];
                $fillerArray['year'] = $_GET['year'];
            } 

            $get_my_insentive = $this->mapi_model->get_my_revenue($this->data['bd_user']->user_id,$fillerArray);
            if($get_my_insentive->num_rows() > 0){
                $ReturnArray['my_revenue'] = $get_my_insentive->row()->rental_amount ? $get_my_insentive->row()->rental_amount : 0;
            }
            $my_users = $this->mapi_model->get_bdexecuter_users($this->data['bd_user']->user_id);
            if($my_users->num_rows() > 0){
                $get_broker_insentive = $this->mapi_model->get_broker_revenue($my_users->row(),$fillerArray);
                if($get_broker_insentive->num_rows() > 0){
                     $ReturnArray['broker_revenue'] = $get_broker_insentive->row()->rental_amount ?  $get_broker_insentive->row()->rental_amount : 0;
                }
                $get_broker_order_count = $this->mapi_model->get_broker_order_count($my_users->row(),$fillerArray);
                if($get_broker_order_count->num_rows() > 0){
                     $ReturnArray['broker_orders'] = $get_broker_order_count->num_rows() ?  $get_broker_order_count->num_rows() : 0;
                }
            }
            // order details 
            $get_my_order_count = $this->mapi_model->get_my_order_count($this->data['bd_user']->user_id,$fillerArray);
             if($get_my_order_count->num_rows() > 0){
                $ReturnArray['my_orders'] = $get_my_order_count->num_rows() ? $get_my_order_count->num_rows() : 0;
            }
            $ReturnArray['total_revenue'] = $ReturnArray['my_revenue'] + $ReturnArray['broker_revenue'];
            $ReturnArray['total_orders'] = $ReturnArray['my_orders'] + $ReturnArray['broker_orders'];


            $result = array('message' => $this->lang->line('under_bd_users_list'),'status_code' => 200, 'data' =>$ReturnArray );
            $this->sendResponse($result,false);
            return;
        }else{
            $response = array("message"=> $this->lang->line('something_wrong'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;      
        }
    }


    public function get_todays_order(){
        if(!empty($this->data['bd_user'])){
            $user = $this->mapi_model->get_all_details(USERS,array('id' => $this->data['bd_user']->user_id));
            if($user->row()->user_type != 'BD Executive'){
                $result = array('message' => $this->lang->line('invalid_access'),'status_code' => 400, 'data' =>new stdclass() );
                $this->sendResponse($result,false);
                return;
            }

            $allTodaysOrder = $this->mapi_model->get_todays_order($this->data['bd_user']->user_id,$this->input->post('date'));
  
            $result = array('message' => $this->lang->line('under_bd_users_list'),'status_code' => 200, 'data' =>$allTodaysOrder->result() );
            $this->sendResponse($result,false);
            return;
        }else{
            $response = array("message"=> $this->lang->line('something_wrong'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;      
        }
    }

    public function get_notifications(){
        if(!empty($this->data['bd_user'])){
            $where = 'user_id = '. $this->data['bd_user']->user_id.' AND created BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW() ORDER BY created  DESC';
            $all_notifications = $this->mapi_model->get_all_details(BD_NOTIFICATION,$where);

            // print_r($this->db->last_query());exit;
            $result = array('message' => $this->lang->line('under_bd_users_list'),'status_code' => 200, 'data' =>$all_notifications->result() );
            $this->sendResponse($result,false);
            return;
        }else{
            $response = array("message"=> $this->lang->line('something_wrong'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;      
        }
    }

    public function do_logout(){
        if(!empty($this->data['bd_user'])){
            $all_notifications = $this->mapi_model->update_details(PARTNER_USERS,array('access_token' => '', 'device_token' => ''),array('user_id' => $this->data['bd_user']->user_id));
            $result = array('message' => 'Log out success','status_code' => 200, 'data' => new stdclass() );
            $this->sendResponse($result,false);
            return;
        }else{
            $response = array("message"=> $this->lang->line('something_wrong'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;      
        }   
    }

    //get client respone status
    public function get_client_response_status(){
        if(!empty($this->data['bd_user'])){
            $get_all_status = $this->mapi_model->get_all_details(LEAD_RESPONSE_STATUS,array());
            $result = array('message' => 'Client Response Statsu','status_code' => 200, 'data' => $get_all_status->result_array());
            $this->sendResponse($result,false);
            return;
        }else{
            $response = array("message"=> $this->lang->line('something_wrong'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;      
        }      
    }

    // update client response for lead (hot/cold/warm)
    public function update_client_response_for_lead(){
        if(!empty($this->data['bd_user'])){
            $_POST = $this->input->post();
            $requiredParams = array("client_status"=>"Status","lead_id" => "Lead ID","color_code" => "Color Code");
            $isValid = $this->validateRequest($requiredParams,$_POST);
            if ($isValid)
            {           
                $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());
                $this->sendResponse($response,false);
                return;   
            }
            $get_all_status = $this->mapi_model->update_details(BD_LEADS,array('client_status' => $_POST['client_status'],'color_code' => $_POST['color_code']),array('id' => $_POST['lead_id']));
            $result = array('message' => 'Lead update successfully','status_code' => 200, 'data' => new stdclass());
            $this->sendResponse($result,false);
            return;
        }else{
            $response = array("message"=> $this->lang->line('something_wrong'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;      
        }    
    }
    
    //add product in virtual cart
    public function add_in_virtual_cart(){
        $_POST = $this->input->post();
        $requiredParams = array("product_id"=>"Product ID",'attr_id' => 'Tenure','user_id' => 'user_id');
        $isValid = $this->validateRequest($requiredParams,$_POST);
        if ($isValid)
        {    
            $response = array("message"=>reset($isValid),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;   
        }
        $excludeArr = array('attr_id');
        $dataArrVal = array();
        $cart_data =  $this->mapi_model->get_all_details(BD_VIRTUAL_CART,array( 'user_id' => $this->data['bd_user']->user_id));
        $current_product_attr =  $this->mapi_model->get_all_details(SUBPRODUCT,array( 'pid' => $this->input->post('attr_id') ));

        if($cart_data->num_rows() > 0){
            foreach ($cart_data->result() as $key => $value) {
                $attr_value =  $this->mapi_model->get_all_details(SUBPRODUCT,array( 'pid' =>$value->attribute_values ));
                if($this->input->post('attr_id') != '' && $attr_value->row()->attr_name !=''){
                    if(strtoupper($attr_value->row()->attr_name) != strtoupper($current_product_attr->row()->attr_name))
                    {
                        $result = array('message' => 'Please select same tenure as selected for other cart items','status_code' => 400, 'data' => new stdclass());
                        $this->sendResponse($result,false);
                        return;
                    }
                }
            }   
        }
        $product_details =   $this->mapi_model->get_all_details(PRODUCT,array( 'id' =>  $this->input->post('product_id')));
        if($this->input->post('attr_id') !=''){
            $dataArrVal['price'] = $current_product_attr->row()->attr_price;
            $dataArrVal['product_shipping_cost'] = $product_details->row()->shipping_cost;
        }else{
            $dataArrVal['price'] = $product_details->row()->price;
            $dataArrVal['product_shipping_cost'] = $product_details->row()->shipping_cost;
        }   

        $datestring = date('Y-m-d H:i:s',now());
        $indTotal = ( $current_product_attr->row()->attr_price + $product_details->row()->shipping_cost + ($current_product_attr->row()->attr_price * 0.01 * 0) ) * 1;

        $dataArry_data = array('created' => $datestring, 'user_id' => $this->data['bd_user']->user_id, 'indtotal' => $indTotal, 'total' => $indTotal,'quantity' => 1,'attribute_values' => $this->input->post('attr_id'));
        $dataArr = array_merge($dataArrVal,$dataArry_data);
        $condition ='';


        if($this->input->post('attr_id') !=''){
            $productVal = $this->mapi_model->get_all_details(BD_VIRTUAL_CART,array( 'user_id' => $this->data['bd_user']->user_id,'product_id' => $this->input->post('product_id'),'attribute_values' => $this->input->post('attr_id')));
        }else {
            $productVal = $this->mapi_model->get_all_details(BD_VIRTUAL_CART,array( 'user_id' => $this->data['bd_user']->user_id,'product_id' => $this->input->post('product_id')));
        }


        $for_qty_check = $this->mapi_model->get_all_details(BD_VIRTUAL_CART,array( 'user_id' => $this->data['bd_user']->user_id,'product_id' => $this->input->post('product_id')));

        if($for_qty_check->num_rows() > 0){
            $new_tot_qty = 0;
            foreach ($for_qty_check->result() as $for_qty_check_row){
                $new_tot_qty += $for_qty_check_row->quantity;
            }
            $new_tot_qty += 1;
            if ($new_tot_qty <= 50){
                if ($productVal->num_rows() > 0){
                    $newQty = ($productVal->row()->quantity + 1);
                    $indTotal = ( $current_product_attr->row()->attr_price +  $product_details->row()->shipping_cost + 
                        ($current_product_attr->row()->attr_price * 0.01 * 0) ) * $newQty;
                    $dataArr = array('quantity' => $newQty, 'indtotal' => $indTotal, 'total' => $indTotal);
                    $condition =array('id' => $productVal->row()->id);
                    $this->mapi_model->commonInsertUpdate(BD_VIRTUAL_CART,'update',$excludeArr,$dataArr,$condition);
                } else {
                    $message =  $this->mapi_model->commonInsertUpdate(BD_VIRTUAL_CART,'insert',$excludeArr,$dataArr,$condition);
                }
            }  else{
                $cart_qty = $new_tot_qty - 1;
                $result = array('message' =>  'Maximum Quantity: 50. Already in your cart'.$cart_qty ,'status_code' => 400, 'data' => new stdclass());
                $this->sendResponse($result,false);
                return;
            }
        }else{
            $this->mapi_model->commonInsertUpdate(BD_VIRTUAL_CART,'insert',$excludeArr,$dataArr,$condition);
        }
        $ProductCount = $this->mapi_model->get_all_details(BD_VIRTUAL_CART,array('user_id' => $this->data['bd_user']->user_id));
        $return_array = array('product_count' => $ProductCount->num_rows());
        $response = array('message' =>  'Product added successfully' ,"status_code"=>200,"data"=>$return_array);
        $this->sendResponse($response,false);
        return;         
    }

    //get virtual cart details 
    public function get_my_cart_details(){
        if(!empty($this->data['bd_user'])){
            $returnArray = [];
            $totalQuantity = 0;
            $totalRental = 0;
            $totalSecurityDeposite = 0;
            $tenure = '24 Months';
            $cartDetails = $this->mapi_model->get_my_cart($this->data['bd_user']->user_id);
            if($cartDetails->num_rows() > 0){
                foreach ($cartDetails->result_array() as $key => $value) {
                    $returnArray[$key]['product_name'] = $value['product_name'];
                    $returnArray[$key]['quantity'] = $value['quantity'];
                    $returnArray[$key]['product_price'] = $value['attr_price'] * $value['quantity'];
                    $totalQuantity += $value['quantity'];
                    $totalRental += $value['attr_price'] * $value['quantity'];
                    $totalSecurityDeposite += $value['product_shipping_cost'] * $value['quantity'];
                    $tenure = $value['attr_name'];
                }

                $newArray = array('product_info' => $returnArray,'quantity' => $totalQuantity,'tenure' => $tenure,'total_rental' => $totalRental, 'security_deposite' => $totalSecurityDeposite, 'total' => $totalRental + $totalSecurityDeposite);

                $response = array('message' =>  'Cart Listing' ,"status_code"=>200,"data"=>$newArray);
                $this->sendResponse($response,false);
                return;        
            }else{
                $response = array('message' =>  'Cart Listing' ,"status_code"=>200,"data"=>new stdclass());
                $this->sendResponse($response,false);
                return;      
            }
        }else{
            $response = array("message"=> $this->lang->line('something_wrong'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return; 
        }
    }

    // Clear all products details from virtual carts 
    public function clear_my_cart(){
        if(!empty($this->data['bd_user'])){
            $ChecKCartValue = $this->mapi_model->get_all_details(BD_VIRTUAL_CART,array('user_id' =>$this->data['bd_user']->user_id ));
           if($ChecKCartValue->num_rows() > 0){
            $this->mapi_model->commonDelete(BD_VIRTUAL_CART,array('user_id' =>$this->data['bd_user']->user_id ));
            $response = array("message"=> 'Cart clear successfully',"status_code"=>200,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return; 
           }
        }else{
            $response = array("message"=> $this->lang->line('something_wrong'),"status_code"=>400,"data"=>new stdclass());
            $this->sendResponse($response,false);
            return;    
        }
    }

}

 ?>