<?php  
// if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    require 'vendor/autoload.php';

/**
 * 
 * CMS related functions
 * @author Teamtweaks
 *
 */

class Crone extends MY_Controller {
	function __construct(){
        parent::__construct();
// 		 if (!$this->input->is_cli_request()) show_error('Direct access is not allowed');
		$this->load->model('product_model');
		$this->load->model('Cms_model');
		$this->load->model('order_model');
		error_reporting(0);
    }

    
    public function getClient()
	{
    	$client = new Google_Client();
	    $client->setClientId('881003059391-a0vl5m6cbidc51mgeegfbfqfcfpnr83c.apps.googleusercontent.com');
	    $client->setClientSecret('rrQG2uVHfgtzCQnOy4QdYG9q');
	    $client->setAccessType('offline');
	    $client->refreshToken('1/qO_cgTtpx5aE5Wqqj_1kgNIRFvUtpoh3HVkGLdyOE9qowax2zebve1vot0agjc50');
    	return $client;
	}



    public function GetTodayReports(){

		$client = $this->getClient();
		$service = new Google_Service_Sheets($client);
		$Abandoned_Cart_spreadsheetId =  '1Yg-zA7Vv1AqIGa2EwTDmxkCx_k5kjfnW80NfH0SY4Hc';
		$Abandoned_SignUp_spreadsheetId = '1-V6X9lGOENlvqsKRenhndC7j5YJ1QG2fua4Aq9f2uEY';
		$Abandoned_Cart_range = 'A2:U';
		$Abandoned_SignUp_range = 'A2:O';
    	$time = time();
    	$curr_date = date("Y-m-d", mktime(0,0,0,date("n", $time),date("j",$time)- 1 ,date("Y", $time)));
    	$for_mail_formate = date("d-m-y", mktime(0,0,0,date("n", $time),date("j",$time)- 1 ,date("Y", $time)));

 		$Admin_setting = $this->Cms_model->get_all_details(ADMIN_SETTINGS,array('id','1'));
    	$this->data['OnlyNewSignUpUsers'] = $this->Cms_model->Abandoned_SignUp_Users($curr_date);
    	$this->data['SignUpWithCart'] = $this->Cms_model->Abandoned_Cart($curr_date);

    	if(!empty($this->data['OnlyNewSignUpUsers'])){
			foreach ($this->data['OnlyNewSignUpUsers'] as $user ) {
				$datatocsv = [[$user->id, $user->full_name, $user->user_name, $user->email, $user->status, $user->created, $user->last_login_date, $user->address, $user->address2, $user->city, $user->district, $user->state, $user->country, $user->postal_code, $user->phone_no]];
				$response = $service->spreadsheets_values->get($Abandoned_SignUp_spreadsheetId, $Abandoned_SignUp_range);
				$options = array('valueInputOption' => 'RAW');
				$body   = new Google_Service_Sheets_ValueRange(['values' => $datatocsv]);
				$result = $service->spreadsheets_values->append($Abandoned_SignUp_spreadsheetId, 'A1:O', $body, $options);
			}
    	}
    	if(!empty($this->data['SignUpWithCart'])){
			foreach ( $this->data['SignUpWithCart'] as $new_key => $user ) {
				$mini_cart_items  = $this->minicart_model->items_in_cart($user->user_id);
				$new_total = [];
				$cartDiscountAmt = '';
				$product_shipping_cost = '';
				$cartAmt = '';
				$grantAmt = '';
				$product_name = '';	
				foreach ($mini_cart_items as $key => $product) {
					    $product_name .= $product->product_name.'|';
 		                $cartDiscountAmt = $cartDiscountAmt + ($product->discountAmount * $product->quantity);
					 	$product_shipping_cost = $product_shipping_cost + ($product->product_shipping_cost * $product->quantity);
		                $cartAmt = $cartAmt + (($product->price - $product->discountAmount + ($product->price * 0.01 * $product->product_tax_cost)) * $product->quantity);
		                $cartTAmt = ($cartAmt * 0.01 * 0);
		                $grantAmt = $cartAmt + $product_shipping_cost + $cartTAmt;
	            	}
            		if(isset($mini_cart_items[0]->attr_name)){
            		    $tenure = $mini_cart_items[0]->attr_name;
            		}else{
            		     $tenure = '';
            		}
					
		           	$product_name_temp = rtrim($product_name,'|');
					$datatocsv = [[ $user->user_id, $user->full_name, $user->user_name, $user->email, $user->status, $user->created, $user->last_login_date, $user->address, $user->address2, $user->city, $user->district, $user->state, $user->country, $user->postal_code, $user->phone_no,$product_name_temp , $tenure , $user->couponCode , $product_shipping_cost ,$cartDiscountAmt, $grantAmt]];
					$response = $service->spreadsheets_values->get($Abandoned_Cart_spreadsheetId, $Abandoned_Cart_range);
					$options = array('valueInputOption' => 'RAW');
					$body   = new Google_Service_Sheets_ValueRange(['values' => $datatocsv]);
					$result = $service->spreadsheets_values->append($Abandoned_Cart_spreadsheetId, 'A1:U', $body, $options);
			}
    	}
    
    	echo "success";
 
    }
    
    public function getFailedOrders() {
		$date = date('Y-m-d');

		$this->db->select('*');
		$this->db->from('fc_payment');
		$this->db->where('is_failed_mail_sent = "0" and status = "Pending" and DATE(modified) = "'.$date.'" and modified <= DATE_SUB(now(), INTERVAL 5 MINUTE) and modified > DATE_SUB(now(), INTERVAL 10 MINUTE)');
		$this->db->group_by('dealCodeNumber');
		$orders = $this->db->get();

		foreach($orders->result() as $val) {
			$this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.image,pd.id as PrdID,pAr.attr_name as attr_type,sp.attr_name');
			$this->db->from(PAYMENT . ' as p');
			$this->db->join(USERS . ' as u', 'p.user_id = u.id');
			$this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
			$this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
			$this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
			$this->db->where('p.dealCodeNumber="' . $val->dealCodeNumber . '"');
			$PrdList = $this->db->get();

			$this->db->select('p.sell_id,p.couponCode,u.email');
			$this->db->from(PAYMENT . ' as p');
			$this->db->join(USERS . ' as u', 'p.sell_id = u.id');
			$this->db->where('p.dealCodeNumber="' . $val->dealCodeNumber . '"');
			$this->db->group_by("p.sell_id");
			$SellList = $this->db->get();

			$isSend = $this->order_model->new_failed_order_email($PrdList, $SellList);
		    $this->db->where('dealCodeNumber', $val->dealCodeNumber);
			$this->db->update('fc_payment', array('is_failed_mail_sent' => '1'));
		}
	}
	
	
// 	function create_csv_string() {
//  		$Admin_setting = $this->Cms_model->get_all_details(ADMIN_SETTINGS,array('id','1'));
//     	$data = $this->Cms_model->get_last_seven_days_report();
// 	    $array = [];
// 	    if($data->num_rows() > 0){
// 	    	foreach ($data->result_array() as $key => $value) {
// 	    		$array[$value['product_id']][] = $value;
// 	    	}
// 	    	$city_array =  [];
// 	    	$new_array = [];
// 	    	foreach ($array as $key => $value) {
// 	    		foreach ($value as $k => $v) {
// 	    			if($key == $v['product_id']){
// 	    				$new_array[$key]['id'] = $v['product_id'];	
// 	    				$new_array[$key]['name'] = $v['product_name'];
// 	    				$new_array[$key]['city'][$v['city']] = $v['total_sold'];
// 	    				$city_array[] = $v['city'];
// 	    			}
// 	    		}    	
// 	    	}
// 	    	$start_date = date("d M",strtotime($data->result_array()[0]['created']));
// 	    	$end_date = date("d M");
// 	    	$year = date('Y');
// 	    	$city_array = array_unique($city_array);
// 	    	if(!empty($new_array)){
// 	    		$new_user_csvName = "Product ordered online  $start_date - $end_date $year.csv";
// 				$header = array('Product ID','Product Name');
// 				$new_header = array_merge($header,$city_array);
// 				array_push($new_header, 'Total');
// 				$fp = fopen(CSV_FILE_PATH.$new_user_csvName, 'w+');
// 				fwrite($fp, implode(',', $new_header)."\n");
// 				foreach ($new_array as $val ) {
// 					$product_count = 0;
// 					$datatocsv = array($val['id'],$val['name']);
// 					$city_vise_array = $this->abc($new_header,$val);
// 					foreach ($city_vise_array as $key => $value) {
// 						$datatocsv[$key] = $value;
// 						$product_count += $value;
// 					}
// 					$datatocsv[end(array_keys($new_header))] = $product_count;
// 					fwrite($fp, implode(',', $datatocsv)."\n");
// 				}
// 				fclose($fp);
// 	    	}else{
// 	    		$new_user_csvName = "Product ordered online  $start_date - $end_date $year.csv";
// 				$header = array('No Order Found');
// 				$fp = fopen(CSV_FILE_PATH.$new_user_csvName, 'w+');
// 				fwrite($fp, implode(',', $header)."\n");
// 				$datatocsv = array();
// 				fwrite($fp,'');
// 				fclose($fp);
// 	    	}

// 	    	$email_values = array('mail_type'=>'html',
// 	                 'from_mail_id'=> $Admin_setting->row()->site_contact_mail,
// 	                 'mail_name'=>'Daily Report',
// 	                 'to_mail_id'=> $Admin_setting->row()->weekly_report_email,
// 	                 'subject_message'=> "Products ordered online:  $start_date - $end_date $year",
// 	                 'body_messages'=> 'Weekly report of citywise products ordered from site.',
// 	                 'attachment1' => $new_user_csvName
// 			);
// 		 	$email_send_to_common = $this->Cms_model->common_email_send($email_values);
// 	    }

// 	}

    function create_csv_string() {
 		$Admin_setting = $this->Cms_model->get_all_details(ADMIN_SETTINGS,array('id','1'));
    	$data = $this->Cms_model->get_last_seven_days_report();
	    $array = [];
	    if($data->num_rows() > 0){
	    	foreach ($data->result_array() as $key => $value) {
	    		$array[$value['product_id']][] = $value;
	    	}
	    	$city_array =  [];
	    	$new_array = [];
	    	
	    	$product_id_array = [];

	    	foreach ($array as $key => $value) {

	    		foreach ($value as $k => $v) {
	    			if($key == $v['product_id'] && $v['subproducts'] == ''){
	    				$new_array[$key]['id'] = $v['product_id'];	
	    				$new_array[$key]['name'] = $v['product_name'];
	    				$new_array[$key]['city'][$v['city']] = $v['total_sold'] * $v['quantity'];
	    				$city_array[] = $v['city'];
	    			}else{
	    				$product_id = explode(',', $v['subproducts']);
	    				$product_data = $this->Cms_model->get_last_seven_days_report_on_products($product_id);
	    				foreach ($product_data->result_array() as $pkey => $pvalue) {
							if(array_key_exists($pid, $product_id_array)){
	    						$product_id_array[$pkey]['id'] = $pvalue['product_id'];	
			    				$product_id_array[$pkey]['name'] = $pvalue['product_name'];
			    				$product_id_array[$pkey]['city'][$v['city']] = ($v['total_sold'] * $v['quantity']) + 1;
			    				$city_array[] = $v['city'];
	    					}else{
	    						$product_id_array[$pkey]['id'] = $pvalue['product_id'];	
			    				$product_id_array[$pkey]['name'] = $pvalue['product_name'];
			    				$product_id_array[$pkey]['city'][$v['city']] = $v['total_sold'] * $v['quantity'];
			    				$city_array[] = $v['city'];
	    					} 
	    				}

	    			}
	    		}    	
	    	}

	    	$new_array = array_merge($new_array,$product_id_array);
	    	$start_date = date("d M",strtotime($data->result_array()[0]['created']));
	    	$end_date = date("d M");
	    	$year = date('Y');
	    	$city_array = array_unique($city_array);
	    	if(!empty($new_array)){
	    		$new_user_csvName = "Product ordered online  $start_date - $end_date $year.csv";
				$header = array('Product ID','Product Name');
				$new_header = array_merge($header,$city_array);
				array_push($new_header, 'Total');
				$fp = fopen(CSV_FILE_PATH.$new_user_csvName, 'w+');
				fwrite($fp, implode(',', $new_header)."\n");
				foreach ($new_array as $val ) {
					$product_count = 0;
					$datatocsv = array($val['id'],$val['name']);
					$city_vise_array = $this->abc($new_header,$val);
					foreach ($city_vise_array as $key => $value) {
						$datatocsv[$key] = $value;
						$product_count += $value;
					}
					$datatocsv[end(array_keys($new_header))] = $product_count;
					fwrite($fp, implode(',', $datatocsv)."\n");
				}
				fclose($fp);
	    	}else{
	    		$new_user_csvName = "Product ordered online  $start_date - $end_date $year.csv";
				$header = array('No Order Found');
				$fp = fopen(CSV_FILE_PATH.$new_user_csvName, 'w+');
				fwrite($fp, implode(',', $header)."\n");
				$datatocsv = array();
				fwrite($fp,'');
				fclose($fp);
	    	}

	    	$email_values = array('mail_type'=>'html',
	                 'from_mail_id'=> $Admin_setting->row()->site_contact_mail,
	                 'mail_name'=>'Daily Report',
	                 'to_mail_id'=> $Admin_setting->row()->weekly_report_email,
	                 'subject_message'=> "Products ordered online:  $start_date - $end_date $year",
	                 'body_messages'=> 'Weekly report of citywise products ordered from site.',
	                 'attachment1' => $new_user_csvName
			);
		 	$email_send_to_common = $this->Cms_model->common_email_send($email_values);
	    }

	}

	function abc($new_header,$val){
		$array = [];
		foreach ($new_header as $key => $value) {
			foreach ($val['city'] as $k => $v) {
				if($k == $value ){
					 $array[$key] = $val['city'][$k]; 
				}
				else{
					if($key != 0 && $key != 1){
						if($array[$key] == '' || $array[$key] == ''){
						 $array[$key] = 0; 
						}
					}
				}
			}
		}
		return $array;
	}
	
	
	
	public function get_state(){
		switch ($this->input->post('city')) {
			case 'Bangalore':
				$state = 'Karnataka';
				break;
			case 'Delhi':
				$state = 'Delhi';
				break;
			case 'Ghaziabad':
				$state = 'Uttar Pradesh'; 
				break;
			case 'Gurgaon':
				$state = 'Haryana';
				break;
			case 'Mumbai':
				$state = 'Maharashtra';
				break;
			case 'Noida':
				$state = 'Uttar Pradesh';
				break;
			case 'Pune':
				$state = 'Maharashtra';
				break;
			case 'Hyderabad':
				$state = 'Telangana';
				break;
			default:
				$state = '';
				break;
		}

		$cityarray = array('message' => 'State Found', 'data'=> $state);
		echo json_encode($cityarray);
	}
	
	public function catch_all_crif_response(){
	    $email_values = array('mail_type'=>'html',
	                 'from_mail_id'=> 'hello@cityfurnish.com',
	                 'mail_name'=>'Daily Report',
	                 'to_mail_id'=> 'naresh.suthar@agileinfoways.com',
	                 'subject_message'=> "Products ordered online",
	                 'body_messages'=> json_encode($_POST)
		    );
		$email_send_to_common = $this->Cms_model->common_email_send($email_values);
	}
	
	




}