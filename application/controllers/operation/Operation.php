<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operation extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','form','security'));
		$this->load->model('operation_model');
	}
	public $data = array();
	private function esc_string($x){
		return $this->security->xss_clean(mysql_real_escape_string($x));
	}	
	function index(){
		$data['msg'] = '';
		$data['allgood'] = 0;
		if($this->input->post('submit')){
			if($this->input->post('username')=='' || $this->input->post('password')==''){
				$data['msg'] = 'Either username or password is incorrect';
			}else{
				$query = $this->db->query("SELECT id,admin_name,email,admin_type,cat_roles FROM ".ADMIN." WHERE admin_name='".$this->esc_string($this->input->post('username'))."' AND admin_password='".md5($this->input->post('password'))."' AND status='Active' LIMIT 1");
				if($query->num_rows() > 0){					
					foreach ($query->result() as $row){
						$ssess_array = array('user_id'=>$row->id, 'username'=>$row->admin_name, 'logged_in' => TRUE, 'email' => $row->email, 'admin_type' => $row->admin_type, 'cat_roles' => $row->cat_roles);
						$this->session->set_userdata($ssess_array);
					}
					redirect(''.BASE_URL.'dashboard', 'location');
				}else{
					$data['msg'] = 'Either username or password is incorrect';
				}
			}
		}			
		$this->load->view('operation/index',$data);
	}
	function dashboard(){
		$this->data['template'] = 'dashboard';
		$this->load->view('operation/template',$this->data);
	}
	function addtickets(){
		if($this->input->post('submit')){
			$this->operation_model->add_tciket_topic(array('t_title'=>$this->esc_string($this->input->post('title')),'t_desc'=>$this->esc_string($this->input->post('description')),'t_emailtitle'=>$this->esc_string($this->input->post('t_emailtitle')),'t_emailid'=>$this->esc_string($this->input->post('t_emailid')),'t_emailidccc'=>$this->esc_string($this->input->post('t_emailidccc')),'adddateTime'=>''.date('Y-m-d H:i:s').''));
			redirect(''.BASE_URL.'viewtopics', 'location');
		}
		$this->data['template'] = 'addtickets';
		$this->load->view('operation/template',$this->data);		
	}
	function viewtopics(){
		$this->data['topics'] = $this->operation_model->getalltopics();		
		$this->data['template'] = 'viewtopics';
		$this->load->view('operation/template',$this->data);		
	}
	function edittopics($id){
		if($this->input->post('submit')){
			$this->db->query("UPDATE ".TICKETS_TOPIC." SET t_title='".$this->esc_string($this->input->post('title'))."',t_desc='".$this->esc_string($this->input->post('description'))."',t_emailtitle='".$this->esc_string($this->input->post('t_emailtitle'))."',t_emailid='".$this->esc_string($this->input->post('t_emailid'))."',t_emailidccc='".$this->esc_string($this->input->post('t_emailidccc'))."' WHERE ID='{$id}' LIMIT 1");
		}		
		$query = $this->db->query("SELECT * FROM ".TICKETS_TOPIC." WHERE ID='{$id}' LIMIT 1");
		$this->data['topic'] = $query->result();
		$this->data['template'] = 'edittopics';
		$this->load->view('operation/template',$this->data);		
	}
	function deletetopic(){
		$this->db->query("DELETE FROM ".TICKETS_TOPIC." WHERE ID='".$this->esc_string($this->input->post('id'))."' LIMIT 1");
		$result['ok'] = true;
		echo json_encode($result);
	}
	function viewtickets(){
		$this->data['tickets'] = $this->operation_model->getalltickets();		
		$this->data['template'] = 'viewtickets';
		$this->load->view('operation/template',$this->data);		
	}
	function changeticketstatus(){
		$this->db->query("UPDATE ".TICKETS." SET t_status='".$this->esc_string($this->input->post('v'))."' WHERE ID='".$this->esc_string($this->input->post('id'))."' LIMIT 1");
	}
	function addroles(){
		if($this->input->post('submit')){
			$topic = '';
			if(!($this->input->post('topic'))){
				$topic = implode(",",$this->input->post('topic'));
			}
			$this->db->query("INSERT INTO ".ADMIN." SET created='".date('Y-m-d')."',modified='".date('Y-m-d')."',admin_name='".$this->esc_string($this->input->post('admin_name'))."',admin_password='".md5('welcome123')."',email='".$this->esc_string($this->input->post('email'))."',admin_type='operation',privileges='',last_login_date=now(),last_logout_date=now(),last_login_ip='',is_verified='Yes',cat_roles='".$topic."'");
			$headers = "From: CityFurnish Team <info@cityfurnish.com>\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			$message = "Hello,<br/><br/>
						Please find you login details for <a href='".BASE_URL."operation'>Click Me</a><br/>
						Username: ".$this->input->post('admin_name')."<br/>
						Password: welcome123<br/><br/>
						Regards
			";
			mail($this->esc_string($this->input->post('email')), "Login Details For Operation Panel", $message, $headers);
			redirect(''.BASE_URL.'viewtopics', 'location');
		}
		$this->data['topics'] = $this->operation_model->getalltopics();
		$this->data['template'] = 'addroles';
		$this->load->view('operation/template',$this->data);		
	}
	function viewadminusers(){
		$this->data['users'] = $this->operation_model->getalladminusers();		
		$this->data['template'] = 'viewadminusers';
		$this->load->view('operation/template',$this->data);		
	}
	function operationlogout(){
		redirect(''.BASE_URL.'operation', 'location');
	}	
	function ticketinfo($id){
		$this->data['ticket'] = $this->operation_model->getsingleticket($id);		
		$this->data['template'] = 'ticketinfo';
		$this->load->view('operation/template',$this->data);		
	}
}