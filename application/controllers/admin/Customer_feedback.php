<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * This controller contains the functions related to Product management
 * @author Teamtweaks
 *
 */
class Customer_feedback extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        }
        $this->load->helper(array('cookie', 'date', 'form'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->model('customer_feedback_model');

//         $method = $this->router->fetch_method();
// 		if($method == 'display_product_quantity_details' || $method == 'get_all_product_listing' || $method == 'update_city_qty') {
// 			if($this->checkPrivileges('product_quantity',$this->privStatus) == FALSE) {
// 				redirect('admin');
// 			}
// 		} else if ($this->checkPrivileges('product',$this->privStatus) == FALSE) {
// 	 		redirect('admin');
// 		}
    }
    
    public function customer_feedback_listing(){
        $this->data['heading'] = 'Customer Feedback List';
        $this->data['customerReviewList'] = $this->customer_feedback_model->get_all_details(CUSTOMER_REVIEW, '1 = 1');
        $this->load->view('admin/customer_review/customer_review_list', $this->data);
    }
    
    public function add_customer_feedback($id = null){
        if($id){
            $this->data['heading'] = 'Edit Customer Feedback';    
            $this->data['customerFeedbackDetails'] = $this->customer_feedback_model->get_all_details(CUSTOMER_REVIEW, ['id' => $id])->result()[0];
        }else{
            $this->data['heading'] = 'Add Customer Feedback';
        }
        $get_published_customer_feedback = $this->customer_feedback_model->get_all_details(CUSTOMER_REVIEW, ['status' => 'Publish']);
        
        $this->form_validation->set_rules('customer_name', 'Customer Name', 'required|trim');
        $this->form_validation->set_rules('customer_comment', 'Customer Comment', 'required|trim');
        $this->form_validation->set_rules('customer_place', 'Customer Place', 'required|trim');
        
        if($this->form_validation->run()){
            $config['overwrite'] = FALSE;
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['upload_path'] = './images/customer_feedback';
            $this->load->library('upload', $config);
            if(!$id){
                if(empty($_FILES['product_image']['name'])){
                   $this->session->set_flashdata('error', 'Please select product image'); 
                }else{
                    
                    
                    if($this->upload->do_upload('product_image')){
                        $addData = array();
                        $product_image = $this->upload->data();
                        $addData['product_image'] = $product_image['file_name'];
                        $addData['customer_name'] = $this->input->post('customer_name');
                        $addData['customer_comment'] = $this->input->post('customer_comment');
                        $addData['customer_place'] = $this->input->post('customer_place');
                        // if($get_published_customer_feedback->num_rows() >= 4){
                        //     $addData['status'] = 'Unpublish';
                        // }else{
                        //     $addData['status'] = 'Publish';
                        // }
                        $this->customer_feedback_model->commonInsertUpdate(CUSTOMER_REVIEW, 'insert', [], $addData, "");
                        $this->setErrorMessage('success', 'Customer review added successfully');
                        redirect('admin/customer_feedback/customer_feedback_listing');
                    }else{
                        $this->setErrorMessage('error', $this->upload->display_errors());
                        redirect('admin/customer_feedback/add_customer_feedback');
                    }
                }
            }else{
                $updateData = array();
                $updateData['customer_name'] = $this->input->post('customer_name');
                $updateData['customer_comment'] = $this->input->post('customer_comment');
                $updateData['customer_place'] = $this->input->post('customer_place');
                if(!empty($_FILES['product_image']['name'])){
                    if($this->upload->do_upload('product_image')){
                        $product_image = $this->upload->data();
                        $updateData['product_image'] = $product_image['file_name'];
                    }else{
                        $this->setErrorMessage('error', $this->upload->display_errors());
                        redirect('admin/customer_feedback/edit_customer_feedback/'.$id);
                    }
                }
                $this->customer_feedback_model->commonInsertUpdate(CUSTOMER_REVIEW, 'update', [], $updateData, ['id' => $id]);
                $this->setErrorMessage('success', 'Customer review updated successfully');
                redirect('admin/customer_feedback/customer_feedback_listing');
            }
            
        }
        $this->load->view('admin/customer_review/add_customer_feedback', $this->data);
    }
    
    public function change_customer_feedback_status($status, $id){
        $get_published_customer_feedback = $this->customer_feedback_model->get_all_details(CUSTOMER_REVIEW, ['status' => 'Publish']);
        if($status == 'Publish'){
            $this->customer_feedback_model->commonInsertUpdate(CUSTOMER_REVIEW, 'update', [], ['status' => 'Unpublish'], ['id' => $id]);
            $this->setErrorMessage('success', 'Customer feedback status updated successfully.');
            redirect('admin/customer_feedback/customer_feedback_listing');
        }else{
            $this->customer_feedback_model->commonInsertUpdate(CUSTOMER_REVIEW, 'update', [], ['status' => 'Publish'], ['id' => $id]);
            $this->setErrorMessage('success', 'Customer feedback status updated successfully.');
            redirect('admin/customer_feedback/customer_feedback_listing');
        }
    }
}