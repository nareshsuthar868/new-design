<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * This controller contains the functions related to Product management
 * @author Teamtweaks
 *
 */
class Product extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper(array('cookie', 'date', 'form'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->model('product_model');

        $method = $this->router->fetch_method();
		if($method == 'display_product_quantity_details' || $method == 'get_all_product_listing' || $method == 'update_city_qty') {
			if($this->checkPrivileges('product_quantity',$this->privStatus) == FALSE) {
				redirect('admin');
			}
		} else if ($this->checkPrivileges('product',$this->privStatus) == FALSE) {
	 		redirect('admin');
		}
    }

    /**
     *
     * This function loads the product list page
     */
    public function index() {
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            redirect('admin/product/display_product_list');
        }
    }

    /**
     *
     * This function loads the selling product list page
     */
    public function display_product_list() {
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $this->data['heading'] = 'Selling Product List';
            $this->data['productList'] = $this->product_model->view_product_details('  where u.group="Seller" and u.status="Active" or p.user_id=0 order by p.status asc');
            $this->load->view('admin/product/display_product_list', $this->data);
        }
    }

    /**
     *
     * This function loads the affiliate product list page
     */
    public function display_user_product_list() {
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $this->data['heading'] = 'Affiliate Product List';
            $this->data['productList'] = $this->product_model->view_notsell_product_details();
            $this->load->view('admin/product/display_user_product_list', $this->data);
        }
    }

    /*     * ****** This function loads the add new product form ********* */

    public function add_pricing() {

        $this->data['heading'] = 'Add Price';
        $this->load->view('admin/pricing/add_pricing', $this->data);
    }

    /**
     *
     * This function loads the add new product form
     */
    public function add_product_form() {

        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {

            $this->data['addon_prod_list'] = $this->product_model->view_addon_prod();
            $this->data['sub_products_list'] = $this->product_model->get_sub_products();
            $this->data['sub_products_list'] = $this->product_model->get_sub_products();
            //var_dump($this->data['sub_products_list']);
            $product_id = $this->uri->segment(4, 0);
            $condition = array('id' => $product_id);

            $this->data['product_details'] = $this->product_model->view_product($condition);

            $this->data['heading'] = 'Add New Product';
            $this->data['Product_id'] = $this->uri->segment(4, 0);
            $this->data['categoryView'] = $this->product_model->view_category_details();
            $this->data['atrributeValue'] = $this->product_model->view_atrribute_details();
            $this->data['PrdattrVal'] = $this->product_model->view_product_atrribute_details();
            $this->load->view('admin/product/add_product', $this->data);
        }
    }

    /**
     *
     * This function insert and edit product
     */
    public function insertEditProduct() {
        $slider = $this->input->post('slider_view');
        $min_tenure = $this->input->post('min_tenure');
        $rental_freq = $this->input->post('rental_freq');

        if ($slider != '') {
            $slider = 'yes';
        } else {
            $slider = 'no';
        }
        //		echo "<pre>";print_r($_POST);die;
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $product_name = $this->input->post('product_name');
            $product_id = $this->input->post('productID');
            if ($product_name == '') {
                $this->setErrorMessage('error', 'Product name required');
                //				redirect('admin/product/add_product_form');
                echo "<script>window.history.go(-1)</script>";
                exit();
            }

            $sale_price = $this->input->post('sale_price');
            $is_add_on = $this->input->post('is_addon');
            $is_sub_product = $this->input->post('is_sub_product');
            $brand = $this->input->post('brand');
            $material = $this->input->post('material');
            $colour = $this->input->post('colour');
            $display_excerpt = $this->input->post('display_excerpt');

            if ($sale_price == '') {
                $this->setErrorMessage('error', 'Sale price required');
                //				redirect('admin/product/add_product_form');
                echo "<script>window.history.go(-1)</script>";
                exit();
            } else if ($sale_price <= 0) {
                $this->setErrorMessage('error', 'Sale price must be greater than zero');
                echo "<script>window.history.go(-1)</script>";
                exit();
                //redirect('admin/product/add_product_form');
            }
            if ($product_id == '') {
                $old_product_details = array();
                $condition = array('product_name' => $product_name);
            } else {
                $old_product_details = $this->product_model->get_all_details(PRODUCT, array('id' => $product_id));
                $condition = array('product_name' => $product_name, 'id !=' => $product_id);
            }
            /* 			$duplicate_name = $this->product_model->get_all_details(PRODUCT,$condition);
              if ($duplicate_name->num_rows() > 0){
              $this->setErrorMessage('error','Product name already exists');
              echo "<script>window.history.go(-1)</script>";exit();
              }
             */ $price_range = '';
            if ($sale_price > 0 && $sale_price < 21) {
                $price_range = '1-20';
            } else if ($sale_price > 20 && $sale_price < 101) {
                $price_range = '21-100';
            } else if ($sale_price > 100 && $sale_price < 201) {
                $price_range = '101-200';
            } else if ($sale_price > 200 && $sale_price < 501) {
                $price_range = '201-500';
            } else if ($sale_price > 500) {
                $price_range = '501+';
            }
            $excludeArr = array("filters","gateway_tbl_length", "imaged", "productID", "changeorder", "status", "category_id", "attribute_name", "attribute_val", "attribute_weight", "brand", "attribute_price", "product_image", "userID", "product_attribute_name", "product_attribute_val", "attr_name1", "attr_val1", "attr_type1", "product_attribute_type", "addon_checkbox_id", "subproduct_checkbox_id");

            if ($this->input->post('status') != '') {
                $product_status = 'Publish';
            } else {
                $product_status = 'UnPublish';
            }

            if ($is_add_on != '') {
                $addon = '1';
            } else {
                $addon = '0';
            }


            if ($is_sub_product != '') {
                $sub_product = '1';
            } else {
                $sub_product = '0';
            }

            $seourl = url_title($product_name, '-', TRUE);
            $checkSeo = $this->product_model->get_all_details(PRODUCT, array('seourl' => $seourl, 'id !=' => $product_id));
            $seo_count = 1;
            while ($checkSeo->num_rows() > 0) {
                $seourl = $seourl . $seo_count;
                $seo_count++;
                $checkSeo = $this->product_model->get_all_details(PRODUCT, array('seourl' => $seourl, 'id !=' => $product_id));
            }
            if ($this->input->post('category_id') != '') {
                $category_id = implode(',', $this->input->post('category_id'));
            } else {
                $category_id = '';
            }
            $ImageName = '';
            $list_name_str = $list_val_str = '';
            $list_name_arr = $this->input->post('attribute_name');
            $list_val_arr = $this->input->post('attribute_val');
            if (is_array($list_name_arr) && count($list_name_arr) > 0) {
                $list_name_str = implode(',', $list_name_arr);
                $list_val_str = implode(',', $list_val_arr);
            }
            $addon_ids_array = array();
            $subproduct_ids_array = array();
            $recommended_checkbox_ids_array = array();

            $addon_ids_array = $this->input->post('addon_checkbox_id');
            if (count($addon_ids_array) > 0) {
                $addon_prod_ids = implode(',', $addon_ids_array);
            } else {
                $addon_prod_ids = '';
            }

            $subproduct_ids_array = $this->input->post('subproduct_checkbox_id');

            if (count($subproduct_ids_array) > 0) {
                $sub_prod_ids = implode(',', $subproduct_ids_array);
            } else {
                $sub_prod_ids = '';
            }
            
            $recommended_checkbox_ids_array = $this->input->post('recommended_products');
            if (count($recommended_checkbox_ids_array) > 0) {
                $recommended_checkbox_ids = implode(',', $recommended_checkbox_ids_array);
            } else {
                $recommended_checkbox_ids = '';
            }
            
            $you_might_also_like_checkbox_ids_array = $this->input->post('you_might_also_like');
            if (count($you_might_also_like_checkbox_ids_array) > 0) {
                $you_might_also_like_checkbox_ids = implode(',', $you_might_also_like_checkbox_ids_array);
            } else {
                $you_might_also_like_checkbox_ids = '';
            }
            //addon_checkbox_id
            //			$option['attribute_name'] = $this->input->post('attribute_name');
            //			$option['attribute_val'] = $this->input->post('attribute_val');
            //			$option['attribute_weight'] = $this->input->post('attribute_weight');
            //			$option['attribute_price'] = $this->input->post('attribute_price');
            $datestring = "%Y-%m-%d %h:%i:%s";
            $time = time();

            if ($product_id == '') {
                $inputArr = array(
                    'created' => mdate($datestring, $time),
                    'seourl' => $seourl,
                    'category_id' => $category_id,
                    'status' => $product_status,
                    'list_name' => $list_name_str,
                    'list_value' => $list_val_str,
                    'price_range' => $price_range,
                    'display_excerpt' => $display_excerpt,
                    'brand' => trim($brand),
                    'material' => trim($material),
                    'colour' => trim($colour),
                    'is_addon' => $addon,
                    'is_sub_product' => $sub_product,
                    'user_id' => $this->input->post('userID'),
                    'seller_product_id' => time(),
                    'addons' => $addon_prod_ids,
                    'subproducts' => $sub_prod_ids,
                    'recommended_products' => $recommended_checkbox_ids,
                    'you_might_also_like' => $you_might_also_like_checkbox_ids,
                    'slider_view' => $slider,
                    'min_tenure' => $min_tenure,
                    'rental_freq' => $rental_freq
                );
            } else {
                $inputArr = array(
                    'modified' => mdate($datestring, $time),
                    'seourl' => $seourl,
                    'category_id' => $category_id,
                    'is_addon' => $addon,
                    'is_sub_product' => $sub_product,
                    'status' => $product_status,
                    'price_range' => $price_range,
                    'display_excerpt' => $display_excerpt,
                    'brand' => trim($brand),
                    'material' => trim($material),
                    'colour' => trim($colour),
                    'list_name' => $list_name_str,
                    'list_value' => $list_val_str,
                    'addons' => $addon_prod_ids,
                    'subproducts' => $sub_prod_ids,
                    'recommended_products' => $recommended_checkbox_ids,
                    'you_might_also_like' => $you_might_also_like_checkbox_ids,
                    'slider_view' => $slider,
                    'min_tenure' => $min_tenure,
                    'rental_freq' => $rental_freq,
                    'filter_tag' =>implode(',',$this->input->post('filters')) 
                        //						 	'option' => serialize($option)
                );
            }
            //print_r($inputArr);exit;
            $config['overwrite'] = FALSE;
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['upload_path'] = './images/product';
            $this->load->library('upload', $config);


            if ($this->upload->do_multi_upload('product_image')) {
                $logoDetails = $this->upload->get_multi_upload_data();
                foreach ($logoDetails as $fileDetails) {
                    if ($fileDetails['image_width'] < 550 && $fileDetails['image_height'] < 1680) {
                        $this->session->set_flashdata('error', 'Please select big images');
                        redirect('admin/product/display_product_list');
                    } else {

                        $this->imageResizeWithSpace(800, 600, $fileDetails['file_name'], './images/product/');
                        $ImageName .= $fileDetails['file_name'] . ',';
                    }
                }
            }
            
            if($_POST['imaged']){
				$zero_index_img = array_search('0', $_POST['changeorder']);
	 			if($_POST['imaged'][$zero_index_img]){
					$this->generateThumbnail($config['upload_path'].'/'.$_POST['imaged'][$zero_index_img],'./images/product/Copressed Images/'.$_POST['imaged'][$zero_index_img],360,270);
	 			}
			}
			
            if ($product_id == '') {
                $product_data = array('image' => $ImageName);
            } else {
                $existingImage = $this->input->post('imaged');

                $newPOsitionArr = $this->input->post('changeorder');
                $imagePOsit = array();

                for ($p = 0; $p < sizeof($existingImage); $p++) {
                    $imagePOsit[$newPOsitionArr[$p]] = $existingImage[$p];
                }

                ksort($imagePOsit);
                foreach ($imagePOsit as $keysss => $vald) {
                    $imgArraypos[] = $vald;
                }
                $imagArraypo0 = @implode(",", $imgArraypos);
                $allImages = $imagArraypo0 . ',' . $ImageName;

                $product_data = array('image' => $allImages);
            }
            if ($product_id != '') {
                $this->update_old_list_values($product_id, $list_val_arr, $old_product_details);
            }
            $dataArr = array_merge($inputArr, $product_data);
            
            if ($product_id == '') {
                $condition = array();
                $this->product_model->commonInsertUpdate(PRODUCT, 'insert', $excludeArr, $dataArr, $condition);
                $product_id = $this->product_model->get_last_insert_id();
                
                if($product_id != '') {
                   $header_snippet = $this->input->post('header_code_snippet');
					if($header_snippet == '') {
						$header_snippet = '<link rel="canonical" href="'.base_url().'things/'.$product_id.'/'.$seourl.'" />';
					}

					$quantityData_array = array('header_code_snippet' => $header_snippet);
					$condition  = array('id' => $product_id);
					$this->product_model->update_details(PRODUCT, $quantityData_array, $condition);
                }

                //Generate short url
                $short_url = $this->get_rand_str('6');
                $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $short_url));
                while ($checkId->num_rows() > 0) {
                    $short_url = $this->get_rand_str('6');
                    $checkId = $this->product_model->get_all_details(SHORTURL, array('short_url' => $short_url));
                }
                $url = base_url() . 'things/' . $product_id . '/' . url_title($product_name, '-');
                $this->product_model->simple_insert(SHORTURL, array('short_url' => $short_url, 'long_url' => $url));
                $urlid = $this->product_model->get_last_insert_id();
                $this->product_model->update_details(PRODUCT, array('short_url_id' => $urlid), array('id' => $product_id));
                ////////////////////

                $Attr_name_str = $Attr_val_str = '';
                $Attr_type_arr = $this->input->post('product_attribute_type');
                $Attr_name_arr = $this->input->post('product_attribute_name');
                $Attr_val_arr = $this->input->post('product_attribute_val');
                if (is_array($Attr_name_arr) && count($Attr_name_arr) > 0) {
                    for ($k = 0; $k < sizeof($Attr_name_arr); $k++) {
                        $dataSubArr = '';
                        $dataSubArr = array('product_id' => $product_id, 'attr_id' => $Attr_type_arr[$k], 'attr_name' => $Attr_name_arr[$k], 'attr_price' => $Attr_val_arr[$k]);
                        //echo '<pre>'; print_r($dataSubArr);
                        $this->product_model->add_subproduct_insert($dataSubArr);
                    }
                }

                $this->setErrorMessage('success', 'Product added successfully');
                $product_id = $this->product_model->get_last_insert_id();
                $this->update_price_range_in_table('add', $price_range, $product_id, $old_product_details);
            } else {
                $condition = array('id' => $product_id);
                
                $this->product_model->commonInsertUpdate(PRODUCT, 'update', $excludeArr, $dataArr, $condition);
                $Attr_name_str = $Attr_val_str = '';
                $Attr_type_arr = $this->input->post('product_attribute_type');
                $Attr_name_arr = $this->input->post('product_attribute_name');
                $Attr_val_arr = $this->input->post('product_attribute_val');
                if (is_array($Attr_name_arr) && count($Attr_name_arr) > 0) {
                    for ($k = 0; $k < sizeof($Attr_name_arr); $k++) {
                        $dataSubArr = '';
                        $dataSubArr = array('product_id' => $product_id, 'attr_id' => $Attr_type_arr[$k], 'attr_name' => $Attr_name_arr[$k], 'attr_price' => $Attr_val_arr[$k]);
                        //echo '<pre>'; print_r($dataSubArr);
                        $this->product_model->add_subproduct_insert($dataSubArr);
                    }
                }

                $this->setErrorMessage('success', 'Product updated successfully');
                $this->update_price_range_in_table('edit', $price_range, $product_id, $old_product_details);
            }
            
            // if(count($this->input->post('filters')) > 0){
            //     $this->product_model->commonDelete(APPLIED_FILTERS,array('product_id' => $product_id));
            //     foreach($this->input->post('filters') as $key => $val){
            //         $filters[] = array('product_id' => $product_id,'filter_tag' => $val);
            //     }
            //     $this->db->insert_batch(APPLIED_FILTERS, $filters); 
            // }

            //Update the list table
            if (is_array($list_val_arr)) {
                foreach ($list_val_arr as $list_val_row) {
                    $list_val_details = $this->product_model->get_all_details(LIST_VALUES, array('id' => $list_val_row));
                    if ($list_val_details->num_rows() == 1) {
                        $product_count = $list_val_details->row()->product_count;
                        $products_in_this_list = $list_val_details->row()->products;
                        $products_in_this_list_arr = explode(',', $products_in_this_list);
                        if (!in_array($product_id, $products_in_this_list_arr)) {
                            array_push($products_in_this_list_arr, $product_id);
                            $product_count++;
                            $list_update_values = array(
                                'products' => implode(',', $products_in_this_list_arr),
                                'product_count' => $product_count
                            );
                            $list_update_condition = array('id' => $list_val_row);
                            $this->product_model->update_details(LIST_VALUES, $list_update_values, $list_update_condition);
                        }
                    }
                }
            }

            //Update user table count
            if ($edit_mode == 'insert') {
                if ($this->checkLogin('U') != '') {
                    $user_details = $this->product_model->get_all_details(USERS, array('id' => $this->checkLogin('U')));
                    if ($user_details->num_rows() == 1) {
                        $prod_count = $user_details->row()->products;
                        $prod_count++;
                        $this->product_model->update_details(USERS, array('products' => $prod_count), array('id' => $this->checkLogin('U')));
                    }
                }
            }
            $this->upload_more($_FILES['product_image']);

            redirect('admin/product/display_product_list');
        }
    }

    public function upload_more($image) {
        $config1['overwrite'] = false;
        $config1['allowed_types'] = 'jpg|jpeg|gif|png';
        $config1['upload_path'] = './images/product/slider_view';
        $this->upload->initialize($config1);

        if ($this->upload->do_multi_upload('product_image')) {
            $logoDetails = $this->upload->get_multi_upload_data();
            foreach ($logoDetails as $fileDetails) {

                $this->imageResizeWithSpace(1920, 600, $fileDetails['file_name'], './images/product/slider_view');
            }
        }
    }

    public function insertEditAffiliateProduct() {
//		echo "<pre>";print_r($_POST);die;
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $product_name = $this->input->post('product_name');
            $product_id = $this->input->post('productID');
            if ($product_name == '') {
                $this->setErrorMessage('error', 'Product name required');
//				redirect('admin/product/add_product_form');
                echo "<script>window.history.go(-1)</script>";
                exit();
            }
            $sale_price = $this->input->post('sale_price');
            if ($sale_price == '') {
                $this->setErrorMessage('error', 'Sale price required');
//				redirect('admin/product/add_product_form');
                echo "<script>window.history.go(-1)</script>";
                exit();
            } else if ($sale_price <= 0) {
                $this->setErrorMessage('error', 'Sale price must be greater than zero');
                echo "<script>window.history.go(-1)</script>";
                exit();
                //redirect('admin/product/add_product_form');
            }
            if ($product_id == '') {
                $old_product_details = array();
                $condition = array('product_name' => $product_name);
            } else {
                $old_product_details = $this->product_model->get_all_details(USER_PRODUCTS, array('id' => $product_id));
                $condition = array('product_name' => $product_name, 'id !=' => $product_id);
            }
            /* 			$duplicate_name = $this->product_model->get_all_details(PRODUCT,$condition);
              if ($duplicate_name->num_rows() > 0){
              $this->setErrorMessage('error','Product name already exists');
              echo "<script>window.history.go(-1)</script>";exit();
              }
             */
            $excludeArr = array("gateway_tbl_length", "imaged", "productID", "changeorder", "status", "category_id", "attribute_name", "attribute_val", "attribute_weight", "attribute_price", "product_image", "userID");

            if ($this->input->post('status') != '') {
                $product_status = 'Publish';
            } else {
                $product_status = 'UnPublish';
            }

            $seourl = url_title($product_name, '-', TRUE);
            $checkSeo = $this->product_model->get_all_details(USER_PRODUCTS, array('seourl' => $seourl, 'id !=' => $product_id));
            $seo_count = 1;
            while ($checkSeo->num_rows() > 0) {
                $seourl = $seourl . $seo_count;
                $seo_count++;
                $checkSeo = $this->product_model->get_all_details(USER_PRODUCTS, array('seourl' => $seourl, 'id !=' => $product_id));
            }
            if ($this->input->post('category_id') != '') {
                $category_id = implode(',', $this->input->post('category_id'));
            } else {
                $category_id = '';
            }
            $ImageName = '';
            $list_name_str = $list_val_str = '';
            $list_name_arr = $this->input->post('attribute_name');
            $list_val_arr = $this->input->post('attribute_val');
            if (is_array($list_name_arr) && count($list_name_arr) > 0) {
                $list_name_str = implode(',', $list_name_arr);
                $list_val_str = implode(',', $list_val_arr);
            }
//			$option['attribute_name'] = $this->input->post('attribute_name');
//			$option['attribute_val'] = $this->input->post('attribute_val');
//			$option['attribute_weight'] = $this->input->post('attribute_weight');
//			$option['attribute_price'] = $this->input->post('attribute_price');
            $datestring = "%Y-%m-%d %h:%i:%s";
            $time = time();
            if ($product_id == '') {
                $inputArr = array(
                    'created' => mdate($datestring, $time),
                    'seourl' => $seourl,
                    'category_id' => $category_id,
                    'status' => $product_status,
                    'list_name' => $list_name_str,
                    'list_value' => $list_val_str,
//							'option' => serialize($option),
                    'user_id' => $this->input->post('userID'),
                    'seller_product_id' => mktime()
                );
            } else {
                $inputArr = array(
                    'modified' => mdate($datestring, $time),
                    'seourl' => $seourl,
                    'category_id' => $category_id,
                    'status' => $product_status,
                    'list_name' => $list_name_str,
                    'list_value' => $list_val_str
//							'option' => serialize($option)
                );
            }
            //$config['encrypt_name'] = TRUE;
            $config['overwrite'] = FALSE;
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
//		    $config['max_size'] = 2000;
            $config['upload_path'] = './images/product';
            $this->load->library('upload', $config);
            //echo "<pre>";print_r($_FILES);die;
            if ($this->upload->do_multi_upload('product_image')) {
                $logoDetails = $this->upload->get_multi_upload_data();
                foreach ($logoDetails as $fileDetails) {
                    $this->imageResizeWithSpace(800, 600, $fileDetails['file_name'], './images/product/');
                    $ImageName .= $fileDetails['file_name'] . ',';
                }
            }
            if ($product_id == '') {
                $product_data = array('image' => $ImageName);
            } else {
                $existingImage = $this->input->post('imaged');

                $newPOsitionArr = $this->input->post('changeorder');
                $imagePOsit = array();

                for ($p = 0; $p < sizeof($existingImage); $p++) {
                    $imagePOsit[$newPOsitionArr[$p]] = $existingImage[$p];
                }

                ksort($imagePOsit);
                foreach ($imagePOsit as $keysss => $vald) {
                    $imgArraypos[] = $vald;
                }
                $imagArraypo0 = @implode(",", $imgArraypos);
                $allImages = $imagArraypo0 . ',' . $ImageName;

                $product_data = array('image' => $allImages);
            }
            if ($product_id != '') {
                $this->update_old_list_values($product_id, $list_val_arr, $old_product_details);
            }
            $dataArr = array_merge($inputArr, $product_data);
            if ($product_id == '') {
                $condition = array();
                $this->product_model->commonInsertUpdate(USER_PRODUCTS, 'insert', $excludeArr, $dataArr, $condition);
                $this->setErrorMessage('success', 'Product added successfully');
                $product_id = $this->product_model->get_last_insert_id();
                $this->update_price_range_in_table('add', $price_range, $product_id, $old_product_details);
            } else {
                $condition = array('id' => $product_id);
                $this->product_model->commonInsertUpdate(USER_PRODUCTS, 'update', $excludeArr, $dataArr, $condition);
                $this->setErrorMessage('success', 'Product updated successfully');
                $this->update_price_range_in_table('edit', $price_range, $product_id, $old_product_details);
            }

            //Update the list table
            if (is_array($list_val_arr)) {
                foreach ($list_val_arr as $list_val_row) {
                    $list_val_details = $this->product_model->get_all_details(LIST_VALUES, array('id' => $list_val_row));
                    if ($list_val_details->num_rows() == 1) {
                        $product_count = $list_val_details->row()->product_count;
                        $products_in_this_list = $list_val_details->row()->products;
                        $products_in_this_list_arr = explode(',', $products_in_this_list);
                        if (!in_array($product_id, $products_in_this_list_arr)) {
                            array_push($products_in_this_list_arr, $product_id);
                            $product_count++;
                            $list_update_values = array(
                                'products' => implode(',', $products_in_this_list_arr),
                                'product_count' => $product_count
                            );
                            $list_update_condition = array('id' => $list_val_row);
                            $this->product_model->update_details(LIST_VALUES, $list_update_values, $list_update_condition);
                        }
                    }
                }
            }

            //Update user table count
            if ($edit_mode == 'insert') {
                if ($this->checkLogin('U') != '') {
                    $user_details = $this->product_model->get_all_details(USERS, array('id' => $this->checkLogin('U')));
                    if ($user_details->num_rows() == 1) {
                        $prod_count = $user_details->row()->products;
                        $prod_count++;
                        $this->product_model->update_details(USERS, array('products' => $prod_count), array('id' => $this->checkLogin('U')));
                    }
                }
            }

            redirect('admin/product/display_user_product_list');
        }
    }

    /**
     *
     * This function insert and edit affliated product
     */
    public function insertEditaffliatedProduct() {
        //echo "<pre>";print_r($_POST);die;
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $product_name = $this->input->post('product_name');
            $product_id = $this->input->post('productID');
            if ($product_name == '') {
                $this->setErrorMessage('error', 'Product name required');
                //				redirect('admin/product/add_product_form');
                echo "<script>window.history.go(-1)</script>";
                exit();
            }
            /* $sale_price = $this->input->post('sale_price');
              if ($sale_price == ''){
              $this->setErrorMessage('error','Sale price required');
              //				redirect('admin/product/add_product_form');
              echo "<script>window.history.go(-1)</script>";exit();
              }else if ($sale_price <= 0){
              $this->setErrorMessage('error','Sale price must be greater than zero');
              echo "<script>window.history.go(-1)</script>";exit();
              //redirect('admin/product/add_product_form');
              } */
            if ($product_id == '') {
                $old_product_details = array();
                $condition = array('product_name' => $product_name);
            } else {
                $old_product_details = $this->product_model->get_all_details(USER_PRODUCTS, array('id' => $product_id));
                $condition = array('product_name' => $product_name, 'id !=' => $product_id);
            }
            /* 			$duplicate_name = $this->product_model->get_all_details(PRODUCT,$condition);
              if ($duplicate_name->num_rows() > 0){
              $this->setErrorMessage('error','Product name already exists');
              echo "<script>window.history.go(-1)</script>";exit();
              }
             */ $price_range = '';
            if ($sale_price > 0 && $sale_price < 21) {
                $price_range = '1-20';
            } else if ($sale_price > 20 && $sale_price < 101) {
                $price_range = '21-100';
            } else if ($sale_price > 100 && $sale_price < 201) {
                $price_range = '101-200';
            } else if ($sale_price > 200 && $sale_price < 501) {
                $price_range = '201-500';
            } else if ($sale_price > 500) {
                $price_range = '501+';
            }
            $excludeArr = array("gateway_tbl_length", "imaged", "productID", "changeorder", "status", "category_id", "attribute_name", "attribute_val", "attribute_weight", "attribute_price", "product_image", "userID", "category_id", "web_link");

            if ($this->input->post('status') != '') {
                $product_status = 'Publish';
            } else {
                $product_status = 'UnPublish';
            }

            $seourl = url_title($product_name, '-', TRUE);
            $checkSeo = $this->product_model->get_all_details(USER_PRODUCTS, array('seourl' => $seourl, 'id !=' => $product_id));
            $seo_count = 1;
            while ($checkSeo->num_rows() > 0) {
                $seourl = $seourl . $seo_count;
                $seo_count++;
                $checkSeo = $this->product_model->get_all_details(USER_PRODUCTS, array('seourl' => $seourl, 'id !=' => $product_id));
            }
            if ($this->input->post('category_id') != '') {
                $category_id = implode(',', $this->input->post('category_id'));
            } else {
                $category_id = '';
            }
            $ImageName = '';
            $list_name_str = $list_val_str = '';
            $list_name_arr = $this->input->post('attribute_name');
            $list_val_arr = $this->input->post('attribute_val');
            if (is_array($list_name_arr) && count($list_name_arr) > 0) {
                $list_name_str = implode(',', $list_name_arr);
                $list_val_str = implode(',', $list_val_arr);
            }
            //			$option['attribute_name'] = $this->input->post('attribute_name');
            //			$option['attribute_val'] = $this->input->post('attribute_val');
            //			$option['attribute_weight'] = $this->input->post('attribute_weight');
            //			$option['attribute_price'] = $this->input->post('attribute_price');
            $datestring = "%Y-%m-%d %h:%i:%s";
            $time = time();
            if ($product_id == '') {
                $inputArr = array(
                    'created' => mdate($datestring, $time),
                    'seourl' => $seourl,
                    'category_id' => $this->input->post('category_id'),
                    'web_link' => $this->input->post('web_link'),
                    'status' => $product_status,
                    'list_name' => $list_name_str,
                    'list_value' => $list_val_str,
                    //							'price_range'=> $price_range,
                    //							'option' => serialize($option),
                    'user_id' => $this->input->post('userID'),
                    'seller_product_id' => mktime()
                );
            } else {
                $inputArr = array(
                    'modified' => mdate($datestring, $time),
                    'seourl' => $seourl,
                    'category_id' => $this->input->post('category_id'),
                    'web_link' => $this->input->post('web_link'),
                    'status' => $product_status,
                    //							'price_range'=> $price_range,
                    'list_name' => $list_name_str,
                    'list_value' => $list_val_str
                        //							'option' => serialize($option)
                );
            }
            //$config['encrypt_name'] = TRUE;
            //$config['overwrite'] = FALSE;
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            //		    $config['max_size'] = 2000;
            $config['upload_path'] = './images/product';
            $this->load->library('upload', $config);
            //echo "<pre>";print_r($_FILES);
            if ($this->upload->do_upload('product_image')) {

                //echo 'fdsf'; die;
                $logoDetails = $this->upload->do_upload();
                $logoDetails = $this->upload->data();
                $this->imageResizeWithSpace(800, 600, $logoDetails['file_name'], './images/product/');
                $ImageName = $logoDetails['file_name'];
            }

            if ($product_id == '') {
                $product_data = array('image' => $ImageName);
            } else {
                if ($ImageName == '') {
                    $product_data = array();
                } else {
                    $product_data = array('image' => $ImageName);
                }
            }
            //print_r($product_data); die;

            if ($product_id != '') {
                $this->update_old_list_values($product_id, $list_val_arr, $old_product_details);
            }
            $dataArr = array_merge($inputArr, $product_data);
            if ($product_id == '') {
                $condition = array();
                $this->product_model->commonInsertUpdate(USER_PRODUCTS, 'insert', $excludeArr, $dataArr, $condition);
                $this->setErrorMessage('success', 'Product added successfully');
                $product_id = $this->product_model->get_last_insert_id();
                $this->update_price_range_in_table('add', $price_range, $product_id, $old_product_details);
            } else {
                $condition = array('id' => $product_id);
                $this->product_model->commonInsertUpdate(USER_PRODUCTS, 'update', $excludeArr, $dataArr, $condition);
                $this->setErrorMessage('success', 'Product updated successfully');
                $this->update_price_range_in_table('edit', $price_range, $product_id, $old_product_details);
            }

            //Update the list table
            if (is_array($list_val_arr)) {
                foreach ($list_val_arr as $list_val_row) {
                    $list_val_details = $this->product_model->get_all_details(LIST_VALUES, array('id' => $list_val_row));
                    if ($list_val_details->num_rows() == 1) {
                        $product_count = $list_val_details->row()->product_count;
                        $products_in_this_list = $list_val_details->row()->products;
                        $products_in_this_list_arr = explode(',', $products_in_this_list);
                        if (!in_array($product_id, $products_in_this_list_arr)) {
                            array_push($products_in_this_list_arr, $product_id);
                            $product_count++;
                            $list_update_values = array(
                                'products' => implode(',', $products_in_this_list_arr),
                                'product_count' => $product_count
                            );
                            $list_update_condition = array('id' => $list_val_row);
                            $this->product_model->update_details(LIST_VALUES, $list_update_values, $list_update_condition);
                        }
                    }
                }
            }

            //Update user table count
            if ($edit_mode == 'insert') {
                if ($this->checkLogin('U') != '') {
                    $user_details = $this->product_model->get_all_details(USERS, array('id' => $this->checkLogin('U')));
                    if ($user_details->num_rows() == 1) {
                        $prod_count = $user_details->row()->products;
                        $prod_count++;
                        $this->product_model->update_details(USERS, array('products' => $prod_count), array('id' => $this->checkLogin('U')));
                    }
                }
            }
            //echo 'fdgdgd'; die;
            redirect('admin/product/display_user_product_list');
        }
    }

    /**
     *
     * Update the products_count and products in list_values table, when edit or delete products
     * @param Integer $product_id
     * @param Array $list_val_arr
     * @param Array $old_product_details
     */
    public function update_old_list_values($product_id, $list_val_arr, $old_product_details = '') {
        if ($old_product_details == '' || count($old_product_details) == 0) {
            $old_product_details = $this->product_model->get_all_details(PRODUCT, array('id' => $product_id));
        }
        $old_product_list_values = array_filter(explode(',', $old_product_details->row()->list_value));
        if (count($old_product_list_values) > 0) {
            if (!is_array($list_val_arr)) {
                $list_val_arr = array();
            }
            foreach ($old_product_list_values as $old_product_list_values_row) {
                if (!in_array($old_product_list_values_row, $list_val_arr)) {
                    $list_val_details = $this->product_model->get_all_details(LIST_VALUES, array('id' => $old_product_list_values_row));
                    if ($list_val_details->num_rows() == 1) {
                        $product_count = $list_val_details->row()->product_count;
                        $products_in_this_list = $list_val_details->row()->products;
                        $products_in_this_list_arr = array_filter(explode(',', $products_in_this_list));
                        if (in_array($product_id, $products_in_this_list_arr)) {
                            if (($key = array_search($product_id, $products_in_this_list_arr)) !== false) {
                                unset($products_in_this_list_arr[$key]);
                            }
                            $product_count--;
                            $list_update_values = array(
                                'products' => implode(',', $products_in_this_list_arr),
                                'product_count' => $product_count
                            );
                            $list_update_condition = array('id' => $old_product_list_values_row);
                            $this->product_model->update_details(LIST_VALUES, $list_update_values, $list_update_condition);
                        }
                    }
                }
            }
        }

        if ($old_product_details != '' && count($old_product_details) > 0 && $old_product_details->num_rows() == 1) {

            /*             * * Delete product id from lists which was created by users ** */

            $user_created_lists = $this->product_model->get_user_created_lists($old_product_details->row()->seller_product_id);
            if ($user_created_lists->num_rows() > 0) {
                foreach ($user_created_lists->result() as $user_created_lists_row) {
                    $list_product_ids = array_filter(explode(',', $user_created_lists_row->product_id));
                    if (($key = array_search($old_product_details->row()->seller_product_id, $list_product_ids)) !== false) {
                        unset($list_product_ids[$key]);
                        $update_ids = array('product_id' => implode(',', $list_product_ids));
                        $this->product_model->update_details(LISTS_DETAILS, $update_ids, array('id' => $user_created_lists_row->id));
                    }
                }
            }

            /*             * * Delete product id from product likes table and decrease the user likes count ** */

            $like_list = $this->product_model->get_like_user_full_details($old_product_details->row()->seller_product_id);
            if ($like_list->num_rows() > 0) {
                foreach ($like_list->result() as $like_list_row) {
                    $likes_count = $like_list_row->likes;
                    $likes_count--;
                    if ($likes_count < 0)
                        $likes_count = 0;
                    $this->product_model->update_details(USERS, array('likes' => $likes_count), array('id' => $like_list_row->id));
                }
                $this->product_model->commonDelete(PRODUCT_LIKES, array('product_id' => $old_product_details->row()->seller_product_id));
            }

            /*             * * Delete product id from activity, notification and product comment tables ** */

            $this->product_model->commonDelete(USER_ACTIVITY, array('activity_id' => $old_product_details->row()->seller_product_id));
            $this->product_model->commonDelete(NOTIFICATIONS, array('activity_id' => $old_product_details->row()->seller_product_id));
            $this->product_model->commonDelete(PRODUCT_COMMENTS, array('product_id' => $old_product_details->row()->seller_product_id));
        }
    }

    public function update_price_range_in_table($mode = '', $price_range = '', $product_id = '0', $old_product_details = '') {
        $list_values = $this->product_model->get_all_details(LIST_VALUES, array('list_value' => $price_range));
        if ($list_values->num_rows() == 1) {
            $products = explode(',', $list_values->row()->products);
            $product_count = $list_values->row()->product_count;
            if ($mode == 'add') {
                if (!in_array($product_id, $products)) {
                    array_push($products, $product_id);
                    $product_count++;
                }
            } else if ($mode == 'edit') {
                $old_price_range = '';
                if ($old_product_details != '' && count($old_product_details) > 0 && $old_product_details->num_rows() == 1) {
                    $old_price_range = $old_product_details->row()->price_range;
                }
                if ($old_price_range != '' && $old_price_range != $price_range) {
                    $old_list_values = $this->product_model->get_all_details(LIST_VALUES, array('list_value' => $old_price_range));
                    if ($old_list_values->num_rows() == 1) {
                        $old_products = explode(',', $old_list_values->row()->products);
                        $old_product_count = $old_list_values->row()->product_count;
                        if (in_array($product_id, $old_products)) {
                            if (($key = array_search($product_id, $old_products)) !== false) {
                                unset($old_products[$key]);
                                $old_product_count--;
                                $updateArr = array('products' => implode(',', $old_products), 'product_count' => $old_product_count);
                                $updateCondition = array('list_value' => $old_price_range);
                                $this->product_model->update_details(LIST_VALUES, $updateArr, $updateCondition);
                            }
                        }
                    }
                    if (!in_array($product_id, $products)) {
                        array_push($products, $product_id);
                        $product_count++;
                    }
                } else if ($old_price_range != '' && $old_price_range == $price_range) {
                    if (!in_array($product_id, $products)) {
                        array_push($products, $product_id);
                        $product_count++;
                    }
                }
            }
            $updateArr = array('products' => implode(',', $products), 'product_count' => $product_count);
            $updateCondition = array('list_value' => $price_range);
            $this->product_model->update_details(LIST_VALUES, $updateArr, $updateCondition);
        }
    }

    /**
     *
     * Ajax function for delete the product pictures
     */
    public function editPictureProducts() {
        $ingIDD = $this->input->post('imgId');
        $currentPage = $this->input->post('cpage');
        $id = $this->input->post('val');
        $productImage = explode(',', $this->session->userdata('product_image_' . $ingIDD));
        if (count($productImage) < 2) {
            echo json_encode("No");
            exit();
        } else {
            $empImg = 0;
            foreach ($productImage as $product) {
                if ($product != '') {
                    $empImg++;
                }
            }
            if ($empImg < 2) {
                echo json_encode("No");
                exit();
            }
            $this->session->unset_userdata('product_image_' . $ingIDD);
            $resultVar = $this->setPictureProducts($productImage, $this->input->post('position'));
            $insertArrayItems = trim(implode(',', $resultVar)); //need validation here...because the array key changed here

            $this->session->set_userdata(array('product_image_' . $ingIDD => $insertArrayItems));
            $dataArr = array('image' => $insertArrayItems);
            $condition = array('id' => $ingIDD);
            $this->product_model->update_details(PRODUCT, $dataArr, $condition);
            echo json_encode($insertArrayItems);
        }
    }

    /**
     *
     * This function loads the edit product form
     */
    public function edit_product_form() {
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $this->data['heading'] = 'Edit Product';
            $this->data['addon_prod_list'] = $this->product_model->view_addon_prod();
            $this->data['sub_products_list'] = $this->product_model->get_sub_products();

            $product_id = $this->uri->segment(4, 0);
            $condition = array('id' => $product_id);

            $this->data['product_details'] = $this->product_model->view_product($condition);
            if ($this->data['product_details']->num_rows() == 1) {
                $this->data['allProducts'] = $this->product_model->get_all_details(PRODUCT, ['id !=' => $product_id, 'is_addon' => 0, 'status' => 'Publish'])->result();
                $this->data['categoryView'] = $this->product_model->get_category_details($this->data['product_details']->row()->category_id);
                // print_r(  $this->data['categoryView']);exit;
                $this->data['atrributeValue'] = $this->product_model->view_atrribute_details();
                $this->data['SubPrdVal'] = $this->product_model->view_subproduct_details($product_id);
                $this->data['PrdattrVal'] = $this->product_model->view_product_atrribute_details();
                $this->data['filters'] = $this->product_model->get_all_details(FILTERS,'category_id IN ('.$this->data['product_details']->row()->category_id.')');
                // $my_filter = $this->product_model->get_all_details(APPLIED_FILTERS,array('product_id' => $this->data['product_details']->row()->id));
                // $this->data['myfilters'] = $my_filter->result();
                $this->data['category_id'] = explode(',',$this->data['product_details']->row()->category_id);
                $this->load->view('admin/product/edit_product', $this->data);
            } else {
                redirect('admin');
            }
        }
    }

    /* Ajax update for edit product */

    public function ajaxProductAttributeUpdate() {

        $conditons = array('pid' => $this->input->post('pid'));
        $dataArr = array('attr_id' => $this->input->post('attId'), 'attr_name' => $this->input->post('attname'), 'attr_price' => $this->input->post('attprice'));
        $subproductDetails = $this->product_model->edit_subproduct_update($dataArr, $conditons);
    }

    public function remove_attr() {
        if ($this->checkLogin('A') != '') {
            $this->product_model->commonDelete(SUBPRODUCT, array('pid' => $this->input->post('pid')));
        }
    }

    /**
     *
     * This function loads the edit affliated product form
     */
    public function edit_affliated_form() {
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $this->data['heading'] = 'Edit Affliated Product';
            $product_id = $this->uri->segment(4, 0);
            $condition = array('id' => $product_id);
            $this->data['product_details'] = $this->product_model->view_affliated($condition);
            if ($this->data['product_details']->num_rows() == 1) {
                $this->data['categoryView'] = $this->product_model->get_category_details($this->data['product_details']->row()->category_id);
                $this->data['mainCategories'] = $this->product_model->get_all_details(CATEGORY, array('rootID' => '0', 'status' => 'Active'), $sortArr);

                //echo $this->db->last_query(); die;
                //$this->data['categoryView'] = $this->product_model->get_category_details($productDetails->row()->category_id);

                $this->data['atrributeValue'] = $this->product_model->view_atrribute_details();
                $this->load->view('admin/product/edit_affliated', $this->data);
            } else {
                redirect('admin');
            }
        }
    }

    /**
     *
     * This function change the selling product status
     */
    public function change_product_status() {
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $mode = $this->uri->segment(4, 0);
            $product_id = $this->uri->segment(5, 0);
            $status = ($mode == '0') ? 'UnPublish' : 'Publish';
            $newdata = array('status' => $status);
            $condition = array('id' => $product_id);
            $this->product_model->update_details(PRODUCT, $newdata, $condition);
            $this->setErrorMessage('success', 'Product Status Changed Successfully');
            redirect('admin/product/display_product_list');
        }
    }

    /**
     *
     * This function change the affiliate product status
     */
    public function change_user_product_status() {
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $mode = $this->uri->segment(4, 0);
            $product_id = $this->uri->segment(5, 0);
            $status = ($mode == '0') ? 'UnPublish' : 'Publish';
            $newdata = array('status' => $status);
            $condition = array('seller_product_id' => $product_id);
            $this->product_model->update_details(USER_PRODUCTS, $newdata, $condition);
            $this->setErrorMessage('success', 'Product Status Changed Successfully');
            redirect('admin/product/display_user_product_list');
        }
    }

    /**
     * 
     * This function update the global visibale status
     */
    public function change_user_product_global_status() {
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $product_id = $this->uri->segment(5, 0);
            $status = $this->uri->segment(4, 0);
            if ($status == 1) {
                $status = 0;
            } else {
                $status = 1;
            }
            $this->product_model->globle_update_details($product_id, $status);
            $this->setErrorMessage('success', 'Product globle Status Changed Successfully');
            redirect('admin/product/display_user_product_list');
        }
    }

    /**
     *
     * This function loads the product view page
     */
    public function view_product() {
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $this->data['heading'] = 'View Product';
            $product_id = $this->uri->segment(4, 0);
            $condition = array('id' => $product_id);
            $this->data['product_details'] = $this->product_model->get_all_details(PRODUCT, $condition);
            if ($this->data['product_details']->num_rows() == 1) {
                $this->data['catList'] = $this->product_model->get_cat_list($this->data['product_details']->row()->category_id);
                $this->load->view('admin/product/view_product', $this->data);
            } else {
                redirect('admin');
            }
        }
    }

    /**
     *
     * This function loads the affliated product view page
     */
    public function view_affliatedproduct() {
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $this->data['heading'] = 'View Product';
            $product_id = $this->uri->segment(4, 0);
            $condition = array('id' => $product_id);
            $this->data['product_details'] = $this->product_model->get_all_details(USER_PRODUCTS, $condition);
            if ($this->data['product_details']->num_rows() == 1) {
                $this->data['catList'] = $this->product_model->get_cat_list($this->data['product_details']->row()->category_id);
                $this->load->view('admin/product/view_affliatedproduct', $this->data);
            } else {
                redirect('admin');
            }
        }
    }

    /**
     *
     * This function adds a product to Our Picks Category
     */
    public function categorize_ourpicks() {
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $product_id = $this->uri->segment(4, 0);
            $condition = array('id' => $product_id);
            $this->product_model->ourpick_update($product_id);
            $this->setErrorMessage('success', 'Product successfully categorized to our picks');
            redirect('admin/product/display_product_list');
        }
    }

    /**
     *
     * This function removes a product from Our Picks Category
     */
    public function decategorize_ourpicks() {
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $product_id = $this->uri->segment(4, 0);
            $condition = array('id' => $product_id);
            $this->product_model->ourpick_remove($product_id);
            $this->setErrorMessage('error', 'Product successfully decategorized from our picks');
            redirect('admin/product/display_product_list');
        }
    }

    /**
     *
     * This function adds a User product to Our Picks Category
     */
    public function user_categorize_ourpicks() {
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $product_id = $this->uri->segment(4, 0);
            $condition = array('id' => $product_id);
            $this->product_model->user_ourpick_update($product_id);
            $this->setErrorMessage('success', 'Product successfully categorized to our picks');
            redirect('admin/product/display_user_product_list');
        }
    }

    /**
     *
     * This function removes a User product from Our Picks Category
     */
    public function user_decategorize_ourpicks() {
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $product_id = $this->uri->segment(4, 0);
            $condition = array('id' => $product_id);
            $this->product_model->user_ourpick_remove($product_id);
            $this->setErrorMessage('error', 'Product successfully decategorized from our picks');
            redirect('admin/product/display_user_product_list');
        }
    }

    /**
     *
     * This function delete the selling product record from db
     */
    public function delete_product() {
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $product_id = $this->uri->segment(4, 0);
            $condition = array('id' => $product_id);
            $old_product_details = $this->product_model->get_all_details(PRODUCT, array('id' => $product_id));
            $this->update_old_list_values($product_id, array(), $old_product_details);
            $this->update_user_product_count($old_product_details);
            $this->product_model->commonDelete(PRODUCT, $condition);
            $this->setErrorMessage('success', 'Product deleted successfully');
            redirect('admin/product/display_product_list');
        }
    }

    /**
     *
     * This function delete the affiliate product record from db
     */
    public function delete_user_product() {
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        } else {
            $product_id = $this->uri->segment(4, 0);
            $condition = array('seller_product_id' => $product_id);
            $old_product_details = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $product_id));
            $this->update_user_created_lists($product_id);
            $this->update_user_likes($product_id);
            $this->update_user_product_count($old_product_details);
            $this->product_model->commonDelete(USER_PRODUCTS, $condition);
            $this->product_model->commonDelete(USER_ACTIVITY, array('activity_id' => $product_id));
            $this->product_model->commonDelete(NOTIFICATIONS, array('activity_id' => $product_id));
            $this->product_model->commonDelete(PRODUCT_COMMENTS, array('product_id' => $product_id));
            $this->setErrorMessage('success', 'Product deleted successfully');
            redirect('admin/product/display_user_product_list');
        }
    }

    public function update_user_likes($product_id = '0') {
        $like_list = $this->product_model->get_like_user_full_details($product_id);
        if ($like_list->num_rows() > 0) {
            foreach ($like_list->result() as $like_list_row) {
                $likes_count = $like_list_row->likes;
                $likes_count--;
                if ($likes_count < 0)
                    $likes_count = 0;
                $this->product_model->update_details(USERS, array('likes' => $likes_count), array('id' => $like_list_row->id));
            }
            $this->product_model->commonDelete(PRODUCT_LIKES, array('product_id' => $product_id));
        }
    }

    public function update_user_created_lists($pid = '0') {
        $user_created_lists = $this->product_model->get_user_created_lists($pid);
        if ($user_created_lists->num_rows() > 0) {
            foreach ($user_created_lists->result() as $user_created_lists_row) {
                $list_product_ids = array_filter(explode(',', $user_created_lists_row->product_id));
                if (($key = array_search($pid, $list_product_ids)) !== false) {
                    unset($list_product_ids[$key]);
                    $update_ids = array('product_id' => implode(',', $list_product_ids));
                    $this->product_model->update_details(LISTS_DETAILS, $update_ids, array('id' => $user_created_lists_row->id));
                }
            }
        }
    }

    public function update_user_product_count($old_product_details) {
        if ($old_product_details != '' && count($old_product_details) > 0 && $old_product_details->num_rows() == 1) {
            if ($old_product_details->row()->user_id > 0) {
                $user_details = $this->product_model->get_all_details(USERS, array('id' => $old_product_details->row()->user_id));
                if ($user_details->num_rows() == 1) {
                    $prod_count = $user_details->row()->products;
                    $prod_count--;
                    if ($prod_count < 0) {
                        $prod_count = 0;
                    }
                    $this->product_model->update_details(USERS, array('products' => $prod_count), array('id' => $old_product_details->row()->user_id));
                }
            }
        }
    }

    /**
     *
     * This function change the selling product status, delete the selling product record
     */
    public function change_product_status_global() {

        if ($_POST['checkboxID'] != '') {

            if ($_POST['checkboxID'] == '0') {
                redirect('admin/product/add_product_form/0');
            } else {
                redirect('admin/product/add_product_form/' . $_POST['checkboxID']);
            }
        } else {
            if (count($_POST['checkbox_id']) > 0 && $_POST['statusMode'] != '') {
                $data = $_POST['checkbox_id'];
                if (strtolower($_POST['statusMode']) == 'categorize') {
                    foreach ($data as $product_id) {
                        if ($product_id != '') {
                            $this->product_model->ourpick_update($product_id);
                        }
                    }
                    $this->setErrorMessage('success', 'Product categorized successfully');
                    redirect('admin/product/display_product_list');
                }
                if (strtolower($_POST['statusMode']) == 'decategorize') {
                    foreach ($data as $product_id) {
                        if ($product_id != '') {
                            $this->product_model->ourpick_remove($product_id);
                        }
                    }
                    $this->setErrorMessage('success', 'Product decategorized successfully');
                    redirect('admin/product/display_product_list');
                }
                if (strtolower($_POST['statusMode']) == 'delete') {
                    for ($i = 0; $i < count($data); $i++) {
                        if ($data[$i] == 'on') {
                            unset($data[$i]);
                        }
                    }
                    foreach ($data as $product_id) {
                        if ($product_id != '') {
                            $old_product_details = $this->product_model->get_all_details(PRODUCT, array('id' => $product_id));
                            $this->update_old_list_values($product_id, array(), $old_product_details);
                            $this->update_user_product_count($old_product_details);
                        }
                    }
                }
                $this->product_model->activeInactiveCommon(PRODUCT, 'id');
                if (strtolower($_POST['statusMode']) == 'delete') {
                    $this->setErrorMessage('success', 'Product records deleted successfully');
                } else {
                    $this->setErrorMessage('success', 'Product records status changed successfully');
                }
                redirect('admin/product/display_product_list');
            }
        }
    }

    /**
     *
     * This function change the affiliate product status, delete the affiliate product record
     */
    public function change_user_product_status_global() {

        if (count($_POST['checkbox_id']) > 0 && $_POST['statusMode'] != '') {
            $data = $_POST['checkbox_id'];
            if (strtolower($_POST['statusMode']) == 'categorize') {
                foreach ($data as $product_id) {
                    if ($product_id != '') {
                        $this->product_model->user_ourpick_update($product_id);
                    }
                }
                $this->setErrorMessage('success', 'Product categorized successfully');
                redirect('admin/product/display_user_product_list');
            }
            if (strtolower($_POST['statusMode']) == 'decategorize') {
                foreach ($data as $product_id) {
                    if ($product_id != '') {
                        $this->product_model->user_ourpick_remove($product_id);
                    }
                }
                $this->setErrorMessage('success', 'Product decategorized successfully');
                redirect('admin/product/display_user_product_list');
            }
            if (strtolower($_POST['statusMode']) == 'globalize') {
                foreach ($data as $product_id) {
                    if ($product_id != '') {
                        $global_status = "1";
                        $this->product_model->globalize_user_product($product_id, $global_status);
                    }
                }
                $this->setErrorMessage('success', 'Products globalized successfully');
                redirect('admin/product/display_user_product_list');
            }
            if (strtolower($_POST['statusMode']) == 'localize') {
                foreach ($data as $product_id) {
                    if ($product_id != '') {
                        $global_status = "0";
                        $this->product_model->globalize_user_product($product_id, $global_status);
                    }
                }
                $this->setErrorMessage('success', 'Product localized successfully');
                redirect('admin/product/display_user_product_list');
            }
            if (strtolower($_POST['statusMode']) == 'delete') {
                for ($i = 0; $i < count($data); $i++) {
                    if ($data[$i] == 'on') {
                        unset($data[$i]);
                    }
                }
                foreach ($data as $product_id) {
                    if ($product_id != '') {
                        $old_product_details = $this->product_model->get_all_details(USER_PRODUCTS, array('seller_product_id' => $product_id));
                        $this->update_user_created_lists($product_id);
                        $this->update_user_likes($product_id);
                        $this->update_user_product_count($old_product_details);
                        $this->product_model->commonDelete(USER_ACTIVITY, array('activity_id' => $product_id));
                        $this->product_model->commonDelete(NOTIFICATIONS, array('activity_id' => $product_id));
                        $this->product_model->commonDelete(PRODUCT_COMMENTS, array('product_id' => $product_id));
                    }
                }
            }
            $this->product_model->activeInactiveCommon(USER_PRODUCTS, 'seller_product_id');
            if (strtolower($_POST['statusMode']) == 'delete') {
                $this->setErrorMessage('success', 'Product records deleted successfully');
            } else {
                $this->setErrorMessage('success', 'Product records status changed successfully');
            }
            redirect('admin/product/display_user_product_list');
        }
    }

    public function loadListValues() {
        $returnStr['listCnt'] = '<option value="">--Select--</option>';
        $lid = $this->input->post('lid');
        $lvID = $this->input->post('lvID');
        if ($lid != '') {
            $listValues = $this->product_model->get_all_details(LIST_VALUES, array('list_id' => $lid,'status'=>'1'));
            if ($listValues->num_rows() > 0) {
                foreach ($listValues->result() as $listRow) {
                    $selStr = '';
                    if ($listRow->id == $lvID) {
                        $selStr = 'selected="selected"';
                    }
                    $returnStr['listCnt'] .= '<option ' . $selStr . ' value="' . $listRow->id . '">' . $listRow->list_value . '</option>';
                }
            }
        }
        echo json_encode($returnStr);
    }

    public function display_upload_req() {
        if ($this->checkLogin('A') != '') {
            $this->data['heading'] = 'Product Upload Requests';
            $this->data['req_details'] = $this->product_model->get_all_details(UPLOAD_MAILS, array());
            $this->load->view('admin/product/display_upload_req', $this->data);
        } else {
            show_404();
        }
    }

    public function delete_upreq() {
        if ($this->checkLogin('A') != '') {
            $rid = $this->uri->segment(4, 0);
            $cond_arr = array('id' => $rid);
            $this->product_model->commonDelete(UPLOAD_MAILS, $cond_arr);
            $this->setErrorMessage('success', 'Request deleted successfully');
            redirect('admin/product/display_upload_req');
        }
    }

    public function change_upreq_status_global() {
        if (count($this->input->post('checkbox_id')) > 0 && $this->input->post('statusMode') != '') {
            $this->product_model->activeInactiveCommon(UPLOAD_MAILS, 'id');
            $this->setErrorMessage('success', 'Requests deleted successfully');
            redirect('admin/product/display_upload_req');
        }
    }

    public function Add_New_Caption() {
        $data['heading'] = 'Update Caption';
        $this->session->set_userdata('url', 'Add/Edit_New_Caption');
        $data['caption'] = $this->product_model->get_caption_list();
        //$this->product_model->commonDelete(UPLOAD_MAILS,$cond_arr);
        $this->setErrorMessage('success', 'Caption Add Successfully');
        $this->load->view('admin/product/Add_New_Caption', $data);
    }

    public function insertcaption() {
        $id = $this->input->post('id');
        $caption = $this->input->post('caption');
        $discount = $this->input->post('discount');
        $url = $this->input->post('url');
        $visible = $this->input->post('visible');
        $heading = $this->input->post('heading');
        if ($visible == 'yes') {
            $visible = 'yes';
        } else {
            $visible = 'no';
        }
        $response = $this->product_model->insert_caption($id, $heading, $caption, $discount, $url, $visible);
        if ($response) {
            $this->session->set_userdata('insert_success_msg', 'Caption Update Successfully');
            redirect(base_url('admin/product/Add_New_Caption'));
        } else {
            $this->session->set_userdata('insert_error_msg', 'Please Try Again');
            redirect(base_url('admin/product/Add_New_Caption'));
        }
    }
    
    
//     public function Calculate_total_amount(){
// 		$product_ids = $this->input->post('product_id');
// 		$quantity_array = $this->input->post('quantity');
// 		$tenure_array = $this->input->post('tenure_array');
// 		if(isset($_SESSION['quantity_array'])){
// 			unset($_SESSION['quantity_array']);
// 		}
// 		$this->session->set_userdata('quantity_array',$this->input->post('quantity_array'));
// 		foreach ($tenure_array as $first_key => $tenure) {
// 			$Product_Details = $this->product_model->get_subproducts_price_without_tenure($product_ids);
// 			foreach ($Product_Details as $main_key => $check_addon) {
//                 if(!$check_addon->is_addon){
//                     $new_product = $this->product_model->get_subproducts_price($product_ids,$tenure);
//                     foreach ($new_product as  $value) {
//                 		$q =  $_SESSION['quantity_array'][$value->id];
//                 		if(!$value->is_addon){
//                 			if($value->subproducts != ''){
// 								$new_subproduct_array = explode(',', $value->subproducts);
// 								$subproduct_prices = $this->product_model->get_subproducts_price($new_subproduct_array,$tenure);
// 								foreach ($subproduct_prices as $key => $new) {
// 								    if($new->attr_price){
// 										$total_price[$first_key] += ($new->attr_price * $q);
// 								   	}else{
// 								   		$total_price[$first_key] += ($new->price * $q);
// 								   	}
// 								}
// 							}
// 							else{
// 							    if($value->attr_price){
// 										$total_price[$first_key] += ($value->attr_price * $q);
// 								   	}else{
// 								   		$total_price[$first_key] += ($value->price * $q);
// 								}
// 							}
//                 		}else{
//                 			$total_price[$first_key] += ($value->price * $q);
//                 		}
//                     }
//                 }else{
//                 	$new_product = $this->product_model->get_subproducts_price_without_tenure($product_ids,$extra =true);
//                     foreach ($new_product as  $value) {
//                     		$q =  $_SESSION['quantity_array'][$value->id];
// 							if($value->subproducts != ''){
// 								$new_subproduct_array = explode(',', $value->subproducts);
// 								$subproduct_prices = $this->product_model->get_subproducts_price_without_tenure($new_subproduct_array,$extra =true);
// 								foreach ($subproduct_prices as $key =>  $new) {
// 									$total_price[$first_key] += ($new->price * $q);
// 								}
// 							}
// 							else
// 							{
// 								$total_price[$first_key] += ($value->price * $q);
// 							}
//                     }
//                 }
// 			} 
// 		}
// 		echo json_encode($total_price);
// 	}
	
// 	public function Calculate_total_amount_edit(){
// 		$other_ids = $this->input->post('other_ids');
// 		$product_id = $this->input->post('product_id');
// 		$new_array  = $this->input->post('quantity_array');
// 		$tenure_array = $this->input->post('tenure_array');
// 		$quantity_array = serialize($new_array);
// 		$excludeArr = array("subproduct_quantity");
// 		$dataArr = array('subproduct_quantity' => $quantity_array);
// 		$condition  = array('id' => $this->input->post('package_id'));
// 		$this->product_model->update_details(PRODUCT,$dataArr,$condition);
// 		$product_data =  $this->product_model->get_all_details(PRODUCT,array('id' => $product_id ));
// 		if($product_data->row()->subproduct_quantity != ''){
// 		    $quantity_data = unserialize($product_data->row()->subproduct_quantity);
// 		}
// 		foreach ($tenure_array as $key123 => $tenure) {
// 			$Product_Detailsnew = $this->product_model->get_subproducts_price_without_tenure($other_ids);
// 			foreach ($Product_Detailsnew as $main_key => $check_addon) {
//                 if(!$check_addon->is_addon) {
//                     $new_product = $this->product_model->get_subproducts_price( $check_addon->id , $tenure);
//                     foreach ($new_product as  $value) {
//                 		$q = $quantity_data[$value->id];
//                 		if(!$value->is_addon){
// 							if($value->subproducts != ''){
// 								$new_subproduct_array = explode(',', $value->subproducts);
// 								$check_product_is_addon = $this->product_model->get_subproducts_price_without_tenure($new_subproduct_array);
// 								foreach ($check_product_is_addon as $check_addonk => $addonk_val) {								
// 									if(!$addonk_val->is_addon){
// 										$subproduct_prices = $this->product_model->get_subproducts_price($new_subproduct_array , $tenure);		
// 										foreach ($subproduct_prices as $key => $new) {
// 											if($new->attr_price){
												
// 												$total_price[$key123] += ($new->attr_price * $q);
// 										   	}else{
// 										   		$total_price[$key123] += ($new->price * $q);
// 										   	}
// 										}	
// 									}
// 									else{
// 										$total_price[$key123] += ($addonk_val->price * $q);																
// 									}
// 								}								
// 							}
// 							else{
// 								if($value->attr_price){
// 									$total_price[$key123] += ($value->attr_price * $q);
// 							   	}else{
// 							   		$total_price[$key123] += ($value->price * $q);
// 							   	}
// 							}
// 						}else{
// 							$total_price[$key123] += ($value->price * $q);
// 						}
//                     }       	
//                 }
//                 else{
//                 	$new_product = $this->product_model->get_subproducts_price_without_tenure( $check_addon->id , $extra =true);
//                     foreach ($new_product as  $value) {
//                 		$q =  $quantity_data[$value->id];
// 						if($value->subproducts != ''){
// 							$new_subproduct_array = explode(',', $value->subproducts);
// 							$subproduct_prices = $this->product_model->get_subproducts_price_without_tenure($new_subproduct_array,$extra =true);
// 							foreach ($subproduct_prices as $key =>  $new) {
// 								$total_price[$key123] += ($new->price * $q);
// 							}
// 						}
// 						else{
// 							$total_price[$key123] += ($value->price * $q);
// 						}
//                     }
//                 }
// 			} 
// 		}		
// 		echo json_encode($total_price);
// 	}


public function Calculate_total_amount(){
		$product_ids = $this->input->post('product_id');
		$quantity_array = $this->input->post('quantity');
		$tenure_array = $this->input->post('tenure_array');
		if(isset($_SESSION['quantity_array'])){
			unset($_SESSION['quantity_array']);
		}
		$this->session->set_userdata('quantity_array',$this->input->post('quantity_array'));

		foreach ($tenure_array as $first_key => $tenure) {
			$Product_Details = $this->product_model->get_subproducts_price_without_tenure($product_ids);
			foreach ($Product_Details as $main_key => $check_addon) {
                // if(!$check_addon->is_addon){

                    $new_product = $this->product_model->get_subproducts_price($product_ids,$tenure);
                    foreach ($new_product as  $value) {
                		$q =  $_SESSION['quantity_array'][$value->id];
                		// if(!$value->is_addon){
                			if($value->subproducts != ''){
								$new_subproduct_array = explode(',', $value->subproducts);
								$subproduct_prices = $this->product_model->get_subproducts_price($new_subproduct_array,$tenure);
								foreach ($subproduct_prices as $key => $new) {
								   	if($new->attr_price){
										$total_price[$first_key] += ($new->attr_price * $q);
								   	}else{
								   		$total_price[$first_key] += ($new->price * $q);
								   	}
								}
							}
							else{
									if($value->attr_price){
										$total_price[$first_key] += ($value->attr_price * $q);
								   	}else{
								   		$total_price[$first_key] += ($value->price * $q);
								   	}
							}
                		// }else{
                		// 	$total_price[$first_key] += ($value->price * $q);
                		// }
                    }

			                 	
                // }
       //          else{

       //          	$new_product = $this->product_model->get_subproducts_price_without_tenure($product_ids,$extra =true);
       //          	// print_r($new_product);
       //              foreach ($new_product as  $value) {
       //              		$q =  $_SESSION['quantity_array'][$value->id];
							// if($value->subproducts != ''){
							// 	$new_subproduct_array = explode(',', $value->subproducts);
							// 	$subproduct_prices = $this->product_model->get_subproducts_price_without_tenure($new_subproduct_array,$extra =true);
							// 	foreach ($subproduct_prices as $key =>  $new) {
							// 		$total_price[$first_key] += ($new->price * $q);
							// 	}
							// }
							// else
							// {
							// 	$total_price[$first_key] += ($value->price * $q);
							// }

       //              }

       //          }

			} 
			
		}
		// exit;
		echo json_encode($total_price);

	}

	public function Calculate_total_amount_edit(){
		$other_ids = $this->input->post('other_ids');
		$product_id = $this->input->post('product_id');
		$new_array  = $this->input->post('quantity_array');
		$tenure_array = $this->input->post('tenure_array');
		$quantity_array = serialize($new_array);
		$excludeArr = array("subproduct_quantity");
		$dataArr = array('subproduct_quantity' => $quantity_array);
		$condition  = array('id' => $this->input->post('package_id'));
		$this->product_model->update_details(PRODUCT,$dataArr,$condition);
		$product_data =  $this->product_model->get_all_details(PRODUCT,array('id' => $product_id ));
		if($product_data->row()->subproduct_quantity != ''){
		    $quantity_data = unserialize($product_data->row()->subproduct_quantity);
		}
		foreach ($tenure_array as $key123 => $tenure) {
			$Product_Detailsnew = $this->product_model->get_subproducts_price_without_tenure($other_ids);
			foreach ($Product_Detailsnew as $main_key => $check_addon) {
                // if(!$check_addon->is_addon) {
                    $new_product = $this->product_model->get_subproducts_price( $check_addon->id , $tenure);
                    foreach ($new_product as  $value) {
                		$q = $quantity_data[$value->id];
                		if(!$value->subproducts){
							if($value->subproducts != ''){
								$new_subproduct_array = explode(',', $value->subproducts);
								$check_product_is_addon = $this->product_model->get_subproducts_price_without_tenure($new_subproduct_array);
								foreach ($check_product_is_addon as $check_addonk => $addonk_val) {								
									// if(!$addonk_val->is_addon){
										$subproduct_prices = $this->product_model->get_subproducts_price($new_subproduct_array , $tenure);		
										foreach ($subproduct_prices as $key => $new) {
											if($new->attr_price){
												
												$total_price[$key123] += ($new->attr_price * $q);
										   	}else{
										   		$total_price[$key123] += ($new->price * $q);
										   	}
										}	
									// }
									// else{

									// 	$total_price[$key123] += ($addonk_val->price * $q);																
									// }
								}								
							}
							else{
								if($value->attr_price){
									$total_price[$key123] += ($value->attr_price * $q);
							   	}else{
							   		$total_price[$key123] += ($value->price * $q);
							   	}
							}
						}else{
							if($value->attr_price){
								$total_price[$key123] += ($value->attr_price * $q);
						   	}else{
						   		$total_price[$key123] += ($value->price * $q);
						   	}
							// $total_price[$key123] += ($value->price * $q);
						}
                    }       	
      //           }
      //           else{
      //           	$new_product = $this->product_model->get_subproducts_price_without_tenure( $check_addon->id , $extra =true);
      //               foreach ($new_product as  $value) {
      //           		$q =  $quantity_data[$value->id];
						// if($value->subproducts != ''){
						// 	$new_subproduct_array = explode(',', $value->subproducts);
						// 	$subproduct_prices = $this->product_model->get_subproducts_price_without_tenure($new_subproduct_array,$extra =true);
						// 	foreach ($subproduct_prices as $key =>  $new) {
						// 		$total_price[$key123] += ($new->price * $q);
						// 	}
						// }
						// else{
						// 	$total_price[$key123] += ($value->price * $q);
						// }
      //               }
      //           }
			} 
		}		
		echo json_encode($total_price);
	}

	public function update_attr_price(){
		$attr_array = $this->input->post('attr');
		foreach ($attr_array as $key => $value) {
			$conditons = array('pid'=> $value['pid']);
			$dataArr = array('attr_id'=> $value['attr_id'] ,'attr_name'=> $value['attr_name'] ,'attr_price'=> $value['attr_price'] );
			$subproductDetails = $this->product_model->edit_subproduct_update($dataArr,$conditons);
		}
    }
    
    //Quantity product details section - Manish Soni - 4/9/2019
    public function display_product_quantity_details() {
		if ($this->checkLogin('A') == '') {
			redirect('admin');
		} else {
			$this->data['heading'] = 'Product Quantity Details';
			$this->data['cityList'] = $this->product_model->get_all_cities(1);
			if(!isset($_GET['type']) || $_GET['type']  == 'all'){
				$datas = $this->product_model->view_product_quantity_details();
			}else{
				$condition = ' where status ="'.$_GET['type'].'"';
				$datas = $this->product_model->view_product_quantity_details($condition);
			}
			$datas = $datas->result();
			foreach($datas as $key => $val) {
				$city_quantity = $this->product_model->view_product_quantity(array('product_id' => $val->id));
				$datas[$key]->city_quantity = $city_quantity;
			}
			$this->data['datas'] = $datas;
			$this->load->view('admin/product/display_product_quantity_details',$this->data);
		}
	}	

	public function edit_product_quantity_form(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Edit Product';
			$this->data['addon_prod_list'] = $this->product_model->view_addon_prod();
			$this->data['sub_products_list'] = $this->product_model->get_sub_products();

			$product_id = $this->uri->segment(4,0);
			$condition = array('id' => $product_id);

			$this->data['product_details'] = $this->product_model->view_product($condition);			

			if ($this->data['product_details']->num_rows() == 1) {
				$this->data['product_quantity'] = $this->product_model->view_product_quantity(array('product_id' => $product_id));
				$this->data['cityList'] = $this->product_model->get_all_cities(1);
				$this->load->view('admin/product/edit_product_quantity',$this->data);
			} else {
				redirect('admin');
			}
		}
	}

	/*
	* Edit product quantity city wise - Manish - 4/9/2019
	*/
	public function EditProductQuantity() {

		if ($this->checkLogin('A') == '') {
			redirect('admin');
		} else {
			$inputArr = $this->input->post();
			$status = $inputArr['status'];

			if (isset($status) && $status != '') {
				$product_status = 'Publish';
			} else {
				$product_status = 'UnPublish';
			}

			foreach($inputArr['quantity'] as $key => $val) {
				$cityCatData = array(
					'city_id' => $key,
					'product_id' => $inputArr['productID'],
					'quantity' => ($val != '') ? $val : '0',
				);

				$checkData = $this->product_model->get_all_details('fc_city_product_quantity',array('city_id' => $key, 'product_id' => $inputArr['productID']));
				if($checkData->num_rows() > 0) {
					$this->db->where('city_id = '.$key.' and product_id = '.$inputArr['productID']);
					$this->db->update('fc_city_product_quantity',$cityCatData);
				} else {
					$this->db->insert('fc_city_product_quantity',$cityCatData);
				}
			}

			$dataArr = array('status' => $product_status);

			$this->db->where('id = '.$inputArr['productID']);
			$this->db->update(PRODUCT, $dataArr);

			$this->setErrorMessage('success','Product Quantity updated successfully');
			redirect('admin/product/display_product_quantity_details');
		}
	}

	public function get_all_product_listing() {
		if ($this->checkLogin('A') == '') {
			redirect('admin');
		} else {
			$datas = $this->product_model->view_product_quantity_details();
			$datas = $datas->result();

			foreach($datas as $key => $val) {
				$city_quantity = $this->product_model->view_product_quantity(array('product_id' => $val->id));
				$datas[$key]->city_quantity = $city_quantity;
			}
	    	$TotalDataCount = count($datas);
			$jsonArray = (object) array("draw" => intval($_REQUEST['draw']), "recordsTotal" => $TotalDataCount, "recordsFiltered" => $TotalDataCount, "data" => $datas);
			echo json_encode($jsonArray);
		}
	}

	public function exportProduct() {
	    if($_GET['type']  == 'all'){
			$datas = $this->product_model->view_product_quantity_details();
		}else{
			$condition = ' where status ="'.$_GET['type'].'"';
			$datas = $this->product_model->view_product_quantity_details($condition);
		}
		$cityList = $this->product_model->get_all_cities(1);

		$output = '';
		if($datas->num_rows() > 0)
		{
			$fileName = "webdamn_export_".date('Ymd') . ".xls";
			header("Content-Type: application/vnd.ms-excel");
			header("Content-Disposition: attachment; filename=\"$fileName\"");

			$datas = $datas->result();
			$key_array = array('Product Name','Status');

			foreach($cityList->result() as $key => $val) {
				$key_array[] = ucfirst($val->list_value).' Quantity';
			}

			echo implode("\t", $key_array) . "\n";

			$value_array = array();
			$i=0;
			$cityQuantity = '';
			foreach($datas as $data)
			{
				$value_array[$i][] = $data->product_name;
				$value_array[$i][] = $data->status;
				$cityQuantity = $this->product_model->view_product_quantity(array('product_id' => $data->id));

				$k=0;
				foreach($cityList->result() as $key => $val) {
					$cityId = $val->id;
					$value_array[$i][] = (isset($cityQuantity[$cityId])) ? $cityQuantity[$cityId] : '0';
					$k++;
				}
				$i++;
			}

			foreach($value_array as $val) {
				echo implode("\t", $val) . "\n";
			}
			exit;
		}
	}
	
	public function update_city_qty() {
		$input = $this->input->post();
		if($input['qty'] != '' && $input['cityid'] != '' && $input['proId'] != '') {
			$checkData = $this->product_model->get_all_details('fc_city_product_quantity',array('city_id' => $input['cityid'], 'product_id' => $input['proId']));
			if($checkData->num_rows() > 0) {
				$this->db->where('city_id', $input['cityid']);
				$this->db->where('product_id', $input['proId']);
				$this->db->update('fc_city_product_quantity', array('quantity' => $input['qty']));
			} else {
				$this->db->insert('fc_city_product_quantity', array('city_id' => $input['cityid'], 'product_id' => $input['proId'], 'quantity' => $input['qty']));
			}
		}
	}
	
	public function update_city_qty_old()
	{
	    $baseURL = 'https://cityfurnish.com/';
		$input = $this->input->post();
		if($input['qty'] != '' && $input['cityid'] != '' && $input['proId'] != '') 
		{
		    $fetchProductData = $this->product_model->get_all_details('fc_product',array('id' => $input['proId']));
		    $productid = $input['proId'];
		     $productname = $fetchProductData->result()[0]->product_name;
		    $description = $fetchProductData->result()[0]->description;
		    $seo_url = $fetchProductData->result()[0]->seourl;
		     $imagestr = $fetchProductData->result()[0]->image;
		     $image = explode(',',$imagestr);
		    echo $productimage = $image[0];
		    
		    
			$checkData = $this->product_model->get_all_details('fc_city_product_quantity',array('city_id' => $input['cityid'], 'product_id' => $input['proId']));
			
			if($checkData->num_rows() > 0) 
			{
				$this->db->where('city_id', $input['cityid']);
				$this->db->where('product_id', $input['proId']);
				$this->db->update('fc_city_product_quantity', array('quantity' => $input['qty']));
				
				$this->setErrorMessage('success','Quantity Updated Successfully');
				
				 $checkUserNotifyData = $this->product_model->get_all_details('fc_notify_user_product',array('cityid' => $input['cityid'], 'productid' => $input['proId'],'status'=>'1'));
				 
    			if($checkUserNotifyData->num_rows() > 0)
    			{
    			    $notifyRows = $checkUserNotifyData->result();
    			    
    			    foreach($notifyRows as $val)
    			    {
    			        
    			        $userid = $val->userid;
    			        $UserQuery = " select `email`, `full_name` from `fc_users` where `id` = '$userid'";
		                $userRow = $this->product_model->ExecuteQuery($UserQuery);
		                $senderemail = $userRow->result()[0]->email;
		                $full_name = $userRow->result()[0]->full_name;
    			         
    			       // $message = $this->load->view('site/templates/notify-product-mail',"", false);
    			       $message = '<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"> 
    <meta name="x-apple-disable-message-reformatting">  
    <title></title> 
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:200,300,400,500,600,700" rel="stylesheet">
    
    <!-- CSS Reset : BEGIN -->
    <style>

        html,
body {
    margin: 0 auto !important;
    padding: 0 !important;
    height: 100% !important;
    width: 100% !important;
    background: #f1f1f1;
}

* {
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
}


div[style*="margin: 16px 0"] {
    margin: 0 !important;
}


table,
td {
    mso-table-lspace: 0pt !important;
    mso-table-rspace: 0pt !important;
}

table {
    border-spacing: 0 !important;
    border-collapse: collapse !important;
    table-layout: fixed !important;
    margin: 0 auto !important;
}

img {
    -ms-interpolation-mode:bicubic;
}


a {
    text-decoration: none;
}


.unstyle-auto-detected-links *,
.aBn {
    border-bottom: 0 !important;
    cursor: default !important;
    color: inherit !important;
    text-decoration: none !important;
    font-size: inherit !important;
    font-family: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
}

.a6S {
    display: none !important;
    opacity: 0.01 !important;
}


.im {
    color: inherit !important;
}


img.g-img + div {
    display: none !important;
}



@media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
    u ~ div .email-container {
        min-width: 320px !important;
    }
}

@media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
    u ~ div .email-container {
        min-width: 375px !important;
    }
}

@media only screen and (min-device-width: 414px) {
    u ~ div .email-container {
        min-width: 414px !important;
    }
}

    </style>

    
    <style>

	    .primary{
	background: #2f89fc;
}
.bg_white{
	background: #ffffff;
}
.bg_light{
	background: #fafafa;
}
.bg_black{
	background: #000000;
}
.bg_dark{
	background: rgba(0,0,0,.8);
}
.email-section{
	padding:2.5em;
}

.btn {display: inline-block;margin-bottom: 0;font-weight: 400;text-align: center;white-space: nowrap;vertical-align: middle;-ms-touch-action:manipulation;touch-action: manipulation;cursor: pointer;background-image: none;border: 1px solid transparent;padding: 6px 12px;font-size: 14px;line-height: 1.42857143;border-radius: 4px;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;
}
.btn-primary{

  background: #2F84E8;border: 1px solid #2F84E8;color: #fff;

	
}
.btn.btn-white{
	border-radius: 5px;
	background: #ffffff;
	color: #000000;
}
.btn.btn-white-outline{
	border-radius: 5px;
	background: transparent;
	border: 1px solid #fff;
	color: #fff;
}

h1,h2,h3,h4,h5,h6{
	font-family: "Work Sans", sans-serif;
	color: #000000;
	margin-top: 0;
	font-weight: 400;
}

body{
	font-family: "Work Sans", sans-serif;
	font-weight: 400;
	font-size: 15px;
	line-height: 1.8;
	color: rgba(0,0,0,.4);
}

a{
	color: #2f89fc;
}

table{
}
/*LOGO*/

.logo h1{
	margin: 0;
}
.logo h1 a{
	color: #000000;
	font-size: 20px;
	font-weight: 700;
	text-transform: uppercase;
	font-family: "Poppins", sans-serif;
}

.navigation{
	padding: 0;
}
.navigation li{
	list-style: none;
	display: inline-block;;
	margin-left: 5px;
	font-size: 13px;
	font-weight: 500;
}
.navigation li a{
	color: rgba(0,0,0,.4);
}



/*HEADING SECTION*/
.heading-section{
}
.heading-section h2{
	color: #000000;
	font-size: 28px;
	margin-top: 0;
	line-height: 1.4;
	font-weight: 400;
}
.heading-section .subheading{
	margin-bottom: 20px !important;
	display: inline-block;
	font-size: 13px;
	text-transform: uppercase;
	letter-spacing: 2px;
	color: rgba(0,0,0,.4);
	position: relative;
}
.heading-section .subheading::after{
	position: absolute;
	left: 0;
	right: 0;
	bottom: -10px;
	content: "";
	width: 100%;
	height: 2px;
	background: #2f89fc;
	margin: 0 auto;
}

.heading-section-white{
	color: rgba(255,255,255,.8);
}
.heading-section-white h2{
	font-family: 
	line-height: 1;
	padding-bottom: 0;
}
.heading-section-white h2{
	color: #ffffff;
}
.heading-section-white .subheading{
	margin-bottom: 0;
	display: inline-block;
	font-size: 13px;
	text-transform: uppercase;
	letter-spacing: 2px;
	color: rgba(255,255,255,.4);
}


/*FOOTER*/

.footer{
	color: rgba(255,255,255,.5);

}
.footer .heading{
	color: #ffffff;
	font-size: 20px;
}
.footer ul{
	margin: 0;
	padding: 0;
}
.footer ul li{
	list-style: none;
	margin-bottom: 10px;
}
.footer ul li a{
	color: rgba(255,255,255,1);
}


@media screen and (max-width: 500px) {


}


    </style>


</head>

<body width="100%" style="margin: 0; padding: 0 !important; mso-line-height-rule: exactly; background-color: #222222;">
	<center style="width: 100%; background-color: #f1f1f1;">
    <div style="display: none; font-size: 1px;max-height: 0px; max-width: 0px; opacity: 0; overflow: hidden; mso-hide: all; font-family: sans-serif;">
      &zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;&zwnj;&nbsp;
    </div>
    <div style="max-width: 600px; margin: 0 auto;" class="email-container">
    	<!-- BEGIN BODY -->
      <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;">
      	<tr>
          <td valign="top" class="" style="padding: 1em 2.5em;background: #fff;">
          	<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
          		<tr>
          			<td class="logo" style="text-align: center;">
			            <h1><a href="https://cityfurnish.com/"><img src="https://d1eohs8f9n2nha.cloudfront.net/images/logo-stick.png" style="height: auto;width: auto;"></a></h1>
			          </td>
          		</tr>
          	</table>
          </td>
	      </tr>
				<tr>
          <td valign="middle" style="padding: 1em 0;background: #fff;position: relative;z-index: 0;">
            <table>
            	<tr>
            		<td>
            			<div class="" style="padding: 0 3em; text-align: center;color: rgba(0,0,0,.3);">
            				<h2 style="color: #000;font-size: 26px;margin-bottom: 0;font-weight: 100 !important;">Hi <span style="color: #091763;font-weight: 500;">'.$full_name.'</span></h2>
            			</div>
            		</td>
            	</tr>
            </table>
          </td>
	      </tr>
	      <tr>
		      <td style="background: #fff;">
		        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
		          <tr>
		            <td class="email-section" style="background: #fff;padding:2.5em;">
		            	<div class="heading-section" style="text-align: center; padding: 0 30px;">
		              	<h2 style="font-size: 22px;font-weight: normal !important;">Wohoo! The product you were looking for is back in Stock!</h2>
		              	<p style="color:rgba(0,0,0,.4) !important;font-weight: 400;font-size: 15px;line-height: 1.8;">Cityfurnish provides quality furniture and home appliances on easy monthly rental.</p>
		            	</div>
		            	<table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%" style="position: relative;">
		            		<tr width="100%">
                      <td valign="top" width="100%">
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" >
                          <tr>
                            <td style="padding-top: 20px; padding-right: 10px;">
                              <a id="pimg-1" ><img src="https://d1eohs8f9n2nha.cloudfront.net/images/product/'.$productimage.'" alt="" style="width: 100%; max-width: 600px; height: auto; margin: auto; display: block;"></a>
                              
                            </td>
                          </tr>
                         <tr width="100%">
                           <td style="background:green;width:100%;display:inline;">
                               <h3 style="margin-bottom: 0;font-weight: normal !important;font-size:18px;"><a href="#" style="color: #000;">'.$productname.'</a>
                                <div style="float:right;margin-right:10px;">
                                <a href="'.$baseURL.'things/'.$productid. '/'.$seo_url.'"><button style="position: absolute;;z-index: 100;display: inline-block;margin-bottom: 0;font-weight: 400;text-align: center;white-space: nowrap;vertical-align: middle;-ms-touch-action:manipulation;touch-action: manipulation;cursor: pointer;background-image: none;border: 1px solid transparent;padding: 4px 10px !important;font-size: 14px;line-height: 1.42857143;border-radius: 4px;-webkit-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;background: #2F84E8!important;border: 1px solid #2F84E8;color: #fff;">&nbsp;Order Now&nbsp;</button></a>
                                <div></h3>
                           </td>
                          </tr>
                        </table>
                         
                      </td> 
                    </tr>
		            	</table>
		            </td>
		          </tr>

		        </table>

		      </td>
		    </tr>
     
      </table>
      <table align="center" role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%" style="margin: auto;background: #fff;">
      	<tr>
          <td valign="middle" class="footer email-section" style="padding-right: 1.5em;padding-left: 2.5em;padding-top: 1em;padding-bottom: 1em;">
            <table>
            	<tr>
                <td valign="top" width="33.333%" style="padding-top: 10px;">
                  <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                    <tr>
                      <td style="text-align: left; padding-left: 5px; padding-right: 5px;">
                      	<h3 class="" style="color: #000;font-size: 20px;text-align:center;">Get 
                      	In Touch With Us</h3>
                      	<ul style="margin: 0;padding: 0;">
			                <li style="list-style: none;margin-bottom: 10px;margin-left:0px;"><span class="text"><a style="color: #000;" href="mailto:hello@cityfurnish.com ">hello@cityfurnish.com&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</a></span><a style="color: #000;" href="tel:+91 8010845000">8010845000</a>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<a style="color: #000;" href="https://cityfurnish.com/" target="_blank">www.cityfurnish.com</a></li>
			            </ul>
                      </td>
                    </tr>
                    <tr>
                      <td style="text-align: left; padding-right: 10px;text-align:center;">
                        <p style="color:rgba(0,0,0,.4)">&copy; Copyright 2019 Cityfurnish. All Rights Reserved.</p>
                      </td>
                    </tr>
                  </table>
                </td>
              
              </tr>
            </table>
          </td>
        </tr>
      </table>

    </div>
  </center>
</body>
</html>

';
    			        
    			     //   print_r($message);die;
    			        
    			        $email_values = array('mail_type' => 'html',
                        'from_mail_id' => 'hello@cityfurnish.com',
                        'mail_name' => 'Cityfurnish',
                        'to_mail_id' =>$senderemail,
                        'cc_mail_id' => $this->config->item('site_contact_mail'),
                        'subject_message' => 'Your Cityfurnish Order Item Is In Stock',
                        'body_messages' => $message
                         );
    			        $email_send_to_common = $this->product_model->common_email_send($email_values);
    			        
    			        
    			        $this->db->where('cityid', $input['cityid']);
        				$this->db->where('productid', $input['proId']);
        				$this->db->where('userid', $userid);
        				$this->db->update('fc_notify_user_product', array('status' => '2'));
    			    }
    			}
    	/* status=2 - notification mamil is sent , status = 1 - mail not sent*/
    			else
    			{
    			    echo "No User Found";
    			}
				
			} 
			else
			{
				$this->db->insert('fc_city_product_quantity', array('city_id' => $input['cityid'], 'product_id' => $input['proId'], 'quantity' => $input['qty']));
			}
		}
	}
	
	
	
	public function updateProductSnippet() {
		$Query = " select id, seourl from fc_product";
		$datas = $this->product_model->ExecuteQuery($Query);

		$header_snippet = '';
		foreach($datas->result_array() as $val) {
			$header_snippet = '<link rel="canonical" href="'.base_url().'things/'.$val['id'].'/'.$val['seourl'].'" />';
			$this->db->where('id', $val['id']);
			$this->db->update('fc_product', array('header_code_snippet' => $header_snippet));
		}
	}
	
  	public function clone_product()
	{	
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			
			$product_id = $this->uri->segment(4,0);
			$condition = array('id' => $product_id);

			$this->data['product_details'] =
			$this->product_model->view_product($condition);
			$this->data['SubPrdVal'] =
			$this->product_model->view_subproduct_details($product_id);
			
			foreach($this->data['SubPrdVal']->result() as $a){$attr_id[] = $a->pid;$attr_name[] = $a->attr_name;$attr_price[] = $a->attr_price;}
			
			$slider  = $this->data['product_details']->row()->slider_view;
			$min_tenure = $this->data['product_details']->row()->min_tenure;
			$rental_freq = $this->data['product_details']->row()->rental_freq;	
			
			if($slider != ''){
				$slider = $slider;
			}
			else{
				$slider = 'no';
			}

			if ($product_id == ''){
				$old_product_details = array();
				$condition = array('product_name' => $product_name);
			}else {
				$old_product_details = $this->product_model->get_all_details(PRODUCT,array('id'=>$product_id));
				$condition = array('product_name' => $product_name,'id !=' => $product_id);
			}

			$product_name = $this->data['product_details']->row()->product_name;
			$package_discount = $this->data['product_details']->row()->package_discount;
			$is_package = $this->data['product_details']->row()->is_package;
			$sale_price = $this->data['product_details']->row()->sale_price;
			$is_add_on = $this->data['product_details']->row()->is_addon;
			$is_sub_product = $this->data['product_details']->row()->is_sub_product;
			$subproduct_quantity = unserialize($this->data['product_details']->row()->subproduct_quantity);
			$brand = $this->data['product_details']->row()->brand;
			$product_status = "UnPublish";
			// echo "<pre>";
			// print_r( $this->data['product_details']->row()->subproduct_quantity);

			if($is_package == '1'){
				$is_package  = '1';
			}else{
				$is_package = '0';
			}

			
			if ($is_add_on != ''){
				$addon = $is_add_on;
			}else {
				$addon = '0';
			}
			
			if ($is_sub_product != ''){
				$sub_product = $is_sub_product;
			}else {
				$sub_product = '0';
			}
			$price_range = '';
			if ($sale_price>0 && $sale_price<21){
				$price_range = '1-20';
			}else if ($sale_price>20 && $sale_price<101){
				$price_range = '21-100';
			}else if ($sale_price>100 && $sale_price<201){
				$price_range = '101-200';
			}else if ($sale_price>200 && $sale_price<501){
				$price_range = '201-500';
			}else if ($sale_price>500){
				$price_range = '501+';
			}
			$excludeArr = array("gateway_tbl_length","imaged","productID","changeorder","status","category_id","attribute_name","attribute_val","attribute_weight","brand","attribute_price","product_image","userID","product_attribute_name","product_attribute_val","attr_name1","attr_val1","attr_type1","product_attribute_type","addon_checkbox_id", "subproduct_checkbox_id","package_discount","is_package");

			if ($this->data['product_details']->row()->category_id != ''){
				$category_id = $this->data['product_details']->row()->category_id;
			}else {
				$category_id = '';
			}
			$ImageName = '';
			$list_name_str = $this->data['product_details']->row()->list_name;
			$list_name_arr = explode(',', $list_name_str);
			$list_val_arr = explode(',', $this->data['product_details']->row()->list_value);
			if (is_array($list_name_arr) && count($list_name_arr)>0){
				$list_name_str = implode(',', $list_name_arr);
				$list_val_str = implode(',', $list_val_arr);
			}
			$addon_ids_array = array();
			$subproduct_ids_array = array();

			$addon_ids_array = explode(",",$this->data['product_details']->row()->addons);
			if(count($addon_ids_array) > 0){				
				$addon_prod_ids = implode(',', $addon_ids_array);
			}else{
				$addon_prod_ids= '';
			}

			$subproduct_ids_array = explode(",",$this->data['product_details']->row()->subproducts);

			if( count($subproduct_ids_array) > 0 ){
				$sub_prod_ids = implode(',', $subproduct_ids_array);
			}else{
				$sub_prod_ids = '';
			}

			$datestring = "%Y-%m-%d %h:%i:%s";
			$time = time();
			$seourl = url_title($product_name, '-', TRUE);
			$checkSeo = $this->product_model->get_all_details(PRODUCT,array('seourl'=>$seourl,'id !='=>$product_id));
			$seo_count = 1;
			while ($checkSeo->num_rows()>0){
				$seourl = $seourl.$seo_count;
				$seo_count++;
				$checkSeo = $this->product_model->get_all_details(PRODUCT,array('seourl'=>$seourl,'id !='=>$product_id));
			}
				$inputArr = array(
							'created' => mdate($datestring,$time),
							'seourl' => $seourl,
							'category_id' => $category_id,
							'product_name' => $product_name,
							'sku' => '',
							'status' => $product_status,
							'list_name' => $list_name_str,
							'list_value' => $list_val_str,
							'price_range'=> $price_range,
							'brand' => trim($brand),
							'is_addon' => $addon,
							'is_sub_product' => $sub_product,
							'user_id' => $this->data['product_details']->row()->user_id,
							'seller_product_id'	=> time(),
							'addons' => $addon_prod_ids,
							'subproducts' => $sub_prod_ids,
							'slider_view' => $slider,
							'min_tenure' => $min_tenure,
							'rental_freq' => $rental_freq,
							'sale_price' => $sale_price,
							'package_discount'=> $package_discount,
							'is_package' => $is_package,
							'subproduct_quantity'=> serialize($subproduct_quantity),
							'image' => ''
				);
				$condition = array();
				$data = $this->data['product_details']->row();	
				$image = $this->data['product_details']->row()->image;
				$count = 0;
				if($image != ''){
					$all_images = explode(',',$image);
					$new_images = '';
					foreach ($all_images as $key => $value) {
						if($value != ''){
							$file = 'images/product/'.$value;
							$full_path = base_url().$file;
							$newfile =  'images/product/'.$product_id.'_'.$value;
							if (copy($file, $newfile)) {
								$new_images .= $product_id.'_'.$value.',';
							}
							if($count == 0){
								$thumbh_img = 'images/product/Copressed Images/'.$value;
								$new_thumbh_img =  'images/product/Copressed Images/'.$product_id.'_'.$value;
								copy($thumbh_img, $new_thumbh_img);	
							}
						}
						$count++;
					}
					$inputArr['image'] = rtrim($new_images, ',');
				}
				$i = $this->product_model->cloneinsert(PRODUCT,'insert',$excludeArr,$inputArr,$condition,$data);
				
				$newproduct_id = $this->product_model->get_last_insert_id();
				
				 if($newproduct_id != ''){
					$header_snippet = '<link rel="canonical" href="'.base_url().'things/'.$newproduct_id.'/'.$seourl.'" />';
					$new_array  = $_SESSION['quantity_array'];
					$quantity_array = serialize($new_array);
					$quantityData_array= array('subproduct_quantity' => $quantity_array, 'header_code_snippet' => $header_snippet);
					$condition  = array('id' => $newproduct_id);
					$this->product_model->update_details(PRODUCT,$quantityData_array,$condition);
					unset( $_SESSION['quantity_array']);
				 }

				//Generate short url
				$short_url = $this->get_rand_str('6');
				$checkId = $this->product_model->get_all_details(SHORTURL,array('short_url'=>$short_url));
				while ($checkId->num_rows()>0){
					$short_url = $this->get_rand_str('6');
					$checkId = $this->product_model->get_all_details(SHORTURL,array('short_url'=>$short_url));
				}
				$url = base_url().'things/'.$newproduct_id.'/'.url_title($product_name,'-');
				$this->product_model->simple_insert(SHORTURL,array('short_url'=>$short_url,'long_url'=>$url));
				$urlid = $this->product_model->get_last_insert_id();
				$this->product_model->update_details(PRODUCT,array('short_url_id'=>$urlid),array('id'=>$newproduct_id));
				////////////////////
				
				$Attr_name_str = $Attr_val_str = '';
				
				$Attr_type_arr = $attr_id;
				$Attr_name_arr = $attr_name;
				$Attr_val_arr = $attr_price;

				
				if (is_array($Attr_name_arr) && count($Attr_name_arr)>0){
					for($k=0;$k<sizeof($Attr_name_arr);$k++){
						$dataSubArr = '';
						$dataSubArr = array('product_id'=> $newproduct_id,'attr_id'=>$Attr_type_arr[$k],'attr_name'=>$Attr_name_arr[$k],'attr_price'=>$Attr_val_arr[$k]);
						//echo '<pre>'; print_r($dataSubArr);
						$this->product_model->add_subproduct_insert($dataSubArr);
					}
				}

				$this->update_price_range_in_table('add',$price_range,$newproduct_id,$old_product_details);
				//Update the list table
			if (is_array($list_val_arr)){
				foreach ($list_val_arr as $list_val_row){
					$list_val_details = $this->product_model->get_all_details(LIST_VALUES,array('id'=>$list_val_row));
					if ($list_val_details->num_rows()==1){
						$product_count = $list_val_details->row()->product_count;
						$products_in_this_list = $list_val_details->row()->products;
						$products_in_this_list_arr = explode(',', $products_in_this_list);
						if (!in_array($product_id, $products_in_this_list_arr)){
							array_push($products_in_this_list_arr, $product_id);
							$product_count++;
							$list_update_values = array(
								'products'=>implode(',', $products_in_this_list_arr),
								'product_count'=>$product_count
							);
							$list_update_condition = array('id'=>$list_val_row);
							$this->product_model->update_details(LIST_VALUES,$list_update_values,$list_update_condition);
						}
					}
				}	
			}

			//Update user table count
			if ($edit_mode == 'insert'){
				if ($this->checkLogin('U') != ''){
					$user_details = $this->product_model->get_all_details(USERS,array('id'=>$this->checkLogin('U')));
					if ($user_details->num_rows()==1){
						$prod_count = $user_details->row()->products;
						$prod_count++;
						$this->product_model->update_details(USERS,array('products'=>$prod_count),array('id'=>$this->checkLogin('U')));
					}
				}
			}
				redirect('admin/product/edit_product_form/'.$newproduct_id);
			}
	}
	
	
	public function getmyfilters(){
	   	if ($this->checkLogin('A') == '') {
			redirect('admin');
		} else {
		    $filterValues = array_filter($this->input->post('cate_ids'));
		    $cats = implode(',',$filterValues);
		    $where = 'sub_category_id IN ('.$cats.') OR category_id IN ('.$cats.')';
			$filters = $this->product_model->get_all_details(FILTERS,$where);
// 			print_r($filters->result());exit;
            $jsonArray = array('data' =>$filters->result());
			echo json_encode($jsonArray);
		}
	}
}

/* End of file product.php */
/* Location: ./application/controllers/admin/product.php */