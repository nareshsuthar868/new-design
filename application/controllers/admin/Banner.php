<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * This controller contains the functions related to Product management
 * @author Teamtweaks
 *
 */
class Banner extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        }
        $this->load->helper(array('cookie', 'date', 'form'));
        $this->load->library(array('encrypt', 'form_validation','S3_upload','S3'));
        $this->load->model('banner_model');
        
        $this->CI =& get_instance();
		$this->CI->load->library('s3');

		$this->CI->config->load('s3', TRUE);
		$s3_config = $this->CI->config->item('s3');
		$this->bucket_name = $s3_config['bucket_name'];
		$this->folder_name = $s3_config['folder_name'];
		$this->s3_url = $s3_config['s3_url'];
    }
    
    public function banner_listing(){
        $this->data['heading'] = 'Banner List';
        $this->data['bannerList'] = $this->banner_model->get_all_details(BANNER, '1 = 1');
        $this->load->view('admin/banner/banner_list', $this->data);
    }
    
    public function add_banner($id = null){
        if($id){
            $this->data['heading'] = 'Edit Banner';    
            $this->data['bannerDetails'] = $this->banner_model->get_all_details(BANNER, ['id' => $id])->result()[0];
        }else{
            $this->data['heading'] = 'Add Banner';
        }
        $get_published_banner = $this->banner_model->get_all_details(BANNER, ['status' => 'Publish']);
        $count_total_banners = $this->banner_model->get_all_details(BANNER, '1 = 1')->num_rows();
        
        $this->form_validation->set_rules('text', 'Banner Text', 'required|trim');
        if($this->form_validation->run()){
            $config['overwrite'] = FALSE;
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['upload_path'] = './images/banner';
        	$upload_path = "images/banner";
            $allowed_types = "gif|jpg|png|jpeg";
            $this->load->library('upload', $config);
            if(!$id){
                if(empty($_FILES['image']['name'])){
                   $this->session->set_flashdata('error', 'Please select banner image'); 
                }else{
                    
                    // if($this->upload->do_upload('image')){
                    $addData = array();
                    $response =  $this->s3_upload->upload_file('image', $upload_path, $allowed_types);
                    if(!array_key_exists('errors', $response)){
                        // $banner_image = $this->upload->data();
                        // if($banner_image['image_width'] > 310 && $banner_image['image_height'] >  239){
                        //     $this->imageResizeWithSpace(310, 239, $banner_image['file_name'], './images/banner/');
                        //     $addData['image'] = $banner_image['file_name'];
                        // }else{
                        $addData['image'] = $response['upload_data']['file_name'];   
                        // }
                        $addData['text'] = $this->input->post('text');
                        $addData['position'] = $count_total_banners + 1;
                        if($get_published_banner->num_rows() >= 4){
                            $addData['status'] = 'Unpublish';
                        }else{
                            $addData['status'] = 'Publish';
                        }
                        $this->banner_model->commonInsertUpdate(BANNER, 'insert', [], $addData, "");
                        $this->setErrorMessage('success', 'Banner added successfully');
                        redirect('admin/banner/banner_listing');
                        
                    }else{
                        $this->setErrorMessage('error', $this->upload->display_errors());
                        redirect('admin/banner/add_banner');
                    }
                }
            }else{
                $updateData = array();
                $updateData['text'] = $this->input->post('text');
                if(!empty($_FILES['image']['name'])){
                    $response =  $this->s3_upload->upload_file('image', $upload_path, $allowed_types);
                    if(array_key_exists('success', $response) && $response['success'] == '1'){
                    // if($this->upload->do_upload('image')){
                        // $banner_image = $this->upload->data();
                        // if($banner_image['image_width'] > 310 || $banner_image['image_height'] >  239){
                        //     $this->imageResizeWithSpace(310, 239, $banner_image['file_name'], './images/banner/');
                        //     $updateData['image'] = $banner_image['file_name'];
                        // }else{
                        //     $updateData['image'] = $banner_image['file_name'];   
                        // }
                        // $updateData['image'] = $banner_image['file_name']; 
                        $updateData['image'] = $response['upload_data']['file_name'];
                    }else{
                        $this->setErrorMessage('error', $this->upload->display_errors());
                        redirect('admin/banner/edit_banner/'.$id);
                    }
                }
                $this->banner_model->commonInsertUpdate(BANNER, 'update', [], $updateData, ['id' => $id]);
                $this->setErrorMessage('success', 'Banner updated successfully');
                redirect('admin/banner/banner_listing');
            }
            
        }
        $this->load->view('admin/banner/add_banner', $this->data);
    }
    

    
    public function addupdate_banner(){
        // print_r(realpath('http://45.113.122.221/images/banner/1580993629.png'));exit;
        $bucket = 'developer-agile';
        $ext = 'png';
        $actual_image_name = time().".".$ext;
        $upload_path = "images/banner/";
        $updateData = array();
        $addData = array();
        if($_POST['image']){
            $file = $_POST['image']; //your data in base64 'data:image/png....';
            $img = str_replace('data:image/png;base64,', '', $file);
            file_put_contents($upload_path.$actual_image_name, base64_decode($img));
            $updateData['image'] = $actual_image_name;
            $addData['image'] =$actual_image_name;
            // $imageData =  base64_decode(end(explode(",", $_POST['image'])));
            
        }

        $get_published_banner = $this->banner_model->get_all_details(BANNER, ['status' => 'Publish']);
        $count_total_banners = $this->banner_model->get_all_details(BANNER, '1 = 1')->num_rows();
        if(!$_POST['banner_id'] ||  $_POST['banner_id'] == ''){
            // $addData['image'] = $response['upload_data']['file_name'];   
            $addData['text'] = $this->input->post('banner_text');
            $addData['position'] = $count_total_banners + 1;
            if($get_published_banner->num_rows() >= 4){
                $addData['status'] = 'Unpublish';
            }else{
                $addData['status'] = 'Publish';
            }
            $this->banner_model->commonInsertUpdate(BANNER, 'insert', ['banner_text'], $addData, "");
            $returnArray = array('status' =>200 ,'msg'=>'Banner added successfully');
        }else{
            $updateData['text'] = $this->input->post('banner_text');
            $this->banner_model->update_details(BANNER,$updateData,array('id' => $_POST['banner_id']));
            $returnArray = array('status' =>200 ,'msg'=>'Banner updated successfully');
        }
        
       
        header('Content-Type: application/json');
        echo json_encode($returnArray);
        exit;
    
        // $response =  $this->CI->s3->putObjectFile(base64_decode($img), $bucket , $actual_image_name, S3::ACL_PUBLIC_READ);
        // echo "<pre>";exit;
        // print_r($response);exit;
    }
    
    public function change_banner_status($status, $id){
        $get_published_banner = $this->banner_model->get_all_details(BANNER, ['status' => 'Publish']);
        if($status == 'Publish'){
            $this->banner_model->commonInsertUpdate(BANNER, 'update', [], ['status' => 'Unpublish'], ['id' => $id]);
            $this->setErrorMessage('success', 'Banner status updated successfully.');
            redirect('admin/banner/banner_listing');
        }else{
            if($get_published_banner->num_rows() >= 4){
                $this->setErrorMessage('error', 'More the 4 banner can not publish at the same time.');
                redirect('admin/banner/banner_listing');
            }else{
                $this->banner_model->commonInsertUpdate(BANNER, 'update', [], ['status' => 'Publish'], ['id' => $id]);
                $this->setErrorMessage('success', 'Banner status updated successfully.');
                redirect('admin/banner/banner_listing');
            }
        }
    }
    
    public function update_banner_position(){
        $id = $this->input->get('id');
        $old_position = $this->input->get('old_position');
        $new_position = $this->input->get('new_position');
        $count_total_banners = $this->banner_model->get_all_details(BANNER, '1 = 1')->num_rows();
        if($new_position > $count_total_banners){
            $this->setErrorMessage('error', 'Position can not be greater then the total number of banners.');
            echo json_encode([
                'status' => false,
                'data' => "Position can not be greater then the total number of banners."
            ]);
            return false;
        }
        $get_updated_row_position = $this->banner_model->get_all_details(BANNER, ['id' => $id])->result()[0];
        $get_existing_row_id = $this->banner_model->get_all_details(BANNER, ['position' => $new_position])->result()[0];
        $this->banner_model->commonInsertUpdate(BANNER, 'update', [], ['position' => $new_position], ['id' => $id]);
        $this->banner_model->commonInsertUpdate(BANNER, 'update', [], ['position' => $old_position], ['id' => $get_existing_row_id->id]);
        $this->setErrorMessage('success', 'Position updated successfully.');
        echo json_encode([
            'status' => true,
            'data' => "Position updated successfully."
        ]);
    }
}