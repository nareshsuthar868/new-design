<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This controller contains the functions related to cms management 
 * @author Teamtweaks
 *
 */

class Filter extends MY_Controller {

	function __construct(){
        parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));		
		$this->load->model('filter_model');
		// if ($this->checkPrivileges('cms',$this->privStatus) == FALSE){
		// 	redirect('admin');
		// }
    }
    
    /**
     * 
     * This function loads the cms list page
     */
   	public function index(){	
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			redirect('admin/cms/display_cms');
		}
	}

	public function display_filter_list(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Filter Listing';
			$this->load->view('admin/filter/display_filter_listing',$this->data);
		}	
	}

	public function get_filter_listing(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$start = (isset($_REQUEST['start']))? intval($_REQUEST['start']) : 0;
		    $length = (isset($_REQUEST['length']) && intval($_REQUEST['length']) > 0 )? $_REQUEST['length'] : 0;
			$orderIndex = (isset($_REQUEST['order'][0]['column']))? $_REQUEST['order'][0]['column'] : 0; 
	    	$orderType = (isset($_REQUEST['order'][0]['dir']))? $_REQUEST['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_REQUEST['search']['value']))? $_REQUEST['search']['value'] : NULL;
	        
	        $data = $this->filter_model->view_filter_details('active',NULL,$length,$start,$orderIndex,$orderType,$searchValue);
	        $TotalCount = $this->filter_model->view_filter_details('inactive','count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_REQUEST['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}	
	}

	public function add_filter(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {

			$this->data['heading'] = 'Add Category Filter';
			$this->data['parentCategory'] = $this->filter_model->get_all_details(CATEGORY,array('rootID' => '0','status' => 'Active'));
 			$this->load->view('admin/filter/add_filter',$this->data);
		}
	}

	public function get_subcategories(){
		if ($this->checkLogin('A') == ''){
			echo json_encode(array('msg' => 'Invalid Access','code' => 400));
		}else {
			$SubCategories = $this->filter_model->get_all_details(CATEGORY,array('rootID' =>$_GET['catID'],'status' => 'Active' ));
			header('Content-Type: application/json');
			echo json_encode(array('data'=> $SubCategories->result_array() , 'msg' => 'Sub Category Listing'));
		}
	}

	public function edit_filter(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['filterDetails'] = $this->filter_model->get_all_details(FILTERS,array('id' => $this->uri->segment(4)));
			if($this->data['filterDetails']->num_rows() > 0){
				$this->data['heading'] = 'Update Category Filter';
				$this->data['parentCategory'] = $this->filter_model->get_all_details(CATEGORY,array('rootID' => '0','status' => 'Active'));
				$this->data['childCategory'] = $this->filter_model->get_all_details(CATEGORY,array('rootID ' => $this->data['filterDetails']->row()->category_id,'status' => 'Active'));
			}
 			$this->load->view('admin/filter/edit_filter',$this->data);
		}

	}
	public function insertUpdateFilters(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else{
			$parentCategory = $this->input->post('parentCategory');
			$ChildCategory = $this->input->post('ChildCategory');
			$filtername = $this->input->post('filter_name');
			$filter_tag = strtolower(str_replace(' ','_', $filtername));
			if($this->input->post('id') != ''){
				$this->filter_model->update_details(FILTERS,array('filter_name'=> $filtername,'category_id' => $parentCategory ,'sub_category_id' => $ChildCategory),array('id' => $this->input->post('id')));
			}else{
			    $Checkfilter = $this->filter_model->get_all_details(FILTERS,array('filter_tag' => $filter_tag,'category_id' => $parentCategory ,'sub_category_id' => $ChildCategory));
			    if($Checkfilter->num_rows() > 0){
			        $this->setErrorMessage('error', 'Filter already exists');
                    echo "<script>window.history.go(-1)</script>";
                    exit();   
			    }
				$this->filter_model->simple_insert(FILTERS,array('filter_name'=> $filtername,'filter_tag'=>$filter_tag ,'category_id' => $parentCategory ,'sub_category_id' => $ChildCategory));
			}
		}
		redirect('admin/filter/display_filter_list');
		// echo "<pre>";
		// print_r($this->input->post());exit;
	}
	

	
}

/* End of file cms.php */
/* Location: ./application/controllers/admin/cms.php */