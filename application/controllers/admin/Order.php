<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This controller contains the functions related to Order management
 * @author Teamtweaks
 *
 */

class Order extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model('order_model');
		if ($this->checkPrivileges('order',$this->privStatus) == FALSE){
			redirect('admin');
		}
	}

	/**
	 *
	 * This function loads the order list page
	 */
	public function index(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			redirect('admin/order/display_order_list');
		}
	}

	/**
	 *
	 * This function loads the order list page
	 */
	public function display_order_paid(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Order List';
// 			$this->data['orderList'] = $this->order_model->view_order_details('Paid');
			$this->load->view('admin/order/display_orders',$this->data);
		}
	}
	
	public function Get_Paid_order(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$start = (isset($_GET['start']))? intval($_GET['start']) : 0;
		    $length = (isset($_GET['length']) && intval($_GET['length']) > 0 )? $_GET['length'] : 0;
			$orderIndex = (isset($_GET['order'][0]['column']))? $_GET['order'][0]['column'] : 0; 
	    	$orderType = (isset($_GET['order'][0]['dir']))? $_GET['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_GET['search']['value']))? $_GET['search']['value'] : NULL;
	        
			// print_r($orderType);exit;
	        $data = $this->order_model->view_order_details('Paid',NULL,$length,$start,$orderIndex,$orderType,$searchValue);
	        $TotalCount = $this->order_model->view_order_details('Paid','count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_GET['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}
	}

	public function display_order_pending(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Order List';
// 			$this->data['orderList'] = $this->order_model->view_order_details('Pending');
			$this->load->view('admin/order/display_orders_pending',$this->data);
		}
	}
	
	public function Get_pending_order(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$start = (isset($_GET['start']))? intval($_GET['start']) : 0;
		    $length = (isset($_GET['length']) && intval($_GET['length']) > 0 )? $_GET['length'] : 0;
			$orderIndex = (isset($_GET['order'][0]['column']))? $_GET['order'][0]['column'] : 0; 
	    	$orderType = (isset($_GET['order'][0]['dir']))? $_GET['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_GET['search']['value']))? $_GET['search']['value'] : NULL;
	        
			// print_r($orderType);exit;
	        $data = $this->order_model->view_order_details('Pending',NULL,$length,$start,$orderIndex,$orderType,$searchValue);
	        $TotalCount = $this->order_model->view_order_details('Pending','count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_GET['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}

	}
	public function display_bulk_orders(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Bulk Order';
// 			$this->data['BulkOrder'] = $this->order_model->view_bulk_orders();
			$this->load->view('admin/order/display_bulk_orders',$this->data);
		}
	}
	
		// Get bulk order listing
	public function get_bulk_order_listing(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$start = (isset($_REQUEST['start']))? intval($_REQUEST['start']) : 0;
		    $length = (isset($_REQUEST['length']) && intval($_REQUEST['length']) > 0 )? $_REQUEST['length'] : 0;
			$orderIndex = (isset($_REQUEST['order'][0]['column']))? $_REQUEST['order'][0]['column'] : 0; 
	    	$orderType = (isset($_REQUEST['order'][0]['dir']))? $_REQUEST['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_REQUEST['search']['value']))? $_REQUEST['search']['value'] : NULL;
	        
			// print_r($orderType);exit;
	        $data = $this->order_model->get_listing_bulk_orders('Pending',NULL,$length,$start,$orderIndex,$orderType,$searchValue);
	        $TotalCount = $this->order_model->get_listing_bulk_orders('Pending','count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_REQUEST['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}
	}

	public function delete_bulk_order(){
		$id = $this->input->post('id');
		$response = $this->order_model->delete_bulk_order($id);
		echo json_encode($response);
	}

	public function subviewDetails(){

		echo $this->input->post('dealId');

	}


	/**
	 *
	 * This function loads the order view page
	 */
	public function view_order(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'View Order';
			$user_id = $this->uri->segment(4,0);
			$deal_id = $this->uri->segment(5,0);
			$this->data['purchaseList'] = $this->order_model->view_orders_new_design($user_id,$deal_id); 
			$this->load->view('site/order/invoice',$this->data);
		}
	}

	/**
	 *
	 * This function delete the order record from db
	 */
	public function delete_order(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$order_id = $this->uri->segment(4,0);
			$condition = array('id' => $order_id);
			$old_order_details = $this->order_model->get_all_details(PRODUCT,array('id'=>$order_id));
			$this->update_old_list_values($order_id,array(),$old_order_details);
			$this->update_user_order_count($old_order_details);
			$this->order_model->commonDelete(PRODUCT,$condition);
			$this->setErrorMessage('success','Order deleted successfully');
			redirect('admin/order/display_order_list');
		}
	}

	public function order_review(){
		if ($this->checkLogin('A')==''){
			show_404();
		}else {
			$dealCode = $this->uri->segment(2,0);
			//$order_details = $this->order_model->get_all_details(PAYMENT,array('dealCodeNumber'=>$dealCode,'status'=>'Paid'));
			$this->db->select('p.*,pAr.attr_name as attr_type,sp.attr_name');
			$this->db->from(PAYMENT.' as p');
			$this->db->join(SUBPRODUCT.' as sp' , 'sp.pid = p.attribute_values','left');
			$this->db->join(PRODUCT_ATTRIBUTE.' as pAr' , 'pAr.id = sp.attr_id','left');
			$this->db->where('p.status = "Paid" and p.dealCodeNumber = "'.$dealCode.'"');
			$order_details = $this->db->get();
				
			if ($order_details->num_rows()==0){
				show_404();
			}else {
				foreach ($order_details->result() as $order_details_row){
					$this->data['prod_details'][$order_details_row->product_id] = $this->order_model->get_all_details(PRODUCT,array('id'=>$order_details_row->product_id));
				}
				$this->data['order_details'] = $order_details;
				$this->data['heading'] = 'View Order Comments';
				$sortArr1 = array('field'=>'date','type'=>'desc');
				$sortArr = array($sortArr1);
				$this->data['order_comments'] = $this->order_model->get_all_details(REVIEW_COMMENTS,array('deal_code'=>$dealCode),$sortArr);
				$this->load->view('admin/order/display_order_reviews',$this->data);
			}
		}
	}


	public function post_order_comment(){
		if ($this->checkLogin('A') != ''){
			$this->order_model->commonInsertUpdate(REVIEW_COMMENTS,'insert',array(),array(),'');
		}
	}

	public function update_payment_type(){
		if ($this->checkLogin('A') != ''){
			$id = $this->input->post('id');
			$payment_type = $this->input->post('payment_type');
			if ($id != ''){
				$this->order_model->update_details(PAYMENT,array('payment_type'=>$payment_type),array('id'=>$id));
			}
		}
	}
	public function update_vendor(){
		if ($this->checkLogin('A') != ''){
			$id = $this->input->post('id');
			$vendor = $this->input->post('vendor');
			if ($id != ''){
				$this->order_model->update_details(PAYMENT,array('Vendor'=>$vendor),array('id'=>$id));
			}
		}
	}
	public function update_sell_id(){
		if ($this->checkLogin('A') != ''){
			$id = $this->input->post('id');
			$sell_id= $this->input->post('sell_id');
			if ($id != ''){
				$this->order_model->update_details(PAYMENT,array('sell_id'=>$sell_id),array('id'=>$id));
			}
		}
	}
	public function update_courier_name(){
		if ($this->checkLogin('A') != ''){
			$id = $this->input->post('id');
			$courier_name= $this->input->post('courier_name');
			if ($id != ''){
				$this->order_model->update_details(PAYMENT,array('courier_name'=>$courier_name),array('id'=>$id));
			}
		}
	}
	public function update_tracking_id(){
		if ($this->checkLogin('A') != ''){
			$id = $this->input->post('id');
			$tracking_id= $this->input->post('tracking_id');
			if ($id != ''){
				$this->order_model->update_details(PAYMENT,array('tracking_id'=>$tracking_id),array('id'=>$id));
			}
		}
	}
	public function update_status(){
		if ($this->checkLogin('A') != ''){
			$id = $this->input->post('id');
			$status= $this->input->post('status');
			if ($id != ''){
				$this->order_model->update_details(PAYMENT,array('status'=>$status),array('id'=>$id));
			}
		}
	}
	public function update_shipping_status(){
		if ($this->checkLogin('A') != ''){
			$id = $this->input->post('id');
			$shipping_status= $this->input->post('shipping_status');
			if ($id != ''){
				$this->order_model->update_details(PAYMENT,array('shipping_status'=>$shipping_status),array('id'=>$id));
			}
		}
	}
	public function update_received_payment(){
		if ($this->checkLogin('A') != ''){
			$id = $this->input->post('id');
			$received_payment = $this->input->post('received_payment');
			if ($id != ''){
				$this->order_model->update_details(PAYMENT,array('received_payment'=>$received_payment),array('id'=>$id));
			}
		}
	}
	public function update_exp_dispatch(){
		if ($this->checkLogin('A') != ''){
			$id = $this->input->post('id');
			$exp_dispatch = $this->input->post('exp_dispatch');
			if ($id != ''){
				$this->order_model->update_details(PAYMENT,array('exp_dispatch'=>$exp_dispatch),array('id'=>$id));
			}
		}
	}
	
	public function send_voucher_email(){
        $user_id = $this->input->post('user_id');
		$order_id = $this->input->post('order_id');
        $User_data = $this->order_model->get_all_details(USERS,array('id' => $user_id));
		$Secret_Data = $this->order_model->get_all_details('fc_voucher_settings',array('id !=' => ''));
	    $Reward  = base64_encode($Secret_Data->row()->reward);
	    $post_url = $Secret_Data->row()->post_url;
	    $return_url = $Secret_Data->row()->return_url;
	    $partner_code = $Secret_Data->row()->partner_code;
	    $user_name = str_replace(' ', '', $User_data->row()->user_name);
	    $timeStamp = base64_encode(time());
	    $url = file_get_contents('https://tinyurl.com/api-create.php?url='.'https://kbba.klippd.in/?clientID='.$Secret_Data->row()->client_id.'&key='.$Secret_Data->row()->key_id.'&userID='.$User_data->row()->id.'&rewards='.$Reward.'&username='.$user_name.'&EmailAddress='.$User_data->row()->email.'&postURL='.$post_url.'&returnURL='.$return_url.'&PartnerCode='.$partner_code.'&timeStamp='.$timeStamp.'');
	    $voucher_url =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                <html xmlns="http://www.w3.org/1999/xhtml">
                <head>
                <title>Welcome Cityfurnish</title>
                  <style type="text/css">
                  @media screen and (max-width: 580px) {
                    .tab-container{max-width: 100%;}
                    .txt-pad-15 {padding: 0 15px 51px 15px !important;}
                    .foo-txt {padding: 0px 15px 18px 15px !important;}
                    .foo-add {padding: 20px !important;}
                    .tab-padd-zero{padding:0px !important;}
                    .tab-padd-right{padding-right:25px !important;}
                    .pad-20{padding:25px 20px 20px !important;}
                    .social-padd-left{padding:15px 20px 0px 0px; !important;}
                    .offerimg{width:100% !important;}
                    .mobilefont{font-size:16px !important;line-height:18px !important;}
                  }
                 </style>
                </head>
                <body style="margin:0px;">
                <table class="tab-container" name="main" border="0" cellpadding="0" cellspacing="0" style="background-color: #fff;margin: 0 auto;font-family:Arial, Helvetica, sans-serif;font-size:14px;border-collapse:collapse;width:600px;
                border-width:1px; border-style:solid; border-color:#e7e7e7;table-layout: fixed;display: block;border-right-width: 0px;">

                    <tr>
                     <td style="width:100%">
                      <table style="width:100%;table-layout:fixed" cellpadding="0" cellspacing="0px">
                      <tr>
                      <td style="text-align: left;padding:15px 0px 15px 20px;padding-left:20px;">
                        <a href="https://cityfurnish.com/"><img src="https://cityfurnish.com/images/logo-2.png" alt="logo" style="width:150px;" /></a>
                      </td>
                      <td style="text-align: right;padding:15px 20px 15px 0px;text-align:right;width:50%;border-right: 1px solid #e7e7e7" class="social-padd-left">
                        <a href="https://www.facebook.com/cityFurnishRental" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/facebook.png" alt="facebook" width="18px" /></a>
                        <a href="https://twitter.com/CityFurnish" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/twitter.png" alt="twitter" width="18px"/></a>
                        <a href="https://plus.google.com/+cityfurnish" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/google-plus.png" alt="google" height="18px"/></a>
                        <a href="https://in.pinterest.com/cityfurnish/" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/pintrest.png" alt="pintrest" width="18px" /></a>
                        <a href="https://www.linkedin.com/company/cityfurnish?trk=biz-companies-cym" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/linkedin.png" alt="linkedin" width="18px" /></a>
                        </td>
                      </tr>
                      </table>
                     </td>
                    </tr>
                    <tr style="width:100%;">
                    <td style="width:100%;border-right: 1px solid #e7e7e7">
                      <img src="https://cityfurnish.com/images/voucher-banner.jpg" style="display: block;width: 100%;">
                    </td>
                    </tr>
                    <tr style="width:100%;">
                      <td style="padding-top: 50px;font-size: 40px;color: #002e40;text-align: center;border-right: 1px solid #e7e7e7">
                      <h1 style="color: #002e40;font-size: 40px;line-height:36px;font-weight:bold;margin:0px;">Congratulations!</h1>
                           <span style="font-size: 18px;">You Got Gift Vouchers Worth</span>
                     </td>
                       
                    </tr>
                    <tr style="width:100%;">
                      <td style="text-align: center;padding-top: 5px;border-right: 1px solid #e7e7e7">
                        <span style="display: inline-block;vertical-align: middle;margin-right: 10px;">
                          <img src="https://cityfurnish.com/images/rupee-icn.png">
                        </span>
                        <span style="display: inline-block;vertical-align: middle;color: #f9aa2b;font-size: 45px;"><strong>'.$Secret_Data->row()->reward.'</strong></span></td>
                    </tr>
                    <tr style="width:100%;">
                      <td style="text-align: center;padding-top: 15px;padding-bottom: 25px;font-size: 24px;border-right: 1px solid #e7e7e7">
                          <span style="display: inline-block;vertical-align: middle;font-size: 18px;">
                            From Cityfurnish for your order #'.$order_id.'
                          </span>
                      </td>
                    </tr>
                    <tr style="width:100%;">
                      <td style="text-align: center;font-size: 24px;color: #141414 !important;border-right: 1px solid #e7e7e7">
                          <a href="'.$url.'" target="_blank" style="background: #a5c8c2;width: 198px;text-decoration: none !important;font-size: 13px;display: inline-block;padding-top: 10px;padding-bottom: 10px;color: #141414 !important;">REDEEM</a>
                      </td>
                    </tr>
                    <tr style="width:100%;">
                    	<td style="padding: 20px;">
                    	  <h5>Terms and Conditions:</h5>
                    	  <span style="font-size: 10px;">1.You can opt for multiple voucher up-to-cumulative total amount specified above.</span><br>
                    	  <span style="font-size: 10px;">2.Voucher link will be active for 60 days from send date .You can redeem desired vouchers by visting link multiple times using link within this period.</span><br>
                    	  <span style="font-size: 10px;">3.Once your have selected vouchers for redemption, you will receive selected voucher on email with details instruction for usage of each voucher. </span><br>
                    	  <span style="font-size: 10px;">4.Please contact us on  8010845000 or hello@cityfurnish.com in case your need any assistance.</span>
                    	</td>
                    </tr>
                    <tr  style="width:100%;">
                        <td style="padding:30px 50px 26px;text-align:center;line-height:24px;width:100%;border-right: 1px solid #e7e7e7">
                            <p style="margin:0px;line-height:22px;font-family:Arial, Helvetica, sans-serif;color:#b2b2b2">Sent by <a href="https://cityfurnish.com/" style="color:#38373d;text-decoration:none;">Cityfurnish</a>, 525-527, Block D, JMD Megapolis, Sohna Road, Sector 48, Gurgaon, Haryana - 122018<br /><a href="mailto:hello@cityfurnish.com" style="color:#38373d; margin-top:8px;display:inline-block;text-decoration:none;">hello@cityfurnish.com</a></p>
                        </td>
                    </tr>
                </table>
                </body>
                </html>';

        $email_values = array('mail_type'=>'html',
                         'from_mail_id'=>'hello@cityfurnish.com',
                         'mail_name'=>'Cityfurnish',
                         'to_mail_id'=> $User_data->row()->email,
                         'cc_mail_id'=> 'hello@cityfurnish.com',
                         'subject_message'=>'Cityfurnish - Gift Vouchers',
                         'body_messages'=> $voucher_url
        );
        $email_send_to_common = $this->order_model->common_email_send($email_values);
		if($email_send_to_common){
			$result = array('status'=> true,'msg'=>'Mail Send Successfully');
			header('Content-Type: application/json');
			echo json_encode($result);exit;
		}
	}



	public function get_recurring_payment_user(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Recurring Order';
			$recurring = $this->order_model->GetSiREcords();
			$customer_payment = $this->order_model->get_customer_payment();
			$this->data['recurring_payment'] = array_merge($customer_payment->result(),$recurring->result());
			$this->load->view('admin/order/display_recurring_orders',$this->data);
		}
	}
	

	public function customer_payment(){
			if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Customer Payment';
// 			$this->data['customer_payment'] = $this->order_model->customer_payment();
			$this->load->view('admin/order/customer_payment',$this->data);
		}
	}
	
    public function show_customer_payment(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$start = (isset($_GET['start']))? intval($_GET['start']) : 0;
		    $length = (isset($_GET['length']) && intval($_GET['length']) > 0 )? $_GET['length'] : 0;
			$orderIndex = (isset($_GET['order'][0]['column']))? $_GET['order'][0]['column'] : 0; 
	    	$orderType = (isset($_GET['order'][0]['dir']))? $_GET['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_GET['search']['value']))? $_GET['search']['value'] : NULL;
	        
	        $data = $this->order_model->view_customer_payment_details('Paid',NULL,$length,$start,$orderIndex,$orderType,$searchValue);

	        $all_payouts = $this->order_model->get_all_details(CP_CUSTOMER_PAYMENT,array('is_recurring !=' => 1));

	        $TotalCount = $this->order_model->view_customer_payment_details('Paid','count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_GET['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}

	}

	
	public function zoho_logs(){
    	if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Zoho Logs';
			$this->data['logs'] = $this->order_model->get_all_details(ZOHO_LOGS,array());
			// $this->order_model->view_bulk_orders();
			$this->load->view('admin/order/zoho_logs',$this->data);
		}	
    }

    public function update_zoho_logs(){
    	if ($this->checkLogin('A') == ''){
			echo "Invalid Access";exit;
		}else {
    		return 	$this->order_model->update_details(ZOHO_LOGS,array('solved'=> $this->input->post('solved')),array('id'=> $this->input->post('log_id')));
    	}
    }

    public function delete_logs(){
    	$id = $this->input->post('id');
		$response = $this->order_model->commonDelete(ZOHO_LOGS,array('id' => $this->input->post('log_id')));
		echo json_encode(true);
    }

    public function get_all_logs(){
    	if ($this->checkLogin('A') == ''){
    		echo "Invalid Access";exit;
    	}else{
	    	$data = $this->order_model->get_all_details(ZOHO_LOGS,array('id' => $_GET['id']));
	    	if($data->num_rows() > 0){
	    		
    			$this->data['logs'] = unserialize($data->row()->json_array);
				// $this->order_model->view_bulk_orders();
				$this->load->view('admin/order/view_zoho_logs',$this->data);
	    	}
	    	
    	}
    }
    
    
    public function display_order_listing(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Order List';
			// $this->data['orderList'] = $this->order_model->view_order_details('Paid');
			$this->load->view('admin/order/customer_payout',$this->data);
		}
	}


	public function order_listing(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$start = (isset($_GET['start']))? intval($_GET['start']) : 0;
		    $length = (isset($_GET['length']) && intval($_GET['length']) > 0 )? $_GET['length'] : 0;
			$orderIndex = (isset($_GET['order'][0]['column']))? $_GET['order'][0]['column'] : 0; 
	    	$orderType = (isset($_GET['order'][0]['dir']))? $_GET['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_GET['search']['value']))? $_GET['search']['value'] : NULL;
	        
	        $data = $this->order_model->view_order_payout_details('Paid',NULL,$length,$start,$orderIndex,$orderType,$searchValue);

	        $all_payouts = $this->order_model->get_all_details(PAYOUT,array('payout_status' => 'Paid'));
	        foreach ($data as $mainkey => $value) {
	        	// $payout_amount = '';
	        	foreach ($all_payouts->result() as $key => $val) {
	        		if($value->id == $val->order_id){
	        			$data[$mainkey]->total_payout += $val->payout_amount;
	        		}
	        	}
	        }
	        $TotalCount = $this->order_model->view_order_payout_details('Paid','count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_REQUEST['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}
	}

	public function get_user_orders(){
		$AdminSettings = $this->order_model->get_all_details('fc_admin_settings',array('id !=' => ''));
		$date = strtotime('+'.$AdminSettings->row()->payout_expiry_days.' day');
		$expire_time =  date('Y-m-d', $date);
		// print_r($expire_time);exit;
		$response = $this->order_model->get_user_order_details($this->input->post('order_id'));
		$response->expire_time = $expire_time;
		echo json_encode($response);
	}



    public function create_payout(){
    	if ($this->checkLogin('A') == ''){
			echo "Invalid Access";exit;
		}else {
			if($this->input->post('resend') == 'false'){
				$check_same_cashgram = 	$this->order_model->get_all_details(PAYOUT,array('order_id' => $this->input->post('order_id'),'payout_amount' => $this->input->post('payout_amount'),'payout_status != ' => 'Paid'));
				if($check_same_cashgram->num_rows() > 0){
					$response = array('subCode' => 400,'message' => "Same amount of cashgram request already pending.");
				    	echo json_encode($response);
				    	exit;
				}
			}
	    	$response = $this->generate_authorize_token();
	   // 	print_r($response);exit;
	    	if($response->subCode == 200){
	    		$is_validate = $this->validate_token($response->data);
	    		if($is_validate->subCode == 200){
	    			$url = CASHGRMA_URL.'/payout/v1/createCashgram';
	    			$cashgramId = 'CF'.$this->input->post('user_id').time();
    			    $curent_date = date("Y-m-d", strtotime("+1 week"));
				    if($this->input->post('expire_time') > $curent_date){
				    	$response = array('subCode' => 400,'message' => "Invalid date selected.");
				    	echo json_encode($response);
				    	exit;
				    }
	    			$expire_date = date('Y/m/d',strtotime($this->input->post('expire_time')));
	    			$data = array();
					$data = array(
						"cashgramId" => $cashgramId,
						"amount" => $this->input->post('payout_amount'),
						"name"=>$this->input->post('name'),
						// "email" => $this->input->post('email'),
						"phone"=> $this->input->post('mobile_number'),
						"linkExpiry" => $expire_date,
						"remarks"=>'',
						"notifyCustomer"=> 1
					);
					$postdata = json_encode($data);
					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
					curl_setopt($ch, CURLOPT_HTTPHEADER, 
						array(
							'Content-Type: application/json',
							'Authorization:Bearer '.$response->data->token
						)
					);
					$result = curl_exec($ch);
					$insert_respo = json_decode($result);
					if($insert_respo->subCode == 200){
						$post  = $this->input->post();
						$post['cashgramId'] = $cashgramId;
						if($this->input->post('resend') == 'true' &&  $this->input->post('payout_status') != 'Expired'){
							$respo = $this->deactive_cashgram($this->input->post('cashgramId'));
							if($respo->subCode == 200){
								$this->update_payout_status('Expired',$this->input->post('cashgramId'));
							}else{
								echo json_encode($respo);
								exit;
							}
						}
  						$this->order_model->update_shipped_payout($insert_respo,$post);
  						$data['email'] = $this->input->post('email');
  						$data['remarks'] = $this->input->post('customer_remark');
  						$this->sendCashgrmaEmail($insert_respo->data->cashgramLink,$data);
						echo $result;
					}else{
						echo $result;
					}
	    		}else{
	    			echo $is_validate;
	    		}
	    	}else{
	    		echo $response;
	    	}
	    }
    }

    public function sendCashgrmaEmail($link,$details){
		$Cashgram =  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	                <html xmlns="http://www.w3.org/1999/xhtml">
	                <head>
	                <title>Welcome Cityfurnish</title>
	                  <style type="text/css">
	                  @media screen and (max-width: 580px) {
	                    .tab-container{max-width: 100%;}
	                    .txt-pad-15 {padding: 0 15px 51px 15px !important;}
	                    .foo-txt {padding: 0px 15px 18px 15px !important;}
	                    .foo-add {padding: 20px !important;}
	                    .tab-padd-zero{padding:0px !important;}
	                    .tab-padd-right{padding-right:25px !important;}
	                    .pad-20{padding:25px 20px 20px !important;}
	                    .social-padd-left{padding:15px 20px 0px 0px; !important;}
	                    .offerimg{width:100% !important;}
	                    .mobilefont{font-size:16px !important;line-height:18px !important;}
	                  }
	                 </style>
	                </head>
	                <body style="margin:0px;">
	                <table class="tab-container" name="main" border="0" cellpadding="0" cellspacing="0" style="background-color: #fff;margin: 0 auto;font-family:Arial, Helvetica, sans-serif;font-size:14px;border-collapse:collapse;width:600px;
	                border-width:1px; border-style:solid; border-color:#e7e7e7;table-layout: fixed;display: block;border-right-width: 0px;">

	                    <tr>
	                     <td style="width:100%">
	                      <table style="width:100%;table-layout:fixed" cellpadding="0" cellspacing="0px">
	                      <tr>
	                      <td style="text-align: left;padding:15px 0px 15px 20px;padding-left:20px;">
	                        <a href="https://cityfurnish.com/"><img src="https://cityfurnish.com/images/logo-2.png" alt="logo" style="width:150px;" /></a>
	                      </td>
	                      <td style="text-align: right;padding:15px 20px 15px 0px;text-align:right;width:50%;border-right: 1px solid #e7e7e7" class="social-padd-left">
	                        <a href="https://www.facebook.com/cityFurnishRental" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/facebook.png" alt="facebook" width="18px" /></a>
	                        <a href="https://twitter.com/CityFurnish" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/twitter.png" alt="twitter" width="18px"/></a>
	                        <a href="https://plus.google.com/+cityfurnish" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/google-plus.png" alt="google" height="18px"/></a>
	                        <a href="https://in.pinterest.com/cityfurnish/" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/pintrest.png" alt="pintrest" width="18px" /></a>
	                        <a href="https://www.linkedin.com/company/cityfurnish?trk=biz-companies-cym" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/linkedin.png" alt="linkedin" width="18px" /></a>
	                        </td>
	                      </tr>
	                      </table>
	                     </td>
	                    </tr>
	                    <tr style="width:100%;">
	                    <td style="width:100%;border-right: 1px solid #e7e7e7">
	                      <img src="https://cityfurnish.com/images/voucher-banner.jpg" style="display: block;width: 100%;">
	                    </td>
	                    </tr>
	                    <tr style="width:100%;">
	                      <td style="font-size: 28px;padding-top: 50px;padding-left: 14px;color: #002e40; border-right: 1px solid #e7e7e7">
	                    
	                           <span style="font-size: 18px;">
	                           Dear '.ucfirst($details['name']).',<br>

								We have processed refund of Rs '.$details['amount'].' for your order at Cityfurnish. Please use below one-time link to receive payment in your account via your preferred mode.
	                           </span>
	                     </td>
	                       
	                    </tr>

	                    <tr style="width:100%;">
	                      <td style="text-align: center;font-size: 24px;color: #141414 !important;border-right: 1px solid #e7e7e7">
	                          <a href="'.$link.'" target="_blank" style="background: #a5c8c2;width: 198px;text-decoration: none !important;font-size: 13px;display: inline-block;padding-top: 10px;padding-bottom: 10px;color: #141414 !important;">Click to get Rs '.$details['amount'].'</a>
	                      </td>
	                    </tr>

	                    <tr style="width:100%;">
	                    	<td style="padding: 20px;">
	                    	  <span style="font-size: 15px;">Cashgram Id	: '.$details['cashgramId'].'</span><br>
	                    	  <span style="font-size: 15px;">Amount  : '.$details['amount'].'</span><br>
	                    	  <span style="font-size: 15px;">Remarks :<br> '.$details['remarks'].'</span><br><br>
	                    	Please feel free to get in touch with us at 8010845000 or hello@cityfurnish.com for any assistance.<br><br>
	                    	Best Regards<br>
							Team Cityfurnish 
	                    	</td>
                    	</tr>
	                    <tr  style="width:100%;">
	                        <td style="padding:30px 50px 26px;text-align:center;line-height:24px;width:100%;border-right: 1px solid #e7e7e7">
	                            <p style="margin:0px;line-height:22px;font-family:Arial, Helvetica, sans-serif;color:#b2b2b2">Sent by <a href="https://cityfurnish.com/" style="color:#38373d;text-decoration:none;">Cityfurnish</a> 525-527, Block D, JMD Megapolis, Sohna Road, Sector 48, Gurgaon, Haryana , 122018<br /><a href="mailto:hello@cityfurnish.com" style="color:#38373d; margin-top:8px;display:inline-block;text-decoration:none;">hello@cityfurnish.com</a></p>
	                        </td>
	                    </tr>
	                </table>
	                </body>
	                </html>';

        $email_values = array('mail_type'=>'html',
                         'from_mail_id'=>'hello@cityfurnish.com',
                         'mail_name'=>'Cityfurnish',
                         'to_mail_id'=> $details['email'],
                         'subject_message'=>'Refund for your order at Cityfurnish',
                         'body_messages'=> $Cashgram
        );
        $email_send_to_common = $this->order_model->common_email_send($email_values);
    }
    
    
    public static function 	getSignature() {
	    $clientId = "CF12992CJT9IE5EXA4IQQ2";
	    // $publicKey = CASHGRAM_PUBLIC_KEY;
	   	$publicKey = openssl_pkey_get_public(file_get_contents("assets/CityFurnish_PublicKey_Prod.pem"));
	    // print_r($publicKey);exit;
	    $encodedData = $clientId.".".strtotime("now");
	    return static::encrypt_RSA($encodedData, $publicKey);

	}
	private static function encrypt_RSA	($plainData, $publicKey) {
	  	if (openssl_public_encrypt($plainData, $encrypted, $publicKey, OPENSSL_PKCS1_OAEP_PADDING))
	  	{
	      $encryptedData = base64_encode($encrypted);
	  	}
	    else
	    {
 			return NULL;
	    }
	    	
	    return $encryptedData;
  	}

    public function generate_authorize_token(){
		if ($this->checkLogin('A') == ''){
			echo "Invalid Access";exit;
		}else {
			$secret_key = $this->order_model->get_all_details(PAYMENT_GATEWAY,array('id' => 5));
			$data = $secret_key->row();
			$secret_key = unserialize($data->settings);
// 			print_r($secret_key);exit;

	    	$url = CASHGRMA_URL.'/payout/v1/authorize';
			$postdata = array();
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, 
				array(
					'X-Client-Id:'.$secret_key['client_id'],
					'X-Client-Secret:'.$secret_key['client_token'],
					'X-Cf-Signature:'.$this->getSignature()
				)
			);
			$result = curl_exec($ch);
			return json_decode($result);
		}
    }

    public function validate_token($data){
    	if ($this->checkLogin('A') == ''){
			echo "Invalid Access";exit;
		}else {
	    	$url = CASHGRMA_URL.'/payout/v1/verifyToken';
			$postdata = array();
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, 
				array(
					'Authorization : Bearer '.$data->token,
				)
			);
			$result = curl_exec($ch);
			return json_decode($result);
		}

    }

    public function check_payout_status(){
    	if ($this->checkLogin('A') == ''){
			echo "Invalid Access";exit;
		}else {
	    	$response = $this->generate_authorize_token();
	    	if($response->subCode == 200){
	    		$is_validate = $this->validate_token($response->data);
	    		if($is_validate->subCode == 200){
	    			$url = CASHGRMA_URL.'/payout/v1/getCashgramStatus?cashgramId='.$this->input->post('cashgramid');
					$postdata = array();
					$ch = curl_init($url);
					// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					// curl_setopt($ch, CURLOPT_POST, 1);
					// curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
					curl_setopt($ch, CURLOPT_HTTPHEADER, 
						array(
							'Authorization : Bearer '.$response->data->token,
						)
					);
					$result = curl_exec($ch);

					// print_r($result);exit;
					echo  $result;

	    		}else{
	    			echo $is_validate;
	    		}
	    	}else{
	    		echo $response;
	    	}
	    }

    }

    public function update_payout_status($status,$cashgramId){
    	if ($this->checkLogin('A') == ''){
			echo "Invalid Access";exit;
		}else {
    		return 	$this->order_model->update_details(PAYOUT,array('payout_status'=> $status),array('cashgramid'=> $cashgramId ));
    	}
    }

    public function update_payout_status_ajax(){
   		if ($this->checkLogin('A') == ''){
			echo "Invalid Access";exit;
		}else {
    		return $this->order_model->update_details(PAYOUT,array('payout_status'=> $this->input->post('status')),array('cashgramid'=> $this->input->post('cashgramid')));
    	}	
    }

    public function deactivate_cashgram(){
   		if ($this->checkLogin('A') == ''){
			echo "Invalid Access";exit;
		}else {
	    	$respo = $this->deactive_cashgram($this->input->post('cashgramid'));
	    	if($respo->subCode == 200){
				$this->update_payout_status('Expired',$this->input->post('cashgramid'));
				echo json_encode($respo);
			}else{
				echo json_encode($respo);
			}
    	}	
    }

    public function deactive_cashgram($cashgramId){
    	// print_r($cashgramId);exit;
    	if ($this->checkLogin('A') == ''){
			echo "Invalid Access";exit;
		}else {
	    	$response = $this->generate_authorize_token();
	    	if($response->subCode == 200){
	    		$is_validate = $this->validate_token($response->data);
	    		if($is_validate->subCode == 200){
	    			$url = CASHGRMA_URL.'/payout/v1/deactivateCashgram';
					$data = array(
						"cashgramId" => $cashgramId
					);
					$postdata = json_encode($data);
					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
					curl_setopt($ch, CURLOPT_HTTPHEADER, 
						array(
							'Content-Type: application/json',
							'Authorization:Bearer '.$response->data->token
						)
					);
					$result = curl_exec($ch);
					return json_decode($result);

	    		}else{
	    			return  $is_validate;
	    		}
	    	}else{
	    		return  $response;
	    	}
	    }
    }


    //check status of all payout users and update
    public function CheckStatusOfPayoutAndUpdate(){
    	$payout_data = $this->order_model->get_all_payout_details();
		$response = $this->generate_authorize_token();
    	if($response->subCode == 200){
    		$is_validate = $this->validate_token($response->data);
    		$payout_array = [];
    		if($is_validate->subCode == 200){
    			foreach ($payout_data->result() as $key => $value) {
    				$url = CASHGRMA_URL.'/payout/v1/getCashgramStatus?cashgramId='.$value->cashgramId;
					$postdata = array();
					$ch = curl_init($url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
					curl_setopt($ch, CURLOPT_HTTPHEADER, 
						array(
							'Authorization : Bearer '.$response->data->token,
						)
					);
					$result = curl_exec($ch);
					$decoded = json_decode($result);
					if(array_key_exists('data', $decoded)){
						$stauts = 'Expired';
						if($decoded->data->cashgramStatus == 'VERIFIED'){
							$stauts = 'OTP Verified';
						}elseif($decoded->data->cashgramStatus == 'ACTIVE'){
							$stauts = 'Link Send';
						}elseif($decoded->data->cashgramStatus == 'EXPIRED'){
							$stauts = 'Expired';
						}elseif($decoded->data->cashgramStatus == 'REDEEMED'){
							$stauts = 'Paid';
						}else{
							$stauts = 'Expired';
						}
						$updateArray[] = array(
							'cashgramid' => $decoded->data->cashgramId,
							'payout_status' => $stauts
						);
					}
		    	}
		    	$this->db->update_batch(PAYOUT,$updateArray, 'cashgramid'); 
					// $this->order_model->payout_update();
    		}else{
    			echo $is_validate;
    		}
    	}else{
    		echo $response;
    	}
    }


    public function get_all_send_payouts(){
    	// print_r($this->input->post());exit;

    	$all_payouts = $this->order_model->Get_shipping_amount_with_payout_listing($this->input->post('order_id'));
    	$respo_array = array('status' => 200 ,'message' => 'Payout Listing', 'data' => $all_payouts->result());
    	echo json_encode($respo_array);
    	// print_r($all_payouts->result_array());
    }

    public function get_payout_cashgram_details(){
    	$cashgram = $this->order_model->get_user_cashgram_details( $this->input->post('cashgramId'));
    	echo json_encode($cashgram);
    }
    
    
        //Show office furniture orders 
    public function display_office_furniture_order(){
    	// print_r("here you are redirecting");exit;
    	if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Order List';
			$this->load->view('admin/order/office_furniture_orders',$this->data);
		}
    }

    public function get_office_funriture_listing(){
    	// print_r($this->input->post());exit;
    	if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$start = (isset($_REQUEST['start']))? intval($_REQUEST['start']) : 0;
		    $length = (isset($_REQUEST['length']) && intval($_REQUEST['length']) > 0 )? $_REQUEST['length'] : 0;
			$orderIndex = (isset($_REQUEST['order'][0]['column']))? $_REQUEST['order'][0]['column'] : 0; 
	    	$orderType = (isset($_REQUEST['order'][0]['dir']))? $_REQUEST['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_REQUEST['search']['value']))? $_REQUEST['search']['value'] : NULL;
	        
			// print_r($orderType);exit;
	        $data = $this->order_model->get_listing_office_furniture_orders(NULL,$length,$start,$orderIndex,$orderType,$searchValue);
	        $TotalCount = $this->order_model->get_listing_office_furniture_orders('count',$length,$start,$orderIndex,$orderType,$searchValue);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_REQUEST['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}
    }

    public function calculate_epc(){
        $this->load->view('site/order/epc_view');
    }
    
      /*----- view offline order listing ----*/ 
    public function offline_orders(){
    	if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Order List';
			$this->load->view('admin/order/offline_orders',$this->data);
		}
    }

    /*----- get offline order details ----*/
    public function get_offline_order_details(){
    	if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$start = (isset($_GET['start']))? intval($_GET['start']) : 0;
		    $length = (isset($_GET['length']) && intval($_GET['length']) > 0 )? $_GET['length'] : 0;
			$orderIndex = (isset($_GET['order'][0]['column']))? $_GET['order'][0]['column'] : 0; 
	    	$orderType = (isset($_GET['order'][0]['dir']))? $_GET['order'][0]['dir'] : 'DESC';
	    	$searchValue = (isset($_GET['search']['value']))? $_GET['search']['value'] : NULL;
	        
	        $data = $this->order_model->view_order_details(NULL,NULL,$length,$start,$orderIndex,$orderType,$searchValue,1);
	        $TotalCount = $this->order_model->view_order_details(NULL,'count',$length,$start,$orderIndex,$orderType,$searchValue,1);
	    	$TotalDataCount = count($data);
	    	$jsonArray1 = array("draw" => intval($_GET['draw']), "recordsTotal" => $TotalCount[0]->count, "recordsFiltered" => $TotalCount[0]->count);
	        $jsonArray1['data'] = $data;
	      	header('Content-Type: application/json');
			echo json_encode($jsonArray1);exit;
		}
    }

    /*----- Update offline orders ----*/
    public function update_offline_orders(){
    	if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$OrderDetails = $this->order_model->get_all_details(PAYMENT,array('dealCodeNumber' => $this->input->post('dealcodenumber'),'status' => 'Pending'));
			if($OrderDetails->num_rows() > 0){
	    		$date = strtotime($this->input->post('order_date'));
	    		$created_date = date('Y-m-d h:i:s', $date);
	    		$Order['created'] = $created_date;
	    		$Order['paypal_transaction_id'] = $this->input->post('transaction_id');
	    		$Order['shipping_status'] = 'Processed';
	    		$Order['status'] = 'Paid';
	    		$Order['payment_type'] =  $this->input->post('payment_mode');
	    		$this->order_model->update_details(PAYMENT,$Order,array('dealCodeNumber'=>$this->input->post('dealcodenumber')));
	    		$this->order_model->SnedOfflineOrderEmail($this->input->post('user_id'),$this->input->post('dealcodenumber'));
	    		$jsonArray1 = array('stauts' => 'success','code' => 200,'message' => 'Order updated successfully');	
    			header('Content-Type: application/json');
				echo json_encode($jsonArray1);exit;
			}else{
				$jsonArray1 = array('stauts' => 'error','code' => 400,'message' => 'No order found');	
    			header('Content-Type: application/json');
				echo json_encode($jsonArray1);exit;
			}
    	}
    }


}

/* End of file order.php */
/* Location: ./application/controllers/admin/order.php */