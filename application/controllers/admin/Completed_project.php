<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * This controller contains the functions related to Product management
 * @author Teamtweaks
 *
 */
class Completed_project extends MY_Controller {

    function __construct() {
        parent::__construct();
        if ($this->checkLogin('A') == '') {
            redirect('admin');
        }
        $this->load->helper(array('cookie', 'date', 'form'));
        $this->load->library(array('encrypt', 'form_validation'));
        $this->load->model('completed_project_model');

//         $method = $this->router->fetch_method();
// 		if($method == 'display_product_quantity_details' || $method == 'get_all_product_listing' || $method == 'update_city_qty') {
// 			if($this->checkPrivileges('product_quantity',$this->privStatus) == FALSE) {
// 				redirect('admin');
// 			}
// 		} else if ($this->checkPrivileges('product',$this->privStatus) == FALSE) {
// 	 		redirect('admin');
// 		}
    }
    
    public function completed_project_listing(){
        $this->data['heading'] = 'Projects List';
        $this->data['projectList'] = $this->completed_project_model->get_all_details(COMPLETED_PROJECT, '1 = 1');
        $this->load->view('admin/completed_project/project_list', $this->data);
    }
    
    public function add_completed_project($id = null){
        if($id){
            $this->data['heading'] = 'Edit Project';    
            $this->data['projectDetails'] = $this->completed_project_model->get_all_details(COMPLETED_PROJECT, ['id' => $id])->result()[0];
        }else{
            $this->data['heading'] = 'Add Project';
        }
        $get_published_project = $this->completed_project_model->get_all_details(COMPLETED_PROJECT, ['status' => 'Publish']);
        $count_total_projects = $this->completed_project_model->get_all_details(COMPLETED_PROJECT, '1 = 1')->num_rows();
        
        $this->form_validation->set_rules('company_name', 'Company Name', 'required|trim');
        $this->form_validation->set_rules('location', 'Location', 'required|trim');
        if($this->form_validation->run()){
            $config['overwrite'] = FALSE;
            $config['allowed_types'] = 'jpg|jpeg|gif|png';
            $config['upload_path'] = './images/completed_project';
            $this->load->library('upload', $config);
            if(!$id){
                if(empty($_FILES['image']['name'])){
                   $this->session->set_flashdata('error', 'Please select project image'); 
                }else{
                    
                    
                    if($this->upload->do_upload('image')){
                        $addData = array();
                        $project_image = $this->upload->data();
                        $addData['image'] = $project_image['file_name'];
                        $addData['company_name'] = $this->input->post('company_name');
                        $addData['location'] = $this->input->post('location');
                        $addData['position'] = $count_total_projects + 1;
                        
                        $this->completed_project_model->commonInsertUpdate(COMPLETED_PROJECT, 'insert', [], $addData, "");
                        $this->setErrorMessage('success', 'Project added successfully');
                        redirect('admin/completed_project/completed_project_listing');
                    }else{
                        $this->setErrorMessage('error', $this->upload->display_errors());
                        redirect('admin/completed_project/add_completed_project');
                    }
                }
            }else{
                $updateData = array();
                $updateData['company_name'] = $this->input->post('company_name');
                $updateData['location'] = $this->input->post('location');
                if(!empty($_FILES['image']['name'])){
                    if($this->upload->do_upload('image')){
                        $project_image = $this->upload->data();
                        $updateData['image'] = $project_image['file_name'];
                    }else{
                        $this->setErrorMessage('error', $this->upload->display_errors());
                        redirect('admin/completed_project/edit_completed_project/'.$id);
                    }
                }
                $this->completed_project_model->commonInsertUpdate(COMPLETED_PROJECT, 'update', [], $updateData, ['id' => $id]);
                $this->setErrorMessage('success', 'Project updated successfully');
                redirect('admin/completed_project/completed_project_listing');
            }
            
        }
        $this->load->view('admin/completed_project/add_project', $this->data);
    }
    
    public function change_completed_project_status($status, $id){
        $get_published_project = $this->completed_project_model->get_all_details(COMPLETED_PROJECT, ['status' => 'Publish']);
        if($status == 'Publish'){
            $this->completed_project_model->commonInsertUpdate(COMPLETED_PROJECT, 'update', [], ['status' => 'Unpublish'], ['id' => $id]);
            $this->setErrorMessage('success', 'Project status updated successfully.');
            redirect('admin/completed_project/completed_project_listing');
        }else{
            $this->completed_project_model->commonInsertUpdate(COMPLETED_PROJECT, 'update', [], ['status' => 'Publish'], ['id' => $id]);
            $this->setErrorMessage('success', 'Project status updated successfully.');
            redirect('admin/completed_project/completed_project_listing');
        }
    }
    
    public function update_project_position(){
        $id = $this->input->get('id');
        $old_position = $this->input->get('old_position');
        $new_position = $this->input->get('new_position');
        $count_total_projects = $this->completed_project_model->get_all_details(COMPLETED_PROJECT, '1 = 1')->num_rows();
        if($new_position > $count_total_projects){
            $this->setErrorMessage('error', 'Position can not be greater then the total number of projects.');
            echo json_encode([
                'status' => false,
                'data' => "Position can not be greater then the total number of projects."
            ]);
            return false;
        }
        $get_updated_row_position = $this->completed_project_model->get_all_details(COMPLETED_PROJECT, ['id' => $id])->result()[0];
        $get_existing_row_id = $this->completed_project_model->get_all_details(COMPLETED_PROJECT, ['position' => $new_position])->result()[0];
        $this->completed_project_model->commonInsertUpdate(COMPLETED_PROJECT, 'update', [], ['position' => $new_position], ['id' => $id]);
        $this->completed_project_model->commonInsertUpdate(COMPLETED_PROJECT, 'update', [], ['position' => $old_position], ['id' => $get_existing_row_id->id]);
        $this->setErrorMessage('success', 'Position updated successfully.');
        echo json_encode([
            'status' => true,
            'data' => "Position updated successfully."
        ]);
    }
}