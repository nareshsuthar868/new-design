<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This controller contains the functions related to Product management
 * @author Teamtweaks
 *
 */

class Offers extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model('offer_model');
// 		if ($this->checkPrivileges('product',$this->privStatus) == FALSE){
// 			redirect('admin');
// 		}
	}
	
	public function index(){	
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			redirect('admin/offers/offer_listing');
		}
	}
	
	
	public function offer_listing(){
	    if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Filter Listing';
			$sort  = ' order by position desc';
			$this->data['offers'] = $this->offer_model->get_offerDetails();
			$this->load->view('admin/offers/offer_listing',$this->data);
		}
	}
	
	public function add_offer_form(){
	    if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Add Offer';
			$this->load->view('admin/offers/add_offer',$this->data);
		}
	}
	
	public function edit_offer_form(){
	    if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Edit Offer';
		    $offer_id = $this->uri->segment(4,0);
		    $this->data['offerDetails'] = $this->offer_model->get_all_details(SITE_OFFERS,array('id' => $offer_id));
			$this->load->view('admin/offers/edit_offer',$this->data);
		}
	}
	
	public function insertUpdateOffers(){
	   // echo "<pre>";
	   // print_r($_POST);exit;
	    if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
		    $offer_id = $this->input->post('offer_id');
		    $excludeArr = array("offer_id");
		    $condition = array('id' => $offer_id);
	        $getDescription = array_filter($this->input->post('description'));
		    $dataArr = array('description' => serialize($getDescription));
 
		    $this->offer_model->update_details(SITE_OFFERS,array('is_publish_wishlist' => '0'),array('is_publish_wishlist' => '1'));
		    if($offer_id == ''){
		        $get_all_count = $this->offer_model->get_all_details(SITE_OFFERS,array());
		        $new_posititon = $get_all_count->num_rows();
		        $dataArr = array('description' => serialize($getDescription),'position' => $new_posititon);
		       	$this->offer_model->commonInsertUpdate(SITE_OFFERS,'insert',$excludeArr,$dataArr,$condition);
				$this->setErrorMessage('success','Coupon Code added successfully');
		    }else{
		        $this->offer_model->commonInsertUpdate(SITE_OFFERS,'update',$excludeArr,$dataArr,$condition);
				$this->setErrorMessage('success','Coupon Code updated successfully');
		    }
		    redirect('admin/offers/offer_listing');
		}
	}
	
    public function change_offer_status(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
		    $get_all_count = $this->offer_model->get_all_details(SITE_OFFERS,array('status' => 'Publish'));
		    if($get_all_count->num_rows() >= 4){
		        $this->setErrorMessage('error','Only 4 offers can be publish at one time');
    			redirect('admin/offers/offer_listing');
		    }else{
    			$mode = $this->uri->segment(4,0);
    			$product_id = $this->uri->segment(5,0);
    			$status = ($mode == '0')?'UnPublish':'Publish';
    			$newdata = array('status' => $status);
    			$condition = array('id' => $product_id);
    			$this->offer_model->update_details(SITE_OFFERS,$newdata,$condition);
    			$this->setErrorMessage('success','Offer Status Changed Successfully');
    			redirect('admin/offers/offer_listing');
		    }
		}
	}
	
	public function update_offer_position(){
	    if ($this->checkLogin('A') == ''){
			echo json_encode(array('msg' => 'Invalid Access','status' => 400));
		}else {
		    $new_position = $this->input->post('position');
		    $offer_id = $this->input->post('offer_id');
		    $checkDuplicatePosition = $this->offer_model->get_all_details(SITE_OFFERS,array('position' => $new_position,'id !=' => $offer_id));
		    if($checkDuplicatePosition->num_rows() > 0){
		        echo json_encode(array('msg' => 'Position already exists','status' => 400));exit;
		    }else{
		        $this->offer_model->update_details(SITE_OFFERS,array('position' => $new_position),array('id' => $offer_id)); 
		        echo json_encode(array('msg' => 'Position updated successfulyy','status' => 200));exit;
		    }
		}
	}
	
	public function delete_offer(){
	    if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$offer_id = $this->uri->segment(4,0);
			$condition = array('id' => $offer_id);
			$this->offer_model->commonDelete(SITE_OFFERS,$condition);
			$this->setErrorMessage('success','Offer deleted successfully');
			redirect('admin/offers/offer_listing');
		}
	}


}