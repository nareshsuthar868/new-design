<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This controller contains the functions related to Order management
 * @author Teamtweaks
 *
 */

class Uslead extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model('order_model');
// 		print_r($this->session->userdata('fc_session_admin_privileges'));exit;
		if ($this->checkPrivileges('order',$this->privStatus) == FALSE){
			redirect('admin');
		}
	}


	public function display_lead_request_orders(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Lead Details';
			$this->data['leadDetails'] = $this->order_model->view_leads_request();
			// print_r($this->data['leadDetails']);exit;
			$this->load->view('admin/order/display_us_leadrequest_orders',$this->data);
		}
	}

	public function delete_lead_request(){
		$id = $this->input->post('id');
		$response = $this->order_model->commonDelete(LEAD_REQUEST,array('id' => $id));
		echo json_encode($response);	
	}
}