<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This controller contains the functions related to CIBIL Documents 
 * @author Teamtweaks
 *
 */

class Documents extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model('admin_model');
		// if ($this->checkPrivileges('product',$this->privStatus) == FALSE){
		// 	redirect('admin');
		// }
    }

    // list of cibil slots
    public function cibil_score_slots(){
        if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Slot List';
			$condition = array();
			$this->data['cibil_documents'] = $this->admin_model->get_all_details(CIBIL_DOCUMENTS,$condition);
			$Array = $this->admin_model->get_all_details(CIBIL_REQUIRE_DOCS,array());
			$this->data['cibil_require_documents'] = $Array->result_array();
			$this->load->view('admin/cibil_documents/slot_listing',$this->data);
		}
    }

    //add new cibil score slots
    public function add_newslots(){
        if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Add New Slot';
			$this->data['cibil_documents'] = array() ; 
			$this->data['cibil_require_documents'] = $this->admin_model->get_all_details(CIBIL_REQUIRE_DOCS,array());
			$this->load->view('admin/cibil_documents/addslot',$this->data);
		}
    }

    //show edit from with slots details
    public function edit_slot(){
    	if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Update Slot';
			$condition = array('id' => $this->uri->segment(4,0));
			$row = $this->admin_model->get_all_details(CIBIL_DOCUMENTS,$condition);
			$this->data['cibil_documents'] = $row->row();
			$this->data['cibil_require_documents'] = $this->admin_model->get_all_details(CIBIL_REQUIRE_DOCS,array());
			$this->load->view('admin/cibil_documents/addslot',$this->data);
		}
    }
    
    //update & insert new cibil slot
    public function insertEditslots(){
    	if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$Data['score_from'] = $this->input->post('start_from');
			$Data['score_to'] = $this->input->post('to_from');
			$Data['documents'] = implode(',', $this->input->post('documents'));
			$slot_id = $this->input->post('slot_id');
			if($slot_id != ''){
				$this->admin_model->update_details(CIBIL_DOCUMENTS,$Data,array('id' => $slot_id));
				$this->setErrorMessage('success','Slot updated successfully');
				redirect('admin/documents/cibil_score_slots');
			}else{
				$this->admin_model->simple_insert(CIBIL_DOCUMENTS,$Data);
				$this->setErrorMessage('success','Slot inserted successfully');
				redirect('admin/documents/cibil_score_slots');
			}
		}
    }

    //Show document listing 
    public function manage_documents(){
    	if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Cibil Documents';
			$this->data['cibil_documents'] = $this->admin_model->get_all_details(CIBIL_REQUIRE_DOCS,array());
			$this->load->view('admin/cibil_documents/cibil_score_documents',$this->data);
		}
    }

    //add new document
    public function add_new_doc(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Add Document';
			$this->data['cibil_documents'] = array();
			$this->load->view('admin/cibil_documents/addupdatedocuments',$this->data);
		}
    }

    //show edit form for docment
    public function edit_doc(){
    	if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Edit Document';
			$condition = array('id' => $this->uri->segment(4,0));
			$row = $this->admin_model->get_all_details(CIBIL_REQUIRE_DOCS,$condition);
			$this->data['cibil_documents'] = $row->row();
			$this->load->view('admin/cibil_documents/addupdatedocuments',$this->data);
		}
    }

    //add & update document
    public function insert_update_documents(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$doc_name =  $this->input->post('document_name');
			$doc_length =  $this->input->post('max_files');
			$doc_vid = strtolower(str_replace(' ','_', $doc_name));
			$doc_id = $this->input->post('doc_id');
			$DataArray = array('doc_name' => $doc_name,'doc_id' => $doc_vid,'max_files' => $doc_length);
			if($doc_id != ''){
				$this->admin_model->update_details(CIBIL_REQUIRE_DOCS,$DataArray,array('id' => $doc_id));
				$this->setErrorMessage('success','Document updated successfully');
				redirect('admin/documents/manage_documents');
			}else{
				$this->admin_model->simple_insert(CIBIL_REQUIRE_DOCS,$DataArray);
				$this->setErrorMessage('success','Document inserted successfully');
				redirect('admin/documents/manage_documents');
			}
		}
    }



    public function show_user_documents(){
    	if ($this->checkLogin('A') == ''){
			$returnArray = array('data' => array() , 'msg' => 'Invalid Access', 'status' => 401);
		 	header('Content-Type: application/json');
			echo json_encode($returnArray);exit;
		}else {
			$userDocs = $this->admin_model->get_all_details(USER_UPLOADED_DOCS,array('order_id' =>$this->input->post('order_id')));
			$returnArray = array('data' => $userDocs->result() , 'msg' => 'User Documents', 'status' => 200);
		 	header('Content-Type: application/json');
			echo json_encode($returnArray);exit;
		}
    }

    public function show_documents(){
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'Listing of user documents';
			$this->data['user_documents'] = $this->admin_model->get_all_user_uploaded_docs();
			$this->load->view('admin/cibil_documents/user_documents',$this->data);	
		}
    }

    public function explain_details(){
    	if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'User Document Details';
			$condition = array('user_id' => $this->uri->segment(4,0));
			$this->data['doc_more_details'] = $this->admin_model->get_all_user_uploaded_docs($condition);
			if($this->data['doc_more_details']->num_rows() > 0){
	    		$this->data['heading'] = '( '.$this->data['doc_more_details']->row()->full_name.' )  Document Details';
			}
			// echo "<pre>";
			// print_r($this->data['doc_more_details']->result_array());exit;
			$this->load->view('admin/cibil_documents/show_all_docs',$this->data);
		}
    }
}