<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * This controller contains the functions related to Order management
 * @author Teamtweaks
 *
 */

class Userwallet extends MY_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('cookie','date','form'));
		$this->load->library(array('encrypt','form_validation'));
		$this->load->model('order_model');
		if ($this->checkPrivileges('order',$this->privStatus) == FALSE){
			redirect('admin');
		}
	}


	public function wallet()
	{
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {

			$this->data['heading'] = 'CF Coins';
			$this->data['leadDetails'] = $this->order_model->getwallet();
			$this->load->view('admin/order/wallet',$this->data);
		}


	}

	public function transaction($id)
	{
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {

			$this->data['heading'] = 'CF Coins Transaction';
			$this->data['leadDetails'] = $this->order_model->getwallettransaction($id);

			$this->load->view('admin/order/wallettransaction',$this->data);
			
		}		
	}

	public function addwallettransaction($id)
	{
		if ($this->checkLogin('A') == ''){
			redirect('admin');
		}else {
			$this->data['heading'] = 'CF Coins Transaction';
			$this->data['id'] = $id;
			$this->load->view('admin/order/addwallettransaction',$this->data);
		}
	}

	public function addeditwallettransaction($id)
	{
		$check_wallet_exsits = $this->order_model->get_all_details(MY_WALLET,array('user_id' => $id));
		$insertData['user_id'] = $id;
		$insertData['mihpayid'] = 'CF-'.rand(11111,99999);
		$insertData['payment_mode'] =  $this->input->post('payment_mode');
		$insertData['payment_status'] = 'success';
		$insertData['txnid'] = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
		$insertData['amount'] = $this->input->post('coin_balance');
		$insertData['order_id'] = mt_rand();
		$insertData['mode'] = 'credit';
		$Insert_response = $this->order_model->simple_insert(WALLET_TRAN,$insertData);
		if($check_wallet_exsits->num_rows() > 0){
			$wallet = $check_wallet_exsits->row();
			$Data['topup_amount'] = $wallet->topup_amount + $this->input->post('coin_balance');
			$Data['updated_at'] = date("Y-m-d H:i:s");
			$this->order_model->update_details(MY_WALLET,$Data,array('user_id' => $id));
		}else{
			$Data['user_id'] = $id;
			$Data['topup_amount'] = $this->input->post('coin_balance');
			$Data['updated_at'] = date("Y-m-d H:i:s");
			$Insert_response = $this->order_model->simple_insert(MY_WALLET,$Data);
		}
		$this->sendWalletEmail($insertData,'success');
		redirect('admin/userwallet/wallet');
	}


	public function sendWalletEmail($data,$mode){
		$useerDetails = $this->order_model->get_all_details(USERS,array('id' => $data['user_id']));
		$newsid = '27';
		$template_values=$this->order_model->get_newsletter_template($newsid);
		$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo']);
		extract($adminnewstemplateArr);

	    $searchArray = array("{{AMOUNT}}", "{{ORDER_ID}}","{{TXT_ID}}");
		$replaceArray = array($data['amount'],$data['order_id'],$data['txnid']);
		$new_message .=  str_replace($searchArray, $replaceArray ,$template_values['news_descrip'] );
		$message .= '<!DOCTYPE HTML>
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
			<meta name="viewport" content="width=device-width"/><body>';
		$message = $new_message;

		$message .= '</body>
			</html>';

        $sender_email = $this->config->item('site_contact_mail');
        $sender_name = $this->config->item('email_title');
     	$email_values = array('mail_type' => 'html',
                'from_mail_id' => $sender_email,
                'mail_name' => $sender_name,
                'to_mail_id' => $useerDetails->row()->email,
                'cc_mail_id' => $this->config->item('site_contact_mail'),
                'subject_message' => 'Wallet Topup Success',
                'body_messages' => $message
            	);
        $email_send_to_common = $this->order_model->common_email_send($email_values);
	}
}


/* End of file UserWallet.php */
/* Location: ./application/controllers/admin/Userwallet.php  */