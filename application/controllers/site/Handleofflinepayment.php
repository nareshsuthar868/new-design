<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**

 * 

 * User related functions

 * @author Teamtweaks

 *

 */
class Handleofflinepayment extends MY_Controller {

    function __construct() {

        parent::__construct();

		$this->load->helper(array('cookie','date','form','email','pdf_helper'));

        $this->load->library(array('encrypt', 'form_validation','curl'));

       	$this->load->model('order_model');
		$this->load->model('product_model');
        $this->load->model('user_model');
      	if($_SESSION['sMainCategories'] == ''){
			$sortArr1 = array('field'=>'cat_position','type'=>'asc');
			$sortArr = array($sortArr1);
			$_SESSION['sMainCategories'] = $this->order_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		}
		$this->data['mainCategories'] = $_SESSION['sMainCategories'];
                
		if($_SESSION['sColorLists'] == ''){
			$_SESSION['sColorLists'] = $this->order_model->get_all_details(LIST_VALUES,array('list_id'=>'1'));
		}
		$this->data['mainColorLists'] = $_SESSION['sColorLists'];

    }

    public function index(){
    	if($this->uri->segment(2) == 'success'){
    		$transId = $_POST['txnid'];
	    	$payerMail = $_POST['email'];
	    	$card_token = $_POST['cardToken'];
	    	$mendate_id = $_POST['mihpayid'];

	    	$randomId = $this->uri->segment(4);
	    	$userid =  $this->uri->segment(3);
	        $conditionCheck = array('user_id' => $userid, 'dealCodeNumber' => $randomId, 'status' => 'Paid');
	        $statusCheck = $this->order_model->get_all_details(PAYMENT, $conditionCheck);
	        if ($statusCheck->num_rows() == 0) {
	            $CoupRes = $this->order_model->get_all_details(PAYMENT, array('user_id' => $userid));
	            $couponID = $CoupRes->row()->coupon_id;
	            $couponAmont = $CoupRes->row()->discountAmount;
	            $couponType = $CoupRes->row()->coupontype;
	            // Update Coupon
	            if ($couponID != 0) {
	                    $SelCoup = $this->order_model->get_all_details(COUPONCARDS, array('id' => $couponID));
	                    if(isset($SelCoup->row()->code)){
	                        $CountValue = $SelCoup->row()->purchase_count + 1;
	                        $condition = array('id' => $couponID);
	                        $dataArr = array('purchase_count' => $CountValue);
	                        $this->order_model->update_details(COUPONCARDS, $dataArr, $condition);
	                    }else{
	                        $conditionCheck = array('user_id' => $userid, 'dealCodeNumber' => $randomId);
	                        $GetDiscount = $this->order_model->get_all_details(PAYMENT, $conditionCheck);
	                        $SelReferral = $this->order_model->get_all_details(REFERRAL_CODE, array('id' => $couponID));
	                        $CountValue = $SelReferral->row()->count - 1;
	                        $array_old = array('user_id' => $userid, 'amount' =>  $GetDiscount->row()->discountAmount);
	                        if(!empty(unserialize($SelReferral->row()->used_by))){
	                            $new_array123 = unserialize($SelReferral->row()->used_by);
	                            $arraaaaa   =  array_push($new_array123, $array_old);
	                        }else{
	                            $new_array123[0] = $array_old;
	                        }
	                        $condition = array('id' => $couponID);
	                        $dataArr = array('count' => $CountValue,'used_by' => serialize($new_array123));
	                        $this->order_model->update_details(REFERRAL_CODE, $dataArr, $condition);    
	                    }
	            }
	            //Update Payment Table
	            $condition1 = array('user_id' => $userid, 'dealCodeNumber' => $randomId);
	            if($_POST['payment_source'] == 'sist'){
	            	$is_si = '1';
	            	$recurring_type = 'si_on_credit_card';
	            	$SelQty = $this->order_model->get_all_details(PAYMENT, array('user_id' => $userid, 'dealCodeNumber' => $randomId));
	            	$orderTotal = $SelQty->row()->total - $SelQty->row()->discount_on_si;
	            	$this->order_model->update_details(PAYMENT,array('total' => $orderTotal),array('dealCodeNumber' => $randomId));
	            }else{
	            	$is_si = '0';
	            	$recurring_type = 'not_opted';
	            }
	            if ($payerMail != '') {
	                $dataArr1 = array('status' => 'Paid', 'shipping_status' => 'Processed', 'paypal_transaction_id' => $transId, 'payer_email' => $payerMail, 'payment_type' => 'Paypal','card_token' => $card_token ,'mandate_id' => $mendate_id,'is_recurring' => $is_si,'recurring_type' => $recurring_type);
	            } else {
	                $dataArr1 = array('status' => 'Paid', 'shipping_status' => 'Processed', 'paypal_transaction_id' => $transId, 'payment_type' => 'Credit Cart','card_token' => $card_token ,'mandate_id' => $mendate_id,'is_recurring' => $is_si,'recurring_type' => $recurring_type);
	            }
	            $this->order_model->update_details(PAYMENT, $dataArr1, $condition1);
	            //Update Quantity
	            $SelQty = $this->order_model->get_all_details(PAYMENT, array('user_id' => $userid, 'dealCodeNumber' => $randomId));
	            foreach ($SelQty->result() as $updPrdRow) {
	                $SelPrd = $this->order_model->get_all_details(PRODUCT, array('id' => $updPrdRow->product_id));
	                $PrdCount = $SelPrd->row()->purchasedCount + $updPrdRow->quantity;

	                /* Code added by Manish - 4/9/2019 - Start */
	                $cityQuantity = $this->order_model->get_all_details('fc_city_product_quantity', array('city_id' => $_SESSION['prcity'], 'product_id' => $updPrdRow->product_id));
	                $quantity = $cityQuantity->row()->quantity - $updPrdRow->quantity;
	                $quantity = ($quantity > 0) ? $quantity : '0';

	                $this->db->where('city_id = '.$_SESSION['prcity'].' and product_id = '.$updPrdRow->product_id);
	                $this->db->update('fc_city_product_quantity',array('quantity' => $quantity));
	                /* Code added by Manish - 4/9/2019 - End */

	                //$productCount = $SelPrd->row()->quantity - $updPrdRow->quantity;
	                $condition2 = array('id' => $updPrdRow->product_id);
	                $dataArr2 = array('purchasedCount' => $PrdCount); //'quantity' => $productCount,
	                $this->order_model->update_details(PRODUCT, $dataArr2, $condition2);
	            }
	            //Send Mail to User
	            $this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.image,pd.id as PrdID,pAr.attr_name as attr_type,sp.attr_name');
	            $this->db->from(PAYMENT . ' as p');
	            $this->db->join(USERS . ' as u', 'p.user_id = u.id');
	            $this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
	            $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
	            $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
	            $this->db->where('p.user_id = "' . $userid . '" and p.dealCodeNumber="' . $randomId . '"');
	            $PrdList = $this->db->get();

	            $this->db->select('p.sell_id,p.couponCode,u.email');
	            $this->db->from(PAYMENT . ' as p');
	            $this->db->join(USERS . ' as u', 'p.sell_id = u.id');
	            $this->db->where('p.user_id = "' . $userid . '" and p.dealCodeNumber="' . $randomId . '"');
	            $this->db->group_by("p.sell_id");
	            $SellList = $this->db->get();
				$_SESSION['offline_user_id'] = $userid;
				$_SESSION['offline_dealcode'] = $randomId;
				if($_POST['payment_source'] == 'sist'){
			  		$this->SendVoucherEmail($PrdList);
			  	}
			  	
			  	$this->set_commision($PrdList->row());
	            // $this->order_model->SendMailUSers($PrdList, $SellList);
	        	// $this->createAutoCaseInZohoCRM($userid,$randomId);
        		$this->data['Confirmation'] = 'Success';
        		$this->updateZohoCase($randomId);
				$this->load->view('site/order/offline_order.php',$this->data);
	        }
	   //      $this->data['Confirmation'] = 'Success';				
				// $this->load->view('site/order/offline_order.php',$this->data);
    	}else{
    		$this->data['Confirmation'] = 'Failure';
			//Code added By Manish Soni - 2/9/2019 - Start
			$payment_data =  $this->order_model->get_faild_order($this->uri->segment(3));

			$this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.image,pd.id as PrdID,pAr.attr_name as attr_type,sp.attr_name');
			$this->db->from(PAYMENT . ' as p');
			$this->db->join(USERS . ' as u', 'p.user_id = u.id');
			$this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
			$this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
			$this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
			$this->db->where('p.user_id = "' . $this->uri->segment(3) . '" and p.dealCodeNumber="' . $payment_data->dealCodeNumber . '"');
			$PrdList = $this->db->get();

			$this->db->select('p.sell_id,p.couponCode,u.email');
			$this->db->from(PAYMENT . ' as p');
			$this->db->join(USERS . ' as u', 'p.sell_id = u.id');
			$this->db->where('p.user_id = "' . $this->uri->segment(3) . '" and p.dealCodeNumber="' . $payment_data->dealCodeNumber . '"');
			$this->db->group_by("p.sell_id");
			$SellList = $this->db->get();
			$this->order_model->new_failed_order_email($PrdList, $SellList);
			$this->load->view('site/order/offline_order.php',$this->data);
    	}
    }
    
    public function set_commision($paymentsData){
        $this->db->select('*');
        $this->db->from(BD_LEADS);
        $this->db->or_where('email',$paymentsData->email);
        $this->db->or_where('phone_no',$paymentsData->phone_no);
        $this->db->where('lead_status','pending');
        // $this->db->order_by('created','asc');
        $lead_matching = $this->db->get();
        if($lead_matching->num_rows() > 0){
            $user_details = $this->order_model->get_all_details(USERS,array('id' => $paymentsData->user_id));
            // if(strtotime($user_details->row()->created) > strtotime($lead_matching->row()->created)){
                $commission_details = $this->order_model->get_all_details(COMISSIONS,array('lower(tenure)' => strtolower($paymentsData->attr_name)));
                if($commission_details->num_rows() > 0){

                    $total_discount = $paymentsData->discountAmount + $paymentsData->discount_on_si;
                    $my_earning = ((($paymentsData->total - $paymentsData->shippingcost ) + $total_discount )  * $commission_details->row()->comission ) / 100;
                    $this->order_model->update_details(BD_LEADS, array('order_id' => $paymentsData->dealCodeNumber,'lead_status' => 'converted'), array('id' => $lead_matching->row()->id));
                    $insert_data['lead_id'] = $lead_matching->row()->id;
                    $insert_data['commission_percentage'] = $commission_details->row()->comission;
                    $insert_data['commission_rate'] = $my_earning;
                    $insert_data['status'] = 'pending';
                    $this->send_notification($lead_matching->row()->bd_user_id,$user_details->row(),$paymentsData->dealCodeNumber,$paymentsData->created);
                    $this->order_model->simple_insert(BD_LEAD_COMMISSION, $insert_data);
                }
            // }
        }
    }
    
    public function updateZohoCase($dealCode){
    	$offline_orderDetails = $this->order_model->get_all_details(OFFLINE_ORDERS,array('dealCodeNumber' => $dealCode));
    	if($offline_orderDetails->num_rows() > 0){
    		if($offline_orderDetails->row()->zoho_case_id != '' || $offline_orderDetails->row()->zoho_case_id != NULL){
    			$url = "https://www.zohoapis.com/crm/v2/Cases/".$offline_orderDetails->row()->zoho_case_id;
    			$acceess_token = $this->get_zohaccess_token();
    			$headers = [
           		    'Content-Type:application/json',
                    'Authorization:'.$acceess_token
                ];
			   $CreateCaseJsonData =   '{
		        "data" : [
				       	{
			       		"Status":"Closed",
			       		"Sub_Status":"Resolved"
				        }
				    ]
				}';
	    	  	$curl = curl_init();
			  	curl_setopt($curl, CURLOPT_URL, $url);
		  		// Set options necessary for request.
		  		curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
		  		curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PUT');
		  		curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	  			curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
			  	curl_setopt($curl, CURLOPT_POSTFIELDS, $CreateCaseJsonData);
			  	// Send request
			  	$response = curl_exec($curl);
    		} 
       	}
    }
    
    public function SendVoucherEmail($PrdList){
       	$Secret_Data = $this->order_model->get_all_details('fc_voucher_settings',array());
       	$shipAddRess = $this->user_model->get_all_details(SHIPPING_ADDRESS,array( 'id' => $PrdList->row()->shippingid ));
        $Reward  = base64_encode ($Secret_Data->row()->reward);
        $post_url = $Secret_Data->row()->post_url;
        $return_url = $Secret_Data->row()->return_url;
        $partner_code = $Secret_Data->row()->partner_code;
        $user_name = str_replace(' ', '', $shipAddRess->row()->full_name);
        $timeStamp = base64_encode(time());
        if($Secret_Data->row()->status){
            $url = file_get_contents('http://tinyurl.com/api-create.php?url='.'http://kbba.klippd.in/?clientID='.$Secret_Data->row()->client_id.'&key='.$Secret_Data->row()->key_id.'&userID='.$shipAddRess->row()->user_id.'&rewards='.$Reward.'&username='.$user_name.'&EmailAddress='.$PrdList->row()->email.'&postURL='.$post_url.'&returnURL='.$return_url.'&PartnerCode='.$partner_code.'&timeStamp='.$timeStamp.'');

        	$voucher ='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                    <html xmlns="http://www.w3.org/1999/xhtml">
                    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                    <title>Welcome Cityfurnish</title>
                      <style type="text/css">
                      @media screen and (max-width: 580px) {
                        .tab-container{max-width: 100%;}
                        .txt-pad-15 {padding: 0 15px 51px 15px !important;}
                        .foo-txt {padding: 0px 15px 18px 15px !important;}
                        .foo-add {padding: 20px !important;}
                        .tab-padd-zero{padding:0px !important;}
                        .tab-padd-right{padding-right:25px !important;}
                        .pad-20{padding:25px 20px 20px !important;}
                        .social-padd-left{padding:15px 20px 0px 0px; !important;}
                        .offerimg{width:100% !important;}
                        .mobilefont{font-size:16px !important;line-height:18px !important;}
                      }
                     </style>
                    </head>
                    <body style="margin:0px;">
                    <table class="tab-container" name="main" border="0" cellpadding="0" cellspacing="0" style="background-color: #fff;margin: 0 auto;font-family:Arial, Helvetica, sans-serif;font-size:14px;border-collapse:collapse;width:600px;
                    border-width:1px; border-style:solid; border-color:#e7e7e7;table-layout: fixed;display: block;border-right-width: 0px;">
                        <tr>
                         <td style="width:100%">
                          <table style="width:100%;table-layout:fixed" cellpadding="0" cellspacing="0px">
                          <tr>
                          <td style="text-align: left;padding:15px 0px 15px 20px;padding-left:20px;">
                            <a href="https://cityfurnish.com/"><img src="https://cityfurnish.com/images/logo-2.png" alt="logo" style="width:150px;" /></a>
                          </td>
                          <td style="text-align: right;padding:15px 20px 15px 0px;text-align:right;width:50%;border-right: 1px solid #e7e7e7" class="social-padd-left">
                            <a href="https://www.facebook.com/cityFurnishRental" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/facebook.png" alt="facebook" width="18px" /></a>
                            <a href="https://twitter.com/CityFurnish" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/twitter.png" alt="twitter" width="18px"/></a>
                            <a href="https://plus.google.com/+cityfurnish" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/google-plus.png" alt="google" height="18px"/></a>
                            <a href="https://in.pinterest.com/cityfurnish/" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/pintrest.png" alt="pintrest" width="18px" /></a>
                            <a href="https://www.linkedin.com/company/cityfurnish?trk=biz-companies-cym" style="display:inline-block;margin:5px 0px 0px 15px;"><img src="https://cityfurnish.com/images/linkedin.png" alt="linkedin" width="18px" /></a>
                            </td>
                          </tr>
                          </table>
                         </td>
                        </tr>
                        <tr style="width:100%;">
                        <td style="width:100%;border-right: 1px solid #e7e7e7">
                          <img src="https://cityfurnish.com/images/voucher-banner.jpg" style="display: block;width: 100%;">
                        </td>
                        </tr>
                        <tr style="width:100%;">
                          <td style="padding-top: 50px;font-size: 40px;color: #002e40;text-align: center;border-right: 1px solid #e7e7e7">
                          <h1 style="color: #002e40;font-size: 40px;line-height:36px;font-weight:bold;margin:0px;">Congratulations!</h1>
                                 <span style="font-size: 18px;">You Got Gift Vouchers Worth</span>
                         </td>
                        </tr>
                        <tr style="width:100%;">
                          <td style="text-align: center;padding-top: 5px;border-right: 1px solid #e7e7e7">
                            <span style="display: inline-block;vertical-align: middle;margin-right: 10px;">
                              <img src="https://cityfurnish.com/images/rupee-icn.png">
                            </span>
                            <span style="display: inline-block;vertical-align: middle;color: #f9aa2b;font-size: 45px;"><strong>'.$Secret_Data->row()->reward.'</strong></span></td>
                        </tr>
                        <tr style="width:100%;">
                          <td style="text-align: center;padding-top: 15px;padding-bottom: 25px;font-size: 24px;border-right: 1px solid #e7e7e7">
                              <span style="display: inline-block;vertical-align: middle;font-size: 18px;">
                                From Cityfurnish for your #'.$PrdList->row()->dealCodeNumber.'
                              </span>
                          </td>
                        </tr>
                        <tr style="width:100%;">
                          <td style="text-align: center;font-size: 24px;color: #141414 !important;border-right: 1px solid #e7e7e7">
                              <a href="'.$url.'" target="_blank" style="background: #a5c8c2;width: 198px;text-decoration: none !important;font-size: 13px;display: inline-block;padding-top: 10px;padding-bottom: 10px;color: #141414 !important;">REDEEM</a>
                          </td>
                        </tr>
                        <tr style="width:100%;">
                            <td style="padding: 20px;">
                              <h5>Terms and Conditions:</h5>
                              <span style="font-size: 10px;">1.You can opt for multiple voucher up-to-comulative total amount specified above.</span><br>
                              <span style="font-size: 10px;">2.Voucher link will be active for 60 days from send date .You can redeem desired vouchers by visting link multiple times using link within this period.</span><br>
                              <span style="font-size: 10px;">3.Once your have selected vouchers for redemption, you will receive selected voucher on email with details instruction for usage of each voucher. </span><br>
                              <span style="font-size: 10px;">4.Please contact us on  080-66084700 or hello@cityfurnish.com in case your need any assistance.</span>
                            </td>
                        </tr>
                        <tr  style="width:100%;">
                            <td style="padding:30px 50px 26px;text-align:center;line-height:24px;width:100%;border-right: 1px solid #e7e7e7">
                                <p style="margin:0px;line-height:22px;font-family:Arial, Helvetica, sans-serif;color:#b2b2b2">Sent by <a href="https://cityfurnish.com/" style="color:#38373d;text-decoration:none;">Cityfurnish</a>, 6B Tower 3, Bellevue Tower, Central Park 2, Sohna Road,
                    Sector 48, Gurgaon, Haryana - 122018<br /><a href="mailto:hello@cityfurnish.com" style="color:#38373d; margin-top:8px;display:inline-block;text-decoration:none;">hello@cityfurnish.com</a></p>
                            </td>
                        </tr>
                    </table>
                    </body>
                    </html>';

            $email_values = array('mail_type'=>'html',
                         'from_mail_id'=>'hello@cityfurnish.com',
                         'mail_name'=>'Cityfurnish',
                         'to_mail_id'=> $PrdList->row()->email,
                         'subject_message'=>'Cityfurnish - Gift Vouchers',
                         'body_messages'=>$voucher
            );
            $email_send_to_common = $this->product_model->common_email_send($email_values);
        }
    }


    public function adminCreateCaseAndInvoices(){
    	// print_r($this->input->post());exit;
    	$user_id = $this->input->post('user_id');
    	$deal_code = $this->input->post('dealcodenumber');
		$this->createAutoCaseInZohoCRM($user_id,$deal_code);  
		$this->CreateAllZohoInvoices($user_id,$deal_code);   
		$response = array('status' => 'success','code' => 200);
	    header('Content-Type: application/json');
	    echo json_encode($response); 	
    }



	public function createAutoCaseInZohoCRM($userID,$dealCode)
    {    
        $purchaseList = $this->user_model->get_purchase_list($userID, $dealCode);
        $invoice = $this->get_invoice($purchaseList);      
		tcpdf();

		$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		$obj_pdf->SetCreator(PDF_CREATOR);
		$title = "PDF Report";
		$obj_pdf->SetTitle($title);
		$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $title, PDF_HEADER_STRING);
		$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
		$obj_pdf->SetDefaultMonospacedFont('helvetica');
		$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
		$obj_pdf->SetFont('helvetica', '', 9);
		$obj_pdf->setFontSubsetting(false);
		$obj_pdf->setPrintHeader(false);
		$obj_pdf->AddPage();
		ob_start();
		// we can have any view part here like HTML, PHP etc
		$content = $invoice;
		ob_end_clean();
		$obj_pdf->writeHTML($content, true, false, true, false, '');
		$obj_pdf->lastPage();
		$pdfFileName = $dealCode."_order_details.pdf";
		$obj_pdf->Output(CASE_PDF_FILE_PATH.$pdfFileName, 'F');

		$currentSuccessOrders = $this->order_model->getCurrentSuccessOrders($userID,$dealCode);
		$checkContactsURL = 'https://www.zohoapis.com/crm/v2/Contacts/search?criteria=(Email:equals:'.$currentSuccessOrders[0]['email'].')';
	    $acceess_token = $this->get_zohaccess_token();
   		$headers = [
   		    'Content-Type:application/json',
            'Authorization:'.$acceess_token
        ];
// 		$headers = [
// 			'Content-Type:application/json',
//     		'Authorization:'.CRM_AUTH_TOKEN
//     	];
		$curl = curl_init($checkContactsURL);
		curl_setopt_array($curl, array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTPHEADER => $headers
		));
		$contactsExists = curl_exec($curl);
		$contactsExists_array = json_decode($contactsExists);		
		$accountName = $currentSuccessOrders[0]['full_name'];

		    
		if(array_key_exists("data",$contactsExists_array)){
				$contactID = $contactsExists_array->data[0]->id;
				$accountName = $contactsExists_array->data[0]->Account_Name->name;
		}
		else
		{

			$checkAccountsURL = 'https://www.zohoapis.com/crm/v2/Accounts/search?criteria=(Email:equals:'.$currentSuccessOrders[0]['email'].')';
		    $curl = curl_init($checkAccountsURL);
			curl_setopt_array($curl, array(
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_HTTPHEADER => $headers
			));
			$accountsExists = curl_exec($curl);
		    $accountsExists_array = json_decode($accountsExists);

			$accountID = "";
			$existingEmail="";
			if(array_key_exists("data",$accountsExists_array)){
		  		$existingEmail = $accountsExists_array->data[0]->Email;
		  		if($currentSuccessOrders[0]['email']!=$existingEmail)
		  		{
		           	$accountName = $currentSuccessOrders[0]['full_name'] . " - " . $currentSuccessOrders[0]['email'];
		           	$checkAccounts2URL = 'https://www.zohoapis.com/crm/v2/Accounts/search?criteria=(accountname:equals:'.$accountName.')';
	           	  	$curl = curl_init($checkAccountsURL);
					curl_setopt_array($curl, array(
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_HTTPHEADER => $headers
					));
					$accounts2Exists = curl_exec($curl);
		    		$accounts2Exists_array = json_decode($accounts2Exists);
		           	if(array_key_exists("data",$accounts2Exists_array)){
						$accountID = $accounts2Exists_array->data[0]->id;
		           	}else{
		           		$accountID = "";
		           	}
			    }   
			}
		
			
			if($accountID=="")
			{
			      	$createAccountURL = 'https://www.zohoapis.com/crm/v2/Accounts';	
			        $AccountJsonData =   '{
			            "data" : [
						       	{
						       		"Account_Name":"'.$accountName.'",
						       		"Account_Owner":"'.CASE_OWNER.'",
						       		"Email":"'.$currentSuccessOrders[0]['email'].'",
						       		"Phone":"'.$currentSuccessOrders[0]['ship_phone'].'",
						       		"Billing_Street":"'.$currentSuccessOrders[0]['ship_address1'].'",
						       		"Billing_Street2":"'.$currentSuccessOrders[0]['ship_address2'].'",
						       		"Billing_City":"'.$currentSuccessOrders[0]['ship_city'].'",
						       		"Billing_State":'.$currentSuccessOrders[0]['ship_state'].',
						       		"Billing_Code":"'.$currentSuccessOrders[0]['ship_postal_code'].'",
						       		"Billing_Country":"'.$currentSuccessOrders[0]['ship_country'].'",
						       		"Shipping_Street":"'.$currentSuccessOrders[0]['ship_address1'].'",
						       		"Shipping_Street2":"'.$currentSuccessOrders[0]['ship_address2'].'",
						       		"Shipping_City":"'.$currentSuccessOrders[0]['ship_city'].'",
						       		"Shipping_State": "'.$currentSuccessOrders[0]['ship_state'].'",
						       		"Shipping_Code":"'.$currentSuccessOrders[0]['ship_postal_code'].'",
						       		"Shipping_Country":"'.$currentSuccessOrders[0]['ship_country'].'"
						        }
						    ]
						}';
			      
					    $curl = curl_init($createAccountURL);
							curl_setopt_array($curl, array(
								CURLOPT_POST => 1,
				            	CURLOPT_HTTPHEADER => $headers,
				            	CURLOPT_POSTFIELDS => $AccountJsonData,
				            	CURLOPT_RETURNTRANSFER => true
							));
						$createAccountResult = curl_exec($curl);
				    	$createAccountResultArray = json_decode($createAccountResult);
			      
			      if(array_key_exists("data",$createAccountResultArray))
			      {
			          $accountID = $createAccountResultArray->data[0]->id;
			      }
			      else
			      {
			          $accountID = "";
			      }
			}
			// create a contact
			$fullNameArr = explode(" ",$currentSuccessOrders[0]['full_name']);
			if(count($fullNameArr)==3)
			{
				$lastName = $fullNameArr[2];
				$firstName = $fullNameArr[0] . " " . $fullNameArr[1];
			}
			else if(count($fullNameArr)==2)
			{
				$lastName = $fullNameArr[1];
				$firstName = $fullNameArr[0];
			}
			else
			{
				$lastName = $currentSuccessOrders[0]['full_name'];
				$firstName = "";
			}
			$createContactURL = 'https://www.zohoapis.com/crm/v2/Contacts';	
			        $ContactJsonData =   '{
			            "data" : [
						       	{
						       		"First_Name":"Mr'.$firstName.'",
						       		"Last_Name":"'.$lastName.'",
						       		"Contact_Owner":"'.CASE_OWNER.'",
						       		"Account_Name":"'.$accountName.'",
						       		"Mobile":"'.$currentSuccessOrders[0]['ship_phone'].'",
						       		"Email":"'.$currentSuccessOrders[0]['email'].'",
						       		"Mailing_Street":"'.$currentSuccessOrders[0]['ship_address1'].'",
						       		"Mailing_Street2":'.$currentSuccessOrders[0]['ship_address2'].',
						       		"Mailing_City":"'.$currentSuccessOrders[0]['ship_city'].'",
						       		"Mailing_State":"'.$currentSuccessOrders[0]['ship_state'].'",
						       		"Mailing_Zip":"'.$currentSuccessOrders[0]['ship_postal_code'].'",
						       		"Mailing_Country":"'.$currentSuccessOrders[0]['ship_country'].'"
						        }
						    ]
						}';
			      
					    $curl = curl_init($createContactURL);
							curl_setopt_array($curl, array(
								CURLOPT_POST => 1,
				            	CURLOPT_HTTPHEADER => $headers,
				            	CURLOPT_POSTFIELDS => $ContactJsonData,
				            	CURLOPT_RETURNTRANSFER => true
							));
						$jsonContactRes = curl_exec($curl);
				    	$createContactResultArray = json_decode($jsonContactRes);
			
			if(array_key_exists("data",$createContactResultArray))
			{
			     $contactID = $createContactResultArray->data[0]->id;
			}
			else
			{
			    $contactID = "";
			}		
			
		}
		$Possible_Values_recurring = '';
		////check order is recurring or not
		if($currentSuccessOrders[0]['recurring_type'] == 'si_on_credit_card'){
			$Possible_Values_recurring  = 'SI On Credit Card';
		}else if($currentSuccessOrders[0]['recurring_type'] == 'enach'){
			$Possible_Values_recurring = 'Enach';
		}else if($currentSuccessOrders[0]['recurring_type'] == 'physical_nach'){
			$Possible_Values_recurring = 'Physical Nach';
		}else if($currentSuccessOrders[0]['recurring_type'] == 'pdc'){
			$Possible_Values_recurring = 'PDC';
		}else if($currentSuccessOrders[0]['recurring_type'] == 'not_opted'){
			$Possible_Values_recurring = 'Not Opted';
		}else{
			$Possible_Values_recurring = 'Not Opted';
		}
		
		$products = "";
		$deposit = 0;
		$monthlyRent = 0;
                $couponcode = "NA";
		$i=1;
		foreach($currentSuccessOrders as $order)
		{
			$products .= $i . '. ' . $order['product_name'] . ' - ' . $order['quantity'] . ' ('.$order['attr_name'].')' . '\n';
			$i++;
		}
		$deposit = $currentSuccessOrders[0]['shippingcost'];
		$monthlyRent = ($currentSuccessOrders[0]['total'] - $currentSuccessOrders[0]['shippingcost']);
		$fullName = $currentSuccessOrders[0]['ship_full_name'];
		$address1 = $currentSuccessOrders[0]['ship_address1'];
		$address2 = $currentSuccessOrders[0]['ship_address2'];
		$city = $currentSuccessOrders[0]['ship_city'];
		$country = $currentSuccessOrders[0]['ship_country'];
		$state = $currentSuccessOrders[0]['ship_state'];
		$zipCode = $currentSuccessOrders[0]['ship_postal_code'];
		$phoneNumber = $currentSuccessOrders[0]['ship_phone'];
        $couponcode = $currentSuccessOrders[0]['couponCode'] ? $currentSuccessOrders[0]['couponCode'] : '';
        $coupon_code_details  = $this->order_model->get_all_details(COUPONCARDS,array('id' => $currentSuccessOrders[0]['coupon_id']));
    	$monthDescription = '';
        if($coupon_code_details->row()->recurring_invoice_tenure > 0 ){
        	$ongoingAmount = $monthlyRent + ($currentSuccessOrders[0]['discountAmount'] +  $currentSuccessOrders[0]['discount_on_si']);
        	for($i = 1;$i <= $coupon_code_details->row()->recurring_invoice_tenure; $i++){
        		$month = '';
        		if($i == 1){
        			$month = $i.'st';
        		}else if($i == 2){
        			$month = $i.'nd';
        		}else if($i == 3){
        			$month = $i.'rd';
        		}else{
        			$month = $i.'th';
        		}
        		$monthDescription .=  $month.' Month Rent: Rs '. $monthlyRent.'\n';
        	}
        }else{
        	$ongoingAmount = $monthlyRent;
        }
		
		$OrderOwnerDetails  = '';
        $userDetails  = $this->order_model->getOfflineOrderOwnerDetails($dealCode);
         if($userDetails->num_rows() > 0){
        	$userData = $userDetails->row();
        	$OrderOwnerDetails = '\n\n OFFLINE ORDER \n\n Advance Payment:'.$userData->advance_rental.' \n Order Owner Name: '.$currentSuccessOrders[0]['ship_full_name'].
        	'\n Order Owner Email: '.$currentSuccessOrders[0]['email'].
        	'\n Transaction ID: '.$currentSuccessOrders[0]['dealCodeNumber'].
        	'\n Mode: website'.
        	'\n Order Date: '. date('Y-m-d h:i:s A', strtotime($currentSuccessOrders[0]['modified']));
        }

        $description ='\n Total Amount Paid: Rs. '.($deposit + $monthlyRent).
        '\n Deposit: Rs. '.$deposit.
        '\n Discount: '.($currentSuccessOrders[0]['discountAmount'] - $currentSuccessOrders[0]['discount_on_si']).
        '\n Coupon Used: '.$couponcode.'\n\n'.
        $monthDescription.
        ' Rent For The Rest Of Mnths: Rs. '.$ongoingAmount .
        '\n Tenure: '.$currentSuccessOrders[0]['attr_name'].'\n'.
        '\n Product:\n'.$products.
        '\n Full Name: '.$fullName.
        '\n Address: '.$address1.
        '\n Address2: '.$address2.
        '\n City: '.$city.
        '\n Country: '.$country.
        '\n  State: '.$state.
        '\n Zip Code: '.$zipCode.
        '\n Phone Number: '.$phoneNumber.
        '\n '.$OrderOwnerDetails;

		$city = $currentSuccessOrders[0]['city'];
		$relatedTo = $contactID;
		$caseOwner = CASE_OWNER;
		$createCaseURL = 'https://www.zohoapis.com/crm/v2/Cases';	
        $CreateCaseJsonData =   '{
        "data" : [
		       	{
	       		"Case_Origin":"Offline",
	       		"Subject":"New Order - '.$currentSuccessOrders[0]['ship_full_name'].' ('.$currentSuccessOrders[0]['email'].')",
	       		"Status":"New Order",
	       		"Owner":"649600348",
	       		"Order_ID":"'.$currentSuccessOrders[0]['dealCodeNumber'].'",
	       		"Type":"Order",
	       		"Sub_Type":"New - Rental",
	       		"Coupon_Code":"'.$couponcode.'",
	       		"Related_To":"'.$relatedTo.'",
	       		"Description":"'.$description.'",
	       		"City":"'.$currentSuccessOrders[0]['ship_city'].'",
	       		"Account_Name" :"'.$accountName.'",
	       		"LOB":"B2C",
	       		"Email":"'.$currentSuccessOrders[0]['email'].'",
	       		"Phone":"'.$currentSuccessOrders[0]['ship_phone'].'",
	       		"Offline_User_Name" : "'.$userData->full_name.'",
	       		"Offline_User_Email": "'.$userData->email.'",
	       		"Invoice_Url":"'.base_url().'uploaded/'.$pdfFileName.'",
		        }
		    ]
		}';
	    $curl = curl_init($createCaseURL);
			curl_setopt_array($curl, array(
				CURLOPT_POST => 1,
            	CURLOPT_HTTPHEADER => $headers,
            	CURLOPT_POSTFIELDS => $CreateCaseJsonData,
            	CURLOPT_RETURNTRANSFER => true
			));
		$jsonCaseRes = curl_exec($curl);
    	$createCaseResultArray = json_decode($jsonCaseRes);
    	
		if(!array_key_exists("data",$createCaseResultArray))
		{
			$failed_msg = 'For '.$fullName . ' Zoho Case is not Working<br>'.$products.'<br> Order ID:'.$currentSuccessOrders[0]['dealCodeNumber'].'<br>Order Amount:'.$currentSuccessOrders[0]['total'].'<br> email is '.$currentSuccessOrders[0]['email'];
			$email_values = array('mail_type'=>'html',
                         'from_mail_id'=>'hello@cityfurnish.com',
                         'mail_name'=>'Zoho Error Mail',
                         'to_mail_id'=>'naresh.suthar@agileinfoways.com',
                         'subject_message'=>'Zoho Case Error',
                         'body_messages'=>$failed_msg
        				);
        	$email_send_to_common = $this->order_model->common_email_send($email_values);
		}

// 		$this->order_model->update_details(PAYMENT,array('is_zoho_invoice_created' => 1) ,array('dealCodeNumber' => $dealCode));
    }


    public function CreateAllZohoInvoicesIndirect(){
    	$user_id = $this->input->post('user_id');
    	$deal_code = $this->input->post('deal_code');
        $this->createAutoCaseInZohoCRM($user_id,$deal_code);
		$this->CreateAllZohoInvoices($user_id,$deal_code);  
	    $response = array('status' => 'success','code' => 200);
	    header('Content-Type: application/json');
	    echo json_encode($response);
    }


    /////create zoho bulk order inovices
    public function CreateAllZohoInvoices($user_id,$deal_code){

    	// $user_id = $this->input->post('user_id');
    	// $deal_code = $this->input->post('deal_code');

    	// unset($_SESSION['offline_user_id']);
	    // unset($_SESSION['offline_dealcode']);   

    	$RentalAmount = $this->order_model->get_all_details(OFFLINE_ORDERS,array('dealCodeNumber' => $deal_code));
    	$product_info = $this->order_model->getCurrentSuccessOrders($user_id, $deal_code);

        $subproducts_quantity = array();   	
        $wearhouse_code  = $this->order_model->get_all_details(CITY_WEARHOUSE_CODE,array('id !=' => ''));	
        $wareCode = 'BAN';	
        $subject_so_code = 'BAN';	
        $gst_number = '';	
        $reg_address = '';
        foreach ($wearhouse_code->result() as $key => $value) {
        	if($value->city_name == $product_info[0]['shippingcity']){
                    $wareCode = $value->wearhouse_code;
                    $subject_so_code = $value->so_city_code;
                    $gst_number = $value->gst_num;
                    $reg_address = $value->reg_address;
        	}else if($value->city_name == 'Delhi'){
					$wareCode = $value->wearhouse_code;
					$subject_so_code = $value->so_city_code;
                    $gst_number = $value->gst_num;
                    $reg_address = $value->reg_address;
        	}
        }
       
       	if($product_info[0]['couponCode'] != ''){
			$couponCode = $product_info[0]['couponCode'];
		}else{
			$couponCode = '';
		}

		$url = "https://books.zoho.com/api/v3/contacts?organization_id=".BOOKS_ORGANIZATION_ID."&email_contains=".$product_info[0]['email']."&Status=Active";
        $headers = [
                    'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
                    'Authorization:'.BOOKS_AUTH_TOKEN
            ];
        $curl = curl_init($url);
        		curl_setopt_array($curl, array(
            		CURLOPT_RETURNTRANSFER => true,
            		CURLOPT_HTTPHEADER => $headers
        		));
        $result = curl_exec($curl);
        $contact_data =  json_decode($result, TRUE);
        if (array_key_exists("contact_id", $contact_data['contacts'][0])) {	
            $contact_id = $contact_data['contacts'][0]['contact_id'];
            $update_contact_url = "https://books.zoho.com/api/v3/contacts/".$contact_id."";
            $data = array(
	                'authtoken'=>BOOKS_AUTH_TOKEN,
	                'JSONString' => '
	                {
	                    "contact_name": "'.$product_info[0]['ship_full_name'].'",
	                    "contact_type": "customer",
	                    "gst_treatment":"consumer",
	                    "shipping_address": {
	                        "attention": "Mr.'.$product_info[0]['ship_full_name'].'",
	                        "address": "'.$product_info[0]['ship_address1'].', '.$product_info[0]['ship_address2'].'",
	                        "city": '.$product_info[0]['ship_city'].',
	                        "state": '.$product_info[0]['ship_state'].',
	                        "zip": '.$product_info[0]['ship_postal_code'].',
	                        "country": "India",
	                        "phone": '.$product_info[0]['ship_phone'].',
	                    },
						"billing_address": {
			                "attention": "Mr.'.$product_info[0]['ship_full_name'].'",
	                        "address": "'.$product_info[0]['ship_address1'].' '.$product_info[0]['ship_address2'].'",
	                        "city": "'.$product_info[0]['ship_city'].'",
	                        "state": '.$product_info[0]['ship_state'].',
	                        "zip": '.$product_info[0]['ship_postal_code'].',
	                        "country": "India",
	                        "phone": '.$product_info[0]['ship_phone'].',
			            },
	                }',
	                "organization_id"=>BOOKS_ORGANIZATION_ID
	            );

					$ch = curl_init($update_contact_url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
					curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
					$response = curl_exec($ch);
        }else{
            $Contact_url ="https://books.zoho.com/api/v3/contacts";
            $Contact_data = array(
                'authtoken'=>BOOKS_AUTH_TOKEN,
                'JSONString' => '
                {
                    "contact_name": '.$product_info[0]['ship_full_name'].',
                    "contact_type": "customer",
                    "gst_treatment":"consumer",
                    "place_of_contact":"",
                    "shipping_address": {
                        "attention": "Mr.'.$product_info[0]['ship_full_name'].'",
                        "address": "'.$product_info[0]['ship_address1'].' '.$product_info[0]['ship_address2'].'",
                        "city": "'.$product_info[0]['ship_city'].'",
                        "state": "'.$product_info[0]['ship_state'].'",
                        "zip": '.$product_info[0]['ship_postal_code'].',
                        "country": "India",
                        "phone": "'.$product_info[0]['ship_phone'].'",
                    },
                     "billing_address": {
			                "attention": "Mr.'.$product_info[0]['ship_full_name'].'",
	                        "address": "'.$product_info[0]['ship_address1'].' '.$product_info[0]['ship_address2'].'",
	                        "city": "'.$product_info[0]['ship_city'].'",
	                        "state": "'.$product_info[0]['ship_state'].'",
	                        "zip": '.$product_info[0]['ship_postal_code'].',
	                        "country": "India",
	                        "phone": "'.$product_info[0]['ship_phone'].'",
			            },
                    "contact_persons": [
                        {
                            "salutation": "Mr",
                            "first_name": "'.$product_info[0]['ship_full_name'].'",
                            "email": "'.$product_info[0]['email'].'",
                            "phone": "'.$product_info[0]['ship_phone'].'"
                        }
                    ],
                    
                }',
                "organization_id"=>BOOKS_ORGANIZATION_ID
            );
        	$curl = curl_init($Contact_url);
                	curl_setopt_array($curl, array(
                    	CURLOPT_POST => 1,
                    	CURLOPT_POSTFIELDS => $Contact_data,
                    	CURLOPT_RETURNTRANSFER => true
                	));
            $Contact_response = curl_exec($curl);
            $decode_Contact = json_decode($Contact_response);
            $contact_id = $decode_Contact->contact->contact_id;
    	}

           	foreach ($product_info as $key =>  $cartRow) {
	        	if($cartRow['attr_name'] != ''){
	        		$tenure  = explode(' ', $cartRow['attr_name']); 
	        		$dayx = date("Y-m-d", strtotime("+".$cartRow['attr_name']));
	    			$pick_up_date =  date("Y-m-d", strtotime($dayx.' +5 day'));
	        	}
	        	array_push($subproducts_quantity, unserialize($cartRow['subproduct_quantity']));
	            $InvImg = @explode(',',$cartRow->image);
	            $unitPrice = ($cartRow['price']*(0.01*$cartRow['product_tax_cost']))+$cartRow['price'] * $cartRow['quantity'] ;
	            $unitDeposit =  $cartRow['product_shipping_cost'] * $cartRow['quantity'];
	            $grandDeposit = $grandDeposit + ($unitDeposit*$cartRow['quantity']);
	            $uTot = $unitPrice + $unitDeposit;
	            $new_deposite +=  $unitDeposit;
        	}

        	$private_total = $new_deposite - $product_info[0]['discountAmount'];
	        $private_total = $private_total + $product_info[0]['tax'];
        	$itemsarray = array();
        	$TotalProductPriceTestingPurpse = 0;
        	$counter = 0;

	        foreach ($product_info as $key =>  $cartRow) {
	       		if($cartRow['subproducts'] == ''){
	       			$zoho_item_id = $this->check_sku($cartRow['sku'], $cartRow['product_name'], $cartRow['id'],'1');
	       			if(!array_key_exists('item_id', $zoho_item_id)){
                          break;
	       			}
	       			$itemsarray[$counter]['item_id'] = $zoho_item_id['item_id'];
	    	 		$itemsarray[$counter]['item_order'] = $cartRow['quantity'];
	                $itemsarray[$counter]['quantity'] = $cartRow['quantity'];
	                if($cartRow['attribute_values'] != ''){
	                	$itemsarray[$counter]['rate'] = round($cartRow['price'] - ($cartRow['price'] / 100) * $cartRow['package_discount']) ;
	                	$TotalProductPriceTestingPurpse += $itemsarray[$counter]['rate'] * $cartRow['quantity'];
	                	$itemsarray[$counter]['bcy_rate'] = round($cartRow['price'] - ($cartRow['price'] / 100) * $cartRow['package_discount']);
	            	}else{
	            		$itemsarray[$counter]['rate'] = $cartRow['price'];
	                	$itemsarray[$counter]['bcy_rate'] = $cartRow['price'];
	                	$TotalProductPriceTestingPurpse += $itemsarray[$counter]['rate'] * $cartRow['quantity'];
	            	}
	               $itemsarray[$counter]['name'] =  $zoho_item_id['item_name'];
	               $itemsarray[$counter]['tax_id'] = $zoho_item_id['item_tax_preferences'][0]['tax_id'];
		           $counter++;
		       	}else{ 
		   			$product_id = explode(',',$cartRow['subproducts']);
		   			$product_data = $this->order_model->Get_subproducts($product_id);
		   			foreach ($product_data as $packagekey => $packagevalue) {
		   				if($packagevalue->subproducts != '')
		   				{
		   					$subproduct_id = explode(',', $packagevalue->subproducts);
		   					$subproducts_subproduct = $this->order_model->Get_subproducts($subproduct_id);
		   					foreach ($subproducts_subproduct as $key => $value) {
		   						$zoho_item_id = $this->check_sku($value->sku, $value->product_name, $value->id,'2');
				       			if(!array_key_exists('item_id', $zoho_item_id)){
			                          break;
				       			}
				       			$itemsarray[$counter]['item_id'] = $zoho_item_id['item_id'];
				       			if(!$value->is_addon){
				       			    $attr_price = $this->order_model->GetPriceOnTenure($cartRow['attr_name'],$value->id);
				       				$itemsarray[$counter]['rate'] =  round($attr_price->attr_price - ($attr_price->attr_price / 100) * $cartRow['package_discount']);
		                    		$itemsarray[$counter]['bcy_rate'] =   round($attr_price->attr_price - ($attr_price->attr_price / 100) * $cartRow['package_discount']);
		                    		$TotalProductPriceTestingPurpse += $itemsarray[$counter]['rate'] * $subproducts_quantity[$key][$packagevalue->id];

				       			}else{
 									$attr_price = $this->order_model->GetPriceOnTenure($cartRow['attr_name'],$value->id);
				       				if($attr_price == ''){
					       				$itemsarray[$counter]['rate'] =  round($value->price - ($value->price / 100) * $cartRow['package_discount']);
			                    		$itemsarray[$counter]['bcy_rate'] = round($value->price - ($value->price / 100) * $cartRow['package_discount']);
		                    		}else{
										$itemsarray[$counter]['rate'] =  round($attr_price->attr_price - ($attr_price->attr_price / 100) * $cartRow['package_discount']);
			                    		$itemsarray[$counter]['bcy_rate'] = round($attr_price->attr_price - ($attr_price->attr_price / 100) * $cartRow['package_discount']);
		                    		}
		                    		$TotalProductPriceTestingPurpse += $itemsarray[$counter]['rate'] * $subproducts_quantity[$key][$packagevalue->id];
				       			}
		   						$itemsarray[$counter]['item_order'] = $cartRow['quantity'];
		                		$itemsarray[$counter]['quantity'] = $subproducts_quantity[$key][$packagevalue->id];
		                    	$itemsarray[$counter]['name'] =  $zoho_item_id['item_name'];
		                    	$itemsarray[$counter]['tax_id'] = $zoho_item_id['item_tax_preferences'][0]['tax_id'];
		   						$counter++;
		   					}
		   				}
		   				else
		   				{
		   					$zoho_item_id = $this->check_sku($packagevalue->sku, $packagevalue->product_name, $packagevalue->id,'3');
				       			if(!array_key_exists('item_id', $zoho_item_id)){
			                          break;
				       			}
				       		$itemsarray[$counter]['item_id'] = $zoho_item_id['item_id'];
				       		if(!$packagevalue->is_addon){
		   						$attr_price = $this->order_model->GetPriceOnTenure($cartRow['attr_name'],$packagevalue->id);
		               	    	$itemsarray[$counter]['rate'] =  round($attr_price->attr_price - ($attr_price->attr_price / 100) * $cartRow['package_discount']);
		                    	$itemsarray[$counter]['bcy_rate'] =   round($attr_price->attr_price - ($attr_price->attr_price / 100) * $cartRow['package_discount']);
		                    	$TotalProductPriceTestingPurpse += $itemsarray[$counter]['rate'] * $subproducts_quantity[$key][$packagevalue->id];

		   					}else{
		   						$attr_price = $this->order_model->GetPriceOnTenure($cartRow['attr_name'],$packagevalue->id);
		   						if($attr_price == ''){
			   						$itemsarray[$counter]['rate'] =  round($packagevalue->price - ($packagevalue->price / 100) * $cartRow['package_discount']);
			                    	$itemsarray[$counter]['bcy_rate'] =   round($packagevalue->price - ($packagevalue->price / 100) * $cartRow['package_discount']);
		   							
		   						}else{
									$itemsarray[$counter]['rate'] =  round($attr_price->attr_price - ($attr_price->attr_price / 100) * $cartRow['package_discount']);
			                    	$itemsarray[$counter]['bcy_rate'] =   round($attr_price->attr_price - ($attr_price->attr_price / 100) * $cartRow['package_discount']);
		   						}
		                    	$TotalProductPriceTestingPurpse += $itemsarray[$counter]['rate'] * $subproducts_quantity[$key][$packagevalue->id];
		   					}
		   					$itemsarray[$counter]['item_order'] = $cartRow['quantity'];
		                	$itemsarray[$counter]['quantity'] = $subproducts_quantity[$key][$packagevalue->id];
		                    $itemsarray[$counter]['name'] =  $zoho_item_id['item_name'];
		                    $itemsarray[$counter]['tax_id'] = $zoho_item_id['item_tax_preferences'][0]['tax_id'];
		                    $counter++;
		   				}						
		   			}
		       	}
			}
			$json = json_encode($itemsarray);
	        $sales_order_id = $this->order_model->getAdminSettings();
	        $new_order_id = str_pad(ltrim($sales_order_id->row()->zoho_books_sales_order_id, '0') + 1, 4, '0', STR_PAD_LEFT);
	        $order_date = $product_info[0]['created'];
	        $createDate = strtotime($order_date);
	        $strip = date('Y-m-d', $createDate); 
	        if($product_info[0]['tax'] > 0){
	            $tax = 'true';
	        }else{
	            $tax = 'false';
	        }

          	$customer_notes = 'Tenure: '.$tenure[0].' '.$tenure[1].' Place of Supply: ' .$product_info[0]['full_name'].', '.$product_info[0]['ship_address1'].', '.$product_info[0]['ship_address2'].', '.$product_info[0]['ship_city'].', '.$product_info[0]['ship_state'].', '.$product_info[0]['ship_postal_code'].', India , Phone: '.$product_info[0]['ship_phone'].'';


	        $Curl = "https://books.zoho.com/api/v3/salesorders";
	        $Invoice = array(
	                        'authtoken'=>BOOKS_AUTH_TOKEN,
	                        'JSONString' => '{
                    	        "customer_id": '.$contact_id.',
	                            "date": '.$strip.',    
	                            "line_items": '.$json.',
	                            "notes":"'.$customer_notes.'",
	                            "place_of_supply": "HR",
	                            "salesorder_number":"ON-RET-'.$subject_so_code.'-'.$new_order_id.'",
	                            "reference_number": "#'.$product_info[0]['dealCodeNumber'].'",
	                            "salesperson_name":"Website",
	                            "status": "open",
	                            "is_inclusive_tax": true,
	                            "delivery_method": "Company Transport",
	                            "custom_fields": [
	                          		{
	                          			"placeholder": "cf_warehouse_code",
								    	"index": 1,
								    	"value":"'.$wareCode.'" 
	                                },
	                                {
									    "placeholder": "cf_order_type",
									    "index": 2,
									    "value":"Rent"
									},
									{
									    "placeholder": "cf_order_status",
									    "index": 3,
									    "value":"Live"
									},
									{
									    "placeholder": "cf_tenure",
									    "index": 4,
									    "value": '.$tenure[0].'
									},
									{
									    "placeholder": "cf_in",
									    "index": 5,
									    "value":"'.ucfirst($tenure[1]).'"
									},
									{
									    "placeholder": "cf_pickup_date",
									    "index": 6,
									    "value": "'.$pick_up_date.'"
									},

									{
									    "placeholder": "cf_offer_given",
									    "index": 9,
									    "value":"'.$couponCode.'"
									   
									},		
	                            ],
	                            "adjustment_description": "Adjustment",
	                            "documents": [
	                                {}
	                            ]
	                        }',
	                        "organization_id"=>BOOKS_ORGANIZATION_ID
	                    );
	        $curl = curl_init($Curl);
	            	curl_setopt_array($curl, array(
	                	CURLOPT_POST => 1,
	                	CURLOPT_POSTFIELDS => $Invoice,
	                	CURLOPT_RETURNTRANSFER => true
	            	));
	        $invoice_result = curl_exec($curl); 
			$decode_result = json_decode($invoice_result);

			$newItemArray = array();
 			$reference_number = '';

	        if (isset($decode_result->salesorder)) {
        	 	$reference_number = $decode_result->salesorder->salesorder_number;
	        	$invoice_id = $decode_result->salesorder->salesorder_id;
	            $status_url ='https://books.zoho.com/api/v3/salesorders/'.$invoice_id.'/status/open?organization_id='.BOOKS_ORGANIZATION_ID;
			    $headers = [
		                    'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
		                    'Authorization:'.BOOKS_AUTH_TOKEN
				            ];
		        $curl = curl_init($status_url);
		        		curl_setopt_array($curl, array(
		            		CURLOPT_RETURNTRANSFER => true,
		            		CURLOPT_HTTPHEADER => $headers,
		            		CURLOPT_POST => 1,
		        		));
		        $result = curl_exec($curl);
        		$respo =  json_decode($result, TRUE);

        		foreach ($decode_result->salesorder->line_items as $key => $value) {
        			$newItemArray[$key]['item_id'] =  $value->item_id;
        			$newItemArray[$key]['rate'] = $value->rate;
        			$newItemArray[$key]['bcy_rate'] = $value->bcy_rate;
        			$newItemArray[$key]['item_order'] = $value->item_order;
        			$newItemArray[$key]['quantity'] = $value->quantity;
                    $newItemArray[$key]['name'] = $value->name;
                    $newItemArray[$key]['tax_id'] = $value->tax_id;
                    $newItemArray[$key]['salesorder_item_id'] = $value->line_item_id;
        		}

					
	      	}
			$dataArr = array('zoho_books_sales_order_id' => $new_order_id);
			$condition  = array('id' => 1);
	        $this->order_model->UpdateAdminSetting($dataArr,$condition);
	        //Sales Invoice end here


	        //Retainer Invoice Start here 

            $order_date = $product_info[0]['created'];
            $createDate = strtotime($order_date);
            $strip = date('Y-m-d', $createDate); 
	        $RetainerUrl = "https://books.zoho.com/api/v3/retainerinvoices";
	        $RetainerData = array(
                    'authtoken'=>BOOKS_AUTH_TOKEN,
                    'JSONString' => '{
                        "customer_id": '.$contact_id.',
                        "reference_number":"'.$reference_number.'",
                        "notes":  "'.$customer_notes.'",
                        "terms": "Terms & Conditions apply",
                        "line_items":[
						        {
						            "description": "Security Deposit",
						            "item_order": 1,
						            "rate": '.$new_deposite.'
						        }
						    ],
					     "custom_fields": [
      						{
							    "placeholder": "cf_gst",
							    "index": 1,
							    "value":"'.$gst_number.'"
							   
							},
                        ],
                        "payment_options": {
                            "payment_gateways": [
                                {
                                    "gateway_name": "paypal"
                                }
                            ]
                        },
                    }',
                    "organization_id"=>BOOKS_ORGANIZATION_ID
                );
	      	$curl = curl_init($RetainerUrl);
	        curl_setopt_array($curl, array(
	            CURLOPT_POST => 1,
	            CURLOPT_POSTFIELDS => $RetainerData,
	            CURLOPT_RETURNTRANSFER => true
	        ));
	        $result = curl_exec($curl);
	        $respo = json_decode($result);
	        if(isset($respo->retainerinvoice)){
				$Invoice_id = $respo->retainerinvoice->retainerinvoice_id;
    			$this->CreateCustomerPayment_retainer($contact_id,$product_info[0]['mandate_id'],$Invoice_id,$new_deposite);
    		}

				//Create Invoice 


			$seller_info = $this->order_model->get_all_details(USERS,array('id' => $product_info[0]['sell_id']));
		    $coupon_code  = $this->order_model->get_all_details(COUPONCARDS,array('id' => $product_info[0]['coupon_id']));
	    	if($coupon_code->row()->price_value >= 100){
	                $discount_first_month  = 0;
	    	}else{
	    		$discount_first_month =  $product_info[0]['discountAmount'];
	    	}
			$is_inclusive_tax = 'true';
	    	if($product_info[0]['discountAmount'] >= $unitPrice){
	    		$is_inclusive_tax = 'false';
	    	}
	    	
	    	$total_discount = ($product_info[0]['discountAmount'] +  $product_info[0]['discount_on_si']); 
	    	if($total_discount > $TotalProductPriceTestingPurpse){
	    		$total_discount = $TotalProductPriceTestingPurpse - 1;
	    	}

	    	$DiscountInPercentage = (($total_discount * 100) / $TotalProductPriceTestingPurpse);

    	  	$Curl = "https://books.zoho.com/api/v3/invoices";
       		$Invoice = array(
                        'authtoken'=>BOOKS_AUTH_TOKEN,
                        'JSONString' => '{
                            "customer_id": '.$contact_id.',
                            "date": '.$strip.',
                            "discount": "'.$DiscountInPercentage.'%",
                            "reference_number":"'.$reference_number.'",
                            "is_inclusive_tax": true,
                            "discount_type":"entity_level",
    						"salesperson_name": "Website",
                            "line_items":'.json_encode($newItemArray).',
                            "place_of_supply": "HR",
                            "notes":"'.$customer_notes.'",
                            "payment_options": {},
                            "allow_partial_payments": true,
                            "is_discount_before_tax":true,
                            "custom_body": " ",
                            "custom_subject": "Sale Invoice",
                            "terms": "Terms & Conditions apply",
                            "gateway_name": "PayU",
                            "custom_fields": [
	          						{
									    "placeholder": "cf_gst",
									    "index": 4,
									    "value":"'.$gst_number.'"
									   
									},
									{
									    "placeholder": "cf_reg_address",
									    "index": 5,
									    "value":"'.$reg_address.'"
									   
									},
									{
									    "placeholder": "cf_reverse_charge_applicable",
									    "index": 6,
									    "value":"No"
									   
									},		
									{
									    "placeholder": "cf_invoice_number_exception",
									    "index": 7,
									    "value":"No"
									   
									},
									{
									    "placeholder": "cf_pos",
									    "index": 8,
									    "value":"'.$product_info[0]['ship_city'].'"
									   
									},
									{
									    "placeholder": "cf_delivery_state",
									    "index": 9,
									    "value":"'.$product_info[0]['ship_state'].'"
									   
									},	
	                            ],
                            "additional_field1": "standard"
                           
                        }',
                        "organization_id"=>BOOKS_ORGANIZATION_ID
                    );

          $curl = curl_init($Curl);
            curl_setopt_array($curl, array(
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $Invoice,
                CURLOPT_RETURNTRANSFER => true
            ));
            $invoice_result = curl_exec($curl);  
            $new_result = json_decode($invoice_result);   
            if (isset($new_result->invoice)) {
	            $Invoice_id = $new_result->invoice->invoice_id;
	            $status_url ='https://books.zoho.com/api/v3/invoices/'.$Invoice_id.'/status/sent?organization_id=82487023';
			    $headers = [
		                    'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
		                    'Authorization:'.BOOKS_AUTH_TOKEN
				            ];
		        $curl = curl_init($status_url);
		        		curl_setopt_array($curl, array(
		            		CURLOPT_RETURNTRANSFER => true,
		            		CURLOPT_HTTPHEADER => $headers,
		            		CURLOPT_POST => 1,
		        		));
		        $result = curl_exec($curl);
        		$respo =  json_decode($result, TRUE);
          		$this->CreateCustomerPayment($contact_id,$product_info[0]['mandate_id'],$Invoice_id,$new_result->invoice->total);	
	      	}
	      	$total_discount = $product_info[0]['discountAmount']; 
	    	if($total_discount > $TotalProductPriceTestingPurpse){
	    		$total_discount = $TotalProductPriceTestingPurpse - 1;
	    	}

	    	$DiscountInPercentage = (($total_discount * 100) / $TotalProductPriceTestingPurpse);
	      	
	        if($coupon_code->row()->recurring_invoice_tenure > 0){
	      		$loopScheduler = ($coupon_code->row()->recurring_invoice_tenure - 1); 
	      		for ($i=0; $i < $loopScheduler; $i++) { 
	      			$end_date = date('Y-m-d',strtotime('+'.($i+1).' month'));
		           	$Curl = "https://books.zoho.com/api/v3/recurringinvoices";
			        $Invoice = array(
			                        'authtoken'=> BOOKS_AUTH_TOKEN,
			                        'JSONString' => '{
			                        		"recurrence_name": "'.strtoupper($product_info[0]['ship_full_name']).'('.$reference_number.rand(9999,0000).')",
										    "customer_id": '.$contact_id.',
										    "place_of_supply": "'.$place_of_supply.'",
										    "start_date": "'.date('Y-m-d',strtotime('+'.($i+1).' month')).'",
										    "end_date":"'.date('Y-m-d', strtotime('+1 day', strtotime($end_date))).'",
										    "discount_type":"entity_level",
										    "reference_number":"'.$reference_number.'",
									    	"discount": '.$DiscountInPercentage.'%,
		 									"is_discount_before_tax":true,
										    "recurrence_frequency": "months",
										    "salesperson_name":"Accounts Team",
										    "is_inclusive_tax": true,
										    "payment_terms":"0",
										    "payment_terms_label":"Due on Receipt",
										    "line_items":'.$json.',
										    "notes":"'.$customer_notes.'",
										    "email": "'.$product_info[0]['email'].'",
										    "payment_gateways": [
										        {
										            "configured": true,
										            "additional_field1": "standard",
										            "gateway_name": "paypal"
										        }
										    ],

										    "custom_fields": [
			          						{
											    "placeholder": "cf_gst",
											    "index": 4,
											    "value":"'.$gst_number.'"
											   
											},
											{
											    "placeholder": "cf_reg_address",
											    "index": 5,
											    "value":"'.$reg_address.'"
											   
											},
											{
											    "placeholder": "cf_reverse_charge_applicable",
											    "index": 6,
											    "value":"No"
											   
											},		
											{
											    "placeholder": "cf_invoice_number_exception",
											    "index": 7,
											    "value":"No"
											   
											},
											{
											    "placeholder": "cf_pos",
											    "index": 8,
											    "value":"'.$product_info[0]['ship_city'].'"
											   
											},
											{
											    "placeholder": "cf_delivery_state",
											    "index": 9,
											    "value":"'.$product_info[0]['ship_state'].'"
											   
											},	
			                            ],
										 
										}',
			                        "organization_id"=> BOOKS_ORGANIZATION_ID
			                    );
			        
		           	$curl = curl_init($Curl);
		            	curl_setopt_array($curl, array(
		                	CURLOPT_POST => 1,
		                	CURLOPT_POSTFIELDS => $Invoice,
		                	CURLOPT_RETURNTRANSFER => true
		            	));
			        $invoice_result = curl_exec($curl);
			      	$respo = json_decode($invoice_result);		      
	  	       	}  
	      	}


      		if($coupon_code->row()->recurring_invoice_tenure > 0 ){
      				$DiscountInPercentage = 0;
      				$next_month =  $coupon_code->row()->recurring_invoice_tenure;
      		}else{
      		    $next_month = 1;	
      		}


          	$Curl = "https://books.zoho.com/api/v3/recurringinvoices";
            $Invoice = array(
                    'authtoken'=> BOOKS_AUTH_TOKEN,
                    'JSONString' => '{
                    		"recurrence_name": "'.strtoupper($product_info[0]['ship_full_name']).'('.$reference_number.')",
						    "customer_id": '.$contact_id.',
						    "place_of_supply": "HR",
						    "start_date": "'.date('Y-m-d',strtotime('+'.$next_month.' month')).'",
						    "discount_type":"entity_level",
						    "reference_number":"'.$reference_number.'",
					    	"discount": '.$DiscountInPercentage.'%,
 							"is_discount_before_tax":true,
						    "recurrence_frequency": "months",
						    "salesperson_name":"Accounts Team",
						    "is_inclusive_tax": true,
						    "payment_terms":"0",
						    "payment_terms_label":"Due on Receipt",
						    "line_items":'.$json.',
						    "notes":"'.$customer_notes.'",
						    "email": "'.$product_info[0]['email'].'",
						    "payment_gateways": [
						        {
						            "configured": true,
						            "additional_field1": "standard",
						            "gateway_name": "paypal"
						        }
						    ],

						    "custom_fields": [
      						{
							    "placeholder": "cf_gst",
							    "index": 4,
							    "value":"'.$gst_number.'"
							   
							},
							{
							    "placeholder": "cf_reg_address",
							    "index": 5,
							    "value":"'.$reg_address.'"
							   
							},
							{
							    "placeholder": "cf_reverse_charge_applicable",
							    "index": 6,
							    "value":"No"
							   
							},		
							{
							    "placeholder": "cf_invoice_number_exception",
							    "index": 7,
							    "value":"No"
							   
							},
							{
							    "placeholder": "cf_pos",
							    "index": 8,
							    "value":"'.$product_info[0]['ship_city'].'"
							   
							},
							{
							    "placeholder": "cf_delivery_state",
							    "index": 9,
							    "value":"'.$product_info[0]['ship_state'].'"
							   
							},	
                        ],
						 
						}',
                    "organization_id"=> BOOKS_ORGANIZATION_ID
                );
                
		        $curl = curl_init($Curl);
		            	curl_setopt_array($curl, array(
		                	CURLOPT_POST => 1,
		                	CURLOPT_POSTFIELDS => $Invoice,
		                	CURLOPT_RETURNTRANSFER => true
		            	));
		        $invoice_result = curl_exec($curl);
		      	$respo = json_decode($invoice_result);
	}

	public function CreateCustomerPayment_retainer($contact_id,$order_id,$Invoice_id,$total ){
    	  	$Curl = "https://books.zoho.com/api/v3/customerpayments?organization_id=".BOOKS_ORGANIZATION_ID;
    	  	$Invoice = 'JSONString={
    	  		   	"customer_id": '.$contact_id.',
					"payment_mode": "Payment Gateway",
					"amount": '.$total.',
					"date": "'.date('Y-m-d').'",
					"reference_number": "#'.$order_id.'",
					"invoice_id":'.$Invoice_id.',
					"deposit_to":"PayU"

    	  	}';
	         $headers = [
                'Content-Type: application/x-www-form-urlencoded',
                'Authorization:'.BOOKS_AUTH_TOKEN
	            ];
		
	        $curl = curl_init($Curl);
	            	curl_setopt_array($curl, array(
	                	CURLOPT_POST => 1,
	                	CURLOPT_HTTPHEADER => $headers,
	                	CURLOPT_POSTFIELDS => $Invoice,
	                	CURLOPT_RETURNTRANSFER => true
	            	));
	        $customer_payment = curl_exec($curl);
	        $respo = json_decode($customer_payment);
	        if(!isset($respo->payment)){
	        		$email_values = array(
									'mail_type'=>'html',
	             					'from_mail_id'=>'hello@cityfurnish.com',
	             					'mail_name'=>'Zoho Error Mail',
	             					'to_mail_id'=>'naresh.suthar@agileinfoways.com',
	             					'subject_message'=>'Customer Payment Error',
	             					'body_messages'=> $customer_payment
								);
				$email_send_to_common = $this->order_model->common_email_send($email_values);
	        }
    }

    public function CreateCustomerPayment($contact_id,$order_id,$Invoice_id,$total ){
    	  	$Curl = "https://books.zoho.com/api/v3/customerpayments?organization_id=".BOOKS_ORGANIZATION_ID;
    	  	$Invoice = 'JSONString={
    	  		   "customer_id": '.$contact_id.',
					"payment_mode": "Payment Gateway",
					"amount": '.$total.',
					"date": "'.date('Y-m-d').'",
				    "reference_number": "#'.$order_id.'",
				    "deposit_to":"PayU",
					"description": "Payment has been added to '.$order_id.'",
					"invoices": [
					    {
						    "invoice_id": '.$Invoice_id.',
							"amount_applied": '.$total.'
						}
					],
					"amount_applied": '.$total.',
					"tax_amount_withheld": 0
    	  	}';
	         $headers = [
                'Content-Type: application/x-www-form-urlencoded',
                'Authorization:'.BOOKS_AUTH_TOKEN
	            ];
		
			// // 	    // "shipping_charge": '.$product_info[0]['shippingcost'].',
	        $curl = curl_init($Curl);
	            	curl_setopt_array($curl, array(
	                	CURLOPT_POST => 1,
	                	CURLOPT_HTTPHEADER => $headers,
	                	CURLOPT_POSTFIELDS => $Invoice,
	                	CURLOPT_RETURNTRANSFER => true
	            	));
	        $customer_payment = curl_exec($curl);
	        $respo = json_decode($customer_payment);
	        if(!isset($respo->payment)){
	        		$email_values = array(
									'mail_type'=>'html',
	             					'from_mail_id'=>'hello@cityfurnish.com',
	             					'mail_name'=>'Zoho Error Mail',
	             					'to_mail_id'=>'naresh.suthar@agileinfoways.com',
	             					'subject_message'=>'Customer Payment Error',
	             					'body_messages'=> $customer_payment
								);
				$email_send_to_common = $this->order_model->common_email_send($email_values);
	        }
    }

    public function MarkCutomPayment($contact_id,$Invoice_id,$total){
		$Curl = "https://books.zoho.com/api/v3/customerpayments?organization_id=".BOOKS_ORGANIZATION_ID;
	  	$Invoice = 'JSONString={
	  		   	"customer_id": '.$contact_id.',
				"payment_mode": "Payment Gateway",
				"amount": '.$total.',
				"date": "'.date('Y-m-d').'",
				"reference_number": "#'.rand(00000,99999).'",
				"invoice_id":'.$Invoice_id.',
				"deposit_to":"PayU"

	  	}';
         $headers = [
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization:'.BOOKS_AUTH_TOKEN
            ];
	
        $curl = curl_init($Curl);
            	curl_setopt_array($curl, array(
                	CURLOPT_POST => 1,
                	CURLOPT_HTTPHEADER => $headers,
                	CURLOPT_POSTFIELDS => $Invoice,
                	CURLOPT_RETURNTRANSFER => true
            	));
        $customer_payment = curl_exec($curl);
        $respo = json_decode($customer_payment);
    }

    public function check_sku($sku,$product_name,$product_id, $count = 0){
    	$url_pro =  "https://books.zoho.com/api/v3/items?organization_id=". BOOKS_ORGANIZATION_ID ."&search_text=".$sku."";
        $headers = [
                    'Content-Type: application/json;charset=UTF-8',
                    'Authorization:'.BOOKS_AUTH_TOKEN
        ];
        $curl = curl_init($url_pro);
				curl_setopt_array($curl, array(
        		CURLOPT_RETURNTRANSFER => true,
        		CURLOPT_HTTPHEADER => $headers
	    ));
	        $result = curl_exec($curl);
	        $product_info_data =  json_decode($result, TRUE);
        if(array_key_exists("item_id", $product_info_data['items'][0])){
            return $product_info_data['items'][0];
        }
        else{
        	$failed_msg = $product_name.' is not found in zoho items where item id is '.$product_id.' and sku is'.$sku.' delay '.$count;
			$email_values = array(
								'mail_type'=>'html',
             					'from_mail_id'=>'hello@cityfurnish.com',
             					'mail_name'=>'Zoho Error Mail',
             					'to_mail_id'=>'naresh.suthar@agileinfoways.com',
             					'subject_message'=>'Zoho Case Error',
             					'body_messages'=>$failed_msg
							);
			$email_send_to_common = $this->order_model->common_email_send($email_values);
			return null;
    	}

    }


    public function get_invoice($PrdList)
	{
        $redoliveAdd = $this->user_model->get_all_details(USERS, array( 'full_name' => 'RedOlive'));
		$shipAddRess = $this->user_model->get_all_details(SHIPPING_ADDRESS,array( 'id' => $PrdList->row()->shippingid ));
		if($shipAddRess->row()->address2!="")
		{
			$shipAddress2 = stripslashes($shipAddRess->row()->address2) . ", ";
		}
		else
		{
			$shipAddress2 = "";
		}
		$disTotal =0; 
		$grantTotal = 0;
		$products = "";
		foreach ($PrdList->result() as $cartRow) 
		{ 
			$InvImg = @explode(',',$cartRow->image);
			$unitPrice = ($cartRow->price*(0.01*$cartRow->product_tax_cost))+$cartRow->price;
			$unitDeposit =  $cartRow->product_shipping_cost;
			$grandDeposit = $grandDeposit + $unitDeposit;
			$uTot = ($unitPrice + $unitDeposit)*$cartRow->quantity;
			if($cartRow->attr_name != '' || $cartRow->attr_type != '')
			{ 
			  $atr = '<br>'.$cartRow->attr_type.' / '.$cartRow->attr_name; 
			}
			else
			{ 
			  $atr = '';
			}
			$products.='<tr>
					<td style="border-right:1px solid #cecece; text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;"><img src="'.base_url().PRODUCTPATH.$InvImg[0].'" alt="'.stripslashes($cartRow->product_name).'" width="70" /></span></td>
					<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.stripslashes($cartRow->sku).'</span></td>
					<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style=" font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.stripslashes($cartRow->product_name).$atr.'</span></td>
					<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.strtoupper($cartRow->quantity).'</span></td>
					<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$this->data['currencySymbol'].number_format($unitDeposit,2,'.','').'</span></td>
					<td style="border-right:1px solid #cecece;text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$this->data['currencySymbol'].number_format($unitPrice,2,'.','').'</span></td>
					<td style="text-align:center;border-top:1px solid #cecece;"><span style="font-family:Arial, Helvetica, sans-serif; font-weight:normal; color:#000000; line-height:30px;  text-align:center;">'.$this->data['currencySymbol'].number_format($uTot,2,'.','').'</span></td>
				</tr>';
			$grantTotal = $grantTotal + $uTot;
		}
		$private_total = $grantTotal - $PrdList->row()->discountAmount;
		$private_total = $private_total + $PrdList->row()->tax;
		$private_total = $private_total - $PrdList->row()->discount_on_si;
		if($PrdList->row()->note !='')
		{
			$note ='<tr><td colspan="2">
						   <table cellpadding="3">
							   <tr>
								  <td><span>Note:</span></td>               
							   </tr>
					   <tr>
								  <td style="border:1px solid #cecece;"><span>'.stripslashes($PrdList->row()->note).'</span></td>
							   </tr>
							</table>
						 </td>
					 </tr>
					 <tr><td colspan="2">&nbsp;</td></tr>';
		}
		else
		{
			 $note = '';
		}
		if($PrdList->row()->order_gift == 1)
		{
			$gift ='<tr><td colspan="2">
					 <table cellpadding="3">
						<tr>
						   <td style="border:1px solid #cecece;"><span style="font-weight:bold; font-family:Arial, Helvetica, sans-serif; text-align:center; color:#000000; margin:10px;">This Order is a gift</span></td>
						</tr>
					 </table></td></tr>
				   <tr><td colspan="2">&nbsp;</td></tr>';
		}
		else
		{
			 $gift = '';
		}

		$message = '
			<html>
			<head>
			<style>
			 .table1 {
			    
			    border: 1px solid black;
			}
			 .items th{
			    background-color: #f3f3f3;
			    border: 1px solid #cecece;
			}
			 .items td{
			border: 1px solid #cecece;
			}
			</style>
			</head>
			<body>
			<table class="table1" cellpadding="3">
			<tr>
			   <td width="30%" height="43px;"><img src="'.CDN_URL.'/images/logo-stick.png" alt="'.$this->data['WebsiteTitle'].'" title="'.$this->data['WebsiteTitle'].'" style="height:43px;"></td>
			   <td width="70%" height="80px;">
			       <table style="font-size:12px;" cellpadding="3">
			        	<tr>
			        		<td align="right">Merchant Name :&nbsp;&nbsp;</td>
			        		<td align="left">'.stripslashes($PrdList->row()->full_name).'</td>
			        	</tr>
			        	<tr>
			        		<td align="right">TIN / VAT / CST No. :&nbsp;&nbsp;</td>
			        		<td align="left">'.stripslashes($PrdList->row()->s_tin_no).'/'.stripslashes($PrdList->row()->s_vat_no).'/'.stripslashes($PrdList->row()->s_cst_no).'</td>
			        	</tr>
			        	
			    	</table>
			   </td>
			</tr>
			<tr style="border-bottom:solid 1px;"><td colspan="2"><hr></td></tr>
			<tr>
			   <td width="50%"></td>
			   <td width="50%" align="right">
			        <table width="100%" style="border:1px solid #cecece;font-size:11px;" cellspacing="3" cellpadding="3">
				    <tr bgcolor="#f3f3f3">
			            	<td style="border-right:1px solid #cecece;" width="30%"><b>Order Id</b></td>
				        <td width="70%"><span>#'.$PrdList->row()->dealCodeNumber.'</span></td>
				    </tr>
				    <tr bgcolor="#f3f3f3">
			                <td style="border-right:1px solid #cecece;" width="30%"><b>Order Date</b></td>
			                <td width="70%"><span>'.date("F j, Y g:i a",strtotime($PrdList->row()->created)).'</span></td>
			            </tr>	 
			        </table>
			   </td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<tr>
			   <td>
			        <table style="font-size:11px;" cellpadding="5">
			    	   <tr><td colspan="2" style="background-color:#f3f3f3;border:1px solid #cecece;">Delivery Address</td></tr>
			        </table>
			        <table style="border:1px solid #cecece;font-size:11px;" cellpadding="3">
			           <tr>
			               <td width="40%">Full Name</td>
			               <td width="10%">:</td>
			               <td width="50%" align="left">'.stripslashes($shipAddRess->row()->full_name).'</td>
			           </tr>
			           <tr>
			               <td width="40%">Address</td>
			               <td width="10%">:</td>
			               <td width="50%" align="left">'.stripslashes($shipAddRess->row()->address1).', '.$shipAddress2.stripslashes($shipAddRess->row()->city).', '.stripslashes($shipAddRess->row()->state).', '.stripslashes($shipAddRess->row()->postal_code).', '.stripslashes($shipAddRess->row()->country).'</td>
			           </tr>
			           <tr>
			               <td width="40%">Phone Number</td>
			               <td width="10%">:</td>
			               <td width="50%" align="left">'.stripslashes($shipAddRess->row()->phone).'</td>
			           </tr>            	
			        </table>
			   </td>
			   <td>
			        <table style="font-size:11px;" cellpadding="5">
			    	   <tr><td colspan="2" style="background-color:#f3f3f3;border: 1px solid #cecece;">From :</td></tr>
			        </table>
			        <table style="border:1px solid #cecece;font-size:11px;" cellpadding="3">
			           <tr>
			               <td width="30%">Full Name</td>
			               <td width="5%">:</td>
			               <td width="65%" align="left">'.stripslashes($PrdList->row()->full_name).'</td>
			           </tr>
			           <tr>
			               <td width="30%">Address</td>
			               <td width="5%">:</td>
			               <td width="65%" align="left">'.stripslashes($PrdList->row()->address).','.stripslashes($PrdList->row()->address2).','.stripslashes($PrdList->row()->city).','.stripslashes($PrdList->row()->state).','.stripslashes($PrdList->row()->postal_code).','.stripslashes($PrdList->row()->country).'</td>
			           </tr>                      	
			        </table>
			   </td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<tr>
			   <td colspan="2">
			        <table class="items" cellpadding="5" style="font-size:9px;border: 1px solid #cecece;">
			           <tr style="font-size:10px;">
			               <th width="20%">Product Image</th>
			               <th width="12.25%">SKU Code</th>
			               <th width="23%">Product Name</th>
			               <th width="7%">Qty</th>
			               <th width="12.25%">Deposit</th>
			               <th width="12.25%">Rental</th>
			               <th width="12.25%">Sub Total</th>
			           </tr>
			           '.$products.'
			        </table>
			   </td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			'.$note.'
			'.$gift.'
			<tr>
			   <td width="40%">&nbsp;</td>
			   <td width="60%">
			     <table class="items" style="border:1px solid #cecece;" cellpadding="5">
				<tr bgcolor="#f3f3f3">
					<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Sub Total</b></td>
					<td style="border-bottom:1px solid #cecece;"><span>'.$this->data['currencySymbol'].number_format($grantTotal,'2','.','').'</span></td>
				</tr>
				<tr>
					<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Discount Amount</b></td>
					<td style="border-bottom:1px solid #cecece;"><span>'.$this->data['currencySymbol'].number_format($PrdList->row()->discountAmount,'2','.','').'</span></td>
				</tr>

				<tr>
					<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Discount For SI Amount</b></td>
					<td style="border-bottom:1px solid #cecece;"><span>'.$this->data['currencySymbol'].number_format($PrdList->row()->discount_on_si,'2','.','').'</span></td>
				</tr>

				<tr bgcolor="#f3f3f3">
					<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Deposit</b></td>
					<td style="border-bottom:1px solid #cecece;"><span>'.$this->data['currencySymbol'].number_format($grandDeposit,2,'.','').'</span></td>
				</tr>
				<tr>
					<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Shipping Tax</b></td>
					<td style="border-bottom:1px solid #cecece;"><span>'.$this->data['currencySymbol'].number_format($PrdList->row()->tax ,2,'.','').'</span></td>
				</tr>
				<tr bgcolor="#f3f3f3">
					<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Grand Total</b></td>
					<td style="border-bottom:1px solid #cecece;"><span>'.$this->data['currencySymbol'].number_format($private_total,'2','.','').'</span></td>
				</tr>
				<tr>
					<td style="border-right:1px solid #cecece;border-bottom:1px solid #cecece;"><b>Amount Paid</b></td>
					<td style="border-bottom:1px solid #cecece;"><b>'.$this->data['currencySymbol'].number_format($private_total,'2','.','').'**</b></td>
				</tr>
			      </table>
			   </td>
			</tr>
			<tr><td colspan="2">&nbsp;</td></tr>
			<tr><td colspan="2" align="center"><p>Have a question? Our customer service is here to help you - 080-66084700</p></td></tr>

			</table>
			</body></html>';
        return $message;
	}


	public function show_success(){
			$this->data['Confirmation'] = 'Success';				
			$this->load->view('site/order/offline_order.php',$this->data);
	}




}