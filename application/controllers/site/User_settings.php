<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * User Settings related functions
 * @author Teamtweaks
 *
 */

class User_settings extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encryption','form_validation','pagination','S3_upload','S3'));
		$this->load->model('user_model');
		$this->load->model('seller_location_model');
		$this->load->model('commission_model','commission');

		if($_SESSION['sMainCategories'] == ''){
			$sortArr1 = array('field'=>'cat_position','type'=>'asc');
			$sortArr = array($sortArr1);
			$_SESSION['sMainCategories'] = $this->user_model->get_all_details(CATEGORY,array('rootID'=>'0','status'=>'Active'),$sortArr);
		}
		$this->data['mainCategories'] = $_SESSION['sMainCategories'];

		if($_SESSION['sColorLists'] == ''){
			$_SESSION['sColorLists'] = $this->user_model->get_all_details(LIST_VALUES,array('list_id'=>'1'));
		}
		$this->data['mainColorLists'] = $_SESSION['sColorLists'];

		$this->data['loginCheck'] = $this->checkLogin('U');
	}

	public function index(){
	   // print_r("fdff");exit;
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Settings';
			$this->data['locations'] = $this->seller_location_model->get_sellerlocation_details();
			$this->load->view('site/user/settings',$this->data);
		}
	}

	public function update_profile(){
		$inputArr = array();
		$response['success'] = '0';
		if ($this->checkLogin('U') == ''){
			$response['msg'] = 'You must login';
		}else {
			$update = '0';
			$email = $this->input->post('email');
			$name = $this->input->post('full_name');
			$phone_no = $this->input->post('phone_no');
			if ($email!=''){
				if (filter_var($email)){
					$condition = array('email'=>$email,'id !='=>$this->checkLogin('U'));
					$duplicateMail = $this->user_model->get_all_details(USERS,$condition);
					if ($duplicateMail->num_rows()>0){
						$response['msg'] = 'Email already exists';
					}else {
				// 		$condition = array('phone_no'=>$phone_no,'id !=' => $this->checkLogin('U'));
				// 		$duplicateNumber = $this->user_model->get_all_details(USERS,$condition);
				// 		// print_r($duplicateNumber->row());exit;
				// 		if($duplicateNumber->num_rows()>0){
				// 			$response['msg'] = 'Number already exists';
				// 		}else{
							$condition = array('phone_no'=>$phone_no,'id' => $this->checkLogin('U'));
							$number_exsits = $this->user_model->get_all_details(USERS,$condition);
							if($number_exsits->num_rows() < 1) {
	               				$inputArr['is_mobile_verified'] = 'No';
							}
								$inputArr['email'] = $email;
								$update = '1';
				// 		}
					}
				}else {
					$response['msg'] = 'Invalid email';
				}
			}else {
				$update = '1';
			}
			if ($update == '1'){
				$birthday = $this->input->post('b_year').'-'.$this->input->post('b_month').'-'.$this->input->post('b_day');
				$excludeArr = array('b_year','b_month','b_day','email');
				$inputArr['birthday'] = $birthday;
				$condition = array('id'=>$this->checkLogin('U'));
				$this->user_model->commonInsertUpdate(USERS,'update',$excludeArr,$inputArr,$condition);
				if($this->lang->line('prof_looks_better') != '')
				$lg_err_msg = $this->lang->line('prof_looks_better');
				else
				$lg_err_msg = 'Done ! Your profile looks even better now';
				$this->setErrorMessage('success',$lg_err_msg);
				$response['success'] = '1';
			}
		}
		echo json_encode($response);
	}

	public function changePhoto(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$config['overwrite'] = FALSE;
			$config['remove_spaces'] = TRUE;
			$config['allowed_types'] = 'jpg|jpeg|gif|png';
			$config['max_size'] = 20000;
			$config['max_width']  = '1200';
			$config['max_height']  = '1200';
			$config['upload_path'] = './images/users';
			$this->load->library('upload', $config);
			if ( $this->upload->do_upload('upload-file')){
				$imgDetails = $this->upload->data();
				$dataArr['thumbnail'] = $imgDetails['file_name'];
				$condition = array('id'=>$this->checkLogin('U'));
				$this->user_model->update_details(USERS,$dataArr,$condition);
				redirect('image-crop/'.$this->checkLogin('U'));
			}else if ( $this->upload->do_upload('docone-file')){
				$imgDetails = $this->upload->data();
				$dataArr['doc1'] = $imgDetails['file_name'];
				$condition = array('id'=>$this->checkLogin('U'));
				$this->user_model->update_details(USERS,$dataArr,$condition);
				redirect(base_url().'settings');
			}else if ( $this->upload->do_upload('docsecond-file')){
				$imgDetails = $this->upload->data();
				$dataArr['doc2'] = $imgDetails['file_name'];
				$condition = array('id'=>$this->checkLogin('U'));
				$this->user_model->update_details(USERS,$dataArr,$condition);
				redirect(base_url().'settings');
			}else if ( $this->upload->do_upload('docthird-file')){
				$imgDetails = $this->upload->data();
				$dataArr['doc3'] = $imgDetails['file_name'];
				$condition = array('id'=>$this->checkLogin('U'));
				$this->user_model->update_details(USERS,$dataArr,$condition);
				redirect(base_url().'settings');
			}else {
				$this->setErrorMessage('error',strip_tags($this->upload->display_errors()));
			}
			redirect(base_url().'settings');
		}
	}

	public function delete_user_photo(){
		$response['success'] = '0';
		if ($this->checkLogin('U')==''){
			$response['msg'] = 'You must login';
		}else {
			$condition = array('id'=>$this->checkLogin('U'));
			$dataArr = array('thumbnail'=>'');
			$this->user_model->update_details(USERS,$dataArr,$condition);
			if($this->lang->line('prof_photo_del') != '')
			$lg_err_msg = $this->lang->line('prof_photo_del');
			else
			$lg_err_msg = 'Profile photo deleted successfully';
			$this->setErrorMessage('success',$lg_err_msg);
			$response['success'] = '1';
		}
		echo json_encode($response);
	}

	public function delete_user_account(){
		if ($this->checkLogin('U')!=''){
			$datestring = "%Y-%m-%d %h:%i:%s";
			$time = time();
			$newdata = array(
	               'last_logout_date' => mdate($datestring,$time),
				   'status'=>'Inactive'
				   );
				   $condition = array('id' => $this->checkLogin('U'));
				   $this->user_model->update_details(USERS,$newdata,$condition);
				   $userdata = array(
							'fc_session_user_id'=>'',
							'session_user_name'=>'',
							'session_user_email'=>'',
							'fc_session_temp_id'=>''
							);
							$this->session->set_userdata($userdata);
							if($this->lang->line('prof_inact_succ') != '')
							$lg_err_msg = $this->lang->line('prof_inact_succ');
							else
							$lg_err_msg = 'Your account inactivated successfully';
							$this->setErrorMessage('success',$lg_err_msg);
		}
	}

	public function password_settings(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Password Settings';
			$this->load->view('site/user/changepassword',$this->data);
		}
	}

	public function change_user_password(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$pwd = trim($this->input->post('pass'));
			$cfmpwd = trim($this->input->post('confirmpass'));

			if ($pwd != '' && $cfmpwd != '' && strlen($pwd) > 5){
				if ($pwd == $cfmpwd){
					$dataArr = array('password'=>md5($pwd));
					$condition = array('id'=>$this->checkLogin('U'));
					$this->user_model->update_details(USERS,$dataArr,$condition);
					if($this->lang->line('pwd_cge_succ') != '')
					$lg_err_msg = $this->lang->line('pwd_cge_succ');
					else
					$lg_err_msg = 'Password changed successfully';
					$this->setErrorMessage('success',$lg_err_msg);
				}else {
					if($this->lang->line('pwd_donot_match') != '')
					$lg_err_msg = $this->lang->line('pwd_donot_match');
					else
					$lg_err_msg = 'Passwords does not match';
					$this->setErrorMessage('error',$lg_err_msg);
				}
			}else {
				if($this->lang->line('pwd_cfm_not_match') != '')
				$lg_err_msg = $this->lang->line('pwd_cfm_not_match');
				else
				$lg_err_msg = 'Password and Confirm password fields required';
				$this->setErrorMessage('error',$lg_err_msg);
			}
			redirect(base_url().'settings');
		}
	}

	public function preferences_settings(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Preference Settings';
			$this->data['languages'] = $this->user_model->get_all_details(LANGUAGES,array());
			$this->load->view('site/user/change_preferences',$this->data);
		}
	}

	public function update_preferences(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->user_model->commonInsertUpdate(USERS,'update',array(),array(),array('id'=>$this->checkLogin('U')));
			if($this->lang->line('pref_sav_succ') != '')
			$lg_err_msg = $this->lang->line('pref_sav_succ');
			else
			$lg_err_msg = 'Preferences saved successfully';
			$this->setErrorMessage('success',$lg_err_msg);
			redirect(base_url().'settings/preferences');
		}
	}

	public function notifications_settings(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Notifications Settings';
			$this->data['languages'] = $this->user_model->get_all_details(LANGUAGES,array());
			$this->load->view('site/user/change_notifications',$this->data);
		}
	}

	public function update_notifications(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$emailArr = $this->data['emailArr'];
			$notyArr = $this->data['notyArr'];
			$emailStr = '';
			$notyStr = '';
			foreach ($this->input->post() as $key=>$val){
				if (in_array($key, $emailArr)){
					$emailStr .= $key.',';
				}else if (in_array($key, $notyArr)){
					$notyStr .= $key.',';
				}
			}
			$updates = $this->input->post('updates');
			$updates = ($updates == '')?'0':'1';
			$emailStr = substr($emailStr, 0,strlen($emailStr)-1);
			$notyStr = substr($notyStr, 0,strlen($notyStr)-1);
			$dataArr = array(
	    		'email_notifications'	=>	$emailStr,
	    		'notifications'			=>	$notyStr,
	    		'updates'				=>	$updates
			);
			$condition = array('id'=>$this->checkLogin('U'));
			$this->user_model->update_details(USERS,$dataArr,$condition);
			if ($updates == 1){
				$checkEmail = $this->user_model->get_all_details(SUBSCRIBERS_LIST,array('subscrip_mail'=>$this->data['userDetails']->row()->email));
				if ($checkEmail->num_rows()==0){
					$this->user_model->simple_insert(SUBSCRIBERS_LIST,array('subscrip_mail'=>$this->data['userDetails']->row()->email,'active'=>1,'status'=>'Active'));
				}
			}else {
				$this->user_model->commonDelete(SUBSCRIBERS_LIST,array('subscrip_mail'=>$this->data['userDetails']->row()->email));
			}
			if($this->lang->line('noty_sav_succ') != '')
			$lg_err_msg = $this->lang->line('noty_sav_succ');
			else
			$lg_err_msg = 'Notifications settings saved successfully';
			$this->setErrorMessage('success',$lg_err_msg);
			redirect(base_url().'settings/notifications');
		}
	}

	public function user_purchases(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			unset($_SESSION['OrderID']);
			unset($_SESSION['OrderAmount']);
			$userDetails =$this->user_model->get_all_details(USERS,array('id' => $this->checkLogin('U')));
			if($userDetails->row()->is_offline_user == 1){
				$this->data['heading'] = 'Purchases';
				$this->data['purchasesList'] = $this->user_model->get_offline_purchase_details($this->checkLogin('U'));
				$this->load->view('site/user/user_offline_purchases',$this->data);
			}else{
				$this->data['heading'] = 'Purchases';
				$this->data['purchasesList'] = $this->user_model->get_purchase_details($this->checkLogin('U'));
				$this->load->view('site/user/user_purchases',$this->data);
			}
		}
	}

	
	public function user_documentation(){
		if ($this->checkLogin('U')==''){
			redirect(base_url());
		}else {
		    $checkcrif_score = $this->user_model->get_cibil_score($this->checkLogin('U'));
		  //  $checkcrif_score = $this->user_model->get_all_details(CRIF_SCORE,array('user_id' =>$this->checkLogin('U')));
			if($checkcrif_score->num_rows() == 0){
				$checkcrif_stage = $this->user_model->get_all_details(CRIF_REQUEST,array('order_id' => $_SESSION['ORDERID'],'user_id' =>$this->checkLogin('U')));
				$this->data['crifData'] = ''; 
				if($checkcrif_stage->num_rows() > 0){
				 	$response = json_decode($checkcrif_stage->row()->response);
				 	$this->data['encode'] = $checkcrif_stage->row()->encode;
			        if($response->status == 'S06'){
			        	$data = $this->authenticate_crif($response,$checkcrif_stage->row()->encode);
			        	if(array_key_exists('question', $data) && !empty($data)){
			        		$RadioButtons = '';
			        		foreach ($data->optionsList as $key => $value) {
			        			$RadioButtons .= '<div class="radiocol"><input type="radio" name="answeroption" value="'.$value.'" id="answer_'.$key.'"> <label for="answer_'.$key.'">'.$value.' </label></div><br>';
			        		}
			        		$newHtml = '<form class="credit-form flex-full form-group"><div class="FlowupLabels "><div class="fl_wrap">'.$data->question.'.? </div></div> '.$RadioButtons.'<div class="not-found-block flex-full justify-content-center" style="justify-content: center;text-align: center;"> <a href="javacript:void(0)" onclick=AuthenticateCrifQuestions(\''.$data->reportId.'\',\''. $data->orderId .'\') class="btn btn-danger explore-btn proceed">Proceed</a></div></form>';
	                        $this->data['crifData'] = $newHtml; 
			        	}else{
			        		$data = $this->get_creditxmlandsave($response,$checkcrif_stage->row()->encode);
			        		$xmlcont = new SimpleXMLElement($data);
							$CheckXml = (array) $xmlcont;
							if(array_key_exists('SCORES', $CheckXml)){
								if(array_key_exists('SCORE', $CheckXml['SCORES'])){
									$toArray = (array) $CheckXml['SCORES']->SCORE;
									$text = "Your Credit Score is :".$toArray['SCORE-VALUE']."<br>".$toArray['SCORE-COMMENTS'];
									$this->data['crifData'] = $text;				
								}else{
									$this->data['crifData'] = 'No Cibil Score Found';
								}
							}else{
								$this->data['crifData'] = 'No Cibil Score Found';
							}
			        	}
			        }
				}
			}else{
				$xmlcont = new SimpleXMLElement($checkcrif_score->row()->crif_xml);
				$CheckXml = (array) $xmlcont;
				if(array_key_exists('SCORES', $CheckXml)){
					if(array_key_exists('SCORE', $CheckXml['SCORES'])){
						$toArray = (array) $CheckXml['SCORES']->SCORE;
						$text = "Your Credit Score is :".$toArray['SCORE-VALUE']."<br>".$toArray['SCORE-COMMENTS'];
						$this->data['scoreData'] = $text;				
					}else{
						$this->data['scoreData'] = 'No Cibil Score Found';
					}
				}else{
					$this->data['scoreData'] = 'No Cibil Score Found';
				}
			}

			$this->data['get_my_orders'] = $this->user_model->get_past_orderDetails($this->checkLogin('U'));
			if($toArray['SCORE-VALUE'] != ''){
				$cibil_score = $toArray['SCORE-VALUE'];				
			}else{
				$cibil_score = 0;
			}
			$DocID = $this->user_model->get_my_cibil_docs($cibil_score);
			$this->data['cibil_docs'] = $this->user_model->get_cibil_docs($DocID->row());
			$this->load->view('site/user/documentation',$this->data);
		}
	}
	
    public function initiateCrifRequest(){
		if ($this->checkLogin('U')==''){
			$response = array('status' => 'error','code' => 401,'message' => 'Invalid Access');
			header('Content-Type: application/json');
			echo json_encode($response);exit;
		}else {	
			$userDetails = $this->user_model->get_all_details(USERS,array('id' => $this->checkLogin('U')));
			$row = $userDetails->row();

			$pandCardNumber = $this->input->post('pan_number');
			$voterID = $this->input->post('voter_id');
			$date = $this->input->post('dob');
			$dob = date("d-m-Y", strtotime($dob));

			$url = 'https://test.crifhighmark.com/Inquiry/do.getSecureService/DTC/initiate';
			$Code = 'chm_bbc_uat@cityfurnish.com|DTC0000051|BBC_CONSUMER_SCORE#85#2.0|0EBBF01B8D92A28B74322D8C1899AFEF0D32DB76|'.date('Y-m-d h:i:s');

			$encode = base64_encode($Code);
			$name = explode(' ', $row->full_name);
			if(!array_key_exists(1, $name)){
				$name[1] = 'kumar';
			}
			// $request = ''.$name[0].'||'.$name[1].'||'.$dob.'|||'.$row->phone_no.'|||'.$row->email.'||'.$pandCardNumber.'|ZNPEKW71699719|'.$voterID.'|||||||||'.$row->address.'|'.$row->city.'|'.$row->city.'|'.$row->state.'|'.$row->postal_code.'|india|||||||DTC0000051|BBC_CONSUMER_SCORE#85#2.0|Y|';
			$request = 'SRIVATSA||RAJAGOPAL||28-09-1957|||9826177970|||abc@abc.com||IBAPP9502R|ZNPEKW71699719|||||||NA|||NO B-144,  TARA MARG,  HANUMAN N NULL|COLABA|COLABA|RJ|400053|india|||||||DTC0000051|BBC_CONSUMER_SCORE#85#2.0|Y|';
   	 		$headers = [
                    'orderId:'.mt_rand(),
                    'accessCode:'.$encode,
                    'appID:yC@$1R!)N2o!i&B&!(U^@fr!',
                    'merchantID:DTC0000051',
                    'Content-Type: text/plain'
            ];
	        $curl = curl_init($url);
	        		curl_setopt_array($curl, array(
	            		CURLOPT_RETURNTRANSFER => true,
	            		CURLOPT_POSTFIELDS =>$request,
	            		CURLOPT_POST => 1,
	            		CURLOPT_HTTPHEADER => $headers
	        		));
	        $result = curl_exec($curl);
	        $response = json_decode($result);
	        $_SESSION['ORDERID'] = $response->orderId;
	        if($response->status == 'S06'){
	        	$this->user_model->simple_insert(CRIF_REQUEST,array('user_id' => $this->checkLogin('U'),'order_id' => $response->orderId,'report_id' => $response->reportId,'encode' => $encode,'stage' => '1','url' => $url,'response' => $result ));
	        	$data = $this->authenticate_crif($response,$encode);
	        	if(array_key_exists('question', $data)){
	        		$returnArray = array('type' => 'questions','status' => 200,'data'=> $data,'encode' => $encode);
		        	header('Content-Type: application/json');
					echo json_encode($returnArray);exit;
	        	}else{
	        		$data = $this->get_creditxmlandsave($data,$encode);
	        		$xmlcont = new SimpleXMLElement($result);

	        		$returnArray = array('type' => 'normal','status' => 200,'data'=> $data);
	        		header('Content-Type: application/json');
					echo json_encode($returnArray);exit;
	        	}
	        }
		}	
	}

	public function savecibildocs(){
		if ($this->checkLogin('U')==''){
			redirect(base_url());
		}else {	
			$linkedin_url = $this->input->post('linkdin_profile_url');
			$special_remarks = $this->input->post('special_remarks');
			$order_id = $this->input->post('order_id');
			$this->createZohocase_cibil($this->checkLogin('U'));
            $allowed_types = "gif|jpg|png|jpeg";
            $checkRecordExists = $this->user_model->get_all_details(USER_UPLOADED_DOCS,array('order_id' => $order_id,'user_id' => $this->checkLogin('U') ));
            if($checkRecordExists->num_rows() > 0){
            	$this->user_model->commonDelete(USER_UPLOADED_DOCS,array('order_id' => $order_id,'user_id' => $this->checkLogin('U')));
            }
            $file_names = array_keys($_FILES);
            foreach ($_FILES as $key => $value) {
            	if($_FILES[$key]['name'][0] != ''){
            		$upload_path = "images/cibil_docments/".$key;
	        		$file = $this->s3_upload->upload_file($key, $upload_path,$allowed_types);
	        		if(array_key_exists('success', $file)){
	        			$all_doc =  $file['upload_data']['file_name'];
	        			$this->user_model->simple_insert(USER_UPLOADED_DOCS,array('user_id' => $this->checkLogin('U'),'doc_name' =>	$all_doc,'doc_type' => $key,'linkedin_url' => $linkedin_url,'special_remarks' => $special_remarks,'order_id' => $order_id));
	        		}
            	}
            }
			redirect(base_url().'documentation');
		}
	}

	public function createZohocase_cibil($user_id){
		$acceess_token = $this->get_zohaccess_token();
   		$headers = [
   		    'Content-Type:application/json',
            'Authorization:'.$acceess_token
        ];

        $ContactID = $this->get_customer_id($user_id);

		// $createCaseURL = 'https://www.zohoapis.com/crm/v2/Cases';	
  //       $CreateCaseJsonData =   '{
  //       "data" : [
		//        	{
	 //       		"Case_Origin":"Website",
	 //       		"Subject":"New Order - '.$currentSuccessOrders[0]['ship_full_name'].' ('.$currentSuccessOrders[0]['email'].')",
	 //       		"Status":"New Order",
	 //       	    "Sub_Status":"KYC In Progress",
	 //       		"Owner":"649600348",
	 //       		"Recurring_Opted":"'.$Possible_Values_recurring.'",
	 //       		"Mandate_ID":"'.$currentSuccessOrders[0]['mandate_id'].'",
	 //       		"Order_ID":"'.$currentSuccessOrders[0]['dealCodeNumber'].'",
	 //       		"Type":"Order",
	 //       		"Sub_Type":"New - Rental",
	 //       		"Coupon_Code":"'.$couponcode.'",
	 //       		"Related_To":"'.$relatedTo.'",
	 //       		"Description":"'.$description.'",
	 //       		"City":"'.$currentSuccessOrders[0]['ship_city'].'",
	 //       		"Account_Name" :"'.$accountName.'",
	 //       		"LOB":"B2C",
	 //       		"Email":"'.$currentSuccessOrders[0]['email'].'",
	 //       		"Phone":"'.$currentSuccessOrders[0]['ship_phone'].'",
	 //       		"Invoice_Url":"'.base_url().'uploaded/'.$pdfFileName.'",
		//         }
		//     ]
		// }';
	 //    $curl = curl_init($createCaseURL);
		// 	curl_setopt_array($curl, array(
		// 		CURLOPT_POST => 1,
  //           	CURLOPT_HTTPHEADER => $headers,
  //           	CURLOPT_POSTFIELDS => $CreateCaseJsonData,
  //           	CURLOPT_RETURNTRANSFER => true
		// 	));
		// $jsonCaseRes = curl_exec($curl);
  //   	$createCaseResultArray = json_decode($jsonCaseRes);
	}

	public function verifycrifanswer(){
		$url = 'https://test.crifhighmark.com/Inquiry/do.getSecureService/DTC/response'; 		
		$request = ''.$this->input->post('order_id').'|'.$this->input->post('report_id').'|'.$this->input->post('encode').'|http://45.113.122.221/crone/catch_all_crif_response|N|N|Y|'.$this->input->post('answer');
		$headers = [
	        'Content-Type: text/plain',
	        'orderId:'.$this->input->post('order_id'),
	        'accessCode:'.$this->input->post('encode'),
	        'appID:yC@$1R!)N2o!i&B&!(U^@fr!',
	        'merchantID:DTC0000051',
	        'reportId:'.$this->input->post('report_id'),
	        'requestType:Authorization'
	    ];
		$curl = curl_init($url);
			curl_setopt_array($curl, array(
	    		CURLOPT_RETURNTRANSFER => true,
	    		CURLOPT_POSTFIELDS =>$request,
	    		CURLOPT_POST => 1,
	    		CURLOPT_HTTPHEADER => $headers
			));
		$result = curl_exec($curl);
		$response = json_decode($result);
		if($response->status == 'S11'){
			$returnArray = array('type' => 'questions','status' => 200,'data'=> $response);
        	header('Content-Type: application/json');
			echo json_encode($returnArray);exit;
		}else{	
		    if($response->status == 'S01'){
		        	$res= array('status' => 200, 'statusDesc' => $response->statusDesc);
		        	$returnArray = array('type' => 'normal','status' => 200,'data'=> $response);
                	header('Content-Type: application/json');
        			echo json_encode($returnArray);exit;
		    }else{
    			$res= array('status' => 401, 'statusDesc' => 'Authentication was failed due to unsuccesfull all attempt');
    			$returnArray = array('type' => 'normal','status' => 400,'data'=> $res);
    			$this->user_model->commonDelete(CRIF_REQUEST,array('user_id' => $this->checkLogin('U')));
            	header('Content-Type: application/json');
    			echo json_encode($returnArray);exit;
		    }
		}
	}

	public function authenticate_crif($response,$encode){
		$url = 'https://test.crifhighmark.com/Inquiry/do.getSecureService/DTC/response'; 		
		$request = ''.$response->orderId.'|'.$response->reportId.'|'.$encode.'|http://45.113.122.221/crone/catch_all_crif_response|N|N|Y|Null';
		$headers = [
	        'Content-Type: text/plain',
	        'orderId:'.$response->orderId,
	        'accessCode:'.$encode,
	        'appID:yC@$1R!)N2o!i&B&!(U^@fr!',
	        'merchantID:DTC0000051',
	        'reportId:'.$response->reportId,
	        'requestType:Authorization'
	    ];
		$curl = curl_init($url);
			curl_setopt_array($curl, array(
	    		CURLOPT_RETURNTRANSFER => true,
	    		CURLOPT_POSTFIELDS =>$request,
	    		CURLOPT_POST => 1,
	    		CURLOPT_HTTPHEADER => $headers
			));
		$result = curl_exec($curl);
		// print_r($result);exit;
		return json_decode($result);
	}

	public function get_creditxmlandsave($data,$encode){
		$url = 'https://test.crifhighmark.com/Inquiry/do.getSecureService/DTC/response'; 		
		$request = ''.$data->orderId.'|'.$data->reportId.'|'.$encode.'||N|N|N';
		$headers = [
	       	'Content-Type: text/plain',
	        'OrderId:'.$data->orderId,
	        'accesscode:'.$encode,
	        'AppID:yC@$1R!)N2o!i&B&!(U^@fr!',
	        'MerchantID:DTC0000051',
	        'ReportID:'.$data->reportId,
	    ];
		$curl = curl_init($url);
			curl_setopt_array($curl, array(
	    		CURLOPT_RETURNTRANSFER => true,
	    		CURLOPT_POSTFIELDS =>$request,
	    		CURLOPT_POST => 1,
	    		CURLOPT_HTTPHEADER => $headers
			));
		$result = curl_exec($curl);
		if(!empty($result)){
			$xmlcont = new SimpleXMLElement($result);
			$scoreValue = 'SCORE-VALUE';
			$scoreComments = 'SCORE-COMMENTS';
			// print_r($xmlcont->SCORES->SCORE->$scoreValue);
			if(!empty($xmlcont)){
				$checkDuplicate = $this->user_model->get_all_details(CRIF_SCORE,array('user_id' =>$this->checkLogin('U'),'order_id' => $data->orderId ));
				if($checkDuplicate->num_rows() == 0){
					$this->user_model->simple_insert(CRIF_SCORE,array('code' => $encode, 'user_id' =>$this->checkLogin('U') ,'order_id' => $data->orderId,'reportId' => $data->reportId,'credit_score' => $xmlcont->SCORES->SCORE->$scoreValue,'crif_xml' => $result));
				}
			}			
			return $result;
			exit;
		}
	}

	public function user_orders(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Orders';
			$this->data['ordersList'] = $this->user_model->get_user_orders_list($this->checkLogin('U'));
			$this->load->view('site/user/user_orders_list',$this->data);
		}
	}

	public function manage_fancyybox(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Subscriptions';
			$this->data['subscribeList'] = $this->user_model->get_subscriptions_list($this->checkLogin('U'));
			$this->load->view('site/user/manage_fancyybox',$this->data);
		}
	}

	public function shipping_settings(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Shipping Address';
			$this->data['countryList'] = $this->user_model->get_all_details(COUNTRY_LIST,array(),array(array('field'=>'name','type'=>'asc')));
			$this->data['shippingList'] = $this->user_model->get_all_details(SHIPPING_ADDRESS,array('user_id'=>$this->checkLogin('U')));
			$this->data['full_name']  = $this->user_model->get_all_details(USERS,array('id'=>$this->checkLogin('U')));
			$this->data['city_merge'] = $this->user_model->get_city_array();
			$this->load->view('site/user/shipping_settings',$this->data);
		}
	}

	public function insertEdit_shipping_address(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$shipID = $this->input->post('shipID');
			$is_default = $this->input->post('make_default');
			if ($is_default == '0' || $is_default == 0){
				$primary = 'No';
			}else{
				$primary = 'Yes';
			}
			if($primary == 'Yes')
			{
				$uncheckAll = $this->user_model->make_priamry($this->checkLogin('U'));
			}
			$excludeArr = array('shipID','is_default','make_default');
			$dataArr = array('primary'=>$primary);
			$condition = array('id'=>$shipID);
			if ($shipID==''){
				$this->user_model->commonInsertUpdate(SHIPPING_ADDRESS,'insert',$excludeArr,$dataArr,$condition);
				$shipID = $this->user_model->get_last_insert_id();
				if($this->lang->line('ship_add_succ') != '')
				$lg_err_msg = $this->lang->line('ship_add_succ');
				else
				$lg_err_msg = 'Your Shipping address is added successfully !';
				$this->setErrorMessage('success',$lg_err_msg);
				$response = array('status' => 'true', 'message'=> $lg_err_msg);
				 header('Content-Type: application/json');
			    echo json_encode($response);exit;
			}else {
				$this->user_model->commonInsertUpdate(SHIPPING_ADDRESS,'update',$excludeArr,$dataArr,$condition);
				if($this->lang->line('ship_updat_succ') != '')
				$lg_err_msg = $this->lang->line('ship_updat_succ');
				else
				$lg_err_msg = 'Shipping address updated successfully';
				$this->setErrorMessage('success',$lg_err_msg);
				$response = array('status' => 'true','message'=> $lg_err_msg);
				 header('Content-Type: application/json');
			    echo json_encode($response);exit;
			}
			if ($primary == 'Yes'){
				$condition = array('id !='=>$shipID,'user_id'=>$this->checkLogin('U'));
				$dataArr = array('primary'=>'No');
				$this->user_model->update_details(SHIPPING_ADDRESS,$dataArr,$condition);
			}else {
				$condition = array('primary'=>'Yes','user_id'=>$this->checkLogin('U'));
				$checkPrimary = $this->user_model->get_all_details(SHIPPING_ADDRESS,$condition);
				if ($checkPrimary->num_rows()==0){
					$condition = array('id'=>$shipID,'user_id'=>$this->checkLogin('U'));
					$dataArr = array('primary'=>'Yes');
					$this->user_model->update_details(SHIPPING_ADDRESS,$dataArr,$condition);
				}
			}
			redirect(base_url().'settings/shipping');
		}
	}
	
	public function udatesetdefault_shipping(){
	    if ($this->checkLogin('U')!=''){
	        $uncheckAll = $this->user_model->make_priamry($this->checkLogin('U'));
        	$condition = array('id'=>$this->input->post('shipID'),'user_id'=>$this->checkLogin('U'));
			$dataArr = array('primary'=>'Yes');
	       	$this->user_model->update_details(SHIPPING_ADDRESS,$dataArr,$condition);
       		$response = array('status' => 'true','message'=>'Update Success');
		    header('Content-Type: application/json');
    	    echo json_encode($response);exit;
	    }
	}

	public function get_shipping(){
		$shipID = $this->input->post('shipID');
		$shipDetails = $this->user_model->get_all_details(SHIPPING_ADDRESS,array('id'=>$shipID));
		$returnStr['ship_id'] = $shipDetails->row()->id;
		$returnStr['full_name'] = $shipDetails->row()->full_name;
		$returnStr['type'] = $shipDetails->row()->nick_name;
		$returnStr['address1'] = $shipDetails->row()->address1;
		$returnStr['address2'] = $shipDetails->row()->address2;
		$returnStr['city'] = $shipDetails->row()->city;
		$returnStr['state'] = $shipDetails->row()->state;
		$returnStr['country'] = $shipDetails->row()->country;
		$returnStr['postal_code'] = $shipDetails->row()->postal_code;
		$returnStr['phone'] = $shipDetails->row()->phone;
		$returnStr['alter_phone'] = $shipDetails->row()->phone_alternate;
		$returnStr['primary'] = $shipDetails->row()->primary;
		echo json_encode($returnStr);
	}

	public function remove_shipping_addr(){
		$returnStr['status_code'] = 0;
		if ($this->checkLogin('U')==''){
			if($this->lang->line('u_must_login') != '')
			$returnStr['message'] = $this->lang->line('u_must_login');
			else
			$returnStr['message'] = 'You must login';
		}else {
			$shipID = $this->input->post('id');
			$user_id = $this->input->post('user_id');
			$this->user_model->commonDelete(SHIPPING_ADDRESS,array('id'=>$shipID));
			$count = $this->user_model->get_count_address($user_id);
			$returnStr['count'] = $count;
			$returnStr['status_code'] = 1;
		}
		echo json_encode($returnStr);
	}

	public function user_credits(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'My Earnings';
			$orderDetails = $this->data['orderDetails'] = $this->commission->get_total_order_amount($this->checkLogin('U'));
			$commission_to_admin = 0;
			$amount_to_vendor = 0;
			$total_amount = 0;
			$this->data['total_amount'] = $total_amount;
			$total_orders = 0;
			$this->data['except_refunded'] = 0;
			if ($orderDetails->num_rows()==1){
				$commission_percentage = $this->data['userDetails']->row()->commision;
				$total_amount = $orderDetails->row()->TotalAmt;
				$this->data['total_amount'] = $total_amount;
				$total_amount = $total_amount-$this->data['userDetails']->row()->refund_amount;
				$this->data['except_refunded'] = $total_amount;
				$commission_to_admin = $total_amount*($commission_percentage*0.01);
				if ($commission_to_admin<0)$commission_to_admin=0;
				$amount_to_vendor = $total_amount-$commission_to_admin;
				if ($amount_to_vendor<0)$amount_to_vendor=0;
				$total_orders = $orderDetails->row()->orders;
			}
			$paidDetails = $this->commission->get_total_paid_details($this->checkLogin('U'));
			$paid_to = 0;
			if ($paidDetails->num_rows()==1){
				$paid_to = $paidDetails->row()->totalPaid;
				if ($paid_to<0)$paid_to=0;
			}
			$paid_to_balance = $amount_to_vendor-$paid_to;
			if ($paid_to_balance<0)$paid_to_balance=0;
			$this->data['commission_to_admin'] = $commission_to_admin;
			$this->data['amount_to_vendor'] = $amount_to_vendor;
			$this->data['total_orders'] = $total_orders;
			$this->data['paid_to'] = $paid_to;
			$this->data['paid_to_balance'] = $paid_to_balance;
			$sortArr1 = array('field'=>'date','type'=>'desc');
			$sortArr = array($sortArr1);
			$this->data['paidDetailsList'] = $this->commission->get_all_details(VENDOR_PAYMENT,array('vendor_id'=>$this->checkLogin('U'),'status'=>'success'),$sortArr);
			$this->load->view('site/user/user_credits',$this->data);
		}
	}

	public function user_referrals(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			//echo "hi";die;
			$paginationNo = $this->uri->segment('2');

			if($paginationNo == '')
			{
				$paginationNo = 0;
			}
			else
			{
				$paginationNo = $paginationNo;
			}

			$searchPerPage = $this->config->item('pagination_per_page');
			//echo "DSf".$this->uri->segment('2');die;

			$referalBaseUrl = base_url().'referrals';


			$getReferalListCount = $this->user_model->getReferalList();
			$getReferalList = $this->user_model->getReferalList($searchPerPage,$paginationNo);

			$config['base_url'] = $referalBaseUrl;
			$config['total_rows'] = count($getReferalListCount);
			$config["per_page"] = $searchPerPage;
			$config["uri_segment"] =2;
			$this->pagination->initialize($config);
			$paginationLink = $this->pagination->create_links();//die;



			$this->data['heading'] = 'Referrals';
			$this->data['getReferalList'] = $getReferalList;
			$this->data['paginationLink'] = $paginationLink;


			//	echo "<pre>";print_r($getReferalList);die;


			//	    	$this->data['purchasesList'] = $this->user_model->get_group_gifts_list($this->checkLogin('U'));
			$this->load->view('site/user/user_referrals',$this->data);
		}
	}

	public function user_giftcards(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'Gift Cards';
			$this->data['giftcardsList'] = $this->user_model->get_gift_cards_list($this->data['userDetails']->row()->email);
			$this->load->view('site/user/user_giftcards',$this->data);
		}
	}

	public function change_photo(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$config['overwrite'] = FALSE;
			$config['remove_spaces'] = TRUE;
			$config['allowed_types'] = 'jpg|jpeg|gif|png';
			$config['max_size'] = 2000;
			$config['max_width']  = '600';
			$config['max_height']  = '600';
			$config['upload_path'] = './images/users';
			$this->load->library('upload', $config);
			if ( $this->upload->do_upload('upload-file')){
				$imgDetails = $this->upload->data();
				$dataArr['thumbnail'] = $imgDetails['file_name'];
				$condition = array('id'=>$this->checkLogin('U'));
				$this->user_model->update_details(USERS,$dataArr,$condition);
				redirect('image-crop/'.$this->checkLogin('U'));
			}else {
				$this->setErrorMessage('error',strip_tags($this->upload->display_errors()));
			}
			echo "<script>window.history.go(-1);</script>";
		}
	}
	
	public function my_payment(){
	    if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
	    	if(!$_SESSION['All_payments']){
			    if(!$_SESSION['contact_id']){
					$url = 'https://books.zoho.com/api/v3/contacts?organization_id='.BOOKS_ORGANIZATION_ID.'&email_contains='.$_SESSION['session_user_email'].'';
			        $headers = [
			                    'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
			                    'Authorization:'.BOOKS_AUTH_TOKEN
			            ];
			        $curl = curl_init($url);
			        		curl_setopt_array($curl, array(
			            		CURLOPT_RETURNTRANSFER => true,
			            		CURLOPT_HTTPHEADER => $headers
			        		));
			        $result = curl_exec($curl);
			        $contact_data =  json_decode($result, TRUE);
		        	$_SESSION['contact_id'] = $contact_data['contacts'][0]['contact_id'];
				}
				$contact_id = $_SESSION['contact_id'];
				$all_payments = [];
				if($contact_id){
		    		$customer_payment = 'https://books.zoho.com/api/v3/customerpayments?organization_id='.BOOKS_ORGANIZATION_ID.'&customer_id='.$contact_id;
	    		   	$headers = [
			                    'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
			                    'Authorization:'.BOOKS_AUTH_TOKEN
			            ];
		            $curl = curl_init($customer_payment);
		        		curl_setopt_array($curl, array(
		            		CURLOPT_RETURNTRANSFER => true,
		            		CURLOPT_HTTPHEADER => $headers
		        		));
			        $result = curl_exec($curl);
			        $all_payments =  json_decode($result, TRUE);
			        $all_payments = $all_payments['customerpayments'];
				}
        		$this->session->set_userdata('All_payments',$all_payments);
			}else{
				$all_payments = $_SESSION['All_payments'];								
			}
			$all_payments = array_map(function($all_payments) {
				if($all_payments['payment_type'] != 'Retainer Payment') {
					return $all_payments;
				}
			}, $all_payments);
			$all_payments = array_filter($all_payments);
		    $this->data['heading'] = 'My Account - Payments';
			$this->data['payments'] = $all_payments;
			$this->load->view('site/user/my_payment',$this->data);
		}
	}
	
	//Show user wallets heres 
	public function user_wallet(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else {
			$this->data['heading'] = 'My Wallet';
			$this->data['wallets'] = $this->user_model->get_my_wallet_details($this->checkLogin('U'));
			$this->data['wallets_transaction'] = $this->user_model->get_wallet_transaction(array('user_id' => $this->checkLogin('U'),'order_id !=' => NULL ) );
			$this->data['wallets_transaction_credit'] = $this->user_model->get_wallet_transaction(array('user_id' => $this->checkLogin('U'),'order_id' => NULL,'mode' => 'credit' ) );
			$this->load->view('site/user/my_wallet',$this->data);
		}	
	}
	
	//show wishlist listing 	
	public function show_wishlist(){
	    if ($this->checkLogin('U')==''){
			redirect(base_url().'user_sign_up');
		}else {
    		$this->data['heading'] = 'Product Listing';
		    $this->data['my_wished_products'] = $this->user_model->get_like_products($this->checkLogin('U'));
		    $this->data['latest_offer'] = $this->user_model->get_all_details(SITE_OFFERS,array('is_publish_wishlist' => '1','status' => 'Publish'));
		    $this->load->view('site/user/wishlist',$this->data);
		}
	}
	
	//remove product from wishlist
	public function removefromwishlist(){
	    if ($this->checkLogin('U')==''){
		    $response = array('status' => 401,'message'=>'Invalid Access');
		    header('Content-Type: application/json');
    	    echo json_encode($response);exit;
		}else {
		    $this->user_model->commonDelete(PRODUCT_LIKES,array('user_id' => $this->checkLogin('U'),'product_id' => $this->input->post('pid')));
		    $count = $this->user_model->get_like_products($this->checkLogin('U'));
		    $response = array('status' => 200,'message'=>'Removed Successfully','data' => $count->num_rows());
		    header('Content-Type: application/json');
    	    echo json_encode($response);exit;
		}
	}
	
	//show my invoice listing 
	public function my_invoices(){
	    if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else{
// 			if(!$_SESSION['All_Invoices']){
                $headers = ['Content-Type: application/x-www-form-urlencoded;charset=UTF-8','Authorization:'.BOOKS_AUTH_TOKEN];
			    $contactID = $this->get_customer_id($this->checkLogin('U'));
				if($contactID){
		            $sales_orders = 'https://books.zoho.com/api/v3/invoices?&organization_id='.BOOKS_ORGANIZATION_ID.'&customer_id='.$contactID.'&status=unpaid&sort_column=date';
		            $curl = curl_init($sales_orders);
		        		curl_setopt_array($curl, array(
		            		CURLOPT_RETURNTRANSFER => true,
		            		CURLOPT_HTTPHEADER => $headers
		        		));
			        $result = curl_exec($curl);
			        $Due_Invoice =  json_decode($result, TRUE);
			        
		    		$sales_orders = 'https://books.zoho.com/api/v3/invoices?&organization_id='.BOOKS_ORGANIZATION_ID.'&customer_id='.$contactID.'&status=paid&sort_column=date';
		            $curl = curl_init($sales_orders);
		        		curl_setopt_array($curl, array(
		            		CURLOPT_RETURNTRANSFER => true,
		            		CURLOPT_HTTPHEADER => $headers
		        		));
			        $result = curl_exec($curl);
			        $PaidInvoice =  json_decode($result, TRUE);
			        $new_array = array_merge($Due_Invoice['invoices'],$PaidInvoice['invoices']);
			        
			        $new_invoice = [];
			        if(!empty($new_array)){
				       	foreach ($new_array as $key => $value) {
					        $new_invoice[$key]['due_days'] = $value['due_date'];
					        $new_invoice[$key]['invoice_url'] = $value['invoice_url'];
					        $new_invoice[$key]['total'] = $value['total'];
					        $new_invoice[$key]['created_time'] = $value['created_time'];
					        $new_invoice[$key]['invoice_number'] = $value['invoice_number'];
				         	$new_invoice[$key]['current_sub_status'] = $value['current_sub_status'];
			         		$new_invoice[$key]['invoice_id'] = $value['invoice_id'];
			         		$new_invoice[$key]['customer_id'] = $value['customer_id'];
							$new_invoice[$key]['order_id'] =  $value['invoice_number'];
							$new_invoice[$key]['deal_code'] =  $value['cf_dealcodenumber'];
						    
						    $checkDuplicate = $this->user_model->get_all_details(INVOICE_PAYMENT,array('user_id' => $this->checkLogin('U') ,'invoice_id' => $value['invoice_id']));
						    if($value['current_sub_status'] != 'paid'){
						        $status = 'Pending';
						    }else{
						        $status = 'Paid';
						    }
    			            if($checkDuplicate->num_rows() == 1){
                                $this->user_model->update_details(INVOICE_PAYMENT,array('amount' => $value['total'],'status' => $status),array('id' => $checkDuplicate->row()->id ));
    			            }else{
    			                $this->user_model->simple_insert(INVOICE_PAYMENT,array('user_id' => $this->checkLogin('U'),'status' => $status,'invoice_id' => $value['invoice_id'] ,'amount' => $value['total']));
    			            }
				       	}
			        }
    			}
        		    $this->session->set_userdata('All_Invoices',$new_invoice);
// 			}else{
// 				$new_invoice = $_SESSION['All_Invoices'];								
// 			}
		$this->data['my_wallet'] = $this->user_model->get_all_details(MY_WALLET,array('user_id' => $this->checkLogin('U')));
	    $this->data['heading'] = 'My Account - My Invoices page';
		$this->data['my_invoice'] = $new_invoice;
		$this->load->view('site/user/my_invoices',$this->data);
	    }
	}
	
	public function makeOutStandingpayment(){
		if ($this->checkLogin('U')==''){
			redirect(base_url().'login');
		}else{
		    $InvoiceDue = $this->user_model->get_invoice_due_amount($this->checkLogin('U'));
		    if($InvoiceDue->num_rows() == 1){
    			$user_data = $this->user_model->get_all_details(USERS, array('email' => $_SESSION['session_user_email']));
    			$invoice_payment = $InvoiceDue->row()->amount;
                if($_SESSION['Invoice_Applied_Walled']){
                    $invoice_payment = $InvoiceDue->row()->amount - $_SESSION['Invoice_Applied_Walled'];
                }
                $paymtdata = array('invoice_randomNo' =>  mt_rand());
                $this->session->set_userdata($paymtdata);
    			$myData = array('user_amount' => $invoice_payment, 'user_email' => $_SESSION['session_user_email']);
    			$this->data['PapiInfo'] = $this->Customerpayment_set_data($user_data,$myData);
    			if(!empty($user_data->result())){
    				$this->load->view('site/checkout/checkout_payu.php', $this->data);
    			}else{
    				$this->load->view('site/customerpayment/cpfailure.php',$this->data);
    			}
		    }else{
		      	redirect('invoices');
		    }
		}
	}
}

/* End of file user_settings.php */
/* Location: ./application/controllers/site/user_settings.php */