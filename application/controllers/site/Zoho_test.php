<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * User related functions
 * @author Teamtweaks
 *
 */

class Zoho_test extends MY_Controller {
	function __construct(){    
		parent::__construct();
		$this->load->helper(array('cookie','date','form','email','pdf_helper'));
		$this->load->library(array('encrypt','form_validation','curl'));
		$this->load->model('order_model');
		$this->load->model('product_model');
		$this->load->model('user_model');
		$this->load->model('Cms_model');
		$this->data['loginCheck'] = $this->checkLogin('U');
	}


    /////create zoho bulk order inovices


    public function CreateAllZohoInvoices(){
    	$sender_email=$this->data['siteContactMail'];
		$sender_name=$this->data['siteTitle'];
        $email_values = array('mail_type'=>'html',
    		'from_mail_id'=>$sender_email,
    		'mail_name'=>$sender_name,
    		'to_mail_id'=>'naresh.suthar@agileinfoways.com',
    		'subject_message'=>'Hello',
    		'body_messages'=>'Hello Test',
    		'mail_id'=>'register mail'
		);
		$email_send_to_common = $this->product_model->common_email_send($email_values);
		print_r( $this->email->print_debugger());exit;
		echo "success";
exit;
    	//sales invoice start here
    	$user_id =  4907;
    // 	$this->input->post('user_id');
    	$deal_code = 926891539;
    // 	$this->input->post('dealcodenumber');
    // 	if($user_id == '' ||  $user_id == null){
  		// 	$response = array('status' => 'success','code' => 200);
	   //      header('Content-Type: application/json');
	   //      echo json_encode($response);
	   //      exit;
    // 	}
   
    	$product_info = $this->order_model->getCurrentSuccessOrders($user_id, $deal_code);
    	if(empty($product_info)){
    		$response = array('status' => 'success','code' => 200);
	        header('Content-Type: application/json');
	        echo json_encode($response);
	        exit;	
    	}
        $subproducts_quantity = array();   
        $wearhouse_code  = $this->order_model->get_all_details(CITY_WEARHOUSE_CODE,array('id !=' => ''));
        $wareCode = 'BAN';
        $gst_number = '';
        $reg_address = '';
        foreach ($wearhouse_code->result() as $key => $value) {
        	if($value->city_name == $product_info[0]['shippingcity']){
                    $wareCode = $value->wearhouse_code;
                    $gst_number = $value->gst_num;
                    $reg_address = $value->reg_address;
        	}
        }
       	if($product_info[0]['couponCode'] != ''){
			$couponCode = $product_info[0]['couponCode'];
		}else{
			$couponCode = '';
		}

		$url = "https://books.zoho.com/api/v3/contacts?organization_id=".BOOKS_ORGANIZATION_ID."&email_contains=".$product_info[0]['email']."";
        $headers = [
                    'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
                    'Authorization:'.BOOKS_AUTH_TOKEN
            ];
        $curl = curl_init($url);
        		curl_setopt_array($curl, array(
            		CURLOPT_RETURNTRANSFER => true,
            		CURLOPT_HTTPHEADER => $headers
        		));
        $result = curl_exec($curl);
        $contact_data =  json_decode($result, TRUE);
        // print_r("expression");
        // print_r($product_info);exit;
        if (array_key_exists("contact_id", $contact_data['contacts'][0])) {
            $contact_id = $contact_data['contacts'][0]['contact_id'];
            $update_contact_url = "https://books.zoho.com/api/v3/contacts/".$contact_id."";
            $data = array(
	                'authtoken'=>BOOKS_AUTH_TOKEN,
	                'JSONString' => '
	                {
	                    "contact_name": "'.$product_info[0]['ship_full_name'].'",
	                    "contact_type": "customer",
	                    "shipping_address": {
	                        "attention": "Mr.'.$product_info[0]['ship_full_name'].'",
	                        "address": "'.$product_info[0]['ship_address1'].'",
	                        "city": "'.$product_info[0]['ship_city'].'",
	                        "state": "'.$product_info[0]['ship_state'].'",
	                        "zip": '.$product_info[0]['ship_postal_code'].',
	                        "country": "India",
	                        "phone": "'.$product_info[0]['ship_phone'].'",
	                    },
	                      "billing_address": {
			                 "attention": "Mr.'.$product_info[0]['ship_full_name'].'",
	                        "address": "'.$product_info[0]['ship_address1'].'",
	                        "city": "'.$product_info[0]['ship_city'].'",
	                        "state": "'.$product_info[0]['ship_state'].'",
	                        "zip": '.$product_info[0]['ship_postal_code'].',
	                        "country": "India",
	                        "phone": "'.$product_info[0]['ship_phone'].'",
			            },
	                    
	                }',
	                "organization_id"=>BOOKS_ORGANIZATION_ID
	            );

					$ch = curl_init($update_contact_url);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
					curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));
					$response = curl_exec($ch);
					// print_r($response);exit;
        }else{
        	// print_r("else");
        	// exit;
            $Contact_url ="https://books.zoho.com/api/v3/contacts";
            $Contact_data = array(
                'authtoken'=>BOOKS_AUTH_TOKEN,
                'JSONString' => '
                {
                    "contact_name": '.$product_info[0]['ship_full_name'].',
                    "contact_type": "customer",
                    "place_of_contact":"",
                    "shipping_address": {
                        "attention": "Mr.'.$product_info[0]['ship_full_name'].'",
                        "address": "'.$product_info[0]['ship_address1'].'",
                        "city": "'.$product_info[0]['ship_city'].'",
                        "state": "'.$product_info[0]['ship_state'].'",
                        "zip": '.$product_info[0]['ship_postal_code'].',
                        "country": "India",
                        "phone": "'.$product_info[0]['ship_phone'].'",
                    },
                     "billing_address": {
			                "attention": "Mr.'.$product_info[0]['ship_full_name'].'",
	                        "address": "'.$product_info[0]['ship_address1'].'",
	                        "city": "'.$product_info[0]['ship_city'].'",
	                        "state": "'.$product_info[0]['ship_state'].'",
	                        "zip": '.$product_info[0]['ship_postal_code'].',
	                        "country": "India",
	                        "phone": "'.$product_info[0]['ship_phone'].'",
			            },
                    "contact_persons": [
                        {
                            "salutation": "Mr",
                            "first_name": "'.$product_info[0]['ship_full_name'].'",
                            "email": "'.$product_info[0]['email'].'",
                            "phone": "'.$product_info[0]['ship_phone'].'"
                        }
                    ],
                    
                }',
                "organization_id"=>BOOKS_ORGANIZATION_ID
            );
        	$curl = curl_init($Contact_url);
                	curl_setopt_array($curl, array(
                    	CURLOPT_POST => 1,
                    	CURLOPT_POSTFIELDS => $Contact_data,
                    	CURLOPT_RETURNTRANSFER => true
                	));
            $Contact_response = curl_exec($curl);
            $decode_Contact = json_decode($Contact_response);
            // print_r($decode_Contact);exit;
            $contact_id = $decode_Contact->contact->contact_id;
    	}

            // print_r($response);exit;
           	foreach ($product_info as $key =>  $cartRow) {
	        	if($cartRow['attr_name'] != ''){
	        		$tenure  = explode(' ', $cartRow['attr_name']);  
	        		$dayx = date("Y-m-d", strtotime("+".$cartRow['attr_name']));
	    			$pick_up_date =  date("Y-m-d", strtotime($dayx.' +5 day'));
	        	}
	        	array_push($subproducts_quantity, unserialize($cartRow['subproduct_quantity']));
	            $InvImg = @explode(',',$cartRow->image);
	            $unitPrice = ($cartRow['price']*(0.01*$cartRow['product_tax_cost']))+$cartRow['price'] * $cartRow['quantity'] ;
	            $unitDeposit =  $cartRow['product_shipping_cost'] * $cartRow['quantity'];
	            $grandDeposit = $grandDeposit + ($unitDeposit*$cartRow['quantity']);
	            $uTot = $unitPrice + $unitDeposit;
	            $new_deposite +=  $unitDeposit;

	             // $unitPrice = ($cartRow['price']*(0.01*$cartRow['product_tax_cost']))+$cartRow['price'] * $cartRow['quantity'] ;
                    // $unitDeposit =  $cartRow['product_shipping_cost'] * $cartRow['quantity'];
                    // $grandDeposit = $grandDeposit + ($unitDeposit*$cartRow['quantity']);
                    // $uTot = $unitPrice + $unitDeposit;
                	// $new_deposite +=  $unitDeposit;
        	}

        	$private_total = $new_deposite - $product_info[0]['discountAmount'];
	        $private_total = $private_total + $product_info[0]['tax'];
        	$itemsarray = array();
        	$counter = 0;
	        foreach ($product_info as $key =>  $cartRow) {
	       		if($cartRow['subproducts'] == ''){
	       			$zoho_item_id = $this->check_sku($cartRow['sku'], $cartRow['product_name'], $cartRow['id'],'1');
	       			if(!array_key_exists('item_id', $zoho_item_id)){
                          break;
	       			}
	       			$itemsarray[$counter]['item_id'] = $zoho_item_id['item_id'];
	    	 		$itemsarray[$counter]['item_order'] = $cartRow['quantity'];
	                $itemsarray[$counter]['quantity'] = $cartRow['quantity'];
	                if($cartRow['attribute_values'] != ''){
	                	$itemsarray[$counter]['rate'] = round($cartRow['price'] - ($cartRow['price'] / 100) * $cartRow['package_discount']) ;
	                	$itemsarray[$counter]['bcy_rate'] = round($cartRow['price'] - ($cartRow['price'] / 100) * $cartRow['package_discount']);
	            	}else{
	            		$itemsarray[$counter]['rate'] = $cartRow['price'];
	                	$itemsarray[$counter]['bcy_rate'] = $cartRow['price'];
	                	$decimal_price +=  $cartRow['price'] * $cartRow['quantity'];
	            	}
	               $itemsarray[$counter]['name'] =  $zoho_item_id['item_name'];
	               $itemsarray[$counter]['tax_id'] = $zoho_item_id['item_tax_preferences'][0]['tax_id'];
		            $counter++;
		       	}else{ 
		   			$product_id = explode(',',$cartRow['subproducts']);
		   			$product_data = $this->order_model->Get_subproducts($product_id);
		   			foreach ($product_data as $packagekey => $packagevalue) {
		   				if($packagevalue->subproducts != '')
		   				{
		   					$subproduct_id = explode(',', $packagevalue->subproducts);
		   					$subproducts_subproduct = $this->order_model->Get_subproducts($subproduct_id);
		   					foreach ($subproducts_subproduct as $key => $value) {
		   						$zoho_item_id = $this->check_sku($value->sku, $value->product_name, $value->id,'2');
				       			if(!array_key_exists('item_id', $zoho_item_id)){
			                          break;
				       			}

				       			$itemsarray[$counter]['item_id'] = $zoho_item_id['item_id'];
				       			if(!$value->is_addon){
				       			    $attr_price = $this->order_model->GetPriceOnTenure($cartRow['attr_name'],$value->id);
				       				$itemsarray[$counter]['rate'] =  round($attr_price->attr_price - ($attr_price->attr_price / 100) * $cartRow['package_discount']);
		                    		$itemsarray[$counter]['bcy_rate'] =   round($attr_price->attr_price - ($attr_price->attr_price / 100) * $cartRow['package_discount']);

				       			}else{
				       				$itemsarray[$counter]['rate'] =  round($value->price - ($value->price / 100) * $cartRow['package_discount']);
		                    		$itemsarray[$counter]['bcy_rate'] = round($value->price - ($value->price / 100) * $cartRow['package_discount']);
				       			}
		   						$itemsarray[$counter]['item_order'] = $cartRow['quantity'];
		                		$itemsarray[$counter]['quantity'] = $subproducts_quantity[$key][$packagevalue->id];
		                    	$itemsarray[$counter]['name'] =  $zoho_item_id['item_name'];
		                    	 $itemsarray[$counter]['tax_id'] = $zoho_item_id['item_tax_preferences'][0]['tax_id'];
		   						$counter++;
		   					}
		   				}
		   				else
		   				{
		   					$zoho_item_id = $this->check_sku($packagevalue->sku, $packagevalue->product_name, $packagevalue->id,'3');
				       			if(!array_key_exists('item_id', $zoho_item_id)){
			                          break;
				       			}
				       		$itemsarray[$counter]['item_id'] = $zoho_item_id['item_id'];
				       		if(!$packagevalue->is_addon){
		   						$attr_price = $this->order_model->GetPriceOnTenure($cartRow['attr_name'],$packagevalue->id);
		               	    	$itemsarray[$counter]['rate'] =  round($attr_price->attr_price - ($attr_price->attr_price / 100) * $cartRow['package_discount']);
		                    	$itemsarray[$counter]['bcy_rate'] =   round($attr_price->attr_price - ($attr_price->attr_price / 100) * $cartRow['package_discount']);

		   					}else{
		   						$itemsarray[$counter]['rate'] =  round($packagevalue->price - ($packagevalue->price / 100) * $cartRow['package_discount']);
		                    	$itemsarray[$counter]['bcy_rate'] =   round($packagevalue->price - ($packagevalue->price / 100) * $cartRow['package_discount']);
		   					}
		   					$itemsarray[$counter]['item_order'] = $cartRow['quantity'];
		                	$itemsarray[$counter]['quantity'] = $subproducts_quantity[$key][$packagevalue->id];
		                    $itemsarray[$counter]['name'] =  $zoho_item_id['item_name'];
		                    $itemsarray[$counter]['tax_id'] = $zoho_item_id['item_tax_preferences'][0]['tax_id'];
		                    $counter++;
		   				}						
		   			}
		       	}
			}
			$json = json_encode($itemsarray);
			// print_r($json);exit;
	        $sales_order_id = $this->order_model->getAdminSettings();
	        $new_order_id = str_pad(ltrim($sales_order_id->row()->zoho_books_sales_order_id, '0') + 1, 4, '0', STR_PAD_LEFT);
	        $order_date = $product_info[0]['created'];
	        $createDate = strtotime($order_date);
	        $strip = date('Y-m-d', $createDate); 
	        if($product_info[0]['tax'] > 0){
	            $tax = 'true';
	        }else{
	            $tax = 'false';
	        }

          	$customer_notes = 'Tenure: '.$tenure[0].' Place of Supply: ' .$product_info[0]['full_name'].', '.$product_info[0]['ship_address1'].','.$product_info[0]['ship_city'].' '.$product_info[0]['ship_state'].' '.$product_info[0]['ship_postal_code'].' '.'India Phone: '.' '.$product_info[0]['ship_phone'].'';


	        $Curl = "https://books.zoho.com/api/v3/salesorders";
	        $Invoice = array(
	                        'authtoken'=>BOOKS_AUTH_TOKEN,
	                        'JSONString' => '{
                    	        "customer_id": '.$contact_id.',
	                            "date": '.$strip.',    
	                            "line_items": '.$json.',
	                            "notes":"'.$customer_notes.'",
	                            "salesorder_number":"ON-RET-'.$wareCode.'-'.$new_order_id.'",
	                            "reference_number": "#'.$product_info[0]['dealCodeNumber'].'",
	                            "status": "open",
	                            "is_inclusive_tax": true,
	                            "delivery_method": "Company Transport",
	                            "custom_fields": [
	                          		{
	                          			"placeholder": "cf_warehouse_code",
								    	"index": 1,
								    	"value":"'.$wareCode.'" 
	                                },
	                                {
									    "placeholder": "cf_order_type",
									    "index": 2,
									    "value":"Rent"
									},
									{
									    "placeholder": "cf_order_status",
									    "index": 3,
									    "value":"Live"
									},
									{
									    "placeholder": "cf_tenure",
									    "index": 4,
									    "value": '.$tenure[0].'
									},
									{
									    "placeholder": "cf_in",
									    "index": 5,
									    "value":"'.ucfirst($tenure[1]).'"
									},
									{
									    "placeholder": "cf_pickup_date",
									    "index": 6,
									    "value": "'.$pick_up_date.'"
									},
									{
									    "placeholder": "cf_actual_pickup_date",
									    "index": 7,
									    "value": "'.$dayx.'"
									},
									{
									    "placeholder": "cf_offer_given",
									    "index": 9,
									    "value":"'.$couponCode.'"
									   
									},		
	                            ],
	                            "adjustment_description": "Adjustment",
	                            "documents": [
	                                {}
	                            ]
	                        }',
	                        "organization_id"=>BOOKS_ORGANIZATION_ID
	                    );
	        print_r($Invoice);exit;
	        $curl = curl_init($Curl);
	            	curl_setopt_array($curl, array(
	                	CURLOPT_POST => 1,
	                	CURLOPT_POSTFIELDS => $Invoice,
	                	CURLOPT_RETURNTRANSFER => true
	            	));
	        $invoice_result = curl_exec($curl); 
	        $decode_result = json_decode($invoice_result);
	        print_r($decode_result);exit;
 			$reference_number = '';
	        if (isset($decode_result->salesorder)) {
        	 	$reference_number = $decode_result->salesorder->salesorder_number;
	        	$invoice_id = $decode_result->salesorder->salesorder_id;
	            $status_url ='https://books.zoho.com/api/v3/salesorders/'.$invoice_id.'/status/open?organization_id=82487023';
			    $headers = [
		                    'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
		                    'Authorization:'.BOOKS_AUTH_TOKEN
				            ];
		        $curl = curl_init($status_url);
		        		curl_setopt_array($curl, array(
		            		CURLOPT_RETURNTRANSFER => true,
		            		CURLOPT_HTTPHEADER => $headers,
		            		CURLOPT_POST => 1,
		        		));
		        $result = curl_exec($curl);
        		$respo =  json_decode($result, TRUE);
					
	      	}else{
	      	// 	$json_array = '';
	      	// 	if($decode_result->message == 'JSON is not well formed'){
	      			$json_array = $Invoice;
	      	// 	}
	      		$logs_array = array('log_message' => $decode_result->message,'json_array' => serialize($json_array)  ,'order_id' => $deal_code,'invoice_name' => 'Sales Invoice');
	      		$this->order_model->simple_insert(ZOHO_LOGS,$logs_array);
	      	}
			$dataArr = array('zoho_books_sales_order_id' => $new_order_id);
			$condition  = array('id' => 1);
	        $this->order_model->UpdateAdminSetting($dataArr,$condition);
	        //Sales Invoice end here


	        //Retainer Invoice Start here 

            $order_date = $product_info[0]['created'];
            $createDate = strtotime($order_date);
            $strip = date('Y-m-d', $createDate); 
	        $RetainerUrl = "https://books.zoho.com/api/v3/retainerinvoices";
	        $RetainerData = array(
                    'authtoken'=>BOOKS_AUTH_TOKEN,
                    'JSONString' => '{
                        "customer_id": '.$contact_id.',
                        "reference_number":"'.$reference_number.'",
                        "notes":  "'.$customer_notes.'",
                        "terms": "Terms & Conditions apply",
                        "line_items":[
						        {
						            "description": "Security Deposit",
						            "item_order": 1,
						            "rate": '.$new_deposite.'
						        }
						    ],
					     "custom_fields": [
      						{
							    "placeholder": "cf_gst",
							    "index": 1,
							    "value":"'.$gst_number.'"
							   
							},
                        ],
                        "payment_options": {
                            "payment_gateways": [
                                {
                                    "gateway_name": "paypal"
                                }
                            ]
                        },
                    }',
                    "organization_id"=>BOOKS_ORGANIZATION_ID
                );
	      	$curl = curl_init($RetainerUrl);
	        curl_setopt_array($curl, array(
	            CURLOPT_POST => 1,
	            CURLOPT_POSTFIELDS => $RetainerData,
	            CURLOPT_RETURNTRANSFER => true
	        ));
	        $result = curl_exec($curl);
	        $respo = json_decode($result);
	        if(isset($respo->retainerinvoice)){
					$Invoice_id = $respo->retainerinvoice->retainerinvoice_id;
    			$this->CreateCustomerPayment_retainer($contact_id,$product_info[0]['mandate_id'],$Invoice_id,$new_deposite);
    		}else
    		{
    // 			$json_array = '';
	      	// 	if($decode_result->message == 'JSON is not well formed'){
	      			$json_array = $RetainerData;
	      	// 	}
    			$logs_array = array('log_message' => $respo->message, 'json_array' => serialize($json_array)  ,'order_id' => $deal_code,'invoice_name' => 'Retainer Invoice');
	      		$this->order_model->simple_insert(ZOHO_LOGS,$logs_array);
			}

			//Create Invoice 


			$seller_info = $this->order_model->get_all_details(USERS,array('id' => $product_info[0]['sell_id']));
		    $coupon_code  = $this->order_model->get_all_details(COUPONCARDS,array('id' => $product_info[0]['coupon_id']));
	    	if($coupon_code->row()->price_value >= 100){
	                $discount_first_month  = 0;
	    	}else{
	    		$discount_first_month =  $product_info[0]['discountAmount'];
	    	}
			$is_inclusive_tax = 'true';
	    	if($product_info[0]['discountAmount'] >= $unitPrice){
	    		$is_inclusive_tax = 'false';
	    	}

    	  	$Curl = "https://books.zoho.com/api/v3/invoices";
       		$Invoice = array(
                        'authtoken'=>BOOKS_AUTH_TOKEN,
                        'JSONString' => '{
                            "customer_id": '.$contact_id.',
                            "date": '.$strip.',
                            "discount": '.$product_info[0]['discountAmount'].',
                            "reference_number":"'.$reference_number.'",
                            "is_inclusive_tax": '.$is_inclusive_tax.',
                            "discount_type":"entity_level",
    						"salesperson_name": '.$seller_info->row()->full_name.',
                            "line_items":'.$json.',
                            "notes":"'.$customer_notes.'",
                            "payment_options": {},
                            "allow_partial_payments": true,
                            "custom_body": " ",
                            "custom_subject": "Sale Invoice",
                            "terms": "Terms & Conditions apply",
                            "gateway_name": "PayU",
                            "custom_fields": [
	          						{
									    "placeholder": "cf_gst",
									    "index": 4,
									    "value":"'.$gst_number.'"
									   
									},
									{
									    "placeholder": "cf_reg_address",
									    "index": 5,
									    "value":"'.$reg_address.'"
									   
									},
									{
									    "placeholder": "cf_reverse_charge_applicable",
									    "index": 6,
									    "value":"No"
									   
									},		
									{
									    "placeholder": "cf_invoice_number_exception",
									    "index": 7,
									    "value":"No"
									   
									},
									{
									    "placeholder": "cf_pos",
									    "index": 8,
									    "value":"'.$product_info[0]['ship_city'].'"
									   
									},
									{
									    "placeholder": "cf_delivery_state",
									    "index": 9,
									    "value":"'.$product_info[0]['ship_state'].'"
									   
									},	
	                            ],
                            "additional_field1": "standard"
                           
                        }',
                        "organization_id"=>BOOKS_ORGANIZATION_ID
                    );

        print_r($Invoice);exit;
          $curl = curl_init($Curl);
            curl_setopt_array($curl, array(
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $Invoice,
                CURLOPT_RETURNTRANSFER => true
            ));
            $invoice_result = curl_exec($curl);  
            $new_result = json_decode($invoice_result);   
            if (isset($new_result->invoice)) {
	            $Invoice_id = $new_result->invoice->invoice_id;
	            $status_url ='https://books.zoho.com/api/v3/invoices/'.$Invoice_id.'/status/sent?organization_id=82487023';
			    $headers = [
		                    'Content-Type: application/x-www-form-urlencoded;charset=UTF-8',
		                    'Authorization:'.BOOKS_AUTH_TOKEN
				            ];
		        $curl = curl_init($status_url);
		        		curl_setopt_array($curl, array(
		            		CURLOPT_RETURNTRANSFER => true,
		            		CURLOPT_HTTPHEADER => $headers,
		            		CURLOPT_POST => 1,
		        		));
		        $result = curl_exec($curl);
        		$respo =  json_decode($result, TRUE);
          		$this->CreateCustomerPayment($contact_id,$product_info[0]['mandate_id'],$Invoice_id,$new_result->invoice->total);
	      	}else{
	      	// 	$json_array = '';
	      	// 	if($new_result->message == 'JSON is not well formed'){
	      			$json_array = $Invoice;
	      	// 	}
	      		$logs_array = array('log_message' => $new_result->message ,'json_array' => serialize($json_array) ,'order_id' => $deal_code,'invoice_name' => 'Invoice');
	      		$this->order_model->simple_insert(ZOHO_LOGS,$logs_array);
	      	}

	      	$end_date =  date("Y-m-d", strtotime($dayx.' +5 day'));       
           	$Curl = "https://books.zoho.com/api/v3/recurringinvoices";
	        $Invoice = array(
	                        'authtoken'=> BOOKS_AUTH_TOKEN,
	                        'JSONString' => '{
	                        		"recurrence_name": "'.strtoupper($product_info[0]['ship_full_name']).'('.$reference_number.')",
								    "customer_id": '.$contact_id.',
								    "place_of_supply": "TN",
								    "start_date": "'.date('Y-m-d',strtotime("+1 month")).'",
								    "discount_type":"entity_level",
								    "reference_number":"'.$reference_number.'",
								    "recurrence_frequency": "months",
								    "salesperson_name":"Accounts Team",
								    "is_inclusive_tax": '.$is_inclusive_tax.',
								    "payment_terms_label":"Due on Receipt",
								    "payment_terms":"0",
								    "line_items":'.$json.',
								    "notes":"'.$customer_notes.'",
								    "email": "'.$product_info[0]['email'].'",
								    "payment_gateways": [
								        {
								            "configured": true,
								            "additional_field1": "standard",
								            "gateway_name": "paypal"
								        }
								    ],

								    "custom_fields": [
	          						{
									    "placeholder": "cf_gst",
									    "index": 4,
									    "value":"'.$gst_number.'"
									   
									},
									{
									    "placeholder": "cf_reg_address",
									    "index": 5,
									    "value":"'.$reg_address.'"
									   
									},
									{
									    "placeholder": "cf_reverse_charge_applicable",
									    "index": 6,
									    "value":"No"
									   
									},		
									{
									    "placeholder": "cf_invoice_number_exception",
									    "index": 7,
									    "value":"No"
									   
									},
									{
									    "placeholder": "cf_pos",
									    "index": 8,
									    "value":"'.$product_info[0]['ship_city'].'"
									   
									},
									{
									    "placeholder": "cf_delivery_state",
									    "index": 9,
									    "value":"'.$product_info[0]['ship_state'].'"
									   
									},	
	                            ],
								 
								    "is_discount_before_tax": true,
								    "discount": '.$discount_first_month.',
								    "adjustment_description": "Rounding off"
								}',
	                        "organization_id"=> BOOKS_ORGANIZATION_ID
	                    );
	        $curl = curl_init($Curl);
	            	curl_setopt_array($curl, array(
	                	CURLOPT_POST => 1,
	                	CURLOPT_POSTFIELDS => $Invoice,
	                	CURLOPT_RETURNTRANSFER => true
	            	));
	        $invoice_result = curl_exec($curl);
	        $respo = json_decode($invoice_result);
	        if (!isset($respo->recurring_invoice)) {
	       // 	$json_array = '';
	      	// 	if($decode_result->message == 'JSON is not well formed'){
	      			$json_array = $Invoice;
	      	// 	}
	        	$logs_array = array('log_message' => $respo->message,'json_array' => serialize($json_array)  ,'order_id' => $deal_code,'invoice_name' => 'Recurring Invoice');
	      		$this->order_model->simple_insert(ZOHO_LOGS,$logs_array);
	        }
	        unset($_SESSION['dealCodeNumber']);
	        unset($_SESSION['user_id']);
	        // echo "success";
	          $response = array('status' => 'success','code' => 200);
	         header('Content-Type: application/json');
	         echo json_encode($response);
	}

	public function CreateCustomerPayment_retainer($contact_id,$order_id,$Invoice_id,$total ){
    	  	$Curl = "https://books.zoho.com/api/v3/customerpayments?organization_id=82487023";
    	  	$Invoice = 'JSONString={
    	  		   	"customer_id": '.$contact_id.',
					"payment_mode": "Payment Gateway",
					"amount": '.$total.',
					"date": "'.date('Y-m-d').'",
					"reference_number": "#'.$order_id.'",
					"retainerinvoice_id" : '.$Invoice_id.'
    	  	}';
	         $headers = [
                'Content-Type: application/x-www-form-urlencoded',
                'Authorization:'.BOOKS_AUTH_TOKEN
	            ];
		
	        $curl = curl_init($Curl);
	            	curl_setopt_array($curl, array(
	                	CURLOPT_POST => 1,
	                	CURLOPT_HTTPHEADER => $headers,
	                	CURLOPT_POSTFIELDS => $Invoice,
	                	CURLOPT_RETURNTRANSFER => true
	            	));
	        $customer_payment = curl_exec($curl);
	        $respo = json_decode($customer_payment);
	        if(!isset($respo->payment)){
	        		$email_values = array(
									'mail_type'=>'html',
	             					'from_mail_id'=>'hello@cityfurnish.com',
	             					'mail_name'=>'Zoho Error Mail',
	             					'to_mail_id'=>'naresh.suthar@agileinfoways.com',
	             					'subject_message'=>'Customer Payment Error',
	             					'body_messages'=> $customer_payment
								);
				$email_send_to_common = $this->order_model->common_email_send($email_values);
	        }
    }

    public function CreateCustomerPayment($contact_id,$order_id,$Invoice_id,$total ){
    	  	$Curl = "https://books.zoho.com/api/v3/customerpayments?organization_id=82487023";
    	  	$Invoice = 'JSONString={
    	  		   "customer_id": '.$contact_id.',
					"payment_mode": "Payment Gateway",
					"amount": '.$total.',
					"date": "'.date('Y-m-d').'",
				    "reference_number": "#'.$order_id.'",
					"description": "Payment has been added to '.$order_id.'",
					"invoices": [
					    {
						    "invoice_id": '.$Invoice_id.',
							"amount_applied": '.$total.'
						}
					],
					"amount_applied": '.$total.',
					"tax_amount_withheld": 0
    	  	}';
	         $headers = [
                'Content-Type: application/x-www-form-urlencoded',
                'Authorization:'.BOOKS_AUTH_TOKEN
	            ];
		
			// // 	    // "shipping_charge": '.$product_info[0]['shippingcost'].',
	        $curl = curl_init($Curl);
	            	curl_setopt_array($curl, array(
	                	CURLOPT_POST => 1,
	                	CURLOPT_HTTPHEADER => $headers,
	                	CURLOPT_POSTFIELDS => $Invoice,
	                	CURLOPT_RETURNTRANSFER => true
	            	));
	        $customer_payment = curl_exec($curl);
	        $respo = json_decode($customer_payment);
	        if(!isset($respo->payment)){
	        		$email_values = array(
									'mail_type'=>'html',
	             					'from_mail_id'=>'hello@cityfurnish.com',
	             					'mail_name'=>'Zoho Error Mail',
	             					'to_mail_id'=>'naresh.suthar@agileinfoways.com',
	             					'subject_message'=>'Customer Payment Error',
	             					'body_messages'=> $customer_payment
								);
				$email_send_to_common = $this->order_model->common_email_send($email_values);
	        }
    }

    public function check_sku($sku,$product_name,$product_id, $count = 0){
    	$url_pro =  "https://books.zoho.com/api/v3/items?organization_id=". BOOKS_ORGANIZATION_ID ."&search_text=".$sku."";
        $headers = [
                    'Content-Type: application/json;charset=UTF-8',
                    'Authorization:'.BOOKS_AUTH_TOKEN
        ];
        $curl = curl_init($url_pro);
				curl_setopt_array($curl, array(
        		CURLOPT_RETURNTRANSFER => true,
        		CURLOPT_HTTPHEADER => $headers
	    ));
	        $result = curl_exec($curl);
	        $product_info_data =  json_decode($result, TRUE);
        if(array_key_exists("item_id", $product_info_data['items'][0])){
            return $product_info_data['items'][0];
        }
        else{
        	$failed_msg = $product_name.' is not found in zoho items where item id is '.$product_id.' and sku is'.$sku.' delay '.$count;
			$email_values = array(
								'mail_type'=>'html',
             					'from_mail_id'=>'hello@cityfurnish.com',
             					'mail_name'=>'Zoho Error Mail',
             					'to_mail_id'=>'naresh.suthar@agileinfoways.com',
             					'subject_message'=>'Zoho Case Error',
             					'body_messages'=>$failed_msg
							);
			$email_send_to_common = $this->order_model->common_email_send($email_values);
			return null;
    	}

    }
    

    
//     public function check_test_email(){
//         // set post fields
//         $post = [
//             'sub_status' => $this->input->post('sub_status'),
//             'case_id' => $this->input->post('case_id'),
//             'order_id'   => $this->input->post('order_id'),
//             'status' => $this->input->post('status')
//         ];
        
//         $ch = curl_init('http://180.211.99.165:8080/rajan/cityfurnish/send_test_email');
//         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//         curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        
//         // execute!
//         $response = curl_exec($ch);
        
//         // close the connection, release resources used
//         curl_close($ch);
        
//         // do anything you want with your response
//         // var_dump($response);

// 	}
	
	public function check_test_email(){
		$update_array['zoho_lead_status'] = $this->input->post('status');
		$update_array['zoho_lead_substatus'] = $this->input->post('sub_status');

		if($this->input->post('status')  == 'Delivery Done'){
			$update_array['lead_status'] = 'delivered';
		}
		$this->Cms_model->update_details(BD_LEADS,$update_array,array('order_id' => $this->input->post('order_id')) );

		$Details = $this->Cms_model->get_user_details_on_payment($this->input->post('order_id'),$this->input->post('sub_status'));
		if($Details->num_rows() > 0){
			// ucfirst();
			$template_values = $this->Cms_model->get_all_details(ORDER_NOTIFICATION,array('order_sub_status' => $this->input->post('sub_status')));
			if($template_values->num_rows() > 0){
				$template_values = $template_values->row_array();

				if($template_values['sms_status'] == '1'){
					$searchArray = array("{{CUSTOMER_NAME}}", "{{ORDER_ID}}");
					$replaceArray = array(ucfirst($Details->row()->full_name), '#'.$this->input->post('order_id'));
					$new_sms_msg .=  str_replace($searchArray, $replaceArray ,$template_values['sms_content'] );

					$mobile_number = '91'.$Details->row()->phone_no;
			        
			        $fields = [
                		  	'username' => 'cityfrnshhtptrn',
                		  	'password' => 'cityf121',
                			'text' =>$new_sms_msg,
                			'to' => $mobile_number,
                			'from' => 'CITYFN'
                		  ];

			        $this->load->helper('sms');
			    	$response = send_sms($fields);
				}


		    	// $newsid = '3';
				if($template_values['email_status'] == '1'){

					$subject = 'CityFurnish - Order #'.$this->input->post('order_id').' - '.$template_values['order_sub_status'];
					$adminnewstemplateArr=array('email_title'=> $this->config->item('email_title'),'logo'=> $this->data['logo']);
					extract($adminnewstemplateArr);

				    $searchArray = array("{{CUSTOMER_NAME}}", "{{ORDER_ID}}");
					$replaceArray = array(ucfirst($Details->row()->full_name), '#'.$this->input->post('order_id'));
					$new_message .=  str_replace($searchArray, $replaceArray ,$template_values['email_content'] );
					$message .= '<!DOCTYPE HTML>
						<html>
						<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
						
						<meta name="viewport" content="width=device-width"/><body>';
					$message = $new_message;

					$message .= '</body>
						</html>';

					$sender_email=$this->data['siteContactMail'];
					$sender_name=$this->data['siteTitle'];

					$email_values = array('mail_type'=>'html',
										'from_mail_id'=>$sender_email,
										'mail_name'=>$sender_name,
										'to_mail_id'=> $Details->row()->email,
										'subject_message'=> $subject,
										'body_messages'=>$message
										);
					$email_send_to_common = $this->product_model->common_email_send($email_values);
				}
				
			}
			$is_update = $this->Cms_model->update_details(PAYMENT,array('zoho_status' => $this->input->post('status'),'zoho_sub_status' => $this->input->post('sub_status')),array('dealCodeNumber' => $this->input->post('order_id')) );

		}
	}
	
	
	public function get_so_params(){
	   //$post = [
    //         'sub_status' => $this->input->post('sub_status'),
    //         'case_id' => $this->input->post('case_id'),
    //         'order_id'   => $this->input->post('order_id'),
    //         'status' => $this->input->post('status')
    //     ];
        
        $ch = curl_init('http://180.211.99.165:8080/rajan/cityfurnish/send_test_email');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $_POST);
        
        // execute!
        $response = curl_exec($ch);
        
        // close the connection, release resources used
        curl_close($ch);
        
//         // do anything you want with your response
//         // var_dump($response);   
	}
	
	




}
