<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 * User related functions
 * @author Teamtweaks
 *
 */

class Epc_calculator extends MY_Controller {
	function __construct(){
        parent::__construct();
        $this->load->model('couponcards_model');
	}
    
    public function index(){
        $this->data['couponCardsList'] = $this->couponcards_model->get_all_details(COUPONCARDS,$condition);
        $this->data['products'] = $this->product_model->get_all_details(PRODUCT, '1 = 1');
        $this->data['PrdattrVal'] = $this->product_model->view_product_atrribute_details();
        $this->load->view('site/order/epc_view', $this->data);
    }
    
    public function get_sub_product_values(){
        $product_ids = $this->input->post('product_ids');
        if(!$product_ids){
            echo json_encode([
                'status' => false,
                'SubPrdVal' => [] 
            ]);
            return false;
        }
        $SubPrdVal = array();
        foreach($product_ids as $product_id){
            $SubPrdVal[] = $this->product_model->view_subproduct_details($product_id)->result();
        }
        
        if(!empty($SubPrdVal)){
            echo json_encode([
                'status' => true,
                'SubPrdVal' => $SubPrdVal
            ]);
        }else{
            echo json_encode([
                'status' => false,
                'SubPrdVal' => []
            ]);
        }
    }
    
    public function calculate_ecp(){
        $firstName = $this->input->post('firstName');
        $lastName = $this->input->post('lastName');
        $txtEmail = $this->input->post('txtEmail');
        $securityDeposit = $this->input->post('securityDeposit');
        $tenureslider = $this->input->post('tenureslider');
        $productActualUsedMonths = $this->input->post('productActualUsedMonths');
        $productActualUsedDays = $this->input->post('productActualUsedDays') != "" && $this->input->post('productActualUsedDays') > 0 && $this->input->post('productActualUsedDays') < 31 ? $this->input->post('productActualUsedDays') : 0;
        $tenureAmount = $this->input->post('tenureAmount');
        $productAmount = $this->input->post('productAmount');
        $actualPercentage = $this->input->post('actualPercentage');
        $damageAmount = $this->input->post('damageAmount') != "" && $this->input->post('damageAmount') > 0 ? $this->input->post('damageAmount') : 0;
        $oneDayRent = $productAmount/30;
        $daysRent = $oneDayRent * $productActualUsedDays;
        $additionalDiscount = $this->input->post('additionalDiscount') != "" && $this->input->post('additionalDiscount') > 0 ? $this->input->post('additionalDiscount') : 0;
        $earliyPickupCharges = (($tenureAmount * $actualPercentage) / 100) + $daysRent + $damageAmount;
        $waiverAmount = $earliyPickupCharges - $additionalDiscount;
        $refundableAmount = $securityDeposit - $waiverAmount;
        echo json_encode([
            'status' => true,
            'data' => [
                'earliyPickupCharges' => ceil($earliyPickupCharges) ,
                'waiverAmount' => ceil($waiverAmount),
                'refundableAmount' => ceil($refundableAmount)
            ]
        ]);
    }
    
    public function get_coupan_details($coupan_id = null){
        if(!$coupan_id){
            echo json_encode([
                'status' => false,
                'coupan_data' => []
            ]);
            return false;
        }
        $coupan_details = $this->couponcards_model->get_all_details(COUPONCARDS,['id' => $coupan_id]);
        echo json_encode([
            'status' => true,
            'coupan_data' => $coupan_details->result()
        ]);
    }
}