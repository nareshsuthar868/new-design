<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 *
 * User Settings related functions
 * @author Teamtweaks
 *
 */

class InvoicePayment extends MY_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('cookie','date','form','email'));
		$this->load->library(array('encrypt','form_validation','pagination'));
		$this->load->model('invoice_model');
	}
	
	
	
	public function index(){
	   // echo "<pre>";
	   // print_r($_POST);exit;
	    if($this->uri->segment(2) == 'success'){
            $checkDueInvoice = $this->invoice_model->get_all_details(INVOICE_PAYMENT,array('user_id' => $this->checkLogin('U'),'status' => 'Pending'));
            $contactID = $this->get_customer_id($this->checkLogin('U'));
            $TxnID = $_POST['txnid'];
            $mendate_id = $_POST['mihpayid'];
            $randomId = $this->uri->segment(4);
            foreach($checkDueInvoice->result() as $key =>  $value){
                $this->CreateCustomerPayment($contactID,$TxnID,$value->invoice_id,$value->amount);
            }
            
            if($_SESSION['Invoice_Applied_Walled']){
                $insertData = array('user_id' => $this->checkLogin('U'),'mihpayid' =>$mendate_id ,'payment_mode' => '', 'payment_status'=> 'success','txnid' => $TxnID, 'amount' => $_SESSION['Invoice_Applied_Walled'] , 'order_id' => $randomId,'mode' => 'debit');
                $Insert_response = $this->invoice_model->simple_insert(WALLET_TRAN,$insertData);
                $my_wallet = $this->invoice_model->get_all_details(MY_WALLET,array('user_id' => $this->checkLogin('U')));
                if($my_wallet->num_rows() > 0){
                    $wallet = $my_wallet->row();
                    $Data['topup_amount'] = $wallet->topup_amount - $_SESSION['Invoice_Applied_Walled'];
                    $this->invoice_model->update_details(MY_WALLET,$Data,array('user_id' => $this->checkLogin('U')));
                }
                unset($_SESSION['Invoice_Applied_Walled']);
            }
            
            $this->data['Confirmation'] = 'Success';				
		    $this->load->view('site/order/order.php',$this->data);
	    }else{
	        $this->data['Confirmation'] = 'Failure';				
		    $this->load->view('site/order/order.php',$this->data);
	    }
	}
	
	
	public function CreateCustomerPayment($contact_id,$order_id,$Invoice_id,$total ){
      	$Curl = "https://books.zoho.com/api/v3/customerpayments?organization_id=82487023";
      	$Invoice = 'JSONString={
  	                "customer_id": '.$contact_id.',
		            "payment_mode": "Payment Gateway",
					"amount": '.$total.',
					"date": "'.date('Y-m-d').'",
				    "reference_number": "#'.$order_id.'",
					"description": "Payment has been added to '.$order_id.'",
					"invoices": [
					    {
						    "invoice_id": '.$Invoice_id.',
							"amount_applied": '.$total.'
						}
					],
					"amount_applied": '.$total.',
					"tax_amount_withheld": 0
    	  	}';
        $headers = [
            'Content-Type: application/x-www-form-urlencoded',
            'Authorization:'.BOOKS_AUTH_TOKEN
        ];
        $curl = curl_init($Curl);
    	    curl_setopt_array($curl, array(
        	CURLOPT_POST => 1,
        	CURLOPT_HTTPHEADER => $headers,
        	CURLOPT_POSTFIELDS => $Invoice,
        	CURLOPT_RETURNTRANSFER => true
    	));
        $customer_payment = curl_exec($curl);
        $respo = json_decode($customer_payment);
        if(isset($respo->payment)){
            $this->invoice_model->update_details(INVOICE_PAYMENT,array('status' => 'Paid'),array('user_id' => $this->checkLogin('U') ,'invoice_id' => $Invoice_id ));
        }
    }

	
}