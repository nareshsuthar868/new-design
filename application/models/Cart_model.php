<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * This model contains all db functions related to Cart Page
 * @author Teamtweaks
 *
 */
class Cart_model extends My_Model {

    public function view_sub_product_details_join($prdId=''){
        //echo "123-".$prdId;exit;
        $select_qry = "select a.*,b.attr_name as attr_type from ".SUBPRODUCT." a join ".PRODUCT_ATTRIBUTE." b on a.attr_id = b.id where a.product_id = '".$prdId."'";
        
        return $attList = $this->ExecuteQuery($select_qry);
    }
    
    public function add_cart($dataArr = '') {
        $this->db->insert(PRODUCT, $dataArr);
    }

    public function edit_cart($dataArr = '', $condition = '') {
        $this->db->where($condition);
        $this->db->update(PRODUCT, $dataArr);
    }

    public function view_cart($condition = '') {
        return $this->db->get_where(PRODUCT, $condition);
    }
    
    
    public function get_cart_details($userid = ''){
        $this->db->select('a.*,b.product_name,b.is_addon,b.addons,b.quantity as mqty,b.seourl,b.image,b.id as prdid,b.price as orgprice,b.ship_immediate,c.attr_name as attr_type,d.attr_name');
        $this->db->from(SHOPPING_CART . ' as a');
        $this->db->join(PRODUCT . ' as b', 'b.id = a.product_id');
        $this->db->join(SUBPRODUCT . ' as d', 'd.pid = a.attribute_values', 'left');
        $this->db->join(PRODUCT_ATTRIBUTE . ' as c', 'c.id = d.attr_id', 'left');
        $this->db->where('a.user_id = ' . $userid);
        return $this->db->get();
    }

    public function mani_cart_view($userid = '') {
        $mobiledetect = new Mobile_Detect();
        if ($mobiledetect->isMobile() || $mobiledetect->isTablet() || $mobiledetect->isAndroidOS()) {
            $is_mobile = 1;
        } else {
            $is_mobile = 0;
        }
        $MainShipCost = 0;
        $MainTaxCost = 0;
        $cartQty = 0;
        $count = 0;
            
        $country_query = $this->db->get('fc_country');

        $shipVal = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $userid));//12140, 
        $new_coupon_code = $this->get_coupon_value($userid);
        
        if ($shipVal->num_rows() > 0) {

            $shipValID = $this->cart_model->get_all_details(SHIPPING_ADDRESS, array('user_id' => $userid, 'primary' => 'Yes'));
            $dataArr = array('shipping_id' => $shipValID->row()->id);
            $condition = array('user_id' => $userid);
            $this->cart_model->update_details(FANCYYBOX_TEMP, $dataArr, $condition);
            $ShipCostVal = $this->cart_model->get_all_details(COUNTRY_LIST, array('country_code' => $shipValID->row()->country));
            
            $MainShipCost = $ShipCostVal->row()->shipping_cost;
            $MainTaxCost = $ShipCostVal->row()->shipping_tax;
            $dataArr2 = array('shipping_cost' => $MainShipCost, 'tax' => $MainTaxCost);
            $this->cart_model->update_details(SHOPPING_CART, $dataArr2, $condition);
        }
        
        $GiftValue = '';
        $CartValue = '';
        $SubscribValue = '';
        
        $voucher_data = $this->product_model->get_all_details(VOUCHER,array('voucher_title != ' => ''));

        $giftSet = $this->cart_model->get_all_details(GIFTCARDS_SETTINGS, array('id' => '1'));
        $giftRes = $this->cart_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $userid));
        $discount_on_si_setting = $this->cart_model->get_all_details(SI_DISCOUNT_SETTING,array('id' => 1));

        $SubcribRes = $this->minicart_model->get_all_details(FANCYYBOX_TEMP, array('user_id' => $userid));
         $get_user = $this->minicart_model->get_all_details(USERS, array('id' => $userid));

        $this->db->select('a.*,b.product_name,b.is_addon,b.addons,b.quantity as mqty,b.seourl,b.image,b.id as prdid,b.price as orgprice,b.ship_immediate,c.attr_name as attr_type,d.attr_name');
        $this->db->from(SHOPPING_CART . ' as a');
        $this->db->join(PRODUCT . ' as b', 'b.id = a.product_id');
        $this->db->join(SUBPRODUCT . ' as d', 'd.pid = a.attribute_values', 'left');
        $this->db->join(PRODUCT_ATTRIBUTE . ' as c', 'c.id = d.attr_id', 'left');
        $this->db->where('a.user_id = ' . $userid);
        $this->data['cartVal'] = $this->db->get();
        

        $this->db->select('discountAmount,couponID,couponCode,coupontype');
        $this->db->from(SHOPPING_CART);
        $this->db->where('user_id = ' . $userid);
        $discountQuery = $this->db->get();

        $disAmt = $discountQuery->row()->discountAmount;

        if(isset($_SESSION['prcity']) && $_SESSION['prcity'] != '') {
            $this->db->select('a.product_id, b.product_name, a.quantity');
            $this->db->from(SHOPPING_CART . ' as a');
            $this->db->join(PRODUCT . ' as b', 'b.id = a.product_id');
            $this->db->where('a.user_id = ' . $userid);
            $cartProductVal = $this->db->get()->result_array();

            $proNameArr = array();
            $quantityErr = array();
            if($cartProductVal) {
                foreach($cartProductVal as $cartProduct) {
                    $this->db->select('count(id) as cnt, list_value_seourl');
                    $this->db->from('fc_list_values');
                    $this->db->where('FIND_IN_SET("'.$cartProduct['product_id'].'", `products`) and id = '. $_SESSION['prcity']);
                    $productInCity = $this->db->get()->first_row();

                    if($productInCity->cnt == '0') {
                        $proNameArr[] = $cartProduct['product_name'];                        
                    } else {
                        $cityQuantity = $this->product_model->view_product_quantity(array('product_id' => $cartProduct['product_id']));

                        if($cartProduct['quantity'] > $cityQuantity[$_SESSION['prcity']]) {
                            $quantityErr[] = $cartProduct['product_name'];
                        }
                    }
                }
            }

        }

        $city_merge = $this->get_city_array();

    }

    public function mani_gift_total($userid = '') {

        $giftRes = $this->cart_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $userid));
        $giftAmt = 0;
        if ($giftRes->num_rows() > 0) {

            foreach ($giftRes->result() as $giftRow) {
                $giftAmt = $giftAmt + $giftRow->price_value;
            }
        }
        $SubcribRes = $this->cart_model->get_all_details(FANCYYBOX_TEMP, array('user_id' => $userid));
        $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $countVal = $giftRes->num_rows() + $SubcribRes->num_rows() + $cartVal->num_rows();

        return number_format($giftAmt, 2, '.', '') . '|' . $countVal;
    }

    public function mani_subcribe_total($userid = '') {

        $SubcribRes = $this->cart_model->get_all_details(FANCYYBOX_TEMP, array('user_id' => $userid));
        $SubcribAmt = 0;
        $SubcribSAmt = 0;
        $SubcribTAmt = 0;
        $SubcribTotalAmt = 0;
        if ($SubcribRes->num_rows() > 0) {

            foreach ($SubcribRes->result() as $SubscribRow) {
                $SubcribAmt = $SubcribAmt + $SubscribRow->price;
            }
            $SubcribSAmt = $SubcribRes->row()->shipping_cost;
            $SubcribTAmt = $SubcribRes->row()->tax;
            $SubcribTotalAmt = $SubcribAmt + $SubcribSAmt + $SubcribTAmt;
        }
        $giftRes = $this->cart_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $userid));
        $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $countVal = $SubcribRes->num_rows() + $giftRes->num_rows() + $cartVal->num_rows();

        return number_format($SubcribAmt, 2, '.', '') . '|' . number_format($SubcribSAmt, 2, '.', '') . '|' . number_format($SubcribTAmt, 2, '.', '') . '|' . number_format($SubcribTotalAmt, 2, '.', '') . '|' . $countVal;
    }

    public function mani_cart_total($userid = '',$code = '') {

        $giftRes = $this->cart_model->get_all_details(GIFTCARDS_TEMP, array('user_id' => $userid));
        $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $SubcribRes = $this->cart_model->get_all_details(FANCYYBOX_TEMP, array('user_id' => $userid));
        $referral_code = $this->cart_model->get_all_details(REFERRAL_CODE, array('referral_code' => $code));
        if(!empty($referral_code->result())){

            $is_referral = 'yes';
        }else{
            $is_referral = 'no';
        }
        $cartAmt = 0;
        $cartShippingAmt = 0;
        $cartTaxAmt = 0;
        $cartMiniMainCount = 0;
        $cartDiscAmt = 0;

        if ($cartVal->num_rows() > 0) {
            foreach ($cartVal->result() as $CartRow) {
                $qu = $this->db->query("SELECT maximumamount FROM " . COUPONCARDS . " WHERE id='" . $CartRow->couponID . "' LIMIT 1");
                $rr = $qu->result();
                $cartAmt = $cartAmt - ($CartRow->discountAmount * $CartRow->quantity) + ((($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
                $cartMiniMainCount = $cartMiniMainCount + $CartRow->quantity;
                $cartShippingAmt = $cartShippingAmt + ( $CartRow->product_shipping_cost * $CartRow->quantity );
                $cartTaxAmt = $cartTaxAmt + ($cartAmt * 0.01 * $CartRow->tax);
                $cartDiscAmt = $cartDiscAmt + ($CartRow->discountAmount * $CartRow->quantity);
                if ($rr[0]->maximumamount != 0) {
                    If ($cartDiscAmt > $rr[0]->maximumamount) {
                        $cartAmt = $cartAmt + $cartDiscAmt - $rr[0]->maximumamount;
                        $cartDiscAmt = $rr[0]->maximumamount;
                    }
                }
            }

            //$cartSAmt = $cartVal->row()->shipping_cost;
            //$cartTAmt = $cartAmt * 0.01 * $cartVal->row()->tax;
            $grantAmt = $cartAmt + $cartShippingAmt + $cartTaxAmt;
            
        }else{
           		$this->session->unset_userdata('first_tenure');
        }
        
        $countVal = $giftRes->num_rows() + $SubcribRes->num_rows() + $cartMiniMainCount;        

        $this->db->select('count(fsp.id) as product_count');
        $this->db->from('fc_shopping_carts fsp');
        $this->db->join('fc_product fp','fsp.product_id = fp.id','left');
        $this->db->where('fsp.user_id',$userid);
        $this->db->where('fp.is_addon',0);
        $cart_product_count = $this->db->get();
        $No_addon_count = $cart_product_count->row()->product_count;
        $c = $this->minicart_model->items_in_cart($userid);
        //print_r($c);exit;

        //var_dump($countVal);exit;

        //$this->db->select('discountAmount');
        //$this->db->where('user_id',$userid);
        //$query = $this->db->get(SHOPPING_CART);
        //if($query->row()->discountAmount !=''){
        //	$grantAmt = $grantAmt - $query->row()->discountAmount;
        //}
        
        if($_SESSION['Applied_Walled']){
        	$grantAmt = $grantAmt - $_SESSION['Applied_Walled'];
        }

        $proname = '';
        if(isset($_SESSION['prcity']) && $_SESSION['prcity'] != '') {
            $this->db->select('a.product_id, b.product_name, a.quantity');
            $this->db->from(SHOPPING_CART . ' as a');
            $this->db->join(PRODUCT . ' as b', 'b.id = a.product_id');
            $this->db->where('a.user_id = ' . $userid);
            $cartProductVal = $this->db->get()->result_array();

            $proNameArr = array();
            $quantityErr = array();
            if($cartProductVal) {
                foreach($cartProductVal as $cartProduct) {
                    $this->db->select('count(id) as cnt, list_value_seourl');
                    $this->db->from('fc_list_values');
                    $this->db->where('FIND_IN_SET("'.$cartProduct['product_id'].'", `products`) and id = '. $_SESSION['prcity']);
                    $productInCity = $this->db->get()->first_row();

                    if($productInCity->cnt == '0') {
                        $proNameArr[] = $cartProduct['product_name'];
                    } else {
                        $cityQuantity = $this->product_model->view_product_quantity(array('product_id' => $cartProduct['product_id']));

                        if($cartProduct['quantity'] > $cityQuantity[$_SESSION['prcity']]) {
                            $quantityErr[] = $cartProduct['product_name'];
                        }
                    }
                }
            }
            $proname = '';
            if(!empty($proNameArr)) {
                $proname = json_encode($proNameArr);
            }
            $quantityErrs = '';
            if(!empty($quantityErr)) {
                $quantityErrs = json_encode($quantityErr);
            }
        }

        return number_format($cartAmt, 2, '.', '') . '|' . number_format($cartShippingAmt, 2, '.', '') . '|' . number_format($cartTaxAmt, 2, '.', '') . '|' . number_format($grantAmt, 2, '.', '') . '|' . count($c) . '|' . number_format($cartDiscAmt, 2, '.', '').'|'.$No_addon_count.'|'.$is_referral.'|'.$proname.'|'.$quantityErrs;
    }

        public function mani_cart_coupon_sucess($userid = '') {

        $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $cartAmt = 0;
        $cartShippingAmt = 0;
        $cartTaxAmt = 0;
        $cartDisc = 0;

        if ($cartVal->num_rows() > 0) {
            $k = 0;
            foreach ($cartVal->result() as $CartRow) {
                $qu = $this->db->query("SELECT maximumamount FROM " . COUPONCARDS . " WHERE id='" . $CartRow->couponID . "' LIMIT 1");
                $rr = $qu->result();
                $cartAmt = $cartAmt - ($CartRow->discountAmount * $CartRow->quantity) + (( ($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
                $rental_amount = $rental_amount + (( ($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
                $newCartInd[] = $CartRow->indtotal;
                $cartDisc = $cartDisc + ($CartRow->discountAmount * $CartRow->quantity);
                $shippingCost = $shippingCost + ($CartRow->product_shipping_cost * $CartRow->quantity);
                $k = $k + 1;
                if ($rr[0]->maximumamount != 0) {
                    If ($cartDisc > $rr[0]->maximumamount) {
                        $cartAmt = $cartAmt + $cartDisc - $rr[0]->maximumamount;
                        $cartDisc = $rr[0]->maximumamount;
                    }
                }
            }
            if($cartVal->row()->discount_on_si != ''){
                // if( $cartVal->row()->discount_on_si < $cartAmt )
                // {
                    $cartAmt  = $cartAmt -  $cartVal->row()->discount_on_si  ;
                // }
                // else{
                //     print_r($cartAmt);
                //     // $cartAmt = $cartAmt
                // }
            }

            // print_r($cartAmt);
            // exit;
            $cartSAmt = $shippingCost;
            $cartTAmt = $cartAmt * 0.01 * $cartVal->row()->tax;
            $grantAmt = $cartAmt + $cartSAmt + $cartTAmt;
        }

        //$this->db->select('discountAmount');
        //$this->db->from(SHOPPING_CART);
        //$this->db->where('user_id = '.$userid);
        //$query = $this->db->get();
        $newAmtsValues = @implode('|', $newCartInd);


        //if($query->row()->discountAmount !=''){
        //	$grantAmt = $grantAmt - $query->row()->discountAmount;
        //}
        // $grantAmt = $grantAmt - $cartDisc;
        if($_SESSION['Applied_Walled']){
            $grantAmt = $grantAmt - $_SESSION['Applied_Walled'];
        }

        return number_format($rental_amount, 2, '.', '') . '|' . number_format($cartSAmt, 2, '.', '') . '|' . number_format($cartTAmt, 2, '.', '') . '|' . number_format($grantAmt, 2, '.', '') . '|' . number_format($cartDisc, 2, '.', '') . '|' . $k . '|' . $newAmtsValues;
    }

    
    
     public function mani_cart_referralCode_sucess($userid = ''){

        $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $cartAmt = 0;
        $cartShippingAmt = 0;
        $cartTaxAmt = 0;
        $cartDisc = 0;

        if ($cartVal->num_rows() > 0) {
            $k = 0;
            foreach ($cartVal->result() as $CartRow) {
                $qu = $this->db->query("SELECT referral_amount FROM " . REFERRAL_CODE . " WHERE id='" . $CartRow->couponID . "' LIMIT 1");
                $rr = $qu->result();
                $cartAmt = $cartAmt - ($CartRow->discountAmount * $CartRow->quantity) + (( ($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
                $newCartInd[] = $CartRow->indtotal;
                $cartDisc = $cartDisc + ($CartRow->discountAmount * $CartRow->quantity);
                $shippingCost = $shippingCost + ($CartRow->product_shipping_cost * $CartRow->quantity);
                $k = $k + 1;
            }
            $cartSAmt = $shippingCost;
            $cartTAmt = $cartAmt * 0.01 * $cartVal->row()->tax;
            $grantAmt = $cartAmt + $cartSAmt + $cartTAmt;
        }

        $newAmtsValues = @implode('|', $newCartInd);
        return number_format($cartAmt, 2, '.', '') . '|' . number_format($cartSAmt, 2, '.', '') . '|' . number_format($cartTAmt, 2, '.', '') . '|' . number_format($grantAmt, 2, '.', '') . '|' . number_format($cartDisc, 2, '.', '') . '|' . $k . '|' . $newAmtsValues;
    }

    public function view_cart_details($condition = '') {
        $select_qry = "select p.*,u.full_name,u.user_name,u.thumbnail from " . PRODUCT . " p LEFT JOIN " . USERS . " u on u.id=p.user_id " . $condition;
        $cartList = $this->ExecuteQuery($select_qry);
        return $cartList;
    }

    public function view_atrribute_details() {
        $select_qry = "select * from " . ATTRIBUTE . " where status='Active'";
        return $attList = $this->ExecuteQuery($select_qry);
    }

    public function Check_Code_Val($Code = '', $amount = '', $shipamount = '', $userid = '') {

        $code = $Code;
        $amount = $amount;
        $amountOrg = $amount;
        $ship_amount = $shipamount;

        $CoupRes = $this->cart_model->get_all_details(COUPONCARDS, array('code' => $code, 'card_status' => 'not used', 'status' => 'Active'));
        $GiftRes = $this->cart_model->get_all_details(GIFTCARDS, array('code' => $code));
        $ShopArr = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $excludeArr = array('code', 'amount', 'shipamount');
        
         foreach ($ShopArr->result() as $shopRow) {
              $check_product_price  = $check_product_price + $shopRow->price;
         }


        if ($CoupRes->num_rows() > 0) {

            //$PayVal = $this->cart_model->get_all_details(PAYMENT,array( 'user_id' => $userid, 'coupon_id' => $CoupRes->row()->id, 'status'=>'Paid' ));
            //if($PayVal->num_rows() == 0){

            if ($ShopArr->row()->couponID == 0) {

                if ($CoupRes->row()->quantity > $CoupRes->row()->purchase_count) {

                    $today = getdate();
                    $tomorrow = mktime(0, 0, 0, date("m"), date("d"), date("Y"));
                    $currDate = date("Y-d-m", $tomorrow);
                    $couponExpDate = $CoupRes->row()->dateto;

                    $curVal = (strtotime($couponExpDate) < time());
                    if ($curVal != '') {
                        echo '5';
                        exit();
                    }

                    if ($CoupRes->row()->coupon_type == "shipping") {
                        $totalamt = number_format($amount - $ship_amount, 2, '.', '');
                        $discount = '0';

                        $dataArr = array('discountAmount' => $discount,
                            'couponID' => $CoupRes->row()->id,
                            'couponCode' => $code,
                            'coupontype' => 'Free Shipping',
                            'is_coupon_used' => 'Yes',
                            'shipping_cost' => 0,
                            'total' => $totalamt);
                        $condition = array('user_id' => $userid);
                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                        echo 'Success|' . $CoupRes->row()->id . '|Shipping';
                        exit();
                    }elseif($CoupRes->row()->coupon_type == "category") {
                        $newAmt = $amount;
                        $catAry = @explode(',', $CoupRes->row()->category_id);
                        $cat_i  = 1;
                        foreach ($ShopArr->result() as $shopRow) {
                            $shopCatArr = '';

                            $shopCatArr = @explode(',', $shopRow->cate_id);

                            $combArr = array_merge($catAry, $shopCatArr);
                            $combArr1 = array_unique($combArr);
                            if (count($combArr) != count($combArr1)) {

                                if ($CoupRes->row()->price_type == 2) {
                                    if ($CoupRes->row()->maxtenure != '' || $CoupRes->row()->mintenure != '') {
                                        $q = $this->db->query("SELECT attr_name FROM " . SUBPRODUCT . " WHERE pid='" . $shopRow->attribute_values . "' LIMIT 1");
                                        if ($q->num_rows() > 0) {
                                            $r = $q->result();
                                        }
                                        $ex = explode(" ", $r[0]->attr_name);
                                        $max = explode(" ", $CoupRes->row()->maxtenure);
                                        $min = explode(" ", $CoupRes->row()->mintenure);
                                        if ($max[0] >= $ex[0] || $min[0] <= $ex[0]) {
                                            $percentage = $CoupRes->row()->price_value;
                                            $amountOrg = $shopRow->indtotal;
                                            $prodPrice = $shopRow->price;
                                            $discount = ($percentage * 0.01) * $prodPrice;
                                            if($cat_i == 1){
                                                $discount = $discount - $shopRow->discount_on_si;
                                            }
                                            $cat_i++;
                                            if ($CoupRes->row()->maximumamount != 0) {
                                                if ($discount > $CoupRes->row()->maximumamount) {
                                                    $discount = $CoupRes->row()->maximumamount;
                                                }
                                            }
                                            $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                            $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                            $dataArr = array('discountAmount' => $discount,
                                                'couponID' => $CoupRes->row()->id,
                                                'couponCode' => $code,
                                                'coupontype' => 'Category',
                                                'is_coupon_used' => 'Yes',
                                                'indtotal' => $IndAmt);
                                            $condition = array('id' => $shopRow->id);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);

                                            $dataArr1 = array('total' => $TotalAmt);
                                            $condition1 = array('user_id' => $userid);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                        } else {
                                            echo '7';
                                            exit();
                                        }
                                    } elseif ($CoupRes->row()->maxamountf != 0 || $CoupRes->row()->minamountf != 0) {
                                        $totalamtrespected = $newAmt - $ship_amount;
                                        if ($CoupRes->row()->maxamountf >= $totalamtrespected || $CoupRes->row()->minamountf <= $totalamtrespected) {
                                            $percentage = $CoupRes->row()->price_value;
                                            $amountOrg = $shopRow->indtotal;
                                            $prodPrice = $shopRow->price;
                                            $discount = ($percentage * 0.01) * $prodPrice;
                                            if ($CoupRes->row()->maximumamount != 0) {
                                                if ($discount > $CoupRes->row()->maximumamount) {
                                                    $discount = $CoupRes->row()->maximumamount;
                                                }
                                            }
                                            $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                            $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                            $dataArr = array('discountAmount' => $discount,
                                                'couponID' => $CoupRes->row()->id,
                                                'couponCode' => $code,
                                                'coupontype' => 'Category',
                                                'is_coupon_used' => 'Yes',
                                                'indtotal' => $IndAmt);
                                            $condition = array('id' => $shopRow->id);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);

                                            $dataArr1 = array('total' => $TotalAmt);
                                            $condition1 = array('user_id' => $userid);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                        } else {
                                            echo '6';
                                            exit();
                                        }
                                    } else {
                                        $percentage = $CoupRes->row()->price_value;
                                        $amountOrg = $shopRow->indtotal;
                                        $prodPrice = $shopRow->price;
                                        $discount = ($percentage * 0.01) * $prodPrice;
                                        if ($CoupRes->row()->maximumamount != 0) {
                                            if ($discount > $CoupRes->row()->maximumamount) {
                                                $discount = $CoupRes->row()->maximumamount;
                                            }
                                        }
                                        if($shopRow->discount_on_si >= $check_product_price){
                                            $discount = 0;
                                        }
                                        $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                        $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                        $dataArr = array('discountAmount' => $discount,
                                            'couponID' => $CoupRes->row()->id,
                                            'couponCode' => $code,
                                            'coupontype' => 'Category',
                                            'is_coupon_used' => 'Yes',
                                            'indtotal' => $IndAmt);
                                        $condition = array('id' => $shopRow->id);
                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);

                                        $dataArr1 = array('total' => $TotalAmt);
                                        $condition1 = array('user_id' => $userid);
                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                    }
                                } elseif ($CoupRes->row()->price_type == 1) {
                                    if ($CoupRes->row()->maxtenure != '' || $CoupRes->row()->mintenure != '') {
                                        $q = $this->db->query("SELECT attr_name FROM " . SUBPRODUCT . " WHERE pid='" . $shopRow->attribute_values . "' LIMIT 1");
                                        if ($q->num_rows() > 0) {
                                            $r = $q->result();
                                        }
                                        $ex = explode(" ", $r[0]->attr_name);
                                        $max = explode(" ", $CoupRes->row()->maxtenure);
                                        $min = explode(" ", $CoupRes->row()->mintenure);
                                        if ($max[0] >= $ex[0] && $min[0] <= $ex[0]) {
                                            $totalamtrespected = $newAmt - $ship_amount;
                                            if ($CoupRes->row()->minamountf <= $totalamtrespected) {
                                                $discount = $CoupRes->row()->price_value;
                                                $amountOrg = $shopRow->indtotal;
                                                if ($amountOrg > $discount) {
                                                    $amountOrg = number_format($amountOrg - $discount, 2, '.', '');
                                                    $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                                    $dataArr = array('discountAmount' => $discount,
                                                        'couponID' => $CoupRes->row()->id,
                                                        'couponCode' => $code,
                                                        'coupontype' => 'Category',
                                                        'is_coupon_used' => 'Yes',
                                                        'indtotal' => $amountOrg);
                                                    $condition = array('id' => $shopRow->id);
                                                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                                    $dataArr1 = array('total' => $TotalAmt);
                                                    $condition1 = array('user_id' => $userid);
                                                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                                } else {
                                                    echo '7';
                                                    exit();
                                                }
                                            } else {
                                                echo '7';
                                                exit();
                                            }
                                        } else {
                                            echo '7';
                                            exit();
                                        }
                                    }
                                }
                            }
                        }
                        echo 'Success|' . $CoupRes->row()->id . '|Category|'. $CoupRes->row()->cart_page_label;
                        exit();
                    } elseif ($CoupRes->row()->coupon_type == "product") {
                        $PrdArr = @explode(',', $CoupRes->row()->product_id);
                        $newAmt = $amount;
                        foreach ($ShopArr->result() as $shopRow) {

                            $shopPrd = $shopRow->product_id;

                            if (in_array($shopPrd, $PrdArr) == 1) {

                                if ($CoupRes->row()->price_type == 2) {
                                    if ($CoupRes->row()->maxtenure != '' || $CoupRes->row()->mintenure != '') {
                                        $q = $this->db->query("SELECT attr_name FROM " . SUBPRODUCT . " WHERE pid='" . $shopRow->attribute_values . "' LIMIT 1");
                                        if ($q->num_rows() > 0) {
                                            $r = $q->result();
                                        }
                                        $ex = explode(" ", $r[0]->attr_name);
                                        $max = explode(" ", $CoupRes->row()->maxtenure);
                                        $min = explode(" ", $CoupRes->row()->mintenure);
                                        if ($max[0] >= $ex[0] && $min[0] <= $ex[0]) {
                                            if ($CoupRes->row()->maxamountf != 0 || $CoupRes->row()->minamountf != 0) {
                                                $totalamtrespected = $newAmt - $ship_amount;
                                                if ($CoupRes->row()->maxamountf >= $totalamtrespected || $CoupRes->row()->minamountf <= $totalamtrespected) {
                                                    $percentage = $CoupRes->row()->price_value;
                                                    $amountOrg = $shopRow->indtotal;
                                                    $prodPrice = $shopRow->price;
                                                    $discount = ($percentage * 0.01) * $prodPrice;
                                                    if ($CoupRes->row()->maximumamount != 0) {
                                                        //$discount = ($shopRow->quantity>1) ? ($discount * $shopRow->quantity) : $discount;
                                                        if ($discount > $CoupRes->row()->maximumamount) {
                                                            $discount = $CoupRes->row()->maximumamount;
                                                        }
                                                    }
                                                    $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                                    $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                                    $dataArr = array('discountAmount' => $discount,
                                                        'couponID' => $CoupRes->row()->id,
                                                        'couponCode' => $code,
                                                        'coupontype' => 'Product',
                                                        'is_coupon_used' => 'Yes',
                                                        'indtotal' => $IndAmt);
                                                    $condition = array('id' => $shopRow->id);

                                                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                                    $dataArr1 = array('total' => $TotalAmt);
                                                    $condition1 = array('user_id' => $userid);
                                                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                                } else {
                                                    echo '6';
                                                    exit();
                                                }
                                            } else {
                                                $percentage = $CoupRes->row()->price_value;
                                                $amountOrg = $shopRow->indtotal;
                                                $prodPrice = $shopRow->price;
                                                $discount = ($percentage * 0.01) * $prodPrice;
                                                if ($CoupRes->row()->maximumamount != 0) {
                                                    //$discount = ($shopRow->quantity>1) ? ($discount * $shopRow->quantity) : $discount;
                                                    if ($discount > $CoupRes->row()->maximumamount) {
                                                        $discount = $CoupRes->row()->maximumamount;
                                                    }
                                                }
                                                $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                                $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                                $dataArr = array('discountAmount' => $discount,
                                                    'couponID' => $CoupRes->row()->id,
                                                    'couponCode' => $code,
                                                    'coupontype' => 'Product',
                                                    'is_coupon_used' => 'Yes',
                                                    'indtotal' => $IndAmt);
                                                $condition = array('id' => $shopRow->id);

                                                $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                                $dataArr1 = array('total' => $TotalAmt);
                                                $condition1 = array('user_id' => $userid);
                                                $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                            }
                                        } else {
                                            echo '6';
                                            exit();
                                        }
                                    } elseif ($CoupRes->row()->maxamountf != 0 || $CoupRes->row()->minamountf != 0) {
                                        $totalamtrespected = $newAmt - $ship_amount;
                                        if ($CoupRes->row()->maxamountf >= $totalamtrespected || $CoupRes->row()->minamountf <= $totalamtrespected) {
                                            $percentage = $CoupRes->row()->price_value;
                                            $amountOrg = $shopRow->indtotal;
                                            $prodPrice = $shopRow->price;
                                            $discount = ($percentage * 0.01) * $prodPrice;
                                            if ($CoupRes->row()->maximumamount != 0) {
                                                //$discount = ($shopRow->quantity>1) ? ($discount * $shopRow->quantity) : $discount;
                                                if ($discount > $CoupRes->row()->maximumamount) {
                                                    $discount = $CoupRes->row()->maximumamount;
                                                }
                                            }
                                            $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                            $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                            $dataArr = array('discountAmount' => $discount,
                                                'couponID' => $CoupRes->row()->id,
                                                'couponCode' => $code,
                                                'coupontype' => 'Product',
                                                'is_coupon_used' => 'Yes',
                                                'indtotal' => $IndAmt);
                                            $condition = array('id' => $shopRow->id);

                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                            $dataArr1 = array('total' => $TotalAmt);
                                            $condition1 = array('user_id' => $userid);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                        } else {
                                            echo '6';
                                            exit();
                                        }
                                    } else {
                                        $percentage = $CoupRes->row()->price_value;
                                        $amountOrg = $shopRow->indtotal;
                                        $prodPrice = $shopRow->price;
                                        $discount = ($percentage * 0.01) * $prodPrice;
                                        if ($CoupRes->row()->maximumamount != 0) {
                                            //$discount = ($shopRow->quantity>1) ? ($discount * $shopRow->quantity) : $discount;
                                            if ($discount > $CoupRes->row()->maximumamount) {
                                                $discount = $CoupRes->row()->maximumamount;
                                            }
                                        }
                                        $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                        $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                        $dataArr = array('discountAmount' => $discount,
                                            'couponID' => $CoupRes->row()->id,
                                            'couponCode' => $code,
                                            'coupontype' => 'Product',
                                            'is_coupon_used' => 'Yes',
                                            'indtotal' => $IndAmt);
                                        $condition = array('id' => $shopRow->id);

                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                        $dataArr1 = array('total' => $TotalAmt);
                                        $condition1 = array('user_id' => $userid);
                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                    }
                                } elseif ($CoupRes->row()->price_type == 1) {
                                    if ($CoupRes->row()->maxtenure != '' || $CoupRes->row()->mintenure != '') {
                                        $q = $this->db->query("SELECT attr_name FROM " . SUBPRODUCT . " WHERE pid='" . $shopRow->attribute_values . "' LIMIT 1");
                                        if ($q->num_rows() > 0) {
                                            $r = $q->result();
                                        }
                                        $ex = explode(" ", $r[0]->attr_name);
                                        $max = explode(" ", $CoupRes->row()->maxtenure);
                                        $min = explode(" ", $CoupRes->row()->mintenure);
                                        if ($max[0] >= $ex[0] && $min[0] <= $ex[0]) {
                                            $totalamtrespected = $newAmt - $ship_amount;
                                            if ($CoupRes->row()->minamountf <= $totalamtrespected) {
                                                $discount = $CoupRes->row()->price_value;
                                                $amountOrg = $shopRow->indtotal;
                                                if ($amountOrg > $discount) {
                                                    $amountOrg = number_format($amountOrg - $discount, 2, '.', '');
                                                    $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                                    $dataArr = array('discountAmount' => $discount,
                                                        'couponID' => $CoupRes->row()->id,
                                                        'couponCode' => $code,
                                                        'coupontype' => 'Category',
                                                        'is_coupon_used' => 'Yes',
                                                        'indtotal' => $amountOrg);
                                                    $condition = array('id' => $shopRow->id);
                                                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                                    $dataArr1 = array('total' => $TotalAmt);
                                                    $condition1 = array('user_id' => $userid);
                                                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                                } else {
                                                    echo '7';
                                                    exit();
                                                }
                                            } else {
                                                echo '7';
                                                exit();
                                            }
                                        } else {
                                            echo '7';
                                            exit();
                                        }
                                    }
                                }
                            }
                        }
                        echo 'Success|' . $CoupRes->row()->id . '|Product';
                        exit();
                    } else if ($CoupRes->row()->coupon_type == "seller" || $CoupRes->row()->coupon_type == "exclusive") {
                        $coupUserIdArr = @explode(',', $CoupRes->row()->user_id);
                        $newAmt = $amount;
                        foreach ($ShopArr->result() as $shopRow) {
                            $sellerId = $shopRow->sell_id;
                            if (in_array($sellerId, $coupUserIdArr) == 1) {
                                if ($CoupRes->row()->price_type == 2) {
                                    if ($CoupRes->row()->maxtenure != '' || $CoupRes->row()->mintenure != '') {
                                        $q = $this->db->query("SELECT attr_name FROM " . SUBPRODUCT . " WHERE pid='" . $shopRow->attribute_values . "' LIMIT 1");
                                        if ($q->num_rows() > 0) {
                                            $r = $q->result();
                                        }
                                        $ex = explode(" ", $r[0]->attr_name);
                                        $max = explode(" ", $CoupRes->row()->maxtenure);
                                        $min = explode(" ", $CoupRes->row()->mintenure);
                                        if ($max[0] >= $ex[0] || $min[0] <= $ex[0]) {
                                            $percentage = $CoupRes->row()->price_value;
                                            $amountOrg = $shopRow->indtotal;
                                            $prodPrice = $shopRow->price;
                                            $discount = ($percentage * 0.01) * $prodPrice;
                                            if ($CoupRes->row()->maximumamount != 0) {
                                                if ($discount > $CoupRes->row()->maximumamount) {
                                                    $discount = $CoupRes->row()->maximumamount;
                                                }
                                            }
                                            $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                            $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                            $dataArr = array('discountAmount' => $discount,
                                                'couponID' => $CoupRes->row()->id,
                                                'couponCode' => $code,
                                                'coupontype' => 'Seller',
                                                'is_coupon_used' => 'Yes',
                                                'indtotal' => $IndAmt);
                                            $condition = array('id' => $shopRow->id);

                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                            $dataArr1 = array('total' => $TotalAmt);
                                            $condition1 = array('user_id' => $userid);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                        } else {
                                            echo '7';
                                            exit();
                                        }
                                    } elseif ($CoupRes->row()->maxamountf != 0 || $CoupRes->row()->minamountf != 0) {
                                        $totalamtrespected = $newAmt - $ship_amount;
                                        if ($CoupRes->row()->maxamountf >= $totalamtrespected || $CoupRes->row()->minamountf <= $totalamtrespected) {
                                            $percentage = $CoupRes->row()->price_value;
                                            $amountOrg = $shopRow->indtotal;
                                            $prodPrice = $shopRow->price;
                                            $discount = ($percentage * 0.01) * $prodPrice;
                                            if ($CoupRes->row()->maximumamount != 0) {
                                                if ($discount > $CoupRes->row()->maximumamount) {
                                                    $discount = $CoupRes->row()->maximumamount;
                                                }
                                            }
                                            $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                            $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                            $dataArr = array('discountAmount' => $discount,
                                                'couponID' => $CoupRes->row()->id,
                                                'couponCode' => $code,
                                                'coupontype' => 'Seller',
                                                'is_coupon_used' => 'Yes',
                                                'indtotal' => $IndAmt);
                                            $condition = array('id' => $shopRow->id);

                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                            $dataArr1 = array('total' => $TotalAmt);
                                            $condition1 = array('user_id' => $userid);
                                            $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                        } else {
                                            echo '6';
                                            exit();
                                        }
                                    } else {
                                        $percentage = $CoupRes->row()->price_value;
                                        $amountOrg = $shopRow->indtotal;
                                        $prodPrice = $shopRow->price;
                                        $discount = ($percentage * 0.01) * $prodPrice;
                                        if ($CoupRes->row()->maximumamount != 0) {
                                            if ($discount > $CoupRes->row()->maximumamount) {
                                                $discount = $CoupRes->row()->maximumamount;
                                            }
                                        }
                                        $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                                        $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                        $dataArr = array('discountAmount' => $discount,
                                            'couponID' => $CoupRes->row()->id,
                                            'couponCode' => $code,
                                            'coupontype' => 'Seller',
                                            'is_coupon_used' => 'Yes',
                                            'indtotal' => $IndAmt);
                                        $condition = array('id' => $shopRow->id);

                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                        $dataArr1 = array('total' => $TotalAmt);
                                        $condition1 = array('user_id' => $userid);
                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                    }
                                } elseif ($CoupRes->row()->price_type == 1) {

                                    $discount = $CoupRes->row()->price_value;
                                    $amountOrg = $shopRow->indtotal;
                                    if ($amountOrg > $discount) {
                                        $newDisAmt = number_format($amountOrg - $discount, 2, '.', '');
                                        $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                                        $dataArr = array('discountAmount' => $discount,
                                            'couponID' => $CoupRes->row()->id,
                                            'couponCode' => $code,
                                            'coupontype' => 'Seller',
                                            'is_coupon_used' => 'Yes',
                                            'indtotal' => $newDisAmt);

                                        $condition = array('id' => $shopRow->id);
                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                        $dataArr1 = array('total' => $TotalAmt);
                                        $condition1 = array('user_id' => $userid);
                                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);
                                    } else {
                                        echo '7';
                                        exit();
                                    }
                                }
                            }
                        }
                        echo 'Success|' . $CoupRes->row()->id . '|Seller';
                        exit();
                    } else {

                        if ($CoupRes->row()->price_type == 2) {
                            if ($CoupRes->row()->maxtenure != '' || $CoupRes->row()->mintenure != '') {
                                $shopRow = $ShopArr->result();
                                $q = $this->db->query("SELECT attr_name FROM " . SUBPRODUCT . " WHERE pid='" . $shopRow[0]->attribute_values . "' LIMIT 1");
                                if ($q->num_rows() > 0) {
                                    $r = $q->result();
                                }
                                $ex = explode(" ", $r[0]->attr_name);
                                $max = explode(" ", $CoupRes->row()->maxtenure);
                                $min = explode(" ", $CoupRes->row()->mintenure);
                                if ($max[0] >= $ex[0] || $min[0] <= $ex[0]) {
                                    $percentage = $CoupRes->row()->price_value;
                                    $discount = ($percentage * 0.01) * $amount;
                                    if ($discount > $CoupRes->row()->maximumamount) {
                                        $discount = $CoupRes->row()->maximumamount;
                                    }
                                    $totalAmt = number_format($amount - $discount, 2, '.', '');

                                    $dataArr = array('discountAmount' => $discount,
                                        'couponID' => $CoupRes->row()->id,
                                        'couponCode' => $code,
                                        'coupontype' => 'Cart',
                                        'is_coupon_used' => 'Yes',
                                        'total' => $totalAmt);
                                    $condition = array('user_id' => $userid);

                                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);

                                    echo 'Success|' . $CoupRes->row()->id;
                                    exit();
                                } else {
                                    echo '8';
                                    exit();
                                }
                            } elseif ($CoupRes->row()->maxamountf != 0 || $CoupRes->row()->minamountf != 0) {
                                $totalamtrespected = $amount - $ship_amount;
                                if ($CoupRes->row()->maxamountf >= $totalamtrespected || $CoupRes->row()->minamountf <= $totalamtrespected) {
                                    $percentage = $CoupRes->row()->price_value;
                                    $discount = ($percentage * 0.01) * $amount;
                                    if ($discount > $CoupRes->row()->maximumamount) {
                                        $discount = $CoupRes->row()->maximumamount;
                                    }
                                    $totalAmt = number_format($amount - $discount, 2, '.', '');

                                    $dataArr = array('discountAmount' => $discount,
                                        'couponID' => $CoupRes->row()->id,
                                        'couponCode' => $code,
                                        'coupontype' => 'Cart',
                                        'is_coupon_used' => 'Yes',
                                        'total' => $totalAmt);
                                    $condition = array('user_id' => $userid);

                                    $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);

                                    echo 'Success|' . $CoupRes->row()->id;
                                    exit();
                                } else {
                                    echo '6';
                                    exit();
                                }
                            } else {
                                
                                $percentage = $CoupRes->row()->price_value;
                                
                                $discount = ($percentage * 0.01) * $amount;
                                
                                $totalAmt = number_format($amount - $discount, 2, '.', '');

                                $dataArr = array('discountAmount' => $discount,
                                    'couponID' => $CoupRes->row()->id,
                                    'couponCode' => $code,
                                    'coupontype' => 'Cart',
                                    'is_coupon_used' => 'Yes',
                                    'total' => $totalAmt);
                                $condition = array('user_id' => $userid);

                                $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);

                                /* foreach($ShopArr->result() as $shopRow){
                                  $amountOrg = $shopRow->indtotal;
                                  $discount = ($percentage * 0.01) * $amountOrg;
                                  $IndAmt = number_format($amountOrg - $discount,2,'.','');

                                  $dataArr = array('indtotal' => $IndAmt);
                                  $condition =array('id' => $shopRow->id);
                                  $this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr,$condition);
                                  } */

                                echo 'Success|' . $CoupRes->row()->id;
                                exit();
                            }
                        } elseif ($CoupRes->row()->price_type == 1) {

                            $discount = $CoupRes->row()->price_value;
                            if ($amount > $discount) {
                                $amountOrg = number_format($amount - $discount, 2, '.', '');
                                $dataArr = array('discountAmount' => $discount,
                                    'couponID' => $CoupRes->row()->id,
                                    'couponCode' => $code,
                                    'coupontype' => 'Cart',
                                    'is_coupon_used' => 'Yes',
                                    'total' => $amountOrg);
                                $condition = array('user_id' => $userid);
                                
                                $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
                                /* $newDisAmt = ($discount / $ShopArr->num_rows());
                                  foreach($ShopArr->result() as $shopRow){
                                  $amountOrg = $shopRow->indtotal;
                                  $IndAmt = number_format($amountOrg - $newDisAmt,2,'.','');
                                  $dataArr = array('indtotal' => $IndAmt);
                                  $condition =array('id' => $shopRow->id);
                                  $this->cart_model->commonInsertUpdate(SHOPPING_CART,'update',$excludeArr,$dataArr,$condition);
                                  } */


                                echo 'Success|' . $CoupRes->row()->id;
                                exit();
                            } else {
                                echo '7';
                                exit();
                            }
                        }
                    }
                } else {
                    echo '6';
                    exit();
                }
            } else {
                echo '2';
                exit();
            }


            //}else{
            //	echo '2';
            //	exit();
            //}
        } elseif ($GiftRes->num_rows() > 0) {

            $curGiftVal = (strtotime($GiftRes->row()->expiry_date) < time());
            if ($curGiftVal != '') {
                echo '8';
                exit();
            }

            if ($GiftRes->row()->price_value > $GiftRes->row()->used_amount) {

                $NewGiftAmt = $GiftRes->row()->price_value - $GiftRes->row()->used_amount;
                if ($amount > $NewGiftAmt) {
                    $amountOrg = $amountOrg - $NewGiftAmt;

                    $dataArr = array('discountAmount' => $NewGiftAmt,
                        'couponID' => $GiftRes->row()->id,
                        'couponCode' => $code,
                        'coupontype' => 'Gift',
                        'is_coupon_used' => 'Yes',
                        'total' => $amountOrg);
                    $condition = array('user_id' => $userid);
                    $this->cart_model->update_details(SHOPPING_CART, $dataArr, $condition);



                    /* $newDisAmt = ($NewGiftAmt / $ShopArr->num_rows());
                      //echo '<pre>'; print_r($ShopArr->result_array());
                      foreach($ShopArr->result() as $shopRow){
                      $IndAmt = number_format($shopRow->indtotal - $newDisAmt,2,'.','');
                      $dataArr = array('indtotal' => $IndAmt);
                      $condition =array('id' => $shopRow->id);
                      $this->cart_model->update_details(SHOPPING_CART,$dataArr,$condition);
                      //echo '<pre>'.$this->db->last_query();
                      } */
                } else {
                    $dataArr = array('discountAmount' => $amountOrg,
                        'couponID' => $GiftRes->row()->id,
                        'couponCode' => $code,
                        'coupontype' => 'Gift',
                        'is_coupon_used' => 'Yes',
                        'total' => '0');
                    $condition = array('user_id' => $userid);
                    $this->cart_model->update_details(SHOPPING_CART, $dataArr, $condition);

                    /* $newDisAmt = ($amountOrg / $ShopArr->num_rows());

                      foreach($ShopArr->result() as $shopRow){
                      $amountOrg = $shopRow->indtotal;
                      $IndAmt = number_format($amountOrg - $newDisAmt,2,'.','');
                      $dataArr = array('indtotal' => '0');
                      $condition =array('id' => $shopRow->id);
                      $this->cart_model->update_details(SHOPPING_CART,$dataArr,$condition);
                      } */
                }

                echo 'Success|' . $GiftRes->row()->id . '|Gift';
                exit();
            } else {
                echo '2';
                exit();
            }
        } else {
            $referral_code = $this->cart_model->get_all_details(REFERRAL_CODE,array('referral_code' => $Code));
            $referral_setting = $this->cart_model->get_all_details(REFERRAL_SETTING,array('id' => 1));
            $current_time_check  = date("Y-m-d h:i:s");
            if(!empty($referral_code->result())){
                $used_by = unserialize($referral_code->row()->used_by);
                foreach ($used_by as $key => $value) {
                    if($userid ==  $value['user_id']){
                        echo '2';
                        exit();
                    }
                }
                if($referral_code->row()->user_id == $userid ){
                    echo '1';
                    exit();
                }elseif($referral_code->row()->cart_count < 1){
                    echo '6';
                    exit();
                }elseif(strtotime($current_time_check) > strtotime($referral_code->row()->expire_date)){
                        echo '5';
                        exit();
                }
                else{
                    $rental_amount = $amount - $ship_amount;
                    $newAmt = $amount;
                    if($rental_amount > $referral_setting->row()->amount){
                        $referral_amount = $referral_setting->row()->amount;
                    }else{
                         $referral_amount = $rental_amount;
                    }
                    $discount = $referral_amount / count($ShopArr->result());
                    foreach ($ShopArr->result() as $key => $shopRow) {
                        $amountOrg = $shopRow->indtotal;
                        $prodPrice = $shopRow->price;
                        $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                        $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                        $dataArr = array('discountAmount' => $discount / $shopRow->quantity,
                            'couponID' => $referral_code->row()->id,
                            'couponCode' => $referral_code->row()->referral_code,
                            'coupontype' => 'Product',
                            'is_coupon_used' => 'Yes',
                            'indtotal' => $IndAmt);
                        $condition = array('id' => $shopRow->id);
                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);

                        $dataArr1 = array('total' => $TotalAmt);
                        $condition1 = array('user_id' => $userid);
                        $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);

                        $dataArr_referral = array('cart_count' => $referral_code->row()->cart_count - 1 );
                        $condition_referral = array('id' => $referral_code->row()->id);
                        $this->cart_model->commonInsertUpdate(REFERRAL_CODE, 'update', $excludeArr, $dataArr_referral, $condition_referral);

                    }
                    echo 'Referral_Success|' .  $referral_code->row()->id . '|Referral';
                    exit();
                }
            }else{
                echo '1';
                exit();

            }
           
        }
    }

    public function Check_Code_Val_Remove($userid = '',$code = '') {

        $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $cartAmt = 0;
        if ($cartVal->num_rows() > 0) {
            foreach ($cartVal->result() as $CartRow) {
                $cartAmt = $cartAmt + (($CartRow->product_shipping_cost + ($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
            }
            $cartVal1 = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
            $excludeArr = array('code');
            foreach ($cartVal1->result() as $CartRow1) {
                $dataArr = array('discountAmount' => 0,
                    'couponID' => 0,
                    'indtotal' => ($CartRow1->price + $CartRow1->product_shipping_cost) * $CartRow1->quantity,
                    'total' => $cartAmt,
                    'couponCode' => '',
                    'coupontype' => '',
                    'is_coupon_used' => 'No');
                $condition = array('id' => $CartRow1->id);
                $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);
            }
            
            $codeVal = $this->cart_model->get_all_details(REFERRAL_CODE, array('referral_code' => $code));
            if(!empty($codeVal)){
                $condition_referral = array('referral_code' => $code);
                $excludeArr = array('code');
                $dataArr_referral = array('cart_count' => $codeVal->row()->cart_count + 1);
                $this->cart_model->commonInsertUpdate(REFERRAL_CODE, 'update', $excludeArr, $dataArr_referral, $condition_referral);

            }
            return;
        }
    }

     public function addPaymentCart($userid = '', $payu_type = 0,$recurring_type = '') {
        $this->db->select('a.*,b.city,b.state,b.country,b.postal_code');
        $this->db->from(SHOPPING_CART . ' as a');
        $this->db->join(SHIPPING_ADDRESS . ' as b', 'a.user_id = b.user_id and a.user_id = "' . $userid . '" and b.id="' . $this->input->post('Ship_address_val') . '"');
        $AddPayt = $this->db->get();

        if ($this->session->userdata('randomNo') != '') {
            $delete = 'delete from ' . PAYMENT . ' where dealCodeNumber = "' . $this->session->userdata('randomNo') . '" and user_id = "' . $userid . '" ';
            $this->ExecuteQuery($delete, 'delete');
            $dealCodeNumber = $this->session->userdata('randomNo');
        } else {
            $dealCodeNumber = mt_rand();
        }
        
        $ReturnData = $this->CheckOriginalPriceAfterCoupon($userid);

        $insertIds = array();
         foreach ($AddPayt->result() as $key => $result) {
             $cartAmt = $cartAmt - ($result->discountAmount * $result->quantity) + ((($result->product_tax_cost * 0.01 * $result->price ) + $result->price) * $result->quantity);
                $cartMiniMainCount = $cartMiniMainCount + $result->quantity;
                $cartShippingAmt = $cartShippingAmt + ( $result->product_shipping_cost * $result->quantity );
                $cartTaxAmt = $cartTaxAmt + ($cartAmt * 0.01 * $result->tax);
                $cartDiscAmt = $cartDiscAmt + ($result->discountAmount * $result->quantity);
        }
        // $cartDiscAmt
        $grantAmt = $cartAmt + $cartShippingAmt + $cartTaxAmt  - $AddPayt->row()->discount_on_si;
        foreach ($AddPayt->result() as $result) {
           
            if ($this->input->post('is_gift') == '') {
                $ordergift = 0;
            } else {
                $ordergift = 1;
            }

            $sumTotal = number_format((($result->price + $result->product_shipping_cost + ($result->product_tax_cost * 0.01 * $result->price)) * $result->quantity), 2, '.', '');

            $uncleanNote = $this->input->post('note');
            $cleanNote = mysqli_escape_string($uncleanNote);

            $insert = ' insert into ' . PAYMENT . ' set
								product_id = "' . $result->product_id . '",
								sell_id = "' . $result->sell_id . '",								
								price = "' . $result->price . '",
								quantity = "' . $result->quantity . '",
								indtotal = "' . $result->indtotal . '",
								shippingcountry = "' . $result->country . '",
								shippingid = "' . $this->input->post('Ship_address_val') . '",
								shippingstate = "' . $result->state . '",
								shippingcity = "' . $result->city . '",
							    shippingcost = "' . $ReturnData['cartSAmt'] . '",
								tax = "' . $this->input->post('cart_tax_amount') . '",
								product_shipping_cost = "' . $result->product_shipping_cost . '",
								product_tax_cost = "' . $result->product_tax_cost . '",																												
								coupon_id  = "' . $result->couponID . '",
							    discountAmount = "' . $ReturnData['cartDisc'] . '",
								discount_on_si = "' . $AddPayt->row()->discount_on_si . '",
								couponCode  = "' . $result->couponCode . '",
								coupontype = "' . $result->coupontype . '",
								sumtotal = "' . $sumTotal . '",
								user_id = "' . $result->user_id . '",
								created = now(),
								dealCodeNumber = "' . $dealCodeNumber . '",
								status = "Pending",
								payment_type = "",
								attribute_values = "' . $result->attribute_values . '",
								shipping_status = "Pending",
								total  = "' . $ReturnData['grantAmt'] . '",
								note = "' . $cleanNote . '", 
								order_gift = "' . $ordergift . '", 
								inserttime = "' . time() . '",
                                is_recurring = "'.$payu_type.'",
                                recurring_type = "'.$recurring_type.'"';


            $insertIds[] = $this->cart_model->ExecuteQuery($insert, 'insert');
        }

        $query = $this->db->query("SELECT full_name,email FROM " . USERS . " WHERE id='" . $result->user_id . "' LIMIT 1");
        $userdet = $query->result();
        $paymtdata = array(
            'randomNo' => $dealCodeNumber,
            'randomIds' => $insertIds,
            'sessprice' => $sumTotal,
            'cartusername' => $userdet[0]->full_name,
            'cartuseremail' => $userdet[0]->email,
            'cartuserid' => $result->user_id
        );
        $this->session->set_userdata($paymtdata);

        return $insertIds;
    }
    
    public function CheckOriginalPriceAfterCoupon($userid = '') {
        $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $cartAmt = 0;
        $cartShippingAmt = 0;
        $cartTaxAmt = 0;
        $cartDisc = 0;

        if ($cartVal->num_rows() > 0) {
            $k = 0;
            foreach ($cartVal->result() as $CartRow) {
                $qu = $this->db->query("SELECT maximumamount FROM " . COUPONCARDS . " WHERE id='" . $CartRow->couponID . "' LIMIT 1");
                $rr = $qu->result();
                $cartAmt = $cartAmt - ($CartRow->discountAmount * $CartRow->quantity) + (( ($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
                $rental_amount = $rental_amount + (( ($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
                $newCartInd[] = $CartRow->indtotal;
                $cartDisc = $cartDisc + ($CartRow->discountAmount * $CartRow->quantity);
                $shippingCost = $shippingCost + ($CartRow->product_shipping_cost * $CartRow->quantity);
                $k = $k + 1;
                if ($rr[0]->maximumamount != 0) {
                    If ($cartDisc > $rr[0]->maximumamount) {
                        $cartAmt = $cartAmt + $cartDisc - $rr[0]->maximumamount;
                        $cartDisc = $rr[0]->maximumamount;
                    }
                }
            }
            if($cartVal->row()->discount_on_si != ''){
                $cartAmt  = $cartAmt -  $cartVal->row()->discount_on_si  ;
            }
            $cartSAmt = $shippingCost;
            $cartTAmt = $cartAmt * 0.01 * $cartVal->row()->tax;
            $grantAmt = $cartAmt + $cartSAmt + $cartTAmt;
        }

        $newAmtsValues = @implode('|', $newCartInd);
        $returnData = [];
        $returnData['rental_amount'] = $rental_amount;
        $returnData['cartSAmt'] = $cartSAmt;
        $returnData['cartTAmt'] = $cartTAmt;
        $returnData['grantAmt'] = $grantAmt;
        $returnData['cartDisc'] = $cartDisc;
        $returnData['cartDisc'] = $cartDisc;
        $returnData['newAmtsValues'] = $newAmtsValues;
        // print_r($returnData);exit;
        return $returnData;
    }

    public function addPaymentSubscribe($userid = '') {

        if ($this->session->userdata('InvoiceNo') != '') {
            $InvoiceNo = $this->session->userdata('InvoiceNo');
        } else {
            $InvoiceNo = mt_rand();
        }

        $paymtdata = array('InvoiceNo' => $InvoiceNo);
        $this->session->set_userdata($paymtdata);

        $dataArr = array('invoice_no' => $InvoiceNo,
            'shipping_id' => $this->input->post('SubShip_address_val'),
            'shipping_cost' => $this->input->post('subcrib_ship_amount'),
            'tax' => $this->input->post('subcrib_tax_amount'),
            'total' => $this->input->post('subcrib_total_amount'),
        );
        $condition = array('user_id' => $userid);
        $this->cart_model->update_details(FANCYYBOX_TEMP, $dataArr, $condition);


        return;
    }

    public function check_is_addon_prod($product_id) {
        $this->db->select('id,addons,is_addon');
        $this->db->from(PRODUCT);
        $this->db->where('status', 'Publish');
        $this->db->where('id', $product_id);
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_price($product_id)
    {
       $this->db->select('*');
       $this->db->from(SUBPRODUCT);
       $this->db->where('product_id',$product_id);
       $this->db->where('attr_name','24 Months');
       $query = $this->db->get();
       return $query->result()[0];
    }
    public function get_ship_value($id)
    {
        $this->db->select('*');
        $this->db->from('fc_shipping_address');
        $this->db->where('id',$id);
        $data = $this->db->get();
        return $data->result()[0];
    }
    public function get_coupon_value($id)
    {
        $this->db->select('couponCode');
        $this->db->from('fc_shopping_carts');
        $this->db->where('user_id',$id);
        $this->db->where('couponCode != ""');
        $data = $this->db->get();
        return $data->row()->couponCode;
    }
    public function get_product_quantity($user_id){
        $this->db->select('fsp.product_id as product_id,fsp.quantity as quantity');
        $this->db->from('fc_shopping_carts fsp');
        $this->db->join('fc_product fp','fp.id = fsp.product_id');
        $this->db->where('fsp.user_id',$user_id);
        $this->db->where('fp.is_addon',1);
        $result = $this->db->get();
        //var_dump($result->result());
        $data['count'] =  count($result->result());
        $data['result']=  $result->result();
         return $data;
    }
    
    public function Get_Cart_Value($user_id){
        $this->db->select('a.*');
        $this->db->from(SHOPPING_CART . ' as a');
        $this->db->join(PRODUCT . ' as b', 'b.id = a.product_id');
        $this->db->join(SUBPRODUCT . ' as d', 'd.pid = a.attribute_values', 'left');
        $this->db->join(PRODUCT_ATTRIBUTE . ' as c', 'c.id = d.attr_id', 'left');
        $this->db->where('a.user_id = ' . $user_id);
        $cartVal = $this->db->get();
        return $cartVal;
    }
    public function get_Order_Settings(){
        $this->db->select('RentalAmount,OrderRentalStatus');
        $this->db->from('fc_admin_settings');
        $data = $this->db->get();
        return $data->row();
    }
    
    public function Discount_on_si_cc($user_id,$operation,$discount = ''){
        $ShopArr = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $user_id));
        $totalDiscount = 0;
        $individulaPrice = 0;
        foreach ($ShopArr->result() as $key => $value) {
            $totalDiscount += $value->discountAmount;
            $individulaPrice += $value->price;
        }
        if($totalDiscount > 0 ){
            $individulaPrice = $individulaPrice - $totalDiscount;
        }
        if($discount >  $individulaPrice){
            $discount = $individulaPrice;
        }
         foreach ($ShopArr->result() as $key => $shopRow) {
            $amountOrg = $shopRow->indtotal;
            $prodPrice = $shopRow->price;
            if($operation == 'add'){
                $IndAmt = number_format($amountOrg - $discount, 2, '.', '');
                $TotalAmt = $newAmt = number_format($newAmt - $discount, 2, '.', '');
                $dataArr = array('discount_on_si' => $discount,
                    'indtotal' => $IndAmt);
                $condition = array('id' => $shopRow->id);
                $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);

                $dataArr1 = array('total' => $TotalAmt);
                $condition1 = array('user_id' => $userid);
                $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);    
            }else{
                $discount = $ShopArr->row()->discount_on_si;
                $IndAmt = number_format($amountOrg + $discount, 2, '.', '');
                $TotalAmt = $newAmt = number_format($newAmt + $discount, 2, '.', '');
                $dataArr = array('discount_on_si' => '',
                    'indtotal' => $IndAmt);
                $condition = array('id' => $shopRow->id);
                $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr, $condition);

                $dataArr1 = array('total' => $TotalAmt);
                $condition1 = array('user_id' => $userid);
                $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);    
            }

        }
    }


    public function getCheckDiscount($userid){
        $cartVal = $this->cart_model->get_all_details(SHOPPING_CART, array('user_id' => $userid));
        $cartAmt = 0;
        $cartShippingAmt = 0;
        $cartTaxAmt = 0;
        $cartDisc = 0;
        $discount_on_si = 0;
        if ($cartVal->num_rows() > 0) {
            $k = 0;
            foreach ($cartVal->result() as $CartRow) {

                    $qu = $this->db->query("SELECT maximumamount FROM " . COUPONCARDS . " WHERE id='" . $CartRow->couponID . "' LIMIT 1");
                    $rr = $qu->result();
                    $quantity +=  $CartRow->quantity;
                    $cart_amount_without_discount += $CartRow->price  * $CartRow->quantity;
                    $cart_shipping_amount_without_discount += ( $CartRow->price + $CartRow->product_shipping_cost) * $CartRow->quantity;
                    $cartAmt = $cartAmt + (($CartRow->price - $CartRow->discountAmount + ($CartRow->price * 0.01 * $CartRow->product_tax_cost)) * $CartRow->quantity);
                    $advance_rental =  $advance_rental + ((($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
                    $cartShippingAmt = $cartShippingAmt + ($CartRow->product_shipping_cost * $CartRow->quantity);
                    $cartTaxAmt = $cartTaxAmt + ($CartRow->product_tax_cost * $CartRow->quantity);
                    $cartQty = $cartQty + $CartRow->quantity;
                    $cartDiscountAmt = $cartDiscountAmt + ($CartRow->discountAmount * $CartRow->quantity);
                    $newCartInd[] = $CartRow->indtotal;

                     if ($rr[0]->maximumamount != 0) {
                        if($cartDiscountAmt > $rr[0]->maximumamount) {
                            $cartAmt = $cartAmt + $cartDiscountAmt - $rr[0]->maximumamount;
                            $cartDiscountAmt = $rr[0]->maximumamount;
                        }
                    }
                // $cartAmt = $cartAmt - ($CartRow->discountAmount) + (( ($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
                // $rental_amount = $rental_amount  + (( ($CartRow->product_tax_cost * 0.01 * $CartRow->price ) + $CartRow->price) * $CartRow->quantity);
                // $cartDisc = $CartRow->discount_on_si;
                // $shippingCost = $shippingCost + ($CartRow->product_shipping_cost * $CartRow->quantity);              
            }
            $cartSAmt = $cartShippingAmt;
            $cartTAmt = ($cartAmt * 0.01 * $MainTaxCost);
            $grantAmt = $cartAmt + $cartSAmt + $cartTAmt;
            $disAmt = $cartDiscountAmt;
               
                    if($cartVal->row()->discount_on_si != ''){
                        $cartAmt  = $cartAmt - $cartVal->row()->discount_on_si; 
                        $cartSAmt = $cartShippingAmt;
                        $cartTAmt = ($cartAmt * 0.01 * $MainTaxCost);
                        $grantAmt = $cartAmt + $cartSAmt + $cartTAmt;
                        $disAmt = $cartDiscountAmt; 
                        $discount_on_si =  $cartVal->row()->discount_on_si; 
                    }
            // print_r($cartSAmt);exit;
            // exit;
            // if( $cartVal->row()->discount_on_si > $cartAmt){
            //     $discount_on_si = $cartAmt;
            //     $dataArr1 = array('discount_on_si' => $discount_on_si);
            //     $condition1 = array('user_id' => $userid);
            //     $this->cart_model->commonInsertUpdate(SHOPPING_CART, 'update', $excludeArr, $dataArr1, $condition1);  
            // }else{
            //     $discount_on_si =  $cartVal->row()->discount_on_si;
            // }
            // print_r($cartAmt);exit;
            // $cartAmt  = $cartAmt - $discount_on_si;
            // $cartSAmt = $shippingCost;
            // $cartTAmt = $cartAmt * 0.01 * $cartVal->row()->tax;
            // $grantAmt = $cartAmt + $cartSAmt + $cartTAmt;
        }
        $newAmtsValues = @implode('|', $newCartInd);
        return number_format($cartSAmt, 2, '.', '') . '|' . number_format($cartSAmt, 2, '.', '') . '|' . number_format($cartTAmt, 2, '.', '') . '|' . number_format($grantAmt, 2, '.', '') . '|' . number_format($discount_on_si, 2, '.', '') . '|' . $k . '|' . $newAmtsValues;             
                        
    }
    
    
    //simple singup user
    public function insertUserQuick($fullname='',$email='',$pwd='',$mobile = '',$is_mobile_verified = 'No'){
    /* get Referal user id end */
        $dataArr = array(
            'full_name' =>  $fullname,
            'user_name' =>  $fullname,
            'group'     =>  'User',
            'email'     =>  $email,
            'password'  =>  md5($pwd),
            'status'    =>  'Active',
            'phone_no' => $mobile,
            'is_verified'=> 'No',
            'is_mobile_verified' => $is_mobile_verified,
            'created'   =>  mdate($this->data['datestring'],time())
        );
        $this->simple_insert(USERS,$dataArr);        
   }
    
    
    //accept offline orders 
    public function addOfflinePaymentCart($userid = '',$alter_user_id = '',$address_id = '') {
        $this->db->select('a.*');
        $this->db->from(SHOPPING_CART . ' as a');
        $this->db->where('a.user_id',$userid);
        $AddPayt = $this->db->get();


        $this->db->select('s.*');
        $this->db->from(SHIPPING_ADDRESS . ' as s');
        $this->db->where('s.id',$address_id);
        $Address = $this->db->get();

        if ($this->session->userdata('randomNo') != '') {
            $delete = 'delete from ' . PAYMENT . ' where dealCodeNumber = "' . $this->session->userdata('randomNo') . '" and user_id = "' . $alter_user_id . '" ';
            $this->ExecuteQuery($delete, 'delete');
            $dealCodeNumber = $this->session->userdata('randomNo');
        } else {
            $dealCodeNumber = mt_rand();
        }

        $insertIds = array();
        $ReturnData = $this->CheckOriginalPriceAfterCoupon($userid);
        // $cartDiscAmt
        // $grantAmt = $cartAmt + $cartShippingAmt + $cartTaxAmt  - $AddPayt->row()->discount_on_si;
        foreach ($AddPayt->result() as $result) {

            if($result->discount_on_si != ''){
                $payu_type = true;
                $is_recurring = 'si_on_credit_card';
            }else{
                $payu_type = false;
                $is_recurring = 'not_opted';
            }
           
            if ($this->input->post('is_gift') == '') {
                $ordergift = 0;
            } else {
                $ordergift = 1;
            }

            $sumTotal = number_format((($result->price + $result->product_shipping_cost + ($result->product_tax_cost * 0.01 * $result->price)) * $result->quantity), 2, '.', '');

            $uncleanNote = $this->input->post('note');
            $cleanNote = mysqli_escape_string($uncleanNote);

            $insert = ' insert into ' . PAYMENT . ' set
                                product_id = "' . $result->product_id . '",
                                sell_id = "' . $result->sell_id . '",                               
                                price = "' . $result->price . '",
                                quantity = "' . $result->quantity . '",
                                indtotal = "' . $result->indtotal . '",
                                shippingcountry = "' . $Address->row()->country . '",
                                shippingid = "' . $address_id . '",
                                shippingstate = "' . $Address->row()->state . '",
                                shippingcity = "' . $Address->row()->city . '",
                                shippingcost = "' . $ReturnData['cartSAmt'] . '",
                                tax = "' . $cartTaxAmt . '",
                                product_shipping_cost = "' . $result->product_shipping_cost . '",
                                product_tax_cost = "' . $result->product_tax_cost . '",                                                                                                 
                                coupon_id  = "' . $result->couponID . '",
                                discountAmount = "' . $ReturnData['cartDisc'] . '",
                                discount_on_si = "' . $AddPayt->row()->discount_on_si . '",
                                couponCode  = "' . $result->couponCode . '",
                                coupontype = "' . $result->coupontype . '",
                                sumtotal = "' . $sumTotal . '",
                                user_id = "' . $alter_user_id . '",
                                created = now(),
                                dealCodeNumber = "' . $dealCodeNumber . '",
                                status = "Pending",
                                payment_type = "",
                                attribute_values = "' . $result->attribute_values . '",
                                shipping_status = "Pending",
                                total  = "' . $ReturnData['grantAmt'] . '", 
                                note = "' . $cleanNote . '", 
                                order_gift = "' . $ordergift . '", 
                                inserttime = "' . time() . '",
                                is_recurring = "'.$payu_type.'",
                                recurring_type = "'.$recurring_type.'",
                                is_offline_placed = 1';


            $insertIds[] = $this->cart_model->ExecuteQuery($insert, 'insert');
        }
        $query = $this->db->query("SELECT full_name,email FROM " . USERS . " WHERE id='" . $result->user_id . "' LIMIT 1");
        $userdet = $query->result();
        $paymtdata = array(
            'randomNo' => $dealCodeNumber,
            'randomIds' => $insertIds,
            'sessprice' => $sumTotal,
            'cartusername' => $userdet[0]->full_name,
            'cartuseremail' => $userdet[0]->email,
            'cartuserid' => $result->user_id
        );
        $this->session->set_userdata($paymtdata);
        $paymtdata = array('randomNo' => '');
        $this->session->set_userdata($paymtdata);

        return $dealCodeNumber;
    }


    public function getCurrentSuccessOrdersOffline($userid, $dealCode) {
        $this->db->select('p.*,u.email,u.full_name,u.address,u.address2,u.s_tin_no,u.s_vat_no,u.s_cst_no,pd.product_name,pd.id as PrdID,pd.description as product_description,pd.subproducts,pd.image,pd.seller_product_id,pd.sku,pAr.attr_name as attr_type,sp.attr_name,sh.address1 as ship_address1,sh.address2 as ship_address2, sh.city as ship_city, sh.state as ship_state, sh.postal_code as ship_postal_code, sh.country as ship_country, sh.phone as ship_phone, sh.full_name as ship_full_name, pd.package_discount, pd.subproduct_quantity,p.discount_on_si,p.payment_type');
        $this->db->from(PAYMENT . ' as p');
        $this->db->join(USERS . ' as u', 'p.user_id = u.id');
        $this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
        $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
        $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
        $this->db->join(SHIPPING_ADDRESS . ' as sh', 'sh.id=p.shippingid', 'left');
        $this->db->where('p.user_id = "' . $userid . '" and p.dealCodeNumber="' . $dealCode . '"');
        $currentOrderDetails = $this->db->get();
        return $currentOrderDetails->result_array();
    }
    
        //used by cart controller
    public function getSellProductAndUserDetails($userid,$randomId){
         //Send Mail to Use
        $this->db->select('p.*,u.email,u.full_name,u.address,u.phone_no,u.postal_code,u.state,u.country,u.city,pd.product_name,pd.image,pd.id as PrdID,pAr.attr_name as attr_type,sp.attr_name');
        $this->db->from(PAYMENT . ' as p');
        $this->db->join(USERS . ' as u', 'p.user_id = u.id');
        $this->db->join(PRODUCT . ' as pd', 'pd.id = p.product_id');
        $this->db->join(SUBPRODUCT . ' as sp', 'sp.pid = p.attribute_values', 'left');
        $this->db->join(PRODUCT_ATTRIBUTE . ' as pAr', 'pAr.id = sp.attr_id', 'left');
        $this->db->where('p.user_id = "' . $userid . '" and p.dealCodeNumber="' . $randomId . '"');
        $PrdList = $this->db->get();

        $this->db->select('p.sell_id,p.couponCode,u.email');
        $this->db->from(PAYMENT . ' as p');
        $this->db->join(USERS . ' as u', 'p.sell_id = u.id');
        $this->db->where('p.user_id = "' . $userid . '" and p.dealCodeNumber="' . $randomId . '"');
        $this->db->group_by("p.sell_id");
        $SellList = $this->db->get();
        return array('PrdList' => $PrdList,'SellList'=> $SellList);
    }
    
    //get all shipping address
    public function get_my_shipping_address($user_id){
        $this->db->select('*');
        $this->db->from(SHIPPING_ADDRESS);
        $this->db->where('user_id',$user_id);
        $this->db->order_by('id','desc');
        return $this->db->get();
    }

    
    
    
    
    
}


?>
