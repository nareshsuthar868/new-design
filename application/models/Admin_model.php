<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This model contains all db functions related to admin management
 * @author Teamtweaks
 *
 */
class Admin_model extends My_Model
{
	public function __construct() 
	{
		parent::__construct();
	}
	
	public function add_edit_subadmin($dataArr='',$condition=''){
		if ($condition['id'] != ''){
			$this->db->where($condition);
			$this->db->update(ADMIN,$dataArr);
		}else {
			$this->db->insert(ADMIN,$dataArr);
		}
	}

	public function get_all_user_uploaded_docs($condition = ''){
		if($condition != ''){
			return $this->db->select('uud.*,user.full_name,user.email')->from(USER_UPLOADED_DOCS.' as uud')->join(USERS.' as user','user.id = user_id','left')->where($condition)->order_by('uud.uploaded_date','desc')->get();
		}else{
			return $this->db->select('uud.*,user.full_name,user.email')->from(USER_UPLOADED_DOCS.' as uud')->join(USERS.' as user','user.id = user_id','left')->group_by('uud.order_id')->get();
			
		}
	}
	
}