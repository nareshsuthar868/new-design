<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This model contains all db functions related to cms management
 * @author Teamtweaks
 *
 */
class Filter_model extends My_Model
{
	public function __construct() {
            parent::__construct();
	}


    public function get_filter_listing(){
        $this->db->select('*');
        $this->db->from(FILTERS);
        $data = $this->db->get();
        return $data;
        // exit;
    }

    public function view_filter_details($status,$count = NULL,$length = NULL,$start = NULL, $orderIndex = 2, $orderType = 'DESC', $searchValue = NULL){

        // $column_order = array('id','email',null,'created',null,null,null,'status'); //set column field database for datatable orderable
        $this->db->DISTINCT();
        if($count != NULL){ 
            $this->db->select('count(f.id) as count', FALSE);
        }else{
            $this->db->select('f.*,c.cat_name,c.id as cat_id,cs.cat_name as subcatname,cs.id as subcat_id');
            $this->db->limit($length, $start);
            $this->db->order_by($column_order[$orderIndex],$orderType );
        }    
        $this->db->from(FILTERS . ' as f');
        $this->db->join(CATEGORY . ' as c', 'c.id = f.category_id');
        $this->db->join(CATEGORY . ' as cs', 'cs.id = f.sub_category_id','left');
        $this->db->where('f.status = "active"');

        // if($searchValue != NULL && $searchValue != ''){
        //     $sortQry = "(u.email LIKE '%$searchValue%' or p.id LIKE '%$searchValue%' or p.dealCodeNumber LIKE '%$searchValue%' or p.couponCode LIKE '%$searchValue%' or p.total LIKE '%$searchValue%' or p.shippingcity LIKE '%$searchValue%')";
        //     $this->db->where( $sortQry );
        // }
        $query = $this->db->get();
        $result = $query->result();
        return $result;
    }

}