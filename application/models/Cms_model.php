<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This model contains all db functions related to cms management
 * @author Teamtweaks
 *
 */
class Cms_model extends My_Model
{
	public function __construct() {
            parent::__construct();
	}
	function get_all_details($table='',$condition='',$sortArr=''){            
            if ($sortArr != '' && is_array($sortArr)){
                foreach ($sortArr as $sortRow){
                    if (is_array($sortRow)){
                        $this->db->order_by($sortRow['field'],$sortRow['type']);
                    }
                }
            }
            return $this->db->get_where($table,$condition);
	}
	
   public function Abandoned_SignUp_Users($created){
        $this->db->select('user.*');
        $this->db->from(USERS.' as user');
        $this->db->join(PAYMENT.' as payment','payment.user_id = user.id', 'LEFT');
        $this->db->join(SHOPPING_CART.' as shopping','shopping.user_id = user.id', 'LEFT');
        $this->db->where('DATE(user.created)',$created);
        $this->db->where('payment.user_id',NULL);
        $this->db->where('shopping.user_id',NULL);
        $data = $this->db->get();
        return $data->result();
    }

    public function Abandoned_Cart($created){
        $this->db->select('user.id as user_id,user.*,shopping.*');
        $this->db->from(USERS.' as user');
        $this->db->join(PAYMENT.' as payment','payment.user_id = user.id', 'LEFT');
        $this->db->join(SHOPPING_CART.' as shopping','shopping.user_id = user.id', 'LEFT');
        $this->db->where('DATE(user.created)',$created);
        $this->db->where('payment.user_id',NULL);
        $this->db->where('shopping.user_id !=',NULL);
        $this->db->group_by("user.id");
        $data = $this->db->get();
        return $data->result();
    }
    
    
    // public function get_last_seven_days_report(){
    //     $this->db->select('p.created,product.id as product_id, product.product_name ,add.city, count(p.id) as total_sold');
    //     $this->db->from(PAYMENT.' as p');
    //     $this->db->join(SHIPPING_ADDRESS.' as add','p.shippingid = add.id');
    //     $this->db->join(PRODUCT.' as product','p.product_id = product.id');
    //     $this->db->where('p.created BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW()');
    //     $this->db->group_by('p.product_id');
    //     $this->db->group_by('p.shippingcity');
    //     $this->db->order_by('p.created','asc');
    //     $result = $this->db->get();
    //     return $result;
    // }
    
    
    public function get_last_seven_days_report(){
        $this->db->select('p.created,product.id as product_id, product.product_name ,add.city, count(p.id) as total_sold,p.quantity,product.subproducts');
        $this->db->from(PAYMENT.' as p');
        $this->db->join(SHIPPING_ADDRESS.' as add','p.shippingid = add.id');
        $this->db->join(PRODUCT.' as product','p.product_id = product.id');
        $this->db->where('p.created BETWEEN DATE_SUB(NOW(), INTERVAL 7 DAY) AND NOW()');
        $this->db->group_by('p.product_id');
        $this->db->group_by('p.shippingcity');
        $this->db->order_by('p.created','asc');
        $result = $this->db->get();
        return $result;
    }

    
    public function get_last_seven_days_report_on_products($product_ids){
        $this->db->select('product.id as product_id, product.product_name ,product.subproduct_quantity');
        $this->db->from(PRODUCT.' as product');
        $this->db->where_in('product.id',$product_ids);
        $result = $this->db->get();
        return $result;
    }
    
    public function get_user_details_on_payment($order_id,$sub_status){
        $this->db->select('user.full_name,user.phone_no,user.email');
        $this->db->from(USERS.' as user');
        $this->db->join(PAYMENT.' as payment','payment.user_id = user.id');
        $this->db->where('payment.dealCodeNumber',$order_id);
        $this->db->where('payment.zoho_sub_status !=',$sub_status);
        $this->db->group_by('payment.dealCodeNumber');
        $data = $this->db->get();
        return $data;
    }
    
    
    public function get_offerDetails(){
	    $this->db->select('*');
	    $this->db->from(SITE_OFFERS);
	    $this->db->where('status','Publish');
	    $this->db->order_by('position','asc');
	    return $this->db->get();
	}


}