<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This model contains all db functions related to cms management
 * @author Teamtweaks
 *
 */
class Offer_model extends My_Model
{
	public function __construct() {
            parent::__construct();
	}
	
	public function get_offerDetails(){
	    $this->db->select('*');
	    $this->db->from(SITE_OFFERS);
	    $this->db->order_by('position','asc');
	    return $this->db->get();
	}
}