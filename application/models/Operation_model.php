<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Operation_model extends My_Model{
	public function add_tciket_topic($dataArr=''){
		$this->db->insert(TICKETS_TOPIC,$dataArr);
	}
	function getalltopics(){
		$query = $this->db->query("SELECT * FROM ".TICKETS_TOPIC." ORDER BY adddateTime DESC");
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return array();
		}
	}	
	function getalltickets(){
		$where = '';
		if($this->session->userdata('cat_roles')!=''){
			$pos = strpos($this->session->userdata('cat_roles'), ",");
			if ($pos !== false){
				$ex = explode(",",$this->session->userdata('cat_roles'));
				$txt = '';
				foreach($ex as $r){
					$txt .= "'".$r."',";
				}
				$where = " WHERE tt.ID IN (".substr($txt,0,-1).") ";
			}else{
				$where = " WHERE tt.ID IN (".substr($txt,0,-1).") ";
			}
			$where = " WHERE tt.ID IN (".$this->session->userdata('cat_roles').")";
		}
		$query = $this->db->query("SELECT t.*,tt.t_title FROM ".TICKETS." AS t LEFT JOIN ".TICKETS_TOPIC." AS tt ON tt.ID=t.TPID ".$where." ORDER BY adddateTime DESC");
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return array();
		}
	}
	function getalladminusers(){
		$query = $this->db->query("SELECT * FROM ".ADMIN." WHERE admin_type='operation'");
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return array();
		}
	}
	function getsingleticket($id){
		$query = $this->db->query("SELECT t.*,tt.t_title,tt.t_emailtitle,tt.t_emailid,tt.t_desc FROM ".TICKETS." AS t LEFT JOIN ".TICKETS_TOPIC." AS tt ON tt.ID=t.TPID WHERE t.ID='".$id."' LIMIT 1");
		if($query->num_rows() > 0){
			return $query->result();
		}else{
			return array();
		}
	}	
}