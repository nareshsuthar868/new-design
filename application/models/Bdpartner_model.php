<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This model contains all db functions related to user management
 * @author Teamtweaks
 *
 */
class Bdpartner_model extends My_Model
{
	public function __construct() 
	{
		parent::__construct();
	}


	public function getUsers($count = NULL,$length = NULL,$start = NULL, $orderIndex = 8, $orderType = 'DESC', $searchValue = NULL){
		// print_r("expression");exit;
		$column_order = array(null, 'user.full_name','user.user_name','user.id','user.email','user.created','user.status','user.last_login_date','user.modified','partner.bdexecuter_id'); //set column field database for datatable orderable
     	$this->db->DISTINCT();
	    if($count != NULL){ 
	      $this->db->select( 'count(user.id) as count' , FALSE);
	    }else{
			$this->db->select('user.*,partner.*');
	    	$this->db->limit($length, $start);
	    }  
        $this->db->from(USERS.' as user');
        $this->db->join(PARTNER_USERS.' as partner','user.id = partner.user_id');
       	$this->db->where('user.user_type !=','BD Executive');
	    // print_r("expression");exit;  

       	// $this->db->order_by($column_order[$orderIndex],$orderType );	
       	$this->db->order_by('user.created','desc');	
        if($searchValue != NULL && $searchValue != ''){
		    $sortQry = "(user.email LIKE '%$searchValue%' or user.full_name LIKE '%$searchValue%' or user.user_name LIKE '%$searchValue%'  or  user.id LIKE '%$searchValue%')";
		    $this->db->where( $sortQry );
	    }
		$query = $this->db->get();
		$result=$query->result();
		foreach ($result as $data) {
			$data->executer_name = $this->get_userexecuter($data->bdexecuter_id)[0]->full_name;
			$data->executer_email = $this->get_userexecuter($data->bdexecuter_id)[0]->email;
		}
		return $result;
	}

	public function get_user_details($user_id){
		// print_r($user_id);exit;
		$this->db->select('user.id as user_id,user.phone_no,user.full_name,user.created,user.modified,user.email,user.user_type,partner.*');
		$this->db->from(USERS.' as user');
		$this->db->join(PARTNER_USERS.' as partner','user.id = partner.user_id');
		$this->db->where('partner.id',$user_id);
		$data = $this->db->get();
		// print_r($data->result());exit;
		return $data;
	}

	public function get_userexecuter($id)
	{
		$this->db->select('user.full_name,user.email');
		$this->db->from(USERS.' as user');
		$this->db->where('user.id',$id);
		$data = $this->db->get();
		return $data->result();
	}

	public function check_data_exists($condition){
		$this->db->select('user.*,partner.bdexecuter_id');
		$this->db->from(USERS.' as user');
		$this->db->join(PARTNER_USERS.' as partner','user.id = partner.user_id','left');
		$this->db->where($condition);
		$data = $this->db->get();
		return $data;
	}

	public function add_edit_subadmin($dataArr='',$condition=''){
		if ($condition['id'] != ''){
			$this->db->where($condition);
			$this->db->update(BD_SUBADMIN,$dataArr);
		}else {
			$this->db->insert(BD_SUBADMIN,$dataArr);
		}
	}

	public function getBDexecuters($count = NULL,$length = NULL,$start = NULL, $orderIndex = 2, $orderType = 'DESC', $searchValue = NULL){
		$column_order = array(null, 'user.full_name',NULL,NULL,NULL,'user.created'); //set column field database for datatable orderable
     	$this->db->DISTINCT();
	    if($count != NULL){ 
	      $this->db->select( 'count(user.id) as count' , FALSE);
	    }else{
			$this->db->select('user.*,partner.*');
	    	$this->db->limit($length, $start);
	    }
        $this->db->from(USERS.' as user');
        $this->db->join(PARTNER_USERS.' as partner','user.id = partner.user_id');
       	$this->db->where('user.user_type','BD Executive');
	    // print_r("expression");exit;  
       	$this->db->order_by($column_order[$orderIndex],$orderType );
        if($searchValue != NULL && $searchValue != ''){
		    $sortQry = "(user.email LIKE '%$searchValue%' or user.full_name LIKE '%$searchValue%' or user.user_name LIKE '%$searchValue%'  or  user.id LIKE '%$searchValue%')";
		    $this->db->where( $sortQry );
	    }
		$query = $this->db->get();
		$result=$query->result();
		// print_r("expression");exit;
		return $result;
	}


	public function get_user_bd_executive() {
		$this->db->select('user.id, user.user_name,user.email');
		$this->db->from(USERS.' as user');
		$this->db->join(PARTNER_USERS.' as partner','user.id = partner.user_id');
		$this->db->where('user.status', 'Active');
		$this->db->where('user.user_type', 'BD Executive');
		$data = $this->db->get();
		return $data;
	}

	public function get_leadlisting($count = NULL,$length = NULL,$start = NULL, $orderIndex = 2, $orderType = 'DESC', $searchValue = NULL){
		// print_r("expression");exit;
		$column_order = array(null, 'user.full_name','user.email','user.full_name','user.email','user.phone_no','lead.lead_status','lead.created',null); //set column field database for datatable orderable
     	$this->db->DISTINCT();
	    if($count != NULL){ 
	      $this->db->select( 'count(lead.id) as count' , FALSE);
	    }else{
			$this->db->select('partner.id as partner_id,partner.profile_pic,user.full_name as owner_name,user.email as owner_email,user.user_type,lead.*,cm.status as cm_status');
	    	$this->db->limit($length, $start);
	    }  
        $this->db->from(BD_LEADS.' as lead');
        $this->db->join(PARTNER_USERS.' as partner','partner.user_id = lead.bd_user_id');
		$this->db->join(USERS.' as user','partner.user_id = user.id');
		$this->db->join(BD_LEAD_COMMISSION.' as cm','cm.lead_id = lead.id','LEFT');	
       	// $this->db->where('user.user_type','BD Executive');


       	$this->db->order_by($column_order[$orderIndex],$orderType );
        if($searchValue != NULL && $searchValue != ''){
		    $sortQry = "(user.email LIKE '%$searchValue%' or user.full_name LIKE '%$searchValue%' or user.user_name LIKE '%$searchValue%'  or  user.id LIKE '%$searchValue%')";
		    $this->db->where( $sortQry );
	    }
		$query = $this->db->get();
		$result=$query->result();
		return $result;
	}

	public function getCommissions($count = NULL,$length = NULL,$start = NULL, $orderIndex = 2, $orderType = 'DESC', $searchValue = NULL){
		$column_order = array(null, 'user.full_name','user.email','bdcommission.commission_rate','user.email','user.phone_no','lead.lead_status','lead.created',null); //set column field database for datatable orderable

		$this->db->DISTINCT();
	    if($count != NULL){ 
	      $this->db->select( 'count(user.id) as count' , FALSE);
	    }else{
			$this->db->select('count(lead.id) as total_order ,user.id as user_id,user.user_type,user.email,user.full_name,FORMAT(sum(bdcommission.commission_rate),2)  as total_commission,bdcommission.status');
	    	$this->db->limit($length, $start);
	    }  
        $this->db->from(BD_LEADS.' as lead');
		$this->db->join(USERS.' as user','lead.bd_user_id = user.id');	
        $this->db->join(BD_LEAD_COMMISSION.' as bdcommission','lead.id = bdcommission.lead_id');
        $this->db->group_by('user.id');
       	// $this->db->where('lead.order_id !=','');
   		$this->db->order_by($column_order[$orderIndex],$orderType );
       	// $this->db->order_by('bdcommission.commission_rate','desc');
        if($searchValue != NULL && $searchValue != ''){
		    $sortQry = "(user.email LIKE '%$searchValue%' or user.full_name LIKE '%$searchValue%' or user.user_name LIKE '%$searchValue%'  or  user.id LIKE '%$searchValue%')";
		    $this->db->where( $sortQry );
	    }
		$query = $this->db->get();
		$result=$query->result();
		return $result;
	}

	//get my leads 
	public function get_bduser_leads($user_id){
		$this->db->select('lead.lead_status,IFNULL(lead.order_id, "--") as order_id  ,lead.email ,lead.phone_no,CONCAT(lead.first_name,lead.last_name) as full_name, comission.commission_percentage as cm_percentage, 
			IFNULL(comission.commission_rate, "--") as cm_rate, IFNULL(comission.created, "--") as order_date, IFNULL(comission.status, "--") as comission_status');
		$this->db->from(BD_LEADS.' as lead');
		$this->db->join(BD_LEAD_COMMISSION.' as comission','lead.id = comission.lead_id','left');
		$this->db->where('lead.bd_user_id',$user_id);
		$this->db->order_by('lead.lead_status','desc');
		$data = $this->db->get();

		return $data;
	}

	//check order exists
	public function check_order_id_valid($order_id){
		$this->db->select('*');
		$this->db->from(PAYMENT);
		$this->db->where('dealCodeNumber',$order_id);
		$data = $this->db->get();
		return $data;
	}

	//get pending lead details 
	public function get_pending_lead_details($lead_id){
		$this->db->select('status');
		$this->db->from(BD_LEAD_COMMISSION);
		$this->db->where('lead_id',$lead_id);
		$data = $this->db->get();
		return $data;
	}


	//get listing of cms pages
	public function get_cmslisting($count = NULL,$length = NULL,$start = NULL, $orderIndex = 2, $orderType = 'DESC', $searchValue = NULL){
		// $column_order = array(null, 'user.full_name','user.email','bdcommission.commission_rate','user.email','user.phone_no','lead.lead_status','lead.created',null); //set column field database for datatable orderable

		$this->db->DISTINCT();
	    if($count != NULL){ 
	      $this->db->select( 'count(id) as count' , FALSE);
	    }else{
			$this->db->select('*');
	    	$this->db->limit($length, $start);
	    }  
        $this->db->from(BD_CMS);
       	// $this->db->where('lead.order_id !=','');
   		// $this->db->order_by($column_order[$orderIndex],$orderType );
       	// $this->db->order_by('bdcommission.commission_rate','desc');
     //    if($searchValue != NULL && $searchValue != ''){
		   //  $sortQry = "(user.email LIKE '%$searchValue%' or user.full_name LIKE '%$searchValue%' or user.user_name LIKE '%$searchValue%'  or  user.id LIKE '%$searchValue%')";
		   //  $this->db->where( $sortQry );
	    // }
		$query = $this->db->get();
		$result=$query->result();
		return $result;
	}


	//dashboard details 
	public function getCountDetails($tableName='',$fieldName='',$whereCondition=array())
	{
		$this->db->select($fieldName);
		$this->db->from($tableName);
		$this->db->where($whereCondition);
		
		//$this->db->where(JOB.".dateAdded >= DATE_SUB(NOW(),INTERVAL 30 DAY)", NULL, FALSE);
		$countQuery = $this->db->get();
		return $countQuery->num_rows();
	}

	
	

	public function getDashboardUserCount($whereCondition){
		$this->db->select('user.id');
		$this->db->from(USERS.' as user');
		$this->db->join(PARTNER_USERS.' as partner','partner.user_id = user.id');
		if(!empty($whereCondition)){
			$this->db->where($whereCondition);
		}
		$data = $this->db->get();
		return $data->num_rows();
	}

	public function getTotalCommissions($status){
		$this->db->select('SUM(commission_rate) as total_commission');
		$this->db->from(BD_LEAD_COMMISSION);
		$this->db->where('status',$status);
		$data = $this->db->get();
		return $data->row()->total_commission;
	}

	public function getRecentDetails($userOrderBy='',$userLimit='',$whereCondition=array())
	{
		$this->db->select('user.user_type,user.email,user.full_name,partner.*');
		$this->db->from(USERS.' as user');
		$this->db->join(PARTNER_USERS.' as partner','partner.user_id = user.id');
		if(!empty($whereCondition)){
			$this->db->where($whereCondition);
		}		
		$this->db->order_by('partner.id', $userOrderBy);
		$this->db->limit($userLimit);
		$countQuery = $this->db->get();
		return $countQuery->result_array();
	}

	public function getTodayUsersCount($whereCondition=array())
	{	
		$this->db->select('user.id');
		$this->db->from(USERS.' as user');
		$this->db->join(PARTNER_USERS.' as partner','partner.user_id = user.id');
		if(!empty($whereCondition)){
			$this->db->where($whereCondition);
		}
		$this->db->where("created >= DATE_SUB(NOW(),INTERVAL 24 HOUR)", NULL, FALSE);	
		//$this->db->like("created",date('Y-m-d', strtotime('-24 hours')));
		$countQuery = $this->db->get();
		return $countQuery->num_rows();
	}
	
	public function getThisMonthCount($whereCondition=array())
	{
		$this->db->select('user.id');
		$this->db->from(USERS.' as user');
		$this->db->join(PARTNER_USERS.' as partner','partner.user_id = user.id');
		if(!empty($whereCondition)){
			$this->db->where($whereCondition);
		}		
		$this->db->where("created >= DATE_SUB(NOW(),INTERVAL 30 DAY)", NULL, FALSE);
		$countQuery = $this->db->get();
		return $countQuery->num_rows();
	}
	
	public function getLastYearCount($whereCondition=array())
	{
		$this->db->select('user.id');
		$this->db->from(USERS.' as user');
		$this->db->join(PARTNER_USERS.' as partner','partner.user_id = user.id');
		if(!empty($whereCondition)){
			$this->db->where($whereCondition);
		}
		$this->db->like('created', date("Y"));
		$countQuery = $this->db->get();
		return $countQuery->num_rows();
		
	}

	public function getDashboardLeadsDetails()
	{
		$this->db->select('user.full_name,user.email as owner_email,lead.*');
		$this->db->from(BD_LEADS.' as lead');
		$this->db->join(USERS.'  as user','lead.bd_user_id = user.id');
		$this->db->order_by('lead.id','desc');
		$this->db->limit(3);
		// $this->db->group_by('dealCodeNumber');
		$orderQueryDashboard = $this->db->get();
		return $orderQueryDashboard->result_array();
		//$this->db->where($whereCondition);
	}
	public function getemail($id)
	{
		$this->db->select('user.email');
		$this->db->from(USERS.' as user');
		return $this->db->get()->row();
	}

	public function check_details_exists($data){
		$this->db->select('*');
		$this->db->from(USERS);
		$this->db->or_where('email',$data['email']);
		$this->db->or_where('phone_no',$data['phone_no']);
		$this->db->where('user_type != ','');
		$data = $this->db->get();
		return $data;
	}

	public function get_offerlisting($count = NULL,$length = NULL,$start = NULL, $orderIndex = 2, $orderType = 'DESC', $searchValue = NULL){
		// $column_order = array(null, 'user.full_name','user.email','bdcommission.commission_rate','user.email','user.phone_no','lead.lead_status','lead.created',null); //set column field database for datatable orderable

		$this->db->DISTINCT();
	    if($count != NULL){ 
	      $this->db->select( 'count(id) as count' , FALSE);
	    }else{
			$this->db->select('*');
	    	$this->db->limit($length, $start);
	    }  
        $this->db->from(BD_OFFERS);

		$query = $this->db->get();
		$result=$query->result();
		return $result;
	}


	   //get bd executive revenure
    public function get_my_revenue($user_id,$filter_date = NULL){
    	// print_r($user_id);exit;
    	$this->db->select('SUM(payment.price) as rental_amount');
    	$this->db->from(BD_LEADS.' as lead');
    	$this->db->join(PAYMENT.' as payment','payment.dealCodeNumber = lead.order_id');
    	$this->db->where('lead.bd_user_id',$user_id);

    	// $this->db->where('lead.lead_status','delivered');
    	if($filter_date != NULL){
    		$this->db->where("month(payment.created)",$filter_date['month']);
    		$this->db->where("year(payment.created)",$filter_date['year']);
    		
    	}
    	$data = $this->db->get();
    	return $data;
    }

    public function get_bdexecuter_users($bduser_id){
    	$this->db->select('GROUP_CONCAT(user_id SEPARATOR  ",") as user_id');
    	$this->db->from(PARTNER_USERS);
    	$this->db->where('bdexecuter_id',$bduser_id);
    	$data = $this->db->get();
    	return $data;
    }

    public function get_broker_revenue($user_ids,$filter_date = NULL){
    	$userIds = explode(',', $user_ids->user_id);
    	$this->db->select('SUM(payment.price) as rental_amount');
    	$this->db->from(BD_LEADS.' as lead');
    	$this->db->join(PAYMENT.' as payment','payment.dealCodeNumber = lead.order_id');
    	$this->db->where_in('lead.bd_user_id',$userIds);
    	// $this->db->where('lead.lead_status','delivered');    	
    	if($filter_date != NULL){
    		$this->db->where("month(payment.created)",$filter_date['month']);
    		$this->db->where("year(payment.created)",$filter_date['year']);
    		
    	}
    	$data = $this->db->get();
    	return $data;
    }

    public function get_my_order_count($bd_user_id,$filter_date = NULL){
    	$this->db->select('payment.id as total_order');
    	$this->db->from(BD_LEADS.' as lead');
    	$this->db->join(PAYMENT.' as payment','payment.dealCodeNumber = lead.order_id');
    	$this->db->where('lead.bd_user_id',$bd_user_id);
    	$this->db->group_by('payment.dealCodeNumber');
    	// $this->db->where('lead.lead_status','delivered');
    	if($filter_date != NULL){
    		$this->db->where("month(payment.created)",$filter_date['month']);
    		$this->db->where("year(payment.created)",$filter_date['year']);
    		
    	}
    	$data = $this->db->get();
    	return $data;

    }


    public function get_broker_order_count($user_ids,$filter_date = NULL){
    	$userIds = explode(',', $user_ids->user_id);
    	$this->db->select('payment.id as total_order');
    	$this->db->from(BD_LEADS.' as lead');
    	$this->db->join(PAYMENT.' as payment','payment.dealCodeNumber = lead.order_id');
    	$this->db->where_in('lead.bd_user_id',$userIds);
    	$this->db->group_by('payment.dealCodeNumber');
    	// $this->db->where('lead.lead_status','delivered');    	
    	if($filter_date != NULL){
    		$this->db->where("month(payment.created)",$filter_date['month']);
    		$this->db->where("year(payment.created)",$filter_date['year']);
    		
    	}
    	$data = $this->db->get();
    	return $data;
    }
    
    public function get_export_lead_details($post){
    	$this->db->select('lead.*,user.full_name as lead_owner,user.email as lead_owner_email');
    	$this->db->from(BD_LEADS.' as lead');
    	$this->db->join(USERS.' as user','lead.bd_user_id = user.id');
    	if($post['date_from'] != '' && $post['date_to'] != ''){
    		$this->db->where('lead.created >=', $post['date_from']);
			$this->db->where('lead.created <=', $post['date_to']);
    	}
    	$data = $this->db->get();
    	return $data;

	}



}

