<!DOCTYPE html>
<html>
<head>
	<title>.::Cityfurnish::.</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
	<script type="text/javascript" src="jquery-2.0.3.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Noto+Sans+JP&display=swap" rel="stylesheet">


</head>
<body>

<div class="container-fluid">
	<div class="row" id="header" style="">
		<div class="col-sm-10">
		<img src="https://d1eohs8f9n2nha.cloudfront.net/images/logo-stick.png" style="height: 40px;margin: 15px;">

		</div>
		
	</div>

	
	<div class="ques-container" >
		<div class="row" style="height: auto;width: 100%;;padding: 20px;margin: 0px;color: #363538;">
			 <div class="col-sm-12" style="text-align: center;margin: 10px;font-size: 25px;"> 
			 	<br><br>
			 	<strong>Your Exam has been Successfully Submitted!
			 	<br> We shall get back to you :)</strong> 
			 	<br><br> 
			  </div>
		</div>
		<br>

	</div>
	

</div>
	
<script>
         var timer = setTimeout(function() {
        window.open('login.php','_self');
        }, 3000);
        
        
    </script>
</body>
</html>

