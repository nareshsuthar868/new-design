<!DOCTYPE html>
<html>
<head>
	<title>.::Candidate Login::.</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
	<script type="text/javascript" src="jquery-2.0.3.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</head>
<body style="background: #fff;height: 100%;">
	<div class="container-form">
		
			<center>
			<div class="formDiv">
				
				<form style="width: 100%;" action="index.php" method="POST">
					<br>
						<span class="formTitle">
							Candidate Login
						</span>
						<br><br>
						<div class="form-input">
							<input class="login-input" type="text" name="name" placeholder="Name" required="required">
						</div><br>

						<div class="form-input">
							<input class="login-input" type="number" name="contact" id="contact" maxlength="10" placeholder="Contact" required="required" oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);" onkeyup="checkMobile()">
								<span id="mobileerror" style="font-size:13px;color:red;float:right;"></span>&nbsp;
						</div>

						<div class="form-input">
							<input class="login-input" type="text" name="email" placeholder="Email" id="email" required="required" onkeyup="checkEmail()">
							<span id="emailerror" style="font-size:13px;color:red;float:right;"></span>&nbsp;
						</div>

						<div class="form-input">
							<input class="login-input" type="text" name="profile" placeholder="Profile" required="required">
						</div><br>
						<div class="form-input">
							<input class="login-input" type="text" name="code" id="code" placeholder="Candidate Code" required="required" onkeyup=checkCode()>
							<span id="codeerror" style="font-size:13px;color:red;float:right;"></span>&nbsp;
						</div>
						
					<button class="btn btn-primary" name="start" value="startTest" style="width: 100%;border-radius: 0px;padding: 10px;margin-top: 20px;">
							START TEST
						</button>
						<br><br>
												
				</form>
					
			</div>
			</center>
		
	</div>
	<script>
function checkEmail()
{

email = document.getElementById('email').value;
console.log(email);
if(email.indexOf('@') != -1)
{
  if(email.indexOf('.') != -1)
  {
    emailCount = 1;
    document.getElementById('emailerror').innerHTML = '';
  // alert("Invalid Email Account");
  }
  else
  {
    document.getElementById('emailerror').innerHTML = 'Invalid Email Account!';
  // alert("Invalid Email Account");
    return false;
  }
}
else
{
   document.getElementById('emailerror').innerHTML = 'Invalid Email Account!';
    return false;
}

}


function checkMobile()
{
var mobile = document.getElementById('contact').value;

if(!isNaN(mobile))
{
  if(mobile.length == 10 && mobile > 6000000000 && mobile < 9999999999)
  {
    mobCount = 1;
    document.getElementById('mobileerror').innerHTML = '';
  }
  else
  {
    document.getElementById('mobileerror').innerHTML = 'Please Enter Valid 10 digit Mobile number';
    //alert("Please enter only 10 digit mobile number");
      return false;
  }
}

else
{
    document.getElementById('mobileerror').innerHTML = 'Please Enter Valid  10 digit Mobile number';
      return false;
}



}


function checkCode()
{
var code = document.getElementById('code').value;

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("codeerror").innerHTML =
      this.responseText;
    }
  };
  xhttp.open("GET", "checkCode.php?code="+code, true);
  xhttp.send();
}

	    
	</script>
	
	<script>
	xmlhttp.open("POST","LogoutAction",false);
	window.onbeforeunload = function() { return alert("Your work will be lost."); };    
	</script>
</body>
</html>













