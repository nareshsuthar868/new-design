<!DOCTYPE html>
<html>
<head>
	<title>.::CityFurnish::.</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
	<!--<script type="text/javascript" src="jquery-2.0.3.js"></script>-->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
</head>
<body>
<?php 
include('connection.php');
error_reporting(E_ALL);
ini_set('display_errors', 1);
if(!isset($_POST['start']))
{    
	 ?>
	 
<script type="text/javascript">
  window.location="login.php"; 
</script>

	 <?php
}
    $name = $_POST['name'];
	$email = $_POST['email'];
	$profile = $_POST['profile'];
	$contact = $_POST['contact'];
	$code = $_POST['code'];
	$datetime = date("Y-m-d H:i:s");

	$checkCode = mysqli_query($con,"SELECT * FROM `candidatecodes` WHERE `name` = '$code' AND `status` = '1'");
	
	if(mysqli_fetch_array($checkCode) == '')
	{
	    ?>
        <script type="text/javascript">
          window.location="login.php"; 
        </script>
	    <?php
		
	}

	$insertData = mysqli_query($con,"INSERT INTO `answersheet`(`id`, `name`, `profile`, `contact`, `questionids`, `answerids`, `marks`, `createdate`, `status`, `delete`,`code`) VALUES('','$name','$profile','$contact','','','','$datetime','0','0','$code')");
	$lastid = mysqli_insert_id($con);

	$getQuestions = mysqli_query($con,"SELECT * FROM `questions` ORDER BY `sno` ASC");
?>

<div class="container-fluid">
	<div class="row" id="header">
		<div class="col-sm-10">
		<img src="https://d1eohs8f9n2nha.cloudfront.net/images/logo-stick.png" style="height: 35px;margin: 15px;">

		</div>
		<div class="col-sm-2" id="profile" style="font-weight:;">
		  Name : <?php echo $name;?> <br>
		  Contact : <?php echo $contact;?> <br>
		  Email : <?php echo $email;?> <br>
		  Code : <?php echo $code;?> <br>
		  Applied For: <?php echo $profile;?> <br><br>
		  <div class="countdown" style="margin-top: 10px;"></div>

		</div>
	</div>

<script type="text/javascript">
	var timer2 = "40:01";
	var interval = setInterval(function() {
	var timer = timer2.split(':');
	  //by parsing integer, I avoid all extra string processing
	var minutes = parseInt(timer[0], 10);
	var seconds = parseInt(timer[1], 10);
	  --seconds;
	 minutes = (seconds < 0) ? --minutes : minutes;
	 if (minutes < 0) clearInterval(interval);
	 seconds = (seconds < 0) ? 59 : seconds;
	 seconds = (seconds < 10) ? '0' + seconds : seconds;
	  //minutes = (minutes < 10) ?  minutes : minutes;
	  $('.countdown').html(minutes + ':' + seconds + '  Minutes Left');
	  timer2 = minutes + ':' + seconds;
	}, 1000);

</script>
	
	<div class="ques-container">
		<div class="row" style="height: 50px;width: 100%;border-bottom: 1px solid #ddd;padding: 0px;margin: 0px;color: #363538;font-weight: bold;">
			 <div class="col-sm-12" style="text-align: center;margin: 10px;"> QUESTIONS </div>
		</div>
		<br>

		<form action="save-test.php?id=<?php echo $lastid;?>&code=<?php echo $code;?>" method="POST" id="mainForm">
			<br>
			<?php
			$i = 1; 
			while($getRow = mysqli_fetch_array($getQuestions))
			{

			?>
			<div class="qbox">
				<div class="text"><?php echo trim($getRow['question']);?></div>
			
		        	<label class="optionBox"> &nbsp;&nbsp;<?php echo $getRow['option_a'];?>
					  <input type="radio"  name="radio<?php echo $i;?>"  value="a">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;<?php echo $getRow['option_b'];?>
					  <input type="radio"  name="radio<?php echo $i;?>" value="b">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;<?php echo $getRow['option_c'];?>
					  <input type="radio"  name="radio<?php echo $i;?>" value="c">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;<?php echo $getRow['option_d'];?>
					  <input type="radio"  name="radio<?php echo $i;?>" value="d">
					  <span class="checkmark"></span>
					</label>
					<input type="text" name="question<?php echo $i;?>" value="<?php echo $getRow['id'];?>" style="display: none;" >
					<hr>				
			</div>
			<?php $i++; } ?>
			
			<input type="text" name="" id="upto" value="<?php echo $i;?>" style="display: none;">
			<center><button class="btn btn-success" type="submit">SUBMIT TEST</button></center>
			<br><br>

		</form>
 	
	</div>
	
<script type="text/javascript">
 $(function(){  
   setTimeout(function(){
      $('form').submit();
    },2400000);
});
</script>

</div>
	
</body>
</html>

