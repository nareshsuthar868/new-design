var currentAdmVal = '';
function multiSelect(fromId, toId)
{
var x = '';
   var checkboxArray = document.getElementById(fromId);
        for (var i = 0; i < checkboxArray.length; i++) {
          if (checkboxArray.options[i].selected) {
          	if(fromId == 'route3' && toId == 'route4')
          	{
          	 var temp = checkboxArray[i].value;
          	 temp = temp.split(":::");
          	 x += temp[1]+",";
          	}
          	else
          	{
	          	x += checkboxArray[i].value+",";          		
          	}

          }
        }
        document.getElementById(toId).value = x;
}

var valString = '';

function collectStopValues(i,total,toid)
{
valString = '';
if(document.getElementById('li'+i).className == 'list-group-item')
{
	document.getElementById('li'+i).className = 'list-group-item li-selected';
	if(document.getElementById('not-selectedTab').className == 'active')
	{
		$('#li'+i).slideUp();
	}

}
else
{
	document.getElementById('li'+i).className = 'list-group-item';
	if(document.getElementById('selectedTab').className == 'active')
	{
		$('#li'+i).slideUp();
	}
}



for(j=0;j<total;j++)
{
	if(document.getElementById('li'+j))
	{
		if(document.getElementById('li'+j).className == 'list-group-item li-selected')
		{
			var x = document.getElementById('li'+j).lang;
			x = x.split(":::");
			valString += x[1]+",";
		}
	}
}
document.getElementById(toid).value = valString;

displayRoute();
}

function displayRoute()
{
	document.getElementById('targetFrame').contentWindow.generateRoute();
}


function toogleSelect(elm)
{
	if(elm.lang == '1')
	{
		elm.lang = '0';
		x = elm.className;
		x = x.replace("selectedItem","");
		elm.className = x;

	}
	else
	{
		elm.lang = '1';
		x = elm.className;
		if(x.indexOf('selectedItem') == -1)
		{
		x = x + " selectedItem";			
		}

		elm.className = x;		
	}
}


function selectAll(selector)
{	
	if(selector.lang == '1')
	{
			todo = '1';
			selector.lang = '0';
	}
	else
	{
		todo = '0';
		selector.lang = '1';
	}

	var total = document.getElementById('totalBusCount').value;
	for(j=0;j<total;j++)
	{
		if(document.getElementById('buslistRow'+j))
		{
			elm = document.getElementById('buslistRow'+j);		
if(todo == '1')
{
		elm.lang = '0';
		x = elm.className;
		x = x.replace(" selectedItem","");
		elm.className = x;
}
else
{

		elm.lang = '1';
		x = elm.className;
		if(x.indexOf('selectedItem') == -1)
		{
		x = x + " selectedItem";			
		}

		elm.className = x;
	
}
		}
	}
}


function getAddress(elm)
{
elm.innerHTML = "<i class='fa fa-spin fa-spinner'></i>"
	var geoCodeUrl = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+elm.lang+"&sensor=true";

	 $.ajax({
    type: 'POST',
    dataType:'json',
    url: geoCodeUrl,
    data: '', 
    success: function(responseData) {
    	j=0;
			 for (var key in responseData) {
			 	if(j ==0)
			 	{
			 		var html = "<a href='https://www.google.co.in/maps?q="+elm.lang+"' target='_blank'>"+responseData[key][0].formatted_address+"</a>"
			        elm.innerHTML = html; 
			        elm.className ='';		
			 	}
			j++;
			 	}
    
    },
    error: function() {
    	toast("Unable to get location",'toast bg-danger', '2000');
    }
});
}

function deleteData(id,table)
{
	var reply = confirm("Are you sure you want to delete this entry?")
	if(reply)
	{
		var url = "deleteData.php?table="+table+"&id="+id;
		genAjax(url,'',function(){
			toast("Successfully Deleted.","toast bg-success","3000");
			document.getElementById('tableRow'+id).className = 'bg-danger';
			setTimeout(function(){
			$('#tableRow'+id).hide();
			},2000);

		})
	}
}

function deleteDataSample(id,table)
{
	toast("Data deletion not activated in demo mode","toast bg-danger",3000);
}


function rotateMenu(id)
{
for(j=0;j<6;j++)
{
	if(document.getElementById('menutr'+j))
	{
		if(j != id)
		{
			document.getElementById('menutr'+j).className = "";	
		}
		if(document.getElementById('submenutr'+j))
		{

			$('#submenutr'+j).slideUp();	
		}
	}
}
document.getElementById('menutr'+id).className = "active";
$('#submenutr'+id).slideDown();


}



function filterRouteSelect(type)
{
	if(type == '')
	{
		if(document.getElementById('allTab').className == 'active')
		{
			type = 'all';
		}
		else if(document.getElementById('selectedTab').className == 'active')
		{
			type = 'selected';
		}
		else
		{
			type = 'not-selected';
		}
	}
	else
	{
		document.getElementById('allTab').className= '';
		document.getElementById('selectedTab').className= '';
		document.getElementById('not-selectedTab').className= '';
		document.getElementById(type+'Tab').className= 'active';
	}


var searchTerm = document.getElementById('stopSearcher').value;
searchTerm = searchTerm.toUpperCase();
var total = document.getElementById('totalStops').value;
if(type == 'all')
{
for(j=0;j<total;j++)
{
	if(searchTerm != '')
	{
			t = document.getElementById('li'+j).title;
			t = t.toUpperCase();
			y = t.indexOf(searchTerm);
			if(y != -1)
		{
			$('#li'+j).show();
		}
		else
		{
			$('#li'+j).hide();
		}
	}
	else
	{
		$('#li'+j).show();		
	}


}
}
else if(type == 'selected')
{
for(j=0;j<total;j++)
{
	x = document.getElementById('li'+j).className;
	if(x == 'list-group-item li-selected')
	{
		if(searchTerm != '')
		{
			t = document.getElementById('li'+j).title;
			t = t.toUpperCase();
			y = t.indexOf(searchTerm);
			if(y != -1)
			{
				$('#li'+j).show();
			}
			else
			{
				$('#li'+j).hide();
			}
		}
		else
		{
			$('#li'+j).show();		
		}
	}
	else
	{
		
		$('#li'+j).hide();
	}
}
}
else if(type == 'not-selected')
{
for(j=0;j<total;j++)
{
	x = document.getElementById('li'+j).className;
	if(x == 'list-group-item li-selected')
	{
		$('#li'+j).hide();
	}
	else
	{
		if(searchTerm != '')
		{
			t = document.getElementById('li'+j).title;
			t = t.toUpperCase();
			y = t.indexOf(searchTerm);
			if(y != -1)
			{
				$('#li'+j).show();
			}
			else
			{
				$('#li'+j).hide();
			}
		}
		else
		{
			$('#li'+j).show();		
		}
	}
}
}

}




function savedatasample(url,params,special,prefix,textLength,loadingid,responsediv,showdiv,hidediv)
{
	$('#'+showdiv).show();
	$('#'+hidediv).hide();
	toast("Data Successfully Updated","toast bg-success",0);
}

function rotateNotBox(id)
{
}


function changeParent()
{
$('#newParents').show();	
$('#selectedParents').hide();	
}


function setJsColor()
{

	var itemSearch = $('.jsColor');
		searchList = $('#bottomDiv').find(itemSearch).each(function(){

	var thisId = $(this).attr('id');
			new jscolor(document.getElementById(thisId));
		});

}


function checkSubReq(from,to,prefix,trigger)
{

	for(i=from;i<=to;i++)
	{
		if(document.getElementById(prefix+i))
		{
			thiselm = document.getElementById(prefix+i);
			eVal = evaluate(thiselm);
				if(eVal == '#ERROR30001#')
				{
					return false;
				}
		}
	}

	$('#'+trigger).trigger('click');
}



function addDueRows(value)
{

	if(document.getElementById('thisisstudentrow'))
	{
var ta = 'fdueStd';
var tb = 'fdueamtStd';
var tc = 'dueRowsStd';
	}
	else
	{
var ta = 'fdue';
var tb = 'fdueamt';
var tc = 'dueRows';
	}
	html = '';
		for(j=0;j<value;j++)
		{
			html+= '<tr><td><input type="date" class="input" id="'+ta+j+'" name=""></td><td><input type="number" onkeyup="calculateSumFeeAllot();" class="input" id="'+tb+j+'" name=""></td></tr>';
		}

		document.getElementById(tc).innerHTML = html;
}

function calculatetotalmarks(elm,k)
{
	var sum =0;
	var total = document.getElementById('sjtotal'+k).value;
	for(j=0;j<total;j++)
	{
		if(document.getElementById('sj_'+k+'_'+j))
		{
			var thisVal = document.getElementById('sj_'+k+'_'+j).value;
			if(thisVal == '')
			{
				thisVal = 0;
			}
			sum = sum+parseInt(thisVal);
		}
	}
	console.log(sum);
	document.getElementById('sjsum'+k).innerHTML = sum;
}


function rotateExamSelect(elm)
{
			var itemSearch = $('.list-group-item');
		searchList = $('#examTab').find(itemSearch).each(function(){
			thisid = $(this).attr('id');
			document.getElementById(thisid).className= 'list-group-item';
			});

		elm.className= 'list-group-item li-selected';

}



function toggleTopMenu(elm)
{
	if(document.getElementById('menuSection').lang == '0')
	{
			$('#menuSection').animate({top:'50px'});		
			document.getElementById('menuSection').lang = '1';
	}
	else
	{
			$('#menuSection').animate({top:'-2000px'});
			document.getElementById('menuSection').lang = '0';
	}

	elm.classList.toggle("change");

}

function closeTopMenu()
{
			if(document.getElementById('menuSection').lang == '1')
			{
				document.getElementById('menuBar').classList.toggle("change");				
			$('#menuSection').animate({top:'-2000px'});
			document.getElementById('menuSection').lang = '0';
	
			}

	
}


function dummyMinheight()
{
		showProcessing();
	setTimeout(function(){
		hideProcessing();
		$('.minheight').slideDown();
	},2000);

}


function applyFilter()
{
	var field = document.getElementById('filterField').value;
	var condition = document.getElementById('filterCondition').value;
	var value = document.getElementById('filterValue').value;
	if(value != '')
	{
		var spanId = new Date().getTime();
		spanId = spanId+"filtSpan"
		var html = '<span class="filterspan" id="'+spanId+'">'+field+' '+condition+' \''+value+'\'&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-times" onclick="$(\'#'+spanId+'\').remove();"></i></span>';
		document.getElementById('applybuttonBox').innerHTML += html;	
		$('#filterBox').slideToggle();$('#applybuttonBox').slideToggle();
		showProcessing();
		setTimeout(function(){
			hideProcessing();
			$('.minheight').slideDown();
		},2000);
	}
	else
	{
		toast("Please enter a value","toast bg-danger","3000");
	}

}

function applyMyTodoFilter()
{
	var spanId = new Date().getTime();
	var html = '<span class="filterspan" id="'+spanId+'">mytodo="yes"&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-times" onclick="$(\'#'+spanId+'\').remove();"></i></span>';
	document.getElementById('applybuttonBox').innerHTML += html;	
	showProcessing();
	setTimeout(function(){
		hideProcessing();
	},2000);

}


function addTagRow()
{
	document.getElementById('tagInputHodler').innerHTML += '<input type="text" class="input" name="req" placeholder="Enter Tag" style="margin-top:10px;"> ';
}


function rotateMenuItems(usedclass,id)
{
	console.log('ab');
$('.'+usedclass).hide();
$('#'+id).fadeIn();
if(usedclass == 'stMenuDivs')
{
	scrolltodiv('mainDataHolder',id);
}
}

function scrolltodiv(parent,element)
{

 $('#'+parent).animate({
       scrollTop: $("#"+element)[0].offsetTop - $('#'+parent)[0].offsetTop,
   }, 300, "swing");

/*
	 $('#'+parent).animate({
        scrollTop: $("#"+element).offset().top},
        'slow');
        */
}

function saveComment(id,type)
{
	var url = "comments/save.php";
	var params = "type="+type+"&dataid="+id+"&comment="+document.getElementById('commentBox').value;
	genAjax(url,params,function(response){

        document.getElementById('commentBox').value = '';

        var newRow = response;
        document.getElementById('commentLineTable').insertAdjacentHTML('afterBegin',newRow);
         setTimeout(function(){
		        $('.newlyAdded').fadeIn('slow');
		    },100);
		})
}

function postComment(id,classid)
{
    
    var x = document.getElementById('comBox').value;
    var url = "remarks/save.php";
    var data = "message="+x+"&id="+id+"&classid="+classid;
    genAjax(url,data,function(response){
        document.getElementById('comBox').value = '';

        var newRow = response;
        document.getElementById('commentLine').insertAdjacentHTML('afterBegin',newRow);
            setTimeout(function(){
        $('.noDataBox').slideUp();
        $('.newlyAdded').slideDown();

    },300);


    });

}
function addRowDummy(parent,copy)
{
	document.getElementById(parent).insertAdjacentHTML('beforeEnd',document.getElementById(copy).innerHTML)
}


function dummySuccess()
{
	showProcessing();
	setTimeout(function(){
		hideProcessing();
                toast("Successfully Updated","toast bg-success","3000");
	},1000);

}


function dummyProcessing()
{
	showProcessing();
	setTimeout(function(){
		hideProcessing();
	},1000);

}


function dummyButtonActions()
{
	$('.btn').click(function(){
            var tj = $(this).html();
            tj = tj.toLowerCase();
            if(tj.indexOf('cancel') != -1 || tj.indexOf('update report') != -1)
            {
              $('#myModalBig').modal('hide');
              $('#myModal').modal('hide');
              dummyProcessing();
            }
            if(tj.indexOf('save') != -1)
            {
              $('#myModalBig').modal('hide');
              $('#myModal').modal('hide');
              dummyProcessing();
              setTimeout(function(){
                toast("Successfully Updated","toast bg-success","3000");
              },3000);
            }

          })
}

function addQuick()
{
	dummyProcessing();
	toast("Item added to quick section","toast bg-success","4000");
}



function dummyAtMark()
{
	$('.atmarked').click(function(){
              $('.tooltipCutom').hide();
              var html = $(this).html();
              if(html.indexOf('warning') != -1)
              {
                html = html.replace("warning","check");
              }
              else
              {
                html = html.replace("check","warning"); 
              }
              $(this).html(html);
            })
}


function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
	$('.tooltipdesign').hide();
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault(); 
    var data = ev.dataTransfer.getData("text");
    if(data.indexOf('teach') != -1)
    {
    	text = document.getElementById(data).innerHTML;    	
    	if((!ev.target.id) || ev.target.id == '')
    	{
    		pid = ev.target.parentNode.id;
    	}
    	else
    	{
    		pid = ev.target.id
    	}

    	pid = pid.split("_");
    	$('.timeSelected').attr('class','timeDone');
    	document.getElementById('td_'+pid[1]).className = 'timeSelected';
    	document.getElementById('teacher_'+pid[1]).innerHTML = '<i class="fa fa-user"></i>&nbsp;&nbsp;'+text;
    	document.getElementById('teacher_'+pid[1]).className = 'typeWriter';
    }

    else if(data.indexOf('sbj') != -1)
    {
    	text = document.getElementById(data).innerHTML;    	
    	if((!ev.target.id) || ev.target.id == '')
    	{
    		pid = ev.target.parentNode.id;
    	}
    	else
    	{
    		pid = ev.target.id
    	}
    	pid = pid.split("_");
    	$('.timeSelected').attr('class','timeDone');
    	document.getElementById('td_'+pid[1]).className = 'timeSelected';
    	document.getElementById('subject_'+pid[1]).innerHTML = text;
    	document.getElementById('subject_'+pid[1]).className = 'typeWriter';
    }   
    else if(ev.target.id == 'not-selected-divs' || ev.target.id == 'selected-divs')
    {
    	ev.target.appendChild(document.getElementById(data));
    } 
    else if(ev.target.className == 'sbj-teacher')
    {
    	ev.target.appendChild(document.getElementById(data));	
    }
}


function addBck(elm)
{
	elm.style.backgroundColor = '#fffbe8'; 
	elm.style.borderColor = 'rgb(154,153,202)'; 
	elm.style.borderWidth = '2px';
	elm.style.borderStyle = 'dashed';
}


function removeBck(elm)
{
	elm.style.backgroundColor = '#fff'; 
	elm.style.borderColor = '#fff'; 
	elm.style.borderWidth = '2px';
	elm.style.borderStyle = 'dashed';

}


function setColumnSelectionRule()
{
	$('.selectTable td').click(function(){
		$thisTab = $(this).closest('table');
		var itemSearch = $('td');
		searchList = $thisTab.find(itemSearch).each(function(){
					thisId = $(this).attr('class','');		
				});
		$(this).attr('class','selected');
	});
}



var currentEod = 0;
var maxEod =0;
function setEodMech()
{
	var j=0;
				var itemSearch = $('li');
		searchList = $('#eodlistcontainer').find(itemSearch).each(function(){
			thisId = 'eodMenu'+j;
			thisId = $(this).attr('id',thisId);
			thisId = $(this).attr('lang',j);
			$(this).click(function(){
				$('.eodHolder').hide();
				var lang = $(this).attr('lang');
				$('#eodHolder'+lang).show();
				currentEod = lang;
			})
			maxEod = j;
			j++;
		});


	j=0;
				var itemSearch = $('.eodHolder');
		searchList = $('#eodfieldsholder').find(itemSearch).each(function(){
			thisId = 'eodHolder'+j;
			thisId = $(this).attr('id',thisId);
			j++;
		});

}

function prevEod()
{
	if(currentEod != 0)
	{
				$('.eodHolder').hide();
				currentEod = currentEod - 1;
				$('#eodHolder'+currentEod).show();

	}
}


function nextEod()
{
	if(currentEod != maxEod)
	{
				$('.eodHolder').hide();
				currentEod = currentEod + 1;
				$('#eodHolder'+currentEod).show();

	}
}

function collectTimetableData()
{
var itemSearch = $('td');
thisData = '';
		searchList = $('#timetableTable').find(itemSearch).each(function(){
			
			
			thisId = $(this).attr('id');
			if(thisId != '0')
			{
					

		
						 temp = $(this).attr('lang');

						temp = temp.split(":::");

						thisDay = temp[0];

						
						thisLecture = temp[1];

						thisSubject = $(this).find('#subject').attr('lang');

						thisTeacher = $(this).find('#teacher').attr('lang');

						if(thisSubject != undefined && thisTeacher != undefined)
						{
							thisData = thisData+"THISISBREAKER"+thisDay+":::"+thisLecture+":::"+thisSubject+":::"+thisTeacher;								
						}


			

			}
			
			
		});

var sessionid = document.getElementById('timeValSession').value;
var classid = document.getElementById('timeValClass').value;
params = 
{
	details:thisData,
	sessionid: sessionid,
	classid:classid
}
		var url = 'timetable/save.php';
		genAjax(url,params,function(response){
			console.log(response);
		})
}


function addNewTableRow(tableid)
{
	var html = document.getElementById(tableid+'_0').innerHTML;
	var table = document.getElementById(tableid);
	var rowsCount = table.rows.length;
	var newRowId = rowsCount-1;
	var newRowNum = newRowId;
	newRowId = tableid+'_'+newRowId;
	html = '<tr id="'+newRowId+'">'+html+'</tr>';
	var thisId = "_"+newRowNum+"_";
	html = html.replace(/_0_/g,thisId);
	document.getElementById(tableid).insertAdjacentHTML('beforeEnd',html);
	var itSearch = $('input');
	$('#'+newRowId).find(itSearch).each(function(){
		var thisId = $(this).attr('id');
		console.log(thisId);
		document.getElementById(thisId).value = '';
	});

	var itSearch = $('select');
	$('#'+newRowId).find(itSearch).each(function(){
		var thisId = $(this).attr('id');
		console.log(thisId);
		document.getElementById(thisId).selectedIndex = "0";
	});


	
}

globalAdditionalParams = '';

var lastDivVal = '';
function collectTableValues(tableid,url,params,special,prefix,textLength,loadingid,responsediv,showdiv,hidediv)
{		
	globalAdditionalParams ='';
	
	var error = 0;
	var dataArray = [];
	var headArray = '';
	var table = document.getElementById(tableid);
	var rowsCount = table.rows.length;
	for(j=0;j<rowsCount;j++)
	{
		if(document.getElementById(tableid+"_"+j))
		{
			dataArray[j] = '';
			var thisRowid = tableid+"_"+j;
				var itemSearch = $('input');
						searchList = $('#'+tableid+"_"+j).find(itemSearch).each(function(){
				
					var thisId = $(this).attr('id');
					var thisHead = $(this).attr('name');
					var thisTitle = $(this).attr('title');
					var thisValue = $(this).val();
					if(!validateTableRowField(thisId))
					{
						toast('Please enter correct value for '+thisTitle,'toast bg-danger','4000');
						error++;
						return false;
					}
					
							dataArray[j] += thisValue+":::";
							if(j==0)
							{
								if(headArray != '')
								{
									headArray = headArray+","+thisHead;
								}
								else
								{
									headArray = thisHead;
								}
							}
						});
							var itemSearch = $('select');
						searchList = $('#'+tableid+"_"+j).find(itemSearch).each(function(){
					var thisId = $(this).attr('id');
					var thisHead = $(this).attr('name');
					var thisValue = $(this).val();
					var thisTitle = $(this).attr('title');
					if(!validateTableRowField(thisId))
					{
						toast('Please enter correct value for '+thisTitle,'toast bg-danger','4000');
						error++;
						return false;
					}
					

							dataArray[j] += thisValue+":::";
							if(j==0)
							{
								if(headArray != '')
								{
									headArray = headArray+","+thisHead;
								}
								else
								{
									headArray = thisHead;
								}
							}

						});

			
		}
	}
	
	
	if(error == 0 && url != '')
	{
	globalAdditionalParams=dataArray.join("THISISBREAKER");
	console.log(globalAdditionalParams);
	savedata(url,params,special,prefix,textLength,loadingid,responsediv,showdiv,hidediv);
	}

	if(url == '')
	{
		return dataArray.join("THISISBREAKER");
	}
}







function validateTableRowField(id)
{
	var lang = document.getElementById(id).lang;
	var value = document.getElementById(id).value;
	if(lang =='req' && value == '')
	{
		document.getElementById(id).focus();
		return false;
	}
	else return true;

	if(lang =='reqismobile' && value == '')
	{
		document.getElementById(id).focus();
		return false;
	}
	else return true;

	if(lang =='isdec' && value == '')
	{
		document.getElementById(id).focus();
		return false;
	}
	else return true;
	
	if(lang =='reqisnum' && value == '')
	{
		document.getElementById(id).focus();
		return false;
	}
	else return true;

	if(lang =='reqisPassword' && value == '')
	{
		document.getElementById(id).focus();
		return false;
	}
	else return true;

	if(lang =='reqisEmail' && value == '')
	{
		document.getElementById(id).focus();
		return false;
	}
	else return true;
}


function toggleAttendance(elm)
{
	var lang = elm.lang;
	var temp = lang.split("::");
	thisdate = temp[1];
	classdate = thisdate;
	var month = document.getElementById('monthVal').value;

	var n = new Date();
	var today = n.getDate();
	var thisMonth = n.getMonth();
	thisMonth = thisMonth+1;
	var thisyear = n.getFullYear();
	var todaydate = thisyear+"-"+thisMonth+"-"+today;	

	 thisdate= month+"-"+thisdate;



var a = new Date(todaydate);
var b = new Date(thisdate);
a = a.getTime();
b = b.getTime();
b = b - 86400000;



	if(a >= b)
	{ 

			var x = elm.innerHTML;
			if(x.indexOf('check') != -1)
			{

				x = x.replace("fas fa-check","fas fa-times");

			
				elm.innerHTML = x;

			}
			else if(x.indexOf('times') != -1)
			{

				x = x.replace("fas fa-times","fas fa-check");
				elm.innerHTML = x;
			}
			else if(x.indexOf('circle') != -1)
			{
				x = x.replace("far fa-circle","fas fa-check");
				elm.innerHTML = x;
			}
			elm.className = 'thisisdate'+classdate+' atBox';
	}
	else
	{
		toast("Cannot add attendance for future dates.","toast bg-danger","3000");
	}

}

function employeeAttendanceAction(elm,url)
{
	if(document.getElementById('deductionmode').checked == true)
	{
		getModal(url,'formModal','tableModal','loading')
	}
	else
	{
		toggleAttendance(elm);
	}
}

function markAttendance()
{
	var status = 0;
	var itemSearch = $('.atBox');
		searchList = $('#dataTableAttendance').find(itemSearch).each(function(){
				var thisLang = $(this).attr('lang');
				var html = $(this).html();
				if(html.indexOf('check') != -1)
				{
					thisStatus = 1;
				}
				else if(html.indexOf('times') != -1)
				{
					thisStatus = 2;
				}
				else if(html.indexOf('circle') != -1)
				{
					thisStatus = 0;
				}
				status = status+"---"+thisLang+"::"+thisStatus;
		});
		
		var fullMonth = document.getElementById('monthVal').value;
		var params = "month="+fullMonth+"&status="+status;
		var url = "attendance/save.php";
		genAjax(url,params,function(response){
			console.log(response);
		})

}


function markAttendanceEmployee()
{
	var status = 0;
	var itemSearch = $('.atBox');
		searchList = $('#dataTableAttendance').find(itemSearch).each(function(){
				var thisLang = $(this).attr('lang');
				var html = $(this).html();
				if(html.indexOf('check') != -1)
				{
					thisStatus = 1;
				}
				else if(html.indexOf('remove') != -1)
				{
					thisStatus = 2;
				}
				else if(html.indexOf('circle') != -1)
				{
					thisStatus = 0;
				}
				status = status+"---"+thisLang+"::"+thisStatus;
		});
		var departmentId = document.getElementById('departmentVal').value;
		var fullMonth = document.getElementById('monthVal').value;
		var params = "department="+departmentId+"&month="+fullMonth+"&status="+status;
		var url = "employee-attendance/save.php";
		genAjax(url,params,function(response){
			$('#dispAttdance').trigger('click');
			console.log(response);
		})

}


function searchTable(table,searchterm)
{
	searchterm = searchterm.toLowerCase();;
	var table = document.getElementById(table);
	if(searchterm == '')
	{
		for(j=1;j<table.rows.length;j++)
		{
			thisRow = table.rows[j];
			thisRow.style.display = 'table-row';
		}		
	}
	else
	{
		for(j=1;j<table.rows.length;j++)
		{
			thisRow = table.rows[j];
			thisRow.style.display = 'table-row';
			var rowData = thisRow.innerHTML;
			rowData = rowData.replace(/<(?:.|\n)*?>/gm, '');
			rowData = rowData.toLowerCase();;
			if(rowData.indexOf(searchterm) == -1)
			{
				thisRow.style.display = 'none';
			}
		}	
	}

}

function fillPermissions()
{
var desig  = document.getElementById('acc326').value;
if(desig != '')
{
showProcessing();
//    $('#permdata').slideDown();
$('#permissionhandeler :input[type="checkbox"]').attr('checked',false);

var url = 'access-control/permission-data.php?id='+desig;
genAjax(url,'',function(response){
        document.getElementById('list').value = response;
        setTimeout(function(){

    var list = document.getElementById('list').value;

    list = list.split(",");



    for(j=0;j<list.length;j++)
    {

        var id = $('#permissionhandeler :input[value="'+list[j]+'"]').attr('id');
        if(document.getElementById(id))
        {
                    document.getElementById(id).checked = true;
        }

    }

        },1000);
});
}

}


function admissionProceed()
{

var eval = 	getParams('','stadm','19');

if(eval != '#ERROR30001#')
{
	$('#data-step').hide();$('#class-step').show();	
}


}


function calculateSalary()
{
	var totalEarningCount = document.getElementById('earnValue').value;
	var sumEarn = 0;
	for(j=0;j<totalEarningCount;j++)
	{
		if(document.getElementById('earn'+j))
		{
			sumEarn = sumEarn+parseFloat(document.getElementById('earn'+j).value);
		}
	}
	document.getElementById('totalEarnings').value = sumEarn;

	var totalDedcutionCount = document.getElementById('dedValue').value;
	var sumDed = 0;
	for(j=0;j<=totalDedcutionCount;j++)
	{
		if(document.getElementById('ded'+j))
		{
			sumDed = sumDed+parseFloat(document.getElementById('ded'+j).value);
		}
	}
	document.getElementById('totalDeductions').value = sumDed;
}

function changeDeductionValue()
{
	var total = document.getElementById('thisDeduction').innerHTML;
	total = parseFloat(total);

	var availablePool = document.getElementById('availablePool').innerHTML;
	availablePool= parseFloat(availablePool);
	
	var pool = document.getElementById('thisDeductionPool').value;
	if(pool == '')
	{
		pool = 0;
		document.getElementById('thisDeductionPool').value = 0;
	}
	pool = parseFloat(pool);

	
	if(pool > total)
	{
		toast("Pool deduction cannot be greater than " +total,'toast bg-danger','3000');
		document.getElementById('thisDeductionPool').value = 0;
		document.getElementById('thisDeductionSalary').innerHTML = total;
		return false;
	}
	else if(pool > availablePool)
	{
		toast("Pool deduction cannot be greater than " +availablePool,'toast bg-danger','3000');
		document.getElementById('thisDeductionPool').value = 0;
		document.getElementById('thisDeductionSalary').innerHTML = total;
		return false;

	}
	else
	{
		var diff = total - pool;


		document.getElementById('thisDeductionSalary').innerHTML = diff;
		var ldid = document.getElementById('ldidval').value;
		var salary = document.getElementById('salCont').value;

		document.getElementById(ldid).value = salary * diff/30;
		calculateSalary();

	}

}

function saveSalarySlip()
{
	var totalEarningCount = document.getElementById('earnValue').value;
	var sumEarn = 0;
	var earningList = '';
	var earningListIds = '';
	var deductionList = '';
	var deductionListIds = '';

	for(j=0;j<totalEarningCount;j++)
	{
		if(document.getElementById('earn'+j))
		{
			earningList = earningList+parseFloat(document.getElementById('earn'+j).value)+",";
			earningListIds = earningListIds+parseFloat(document.getElementById('earnid'+j).value)+",";
		}
	}
	var totalEarnings = document.getElementById('totalEarnings').value; 


var totalDedcutionCount = document.getElementById('dedValue').value;
for(j=0;j<totalDedcutionCount;j++)
	{
		if(document.getElementById('ded'+j))
		{
			deductionList = deductionList+parseFloat(document.getElementById('ded'+j).value)+",";
			deductionListIds = deductionListIds+parseFloat(document.getElementById('dedid'+j).value)+",";
		}
	}
	var totalDeductions = document.getElementById('totalDeductions').value; 

var poolDeductions = document.getElementById('thisDeductionPool').value;

var params = "earninglist="+earningList+"&earninglistids="+earningListIds+"&deductionlist="+deductionList+"&deductionlistids="+deductionListIds+"&pooldeductions="+poolDeductions+"&totalearnings="+totalEarnings+"&totaldeductions="+totalDeductions;
var url = "payslips/save-salary.php?empid="+document.getElementById('paySlipEmp').value+"&month="+document.getElementById('paySlipMonth').value;
genAjax(url,params,function(response)
	{
		$('#getSalButton').trigger('click');

});
}

function deleteSalary(id)
{
	var url = "payslips/deletesalary.php?id="+id;
	genAjax(url,'',function(){
		$('#getSalButton').trigger('click');

	});
}

function selectStop(id)
{
	$('#li'+id).trigger('click');
}

function getSelectValues(select) {
  var result = '';
  var options = select && select.options;
  var opt;

  for (var i=0, iLen=options.length; i<iLen; i++) {
    opt = options[i];
    if(opt.selected)
    {
	    result = opt.value+","+result;    
    }

  }
  return result;
}



function getHelp()
{
    var hash = window.location.hash;
    console.log(hash);
    hash = hash.split("rtl=");
    
    if(hash[1].indexOf('setup/general/') != -1)
    {
        temp = hash[1].split("&");
        var val = temp[0];
    }
    else if(hash[1].indexOf("?") != -1)
    {
        temp = hash[1].split("?");
        var val = temp[0];
    }
    else
    {
        var val = hash[1];
    }
    url = "help/index.php?type="+val;
    getModal(url,"tableModalBig","formModalBig","loading");
}





function changePassword(){
	var old = document.getElementById('oldP').value;
	var orgOld = document.getElementById('oldAlready').value;
	var new1 = document.getElementById('new1').value;
	var new2 = document.getElementById('new2').value;


if(old == '' || new1 == '' || new2 == '')
{
				errorMessage = "Please enter all the fields.";
				elm = document.getElementById('oldP');
				showError(errorMessage,elm,1);
				return false;

}

else if(old != orgOld)
{
				errorMessage = "Invalid Old Password";
				elm = document.getElementById('oldP');
				showError(errorMessage,elm,1);
				return false;
}
else if(new1 != new2)
{	
				errorMessage = "Passwords in both fields must match";
				elm = document.getElementById('new1');
				showError(errorMessage,elm,1);
				return false;
}
else
{
	var url = 'savePassword.php?new='+new1;
	genAjax(url,'',function(response){
	    
	    	toast("Password Successfully Changed",'toast bg-success','3000')
	    closeBottom();


		})	
}
}

function letThisEmployeeGo(id) {
    
    var text
    var r = confirm("Are you sure, you want to remove this Employee? \nOnce you click 'OK', entire data corresponding to this employee will be deleted. ");
    if (r == true) {
        var url = "employee/hideEmployee.php?id="+id;
        console.log('here');
         genAjax(url,'',function(){
			toast("Successfully marked as left.","toast bg-success","3000");
			closeBottom();
			$('#employees').trigger('click');
			

		})
		 document.getElementById('empleft').style.display = 'block';		
    } 
    else {
        text = "You pressed Cancel!";
        console.log(text);
    }
    
    
}


function toogleAttendanceStatus(status,date)
{
    $('.thisisdate'+date).trigger('click');
}


function searchClassTable(value)
{
	value = value.toUpperCase();
var total = document.getElementById('totalClassCount').value;
if(value == '')
{
for(j=0;j<total;j++)
{

		$('#classlistRow'+j).show();

}
}
else
{
for(j=0;j<total;j++)
{
	x = document.getElementById('classlistTd'+j).innerHTML;
	if(x.indexOf(value) != -1)
	{
		$('#classlistRow'+j).show();
	}
	else
	{
		$('#classlistRow'+j).hide();
	}
}
}

}

function selectAllClass(selector)
{	
	if(selector.lang == '1')
	{
			todo = '1';
			selector.lang = '0';
	}
	else
	{
		todo = '0';
		selector.lang = '1';
	}

	var total = document.getElementById('totalClassCount').value;
	for(j=0;j<total;j++)
	{
		if(document.getElementById('classlistRow'+j))
		{
			elm = document.getElementById('classlistRow'+j);		
if(todo == '1')
{
		elm.lang = '0';
		x = elm.className;
		x = x.replace(" selectedItem","");
		elm.className = x;
}
else
{

		elm.lang = '1';
		x = elm.className;
		if(x.indexOf('selectedItem') == -1)
		{
		x = x + " selectedItem";			
		}

		elm.className = x;
	
}
		}
	}
}


function addTextToTextarea(textareaid,txtToAdd)
{
        var $txt = jQuery('#'+textareaid);
		var caretPos = $txt[0].selectionStart;
        var textAreaTxt = $txt.val();
        txtToAdd = '{{##'+txtToAdd+'##}}';
        $txt.val(textAreaTxt.substring(0, caretPos) + " " + txtToAdd+ " " + textAreaTxt.substring(caretPos) );	
}




function copytoall(prefix,current,emptyflag)
{
	var toput = document.getElementById(prefix+current).value;
	var totalCount = document.getElementById('totalRowCount').value;
	for(k=1;k<totalCount;k++)
	{
		if(emptyflag == 'empty')
		{
		if((document.getElementById(prefix+k)) && (document.getElementById(prefix+k).value == '') && (k!=current))
		{
			document.getElementById(prefix+k).value = toput;
			document.getElementById(prefix+k).className = 'inputChanged';
		}

		}
		else
		{

		if((document.getElementById(prefix+k)) && (k!=current))
		{
			document.getElementById(prefix+k).value = toput;
			document.getElementById(prefix+k).className = 'inputChanged';
		}

		}


	}

	$('.inputChanged').bind('click',function(){
		var id = $(this).attr('id');
		document.getElementById(id).className = 'input';
	})
}

function sendPreview(prefix)
{
	var valueStr = '';
	var amountStr = '';
	var dateStr = '';
	var homeworkStr = '';

	var totalCount = document.getElementById('totalRowCount').value;
	for(j=1;j<totalCount;j++)
	{
		if(document.getElementById(prefix+'check'+j))
		{
			if(document.getElementById(prefix+'check'+j).checked == true)
			{
				valueStr = valueStr+"$$**$$"+document.getElementById(prefix+'check'+j).value;
				amountStr = amountStr+"$$**$$"+document.getElementById('amount'+j).value;
				dateStr = dateStr+"$$**$$"+document.getElementById('duedate'+j).value;
				if(document.getElementById('homeworktext'+j))
				{
					homeworkStr = homeworkStr+"$$**$$"+document.getElementById('homeworktext'+j).value;
				}
			}
		}
	}

	var adtext = document.getElementById('customTextArea').value;

	var params = "ids="+valueStr+"&amount="+amountStr+"&dates="+dateStr+"&adtext="+adtext;

	genAjax(url,params,function(response){
		document.getElementById('bottomDiv').innerHTML = response;
		$('#bottomDivContainer').animate({top:'0px'})
	});
}


function sendotp()
{
	var mobile = document.getElementById('vis1').value;
	if(mobile > 6000000000 && mobile <= 9999999999)
	{
	var url = 'visitor/generate-otp.php?mobile='+mobile;
	genAjax(url,'',function(response){
		document.getElementById('otpval').lang = response;
		$('#otpBox').fadeIn();
	});		
	}
	else
	{
		toast("Invalid mobile number","toast bg-danger","3000");
	}
}

function verifyotp()
{
	if(document.getElementById('otpval').value == document.getElementById('otpval').lang)
	{
		console.log('here');
		$('#layover1').hide();
		$('#layover2').hide();
		$('#otpBox').fadeOut();
		toast("<i class='fas fa-check'></i>Mobile Number Verified","toast bg-success","3000");
	}
	else
	{
		console.log('here-1');
		$('#layover1').show();
		$('#layover2').show();

		toast("Invalid OTP. Please try again.","toast bg-danger","3000");
	}
}



function checkotpgatepass()
{
	if(document.getElementById('gateotp').value == document.getElementById('gateotp').lang)
	{
				$('#otpBox').slideUp();
				toast("<i class='fas fa-check'></i>Mobile Number Verified","toast bg-success","3000");
				$('#restForm').slideDown();
				$('#stdetails').slideUp();
	}
	else
	{

		toast("Invalid OTP. Please try again.","toast bg-danger","3000");
	}
}

flatColors = '#1abc9c,#2ecc71,#3498db,#9b59b6,#34495e,#f1c40f,#e67e22,#e74c3c,#ecf0f1,#95a5a6,#16a085,#27ae60,#2980b9,#8e44ad,#2c3e50,#f39c12,#d35400,#c0392b,#bdc3c7,#7f8c8d';
flatColors = flatColors.split(",");
var curFlat=-1;
function getFlatColor()
{
	if(curFlat >= 19)
	{
		curFlat = -1;	
	}
	curFlat++
	return flatColors[curFlat];
}



function fillLetterBox()
{
		var itemSearch = $('.letterBox');
		searchList = $('#calStripContainer').find(itemSearch).each(function(){
			thisId = $(this).attr('id');
			
			document.getElementById(thisId).style.backgroundColor = getFlatColor();

    });
		
}



function resetWindow()
{
		$('#sendHereOnly').slideUp();
	$('#simpleProceed').slideDown();
}

function checkMsgWindow()
{
	var found = 0;
var empid = document.getElementById('inmsg0').value;
		var itemSearch = $('.calStCIn');
		searchList = $('#calStripContainer').find(itemSearch).each(function(){
			thisLang = $(this).attr('lang');
			
			if(thisLang == empid)
			{
				found++;
				thisId = $(this).attr('id');

				$('#'+thisId).trigger('click');
			}
    });
	

if(found == 0)
{
	$('#sendHereOnly').slideDown();
	$('#simpleProceed').slideUp();
}
else
{
	closeBigModal();
}
}


function searchChat(event,value)
{
	console.log(value);
	value = value.toLowerCase();
	if(value == '')
	{

		var itemSearch = $('.calStCIn');
		searchList = $('#calStripContainer').find(itemSearch).each(function(){
			thisId = $(this).attr('id');
				$('#'+thisId).show();
    });
	}
			var itemSearch = $('.calStCIn');
			searchList = $('#calStripContainer').find(itemSearch).each(function(){
				

				thisTitle = $(this).attr('title');
				thisTitle = thisTitle.toLowerCase();

				thisId = $(this).attr('id');			
				if(thisTitle.indexOf(value) != -1)
				{
					$('#'+thisId).show();
				}
				else
				{
					$('#'+thisId).hide();	
				}
	    });
}




function sendMessage(convoid,touser)
{
	var message = document.getElementById('tomessage').value;
if(message == '')
{
	return false;
}
var params = 
{
	message:message,
	touser:touser,
	convoid:convoid
}

var url = 'messages/savemessage.php';
genAjax(url,params,function(response){

	var x = document.getElementById('calStripContainer'+convoid).innerHTML;
	var thisLang = document.getElementById('calStripContainer'+convoid).lang;
	var thisTitle = document.getElementById('calStripContainer'+convoid).title;
	var thisClick = $('#calStripContainer'+convoid).attr('onclick');
	$('#calStripContainer'+convoid).remove();
	var html= '<div id="calStripContainer'+convoid+'" lang="'+thisLang+'" title="'+thisTitle+'" onclick="'+thisClick+'">'+x+'</div>';
	document.getElementById('calStripContainer').insertAdjacentHTML('afterBegin',html);
	var temp = response;
	temp = temp.split('lang="');
	temp1 = temp[1].split('"');
	bubbleid = temp1[0];
document.getElementById('bubbleHolder').insertAdjacentHTML('beforeEnd',response);
            
            $("#bubbleHolder").animate({ scrollTop: $('#bubbleHolder').prop("scrollHeight")}, 300);
            	
                       // $("#bubbleHolder").scrollTo("#"+bubbleid, 300);
                        document.getElementById('tomessage').value = '';

});
}


function setIconColors(color)
{

			var itemSearch = $('i');
		searchList = $('#iconOver').find(itemSearch).each(function(){
			$(this).animate({color:color});
		});


}

function setIcon(elm)
{
	id = "icon"+elm.lang;
	icon = document.getElementById(id).innerHTML;
	document.getElementById('cust0').value = icon;
	document.getElementById('icondisplay').innerHTML = icon;
	$('#myModalBig').modal('hide');
}


function dynamicSearch(id,term)
{
	term= term.toLowerCase();
	var itemList = '';
	var itemSearch = $('.list-group-item');
		searchList = $('#'+id).find(itemSearch).each(function(){
			thisId =  $(this).attr('id');

			if(thisId != 'topLi')
			{
				temp = document.getElementById(thisId).innerHTML.toLowerCase();

				if(temp.indexOf(term) != -1)
				{
					document.getElementById(thisId).style.display = 'list-item';
				}
				else
				{
					document.getElementById(thisId).style.display = 'none';	
				}				
			}


		});
}

function selectAllList(id,putelement)
{
	var itemList = '';
	var itemSearch = $('.list-group-item');
		searchList = $('#'+id).find(itemSearch).each(function(){
			var thisId = $(this).attr('id')
			if(thisId != 'topLi')
			{
			thisClass =  $(this).attr('class','list-group-item li-selected');
				itemList+= $(this).attr('lang')+",";
			}


		});

		$('#'+putelement).val(itemList);
}

function deselectAllList(id,putelement)
{
	console.log(id);
	var itemSearch = $('.list-group-item');
		searchList = $('#'+id).find(itemSearch).each(function(){
			var thisId = $(this).attr('id')
			if(thisId != 'topLi')
			{
			thisClass =  $(this).attr('class','list-group-item');
			}


		});

		$('#'+putelement).val('');
}




function collectMultiValues(elm,ulclass,putelement)
{
	if(elm.className.indexOf('li-selected') == -1)
	{
		elm.className = 'list-group-item li-selected';
	}
	else
	{
		elm.className = 'list-group-item'
	}
	var itemList = '';
	var itemSearch = $('.list-group-item');
		searchList = $('#'+ulclass).find(itemSearch).each(function(){
			thisClass =  $(this).attr('class');
			if(thisClass == 'list-group-item li-selected')
			{
				console.log('here');
				itemList+= $(this).attr('lang')+",";
				console.log(itemList);
			}
			else
			{
				console.log('not here');
			}

		});
		document.getElementById(putelement).value = itemList;

}

function searchIcons(value)
{
	if(value.length >= 3 || value == '')
	{
		$('#minChar').hide();
		var itemSearch = $('.iconContainer');
		searchList = $('#iconOver').find(itemSearch).each(function(){
			var thisHtml = $(this).html();
			if(thisHtml.indexOf(value) != '-1')
			{
				$(this).show();
			}
			else
			{
				$(this).hide();
			}


		});
	}
	else
	{
		$('#minChar').show();
	}


}

function saveQuestion(id)
{	
	var question = CKEDITOR.instances['question'].getData();
	var serial = document.getElementById('serial').value;
	var option_a = document.getElementById('option_a').value;
	var option_b = document.getElementById('option_b').value;
	var option_c = document.getElementById('option_c').value;
	var option_d = document.getElementById('option_d').value;
	var answer = document.getElementById('answer').value;
	var marks = document.getElementById('marks').value;
	var url = '';

	var params = 
	{
		serial:serial,
		question:question,
		option_a:option_a,
		option_b:option_b,
		option_c:option_c,
		option_d:option_d,
		answer:answer,
		marks:marks
		
	}

if(id != '')
{
	url = 'questions/update.php?id='+id;
}
else if(id == '')
{
	url = 'questions/save.php';
}
	genAjax(url,params,function(response){
	toast("Data Updated successfully saved","bg-success toast","3000");
	$('#quesMenuList').trigger('click');

    })
}

