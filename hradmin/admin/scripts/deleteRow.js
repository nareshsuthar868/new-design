function crossCheckDelete(table,rowPrefix,tableid)
{
	var comfirmMessage = 'Do you really want to delete selected rows?&nbsp;&nbsp;<br/><br/><button type="button" class="btn btn-dark btn-sm"  onclick="deleteRow(\''+table+'\',\''+rowPrefix+'\',\''+tableid+'\')" id="confirmDelete"><i class="fas fa-check"></i>&nbsp;&nbsp;Yes!</button>&nbsp;&nbsp;<button type="button" class="btn btn-light btn-sm"  onclick="toast(\'Good one. There is no point deleting what is already in system. :) :)\',\'toast bg-success\',2000)"><i class="fas fa-times"></i>&nbsp;&nbsp;No</button>';
	toast(comfirmMessage,'toast bg-danger',0);
//showError(comfirmMessage,'',0);
}
function deleteRow(table,rowPrefix,tableid)
{
	console.log(tableid);
	var checkList = "0";
	var itemSearch = $('.checkInput');
	$('#'+tableid).find(itemSearch).each(function(){

		if(this.checked == true)
		{
			var thisVal = this.value;
			if(document.getElementById(rowPrefix+thisVal))
			{
				$('#'+rowPrefix+thisVal).hide();
			}
			checkList = checkList+","+thisVal;
		}
			
    });	
//    getModule("deleteRow.php?table="+table+"&ids="+checkList,'','','');
//disableButton('confirmDelete','Deleting..');
showProcessing();
var url = "deleteRow.php?table="+table+"&ids="+checkList;
genAjax(url,'',function(response){
hideProcessing();
var html = "Successfully deleted all the selected rows.";
toast(html,'toast bg-success',3000);
});

}

function retrieveRow(table,checkList)
{
	
showProcessing();
var url = "retrieveRow.php?table="+table+"&ids="+checkList;
	genAjax(url,'',function(response){
hideProcessing();
var rowList = checkList.split(",")
	for(j=0;j<rowList.length;j++)
	{
		var thisVal = rowList[j];
			if(document.getElementById(rowPrefix+thisVal))
			{
				$('#'+rowPrefix+thisVal).show();
				//document.getElementById('tableRow'+thisVal).style.display = 'table-row';				
			}

	}
	hideProcessing();
	var html = "Successfully retreived all rows deleted in previous action.";
	toast(html,'toast bg-success',0);
	});

}


