var errorOccured = 0;
var ongoingRequest = 0;
var orgHTML;

function forceSaveData(url,params,special,prefix,textLength,loadingid,responsediv,showdiv,hidediv)
{
	toast('Trying to save data again in 2 seconds..','toast bg-success','2000');
	setTimeout(function(){
	ongoingRequest = 0;
	hideToast();
	savedata(url,params,special,prefix,textLength,loadingid,responsediv,showdiv,hidediv);	
	},2000);
}

function savedata(url,params,special,prefix,textLength,loadingid,responsediv,showdiv,hidediv)
{

if(document.getElementById('checkAvail') && document.getElementById('checkAvail').innerHTML.indexOf('Checking Availability..') != -1)
{
	
	var errorNotif = 'Still checking input availability, please wait';
	toast(errorNotif,'toast bg-danger','2000');
	
}

if(document.getElementById('checkAvail1') && document.getElementById('checkAvail1').innerHTML.indexOf('Checking Availability..') != -1)
{
	
	var errorNotif = 'Still checking input availability, please wait';
	toast(errorNotif,'toast bg-danger','2000');
	
}


else if(ongoingRequest == 0)
{
	ongoingRequest = 1;
	var loadingVars = [];
	showProcessing();


tosendparams = getParams(params,prefix,textLength);
if(tosendparams.indexOf('#ERROR30001#') != -1)
{
	hideProcessing();
	ongoingRequest = 0;
	return false;
}
else
{


	$.post(url,
        {
          tosendparams
        },
        function(data,status){
        	ongoingRequest = 0;
        	if(data.indexOf('ERROR3005') != -1)
        	{
        		showError(data,'','');
        	}
        	else
        	{
       
        	toast('Data successfully updated and everything is all set.','toast bg-success','2000');	
        	if(responsediv != '')
        	{
        		document.getElementById(responsediv).innerHTML = data;
        		$('#'+responsediv).show();        		

        	}
          if(hidediv != '')
          {
      	    $('#'+hidediv).hide();
          }
          if(showdiv != '')
          {
      	    $('#'+showdiv).show();
          }

          if(url.indexOf('trigger') != -1)
          {
          	trig = url.split('trigger=');
          	trigid = trig[1];
          	$('#'+trigid).trigger('click');
          	
          }

          if(special != '')
          {
          	$('#'+special).trigger('click');
          	closeBottom();
          }      
		hideProcessing();

			if(special == 'myModalBig')
			{
				$('#myModalBig').modal('hide');
			}

if(special == 'myModal')
			{
				$('#myModal').modal('hide');
			}

		}
        });
}
}

else
{
	var errorNotif = 'Already trying to save. Please dont click again. <span style="text-decoration:underline;cursor:pointer" onclick="forceSaveData(\''+url+'\',\''+params+'\',\''+special+'\',\''+prefix+'\',\''+textLength+'\',\''+loadingid+'\',\''+responsediv+'\',\''+showdiv+'\',\''+hidediv+'\')">Force Retry</span>'
	toast(errorNotif,'toast bg-danger',0);
//showBottomNotification('stop',errorNotif)
}
}

function getParams(params,prefix,textLength)
{
	var thiselm;
	var paramArray = [];
	var eVal;

	for(j=0;j<textLength;j++)
	{
		thiselm = document.getElementById(prefix+j);
		
		eVal = evaluate(thiselm);
		if(eVal == '#ERROR30001#')
		{
			return '#ERROR30001#';
		}
		else
		{
			paramArray[j] = evaluate(thiselm);			
		}

	}

var temp = params.split(":::");
for(k=0;k<temp.length;k++)
{
	paramArray[k+j] = temp[k];
}

return paramArray;

}



var textTypeArray = Array("text","textarea","email","date","select-one","select","number","time","datetime-local");
var validateArray = Array("req","reqismobile","ismobile","isdec","reqisnum","reqisPassword","reqisEmail");

function evaluate(elm)
{

	
	for(t=0;t<textTypeArray.length;t++)
	{	

		if(elm.type == textTypeArray[t])
		{
			if(elm.lang == 'editor')
			{
				thisId = elm.id;
				return CKEDITOR.instances[thisId].getData();
			}
			var checkReq = elm.name.indexOf('req');
			if(checkReq != -1)
			{

				if(elm.value == '')
				{
					if(elm.title != '')
					{
						var errorMessage = 'Please enter a value for <strong>"'+elm.title+'"</strong>';					
					}
					else
					{
						var errorMessage = "Please fill all the mandatory fields.";
					}
					showError(errorMessage,elm,1);
					return '#ERROR30001#';

				}
				else
				{
					if(elm.name == 'reqismobile')
					{
						if(elm.value.length == 10 && isNumeric(elm.value))
						{
							return elm.value;												
						}
						else
						{
							var errorMessage = "Please enter a valid mobile number";
							showError(errorMessage,elm,1);
							return '#ERROR30001#';
						}
					}
					else if(elm.name == 'reqisdec')
					{
						if((elm.value % 1) == 0)
						{
							return elm.value;												
						}
						else
						{
							var errorMessage = "Please enter a valid decimal number";
							showError(errorMessage,elm,1);
							return '#ERROR30001#';
						}
					}
					else if(elm.name == 'reqisnum')
					{
						if(isNumeric(elm.value))
						{
							if(elm.max)
							{
								v1 = parseInt(elm.value);
								v2 = parseInt(elm.max);
								
								if(v1 > v2)
								{
									var errorMessage = "The value of "+elm.title+" cannot be greater than "+elm.max;
									showError(errorMessage,elm,1);
									return '#ERROR30001#';
								}
								else if(elm.min)
								{
									v3 = parseInt(elm.min);
									if(v1 < v3)
									{
									var errorMessage = "The value of "+elm.title+" cannot be less than "+elm.min;
									showError(errorMessage,elm,1);
									return '#ERROR30001#';										
									}
									else
									{
										return elm.value;	
									}
								}
								else
								{
									return elm.value;	
								}
							}
							else
							{
								return elm.value;
							}


																		
						}
						else
						{
							var errorMessage = "Please enter a valid number";
							showError(errorMessage,elm,1);
							return '#ERROR30001#';
						}
					}
					else if(elm.name == 'reqisEmail')
					{
						if(elm.value.indexOf('@') == -1 || elm.value.indexOf('.') == -1)
						{
							var errorMessage = "Please enter a valid email account";
							showError(errorMessage,elm,1);
							return '#ERROR30001#';							
						}
						else
						{
							return elm.value;
						}
					}
					else if(elm.name == 'reqisPassword')
					{
							var checkSpecial = isValidSpecial(elm.value);
							if(checkSpecial == true)
							{
							var errorMessage = "Password should have atleast one special character and 8 characters long, without spaces";
							showError(errorMessage,elm,1);
							return '#ERROR30001#';							
							}
							else if(elm.value.length < 8)
							{
							var errorMessage = "Password should have atleast one special character and 8 characters long, without spaces";
							showError(errorMessage,elm,1);
							return '#ERROR30001#';							
							}
							else if(elm.value.indexOf(" ") != -1)
							{
							var errorMessage = "Password should have atleast one special character and 8 characters long, without spaces";
							showError(errorMessage,elm,1);
							return '#ERROR30001#';							
							}

							else
							{
								return elm.value;	
							}
						
					}
					else
					{
						return elm.value;	
					}

				}
			}
			else
			{
				return elm.value;		
			}

		}
	}

	if(elm.type == 'checkbox')
	{
		if(elm.checked == true)
		{
			return elm.value;			
		}
		else
		{
			return 0;
		}

	}

}


function isValidSpecial(str){
 return !/[~`!#$%\^&*+=\-\[\]\\';,/@{}|\\":<>\?]/g.test(str);
}