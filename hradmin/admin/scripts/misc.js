function scrollToElement(elmid,minus)
{
	var div= document.getElementById(elmid);
   $('#'+elmid).animate({
      scrollTop: div.scrollHeight - (window.innerHeight-minus)
   }, 500);
}


var historyVal = 0;
var historyArray = [];
var innerChange = 0;
var outerChange = 0;
var massEditUrl = 0;

function showSearch()
{
$('#searchBox').animate({top:'0'},100);
}

function hideSearch()
{
$('#searchBox').animate({top:'-100px'},100);
}

function closeBigModal()
{
	console.log('called');

$("#myModalBig").modal("hide");
}

function toogleFormTable()
{
	if(document.getElementById('formDiv').style.display == 'none')
	{
		$('#formDiv').show();
		$('#tableDiv').hide();
	}
	else
	{
		$('#formDiv').hide();
		$('#tableDiv').show();		
	}
}


function toogleFormTableModal()
{
	if(document.getElementById('formModal').style.display == 'none')
	{
		$('#formModal').show();
		$('#tableModal').hide();
	}
	else
	{
		$('#formModal').hide();
		$('#tableModal').show();		
	}

}



function toogleFormTableModalBig()
{
	if(document.getElementById('formModalBig').style.display == 'none')
	{
		$('#formModalBig').show();
		$('#tableModalBig').hide();
	}
	else
	{
		$('#formModalBig').hide();
		$('#tableModalBig').show();		
	}

}

function addTask()
{
var d = new Date();
var n = d.getTime();
var x= '<div class="checkbox taskList" style="display:none" id="'+n+'">';
x+= '<label><input type="checkbox" value="">'+document.getElementById("taskAdder").value+'</label>';
x+= '</div>';
//var y = document.getElementById('taskList').innerHTML;
document.getElementById('taskList').insertAdjacentHTML('afterBegin',x);
document.getElementById("taskAdder").value = '';
$('#'+n).slideDown('fast');
}

function callCalModal()
{
getModal('projects/calData.php','tableModalBig','formModalBig','loading')
}

function showNotifBar()
{
$('#notifContainer').fadeToggle('fast')
}

function hideNotifBar()
{
$('#notifContainer').fadeOut('fast')
}

function showError(errorMessage,elm,markRed)
{
	if(markRed == 1)
	{
					elm.className = 'inputRed';
					elm.addEventListener('focus',function(){
						this.className = 'input';
					});	
					var thisid = elm.id;
					var thisParent= $('#'+thisid).parent();
					var scrollTop = $(thisParent).offset().top;
					
					scrollTop = $('#formBox').scrollTop() - (scrollTop * -1)-150;					
					
			        $('#formBox').scrollTop(scrollTop);
	}
	toast(errorMessage,'toast bg-danger','2000');
}

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

var notifTimeout;
function showBottomNotification(type,message)
{
	clearTimeout(notifTimeout);
	$('#bottomNotification').html("");

	if(type == 'stop')
	{
		toast(message)
		/*
		document.getElementById('bottomNotification').style.backgroundColor = "rgba(216,99,73,0.8)";
		$('#bottomNotification').animate({opacity:1,width:'330px',padding:'20px'},300,function(){
			$('#bottomNotification').animate({width:'300px'},100,function(){
			$('#bottomNotification').html(message);
			});

		});
	notifTimeout = setTimeout(function(){
		hideBottomNotification();
	},8000);
	*/

	}


	if(type == 'successdel')
	{
		document.getElementById('bottomNotification').style.backgroundColor = "rgba(95,207,128,0.8)";
		$('#bottomNotification').animate({opacity:1,width:'330px',padding:'20px'},300,function(){
			$('#bottomNotification').animate({width:'300px'},100,function(){
			$('#bottomNotification').html(message);
			});

		});
	notifTimeout = setTimeout(function(){
		hideBottomNotification();
	},20000);

	}


	if(type == 'success')
	{
		document.getElementById('bottomNotification').style.backgroundColor = "rgba(95,207,128,0.8)";
		$('#bottomNotification').animate({opacity:1,width:'330px',padding:'20px'},300,function(){
			$('#bottomNotification').animate({width:'300px'},100,function(){
			$('#bottomNotification').html(message);
			});

		});
	notifTimeout = setTimeout(function(){
		hideBottomNotification();
	},8000);

	}

}


function hideBottomNotification()
{
	$('#bottomNotification').html("");
			$('#bottomNotification').animate({opacity:0.3},500,function(){
			$('#bottomNotification').animate({opacity:0,width:'0px',padding:'0px'},200);
		});
}

function tableSearch(event,url)
{

	if(event.keyCode == '13')
	{
		var varName = "";
		var varValue = "";
		var i =0;
		var itemSearch = $('.searchInput');
		searchList = $('#tableDiv').find(itemSearch).each(function(){
			if($(this).val() != '')
			{
			varName += $(this).attr('name')+",";				
			varValue += $(this).val()+"!NBCOMNB!";
			}


    });
		
		checkAlready = url.indexOf("varList=");
		if(checkAlready == -1)
		{
			checkQue = url.indexOf("?");
			if(checkQue == -1)
			{
				url = url+'?varList='+varName+'&varValues='+varValue;				
			}
			else
			{
				url = url+'&varList='+varName+'&varValues='+varValue;				
			}

		}
		else
		{
			temp = url.split("varList=");
			url = temp[0]+'varList='+varName+'&varValues='+varValue;
		}
		getModule(url,'tableDiv','formDiv','loading');
				
	}			
}

function checkAll(elm,tableId)
{
	var itemSearch = $('.checkInput');
	if(elm.checked == true)
	{
		searchList = $('#'+tableId).find(itemSearch).each(function(){
			//$(this).attr('checked') = 'checked';	
			$(this).attr("checked","checked");		
			this.checked = true;	
    });

	}
	else
	{
		searchList = $('#'+tableId).find(itemSearch).each(function(){
			//$(this).attr('checked') = 'checked';	
			$(this).removeAttr("checked");		
			this.checked = false;		
    });		
	}
}

function bulkAction()
{
	var action = document.getElementById('action').value;
	var tempAction = action.split(":");
	if(tempAction[0] == 'D')
	{
		crossCheckDelete(tempAction[1]);
	}
	else
	{
		massEdit();
		//showBottomNotification('stop','Mass edit feature not available yet, but will appear soon');
	}
}

function disableButton(id,loading)
{
	var orgClasName = document.getElementById(id).className;
	var tempClassName = orgClasName+" disabled";
	document.getElementById(id).className = tempClassName;
	if(loading != '')
	{
		document.getElementById(id).innerHTML = "<img src='images/circle-primary.GIF' alt='' style='height:12px;margin-bottom:2px;'/>&nbsp;&nbsp;"+loading;
	}
}

function enableButton(id,orgHTML)
{
	var tempClasName = document.getElementById(id).className;
	var orgClassName = tempClasName.replace(" disabled","");
	document.getElementById(id).className = orgClassName;
	if(orgHTML != '')
	{
		document.getElementById(id).innerHTML = orgHTML;
	}

}

function sortThis(url,tableDiv,formDiv,loading,orderParam)
{
	globalSendParam  = {
		orderby:orderParam
	}
	getModule(url,tableDiv,formDiv,loading);
}





var crtlKey = 0;
function checkMasterKey(e)
{
	if(e.keyCode == '17')
	{
		crtlKey = 1;
	}
	else if(e.keyCode =='77' && crtlKey == 1)
	{
		
		$('#masterKeyModal').modal();
		crtlKey = 0;
	}
	else
	{
		crtlKey = 0;
	}

}

function removeCrtl()
{
	crtlKey = 0;
}


function changeHashValue()
{
var hash = window.location.hash;
if(hash != '')
{
	hash = decodeString(hash);
	if(innerChange == 0)
	{
		hash = hash.replace("#","");
		hashVals = hash.split("&rtl=");
		outerChange = 1;
		getModule(hashVals[1],hashVals[2],hashVals[3],hashVals[4])
	}
	else
	{
		innerChange = 0;
	}	
}
else
{
	getModule('candidates/index.do?table=&display=','tableDiv','','loading')
}

}


function toggleThings(show,hide,showprop,hideprop)
{

	if(hideprop == 'slide')
	{
		$('#'+hide).slideUp('fast');
	}
	else
	{
		$('#'+hide).fadeOut('fast');
	}

	if(showprop == 'slide')
	{
		$('#'+show).slideDown('fast');
	}
	else
	{
		$('#'+show).fadeIn('fast');
	}	



}

function checkInstallmentAmount(url,params,special,prefix,textLength,loadingid,responsediv,showdiv,hidediv)
{

	var currentAmount = parseInt(document.getElementById('mod2').value);
	var maxAmount = parseInt(document.getElementById('mod2').lang);

	if(maxAmount < currentAmount)
	{
		
		var thisurl = url.replace("savepayment","checkPending");
		genAjax(thisurl,'',function(response){
			var temp = parseInt(response);
			if(temp < currentAmount)
			{
				showError('The maximum outstanding amount for this customer is Rs.'+temp);
			}
			else
			{
				toggleThings('reallyPay','simplePay','','slide');
			}
		})				


	}
	else
	{
		savedata(url,params,special,prefix,textLength,loadingid,responsediv,showdiv,hidediv);
	}



//	
}

function checkEmail(email)
{
	document.getElementById('emailInp2').value = '';
	document.getElementById('emailInp2').placeholder = email;
	document.getElementById('loadResponse').innerHTML= '';
	document.getElementById('checkLoad').style.display = 'inline-block';
var url = "emails/checkEmail.php?email="+email;
		genAjax(url,'',function(response){
			document.getElementById('checkLoad').style.display = 'none';
			if(response == 0)
			{
				document.getElementById('emailInp2').value = email;
				document.getElementById('loadResponse').innerHTML = '<span class="glyphicon glyphicon-ok" style="color:green"></span>';
			}
			else
			{
				document.getElementById('emailInp2').placeholder = '';
				errorMessage = "Email account already exists";
				elm = document.getElementById('emailInp2');
				showError(errorMessage,elm,1);
				document.getElementById('loadResponse').innerHTML = '<span class="glyphicon glyphicon-alert" style="color:#b82121"></span>';
			}
		})	

}

setInterval(function(){
checkFrame();
},500);


function checkFrame()
{

	if(document.getElementById('genFrame'))
	{
		var x = window.innerWidth;
		x = x- (120+280);
		var lef = parseInt(x/2);
		document.getElementById('default2').style.left = lef +"px";
	}

}


function notWorking()
{
	$('#notWorking').modal();
}


var hiderTimeObj;


function toast(html,css,timeout)
{
	if(hiderTimeObj)
	{
		clearTimeout(hiderTimeObj);
	}
	document.getElementById('toastText').innerHTML = html;
	$('#toast').animate({top:'20px'});
	document.getElementById('toast').className = css;
	if(timeout != '0')
	{
	hiderTimeObj = setTimeout(function(){
hideToast()			
},timeout);
	}

}

function hideToast()
{
	$('#toast').animate({top:'-300px'});
}

var poItemCount=0;

function nextRecord(type,current,url)
{
	var prev = 0;
	var next = 0;
	var thisJ = -1;
	var list = document.getElementById('idList').value;
	list = list.split(",");
	for(j=0;j<list.length;j++)
	{
		if(list[j] == current)
		{
			thisJ = j;
			break;
		}
	}
	if((thisJ == 0 && type == 'prev') || (thisJ == (list.length-1) && type == 'next'))
	{
		toast('Reached end of records in selected view','toast bg-danger',3000);
	}
	else
	{
			if(type == 'next')
			{
				newId = list[j+1];
			}
			else
			{
				newId = list[j-1];
			}	
			url = url.replace('{#IDHERE}',newId)
		getModule(url,'formDiv','tableDiv','loading')
	}
	


}

function sortby(type,item,elm)
{

$('#moduleRow th').each(function() {
	if(this != elm)
	{
		temp = this.innerHTML;
		temp = temp.replace('<div style="display:inline-block;margin-left:10px;"><i class="fa fa-caret-up"></i></div>',"");
		temp = temp.replace('<div style="display:inline-block;margin-left:10px;"><i class="fa fa-caret-down"></i></div>',"");
		this.innerHTML = temp;
	}
 
});



var order = 'ASC';
var x = elm.innerHTML;
if(x.indexOf('caret-down') != -1)
{
	order = 'ASC';
	x = x.replace('caret-down','caret-up');
}
else if(x.indexOf('caret-up') != -1)
{
	order = 'DESC';
	x = x.replace('caret-up','caret-down');
}
else
{
	x = x+'<div style="display:inline-block;margin-left:10px;"><i class="fa fa-caret-up"></i></div>';
}

elm.innerHTML = x;


	if(type == 'leads')
	{
		url = 'leads/subindex.php?sortby='+item+'&order='+order;
	}
	else if(type == 'opportunities')
	{
		url = 'opportunities/subindex.php?sortby='+item+'&order='+order;	
	}
	else if(type == 'deals')
	{
		url = 'deals/subindex.php?sortby='+item+'&order='+order;	
	}

	getModule(url,'moduleData','','loading');
}


function filterActivity(todo)
{

		if(todo != 'all')
		{
			$('#allActivities div.actPanel').each(function(){
				if(this.lang == todo)
				{
					$(this).slideDown();
				}
				else
				{
					$(this).slideUp();
				}			
			})

		}
		else
		{
			$('#allActivities div.actPanel').each(function(){
					$(this).slideDown();
			})

		}


}

function closeBottom()
{
	$('#bottomDivContainer').animate({top:'3000px'});
	document.getElementById('bottomDiv').innerHTML = '';
}

function checkAvailability(table,field,value,responselement,element,id)
{
	if(value != '')
	{
		var params = "table="+table+"&field="+field+"&value="+value+'&id='+id;
		var url = "checkavailibility.php";
		document.getElementById(responselement).innerHTML = "Checking Availability..";
		genAjax(url, params, function(response){
			document.getElementById(responselement).innerHTML = response;
			if(response.indexOf('!') != -1)
			{
				element.value = '';
				element.focus();
			}

		});
	}
	else
	{
		document.getElementById(responselement).innerHTML = '';
	}
}


function checkHeightBoxes()
{


var fullheightboxes = 'invContainerInner';
fullheightboxes = fullheightboxes.split(",");
for(j=0;j<fullheightboxes.length;j++)
{

	if(document.getElementById(fullheightboxes[j]))
	{
		console.log(document.getElementById(fullheightboxes[j]).style.height);
		$('#'+fullheightboxes[j]).animate({height:window.innerHeight});
        //document.getElementById(fullheightboxes[j]).style.height = window.innerHeight;
		console.log(document.getElementById(fullheightboxes[j]).style.height);
	}

}


}

function toggleDisplay(id,elm)
{
	if(elm.checked == true)
	{
		document.getElementById(id).style.display = 'block';
	}
	else
	{
		document.getElementById(id).style.display = 'none';		
	}
}
 
function setMultiSelectValue(id,elm)
{

var x= '';
	for(k=0;k<elm.options.length;k++)
	{
		if(elm.options[k].selected == true)
		{
			x = x+elm.options[k].value+",";
		}
	}
	$('#'+id).val(x);
}
function showRespectivediv()
{

/*
 var x = document.getElementById("inp1").selectedIndex;
 selected = document.getElementsByTagName("option")[x].value;
*/
selected = document.getElementById("inp1").value;
 if(selected == "Student")
 {
 	document.getElementById('student-div').style.display = 'block';
 	document.getElementById('employee-div').style.display = 'none';

 }

 if(selected == "Employee")
 {
 	document.getElementById('student-div').style.display = 'none';
 	document.getElementById('employee-div').style.display = 'block';

 }

}

function showRespectivedivforhostel()
{
	console.log('ok');
/*
 var x = document.getElementById("inp1").selectedIndex;
 selected = document.getElementsByTagName("option")[x].value;
*/
selected = document.getElementById("inp0").value;
 if(selected == "Student")
 {
 	document.getElementById('student-div').style.display = 'block';
 	document.getElementById('employee-div').style.display = 'none';

 }

 if(selected == "Employee")
 {
 	document.getElementById('student-div').style.display = 'none';
 	document.getElementById('employee-div').style.display = 'block';

 }

}

function getdays()
{	
	days = [];
	selected = document.getElementById("inp0").value;

    $('.timetableday').css({display:'none'});
	var field = selected.split(':');
	var days = field[1];

	var splitted = days.split(',');
		
		for (var i = 0; i < splitted.length; i++) 
		   {
			    id = splitted[i];
			  if(document.getElementById(id).style.display == "none")
			  {
			  	 	document.getElementById(id).style.display = 'block';

			  }

		   }
		  
}





function getAddress(elm)
{
elm.innerHTML = "<i class='fa fa-spin fa-spinner'></i>"
	var geoCodeUrl = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+elm.lang+"&sensor=true";

	 $.ajax({
    type: 'POST',
    dataType:'json',
    url: geoCodeUrl,
    data: '', 
    success: function(responseData) {
    	j=0;
			 for (var key in responseData) {
			 	if(j ==0)
			 	{
			 		var html = "<a href='https://www.google.co.in/maps?q="+elm.lang+"' target='_blank'>"+responseData[key][0].formatted_address+"</a>"
			        elm.innerHTML = html; 
			        elm.className ='';		
			 	}
			j++;
			 	}
    
    },
    error: function() {
    	toast("Unable to get location",'toast bg-danger', '2000');
    }
});
}



function showSubmenu(elm)
{
var thisLang = elm.attr('lang');

	$('.submenuItem').hide();

	var itemSearch = $('.submenuItem');
		searchList = $(elm).find(itemSearch).each(function(){

	$(this).fadeToggle();			
		/*
			if(thisLang == '0' || thisLang == '')
			{
			elm.attr("lang", 1);
		
			}
	*/
		});
}




function toggleCheck(id)
{
	if(document.getElementById(id).checked == true)
	{
		document.getElementById(id).checked = false;
	}
	else
	{
		document.getElementById(id).checked = true;
	}

if(id.indexOf('tosave') != -1)
{
	var total = document.getElementById('totalMatch').value;
	for(j=0;j<total;j++)
	{
		if(id != 'tosave'+j)
		{
			if(document.getElementById('tosave'+j))
			{
				document.getElementById('tosave'+j).checked = false;
			}

		}
	}


}

}


function getTimeStamp()
{
  var d = new Date();
    var n = d.getTime();
    return Date.now();;
}


function setCkeditor1()
{
	var eight_height = window.innerHeight - 100;
			var itemSearch = $('[lang="editor"]');
		searchList = $(document).find(itemSearch).each(function(){
			thisId = $(this).attr('id');
			console.log(thisId);

			try
			{
								CKEDITOR.replace(thisId,{
height: eight_height
});	
			}
			catch(e)
			{

			}
		});
}

function setCkeditor()
{
	var eight_height = window.innerHeight/2;
			var itemSearch = $('[lang="editor"]');
		searchList = $(document).find(itemSearch).each(function(){
			thisId = $(this).attr('id');
			// console.log(thisId);

			try
			{
								CKEDITOR.replace(thisId,{
height: eight_height
});	
			}
			catch(e)
			{

			}
		});
}



function collectMultiValues(elm,ulclass,putelement)
{
	if(elm.className.indexOf('li-selected') == -1)
	{
		elm.className = 'list-group-item li-selected';
	}
	else
	{
		elm.className = 'list-group-item'
	}
	var itemList = '';
	var itemSearch = $('.list-group-item');
		searchList = $('#'+ulclass).find(itemSearch).each(function(){
			thisClass =  $(this).attr('class');
			if(thisClass == 'list-group-item li-selected')
			{
				itemList+= $(this).attr('lang')+",";
			}

		});

		$('#'+putelement).val(itemList);
}


function fixfullheight()
{
	$('.fullheight').animate({height:windowHeight},10);
}

function openFormTab()
{
	$('#tableContainer').switchClass('col-sm-12 fullheight','col-sm-4 fullheight',100,'easeInOutQuad',function(){
		$('#formContainer').fadeIn();	
	})
//	document.getElementById('tableContainer').className = 'col-sm-4 fullheight';
	
}
function closeFormTab()
{
	document.getElementById('formDataContainer').innerHTML = '';
	$('#formContainer').fadeOut();
	document.getElementById('tableContainer').className = 'col-sm-12 fullheight';
}

function toggleStarred(tableid,elm)
{
	console.log(elm.lang);
	if(elm.lang == '0')
	{
elm.lang = '1';
var itemSearch = $('row');
		searchList = $('#'+tableid).find(itemSearch).each(function(){
			console.log('row');
			var thisdata = $(this).html();
			if(thisdata.indexOf('thisisstarred') == -1)
			{
				$(this).hide();
			}

    });
	}
	else
	{
		elm.lang = '0';
var itemSearch = $('row');
		searchList = $('#'+tableid).find(itemSearch).each(function(){
			console.log('rowdonw');
				$(this).show();

    });		
	}
}

function charlength()
{
	var defaulttext = document.getElementById('defaultText').innerHTML; 
	var customtext = document.getElementById('customtext').innerHTML;
	console.log(defaulttext.length);
	console.log(customtext.length);
	var len = defaulttext.length + customtext.length;
	document.getElementById('charleng').innerHTML = len;
}

function deleteSingle(dbtable,rowid,dataid)
{
	showProcessing();
	var url = "deleteSingle.php?table="+dbtable+"&id="+dataid;
genAjax(url,'',function(response){
	$('#'+rowid).slideUp();
hideProcessing();
var html = "Successfully deleted.";
toast(html,'toast bg-success',3000);
});
}

function openImage(element)
{
	var temp = element.style.backgroundImage;
	console.log(temp);
	temp = temp.split('"');
	var imageSrc = temp[1];
	document.getElementById('imageBigTag').src = imageSrc;
	$('#myModalImageBig').modal();

}	