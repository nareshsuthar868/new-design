function setCamera()
{

  (function() {

  var streaming = false,
    video = document.querySelector('#video'),
    canvas = document.querySelector('#canvas'),
    buttoncontent = document.querySelector('#buttoncontent'),
    photo = document.querySelector('#photo'),
    startbutton = document.querySelector('#startbutton'),
    width = 320,
    height = 0;

  navigator.getMedia = (navigator.getUserMedia ||
    navigator.webkitGetUserMedia ||
    navigator.mozGetUserMedia ||
    navigator.msGetUserMedia);

  navigator.getMedia({
      video: true,
      audio: false
    },
    function(stream) {
      if (navigator.mozGetUserMedia) {
        video.mozSrcObject = stream;
      } else {
        var vendorURL = window.URL || window.webkitURL;
        try{
        video.src = vendorURL.createObjectURL(stream);
        }
        catch(e)
        {
          video.srcObject=stream;
        }
        
      }
      video.play();
    },
    function(err) {
      console.log("An error occured! " + err);
    }
  );

  video.addEventListener('canplay', function(ev) {
    if (!streaming) {
      height = video.videoHeight / (video.videoWidth / width);
      video.setAttribute('width', width);
      video.setAttribute('height', height);
      canvas.setAttribute('width', width);
      canvas.setAttribute('height', height);
      streaming = true;
    }
  }, false);

  function takepicture() {
    $('#layoverotp').show();
    video.style.display = "none";
    canvas.style.display = "block";
    startbutton.innerText= "RETAKE";
    canvas.width = width;
    canvas.height = height;
    canvas.getContext('2d').drawImage(video, 0, 0, width, height);
    var data = canvas.toDataURL('image/png');
    photo.setAttribute('src', data);
    uploadToServer(data);
  }

  startbutton.addEventListener('click', function(ev) {
    if(startbutton.innerText==="CAPTURE PIC")
    {
      takepicture();
    }
    else
    {
      video.style.display = "block";
      canvas.style.display = "none";
      startbutton.innerText= "CAPTURE PIC";
    }
    ev.preventDefault();
  }, false);

})();


}
  

var request = new XMLHttpRequest();

function uploadToServer(dataUrl)
{

request.open('POST', 'visitor/saveimage.php');
$('#picsurf').fadeIn();
document.getElementById('startbutton').innerHTML = 'UPLOADING AND VALIDATING PIC..';
request.onreadystatechange = function () {
  if (this.readyState === 4) {
    var resp= this.responseText;
    resp = resp.split("***");
    document.getElementById('vis0').value = resp[0];
    $('#layoverotp').hide();
    if(resp[1].indexOf('MATCHFOUND') != -1)
    {
      var json = JSON.parse(resp[2]);
      document.getElementById('vis1').value = json[1];
      document.getElementById('vis2').value = json[0];
      document.getElementById('vis3').value = json[2];
      document.getElementById('vis4').value = json[3];
      document.getElementById('vis7').value = json[4];
      document.getElementById('vis6').value = json[5];
      document.getElementById('vis9').value = json[6];
      toast("<i class='fas fa-check'></i>&nbsp;Match found.","toast bg-success","3000");
          $('#layover1').hide();
    $('#layover2').hide();
    $('#otpBox').fadeOut();
      
    }
    else
    {

    }
$('#picsurf').fadeOut();
document.getElementById('startbutton').innerHTML = 'RETAKE';
  }
};

var body = {
  'image': dataUrl,
  'gallery_name': 'TestSubLive',
  'subject_id': 'TestGalLive'
};

request.send(JSON.stringify(body));
}