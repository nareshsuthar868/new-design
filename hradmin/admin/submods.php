<div class="loading" id="loading"><div style="width:100%;display:block;text-align:left">
 <div style="width:100%;height:7px;background:#374c9e;text-align:left;position:relative">
    <div style="width:30%;height:7px;background:#f7deda;display:inline-block;position:absolute;top:0px;left:0px;" id="loadbar"></div>
  </div>
</div>


</div>




<div class="toast" id="toast">
<div style="float:right">
  <i class="fa fa-remove" onclick="hideToast()"></i>
</div>
  <span id="toastText"></span>
</div>

<div style="position:fixed;top:3000px;left:0px;z-index:3000;width:100%;height:800px;background:#fff;" id="bottomDivContainer">
<div style="position:absolute;top:19px;right:10px;z-index:200;" onclick="closeBottom();">
<button class="btn btn-danger btn-sm">
	<i class="fa fa-remove"></i>&nbsp;&nbsp;CLOSE
</button>
</div>
<div id="bottomDiv"></div>
</div>  

<div style="position:absolute;bottom:20px;right:20px;z-index:200000;display:none" onclick="hideProcessing();" id="stopLoad">
  <button class="btn btn-sm btn-primary primary-border">
    Request take longer than expected. Click to stop.
  </button>
</div>
