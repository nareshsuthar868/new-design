<?php
include("../include/config.php");

?>
<div class="header">
			<div class="title">
			<div style="float:right;padding:0px 20px;width:60%">
<div style="position:relative;width:100%;">
	<table style="width:100%;" cellpadding="0px">
		<tr>
			<td style="width:60%;text-align:right;position:relative;padding-left:0px;">
		<input type="search" style="width:60%;" onclick="$(this).animate({width:'100%'})" placeholder="Start Typing.." onkeyup="searchTable('dataTable',this.value)" class="input" name="">		
		

		<i class="fas fa-search" style="position:absolute;top: 12px;
    right: 10px;color:#ccc;font-size:11px;"></i>
			</td>
			<td style="width:40%;text-align:right">
			&nbsp;&nbsp;

<div class="dropdown" style="display:inline-block;display: none;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    ADD NEW
  </button>

<div class="dropdown-menu">
  <ul>
  <li style="cursor:pointer"  id="newstudentbutton" onclick="getModule('colleges/new.do','formDataContainer','','loading')">
<i class="fas fa-plus"></i>&nbsp;&nbsp;Add Single</li>
  <li style="cursor:pointer"   id="" onclick="getModal('import/index.php?type=students','formModal','tableModal','loading')">
<i class="far fa-file-excel"></i>&nbsp;&nbsp;Import Via Excel</li>
  </ul>
  </div>
  </div>




			<div class="dropdown" style="display:inline-block;display: none;">
  <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown">
    Options
  </button>

  <div class="dropdown-menu">
  <ul>
  <li onclick="crossCheckDelete('colleges','tenantlist','dataTableInner')">
<i class="fas fa-times"></i>&nbsp;&nbsp;Delete Selected</li>


  <li onclick="toast('Export not allowed from demo account.','toast bg-danger','4000')">
<i class="fas fa-file-excel"></i>&nbsp;&nbsp;Export Selected</li>

  </ul>
  </div>
</div>




			

			</td>
		</tr>
	</table>
		
	</div>

				
			</div>
			<strong>
				<i class="fas fa-th-list"></i>&nbsp;&nbsp;&nbsp;List of Holidays
				</strong>
			</div>
</div>
<div class="whiteCover">
<div class="row">
	<div class="col-sm-12 fullheight" id="tableContainer" style="overflow-y:auto;padding:0px;">
	
	
	<table class="table dataTable" id="dataTable">
	<thead>
<tr>
<th class="nofilter" style="width:20px;">#</th>
<!-- <th class="nofilter" style="width:20px;">
	<input type="checkbox" name="" onclick="checkAll(this,'dataTable')">
	</th> -->
	<th>Event</th>
	<th class="hideThis">Day</th>
	<th class="hideThis">Date</th>
	<th class="hideThis">No. of Days</th>

</tr>
</thead>

<tbody id="dataTableInner" >

<tr>
	<td>1.</td>
	<td>New Year</td>
	<td>1st January 2020</td>
	<td><span class="badge badge-info" style="padding: 7px;">WEDNESDAY</span></td>
	<td>1 Day</td>
</tr>
<tr>
	<td>2.</td>
	<td>Republic Day</td>
	<td>26th January 2020</td>
	<td><span class="badge badge-danger" style="padding: 7px;">SUNDAY</span></td>
	<td>1 Day</td>
</tr>
<tr>
	<td>3.</td>
	<td>Holi</td>
	<td>9th March 2020</td>
	<td><span class="badge badge-info" style="padding: 7px;">MONDAY</span></td>
	<td>1 Day</td>
</tr>
<tr>
	<td>4.</td>
	<td>Good Friday</td>
	<td>10th April 2020</td>
	<td><span class="badge badge-info" style="padding: 7px;">FRIDAY</span></td>
	<td>1 Day</td>
</tr>

<tr>
	<td>5.</td>
	<td>Eid Ul Fitar</td>
	<td>23rd May 2020</td>
	<td><span class="badge badge-info" style="padding: 7px;">SATURDAY</span></td>
	<td>1 Day</td>
</tr>

<tr>
	<td>6.</td>
	<td>Rakshabandhan</td>
	<td>3rd August 2020</td>
	<td><span class="badge badge-info" style="padding: 7px;">MONDAY</span></td>
	<td>1 Day</td>
</tr>

<tr>
	<td>7.</td>
	<td>Independence Day</td>
	<td>15th August 2020</td>
	<td><span class="badge badge-info" style="padding: 7px;">SATURDAY</span></td>
	<td>1 Day</td>
</tr>
<tr>
	<td>8.</td>
	<td>Gandh Jayanti</td>
	<td>2nd October 2020</td>
	<td><span class="badge badge-info" style="padding: 7px;">FRIDAY</span></td>
	<td>1 Day</td>
</tr>
<tr>
	<td>9.</td>
	<td>Dusshera</td>
	<td>25th October 2020</td>
	<td><span class="badge badge-danger" style="padding: 7px;">SUNDAY</span></td>
	<td>1 Day</td>
</tr>
<tr>
	<td>10.</td>
	<td>Diwali</td>
	<td>14th & 15th November 2020</td>
	<td><span class="badge badge-info" style="padding: 7px;">SATURDAY</span> <span class="badge badge-danger" style="padding: 7px;"> SUNDAY</span></td>
	<td>2 Days</td>
</tr>	
<tr>
	<td>11.</td>
	<td>Guru Nanak Jayanti</td>
	<td>30th November 2020</td>
	<td><span class="badge badge-info" style="padding: 7px;">MONDAY</span></td>
	<td>1 Day</td>
</tr>	
<tr>
	<td>12.</td>
	<td>Christmas</td>
	<td>25th November 2020</td>
	<td><span class="badge badge-info" style="padding: 7px;">FRIDAY</span></td>
	<td>1 Day</td>
</tr>	

	</tbody>
</table>	
<div style="height:100px;"></div>
</div>
	
	<div class="col-sm-8" id="formContainer" style="border-left:1px #eee solid;display:none;position:relative;padding:0px;">
	<div style="position:absolute;top:10px;right:10px;cursor:pointer;">
	<button class="btn btn-sm btn-light" style="color:#ea456c;border:1px #ea456c solid;background-color:#fff !important;margin-right:10px;" onclick="closeFormTab()">
			<i class="far fa-times-circle" ></i> Close

	</button>

	</div>
	<div id="formDataContainer">
		
	</div>
	</div>
</div>
</div>


