<?php
include('../include/config.php');
$id = $_GET['id'];

$getdata = mysqli_query($con,"SELECT * FROM `answersheet` WHERE `id` = '$id'");
$fetchRow = mysqli_fetch_array($getdata);
$quesList = $fetchRow['questionids'];
$questions = explode('::', $quesList);
$ansList = $fetchRow['answerids'];
$answers = explode('::', $ansList);
$markList = $fetchRow['marks'];
$marks = explode('::', $markList);

//$mappedarray = array_combine($questions, $answers);
$allquestions = array();
$allquestionsdata  = mysqli_query($con,"SELECT * FROM `questions` ");
while($quesRow = mysqli_fetch_array($allquestionsdata))
{
    $allquestions[$quesRow['id']] = $quesRow['question']; 
}

?>
<div class="title">
	Answer Sheet
</div>
<div class="row">
	<div class="col-sm-12 fullheight" id="tableContainer" style="overflow-y:auto;padding:10px;">
	
<table class="table table-bordered">
	<tr>
		<th>Q No</th>
		<th>Question</th>
		<th>Answer</th>
		<th>Marks</th>
	</tr>

	<?php 
	foreach($questions as $key => $val)
	{	
		if($val != '')
		{
	?>
	<tr>
		<td>Q<?php echo $val;?></td>
		<td><?php echo $allquestions[$val];?></td>
		<td><?php echo $answers[$key];?></td>
		<td><?php echo $marks[$key];?></td>
	</tr>

    <?php
    	}
     } 
     ?>
	
	<tr>
		<th colspan="3" style="text-align:center;">TOTAL QUESTION ATTEMPTED</th>
		<td>
			<?php  $totalattempted = count($questions); 
			echo $totalattempted = $totalattempted -1; ?>
		</td>
	</tr>
	<tr>
		<th colspan="3" style="text-align:center;" >TOTAL MARKS OBTAINED</th>
		<td>
			<?php echo array_sum($marks);?>
		</td>
	</tr>
	
</table>
<div style="height:300px;"></div>
</div>

</div>