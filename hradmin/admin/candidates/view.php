<?php
include('../include/config.php');
$id = $_GET['id'];

$getdata = mysqli_query($con,"SELECT * FROM `answersheet` WHERE `id` = '$id'");
$fetchRow = mysqli_fetch_array($getdata);
$quesList = $fetchRow['questionids'];
$questions = explode('::', $quesList);
$ansList = $fetchRow['answerids'];
$answers = explode('::', $ansList);
$markList = $fetchRow['marks'];
$marks = explode('::', $markList);

//$mappedarray = array_combine($questions, $answers);

?>
<div class="title">
	Answer Sheet
</div>
<div class="row">
	<div class="col-sm-12 fullheight" id="tableContainer" style="overflow-y:auto;padding:10px;">
	
<table class="table table-bordered">
	<tr>
		<th>Question</th>
		<th>Answer</th>
		<th>Marks</th>
	</tr>

	<?php 
	foreach($questions as $key => $val)
	{	
		//if($val != '')
		{
	?>
	<tr>
		<td>Q<?php echo $val;?></td>
		<td><?php echo $answers[$key];?></td>
		<td><?php echo $marks[$key];?></td>
	</tr>

    <?php
    	}
     } 
     ?>
	
	<tr>
		<th colspan="2">TOTAL QUESTION ATTEMPTED</th>
		<td>
			<?php echo count($questions);?>
		</td>
	</tr>
	<tr>
		<th colspan="2" >TOTAL MARKS OBTAINED</th>
		<td>
			<?php echo array_sum($marks);?>
		</td>
	</tr>
</table>
</div>
</div>