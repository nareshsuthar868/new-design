<?php
include("../include/config.php");
$data = getData('answersheet','*','id','DESC');
?>
<div class="header">
			<div class="title">
			<div style="float:right;padding:0px 20px;width:60%">
<div style="position:relative;width:100%;">
	<table style="width:100%;" cellpadding="0px">
		<tr>
			<td style="width:60%;text-align:right;position:relative;padding-left:0px;">
		<input type="search" style="width:60%;" onclick="$(this).animate({width:'100%'})" placeholder="Start Typing.." onkeyup="searchTable('dataTable',this.value)" class="input" name="">		
		

		<i class="fas fa-search" style="position:absolute;top: 12px;
    right: 10px;color:#ccc;font-size:11px;"></i>
			</td>
			<td style="width:40%;text-align:right">
			&nbsp;&nbsp;

<div class="dropdown" style="display:inline-block;display: none;">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
    ADD NEW
  </button>

<div class="dropdown-menu">
  <ul>
  <li style="cursor:pointer"  id="newstudentbutton" onclick="getModule('colleges/new.do','formDataContainer','','loading')">
<i class="fas fa-plus"></i>&nbsp;&nbsp;Add Single</li>
  <li style="cursor:pointer"   id="" onclick="getModal('import/index.php?type=students','formModal','tableModal','loading')">
<i class="far fa-file-excel"></i>&nbsp;&nbsp;Import Via Excel</li>
  </ul>
  </div>
  </div>




			<div class="dropdown" style="display:inline-block;display: none;">
  <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown">
    Options
  </button>

  <div class="dropdown-menu">
  <ul>
  <li onclick="crossCheckDelete('colleges','tenantlist','dataTableInner')">
<i class="fas fa-times"></i>&nbsp;&nbsp;Delete Selected</li>


  <li onclick="toast('Export not allowed from demo account.','toast bg-danger','4000')">
<i class="fas fa-file-excel"></i>&nbsp;&nbsp;Export Selected</li>

  </ul>
  </div>
</div>




			

			</td>
		</tr>
	</table>
		
	</div>

				
			</div>
			<strong>
				<i class="fab fa-odnoklassniki"></i>&nbsp;&nbsp;&nbsp;Candidates
				</strong>
			</div>
</div>
<div class="whiteCover">
<div class="row">
	<div class="col-sm-12 fullheight" id="tableContainer" style="overflow-y:auto;padding:0px;">
	
	
	<table class="table dataTable" id="dataTable">
	<thead>
<tr>
<th class="nofilter" style="width:20px;">#</th>
<!-- <th class="nofilter" style="width:20px;">
	<input type="checkbox" name="" onclick="checkAll(this,'dataTable')">
	</th> -->
	<th>Candidate Name</th>
	<th class="hideThis">Applied For </th>
	<th class="hideThis">Contact</th>
	<th class="hideThis">Exam Code</th>
	<th class="hideThis">Questions Attempted</th>
	<th class="hideThis">Total Marks</th>
	<th class="hideThis">Marks Obtained</th>
	<th class="hideThis">Answer Sheet</th>
	<th class="hideThis">Status</th>

</tr>
</thead>

<tbody id="dataTableInner" >
<?php
$i = 1;
foreach($data as $row)
{
	
	$questionList = $row['questionids'];
	$count = explode('::', $questionList);
	$totalquestion = count($count)-1;
	$marksList = $row['marks'];
	$marks = explode('::', $marksList);
	$totalmarks = array_sum($marks);

	?>
<tr id="tenantlist<?php echo $row['id'];?>">
<td>
	<?php echo $i;?>
</td>
<!-- <td>
		<input class="checkInput" value="" type="checkbox" name="">
	</td> -->
		<td class="text-primary" onclick="getModule('candidates/show.do?id=<?php echo $row['id'];?>','bottomDiv','','loading')"><?php echo $row['name'];?></td>
		<td class="hideThis"><?php echo $row['profile'];?></td>
		<td class="hideThis"><?php echo $row['contact'];?></td>
		<td class="hideThis"><?php echo $row['code'];?></td>
		<td class="hideThis"><?php echo $totalquestion;?></span></td>
		<td class="hideThis">40</td>
		<td class="hideThis"><?php echo $totalmarks;?></td>
		
		<!--<td class="hideThis"><button class="btn btn-primary" onclick="getModal('candidates/view.php?id=<?php echo $row['id'];?>','formModalBig','tableModalBig','loading')">VIEW SHEET</button></td>-->
	<td class="hideThis"><button class="btn btn-primary" onclick="getModule('candidates/show.do?id=<?php echo $row['id'];?>','bottomDiv','','loading')">VIEW SHEET</button></td>
		
		<td>
			<?php
			if($row['status'] == '0')
			{
				?>
				<span style="padding: 10px;font-size: 11px;cursor: pointer;" class="badge badge-warning" onclick="getModal('candidates/status.php?id=<?php echo $row['id'];?>','formModal','tableModal','loading')">PENDING</span>
				<?php
			}
			else if($row['status'] == '1')
			{
				?>
				<span style="padding: 10px;font-size: 11px;cursor: pointer;" class="badge badge-success" onclick="getModal('candidates/status.php?id=<?php echo $row['id'];?>','formModal','tableModal','loading')">SELECTED</span>	
				<?php
			}
			else if($row['status'] == '2')
			{
				?>
				<span style="padding: 10px;font-size: 11px;cursor: pointer;" class="badge badge-danger" onclick="getModal('candidates/status.php?id=<?php echo $row['id'];?>','formModal','tableModal','loading')">REJECTED</span>
				<?php
			}
			?>
						
		</td>
	</tr>

	<?php
	$i++;
}
?>
	
	</tbody>
</table>	
<div style="height:100px;"></div>
</div>
	
	<div class="col-sm-8" id="formContainer" style="border-left:1px #eee solid;display:none;position:relative;padding:0px;">
	<div style="position:absolute;top:10px;right:10px;cursor:pointer;">
	<button class="btn btn-sm btn-light" style="color:#ea456c;border:1px #ea456c solid;background-color:#fff !important;margin-right:10px;" onclick="closeFormTab()">
			<i class="far fa-times-circle" ></i> Close

	</button>

	</div>
	<div id="formDataContainer">
		
	</div>
	</div>
</div>
</div>


