<?php
include("include/config.php");
$type = $_GET['type'];
$id = $_GET['id'];
$getData = mysqli_query($con,"SELECT * FROM `documents` WHERE `type` = '$type' AND `dataid` = '$id'") or die(mysqli_error($con));

$count = mysqli_num_rows($getData);
?>
<div id="demo" class="carousel slide" data-ride="carousel">

  <!-- Indicators -->
  <ul class="carousel-indicators">
  <?php
  for($k=0;$k<$count;$k++)
  {
  	if($k == 0)
  	{
  		?>
    <li data-target="#demo" data-slide-to="0" class="active"></li>
  		<?php
  	}
  	else
  	{
  		?>
    <li data-target="#demo" data-slide-to="<?php echo $k;?>"></li>

  		<?php
  	}
  	?>


  	<?php
  }
  ?>
  </ul>

  <!-- The slideshow -->
  <div class="carousel-inner">

  <?php
  $p=0;
  while($rowpics = mysqli_fetch_array($getData))
  {
  	if($p == 0)
  	{
  		?>
    <div class="carousel-item active">
  		<?php
  	}
  	else
  	{
  		?>
    <div class="carousel-item">
  		<?php
  	}
?>

<div style="width:100%;">
		<div style="background:transparent url('<?php echo $rowpics['filepath'];?>') scroll no-repeat center center;background-size:cover;height:500px;width:100%;"></div>
		<div style="padding:10px;border-top:1px #eee solid;">
		<br/>
		<span style="font-size:14px;font-weight:bold"><?php echo $rowpics['name'];?></span>
		<br/>
		<br/>
		<span style="font-size:12px;"><?php echo $rowpics['notes'];?></span>
		</div>
	</div>

    </div>

<?php
$p++;
  }
  ?>
    
  </div>

  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>

</div>