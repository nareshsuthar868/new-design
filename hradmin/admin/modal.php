<div id="myModalImageBig" class="modal fade" role="dialog"   style="z-index:200000000">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <img src="" id="imageBigTag" style="width:100%"/>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
<i class="fa fa-remove"></i>
        Close</button>
      </div>
    </div>

  </div>
</div>


<div id="myModal" class="modal fade" role="dialog"   style="z-index:200000000">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <div id="tableModal">
      </div>
      <div id="formModal"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
<i class="fa fa-remove"></i>
        Close</button>
      </div>
    </div>

  </div>
</div>

<div id="myModalBig" class="modal fade" role="dialog"  style="z-index:20000000">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content" style="border-radius:0px !important">
    <div id="tableModalBig">
      </div>
      <div id="formModalBig"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">
<i class="fa fa-remove"></i>
        Close</button>

      </div>
    </div>

  </div>
</div>


<div id="massEditModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div id="modalData"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<div id="notWorking" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
    <center style="padding:20px;">
    No Data Available      
    </center>

      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


