<?php
include("../../../include/config.php");
$getData = mysqli_query($con,"SELECT city.id,city.name,state.name,city.createdate FROM city,state WHERE city.state = state.id ORDER BY city.name ASC") or die(mysqli_error($con));
?>
<div class="moduleHead">
<div style="float:right">
	<button class="btn btn-sm btn-primary"  onclick="getModule('setup/masters/cities/new.php','formDiv','tableDiv','loading')">+1 ADD NEW</button>
</div>
<div class="moduleHeading">
Cities
</div>
</div>
<div class="tabelContainer divShadow" style="height:auto">
<table class="table table-striped table-hover fetch" cellpadding="0" cellspacing="0">
<tr>
<th>#</th>
<th>Name</th>
<th>State</th>
<th>Createdate</th>
</tr>

<?php
$i=1;
while($row = mysqli_fetch_array($getData))
{
	?>
<tr id="tableRow">
<td><?php echo $i;?></td>
<td class="text-primary" onclick="getModule('setup/masters/cities/edit.php?id=<?php echo $row[0];?>','formDiv','tableDiv','loading')"><?php echo $row[1];?></td>
<td>
	<?php echo $row[2];?>
</td>
<td>
<?php echo date("d/m/y h:i A",strtotime($row[3]));?>
</td>


</tr>

	<?php
	$i++;
}


?>



</table>
</div>

<br/><br/><br/><br/><br/><br/><br/><br/><br/>