<?php
include("../../../include/config.php");
$saveurl = str_ireplace("new.php", "save.php", $urltocall);
$callbackurl = str_ireplace("new.php", "index.php", $urltocall);

?>

<div class="moduleHead">
		<div style="float: right">
		<button class="btn btn-default btn-sm" onclick="toogleFormTable();" type="button">
			<i class="fa fa-arrow-left"></i>&nbsp;&nbsp;
			BACK TO LIST</button>
			</div>

	<div class="moduleHeading">
	Add New City

	</div>
</div>
<div class="shadow">
<div class="row" style="background:#f8f7f7">
	<div class="col-sm-2 formLeft req">
		Name
	</div>
	<div class="col-sm-10 formRight">
		<input type="text" name="req" title="City Name" id="inp0" class="inputBox">
	</div>	

</div>

<div class="row" style="background:#f8f7f7">
	<div class="col-sm-2 formLeft req">
		State
	</div>
	<div class="col-sm-10 formRight">
	<select class="inputBox" id="inp1">
	<?php
$getData = mysqli_query($con,"SELECT * FROM `state` ORDER BY `name` ASC") or die(mysqli_error($con));
while($row = mysqli_fetch_array($getData))
{
	?>
<option value="<?php echo $row['id'];?>"><?php echo $row['name'];?></option>
	<?php
}
	?>
</select>
	</div>	

</div>





<div class="row">
	<div class="col-sm-2 formLeft">
		Notes
	</div>
	<div class="col-sm-10 formRight">
	<textarea class="inputBox" id="inp2" style="width:100%;height:150px;"></textarea>
	</div>	


</div>




<div class="row">
	<div class="col-sm-2 formLeft">
		
	</div>
	<div class="col-sm-10 formRight">
		<button lang="changeClass" id="moduleSaveButtontop" class="btn btn-primary btn-sm" onclick="savedata('<?php echo $saveurl;?>','','','inp',3,'moduleSaveButtontop:!SCROLL!Saving..','url:<?php echo $callbackurl;?>','tableDiv','formDiv');" type="button">
			<i class="fa fa-check"></i>&nbsp;&nbsp;SAVE DATA</button>
			<br/><br/><br/>
	</div>	


</div>





</div>



<br />
<br />
<br />
<br />
<br />
