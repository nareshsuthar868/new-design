<?php
include("../../include/config.php");
$table = $_GET['table'];
$display = $_GET['display'];
if($table == 'photogallery')
{
  $getdata = mysqli_query($con,"SELECT * FROM `photogallery` ORDER BY `sortorder` ASC") or die(mysqli_error($con));
}

else
{
 $getdata = mysqli_query($con,"SELECT * FROM `$table` ORDER BY `name` ASC") or die(mysqli_error($con));
}

?>

<div class="header">
      <div class="title">
      <div style="float:right;padding:0px 20px;width:60%">
<div style="position:relative;width:100%">
  <table style="width:100%;" cellpadding="0px">
    <tr>
      <td style="width:60%;text-align:right;position:relative;padding-left:0px;">
    <input type="search" style="width:40%;" onclick="$(this).animate({width:'100%'})" placeholder="Start Typing.." onkeyup="searchTable('dataTable',this.value)" class="input" name="">   
    

    <i class="fas fa-search" style="position:absolute;top: 12px;
    right: 10px;color:#ccc;font-size:11px;"></i>
      </td>
      <td style="width:40%;text-align:right">
      <div class="dropdown" style="display:inline-block">
  <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown">
    Options
  </button>

  <div class="dropdown-menu">
  <ul>
  <li onclick="crossCheckDelete('<?php echo $table;?>','<?php echo $table;?>','dataTableInner')">
<i class="fas fa-times"></i>&nbsp;&nbsp;Delete</li>
  <li onclick="toast('Export not allowed from demo account.','toast bg-danger','4000')">
<i class="fas fa-file-excel"></i>&nbsp;&nbsp;Export Selected</li>

  </ul>
  </div>
</div>
&nbsp;&nbsp;
<button type="button" id="newstudentbutton" class="btn btn-primary" onclick="getModule('setup/general/new.do?table=<?php echo $table;?>&display=<?php echo $display;?>','formDataContainer','','loading')">
    +1 Add New
  </button>

      </td>
    </tr>
  </table>
    
  </div>

        
      </div>
        <i class="fas fa-th-large"></i>&nbsp;&nbsp;<?php echo $_GET['display'];?>
      </div>
</div>


<div class="whiteCover">
<div class="row">
  <div class="col-sm-12 fullheight" id="tableContainer" style="overflow-y:auto;padding:0px;">
  
  
  <table class="table dataTable" id="dataTable">
  <thead>
<tr>
<th class="nofilter" style="width:20px;">#</th>
<th class="nofilter" style="width:20px;">
  <input type="checkbox" name="" onclick="checkAll(this,'dataTable')">
  </th>
  <th>Name</th>
<th class="hideThis">Code</th>

  <th class="hideThis">Notes</th>

<th class="hideThis">Createdate</th>  
</tr>
</thead>
<tbody id="dataTableInner">
<?php
$k=0;
while($row = mysqli_fetch_array($getdata))
{

  ?>
<tr id="<?php echo $table;?><?php echo $row['id'];?>">
<td>
  <?php echo $k+1;?>
</td>
<td>
    <input class="checkInput" value="<?php echo $row['id'];?>" type="checkbox" name="">
  </td>

    <td class="text-primary" onclick="getModule('setup/general/edit.do?id=<?php echo $row['id'];?>&k=<?php echo $k+1;?>&table=<?php echo $table;?>&display=<?php echo $display;?>','formDataContainer','','loading')"><?php echo $row['name'];?></td>

<?php
if($table =='photogallery')
{
  ?>
      <td class="hideThis">
      <button id="click<?php echo $row['id'];?>" onclick="getModal('documents/index.do?type=photogallery&id=<?php echo $row['id'];?>&clickid=click<?php echo $row['id'];?>','tableModalBig','formModalBig','loading')" class="btn btn-sm btn-success">PHOTOS</button>&nbsp;&nbsp;<button class="btn btn-sm btn-warning"  onclick="getModal('slideshow.do?type=photogallery&id=<?php echo $row['id'];?>','tableModalBig','formModalBig','loading')" >SLIDESHOW</button>
    </td>

  <?php
}
else{
  ?>
  <td class="hideThis"><?php echo $row['code'];?></td>
  <?php
}
?>


    <td class="hideThis"><?php echo $row['notes'];?></td>
    <td class="hideThis"><?php echo date("d-m-y h:i A",strtotime($row['createdate']));?></td>

    
  </tr>

  <?php
  $k++;
}
?>
  
  </tbody>
</table>  
<div style="height:100px;"></div>
</div>
  
  <div class="col-sm-8" id="formContainer" style="border-left:1px #eee solid;display:none;position:relative;padding:0px;">
  <div style="position:absolute;top:10px;right:10px;cursor:pointer;">
  <button class="btn btn-sm btn-light" style="color:#ea456c;border:1px #ea456c solid;background-color:#fff !important" onclick="closeFormTab()">
      <i class="far fa-times-circle" style=""  ></i> Close

  </button>

  </div>
  <div id="formDataContainer">
    
  </div>
  </div>
</div>
</div>








