<?php
include("include/config.php");
?>
<!DOCTYPE html>
<html>
<head>
<title>.::CityFurnish::.</title>
<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,700|Open+Sans:400,700" rel="stylesheet">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

<script type="text/javascript" src="ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="scripts/misc.js?t=<?php echo time();?>"></script>
<script type="text/javascript" src="scripts/savedata.js?t=<?php echo time();?>"></script>
<script type="text/javascript" src="scripts/getModule.js?t=<?php echo time();?>"></script>
<script type="text/javascript" src="scripts/getModal.js?t=<?php echo time();?>"></script>
<script type="text/javascript" src="scripts/rix.js?t=<?php echo time();?>"></script>
<script type="text/javascript" src="scripts/deleteRow.js?t=<?php echo time();?>"></script>
<script type="text/javascript" src="scripts/encodeComponents.js?t=<?php echo time();?>"></script>
<script type="text/javascript" src="scripts/table-filter-bundle.js?t=<?php echo time();?>"></script>
<script type="text/javascript" src="scripts/camera.js?t=<?php echo time();?>"></script>
<link rel="stylesheet" type="text/css" href="css/style.css?time=<?php echo time();?>">
</head>
 <body  onhashchange="changeHashValue()" onload="changeHashValue();" onkeyup="if(event.keyCode == '27'){ doEscapeItems();}">
<?php
include("modal.php");
include("submods.php");
?>

<div class="row">
	<div class="col-sm-2 desktopMenu fullheight" id="desktopMenu" style="position:relative">

<?php include("menu.php");?>		

	</div>
	<div class="col-sm-10 desktopView fullheight" id="desktopView" style="border-left:1px #eee solid">
		<div id="tableDiv">

		</div>
	</div>
</div>
<?php include("footerjs.php");?>
</body>
</html>