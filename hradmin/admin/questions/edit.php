<?php
include('../include/config.php');
$id = $_GET['id'];
$data = mysqli_query($con,"SELECT * FROM `questions` WHERE `id` = '$id'") or die(mysqli_error($con));
$row = mysqli_fetch_array($data);
?>
<div class="subheading" style="border-bottom:1px #eee solid">
Edit Question
</div>
<div class="row formBox fullheight"  id="formBox" style="overflow-y:scroll">

<div class="row">

	<div class="col-sm-12">
		S.No Number
		<input type="number" name="req" id="serial" value="<?php echo $row['sno'];?>" class="input">
	</div>

    <div class="col-sm-12">
		<div class="innersubheading">
		Type in your Question
		</div>
      <textarea lang="editor" id="question" ><?php echo $row['question'];?></textarea>
    
    </div>
    <div class="col-sm-6">
		Option A
		<input type="text" name="req" id="option_a" value="<?php echo $row['option_a'];?>" class="input">
	</div>

	<div class="col-sm-6">
		Option B
		<input type="text" name="req" id="option_b" value="<?php echo $row['option_b'];?>" class="input">
	</div>

	<div class="col-sm-6">
		Option C
		<input type="text" name="req" id="option_c" value="<?php echo $row['option_c'];?>" class="input">
	</div>

	<div class="col-sm-6">
		Option D
		<input type="text" name="req" id="option_d" value="<?php echo $row['option_d'];?>" class="input">
	</div>
	<div class="col-sm-6">
		Correct Answer
		<select class="input" id="answer">
			<option value="">Select Option</option>
			<option <?php if($row['answer'] == 'a'){echo "selected = 'selected'";}?> value="a">A</option>
			<option <?php if($row['answer'] == 'b'){echo "selected = 'selected'";}?> value="b">B</option>
			<option <?php if($row['answer'] == 'c'){echo "selected = 'selected'";}?> value="c">C</option>
			<option <?php if($row['answer'] == 'd'){echo "selected = 'selected'";}?> value="d">D</option>
		</select>
	</div>
	<div class="col-sm-6">
		Marks
		<input type="text" name="req" id="marks" value="<?php echo $row['marks']?>" class="input">
	</div>

    <div class="col-sm-12" style="padding:10px;border-top:1px #eee solid">
		<button class="btn btn-primary" onclick="saveQuestion('<?php echo $row['id'];?>')">
			<i class="fas fa-check"></i>&nbsp;UPDATE
		</button>&nbsp;&nbsp;
		<button class="btn btn-sm btn-danger">CANCEL</button>
		<div style="height:300px;"></div>
		<br/>
		<br/>
	</div>

</div>
</div>
