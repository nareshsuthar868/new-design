<?php
include("../include/config.php");
$data = getData('questions','*','id','desc');
?>
<div class="header">
			<div class="title">
			<div style="float:right;padding:0px 20px;width:60%">
<div style="position:relative;width:100%">
	<table style="width:100%;" cellpadding="0px">
		<tr>
			<td style="width:60%;text-align:right;position:relative;padding-left:0px;">
		<input type="search" style="width:60%;" onclick="$(this).animate({width:'100%'})" placeholder="Start Typing.." onkeyup="searchTable('dataTable',this.value)" class="input" name="">		
		

		<i class="fas fa-search" style="position:absolute;top: 12px;
    right: 10px;color:#ccc;font-size:11px;"></i>
			</td>
			<td style="width:40%;text-align:right">
			&nbsp;&nbsp;

<div class="dropdown" style="display:inline-block">
  <button type="button" class="btn btn-primary " data-toggle="dropdown" onclick="getModule('questions/new.do','formDataContainer','','loading')">
    <i class="fas fa-plus"></i>&nbsp;&nbsp;ADD NEW</li>
  </button>

<div class="dropdown-menu" style="display: none;">
  <ul>
  <li style="cursor:pointer"  id="newstudentbutton" onclick="getModule('questions/new.do','formDataContainer','','loading')">
<i class="fas fa-plus"></i>&nbsp;&nbsp;Add Single</li>
 <!--  <li style="cursor:pointer"   id="" onclick="getModal('import/index.php?type=students','formModal','tableModal','loading')">
<i class="far fa-file-excel"></i>&nbsp;&nbsp;Import Via Excel</li> -->
  </ul>
  </div>
  </div>


<div class="dropdown" style="display:inline-block;display:;">
  <button type="button" class="btn btn-light dropdown-toggle" data-toggle="dropdown">
    Options
  </button>

  <div class="dropdown-menu">
  <ul>
  <li onclick="crossCheckDelete('questions','quesList','dataTableInner')">
<i class="fas fa-times"></i>&nbsp;&nbsp;Delete Selected</li>

<!-- <li onclick="toast('Export not allowed from demo account.','toast bg-danger','4000')">
<i class="fas fa-file-excel"></i>&nbsp;&nbsp;Export Selected</li>
 -->
  </ul>
  </div>
</div>




			

			</td>
		</tr>
	</table>
		
	</div>

				
			</div>
			<strong>
				<i class="fas fa-th-list"></i>&nbsp;&nbsp;&nbsp;Questions
				</strong>
			</div>
</div>
<div class="whiteCover">
<div class="row">
	<div class="col-sm-12 fullheight" id="tableContainer" style="overflow-y:auto;padding:0px;">
	
	
	<table class="table dataTable" id="dataTable">
	<thead>
<tr>
<th class="nofilter" style="width:20px;">#</th>
<th class="nofilter" style="width:20px;">
	<input type="checkbox" name="" onclick="checkAll(this,'dataTable')">
	</th>
	<th>Question</th>
	<th class="hideThis">Q No</th>
	<th class="hideThis">Preview</th>
	<th class="hideThis">Marks</th>
	<th class="hideThis">CreateDate</th>

	
</tr>
</thead>
<tbody id="dataTableInner" >
<?php
$i = 1;
foreach($data as $row)
{
	?>
<tr id="quesList<?php echo $row['id'];?>">
	<td>
		<?php echo $i;?>
	</td>
	<td>
		<input class="checkInput" value="<?php echo $row['id'];?>" type="checkbox" name="">
	</td>
		<td class="text-primary" onclick="getModule('questions/edit.do?id=<?php echo $row['id'];?>','formDataContainer','','loading')"><?php echo $row['question'];?> &nbsp;.....</td>
		<td class="hideThis">Q<?php echo $row['sno'];?></td>
		<td class="hideThis"><button class="btn btn-primary" onclick="getModal('questions/view.php?id=<?php echo $row['id'];?>','formModalBig','tableModalBig','loading')">VIEW </button></td>
		<td class="hideThis"><?php echo $row['marks'];?></td>
		<td class="hideThis"><?php echo date('d-m-Y  H:i', strtotime($row['createdate']));?></td>
</tr>

	<?php
	$i++;
}
?>
	</tbody>
</table>	
<div style="height:100px;"></div>
</div>
	
	<div class="col-sm-8" id="formContainer" style="border-left:1px #eee solid;display:none;position:relative;padding:0px;">
	<div style="position:absolute;top:10px;right:10px;cursor:pointer;">
	<button class="btn btn-sm btn-light" style="color:#ea456c;border:1px #ea456c solid;background-color:#fff !important;margin-right:10px;" onclick="closeFormTab()">
			<i class="far fa-times-circle" ></i> Close

	</button>

	</div>
	<div id="formDataContainer">
		
	</div>
	</div>
</div>
</div>


