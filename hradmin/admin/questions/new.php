<?php
include('../include/config.php');
?>
<div class="subheading" style="border-bottom:1px #eee solid">
New Question
</div>
<div class="row formBox fullheight"  id="formBox" style="overflow-y:scroll">

<div class="row">

	<div class="col-sm-12">
		S.No Number
		<input type="number" name="req" id="serial" value="" class="input">
	</div>

    <div class="col-sm-12">
		<div class="innersubheading">
		Type in your Question
		</div>
      <textarea lang="editor" id="question" ><?php echo $miniWebRow['admission'];?></textarea>
    
    </div>
    <div class="col-sm-6">
		Option A
		<input type="text" name="req" id="option_a" value="" class="input">
	</div>

	<div class="col-sm-6">
		Option B
		<input type="text" name="req" id="option_b" value="" class="input">
	</div>

	<div class="col-sm-6">
		Option C
		<input type="text" name="req" id="option_c" value="" class="input">
	</div>

	<div class="col-sm-6">
		Option D
		<input type="text" name="req" id="option_d" value="" class="input">
	</div>
	<div class="col-sm-6">
		Correct Answer
		<select class="input" id="answer">
			<option value="">Select Option</option>
			<option value="a">A</option>
			<option value="a">B</option>
			<option value="a">C</option>
			<option value="a">D</option>
		</select>
	</div>
	<div class="col-sm-6">
		Marks
		<input type="text" name="req" id="marks" value="" class="input">
	</div>

    <div class="col-sm-12" style="padding:10px;border-top:1px #eee solid">
		<button class="btn btn-primary" onclick="saveQuestion('')">
			<i class="fas fa-check"></i>&nbsp;SAVE
		</button>&nbsp;&nbsp;
		<!-- <button class="btn btn-sm btn-danger">CANCEL</button> -->
		<div style="height:300px;"></div>
		<br/>
		<br/>
	</div>

</div>
</div>
