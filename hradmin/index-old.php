<!DOCTYPE html>
<html>
<head>
	<title>Questionnaire</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans&display=swap" rel="stylesheet">
	<script type="text/javascript" src="jquery-2.0.3.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

</head>
<body>
<?php 
include('connection.php');
if(isset($_POST['start']))
{
	$name = $_POST['name'];
	$email = $_POST['email'];
	$profile = $_POST['profile'];
	$contact = $_POST['contact'];
	$code = $_POST['code'];
}
?>
<div class="container-fluid">
	<div class="row" id="header">
		<div class="col-sm-10">
		<img src="https://d1eohs8f9n2nha.cloudfront.net/images/logo-white.png" style="height: 35px;margin: 15px;">

		</div>
		<div class="col-sm-2" id="profile">
		  Name : <?php echo $name;?> <br>
		  Contact : <?php echo $contact;?> <br>
		  Email : <?php echo $email;?> <br>
		  Code : <?php echo $code;?> <br>
		  Applied For: <?php echo $profile;?> <br><br>
		  <div class="countdown" style="margin-top: 10px;"></div>

		</div>
	</div>

<script type="text/javascript">
	var timer2 = "30:01";
	var interval = setInterval(function() {
	var timer = timer2.split(':');
	  //by parsing integer, I avoid all extra string processing
	var minutes = parseInt(timer[0], 10);
	var seconds = parseInt(timer[1], 10);
	  --seconds;
	 minutes = (seconds < 0) ? --minutes : minutes;
	 if (minutes < 0) clearInterval(interval);
	 seconds = (seconds < 0) ? 59 : seconds;
	 seconds = (seconds < 10) ? '0' + seconds : seconds;
	  //minutes = (minutes < 10) ?  minutes : minutes;
	  $('.countdown').html(minutes + ':' + seconds + '  Minutes Left');
	  timer2 = minutes + ':' + seconds;
	}, 1000);

</script>
	
	<div id="ques-container">
		<div class="row" style="height: 50px;width: 100%;border-bottom: 1px solid #ddd;padding: 0px;margin: 0px;color: #363538;font-weight: bold;">
			 <div class="col-sm-12" style="text-align: center;margin: 10px;"> QUESTIONS </div>
		</div>
		<br>

		<form action="save-test.php" method="POST">
			<br>
			<div class="qbox">
				<p>1. A is the husband of B. E is the daughter of C. A is the father of C. How is B related to E?</p>
			
		        	<label class="optionBox"> &nbsp;&nbsp;Mother
					  <input type="radio"  name="q1" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Grandmother
					  <input type="radio"  name="q1" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Aunt
					  <input type="radio"  name="q1" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Cousin
					  <input type="radio"  name="q1" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>2. 5,11,25,55,117,?. Find the next number in the series.</p>
			
		        	<label class="optionBox"> &nbsp;&nbsp;5279431
					  <input type="radio"  name="q2" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;5978213
					  <input type="radio"  name="q2" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;8251896
					  <input type="radio"  name="q2" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;8543691
					  <input type="radio"  name="q2" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>3. If a : b = 2 : 3 and b : c = 5 : 7, then find a : b : c</p>
			
		        	<label class="optionBox"> &nbsp;&nbsp;234
					  <input type="radio"  name="q3" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;237
					  <input type="radio"  name="q3" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;241
					  <input type="radio"  name="q3" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;243
					  <input type="radio"  name="q3" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>4. If a : b = 2 : 3 and b : c = 5 : 7, then find a : b : c.</p>
			
		        	<label class="optionBox"> &nbsp;&nbsp;12 : 15 : 9
					  <input type="radio"  name="q4" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;10 : 15 : 21
					  <input type="radio"  name="q4" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;14 : 12 : 21
					  <input type="radio"  name="q4" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;2 : 15 : 7
					  <input type="radio"  name="q4" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>5.  A can do a certain job in 8 days. B is 50% more efficient than A. how many days does B alone take to do the same job?</p>
			
		        	<label class="optionBox"> &nbsp;&nbsp;3 Days
					  <input type="radio"  name="q5" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;4 Days
					  <input type="radio"  name="q5" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;5 Days
					  <input type="radio"  name="q5" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;None of the above
					  <input type="radio"  name="q5" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>6.	A speed 15 meter per second is equal to:</p>
			
		        	<label class="optionBox"> &nbsp;&nbsp;56km/h
					  <input type="radio"  name="q6" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;55km/h
					  <input type="radio"  name="q6" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;57km/h
					  <input type="radio"  name="q6" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;54km/h
					  <input type="radio"  name="q6" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>7.	Today is Monday. After 61 days, it will be</p>
			
		        	<label class="optionBox"> &nbsp;&nbsp;Thursday
					  <input type="radio"  name="q7" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Sunday
					  <input type="radio"  name="q7" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Monday
					  <input type="radio"  name="q7" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Saturday
					  <input type="radio"  name="q7" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>8.	(17)<sup>3.5</sup> x (17)<sup>?</sup> = 178</p>
			
		        	<label class="optionBox"> &nbsp;&nbsp;2.29
					  <input type="radio"  name="q8" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;2.75
					  <input type="radio"  name="q8" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;4.25
					  <input type="radio"  name="q8" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;4.5
					  <input type="radio"  name="q8" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>9.	Find the missing number in the image below. <br>					
					<center><img src="question-images/Q9.png" style="height: 160px;"></center>
				</p>
			
		        	<label class="optionBox"> &nbsp;&nbsp;Thursday
					  <input type="radio"  name="q9" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Sunday
					  <input type="radio"  name="q9" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Monday
					  <input type="radio"  name="q9" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Saturday
					  <input type="radio"  name="q9" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>10.	Six bells commence tolling together and toll at intervals of 2, 4, 6, 8 10 and 12 seconds respectively. In 30 minutes, how many times do they toll together?</p>
			
		        	<label class="optionBox"> &nbsp;&nbsp;10
					  <input type="radio"  name="q10" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;12
					  <input type="radio"  name="q10" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;15
					  <input type="radio"  name="q10" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;16
					  <input type="radio"  name="q10" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;20
					  <input type="radio"  name="q10" value="">
					  <span class="checkmark"></span>
					</label>

					<hr>				
			</div>
			<div class="qbox">
				<p>11.	Brad owns a fruit stand. He sells 40% of his apples and still has 420 apples. Originally, he had how many apples?</p>
			
		        	<label class="optionBox"> &nbsp;&nbsp;600
					  <input type="radio"  name="q11" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;640
					  <input type="radio"  name="q11" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;700
					  <input type="radio"  name="q11" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;720
					  <input type="radio"  name="q11" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;740
					  <input type="radio"  name="q11" value="">
					  <span class="checkmark"></span>
					</label>

					<hr>				
			</div>
			<div class="qbox">
				<p>12.	Which of the figures (1), (2), (3) or (4) can be formed from the pieces given in figure (X).<br>					
					<center><img src="question-images/Q12.png" style="height: 160px;"></center>
				</p>
			
		        	<label class="optionBox"> &nbsp;&nbsp;1
					  <input type="radio"  name="q12" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;2
					  <input type="radio"  name="q12" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;3
					  <input type="radio"  name="q12" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;4
					  <input type="radio"  name="q12" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>13.	Fill in the blank in the sentence below with the word that makes the sentence meaningfully complete.
					<br>
					<center><strong>Man does not live by _______ alone.</strong></center>
				</p>
			
		        	<label class="optionBox"> &nbsp;&nbsp;Food
					  <input type="radio"  name="q13" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Bread
					  <input type="radio"  name="q13" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Meals
					  <input type="radio"  name="q13" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Diet	
					  <input type="radio"  name="q13" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Exercise
					  <input type="radio"  name="q13" value="">
					  <span class="checkmark"></span>
					</label>

					<hr>				
			</div>
			<div class="qbox">
				<p>14.	When used as verbs, the words table and shelf have ____ meanings?
					
				</p>
			
		        	<label class="optionBox"> &nbsp;&nbsp;Similar
					  <input type="radio"  name="q14" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Contradictory
					  <input type="radio"  name="q14" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Unrelated
					  <input type="radio"  name="q14" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>15.	In a party, 10 men shake hands with each other and they get to shake everyone’s hand once. How many total handshakes are there?
					
				</p>
			
		        	<label class="optionBox"> &nbsp;&nbsp;45
					  <input type="radio"  name="q15" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;40
					  <input type="radio"  name="q15" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;50
					  <input type="radio"  name="q15" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;10
					  <input type="radio"  name="q15" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>

			<div class="qbox">
				<p>16.	How many seconds are there in a day?</p>		
			
		        	<label class="optionBox"> &nbsp;&nbsp;82,400
					  <input type="radio"  name="q16" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;85,400
					  <input type="radio"  name="q16" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;88,400
					  <input type="radio"  name="q16" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;86,400
					  <input type="radio"  name="q16" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>17.	Identify the next number in the sequence.  41, 29, 35, 23, _____				
				</p>
			
		        	<label class="optionBox"> &nbsp;&nbsp;22
					  <input type="radio"  name="q17" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;21
					  <input type="radio"  name="q17" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;29
					  <input type="radio"  name="q17" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;24
					  <input type="radio"  name="q17" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>18.	The day after the day after tomorrow is four days before Monday. What day is it today?</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;Monday
					  <input type="radio"  name="q18" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Tuesday
					  <input type="radio"  name="q18" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Wednesday
					  <input type="radio"  name="q18" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;Thursday
					  <input type="radio"  name="q18" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;Friday
					  <input type="radio"  name="q18" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>19.	Count the number of triangle in the following figure: <br>
					<center><img src="question-images/Q19.jpg" style="height: 150px;"></center>
				</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;10
					  <input type="radio"  name="q19" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;14
					  <input type="radio"  name="q19" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;18
					  <input type="radio"  name="q19" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;12
					  <input type="radio"  name="q19" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;16
					  <input type="radio"  name="q19" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>20.	Among six persons – K, L, M, N, O and P- each lives on a different floor of a building having six floors numbered one to six (the ground floor is numbered 1, the above floor is numbered 2 and so on and the top most floor is numbered 6).
L lives on an even numbered floor. L lives on a floor immediately below K’s floor and immediately above M’s floor. P lives on a floor immediately above N’s floor. P lives on an even numbered floor. O does not live on floor number 4.
<br> <i>Who lives on a floor immediately above P's floor ?</i>
</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;L
					  <input type="radio"  name="q20" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;M
					  <input type="radio"  name="q20" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;O
					  <input type="radio"  name="q20" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;K
					  <input type="radio"  name="q20" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;None of the above
					  <input type="radio"  name="q20" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>21.	On which floor does N live ?</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;6th
					  <input type="radio"  name="q21" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;2nd
					  <input type="radio"  name="q21" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;3rd
					  <input type="radio"  name="q21" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;5th
					  <input type="radio"  name="q21" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;1st
					  <input type="radio"  name="q21" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>22.	Who amongst the following does live on 5th floor ?  </p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;M
					  <input type="radio"  name="q22" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;O
					  <input type="radio"  name="q22" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;N
					  <input type="radio"  name="q22" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;K
					  <input type="radio"  name="q22" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;None of the above
					  <input type="radio"  name="q22" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>23.	On which floor does O live ?</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;6th
					  <input type="radio"  name="q23" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;2nd
					  <input type="radio"  name="q23" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;3rd
					  <input type="radio"  name="q23" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;5th
					  <input type="radio"  name="q23" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;None of the above
					  <input type="radio"  name="q23" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>24. Who amongst the following live on the floors exactly between K and P ?  </p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;and L
					  <input type="radio"  name="q24" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;L and M
					  <input type="radio"  name="q24" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;L and N
					  <input type="radio"  name="q24" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;M and N
					  <input type="radio"  name="q24" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;M and O
					  <input type="radio"  name="q24" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>
					<i>Directions for questions 25 to 29: Study the information given below carefully and answer the following questions.</i><br><br>
A Hospital has decided to recruit doctors. The following criteria are to be applied to select the candidates. An applicant must <br><br>

A.	Be an MBBS/BDS graduation with at least 50% marks & 70% marks in MD. <br>
B.	Have minimum 2 years work experience. <br>
C.	Be ready to sign a 2 years bond. <br>
D.	Age criteria: 25 years to 32 years as on 11.11.2016. <br><br>

However, if the applicant fulfils all the eligibility criteria except <br><br>
i.	But has obtained 60% in graduation degree and 50% in MD and has 3 years working experience, the case may be referred to the CMO. <br>
ii.	But is willing to pay an amt.ofRs 1.5 lakh if he/she wants to leave, the case may be referred to the senior doctors.<br>
iii.	But is a BDS doctor, the case is to be referred to the junior doctors.<br><br>
<i>Details of applicants are given below in the questions. On the basis of following course of action which is based on the data given above, mark the answer.</i><br>
All cases are given to you as on the date 11.11.2016.
				</p>
				<p>25.	Kavita is an MBBS doctor with 60% marks and Post graduation in MD with 82% marks. She has been working as doctor for last 5 years. She is not willing to sign the bond but can pay Rs 1 lakh if sheleaves.She has just completed 27 years.</p>			
			
		        	<label class="optionBox"> &nbsp;&nbsp;if candidate is to be selected.
					  <input type="radio"  name="q25" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;if case is to be referred to the Senior doctor
					  <input type="radio"  name="q25" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;if case is to be referred to the Junior doctor.
					  <input type="radio"  name="q25" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;if case is to be referred to the CMO
					  <input type="radio"  name="q25" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;if Applicant is not to be selected
					  <input type="radio"  name="q25" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>26.	Rohit is 28 years old and has been working in a Hospital for the past 4 years. He has scored 72% and 82% marks in MBBS and MD respectively. He is ready to sign a bond with any hospital.</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;if candidate is to be selected.
					  <input type="radio"  name="q26" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;if case is to be referred to the Senior doctor.
					  <input type="radio"  name="q26" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;if case is to be referred to the Junior doctor.
					  <input type="radio"  name="q26" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;if case is to be referred to the CMO.
					  <input type="radio"  name="q26" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;if Applicant is not to be selected.
					  <input type="radio"  name="q26" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>27.	Neetu is a MBBS graduate Doctor with 75% marks and 85% marks in MD. She completed her Graduation in 2014 at the age of 22 years & immediately started working in Hospital. She is interested in going to Finland and is not ready to sign a bond. However, she doesn’t mind to pay 2 lakh amount.</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;if candidate is to be selected.
					  <input type="radio"  name="q27" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;if case is to be referred to the Senior doctor.
					  <input type="radio"  name="q27" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;if case is to be referred to the Junior doctor.
					  <input type="radio"  name="q27" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;if case is to be referred to the CMO.
					  <input type="radio"  name="q27" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;if Applicant is not to be selected
					  <input type="radio"  name="q27" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>28.	Veer is BDS graduate with 74% marks in degree and 93% marks in MD. He joined as a doctor in Sanjeevani Hospital three years ago at the age of 27 years. He is willing to sign a bond with Hospital.</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;if candidate is to be selected.
					  <input type="radio"  name="q28" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;if case is to be referred to the Senior doctor.
					  <input type="radio"  name="q28" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;if case is to be referred to the Junior doctor.
					  <input type="radio"  name="q28" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;if case is to be referred to the CMO.
					  <input type="radio"  name="q28" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;if Applicant is not to be selected
					  <input type="radio"  name="q28" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>29.	Romi is a Doctor and has obtained 59% and 60% marks in MBBS and MD respectively. He is 28 years old and ready to sign a bond.</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;if candidate is to be selected.
					  <input type="radio"  name="q29" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;if case is to be referred to the Senior doctor.
					  <input type="radio"  name="q29" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;if case is to be referred to the Junior doctor.
					  <input type="radio"  name="q29" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;if case is to be referred to the CMO.
					  <input type="radio"  name="q29" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;if Applicant is not to be selected
					  <input type="radio"  name="q29" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>30.	Which number should come next in this series?  25, 24, 22, 19, 15</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;14
					  <input type="radio"  name="q30" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;5
					  <input type="radio"  name="q30" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;30
					  <input type="radio"  name="q30" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;10
					  <input type="radio"  name="q30" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;0
					  <input type="radio"  name="q30" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>31.	How old will Nia be when she is twice as old as her sister? Nia, twelve years old, is three times as old as her sister.</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;15
					  <input type="radio"  name="q31" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;18
					  <input type="radio"  name="q31" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;16
					  <input type="radio"  name="q31" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;20
					  <input type="radio"  name="q31" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;21
					  <input type="radio"  name="q31" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>32.	Which of the following words is closest in meaning to APPREHENSIVE?</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;Thorough
					  <input type="radio"  name="q32" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Foolish
					  <input type="radio"  name="q32" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Anxious
					  <input type="radio"  name="q32" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;Distraught
					  <input type="radio"  name="q32" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;Comprehensive
					  <input type="radio"  name="q32" value="">
					  <span class="checkmark"></span>
					</label>
					<hr>				
			</div>
			<div class="qbox">
				<p>33.	In the following question, there is a relationship between two given words on one side of: : and one word is given on another side of: : while another word is to be found from the given alternatives, having the same relation with this word as the words of the given pair bear. Choose the correct alternative. <br>
					<center><strong>Scribble:Write:Stammer:?</strong></center>
				</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;Walk
					  <input type="radio"  name="q33" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Play
					  <input type="radio"  name="q33" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Speak
					  <input type="radio"  name="q33" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;Dance
					  <input type="radio"  name="q33" value="">
					  <span class="checkmark"></span>
					</label>
					
					<hr>				
			</div>
			<div class="qbox">
				<p>34.	Choose the word out of the given four alternatives which will fill in the blank space and show the same relationship with the third word as between the first two. <br>
					<center><strong>Candid is to indirect as honest is to………?</strong></center>
				</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;Frank
					  <input type="radio"  name="q34" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Wicked
					  <input type="radio"  name="q34" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Truthful
					  <input type="radio"  name="q34" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;Untruthful
					  <input type="radio"  name="q34" value="">
					  <span class="checkmark"></span>
					</label>
					
					<hr>				
			</div>
			<div class="qbox">
				<p>35.	Which of the following can be arranged into a 5-letter English word?<br>
					
				</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;H R G S T
					  <input type="radio"  name="q35" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;R I L S A
					  <input type="radio"  name="q35" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;T O O M T
					  <input type="radio"  name="q35" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;W Q R G S
					  <input type="radio"  name="q35" value="">
					  <span class="checkmark"></span>
					</label>
					
					<hr>				
			</div>
			<div class="qbox">
				<p>36.	Book is to Reading as Fork is to:<br>
					
				</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;Drawing
					  <input type="radio"  name="q36" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Writing
					  <input type="radio"  name="q36" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;Stirring
					  <input type="radio"  name="q36" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;Eating
					  <input type="radio"  name="q36" value="">
					  <span class="checkmark"></span>
					</label>
					
					<hr>				
			</div>
			<div class="qbox">
				<p>37.	What number best completes the analogy:<br>
					<center><strong>8:4 as 10:</strong></center>
				</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;3
					  <input type="radio"  name="q37" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;7
					  <input type="radio"  name="q37" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;24
					  <input type="radio"  name="q37" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;5
					  <input type="radio"  name="q37" value="">
					  <span class="checkmark"></span>
					</label>
					
					<hr>				
			</div>
			<div class="qbox">
				<p>38.	The rate at which a sum becomes four times of itself in 25 years at Simple Interest, will be:<br>
					
				</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;30%
					  <input type="radio"  name="q38" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;25%
					  <input type="radio"  name="q38" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;12%
					  <input type="radio"  name="q38" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;49%
					  <input type="radio"  name="q38" value="">
					  <span class="checkmark"></span>
					</label>
					
					<hr>				
			</div>
			<div class="qbox">
				<p>39.	Half percent, written as a decimal, is<br>
					
				</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;0.2
					  <input type="radio"  name="q39" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;0.02
					  <input type="radio"  name="q39" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;0.005
					  <input type="radio"  name="q39" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;0.05
					  <input type="radio"  name="q39" value="">
					  <span class="checkmark"></span>
					</label>
					
					<hr>				
			</div>
			<div class="qbox">
				<p>40.	What is the average of first five multiples of 12?<br>
					
				</p>				
			
		        	<label class="optionBox"> &nbsp;&nbsp;36
					  <input type="radio"  name="q40" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;38
					  <input type="radio"  name="q40" value="">
					  <span class="checkmark"></span>
					</label>

					<label class="optionBox"> &nbsp;&nbsp;40
					  <input type="radio"  name="q40" value="">
					  <span class="checkmark"></span>
					</label>
					<label class="optionBox"> &nbsp;&nbsp;42
					  <input type="radio"  name="q40" value="">
					  <span class="checkmark"></span>
					</label>
					
					<hr>				
			</div>
			
			<center><button class="btn btn-success" type="submit">SUBMIT TEST</button></center>
			<br><br>

		</form>
 	
	</div>
	

</div>
	
</body>
</html>

