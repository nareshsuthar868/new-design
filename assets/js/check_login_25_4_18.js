

function check_value(){
    $("#password_change").validate ({
        rules: {      
            chngpass: {
                required:true,
                minlength: 6,
                maxlength: 16                              
            },
             cchngpass: {
                required:true,
                minlength: 6,
                maxlength: 16                              
            }
        },          
        messages:{ 
            chngpass: {
                required : "<font color='red'>Password cannot be empty.</font>",
                minlength : "<font color='red'>Password has atleast 6 character long.</font>",
                maxlength : "<font color='red'>Password length should be 16 characters maximum.</font>"
            }, 
            cchngpass: {
                required : "<font color='red'>Confirm Password cannot be empty.</font>",
                minlength : "<font color='red'>Password has atleast 6 character long.</font>",
                maxlength : "<font color='red'>Password length should be 16 characters maximum.</font>"
            }       
        },
        submitHandler: function(form) {
            var base_url = $('#base_url').val();
            var id = $('#user_id').val();
            var pass = $('#chngpass').val();
            var cpass = $('#cchngpass').val();

            if(pass != cpass){
                sweetAlert({
                    title: "Warning!",
                    text: "Password Not Match",
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    animation: "slide-from-top",
                    showConfirmButton: true,      
                });    
            }
            else{
                $.ajax({
                    method: 'POST',
                    url: baseURL + 'site/user/change_password',
                    data: {"id":id , "cpass": cpass },
                    success: function(response) {
                        if(response.status){
                            // sweetAlert({
                            //     title: "success!",
                            //     text: "Password Changed Successfully",
                            //     type: "success",
                            //     showCancelButton: false,
                            //     closeOnConfirm: true,
                            //     animation: "slide-from-top",
                            //     showConfirmButton: true,      
                            // },  function(isConfirm){   
                            //         if(isConfirm) {
                            //             location.href= base_url;
                            //         }
                            //     }
                            //); 
                            $('#reset_pass').hide(); 
                            $('#reset_password').show();
                            setTimeout(function() {
                                 location.href= base_url;
                            }, 3000);
                        }
                        else{
                            sweetAlert({
                                title: "Warning!",
                                text: "Something Went Wrong !...Please Try Again",
                                type: "warning",
                                showCancelButton: false,
                                closeOnConfirm: true,
                                animation: "slide-from-bottom",
                                showConfirmButton: true,      
                            },  function(isConfirm){   
                                    if (isConfirm) {
                                        location.href= base_url;
                                    }
                                }
                            );   
                        }
                    }
                });
            }
        }
          
    });
};


function check_validation()
{
    $("#sign-up").validate ({
        rules: {
            full_name: {
                required:true, 
                minlength: 2,
                maxlength: 20              
            },
            signup_email: {
                required:true,
                email: true                  
            },                
            signup_password: {
                required:true,
                minlength: 6,
                maxlength: 16                              
            }
        },          
        messages:{ 
            full_name: {
                required  : "<font color='red'>Name cannot be empty.</font>",
                minlength : "<font color='red'>Name has atleast 2 character long.</font>",
                maxlength : "<font color='red'>Name should be 20 characters maximum.</font>"
            },              
            signup_email: {
                required  : "<font color='red'>Email cannot be empty.</font>",
                 email: "<font color='red'>Please enter a valid email address.</font>"
            },
            signup_password: {
                required : "<font color='red'>Password cannot be empty.</font>",
                minlength : "<font color='red'>Password has atleast 6 character long.</font>",
                maxlength : "<font color='red'>Password length should be 16 characters maximum.</font>"
            }       
        },
        submitHandler: function(form) {
            //form.submit();
           
           return  register_user();
           
        }   
    });

   
};




function login()
{
     $("#form-login").validate ({
        rules: {
            email: {
                required:true,
                email: true                  
            },                
            password: {
                required:true                           
            }
        },          
        messages:{            
            email: {
                required  : "<font color='red'>Email cannot be empty.</font>",
                email: "<font color='red'>Please enter a valid email address.</font>"

            },
            password: {
                required : "<font color='red'>Password cannot be empty.</font>"
            }      
        },
        submitHandler: function(form) {
            //form.submit();
            var base_url  = $('#base_url').val();
            var email  = $('#login_email').val();
            var password  = $('#login_password').val();
            $.ajax({
                    method: 'POST',
                    url: baseURL + 'site/user/login_user',
                    data: {"email":email , "password": password },
                    success: function(response) {
                        $('#myModal3').hide();
                        if(response.status){
                            if(response.key == 1){
                            var path = window.location.href;
                                location.href= path; 
                            }
                            else
                            {
                            sweetAlert({
                                title: response.title,
                                text: response.message,
                                type: response.type,
                                showCancelButton: false,
                                closeOnConfirm: true,
                                animation: "slide-from-top",
                                showConfirmButton: true,      
                            },  function(isConfirm){   
                                    if(isConfirm) {
                                        if(response.key == 1)
                                        {
                                        var path = window.location.href;
                                           location.href= path; 
                                        }
                                        else{
                                            $('#myModal3').show();
                                    }
                                    }
                                }
                            );
                            }  
                        }
                    }
                });


            //form_data = form;
        }     
    });
}




function Add_to_whishlist(product_id)
{ 
    
    var user_id = $('#user_id').val();
    if(user_id != '')
    {
   var product_id  = product_id;
   var base_url = $('#base_url').val();
   $.ajax({
        method: 'POST',
        url: baseURL + 'site/product/add_to_whislist',
        data: {"user_id": $('#user_id').val(), "product_id": product_id },
        success: function(response) {
            if(response){

             $('#show_icon_'+product_id).show(); 
             $('#show_button_'+product_id).addClass('wishactive');
             //$('#hide_like_'+product_id).css('display','none');
             $('#hide_like_'+product_id).hide();
            }
            else
            {
                $('#show_icon_'+product_id).hide(); 
             //$('#hide_like_'+product_id).css('display','none');
                $('#hide_like_'+product_id).show();
                 //location.reload();
            }
        }
   });
}
    else
    {
         $('#myModal3').modal('show');
        //return false;
    }
}
function remove_wishlist(product_id)
{
    event.preventDefault();
    if(!confirm('Do you really want to remove this Product?'))return;
    var product_id  = product_id;
    var base_url = $('#base_url').val();
    $.ajax({
        method: 'POST',
        url: baseURL + 'site/product/remove_wishlist',
        data: {"user_id": $('#user_id').val(), "product_id": product_id },
        success: function(response) {
             //$('i').addClass('heart');
             //location.reload();
            $('#new_row_'+product_id).fadeOut('slow');

        }

   });
}



// function loginchk(){
//     var usr = $('#username').val();  
  
//     $.ajax({
            
//             url: theme_url+'/ajax/login_ajax.php', // form action url
//             method: 'POST',
//             data: { username: usr, password : pass,rememberme : rememberme},
//             success:function(result) {
                
//             }
//         });

// }

function add_to_cart(id)
{
    var product_id = $('#product_id_'+id).val();
    var sell_id = $('#sell_id_'+id).val();
    var price = $('#price_'+id).val();
    var product_shipping_cost = $('#product_shipping_cost_'+id).val();
    var product_tax_cost = $('#product_tax_cost_'+id).val();
    var cate_id = $('#cateory_id_'+id).val();
    // var attribute_values = $('#attr_name_id').val();

      $.ajax({
        type: 'POST',
        url: baseURL + 'site/cart/add_to_cart',
        data: {
            'product_id': product_id,
            'sell_id': sell_id,
            'cate_id': cate_id,
            'price':price,
            'product_shipping_cost': product_shipping_cost,
            'product_tax_cost': product_tax_cost,
        },
        success: function(response) {
            var  subTotal = 0;
            $('#add_new_product').empty();
              $('#new_count').text(response.product_value.length);
                    for (var i = 0; i < response.product_value.length ; i++) {
                        var image = response.product_value[i].image.split(',');
                        subTotal += (response.product_value[i].product_shipping_cost * response.product_value[i].quantity) + (response.product_value[i].price * response.product_value[i].quantity) - response.product_value[i].discountAmount;
                        $('#null_value').hide();
                        if(response.product_value[i].attr_type !='' && response.product_value[i].attr_name )
                        {
                          var attr_type = response.product_value[i].attr_type+'/';
                          var attr_name = response.product_value[i].attr_name;
                            
                        }else { attr_type = '';  attr_name='';}
                        $('#add_new_product').append('<li id="header-cart-row-'+ response.product_value[i].id +'"><div class="cart-thumb"><img src="https://d1eohs8f9n2nha.cloudfront.net/images/product/'+ image[0] +'" alt="" /></div><div class="cart-desc"><strong>'+ response.product_value[i].product_name +'</strong><small>'+ attr_type + attr_name +'</small><span>'+ response.product_value[i].quantity +'x Rs '+ response.product_value[i].price +'</span><a href="cart"><i class="material-icons">mode_edit</i></a><a href="javascript:void(0)" onclick="delete_cart('+ response.product_value[i].id +','+  response.product_value[i].id +')"><i class="material-icons">delete</i></a></div> <strong></li>');
                    }
                    $('#add_new_product').append('<li><div class="pull-left">SUBTOTAL</div><div class="pull-right"><strong id="CartGAmt"><i id="new_price" class="fa fa-inr" aria-hidden="true"></i>'+ subTotal +'</strong></div></li>');
                    $('#add_new_product').append('<div class="btncartdiv"><a href="'+ baseURL +'cart" class="btn-check pull-left btngray"><i class="material-icons">shopping_cart</i><span>View Cart</span></a><a href="'+ baseURL +'cart" class="btn-check pull-right"><span>Check Out</span> <i class="material-icons checkrot">reply</i></a></div>');        
                   
                             sweetAlert({
                                    title: "Success!",
                                    text: response.message,
                                    animation: "slide-from-bottom",
                                    type: 'success',
                                    showConfirmButton: false,
                                    timer: 1500

                                });
        }
    });
      return false;
}

function search_product()
{
     var x = document.getElementById("search_value").value;

     if(x == '')
     {
        $( ".new_search_data" ).empty();
        $(".new_cat").empty();
        $('#no_found').show();
        $('#new_text').html("Search Your Products");
     }
     else{
    $.ajax({
        type: 'POST',
        url: baseURL + 'site/product/search_product',
        dataType: 'json',
        data: {
            'searchValue': x 
        },
        success: function(response) {
          //  console.log(response.cat[0].id);

            if(response.product.length > 0){
                    $( ".new_search_data" ).empty();
                    $('#no_found').hide();
                var newHTML = [];
                for (var i = 0; i < response.product.length; i++) {
                    var image = response.product[i].image.split(",");                  
                    var product_name = response.product[i].product_name.replace(/\s+/g, '-');
                   $(".new_search_data").append('<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 margin-bottom-30" ><a href="things/'+ response.product[i].id +'/'+ product_name +'"><figure><div class="searchprothumg"><img src="https://d1eohs8f9n2nha.cloudfront.net/images/product/Copressed Images/'+ image[0] +'" alt=/></div><figcaption><span><strong>'+ response.product[i].product_name +'</strong></span><span>&#8377 '+ response.product[i].price +'</span></figcaption></figure></a></div>');
                }
           }else {
                $( ".new_search_data" ).empty();
                $('#no_found').show();
                $('#new_text').html("No Products Found");
            }

           if(response.cat.length > 0){
           // console.log(response.cat.length);
                $( ".new_cat" ).empty();
                $('#not_found_cat').hide();
                var newHTML = [];
                for (var i = 0; i < response.cat.length; i++) {
                    $(".new_cat").append('<div class="catagorycol"><a href="'+ baseURL +'/shopby/'+ response.cat[i].seourl +'"><figure><img src="https://d1eohs8f9n2nha.cloudfront.net/images/category/'+ response.cat[i].image + '" alt="catagory icon"><figcaption>'+ response.cat[i].cat_name +'</figcaption></figure></a></div>');
                }

           }
            else
            {
                $(".new_cat").empty();
                $('#cat_no').show();
                $('#not_found_cat').html("No Cateory Found");
            }

             
        }
    });
}

}

$('.searchicn').click(function(){
    $( ".new_search_data" ).empty();
    $(".new_cat").empty();
        $('#new_text').html("Search Your Products");
        $('#not_found_cat').html("Search By Cateory");
});


function get_default_address()
{
     var id = $("input[name='Ship_address_val']:checked").val();
       $.ajax({
         type: 'POST',
         url: baseURL + 'site/cart/get_ship_value',
         dataType: 'json',
         data: {
             'id': id 
         },
         success:function(response){
            get_coupon_code();
           $('#address_name').html(response.full_name + '<br>' + response.address1 + ',' + response.city + ',<br>' + response.state + '-' + response.postal_code + ',<br> Phone:' + response.phone );
         $('#form_submit_button').removeAttr('disabled');
         }
     });
}




function  get_coupon_code()
{
 var coupon =  $('#user_id').val();
  $.ajax({
         type: 'POST',
         url: baseURL + 'site/cart/get_coupon',
         dataType: 'json',
         data: {
             'id': coupon 
         },
         success:function(response){
            if(response)
            {
                $('#coupon_code').text(response);
            }
            else{
               $('#coupon_code').text("No");
            }
        }
     });  
}

$(function(){

     $("#customer_payment_form").validate ({

        rules: {
            user_first_name: {
                required:true,
                                 
            },                
            user_email: {
                required:true,
                email: true 

            },
             user_amount: {
                required:true,
                min: 1,
                number: true 

            }
        },          
        messages:{            
            user_first_name: {
                required  : "<font color='red'>User name cannot be empty.</font>"

            },
            user_email: {
                required : "<font color='red'>Email cannot be empty.</font>",
                email: "<font color='red'>Please enter a valid email address.</font>"
            },
             user_amount: {
                required : "<font color='red'>Amount cannot be empty.</font>",
                min: "<font color='red'>Amount must be greater than zero</font>",
                number: "<font color='red'>Please Enter Amount</font>"
            }      
        },
        submitHandler: function(form) {
            form.submit();
        }
    });

});

//infinite scrolling
var alreadyFetchingData = false;
var pageNumber = 1;
var needRecordCount = 12;
var isAvailableRecords = true;
$(window).scroll(function() {
   var hT = $('.new_scroll').offset().top,
       hH = $('.new_scroll').outerHeight(),
       wH = $(window).height(),

       wS = $(this).scrollTop();
   if (wS > (hT+hH-wH) && !alreadyFetchingData && isAvailableRecords){
     alreadyFetchingData = true;
     get_data();
   }
});

//get data and show
function get_data()
{
    var url = location.href;

    $.ajax({
        type: 'POST',
        url: baseURL + 'site/searchShop/search_shopby',
        data: {
            'pg': pageNumber,
            'url':url
        },
          beforeSend: function(xhr) {
            $("#product_main_container").after($("<center><li class='loading'>Loading...</li></center>").fadeIn('slow')).data("loading", true);
        },
        success:function(response){
             $(".loading").fadeOut('slow', function() {
                $(this).remove();
            });
            pageNumber++;
            if(response.length < needRecordCount){
                isAvailableRecords = false;
            }
            for (var i = 0; i <= response.length-1; i++) {

                var product = response[i];
                var image  = product.image.split(',');
                var product_name = product.product_name.replace(/\s+/g, '-');
                var description = product.description.substr(0, 70);
                if(product.subproducts != ''){ 
                        var subprodcuts_array =   product.subproducts.split(',');
                        count = subprodcuts_array.length;
                    }
                    else
                    {
                        count = 1;
                    }
                var html = '<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 prolistcol">\
                            <div class="productlistitem">';
                    html += '<a  href="'+ baseURL +'things/'+ product.id + '/'+ product_name + '" class="productcatimg"><img src="https://d1eohs8f9n2nha.cloudfront.net/images/product/Copressed Images/'+ image[0] +'" /></a>';
                    html +=   ' <div class="relative"><div class="proshortdetail hidden-xs"><span class="title_minheight"><a href="'+  baseURL +'/things/'+ product.id + '/'+ product_name +'">'+ product.product_name +'</a></span><div class="listcontent"><p>'+ description +'.....</p></div></div>';
                    html +=  '<div class="priceitemgray"><a href="'+ baseURL +'things/' + product.id +'/'+ product_name +'" class="visible-xs">' + product.product_name + '</a><span class="startprice">Starting from  <i class="fa fa-inr" aria-hidden="true"></i><strong> '+ product.sale_price +'<strong></span><span class="itemleftw">'+ count +' item</span></div></div>';
                
                $('#product_main_container').append(html);

            }
            alreadyFetchingData = false;
    }
    });
}

$(function(){
    $('#form_submit_button').attr('disabled',true);
})
 
