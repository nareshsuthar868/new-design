function check_value() {
    $("#password_change").validate({
        rules: {
            chngpass: {
                required: !0,
                minlength: 6,
                maxlength: 16
            },
            cchngpass: {
                required: !0,
                minlength: 6,
                maxlength: 16
            }
        },
        messages: {
            chngpass: {
                required: "<font color='#ef534e'>Password cannot be empty.</font>",
                minlength: "<font color='#ef534e'>Password has atleast 6 character long.</font>",
                maxlength: "<font color='#ef534e'>Password length should be 16 characters maximum.</font>"
            },
            cchngpass: {
                required: "<font color='#ef534e'>Confirm Password cannot be empty.</font>",
                minlength: "<font color='#ef534e'>Password has atleast 6 character long.</font>",
                maxlength: "<font color='#ef534e'>Password length should be 16 characters maximum.</font>"
            }
        },
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(e) {
            var t = $("#base_url").val(),
                a = $("#user_id").val(),
                o = $("#chngpass").val(),
                r = $("#cchngpass").val();
            o != r ? sweetAlert({
                title: "Warning!",
                text: "Password Not Match",
                type: "warning",
                showCancelButton: !1,
                closeOnConfirm: !0,
                animation: "slide-from-top",
                showConfirmButton: !0
            }) : $.ajax({
                method: "POST",
                url: baseURL + "site/user/change_password",
                data: {
                    id: a,
                    cpass: r
                },
                success: function(e) {
                    e.status ? ($("#reset_pass").hide(), $("#reset_password").show(), setTimeout(function() {
                        location.href = t
                    }, 3e3)) : sweetAlert({
                        title: "Warning!",
                        text: "Something Went Wrong !...Please Try Again",
                        type: "warning",
                        showCancelButton: !1,
                        closeOnConfirm: !0,
                        animation: "slide-from-bottom",
                        showConfirmButton: !0
                    }, function(e) {
                        e && (location.href = t)
                    })
                }
            })
        }
    })
}

function check_validation() {
    $("#sign-up").validate({
        rules: {
            full_name: {
                required: !0,
                minlength: 2,
                maxlength: 20
            },
            signup_email: {
                required: !0,
                email: !0
            },
            signup_password: {
                required: !0,
                minlength: 6,
                maxlength: 16
            },
            mobile_number: {
                required: !0,
                minlength: 10,
                maxlength: 10,
                number: !0
            }
        },
        messages: {
            full_name: {
                required: "<font style='font-size:12px;' color='#ef534e'>Name cannot be empty.</font>",
                minlength: "<font style='font-size:12px;' color='#ef534e'>Name should be atleast 2 characters long.</font>",
                maxlength: "<font style='font-size:12px;' color='#ef534e'>Name should be 20 characters maximum.</font>"
            },
            signup_email: {
                required: "<font style='font-size:12px;' color='#ef534e'>Email cannot be empty.</font>",
                email: "<font style='font-size:12px;' color='#ef534e'>Please enter a valid email address.</font>"
            },
            signup_password: {
                required: "<font style='font-size:12px;' color='#ef534e'>Password cannot be empty.</font>",
                minlength: "<font style='font-size:12px;' color='#ef534e'>Password must be 6 characters long.</font>",
                maxlength: "<font style='font-size:12px;' color='#ef534e'>Password length should be 16 characters maximum.</font>"
            },
            mobile_number: {
                required: "<font style='font-size:12px;' color='#ef534e'>Number cannot be empty.</font>",
                minlength: "<font style='font-size:12px;' color='#ef534e'>Number has atleast 10 character long.</font>",
                maxlength: "<font style='font-size:12px;' color='#ef534e'>Number should be 10 characters maximum.</font>",
                number: "<font style='font-size:12px;' color='#ef534e'>Please enter a valid Mobile Number.</font>"
            }
        },
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error);
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(e) {
            
            if(document.getElementById('otp_signup').value !== '')
            {
                var otp = document.getElementById('otp_signup').value;
                var matchOtp = document.getElementById('otpFieldSignup').value;
              
                if(otp == matchOtp)
                {
                    return register_user();
                    
                }
                else
                {
                  swal({ title: "Oops!", text: "OTP entered does not match. Try Again! :(", type: "error", timer: 2500, confirmButtonText: "Ok" });
                  return false;
                }
            }
            else if(document.getElementById('otp_signup').value === '')
            {
                swal({ title: "Warning", text: "Verify mobile number first! :(", type: "error", timer: 2500, confirmButtonText: "Ok" });
                  return false;
            }
            //return register_user();
        }
    })
}



function login() {
    $("#form-login").validate({
        rules: {
            email: {
                required: !0,
                email: !0
            },
            password: {
                required: !0
            }
        },
        messages: {
            email: {
                required: "<font color='#EF534E'>Email cannot be empty.</font>",
                email: "<font color='#ef534e'>Please enter a valid email address.</font>"
            },
            password: {
                required: "<font color='#EF534E'>Password cannot be empty.</font>"
            }
        },
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(e) {
            $("#base_url").val();
            var t = $("#login_email").val(),
                a = $("#login_password").val(),
                requested_url = $("#requested_url").val();
            $.ajax({
                method: "POST",
                url: baseURL + "site/user/login_user",
                data: {
                    email: t,
                    password: a
                },
                success: function(e) {
                    if ($("#myModal3").hide(), e.status)
                        if (1 == e.key) {
                            var t = window.location.href;
                            // var t = window.history.back();
                            location.href = requested_url;
                        } else sweetAlert({
                            title: e.title,
                            text: e.message,
                            type: e.type,
                            showCancelButton: !1,
                            closeOnConfirm: !0,
                            animation: "slide-from-top",
                            showConfirmButton: !0
                        }, function(t) {
                            if (t)
                                if (1 == e.key) {
                                    var a = window.location.href;
                                    location.href = requested_url;
                                } else $("#myModal3").show()
                        })
                },
                error:function(error){
                    console.log(error);
                }
            })
        }
    })
}


function addtowhishlist(product_id)
{ 
    var user_id = $('#user_id').val();
    if(user_id != '')
    {
        // var product_id  = product_id;
        var base_url = $('#base_url').val();
        $.ajax({
            method: 'POST',
            url: baseURL + 'site/product/add_to_whislist',
            data: {"user_id": $('#user_id').val(), "product_id": product_id },
            success: function(response) {
                if(response.status == 200){
                    $('#whished_'+product_id).removeClass('icn-wishlist-fill-gray');
                    $('#whished_'+product_id).addClass('icn-wishlist-fill-red');
                    // $('#DynamicWishlist_'+product_id).append('<span class="wishlist"  onclick="addtowhishlist('+ product_id +')"><i id="whished_'+product_id+'" class="icn icn-wishlist-fill-red"></i></span>');
                    $('#show_icon_'+product_id).show(); 
                    $('#show_button_'+product_id).addClass('wishactive');
                    $('#hide_like_'+product_id).hide();
                    $('.addtowishlist').addClass('disabled');
                    $('.addtowishlist').text('Adding');
                        setTimeout(function(){ 
                         $('.addtowishlist').text('Remove From Wishlist');
                         $('.addtowishlist').removeClass('disabled');
                        }, 1000);
                }
                else{
                    $('#whished_'+product_id).removeClass('icn-wishlist-fill-red');
                    $('#whished_'+product_id).addClass('icn-wishlist-fill-gray');
                    $('#show_icon_'+product_id).hide(); 
                    $('#hide_like_'+product_id).show();
                    $('.addtowishlist').addClass('disabled');
                    $('.addtowishlist').text('Removing');
                    setTimeout(function(){ 
                     $('.addtowishlist').text('Add to Wishlist');
                     $('.addtowishlist').removeClass('disabled');
                    }, 1000);
                }
                $('#whishlist_count_header').html(response.data);
            }
        });
    }
    else{
        window.location.href = baseURL + "user_sign_up";
        // $('#myModal3').modal('show');
    }
}

function remove_wishlist(e) {
    event.preventDefault(), confirm("Do you really want to remove this Product?") && (e = e, $("#base_url").val(), $.ajax({
        method: "POST",
        url: baseURL + "site/product/remove_wishlist",
        data: {
            user_id: $("#user_id").val(),
            product_id: e
        },
        success: function(t) {
            $("#new_row_" + e).fadeOut("slow")
        }
    }))
}

function add_to_cart(e) {
    var t = $("#product_id_" + e).val(),
        a = $("#sell_id_" + e).val(),
        o = $("#price_" + e).val(),
        r = $("#product_shipping_cost_" + e).val(),
        n = $("#product_tax_cost_" + e).val(),
        s = $("#cateory_id_" + e).val();
    return $.ajax({
        type: "POST",
        url: baseURL + "site/cart/add_to_cart",
        data: {
            product_id: t,
            sell_id: a,
            cate_id: s,
            price: o,
            product_shipping_cost: r,
            product_tax_cost: n
        },
        success: function(e) {
            var t = 0;
            $("#add_new_product").empty(), $("#new_count").text(e.product_value.length);
            for (var a = 0; a < e.product_value.length; a++) {
                var o = e.product_value[a].image.split(",");
                if (t += e.product_value[a].product_shipping_cost * e.product_value[a].quantity + e.product_value[a].price * e.product_value[a].quantity - e.product_value[a].discountAmount, $("#null_value").hide(), "" != e.product_value[a].attr_type && e.product_value[a].attr_name) var r = e.product_value[a].attr_type + "/",
                    n = e.product_value[a].attr_name;
                else r = "", n = "";
                $("#add_new_product").append('<li id="header-cart-row-' + e.product_value[a].id + '"><div class="cart-thumb"><img src="https://d1eohs8f9n2nha.cloudfront.net/images/product/' + o[0] + '" alt="" /></div><div class="cart-desc"><strong>' + e.product_value[a].product_name + "</strong><small>" + r + n + "</small><span>" + e.product_value[a].quantity + "x Rs " + e.product_value[a].price + '</span><a href="cart"><i class="material-icons">mode_edit</i></a><a href="javascript:void(0)" onclick="delete_cart(' + e.product_value[a].id + "," + e.product_value[a].id + ')"><i class="material-icons">delete</i></a></div> <strong></li>')
            }
            $("#add_new_product").append('<li><div class="pull-left">SUBTOTAL</div><div class="pull-right"><strong id="CartGAmt"><i id="new_price" class="fa fa-inr" aria-hidden="true"></i>' + t + "</strong></div></li>"), $("#add_new_product").append('<div class="btncartdiv"><a href="' + baseURL + 'cart" class="btn-check pull-left btngray"><i class="material-icons">shopping_cart</i><span>View Cart</span></a><a href="' + baseURL + 'cart" class="btn-check pull-right"><span>Check Out</span> <i class="material-icons checkrot">reply</i></a></div>'), sweetAlert({
                title: "Success!",
                text: e.message,
                animation: "slide-from-bottom",
                type: "success",
                showConfirmButton: !1,
                timer: 1500
            })
        }
    }), !1
}

$("input[name='search']").on('keypress', function(e){
    if(e.which == 13){
        var search_query = $(this).val();
        window.location.href = baseURL + 'search/' + search_query;
    }
   
});

$("input[name='search']").on('input', function(e){
    var search_query = this.value;
    var search_list = $(this).next().next('ul.search-bar-listing');
    if(search_query == ""){
        search_list.html("");
        return false;
    }
    $.ajax({
        url:baseURL + "site/product/search_product",
        type:'post',
        data:{
            searchValue:search_query
        },
        dataType:'json',
        success:function(e){
            let search_data = '';
            
            if(e.product.length > 0 || e.cat.length > 0){
                search_list.css({'display':'block'});
            }
            
            if(e.product.length > 0){
                $.each(e.product, function(key, val){
                    search_data += `
                        <a href="${baseURL}things/${val.id}/${val.seourl}">
                            <li>
        						<div class="searchbar-content-wrapper flex-full">
        						    <img src="${baseURL}images/product/Copressed Images/${val.image.split(',')[0]}" alt="product" class="product-img"/>
        							<span>${val.product_name}</span>
        						</div>
        					</li>
                        </a>
                    `;
                });
            }
            if(e.cat.length > 0){
                $.each(e.cat, function(key, val){
                    search_data += `
                        <a href="${baseURL}${$("#selected_city_name").val()}/${val.seourl}">
                            <li>
        						<div class="searchbar-content-wrapper flex-full">
        						    <img src="${baseURL}images/category/${val.image.split(',')[0]}" alt="product" class="product-img"/>
        							<span>${val.cat_name}</span>
        						</div>
        					</li>
                        </a>
                    `;
                });
            }
            search_list.html(search_data);
        }
    });
});

function search_product() {
    var e = document.getElementById("search_value").value;
    console.log($(this).parent());
    // $(this).next('ul.search-bar-listing').css({'display':'block'});
    var catslug = document.getElementById("catSlug").value;
    "" == e ? ($(".new_search_data").empty(), $(".new_cat").empty(), $("#no_found").show(), $("#new_text").html("Search Your Products")) : $.ajax({
        type: "POST",
        url: baseURL + "site/product/search_product",
        dataType: "json",
        data: {
            searchValue: e
        },
        success: function(e) {
            if (e.product.length > 0) {
                $(".new_search_data").empty(), $("#no_found").hide(), $(".new_autofill_tranding_product").hide();
                for (var t = 0; t < e.product.length; t++) {
                    var a = e.product[t].image.split(","),
                        o = e.product[t].product_name.replace(/\s+/g, "-");
                    $(".new_search_data").append('<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 margin-bottom-30" ><a href="things/' + e.product[t].id + "/" + o + '"><figure><div class="searchprothumg"><img src="https://d1eohs8f9n2nha.cloudfront.net/images/product/Copressed Images/' + a[0] + '" alt=/></div><figcaption><span><strong>' + e.product[t].product_name + "</strong></span><span>&#8377 " + e.product[t].price + "</span></figcaption></figure></a></div>")
                }
            } else $(".new_search_data").empty(), $("#no_found").show(), $("#new_text").html("No Products Found");
            if (e.cat.length > 0)
                for ($(".new_cat").empty(), $("#not_found_cat").hide(), t = 0; t < e.cat.length; t++) $(".new_cat").append('<div class="catagorycol"><a href="' + baseURL + catslug +"/" + e.cat[t].seourl + '"><figure><img src="https://d1eohs8f9n2nha.cloudfront.net/images/category/' + e.cat[t].image + '" alt="catagory icon"><figcaption>' + e.cat[t].cat_name + "</figcaption></figure></a></div>");
            else $(".new_cat").empty(), $("#cat_no").show(), $("#not_found_cat").html("No Cateory Found");
            $(".new_autofill_tranding_category").hide()
        }
    })
}

function get_default_address(e) {
    var addrssID =  $('#address_proceed_button').attr('data-address');
    if(addrssID == 0){
        alert("Please select address");
    }else{
        $.ajax({
            type: "POST",
            url: baseURL + "site/cart/get_ship_value",
            dataType: "json",
            data: {
                id: addrssID
            },
            success: function(response) {
                $('#dynamicSelectedAddress').html('<div class="address-box selected flex-full"><h3><i class="icn icn-location"></i>'+ response.full_name +'</h3><address>' + response.address1 + ',' + response.city + ',<br>' + response.state + '-' + response.postal_code + '</address></div>');
                $('#form_submit_button').removeAttr('disabled');
                $('#Ship_address_val').val(response.id);
                get_coupon_code();
            }
        })
        return moveToPaymentPage('cart-2');
    }
}

function moveToPaymentPage(){
	var tab_id = $('#address_proceed_button').attr('data-tab');
    $('.fixed-rental-plans-section').addClass('d-none');
	$('#'+tab_id).removeClass('d-none');
	window.scrollTo({top:0,behavior:'smooth'});
}

function get_coupon_code() {
    var e = $("#user_id").val();
    $.ajax({
        type: "POST",
        url: baseURL + "site/cart/get_coupon",
        dataType: "json",
        data: {
            id: e
        },
        success: function(e) {
            e ? $("#coupon_code").text(e) : $("#coupon_code").text("No")
        }
    })
}
$(".searchicn").click(function() {
    $(".new_search_data").empty(), $(".new_cat").empty(), $("#new_text").html("Search Your Products"), $("#not_found_cat").html("Search By Cateory")
}), 

$(function() {
    $("#customer_payment_form").validate({
        rules: {
            user_first_name: {
                required: !0
            },
            user_email: {
                required: !0,
                email: !0
            },
            user_amount: {
                required: !0,
                min: 1,
                number: !0
            }
        },
        messages: {
            user_first_name: {
                required: "<font color='#ef534e'>Name cannot be empty.</font>"
            },
            user_email: {
                required: "<font color='#ef534e'>Email cannot be empty.</font>",
                email: "<font color='#ef534e'>Please enter a valid email address.</font>"
            },
            user_amount: {
                required: "<font color='#ef534e'>Amount cannot be empty.</font>",
                min: "<font color='#ef534e'>Amount must be greater than zero</font>",
                number: "<font color='#ef534e'>Please Enter Amount</font>"
            }
        },
        submitHandler: function(e) {
            var t = $("#user_email").val(),
                a = $("#order_id").val(),
                o = $("input[name=payment_option]:checked").val();
                // console.log("payment mode"+o);
                var payment_mode = $('#debit_card_bank').val()
                var selected_mode =  $("input[name=payment_mode]:checked").val()
                if(o != 'one_time_payment'){
                    if(!selected_mode){
                        alert("Please select payment mode for standing instructions");
                        return false;
                    }else if(selected_mode == 'debit_cart'){
                        if(!payment_mode || payment_mode == 'none'){
                            alert("Please select any bank for standing instructions");
                            return false;
                        }
                    }
                    if(selected_mode != 'debit_cart'){
                        payment_mode = 'credit_card';
                    }
                }
            $.ajax({
                type: "POST",
                url: baseURL + "customerpayment/CustomerPayment",
                data: {
                    user_email: t,
                    order_id: a,
                    payment_option: o,
                    payment_mode:payment_mode,
                    selected_mode:selected_mode
                },
                beforeSend: function() {
                    $("#admin_loader").show()
                },
                success: function(t) {
                    if (400 == t.status_code) return sweetAlert({
                        title: "Warning!",
                        text: t.msg,
                        type: "warning",
                        showCancelButton: !1,
                        closeOnConfirm: !0,
                        animation: "slide-from-top",
                        showConfirmButton: !0
                    }), !1;
                    e.submit()
                }
            })
        }
    })
});


var alreadyFetchingData = !1,
    pageNumber = 1,
    needRecordCount = 12,
    isAvailableRecords = !0,
    listingType = 0;

function get_data()
{
    var url = location.href; 
    if (window.location.href.indexOf('?') > -1) {
        var query = location.search.substr(1);
        var params = query.split("&");
        // return false;
        var CutomeFilters = [];
        for(var i=0; i<params.length; i++) {
            var item = params[i].split("=");
            // console.log("filter_array",item);
            CutomeFilters.push(item[1])
        }
        url = '<?php echo base_url() ?>' + window.location.pathname;
    }    
    $.ajax({
        type: 'POST',
        url: 'site/searchShop/search_shopby',
        data: {
            'pg': pageNumber,
            'url':url,
            'load':'lazy',
            'filter':CutomeFilters,
            'listing_type':listingType,
            'sorting':sortingValue
        },
        beforeSend: function(xhr) {
            $("#product_main_container").after($("<center><li class='loading'>Loading...</li></center>").fadeIn('slow')).data("loading", true);
        },
        success:function(response){
            console.log("response",response);
            category_type = response['category_type'];
            response_product = response['ResponseProductList'];
             $(".loading").fadeOut('slow', function() {
                $(this).remove();
            });
            pageNumber++;
            if(response_product.length < 1){
                listingType = 1;
                isAvailableRecords = false;
            }
            // console.log(response_product[0].pq_quantity);
            if(response_product[0].pq_quantity < 1){
                 isAvailableRecords = false;
            }
            
            if(listingType > 0){
                listingType++;
                if(response_product.length < 12){
                    isAvailableRecords = false;
                }
            }
            // category_type = response_product['category_type'];
            
            if(category_type == 'Packages' || category_type == 'Combos'){
                
                for (var i = 0; i <= response_product.length-1; i++) {

                    var product = response_product[i];
                    var image  = product.image.split(',');
                    var product_name = product.product_name.replace(/\s+/g, '-');
                    var seo_url = product.seourl;
                    var description = product.description.substr(0, 60);
                    if(product.subproducts != ''){ 
                        count = product.subproduct_quan;
                    }
                    else
                    {
                        count = 1;
                    }
                    var html = '';
                    var Item_sold = '';
                    var notifyBtn = '';
                    // var html = '<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 prolistcol">\
                    //             <div class="productlistitem">';
                    var ISsold = '';
                    if(product.is_sold){
                        Item_sold =  '<div class="ribbon ribbon-top-right"><span>Out of Stock </span></div>';
                        notifyBtn = '<div class="overlay"></div><div class="button"><a href="#"> Notify Me </a></div>';
                    }
                        // html += '<a target="_blank" href="'+ baseURL +'things/'+ product.id + '/'+ seo_url + '" class="productcatimg hidden-xs"><img data-src="https://d1eohs8f9n2nha.cloudfront.net/images/product/Copressed Images/'+ image[0] +'" src="'+baseURL+'images/ajax-loader/ajax-loader_new.gif"  />'+ Item_sold +'</a><a href="'+ baseURL +'things/'+ product.id + '/'+ seo_url + '" class="productcatimg visible-xs"><img data-src="https://d1eohs8f9n2nha.cloudfront.net/images/product/Copressed Images/'+ image[0] +'" src="'+baseURL+'images/ajax-loader/ajax-loader_new.gif" />'+ Item_sold +'</a>';
                        // html +=   ' <div class="relative"><div class="proshortdetail hidden-xs"><span class="title_minheight"><a target="_blank" href="'+  baseURL +'things/'+ product.id + '/'+ seo_url +'">'+ product.product_name +'</a></span><div class="listcontent"><p>'+ description +'.....</p></div></div>';
                        // html +=  '<div class="priceitemgray"><a href="'+ baseURL +'things/' + product.id +'/'+ seo_url +'" class="visible-xs">' + product.product_name + '</a><span class="startprice">Starting from  <i class="fa fa-inr" aria-hidden="true"></i><strong> '+ product.sale_price +'<strong></span><span class="itemleftw">'+ count +' item</span></div></div>';
                    var whishlistspan = '<span class="wishlist" onclick="addtowhishlist('+ product.id +')"><i id="whished_'+product.id+'" class="icn icn-wishlist-fill-gray"></i></span>';
                    if(product.like == '1'){
                        whishlistspan = '<span class="wishlist" onclick="addtowhishlist('+ product.id +')"><i id="whished_'+product.id+'" class="icn icn-wishlist-fill-red"></i></span>';
                    }
                    
                    
        //             var dynamicSubproductlist = '<div class="more-items-expand flex-full" style="display: none;" id="subProductList_'+product.id+'">\
                    // 	<span class="items-count w-100">'+product.subproductimages.length+' item</span>\
                    // 	<ul class="items-lisitng flex-full">';
                    // 	    for (var i= 0; i < product.subproductimages.length ; i++) { 
                    // 	        var product_images = product.subproductimages[i].image.split(',');
                    // 	    }
                    // 	dynamicSubproductlist += '<li><a href="javascript:void(0)"><img src="'+baseURL+'images/product/Copressed Images/'+ product_images[0] +'" alt="Product Image"></a></li></ul></div>';
                                        
                                
                    html += '<li><div class="product-single flex-full align-content-start position-relative">\
                                <div class="product-image flex-full position-relative" id="DynamicWishlist_'+product.id+'">\
                                    <a href="'+ baseURL +'things/'+ product.id + '/'+ seo_url + '" class="flex-full position-relative h-100" target="_blank">\
                                        <amp-img src="https://d3juy0zp6vqec8.cloudfront.net/images/product/Copressed Images/'+ image[0] +'" alt="'+ product.product_name +'" layout="responsive" width="300" height="225">\
                                            <noscript>\
                                                <img src="<?php echo base_url(); ?>/images/ajax-loader/ajax-loader.gif" alt="Example" loading="lazy">\
                                            </noscript>\
                                        </amp-img>'+ Item_sold +'\
                                    </a>';
                                        if(product.product_label != ""){
                                            html += '<span class="new-label">'+ product.product_label.split("_").join(" ")+'</span> '+ whishlistspan +notifyBtn;
                                        }else{
                                            html += whishlistspan +notifyBtn;
                                        }
                                html += '</div>\
                                <div class="product-description flex-full">\
                                    <div class="product-description-wrapper flex-full">\
                                        <h3><a href="'+ baseURL +'things/'+ product.id + '/'+ seo_url + '" target="_blank">'+ product.product_name +'</a></h3>\
                                        <p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> '+ product.sale_price +'</strong> / mon</p>\
                                    </div>';
                                    if(product.subproductimages.length > 0){
                                        html += `
                                            <div class="included-items-block flex-full">
                                                <h4>${product.subproductimages.length} Items Included</h4>
                                                <ul class="included-items-listing flex-full">`;
                                                for(k = 0;k < product.subproductimages.length;k++){
                                                    product_image = product.subproductimages[k].image.split(',');
                                                    html += `
                                                        <li>
                                                            <a href="javascript:void(0)">
                                                                <amp-img src="${baseURL}images/product/Copressed Images/${product_image[0]}" alt="Product Image"
                                                                            width="78" height="59"></amp-img>
                                                            </a>
                                                        </li>
                                                    `;
                                                }
                                                html += `</ul>
                                            </div>
                                        `;
                                    }
                                html += '</div>\
                            </div></li>';
                   
                   
                   
                    $('#product_main_container').append(html);
    
                }
            }else{
                for (var i = 0; i <= response_product.length-1; i++) {

                    var product = response_product[i];
                    var image  = product.image.split(',');
                    var product_name = product.product_name.replace(/\s+/g, '-');
                    var seo_url = product.seourl;
                    var description = product.description.substr(0, 60);
                    if(product.subproducts != ''){ 
                        count = product.subproduct_quan;
                    }
                    else
                    {
                        count = 1;
                    }
                    var html = '';
                    var Item_sold = '';
                    var notifyBtn = '';
                    // var html = '<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 prolistcol">\
                    //             <div class="productlistitem">';
                    var ISsold = '';
                    if(product.is_sold){
                        Item_sold =  '<div class="ribbon ribbon-top-right"><span>Out of Stock </span></div>';
                        notifyBtn = '<div class="overlay"></div><div class="button"><a href="#"> Notify Me </a></div>';
                    }
                        // html += '<a target="_blank" href="'+ baseURL +'things/'+ product.id + '/'+ seo_url + '" class="productcatimg hidden-xs"><img data-src="https://d1eohs8f9n2nha.cloudfront.net/images/product/Copressed Images/'+ image[0] +'" src="'+baseURL+'images/ajax-loader/ajax-loader_new.gif"  />'+ Item_sold +'</a><a href="'+ baseURL +'things/'+ product.id + '/'+ seo_url + '" class="productcatimg visible-xs"><img data-src="https://d1eohs8f9n2nha.cloudfront.net/images/product/Copressed Images/'+ image[0] +'" src="'+baseURL+'images/ajax-loader/ajax-loader_new.gif" />'+ Item_sold +'</a>';
                        // html +=   ' <div class="relative"><div class="proshortdetail hidden-xs"><span class="title_minheight"><a target="_blank" href="'+  baseURL +'things/'+ product.id + '/'+ seo_url +'">'+ product.product_name +'</a></span><div class="listcontent"><p>'+ description +'.....</p></div></div>';
                        // html +=  '<div class="priceitemgray"><a href="'+ baseURL +'things/' + product.id +'/'+ seo_url +'" class="visible-xs">' + product.product_name + '</a><span class="startprice">Starting from  <i class="fa fa-inr" aria-hidden="true"></i><strong> '+ product.sale_price +'<strong></span><span class="itemleftw">'+ count +' item</span></div></div>';
                    var whishlistspan = '<span class="wishlist" onclick="addtowhishlist('+ product.id +')"><i id="whished_'+product.id+'" class="icn icn-wishlist-fill-gray"></i></span>';
                    if(product.like == '1'){
                        whishlistspan = '<span class="wishlist" onclick="addtowhishlist('+ product.id +')"><i id="whished_'+product.id+'" class="icn icn-wishlist-fill-red"></i></span>';
                    }
                    // console.log(whishlistspan);
                    
        //             var dynamicSubproductlist = '<div class="more-items-expand flex-full" style="display: none;" id="subProductList_'+product.id+'">\
                    // 	<span class="items-count w-100">'+product.subproductimages.length+' item</span>\
                    // 	<ul class="items-lisitng flex-full">';
                    // 	    for (var i= 0; i < product.subproductimages.length ; i++) { 
                    // 	        var product_images = product.subproductimages[i].image.split(',');
                    // 	    }
                    // 	dynamicSubproductlist += '<li><a href="javascript:void(0)"><img src="'+baseURL+'images/product/Copressed Images/'+ product_images[0] +'" alt="Product Image"></a></li></ul></div>';
                                        
                                
                    html += '<li><div class="product-single flex-full align-content-start position-relative">\
                                <div class="product-image flex-full position-relative" id="DynamicWishlist_'+product.id+'">\
                                    <a href="'+ baseURL +'things/'+ product.id + '/'+ seo_url + '" class="flex-full position-relative h-100" target="_blank">\
                                        <amp-img src="https://d3juy0zp6vqec8.cloudfront.net/images/product/Copressed Images/'+ image[0] +'" alt="'+ product.product_name +'" layout="responsive" width="300" height="225">\
                                            <noscript>\
                                                <img src="<?php echo base_url(); ?>/images/ajax-loader/ajax-loader.gif" alt="Example" loading="lazy">\
                                            </noscript>\
                                        </amp-img>'+ Item_sold +'\
                                    </a>';
                                    if(product.product_label != ""){
                                        html += '<span class="new-label">'+ product.product_label.split("_").join(" ")+'</span> '+ whishlistspan +notifyBtn;
                                    }else{
                                        html += whishlistspan +notifyBtn;
                                    }
                            html += '</div>\
                                <div class="product-description flex-full">\
                                    <div class="product-description-wrapper flex-full">\
                                        <h3><a href="'+ baseURL +'things/'+ product.id + '/'+ seo_url + '" target="_blank">'+ product.product_name +'</a></h3>\
                                        <p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> '+ product.sale_price +'</strong> / mon';
                                        if(product.subproductimages.length > 0){
                                         html +=' <span class="items-count" onclick="openSubBox("'+product.id+'")"> +'+product.subproductimages.length+' item </span>';
                                        } 
                                        html +='</p>\
                                    </div>\
                                </div></div></li>';
                   
                   
                    
                    $('#product_main_container').append(html);
    
                }
            }

            alreadyFetchingData = false;
            $("img").unveil(400);
        }
    });
}

function onCategoryChange(cat_slug,seo_url,type,filter = null){
    if(type == 'c'){
        $('#mobileCatSlider').hide();
    }
    if(filter != null){
        if(filter.length > 0){
            var valNew = filter.join('&filter=');
            window.history.pushState('page2', 'Title', baseURL + cat_slug + '/' + seo_url+'?filter='+valNew);
        }else{
            window.history.pushState('page2', 'Title', baseURL + cat_slug + '/' + seo_url);    
        }
    }else{
        window.history.pushState('page2', 'Title', baseURL + cat_slug + '/' + seo_url);
    }

    $('.mobile-navigation').removeClass('active')
    $(document.body).removeClass('product-listing');
    $('#cat_slug').val(cat_slug);
    $('#category_slug').val(seo_url);
    $("#sortingdrop").val('all');
    if(filter == null){
        filter_array = [];
        $('.justify-content-end').html('');
    }
    $.ajax({
        type: 'POST',
        url: baseURL + 'site/searchShop/search_shopby',
        dataType: 'json',
        async:false,
        data: { 
            // 'pg': 0,
            'url':baseURL + cat_slug + '/' + seo_url,
            'load':'first',
            'cat_slug':cat_slug,
            'filter':filter
        },
        beforeSend: function(xhr) {
            $('#overlay').show();
            $("#product_main_container").after($("<center><li class='loading'>Loading...</li></center>").fadeIn('slow')).data("loading", true);
        },
        success:function(response){
            category_type = response['category_type'];
            if(category_type == 'Packages' || category_type == 'Combos'){
                if(!$("#product_main_container").hasClass('combo')){
                    $("#product_main_container").addClass('combo');
                }
            }else{
                if($("#product_main_container").hasClass('combo')){
                    $("#product_main_container").removeClass('combo');
                }
            }
            $('#overlay').css('display','none');
            $(".content-expand").fadeOut('slow');
            $('#HeaderDescription').html(response.page_description);
            $('#HeaderHeading').html(response.heading+'<i class="icn icn-arrow-up-gray" id="clickToggle"></i>');
            $('.breadcrumbs-listing').html(response.breadcump);
            var filters = '';
            var mobile_filters = '';
            if(filter == null){
                if(response.filters.length > 0){
                    for (var i = 0; i <= response.filters.length-1; i++) {
                        var new_filters = response.filters[i]
                        var checked = '';
                        filters += '<div class="checkbox-grp">\
                                    <input type="checkbox" name="" id="'+ new_filters.filter_tag +'" value="'+ new_filters.filter_tag +'">\
                                    <label for="'+ new_filters.filter_tag +'">'+ new_filters.filter_name +'</label>\
                                </div>';
                        mobile_filters += '<div class="checkbox-grp">\
                            <input type="checkbox" name="" id="mobile_'+ new_filters.filter_tag +'" value="'+ new_filters.filter_tag +'">\
                            <label for="mobile_'+ new_filters.filter_tag +'">'+ new_filters.filter_name +'</label>\
                        </div>';
                    }
                    $('#Customefilters').html(filters);
                    $('#Customefilters_mobile').html(mobile_filters);
                }
                else{
                   filters += '<div class="checkbox-grp">\
                                    <input type="checkbox" name="" id="'+type+'_all" value="'+type+'_all" checked>\
                                    <label for="'+type+'_all">All</label>\
                                </div>';
                    mobile_filters += '<div class="checkbox-grp">\
                                    <input type="checkbox" name="" id="mobile_'+type+'_all" value="'+type+'_all" checked>\
                                    <label for="mobile_'+type+'_all">All</label>\
                                </div>';
                    $('#Customefilters').html(filters);
                    $('#Customefilters_mobile').html(mobile_filters);
                }
            }
            
            if(response.subCat[0].subcat_seourl != null){
                var subcategory = '<div class="mobile-category-slider d-none d-block-xs">';
                $('#mobileCatSlider').show();
                for (var i = 0; i <= response.subCat.length-1; i++) {
                    var new_subcat = response.subCat[i]
                    if(new_subcat.subcat_seourl != '' && new_subcat.subcat_sub_cat_status == 'Active'){
                        subcategory += ' <div class="slide" onclick=onCategoryChange("'+ $.trim(cat_slug) + '","'+ $.trim(new_subcat.subcat_seourl) +'","c")>\
                            <div class="mobile-category-content flex-full justify-content-center">\
                                <i class="icn icn-dining-room"></i>\
                                <span>'+ new_subcat.subcat_sub_cat_name +'</span>\
                            </div>\
                        </div>';
                    }
                }
                subcategory += '</div>';
                $(document.body).addClass("product-listing");
                $('#mobileCatSlider').html(subcategory);
               	$('.mobile-category-slider').slick({
            		dots: false,
            		arrows: true,
            		slidesToShow: 3,
            		infinite: false,
            		prevArrow: '<span class="prev-arrow"><i class="icn-arrow-left"></i></span>',
            		nextArrow: '<span class="next-arrow"><i class="icn-arrow-right"></i></span>'
            	});
            }else{
                $('#mobileCatSlider').hide();
                $(document.body).removeClass("product-listing");
            }
            
            if(type == 'p'){
                $('#parent_slug').val(seo_url);
                $("input[name='category']").prop('checked', false);
            } 
            $('#product_main_container').html('');
           
            $(".loading").fadeOut('slow', function() {
                $(this).remove();
            });
            if(response.products.length <= 12){
                isAvailableRecords = true;
            }

            for (var i = 0; i <= response.products.length-1; i++) {
                var product = response.products[i];
                var image  = product.image.split(',');
                var product_name = product.product_name.replace(/\s+/g, '-');
                var seo_url = product.seourl;
                var description = product.description.substr(0, 70);
                // var soldOutLabel = '';
                if(product.subproducts != ''){ 
                    var subprodcuts_array =   product.subproducts.split(',');
                    count = subprodcuts_array.length;
                }
                else
                {
                    count = 1;
                }
                var Item_sold = '';
                 var html  = '';
                // var html = '<div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 prolistcol">\
                //             <div class="productlistitem">';
                var ISsold = '';
                var notifyBtn = '';
                
                if(product.is_sold){
                    Item_sold =  '<div class="ribbon ribbon-top-right"><span>Out of Stock</span></div><div class="overlay"></div>';
                    notifyBtn  = '<div class="overlay"></div><div class="button"><a href="#"> Notify Me </a></div>';
                }
                
                var whishlistspan = '<span class="wishlist" onclick="addtowhishlist('+ product.id +')"><i id="whished_'+product.id+'" class="icn icn-wishlist-fill-gray"></i></span>';
                if(product.like == '1'){
                    whishlistspan = '<span class="wishlist" onclick="addtowhishlist('+ product.id +')"><i id="whished_'+product.id+'" class="icn icn-wishlist-fill-red"></i></span>';
                }
                html += '<li><div class="product-single flex-full align-content-start position-relative">\
                            <div class="product-image flex-full position-relative" id="DynamicWishlist_'+product.id+'">\
                                <a href="'+ baseURL +'things/'+ product.id + '/'+ seo_url + '" class="flex-full position-relative h-100" target="_blank">\
                                    <amp-img src="https://d3juy0zp6vqec8.cloudfront.net/images/product/Copressed Images/'+ image[0] +'" alt="'+ product.product_name +'" layout="responsive" width="300" height="225">\
                                        <noscript>\
                                            <img src="<?php echo base_url(); ?>/images/ajax-loader/ajax-loader.gif" alt="Example" loading="lazy">\
                                        </noscript>\
                                    </amp-img>'+Item_sold+'\
                                </a>\
                                    <span class="'+ product.product_label +'">'+ product.product_label.split("_").join(" ")+'</span> '+ whishlistspan+notifyBtn +'\
                            </div>\
                            <div class="product-description flex-full">\
                                <div class="product-description-wrapper flex-full">\
                                    <h3><a href="'+ baseURL +'things/'+ product.id + '/'+ seo_url + '" target="_blank">'+ product.product_name +'</a></h3>\
                                    <p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> '+ product.sale_price +'</strong> / mon';
                                    if(product.subproductimages.length > 0){
                                     html +=' <span class="items-count" onclick="openSubBox("'+ product.id +'")"> +'+product.subproductimages.length+' item </span>';
                                    } 
                                    html +='</p>\
                                </div>\
                            </div>\
                        </div></li>';
                // $('#product_main_container').append(html);
                $("#product_main_container").append(html).hide().show('slow');
            }
            pageNumber = 1;
            listingType = 0;
            alreadyFetchingData = false;
            $("img").unveil(400);
        }
    });  
}

var sortingValue = '';
function sortProductlisting(cat_slug,seo_url,value,type = null){
    if(type == 'xs'){
        sortingValue = value;
        $('.mobile-filterbox').removeClass('open');
    }else{
        if(value.value){
            sortingValue = value.value;
        }else{
            sortingValue = $(value).attr('data-attribute');
            console.log($(value).attr('data-attribute'));
            $(value).siblings().removeClass('selected');
            $(value).addClass('selected');
            if(sortingValue == 'all'){
                $(".sort-by > .form-inline > span").html('Default <i class="icn-arrow-bottom"></i>');
            }else if(sortingValue == 'new'){
                $(".sort-by > .form-inline > span").html('New <i class="icn-arrow-bottom"></i>');
            }else if(sortingValue == 'low_high'){
                $(".sort-by > .form-inline > span").html('Price - Low-High <i class="icn-arrow-bottom"></i>');
            }else if(sortingValue == 'high_low'){
                $(".sort-by > .form-inline > span").html('Price - High-Low <i class="icn-arrow-bottom"></i>');
            }
        }
    }
    // pageNumber = 0;
      $.ajax({
        type: 'POST',
        url: baseURL + 'site/searchShop/search_shopby',
        dataType: 'json',
        async:false,
        data: { 
            'url':baseURL + cat_slug + '/' + $('#category_slug').val(),
            'load':'first',
            'cat_slug':$('#category_slug').val(),
            'sorting':sortingValue
        },
        beforeSend: function(xhr) {
            $('#overlay').show();
        },
        success:function(response){
            category_type = response.category_type;
            $('#overlay').css('display','none');
            var html  = '';
            if(response.products.length == 12){
                isAvailableRecords = true;
            }

            if(category_type == 'Packages' || category_type == 'Combos'){
                for (var i = 0; i <= response.products.length-1; i++) {
                    var product = response.products[i];
                    var image  = product.image.split(',');
                    var product_name = product.product_name.replace(/\s+/g, '-');
                    var seo_url = product.seourl;
                    var description = product.description.substr(0, 70);
                    if(product.subproducts != ''){ 
                        var subprodcuts_array =   product.subproducts.split(',');
                        count = subprodcuts_array.length;
                    }
                    else
                    {
                        count = 1;
                    }
                    var Item_sold = '';
                    var ISsold = '';
                    var notifyBtn = '';
                    if(product.is_sold){
                        Item_sold =  '<div class="ribbon ribbon-top-right"><span>Out of Stock</span></div><div class="button"><a href="#"> Notify Me </a></div>';
                        notifyBtn = '<div class="overlay"></div><div class="button"><a href="#"> Notify Me </a></div>';
                    }
                    
                    var whishlistspan = '<span class="wishlist" onclick="addtowhishlist('+ product.id +')"><i id="whished_'+product.id+'" class="icn icn-wishlist-fill-gray"></i></span>';
                    if(product.like == '1'){
                        whishlistspan = '<span class="wishlist" onclick="addtowhishlist('+ product.id +')"><i id="whished_'+product.id+'" class="icn icn-wishlist-fill-red"></i></span>';
                    }
                    html += '<li><div class="product-single flex-full align-content-start position-relative">\
                                <div class="product-image flex-full position-relative" id="DynamicWishlist_'+product.id+'">\
                                    <a href="'+ baseURL +'things/'+ product.id + '/'+ seo_url + '" class="flex-full position-relative h-100" target="_blank">\
                                        <amp-img src="https://d3juy0zp6vqec8.cloudfront.net/images/product/Copressed Images/'+ image[0] +'" alt="'+ product.product_name +'" layout="responsive" width="300" height="225">\
                                            <noscript>\
                                                <img src="<?php echo base_url(); ?>/images/ajax-loader/ajax-loader.gif" alt="Example" loading="lazy">\
                                            </noscript>\
                                        </amp-img>\
                                    </a>';
                                            if(product.product_label != ""){
                                                html += '<span class="new-label">'+ product.product_label.split("_").join(" ")+'</span> '+ whishlistspan +notifyBtn;
                                            }else{
                                                html += whishlistspan +notifyBtn;
                                            }
                                    html += '</div>\
                                <div class="product-description flex-full">\
                                    <div class="product-description-wrapper flex-full">\
                                        <h3><a href="'+ baseURL +'things/'+ product.id + '/'+ seo_url + '" target="_blank">'+ product.product_name +'</a></h3>\
                                        <p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> '+ product.sale_price +'</strong> / mon</p>\
                                    </div>';
                                    if(product.subproductimages.length > 0){
                                        html += `
                                            <div class="included-items-block flex-full">
                                                <h4>${product.subproductimages.length} Items Included</h4>
                                                <ul class="included-items-listing flex-full">`;
                                                for(k = 0;k < product.subproductimages.length;k++){
                                                    product_image = product.subproductimages[k].image.split(',');
                                                    html += `
                                                        <li>
                                                            <a href="javascript:void(0)">
                                                                <amp-img src="${baseURL}images/product/Copressed Images/${product_image[0]}" alt="Product Image"
                                                                            width="78" height="59"></amp-img>
                                                            </a>
                                                        </li>
                                                    `;
                                                }
                                                html += `</ul>
                                            </div>
                                        `;
                                    }
                                    html += '</div>\
                            </div></li>';
                    // $('#product_main_container').append(html);
                }
            }else{
                for (var i = 0; i <= response.products.length-1; i++) {
                    var product = response.products[i];
                    var image  = product.image.split(',');
                    var product_name = product.product_name.replace(/\s+/g, '-');
                    var seo_url = product.seourl;
                    var description = product.description.substr(0, 70);
                    if(product.subproducts != ''){ 
                        var subprodcuts_array =   product.subproducts.split(',');
                        count = subprodcuts_array.length;
                    }
                    else
                    {
                        count = 1;
                    }
                    var Item_sold = '';
                    var ISsold = '';
                    var notifyBtn = '';
                    if(product.is_sold){
                        Item_sold =  '<div class="ribbon ribbon-top-right"><span>Out of Stock</span></div><div class="button"><a href="#"> Notify Me </a></div>';
                        notifyBtn = '<div class="overlay"></div><div class="button"><a href="#"> Notify Me </a></div>';
                    }
                    
                    var whishlistspan = '<span class="wishlist" onclick="addtowhishlist('+ product.id +')"><i id="whished_'+product.id+'" class="icn icn-wishlist-fill-gray"></i></span>';
                    if(product.like == '1'){
                        whishlistspan = '<span class="wishlist" onclick="addtowhishlist('+ product.id +')"><i id="whished_'+product.id+'" class="icn icn-wishlist-fill-red"></i></span>';
                    }
                    html += '<li><div class="product-single flex-full align-content-start position-relative">\
                                <div class="product-image flex-full position-relative" id="DynamicWishlist_'+product.id+'">\
                                    <a href="'+ baseURL +'things/'+ product.id + '/'+ seo_url + '" class="flex-full position-relative h-100" target="_blank">\
                                        <amp-img src="https://d3juy0zp6vqec8.cloudfront.net/images/product/Copressed Images/'+ image[0] +'" alt="'+ product.product_name +'" layout="responsive" width="300" height="225">\
                                            <noscript>\
                                                <img src="<?php echo base_url(); ?>/images/ajax-loader/ajax-loader.gif" alt="Example" loading="lazy">\
                                            </noscript>\
                                        </amp-img>\
                                    </a>';
                                            if(product.product_label != ""){
                                                html += '<span class="new-label">'+ product.product_label.split("_").join(" ")+'</span> '+ whishlistspan +notifyBtn;
                                            }else{
                                                html += whishlistspan +notifyBtn;
                                            }
                                    html += '</div>\
                                <div class="product-description flex-full">\
                                    <div class="product-description-wrapper flex-full">\
                                        <h3><a href="'+ baseURL +'things/'+ product.id + '/'+ seo_url + '" target="_blank">'+ product.product_name +'</a></h3>\
                                        <p class="price"><span class="starts-from">Starts from</span> <strong><i class="rupees-symbol">&#x20B9;</i> '+ product.sale_price +'</strong> / mon';
                                        if(product.subproductimages.length > 0){
                                         html +=' <span class="items-count" onclick="openSubBox("'+ product.id +'")"> +'+product.subproductimages.length+' item </span>';
                                        } 
                                        html +='</p>\
                                    </div>\
                                </div>\
                            </div></li>';
                    // $('#product_main_container').append(html);
                }
            }

            $("#product_main_container").html(html).hide().show('slow');
            pageNumber = 1;
            alreadyFetchingData = false;
        }
    });  
}




function BulkOrder() {
    $("#bulk_order_form").validate({
        rules: {
            User_Name: {
                required: !0,
                minlength: 2,
                maxlength: 20
            },
            Email: {
                required: !0,
                email: !0
            },
            Phone: {
                required: !0,
                minlength: 10,
                maxlength: 12,
                number: !0
            },
            City: {
                required: !0,
                minlength: 2,
                maxlength: 20
            },
            Message: {
                required: !0,
                minlength: 5,
                maxlength: 500
            }
        },
        messages: {
            User_Name: {
                required: "<font color='#ef534e'>Name cannot be empty.</font>",
                minlength: "<font color='#ef534e'>Name has atleast 2 character long.</font>",
                maxlength: "<font color='#ef534e'>Name should be 20 characters maximum.</font>"
            },
            Email: {
                required: "<font color='#ef534e'>Email cannot be empty.</font>",
                email: "<font color='#ef534e'>Please enter a valid email address.</font>"
            },
            Phone: {
                required: "<font color='#ef534e'>Number cannot be empty.</font>",
                minlength: "<font color='#ef534e'>Number has atleast 10 character long.</font>",
                maxlength: "<font color='#ef534e'>Number should be 12 characters maximum.</font>",
                number: "<font color='#ef534e'>Please enter a valid Mobile Number.</font>"
            },
            City: {
                required: "<font color='#ef534e'>City cannot be empty.</font>",
                minlength: "<font color='#ef534e'>City has atleast 2 character long.</font>",
                maxlength: "<font color='#ef534e'>City should be 20 characters maximum.</font>"
            },
            Message: {
                required: "<font color='#ef534e'>Message cannot be empty.</font>",
                minlength: "<font color='#ef534e'>Message has atleast 5 character long.</font>",
                maxlength: "<font color='#ef534e'>Message should be 500 characters maximum.</font>"
            }
        },
        submitHandler: function(e) {
            $("#User_Name").val(), $.ajax({
                type: "POST",
                url: baseURL + "site/order/insert_bulk_order",
                dataType: "json",
                data: {
                    name: $("#User_Name").val(),
                    email: $("#Email").val(),
                    phone: $("#Phone").val(),
                    city: $("#City").val(),
                    message: $("#Message").val()
                },
                beforeSend: function() {
                    $("#bulk_order_button").attr("disabled", "disabled"), $("#response_msg").show(), $("#response_msg").text("Thank You For Your Order")
                },
                success: function(e) {
                    $("#response_msg").fadeIn("slow");
                    $("#bulk_order_form")[0].reset();
                    // e && ($("#response_msg").fadeOut("slow"), $("#bulk_order_form")[0].reset(), $("#bulk_order_button").removeAttr("disabled", "disabled"))
                }
            })
        }
    })
}

function Send_Voucher(e, t) {
    confirm("Do you really want to send voucher mail ?") && $.ajax({
        type: "POST",
        url: baseURL + "admin/order/send_voucher_email",
        data: {
            user_id: e,
            order_id: t
        },
        beforeSend: function() {
            $("#admin_loader").show()
        },
        success: function(e) {
            $("#admin_loader").hide(), e.status ? alert(e.msg) : alert("Please Try Again")
        }
    })
}

function payment_method(e) {
    $(".main_div").show(), $("#new_div").empty(), $("#payment_div").empty(), "cc_card" == e ? (payment_button = '<div class="checkdiv"><input id="type_recurring" name="is_recurring" type="checkbox" value="true" onchange=\'change_label()\' ><label for="type_recurring">Select to register for Standing Instructions on provided credit Card</label></div>', auto_select_button = '<input class="btn-std  btn-check" type="submit" id="form_submit_button1" value="Proceed To Payment">', $("#new_div").html(auto_select_button), $("#payment_div").html(payment_button)) : "dc_card" == e ? (auto_select_button = '<input class="btn-std  btn-check" type="submit" id="form_submit_button1" value="Proceed To Payment">', $("#new_div").html(auto_select_button)) : "nb" == e && (auto_select_button = '<input class="btn-std  btn-check" type="submit" id="form_submit_button1" value="Proceed To Payment">', $("#new_div").html(auto_select_button))
}

function change_label() {
    $("#new_div").empty();
    var e = "";
    if ($("#type_recurring").val(), $("input:radio[name=payment_type]:checked").val(), $("#type_recurring").is(":checked")) {
        var t = '<div class="delivaryaddlocate" ><ul><li type="1" id="recurring_offer"><font color="red">Congratulations, you will get gift vouchers worth Rs ' + $("#voucher_money").val() + " by email.</font></li>";
        e = '<font color="red">' + (t += "<li><a href='" + baseURL + 'pages/faq\' target="_blank"><u>Refer FAQ for details regarding standing instructions and gift vouchers</u><a></li></ul>') + '</font><br><input class="btn-std  btn-check" type="submit" id="form_submit_button1" value="Proceed To Payment"></div>', $("#new_div").html(e)
    } else e = '<input class="btn-std  btn-check" type="submit" id="form_submit_button1" value="Proceed To Payment">', $("#new_div").html(e)
}
$(window).scroll(function() {
    var e = $(".new_scroll").offset().top,
        t = $(".new_scroll").outerHeight(),
        a = $(window).height();
        var listing_page = $('#listing_page').val();
    $(this).scrollTop() > e + t - a && !alreadyFetchingData && listing_page == 'yes' && isAvailableRecords && (alreadyFetchingData = !0, get_data())
}), $(function() {
    $("#form_submit_button").attr("disabled", !0)
});
var clicks = 0;

function Add_more_receipt_email() {
    var e = '<div class="row" id=' + (clicks += 1) + '><input type="text" id="new_reporting_email" class="large tipTop" ><button type="button" onclick="add_more_recipent_email(' + clicks + ')">Add</button><button type="button" onclick="remove_recipent_email(' + clicks + ')">Remove</button><br></div>';
    $("#new_email_address").html(e)
}

function remove_recipent_email(e) {
    $("#" + e).remove()
}

function add_more_recipent_email(e) {
    var t = [],
        a = $("#report_receipt_mail").val(),
        o = $("#new_reporting_email").val();
    t.push(a), t.push(o), $("#report_receipt_mail").val(t), $("#new_reporting_email").val("")
}

function serach_on_home_page() {
    var e = document.getElementById("home_page_search").value;
    var catSlug = document.getElementById("catSlug").value;
    "" == e ? $("#home_search_div").hide() : $.ajax({
        type: "POST",
        url: baseURL + "site/product/search_product",
        dataType: "json",
        data: {
            searchValue: e
        },
        success: function(e) {
            if ($(".home_serach_container").show(), e.product.length > 0) {
                $(".orverflow_search").css({
                    overflow: "initial"
                }), $(".orverflow_search > figure").css({
                    overflow: "initial"
                }), $("#home_search_div").show(), $(".search_data").empty(), $("#home_no_found").hide();
                for (var t = 0; t < e.product.length; t++) {
                    var a = e.product[t].image.split(","),
                        o = e.product[t].product_name.replace(/\s+/g, "-");
                    $(".search_data").append('<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 margin-bottom-30" ><a href="things/' + e.product[t].id + "/" + o + '"><figure><div class="searchprothumg"><img  class="home_search_img" src="https://d1eohs8f9n2nha.cloudfront.net/images/product/Copressed Images/' + a[0] + '" alt=/></div><figcaption><span><strong>' + e.product[t].product_name + "</strong></span><span>&#8377 " + e.product[t].price + "</span></figcaption></figure></a></div>")
                }
            } else $(".orverflow_search").attr("style", ""), $(".search_data").empty(), $("#home_no_found").show(), $("#home_new_text").html("No Products Found");
            if (e.cat.length > 0)
                for ($(".orverflow_search").css({
                        overflow: "initial"
                    }), $(".orverflow_search > figure").css({
                        overflow: "initial"
                    }), $(".new_cat_data").empty(), $("#not_found_cat").hide(), t = 0; t < e.cat.length; t++) $(".new_cat_data").append('<div class="catagorycol"><a href="' + baseURL + catSlug + "/" + e.cat[t].seourl + '"><figure><img src="https://d1eohs8f9n2nha.cloudfront.net/images/category/' + e.cat[t].image + '" alt="catagory icon"><figcaption>' + e.cat[t].cat_name + "</figcaption></figure></a></div>");
            else $(".new_cat_data").empty(), $("#new_cat_data").html("No Category Found")
        }
    })
}

function verify_otp() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: baseURL + "site/User/verify_otp",
        data: {
            otp: $("#new_otp").val(),
            mobile_number: $("#phone_no").val()
        },
        success: function(e) {
            200 == e.status_code ? ($("#is_validate").html("(Verified)"), $("#is_validate").css({
                color: "green"
            }), $(".otp_section").addClass('d-none'), $("#do_verify").hide(), $("#clockdiv").hide(), sweetAlert({
                title: "Success!",
                text: e.message,
                type: "success",
                showCancelButton: !1,
                closeOnConfirm: !0,
                animation: "slide-from-top",
                showConfirmButton: !0
            })) : sweetAlert({
                title: "Opps!",
                text: e.message,
                type: "error",
                showCancelButton: !1,
                closeOnConfirm: !0,
                animation: "slide-from-top",
                showConfirmButton: !0
            })
        }
    })
}

function verify_button() {
    $("#new_otp").val(""), 10 == $("#phone_no").val().length ? ($("#home_do_verify").show(), $(".small-text-details").css("position", "absolute")) : $("#home_do_verify").hide()
}

function Send_otp() {
    $("#home_do_verify").attr("disabled"), $.ajax({
        type: "POST",
        dataType: "json",
        url: baseURL + "site/User/send_otp",
        data: {
            send_to: $("#phone_no").val()
        },
        success: function(e) {
            "success" == e ? ($(".otp_field").hide(), $(".Verify_otp_field").show(), $("#error_msg").hide()) : ($("#error_msg").show(), $("#error_msg").text(e))
        }
    })
}

function verify_otp_home() {
    $.ajax({
        type: "POST",
        dataType: "json",
        url: baseURL + "site/User/verify_otp",
        data: {
            otp: $("#new_otp").val(),
            mobile_number: $("#phone_no").val()
        },
        success: function(e) {
            200 == e.status_code ? ($(".otp_field").show(), $(".Verify_otp_field").hide(), $("#home_do_verify").hide(), $("#sign_up_button *").removeAttr("disabled"), alert(e.message)) : alert(e.message)
        }
    })
}

function CreateZohoCases(e, t) {
    $("#overlay").show(), $.ajax({
        type: "POST",
        dataType: "json",
        url: baseURL + "site/zoho/CreateAllZohoInvoices",
        data: {
            user_id: e,
            dealcodenumber: t
        },
        success: function(e) {
            200 == e.code && $("#overlay").css("display", "none")
        }
    })
}


function OpenOfficeInquiryPage(){
    $('#showInquiryForm').modal('show');
       $.ajax({
                type: 'GET',
                dataType: 'json',
                url: baseURL + 'site/user/getUserDetails',
                data:{ },
                success:function(response){
                    if(response.data){
                        $('#inquiry_user_name').val(response.data.full_name);
                        $('#inquiry_user_email').val(response.data.email);
                        $('#inquiry_user_mobile').val(response.data.phone_no);
                        $('#inquiry_user_city').val(response.data.city);
                    }
                }

            });
}

function SubmitAddOfficeFurnishtureRequest(){
    $("#office_furniture_inquiry").validate({
        rules: {
            inquiry_user_name: {
                required:true, 
                minlength: 2,
                maxlength: 20              
            },
            inquiry_user_email: {
                required:true,
                email: true                  
            },                
            inquiry_user_mobile: {
                required:true,
                minlength:10,
                maxlength:10,
                number: true            
            },
            inquiry_user_city: {
                required:{
                    depends: function(element){
                        if('default' == $('#inquiry_user_city').val()){
                            $('#inquiry_user_city').val('');
                        }
                        return true;
                    }
                }                         
            }, 
            inquiry_user_remarks: {
                required:true,
                minlength: 30,
                maxlength: 400                             
            },
        },          
        messages:{ 
            inquiry_user_name: {
                required  : "Name cannot be empty.",
                minlength : "Name has atleast 2 character long.",
                maxlength : "Name should be 20 characters maximum."
            },              
            inquiry_user_email: {
                required  : "Email cannot be empty.",
                 email: "Please enter a valid email address."
            },
            inquiry_user_mobile: {
                required  : "Number cannot be empty.",
                minlength : "Number has atleast 10 character long.",
                maxlength : "Number should be 10 characters maximum.",
                number: "Please enter a valid Mobile Number."
            },   
            inquiry_user_city: {
                required : "City cannot be empty."
            },
            inquiry_user_remarks: {
                required : "Remark cannot be empty.",
                minlength : "Remark has atleast 30 character long.",
                maxlength : "Remark length should be 400 characters maximum."
            },
        },
        submitHandler: function(form) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: baseURL + 'site/product/OfficeFurniureRequest',
                data:{
                    'user_name':$('#inquiry_user_name').val(),
                    'email':$('#inquiry_user_email').val(),
                    'phone':$('#inquiry_user_mobile').val(),
                    'city':$('#inquiry_user_city').val(),
                    'remark':$('#inquiry_user_remarks').val(),
                },
                beforeSend:function(){
                    $('#overlay').show();
                    $('#requestSubmitButton').attr('disabled','disabled');
                    $('#furniture_order_msg').show();
                    $('#furniture_order_msg').html("<font color='green'>Thank You For Your Order</font>");
                },
                success:function(response){
                    $('#overlay').css('display','none');
                    if(response){
                        sweetAlert({
                            title: "Success!",
                            text: 'Thank you for your enquiry. We will get in touch with you shortly.',
                            type: "success",
                            showCancelButton: false,
                            closeOnConfirm: true,
                            animation: "slide-from-top",
                            showConfirmButton: true,      
                        },  function(isConfirm){   
                                    if (isConfirm) {
                                       $('#showInquiryForm').modal('hide');
                                    }
                                }
                        ); 
                        $('#furniture_order_msg').fadeOut('slow');
                        $('#office_furniture_inquiry')[0].reset();
                        $('#requestSubmitButton').removeAttr('disabled','disabled');
                    }
                }

            });
        }   
    });

}


function change_city(){
    $.ajax({
            type: 'POST',
            dataType: 'json',
            url: baseURL + 'crone/get_state',
            data:{
                'city': $('#city').val()
            },
            success:function(response){
               $('#state').val(response.data);
               	$('.FlowupLabels').FlowupLabels({
            		// Handles the possibility of having input boxes prefilled on page load
            		feature_onInitLoad: true, 
            		
            		// Class when focusing an input
            		class_focused: 		'focused',
            		// Class when an input has text entered
            		class_populated: 	'populated'	
            	});
            }

    });
}


var filter_array = [];
 $(document).ready(function(){
    if (window.location.href.indexOf('?') > -1) {
        var query = location.search.substr(1);
        var params = query.split("&");
        var CutomeFilters = [];
        for(var i=0; i<params.length; i++) {
            var item = params[i].split("=");
            filter_array.push(item[1])
        }
    }
//  Custome filler options
    $('.category-box').on("change",'input[type=checkbox]',function() { // while you're at it listen for change rather than click, this is in case something else modifies the checkbox
        if($('.category-box input[type="checkbox"]:checked').length > 0){
            $(".reset-small").show();
        }else{
            $(".reset-small").hide();
        }
        
        if($(this).prop("checked") == true){
            var filterName = $(this).val().split("_").join(" ");
            $('.justify-content-end').append('<li  class="custome_'+ $(this).val() +'"><span class="tag firstcapital">'+ filterName +'<a href="javascript:void(0)" onclick=removefilter("'+$(this).val()+'");><i class="icn icn-close-blue"></i></a></span></li>');
            filter_array.push($(this).val());
        } else if($(this).prop("checked") == false){
            $('.custome_'+$(this).val()).remove();
            var index = filter_array.indexOf($(this).val());
            if (index !== -1) filter_array.splice(index, 1);
        }
        
        if(filter_array.length > 0){
            ApplyFilters(filter_array);
        }else{
             filter_array = [];
             onCategoryChange($('#cat_slug').val(),$('#category_slug').val(),'','');
        }
    });
    
    $('#sort-link').click(function(){
         $("li[data-tab='tab-1']").removeClass("current");
         $("li[data-tab='tab-2']").addClass("current");
         $('#tab-1').removeClass('current');
         $('#tab-2').addClass('current');
    })
    $('#filter-link').click(function(){
         $("li[data-tab='tab-2']").removeClass("current");
         $("li[data-tab='tab-1']").addClass("current");
         $('#tab-2').removeClass('current');
         $('#tab-1').addClass('current');
    })
})

var pageCount = 1;
function ApplyFilters(filter){
   if($('#category_slug').val() == $('#parent_slug').val()){
        onCategoryChange($('#cat_slug').val(),$('#category_slug').val(),'',filter);
   }else{
        onCategoryChange($('#cat_slug').val(),$('#category_slug').val(),'',filter);
   }
}

function resetFilter(){
	window.history.pushState('page2', 'Title', baseURL + $('#cat_slug').val() + '/' + $('#category_slug').val());
    $(".reset-small").hide();
    $('.category-box input[type="checkbox"]').each(function() {
	    this.checked = false;
		$('.custome_'+$(this).val()).remove();
	});
	filter_array = [];
	onCategoryChange($('#cat_slug').val(),$('#category_slug').val(),'','');
}

function removefilter(className){
    $('.custome_'+className).remove();
    $("#"+className).prop("checked", false);
    if($('.category-box input[type="checkbox"]:checked').length == 0){
	    $(".reset-small").hide();	
	};
    var index = filter_array.indexOf(className);
    // console.log("index",index);
    // return false;
    if (index !== -1) filter_array.splice(index, 1);
    if(filter_array.length > 0){
        ApplyFilters(filter_array);
    }else{
        filter_array = [];
        window.history.pushState('page2', 'Title', baseURL + $('#cat_slug').val() + '/' + $('#category_slug').val());
        onCategoryChange($('#cat_slug').val(),$('#category_slug').val(),'',filter_array);
    }
}  


function openSubBox(product_id){
    // $('#subProductList_'+product_id).show();
    // 	$("#subProductList_"+product_id).slideToggle().toggleClass('expanded');
		$(this).slideToggle().toggleClass('expanded');
}

function VerifyWalletAndApply(type= 'null'){
    var wallet_amount = $('#wallet_amount').val();
    // console.log(wallet_amount.match(/^\d+(\.\d+)?$/));
    if((!wallet_amount.match(/^\d+(\.\d+)?$/))){
        alert("Please Enter Valid Amount");
    }else{
        $.ajax({
            type: 'post',
            url: baseURL + 'site/user/checkAndUpdateWallet',
            data: {
                'wallet_amount':wallet_amount,
                'type':type
            },
            success: function(response) {
                if(response.status == 401){
                   location.href = baseURL; 
                }else if(response.status == 400){
                    // console.log("fff");
                    $('#wallet_message').html('<label style="color:red">'+ response.message +'</label>');

                }else{
                    $('#CoinsRedeemed').html('-&#x20B9;'+wallet_amount);
                    $('#wallet_message').html('<label style="color:green">'+ response.message +'</label>');
                    $('.coins-amt').html('<i class="icn icn-coins-gold"></i>'+(response.data.original_wallet - response.data.wallet_amount ));
                    $('#CartCartGAmt1').html('&#x20B9;'+response.data.total_after_wallet_discount);
                    $('#CartCartGAmt').html('&#x20B9;'+response.data.total_after_wallet_discount);
                    $('.cartInvoiceBal').html(response.data.total_after_wallet_discount);
                    var html = '<label>Coins to be Redeemed</label> <input type="text" id="wallet_amount" name="" value="'+wallet_amount+'" readonly><span class="redeem-btn flex-full justify-content-center"><a href="javascript:void(0)" class="explore-btn" onclick="RemoveAppliedWalletAmount('+wallet_amount+','+response.data.original_wallet+',\''+type+'\')">Remove</a></span>';
                    $("#redeemcoinDiv").html(html);                
                }
                setTimeout(function(){ $('#wallet_message').html(''); }, 3000);
            }
        });
    }
}


function RemoveAppliedWalletAmount(wallet_amount = 0,original_amount,type=null){
     $.ajax({
            type: 'post',
            url: baseURL + 'site/user/removeWalletApplied',
            data: {
                'wallet_amount':wallet_amount,
                'type':type
            },
            success: function(response) {
                $('.wallet_amount_visible').remove();
                $('.wallet_grand_total').remove();
                $('#wallet_message').html('<label style="color:green">Coins Released</label>');
                $('#CoinsRedeemed').html('-&#x20B9; 0');
                $('#CartCartGAmt1').html('&#x20B9;'+ response.data.total_after_wallet_discount);
                $('#CartCartGAmt').html('&#x20B9;'+ response.data.total_after_wallet_discount);
                 $('.cartInvoiceBal').html(response.data.total_after_wallet_discount);
                $('.coins-amt').html('<i class="icn icn-coins-gold"></i>'+response.data.original_wallet);
                 var html = '<label>Coins to be Redeemed</label> <input type="text" id="wallet_amount" name="" value=""><span class="redeem-btn flex-full justify-content-center"><a href="javascript:void(0)" onclick="VerifyWalletAndApply(\''+type+'\')" class="explore-btn" >Redeem</a></span>';
                $("#redeemcoinDiv").html(html);
                setTimeout(function(){ $('#wallet_message').html(''); }, 3000);
            }
        });
}

//submit add coin form 
function SubmitPopupForm(){
    // alert("dfdff");
    $("#wallet_popup_form").validate({
        rules: {
            topup_amount: {
                required:true, 
                number: true                     
            }
        },          
        messages:{ 
            topup_amount: {
                required  : "Please enter amount.",
                number : "Please enter amount only."
            }            
        },
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function(form) {
            form.submit();
        }   
    });
}

function Update_zoho_cash(){
    var rtype = $('#rtype').val();
    var current = $('#ctype').val();
    var user_id = $('#user_id').val();
    var deal_id = $('#deal_id').val();
    var submit_type = "Payment Mode Change";
    // $('#zoho_update_button').attr('disabled','disabled');
    $.ajax({
        url: baseURL + "site/order/zoho_update",
        data: {
            'user_id':user_id,
            'deal_id':deal_id,
            'description':$('#tdescription').val(),
            'rtype': rtype,
            'ctype': current,
            'type': submit_type
        },                         
        type: 'post',
        beforeSend: function(){   
            $('#overlay').show();
        },
        success: function(result){
            $("#payment_case_form").trigger('reset');
            // $('#zoho_submit_button').removeAttr('disabled','disabled');
            $('#overlay').hide();
            if(result.status){
                // $('#myModal2').modal('hide');  
                sweetAlert({
                    title: 'Success',
                    text: result.msg,                         
                    type: "success",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    animation: "slide-from-top",
                    showConfirmButton: true,      
                });          
            }
            else{
                // $('#myModal2').modal('hide');  
                sweetAlert({
                    title: 'warning',
                    text: result.msg,                         
                    type: "warning",
                    showCancelButton: false,
                    closeOnConfirm: true,
                    animation: "slide-from-top",
                    showConfirmButton: true,      
                });
            }
        } 
    });
}




function initiateCibil(){
    if($('#voter_number').val() == '' && $('#pan_number').val() == ''){
        alert("Please provided pan number or voter id");
    }else if($('#dob').val() == ''){
        alert("please select date of birth");
    }else{
        $.ajax({
            url: baseURL + "site/user_settings/initiateCrifRequest",
            data: {
                'voter_id':$('#voter_number').val(),
                'pan_number':$('#pan_number').val(),
                'dob':$('#dob').val()  
            },                         
            type: 'post',
            beforeSend: function(){   
                $('#overlay').show();
            },
            success: function(result){
                $('#overlay').hide();
                console.log("result",result);
                if(result.type == 'questions'){
                    $('#creditScoreForm').hide();
                    $('#encodecode').val(result.encode);
                    var radioButton = '';
                    for (var i = 0; i < result.data.optionsList.length; i++) {
                        radioButton += '<div class="radiocol"><input type="radio" name="answeroption" value="'+result.data.optionsList[i]+'" id="answer_'+i+'"> <label for="answer_'+i+'">'+result.data.optionsList[i]+' </label></div><br>';
                    }
                    var html = '<form class="credit-form flex-full form-group"><div class="FlowupLabels ">\
                        <div class="fl_wrap">\
                        '+result.data.question+'.?\
                        </div></div>\
                        '+radioButton+'\
                        <div class="not-found-block flex-full justify-content-center" style="justify-content: center;text-align: center;">\
                                <a href="javacript:void(0)" onclick=AuthenticateCrifQuestions(\''+result.data.reportId+'\',\''+ result.data.orderId +'\') class="btn btn-danger explore-btn proceed">Proceed</a>\
                        </div></form>';
                    $('#creditScorequestions').html(html);
                }else{
                    // finscoredxml(result);
                }
            } 
        });
    }
}


function AuthenticateCrifQuestions(report_id,order_id){
    var radioValue = $("input[name='answeroption']:checked").val();
    if(radioValue){
        $.ajax({
            url: baseURL + "site/user_settings/verifycrifanswer",
            data: {
                'report_id':report_id,
                'order_id':order_id,
                'encode' :$('#encodecode').val(),
                'answer':radioValue
            },                         
            type: 'post',
            beforeSend: function(){   
                $('#overlay').show();
            },
            success: function(result){
                $('#overlay').hide();  
                if(result.type == 'questions'){
                    var radioButton = '';
                    for (var i = 0; i < result.data.optionsList.length; i++) {
                        radioButton += '<div class="radiocol"><input type="radio" name="answeroption" value="'+result.data.optionsList[i]+'" id="answer_'+i+'"> <label for="answer_'+i+'">'+result.data.optionsList[i]+' </label></div><br>';
                    }
                    var html = '<form class="credit-form flex-full form-group"><div class="FlowupLabels ">\
                        <div class="fl_wrap">\
                        '+result.data.question+'.?\
                        </div></div>\
                        '+radioButton+'\
                        <div class="not-found-block flex-full justify-content-center" style="justify-content: center;text-align: center;">\
                                <a href="javacript:void(0)" onclick=AuthenticateCrifQuestions(\''+result.data.reportId+'\',\''+ result.data.orderId +'\') class="btn btn-danger explore-btn proceed">Proceed</a>\
                        </div></form>';
                    $('#creditScorequestions').html(html);
                }else{
                    alert(result.data.statusDesc);
                    location.reload();
                }
            } 
        });
    }else{
        alert("Please select answer");
    }
}


function finscoredxml(result){
    $.ajax({
        url: baseURL + "site/user_settings/get_creditxmlreport",
        data: {
            'data':result
        },                         
        type: 'post',
        beforeSend: function(){   
            $('#overlay').show();
        },
        success: function(result){
            $('#overlay').hide();  
        } 
    });
}

//remove product from wishlist (wish listing page)
function removeWhishlist(pid){
    $.ajax({
        url: baseURL + "site/user_settings/removefromwishlist",
        data: {
            'pid':pid
        },                         
        type: 'post',
        beforeSend: function(){   
            // $('#overlay').show();
        },
        success: function(result){
            // $('#overlay').hide();  
            if(result.status == 200){
                $('#wishilist_count').html('( '+result.data+' Itmes )');
                $('#whishlist_count_header').html(result.data);
                $('#product_listing_'+pid).hide('slow');
            }else{
                location.reload();
            }
        } 
    });
}



//caputre all offline orders from one specific user
function CreateUserAndPlaceOrder(){
    $.validator.addMethod('decimal', function(value, element) {
        return this.optional(element) || /^((\d+(\\.\d{0,2})?)|((\d*(\.\d{1,2}))))$/.test(value);
    }, "Please enter a correct number, format 0.00");

    $("#offlineUserForm").validate({
        rules: {
            offline_full_name: 'required',
            offline_email: {
                required: true,
                email: true,
            },
            offline_mobile:{
                required:true,
                minlength:10,
                maxlength:10,
                number: true   
            },
            offline_address1:'required',
            offline_state:'required',
            offline_postal_code:{
                required:true,
                minlength: 5,
                maxlength: 6     
            },
            offline_city:{
                required:{
                    depends: function(element){
                        if('default' == $('#offline_city').val()){
                            $('#offline_city').val(null);
                        }
                        return true;
                    }
                }   
            },
            rental_amount:{
                decimal: true   
            },
        },          
        messages:{ 
            offline_full_name: 'Full name cannot be empty.',
            offline_email: {
                required  : "Email cannot be empty.",
                email: "Please enter a valid email address."
            },
            offline_mobile:{
                required  : "Number cannot be empty.",
                minlength : "Number has atleast 10 character long.",
                maxlength : "Number should be 10 characters maximum.",
                number: "Please enter a valid Mobile Number."
            },
            offline_address1:'Address1 cannot be empty.',
            offline_state:"State cannot be empty.",
            offline_postal_code:{
                required : "Postal code cannot be empty.",
                minlength : "Postal code has atleast 5 character long.",
                maxlength : "Postal code length should be 6 characters maximum."
            },
            offline_city:"Please select city.",
            rental_amount:{
             decimal: "Please enter a price only."
            },   
        },
        submitHandler: function(form) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url: baseURL + 'site/cart/placeofflineOrder',
                data:{
                    'user_name':$('#offline_full_name').val(),
                    'email':$('#offline_email').val(),
                    'mobile_no':$('#offline_mobile').val(),
                    'alert_mobile_no':$('#offline_Alter_phone').val(),
                    'password':$('#offline_password').val(),
                    'address1':$('#offline_address1').val(),
                    'address2':$('#offline_address2').val(),
                    'state':$('#state').val(),
                    'postal_code':$('#offline_postal_code').val(),
                    'city':$('#city').val(),
                    'order_status':$('#order_status').val(),
                    'rental_amount': $('#rental_amount').val() ?  $('#rental_amount').val() : 0
                },
                beforeSend:function(){
                    $('#overlay').show();
                },
                success:function(response){
                    $('#overlay').hide();
                    if(response.success == 1){
                        swal({
                            title: "Success!",
                            text: 'Your order submitted successfully.',
                            animation: "slide-from-bottom",
                            type: 'success',
                            showConfirmButton: true,
                            confirmButtonText : 'Okay',
                            confirmButtonColor: "#e8e8e8",
                            allowOutsideClick: true  
                        },  function(isConfirm){   
                            if (isConfirm) {
                                $('#sectional').hide();
                                $('#add_new_product').empty();
                                $('#new_product_added').empty();
                                $('.priceaddon').hide();
                                $('.btncartdiv').hide();
                                $('#subtotal').hide();
                                $('#add_new_product').append('<div class="emptycartbar"><img src="https://d1eohs8f9n2nha.cloudfront.net/images/empty-cart-icn.svg" alt="Empaty cart"><h5>Your Cart is Empty</h5><p>Looks like you have not chosen <br> any product yet</p></div>');
                                $('#new_product_added').append('<div class="emptycartbar"><img src="https://d1eohs8f9n2nha.cloudfront.net/images/empty-cart-icn.svg" alt="Empaty cart"><h5>Your Cart is Empty</h5><p>Looks like you have not chosen <br> any product yet</p></div>');
                                $('#empty_msg').show();
                                $('#title').hide();
                                $('.emptycartbar').show();
                                setTimeout(function() {
                                    location.href = baseURL + 'purchases';
                                }, 6000);
                            }
                        });
                    }else{
                        swal({
                            title: "Error!",
                            text: response.msg,
                            animation: "slide-from-bottom",
                            type: 'error',
                            showConfirmButton: true,
                            confirmButtonText : 'Okay',
                            confirmButtonColor: "#e8e8e8",
                            allowOutsideClick: true  
                        },  function(isConfirm){   
                            if (isConfirm) {
                               
                                // $('#sectional').hide();
                                // $('#add_new_product').empty();
                                // $('#new_product_added').empty();
                                // $('.priceaddon').hide();
                                // $('.btncartdiv').hide();
                                // $('#subtotal').hide();
                                // $('#add_new_product').append('<div class="emptycartbar"><img src="https://d1eohs8f9n2nha.cloudfront.net/images/empty-cart-icn.svg" alt="Empaty cart"><h5>Your Cart is Empty</h5><p>Looks like you have not chosen <br> any product yet</p></div>');
                                // $('#new_product_added').append('<div class="emptycartbar"><img src="https://d1eohs8f9n2nha.cloudfront.net/images/empty-cart-icn.svg" alt="Empaty cart"><h5>Your Cart is Empty</h5><p>Looks like you have not chosen <br> any product yet</p></div>');
                                // $('#empty_msg').show();
                                // $('#title').hide();
                                // $('.emptycartbar').show();
                            }
                        });
                    }
                }
            });
        }   
    });
}

function linkinvoiewithzoho(user_id,dealcodenumber){
    $.confirm({
        'title': '',
        'message': 'Want to create zoho crm and invoice.',
        'buttons': {
            'Yes': {
                'class': 'yes',
                'action': function() {
                    $.ajax({
                        type: 'post',
                        url: baseURL + 'site/zoho/createAutoCaseInZohoCRM',
                        data: {
                            'user_id':user_id,
                            'dealcodenumber':dealcodenumber
                        },
                        beforeSend:function(){
                            $('#admin_loader').show();
                        },
                        success: function(response) {
                            $('#admin_loader').hide();
                            location.reload();
                        }
                    });
                }
            },
            'No': {
                'class': 'no',
                'action': function() {
                    console.log("cancelled by user");
                }
            }
        }
    });    
}




