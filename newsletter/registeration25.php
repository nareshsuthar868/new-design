<?php $message .= '<p>
<title>Welcome Cityfurnish</title>
</p>
<style type=\"text/css\"><!--
                  @media screen and (max-width: 580px) '.
                    .tab-container'.max-width: 100%;.'
                    .txt-pad-15 '.padding: 0 15px 51px 15px !important;.'
                    .foo-txt '.padding: 0px 15px 18px 15px !important;.'
                    .foo-add '.padding: 20px !important;.'
                    .tab-padd-zero'.padding:0px !important;.'
                    .tab-padd-right'.padding-right:25px !important;.'
                    .pad-20'.padding:25px 20px 20px !important;.'
                    .social-padd-left'.padding:15px 20px 0px 0px; !important;.'
                    .offerimg'.width:100% !important;.'
                    .mobilefont'.font-size:16px !important;line-height:18px !important;.'
                  .'
--></style>
<table class=\"tab-container\" name=\"main\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"background-color: #fff; margin: 0 auto; font-family: Arial, Helvetica, sans-serif; font-size: 14px; border-collapse: collapse; width: 600px; table-layout: fixed; display: block; border-right-width: 0px; border: 1px solid #e7e7e7;\">
<tbody>
<tr>
<td style=\"width: 100%;\">
<table style=\"width: 100%; table-layout: fixed;\" cellpadding=\"0\" cellspacing=\"0px\">
<tbody>
<tr>
<td style=\"text-align: left; padding: 15px 0px 15px 20px; padding-left: 20px;\"><a href=\"https://cityfurnish.com/\"><img src=\"https://cityfurnish.com/images/logo-2.png\" alt=\"logo\" style=\"width: 150px;\" /></a></td>
<td style=\"text-align: right; padding: 15px 20px 15px 0px; width: 50%; border-right: 1px solid #e7e7e7;\" class=\"social-padd-left\"><a href=\"https://www.facebook.com/cityFurnishRental\" style=\"display: inline-block; margin: 5px 0px 0px 15px;\"><img src=\"https://cityfurnish.com/images/facebook.png\" alt=\"facebook\" width=\"18px\" /></a> <a href=\"https://twitter.com/CityFurnish\" style=\"display: inline-block; margin: 5px 0px 0px 15px;\"><img src=\"https://cityfurnish.com/images/twitter.png\" alt=\"twitter\" width=\"18px\" /></a> <a href=\"https://plus.google.com/+cityfurnish\" style=\"display: inline-block; margin: 5px 0px 0px 15px;\"><img src=\"https://cityfurnish.com/images/google-plus.png\" alt=\"google\" height=\"18px\" /></a> <a href=\"https://in.pinterest.com/cityfurnish/\" style=\"display: inline-block; margin: 5px 0px 0px 15px;\"><img src=\"https://cityfurnish.com/images/pintrest.png\" alt=\"pintrest\" width=\"18px\" /></a> <a href=\"https://www.linkedin.com/company/cityfurnish?trk=biz-companies-cym\" style=\"display: inline-block; margin: 5px 0px 0px 15px;\"><img src=\"https://cityfurnish.com/images/linkedin.png\" alt=\"linkedin\" width=\"18px\" /></a></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style=\"width: 100%;\">
<td style=\"width: 100%; border-right: 1px solid #e7e7e7;\"><img src=\"https://cityfurnish.com/images/voucher-banner.jpg\" style=\"display: block; width: 100%;\" /></td>
</tr>
<tr style=\"width: 100%;\">
<td style=\"padding-top: 50px; font-size: 40px; color: #002e40; text-align: center; border-right: 1px solid #e7e7e7;\">
<h1 style=\"color: #002e40; font-size: 40px; line-height: 36px; font-weight: bold; margin: 0px;\">Congratulations!</h1>
<span style=\"font-size: 18px;\">Transaction Success, money added in your wallet</span></td>
</tr>
<tr style=\"width: 100%;\">
<td style=\"text-align: center; padding-top: 5px; border-right: 1px solid #e7e7e7;\"><span style=\"display: inline-block; vertical-align: middle; margin-right: 10px;\"> <img src=\"https://cityfurnish.com/images/rupee-icn.png\" /> </span>&nbsp;<span color=\"#f9aa2b\" style=\"color: #f9aa2b;\"><span style=\"font-size: 45px;\"><b>'.'.AMOUNT.'.'</b></span></span></td>
</tr>
<tr style=\"width: 100%;\">
<td style=\"text-align: center; padding-top: 15px; padding-bottom: 25px; font-size: 24px; border-right: 1px solid #e7e7e7;\"><span style=\"display: inline-block; vertical-align: middle; font-size: 18px;\"> From Cityfurnish for your #'.'.ORDER_ID.'.'<br />Transaction ID  '.'.TXT_ID.'.' </span></td>
</tr>
<tr style=\"width: 100%;\">
<td style=\"padding: 30px 50px 26px; text-align: center; line-height: 24px; width: 100%; border-right: 1px solid #e7e7e7;\">
<p style=\"margin: 0px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: #b2b2b2;\">Sent by <a href=\"https://cityfurnish.com/\" style=\"color: #38373d; text-decoration: none;\">Cityfurnish</a> 525-527, Block D, JMD Megapolis, Sohna Road, Sector 48, Gurgaon, Haryana , 122018<br /><a href=\"mailto:hello@cityfurnish.com\" style=\"color: #38373d; margin-top: 8px; display: inline-block; text-decoration: none;\">hello@cityfurnish.com</a></p>
</td>
</tr>
</tbody>
</table>';  ?>