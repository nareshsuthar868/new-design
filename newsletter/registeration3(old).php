<?php $message .= '<p>
<meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\" />
<title>Welcome Cityfurnish</title>
</p>
<style type=\"text/css\"><!--
  @media screen and (max-width: 580px) '.
    .tab-container'.max-width: 100%;.'
    .txt-pad-15 '.padding: 0 15px 51px 15px !important;.'
    .foo-txt '.padding: 0px 15px 18px 15px !important;.'
    .foo-add '.padding: 20px !important;.'
    .tab-padd-zero'.padding:0px !important;.'
	.tab-padd-right'.padding-right:25px !important;.'
	.pad-20'.padding:25px 20px 20px !important;.'
	.social-padd-left'.padding:15px 20px 0px 0px; !important;.'
	.offerimg'.width:100% !important;.'
	.mobilefont'.font-size:16px !important;line-height:18px !important;.'
  .'
--></style>
<table style=\"max-width: 620px; background-color: #fff; margin: 0 auto; background: #90c5bc; font-family: Arial, Helvetica, sans-serif; font-size: 14px; border-collapse: collapse; width: 620px;\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" name=\"main\" class=\"tab-container\">
<tbody>
<tr style=\"background-color: #fff;\">
<td style=\"width: 100%;\">
<table cellspacing=\"0px\" cellpadding=\"0\" style=\"width: 100%; table-layout: fixed;\">
<tbody>
<tr>
<td style=\"text-align: left; padding: 15px 0px 15px 20px;\"><a href=\"https://cityfurnish.com/\"><img style=\"width: 150px;\" alt=\"logo\" src=\"https://cityfurnish.com/images/email/logo-2.png\" /></a></td>
<td class=\"social-padd-left\" style=\"text-align: right; padding: 15px 20px 15px 0px; width: 50%;\"><a style=\"display: inline-block; margin: 5px 0px 0px 15px;\" href=\"https://www.facebook.com/cityFurnishRental\"><img width=\"18px\" alt=\"facebook\" src=\"https://cityfurnish.com/images/email/facebook.png\" /></a> <a style=\"display: inline-block; margin: 5px 0px 0px 15px;\" href=\"https://twitter.com/CityFurnish\"><img width=\"18px\" alt=\"twitter\" src=\"https://cityfurnish.com/images/email/twitter.png\" /></a> <a style=\"display: inline-block; margin: 5px 0px 0px 15px;\" href=\"https://plus.google.com/+cityfurnish\"><img height=\"18px\" alt=\"google\" src=\"https://cityfurnish.com/images/email/google-plus.png\" /></a> <a style=\"display: inline-block; margin: 5px 0px 0px 15px;\" href=\"https://in.pinterest.com/cityfurnish/\"><img width=\"18px\" alt=\"pintrest\" src=\"https://cityfurnish.com/images/email/pintrest.png\" /></a> <a style=\"display: inline-block; margin: 5px 0px 0px 15px;\" href=\"https://www.linkedin.com/company/cityfurnish?trk=biz-companies-cym\"><img width=\"18px\" alt=\"linkedin\" src=\"https://cityfurnish.com/images/email/linkedin.png\" /></a></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style=\"background: #dbdbdb; display: table-row !important;\">
<td style=\"width: 100%;\">
<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"padding: 20px 20px 12px; width: 100%;\">
<tbody>
<tr style=\"background-color: #dbdbdb; width: 100%; display: table-row !important;\">
<td class=\"foo-add-left\" style=\"padding: 37px 10px 39px 25px; text-align: left; background: #90c5bc; vertical-align: top; width: 38%; color: #fff; font-family: Times New Roman;\">
<h1 class=\"mobilefont\" style=\"color: #fff; text-transform: uppercase; font-size: 26px; line-height: 32px; font-weight: bold; margin: 0px 0px 16px;\">Welcome to cityfurnish</h1>
<span style=\"height: 5px; width: 76px; background: #fff; display: inline-block; margin-bottom: 5px;\">&nbsp;</span>
<p class=\"cont_text\" style=\"font-family: Arial, Helvetica, sans-serif; line-height: 22px; color: #fff; font-size: 14px;\">Cityfurnish offers a wide range<br /> of stylish, elegant and modern<br /> furniture on rent.</p>
confmsg</td>
<td class=\"foo-add-left\" style=\"vertical-align: middle; width: 55%; margin-top: 0px; background: #90c5bc;\"><img style=\"width: 100%; display: inline-block; vertical-align: middle;\" alt=\"Welcome Pic\" src=\"https://cityfurnish.com/images/email/welcome--top-pic.png\" /></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style=\"background-color: #dbdbdb; display: table-row !important;\">
<td style=\"padding: 0px 20px 20px; width: 100%;\">
<table cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"width: 100%;\">
<tbody>
<tr>
<td class=\"foo-add-left-offer\" style=\"padding: 0px 12px 0px 0px; width: 48%;\"><img class=\"offerimg\" style=\"width: 100%; display: inline-block; vertical-align: middle;\" alt=\"offer\" src=\"https://cityfurnish.com/images/email/offer-left-pic.png\" /></td>
<td class=\"foo-add-left-offer\" style=\"font-family: \'Times New Roman\', Times, serif; padding: 0px 30px; vertical-align: middle; background-color: #ff9630; text-align: center; color: #fff; width: 48%;\">
<h1 class=\"summer_title\" style=\"color: #fff; text-transform: uppercase; font-size: 20px; line-height: 24px; font-weight: bold; margin: 15px 0px 25px;\">Summer Offers</h1>
<p class=\"cont_text\" style=\"margin: 5px 0px; line-height: 20px; font-family: Arial, Helvetica, sans-serif; color: #fff;\">Upto 100% off on first<br /> month rent! Offer ending tonight.</p>
<a style=\"background: #fff; line-height: 40px; display: inline-block; width: 140px; text-align: center; font-size: 12px; text-transform: uppercase; color: #ff9630; text-decoration: none; font-weight: bold; margin: 10px 0px;\" href=\"https://cityfurnish.com/\">Know More </a></td>
</tr>
</tbody>
</table>
</td>
</tr>
<tr style=\"background: #fff; width: 100%; display: table-row !important;\">
<td class=\"pad-20\" colspan=\"2\" style=\"padding: 26px 50px; text-align: center; line-height: 24px; width: 100%;\">
<p style=\"margin: 0px; line-height: 22px; font-family: Arial, Helvetica, sans-serif; color: #b2b2b2;\">Sent by <a style=\"color: #38373d; text-decoration: none;\" href=\"https://cityfurnish.com/\">Cityfurnish</a>, 530, Block D, JMD Megapolis, Sohna Road, Sector 48, Gurgaon, Haryana , 122018<br /> <a href=\"mailto:hello@cityfurnish.com\"><img alt=\"email\" src=\"https://cityfurnish.com/images/email/email.png\" /></a><a style=\"margin-left: 8px;\" href=\"tel:8010845000\"><img alt=\"email\" src=\"https://cityfurnish.com/images/email/call.png\" /></a></p>
</td>
</tr>
</tbody>
</table>';  ?>